from __future__ import print_function
import kaldi_io
import numpy
import os
import sys

file_ark = sys.argv[1]
txt_out = sys.argv[2]

for key,mat in kaldi_io.read_mat_ark(file_ark):
	numpy.savetxt(txt_out, mat, fmt="%.6f")
	
