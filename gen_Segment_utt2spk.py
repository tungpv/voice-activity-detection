import os
import sys
import time
import timeit

import numpy as np
from six.moves import xrange  # pylint: disable=redefined-builtin

def readDurationFile(durFile, segment_out, utt2spk_out):
	fw1 = open(segment_out,"w")
	fw2 = open(utt2spk_out,"w")
  	with open(durFile) as source_file:
  		line = source_file.readline()
  		while line:
  			content = line.strip().split()
  			spk = content[0].split('_')[0]
  			fw1.write(content[0] + " " + content[0] + " 0.0 " + content[1] + "\n")
  			fw2.write(content[0] + " " + spk + "\n")
  			line = source_file.readline()
  	fw1.close() 
  	fw2.close()

durFile = sys.argv[1]
segment_out = sys.argv[2] 
utt2spk_out = sys.argv[3]
readDurationFile(durFile, segment_out, utt2spk_out)



