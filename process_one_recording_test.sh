#!/bin/bash
. path.sh


audioFiles=$1
datadir=$2
left_context=$3
right_context=$4 
#### Setting
echo "======= Processing test audio files with following setting======"
echo "file list = $audioFiles" 
echo "Test datadir = $datadir"
echo "Left context = $left_context , right context = $right_context"
echo "------------------------------------------------------------"

mkdir -p $datadir

rm -r tmp
rm -r tmp_wav
mkdir -p tmp
mkdir -p tmp_wav
python segmentation_aud.py $audioFiles tmp_wav $datadir
./prepare_test_data.sh tmp_wav tmp

numChan=`cat $datadir/numChan`
numSeg=`cat $datadir/numSeg`

python saveKaldiSegFeatsToTxt.py tmp/data/raw_mfcc_online_pitch_tmp.1.ark $numChan $numSeg $datadir ### Read kaldi mfcc_pitch features and save as txt file for each pair <channel, segment>
python vad6_DNN_test_feats_extraction_v4.py $audioFiles $datadir ### Extract Tung's features


declare -a featCols_arr=("2-9" "10-53" "2-9,50-53" "2-53");
declare -a featSet_arr=("tungFeats" "kaldiAllFeats" "tungFeatsAndKaldiPitch" "tungAndAllKaldiFeats");
declare -a num_of_hidden_arr=(400 2000 1000 3000);


for (( idx=0; idx<4; idx=idx+1 ))
do
	featSet=${featSet_arr[$idx]} 
	feat_cols=${featCols_arr[$idx]} 
	model="models_$featSet/vad_dnn.ckpt"
	scaler="scaler_$featSet.mdl"
	hidd_size=${num_of_hidden_arr[$idx]}
    source activate tensorflow
	for (( i=1; i<=$numSeg; i=i+1 ))
	do
		for (( j=1; j<=$numChan; j=j+1 ))
		do
			python combineAllFeats_test.py $datadir $j $i ### For each pair <channel, segment> , we combine mfcc_pitch and Tung features
			testFile=$datadir/dat_chan${j}_seg${i}_allFeats.txt
			
			testFile_sliced=$datadir/dat_chan${j}_seg${i}_spliced_left${left_context}_right${right_context}_${featSet}_fakeLabels.txt
            outFile=$datadir/chan${j}_seg${i}_${featSet}_labels.txt
			./splice_data.sh $testFile $feat_cols $testFile_sliced $left_context $right_context
            
            python DNN_v5.4_test.py $testFile_sliced $model $scaler $outFile $hidd_size
            
		done
	done
    source deactivate
done

