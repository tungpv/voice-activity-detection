#################VAD version 5.1 with investigation ################
#The difference between this version and the previous 5.1 is that we 
#print the 
import numpy
import math
import soundfile as sf
import scipy.interpolate
import os
import random
import scipy.signal
import scipy.fftpack
import sys
import scipy.stats as ss
from copy import deepcopy
#import scipy.io.wavfile as wav

def getInsDelInfor(consideredFile, targetArr, insOrDel, startField):
    countOK = 0
    countTotalErr = 0
    countPartiallyErr = 0
    with open(consideredFile) as infile: 
        for line in infile:
            toke_line = line.strip().split()
            start = int(round(float(toke_line[startField]),2) * 100)
            end = int(round(float(toke_line[startField + 1]), 2) * 100)
            if start > len(targetArr):
                break
            l = end - start + 1
            matchedLabels = numpy.count_nonzero(targetArr[start - 1: end, 0])
            if matchedLabels == 0:
                print(insOrDel + " for segment [" + str(start/100.0) + "," + str(end/100.0) + "]")
                if end - start >= 100:
                    print("Serious warning " + insOrDel + " for segment [" + str(start/100.0) + "," + str(end/100.0) + "]")
                countTotalErr += 1
            elif l - matchedLabels >= 6:
                print("Partially " + insOrDel + " at segment [" + str(start/100.0) + "," + str(end/100.0) + "]" + " mismatch = " + str(l - matchedLabels))
                if l - matchedLabels >= 100:
                    print("Serious warning segment = [ " + str(start/100.0) + ", " + str(end/100.0) + "], mismatch = " + str(l - matchedLabels)) 
                countPartiallyErr += 1
            else:
                print("Ok Segment [" + str(start/100.0) + "," + str(end/100.0) + "]")
                countOK += 1  
    print("OK segment = " + str(countOK))
    print("Partially error = " + str(countPartiallyErr))
    print("Totally " + insOrDel + " = " + str(countTotalErr))


def evaluateVADpred(refVAD, predictedVAD, finishAt):
    label = numpy.zeros((1000000, 1), numpy.int)
    label = label[:finishAt, :]
    with open(refVAD) as infile: 
        for line in infile:
            #print ("Line = " + line)
            toke_line = line.strip().split()
            start = int(round(float(toke_line[0]),2) * 100)
            end = int(round(float(toke_line[1]), 2) * 100)
            label[start - 1: end, 0] = 1
    print("Shape of label = " + str(label.shape))
    
    pred = numpy.zeros((1000000, 1), numpy.int)
    pred = pred[:finishAt, :]
    with open(predictedVAD) as infile: 
        for line in infile:
            #print ("Line = " + line)
            toke_line = line.strip().split()
            start = int(round(float(toke_line[1]),2) * 100)
            end = int(round(float(toke_line[2]), 2) * 100)
            pred[start - 1: end, 0] = 1
    print("Shape of pred = " + str(pred.shape))
    count_true_pos = 0
    count_false_pos = 0
    count_false_neg = 0
    count_true_neg = 0
    for i in range(finishAt):
        if label[i, 0] == 0 and pred[i, 0] == 0:
            count_true_neg += 1
        elif label[i, 0] == 1 and pred[i, 0] == 1:
            count_true_pos += 1
        elif label[i, 0] == 1 and pred[i, 0] == 0:
            count_false_neg += 1
        elif label[i, 0] == 0 and pred[i, 0] == 1:
            count_false_pos += 1
    speechFrame = numpy.count_nonzero(label)
    #### Now get the statistical information #######
    getInsDelInfor(predictedVAD, label, "insertion", 1)
    getInsDelInfor(refVAD, pred, "deletion", 0)
    corr_pred = (count_true_neg + count_true_pos) * 100.0/finishAt
    fa_rate = count_false_pos * 100.0/finishAt
    miss_rate = count_false_neg * 100.0/(speechFrame)
    miss_rate_v2 = count_false_neg * 100.0/(finishAt)
    print("-----------STATISTICAL INFORMATION ---------")
    print("Number of frames = " + str(finishAt) + ", number of speech frame = " + str(speechFrame))
    print("True positive = " + str(count_true_pos) + ", true neg = " + str(count_true_neg) + ", miss = " + str(count_false_neg) + ", false alarm = " + str(count_false_pos))
    print("Correct pred = " + str(corr_pred) + " percent") 
    print("False alarm (insertion) = " + str(fa_rate) + " percent") 
    print("Miss rate (delete) = "+ str(miss_rate_v2) + " percent of all frames ( or " + str(miss_rate) + " percent of speech frame ) \n" )          

refVAD = sys.argv[1]
predVAD = sys.argv[2]
finishAt = int(sys.argv[3])
evaluateVADpred(refVAD, predVAD, finishAt)
