import numpy
import math
import soundfile as sf
import scipy.interpolate
import sys
import random
import scipy.io.wavfile as wav
import scipy.signal
import scipy.fftpack
import os
from copy import copy, deepcopy
from os.path import basename

class MyClass:
    def __init__(self):
        self.vadFlag = 0
        self.countVADON = 0
        self.frameIdx = 0
        self.nf = 0
        self.slow_nf = 0
        self.speechVar = 0
        self.th = 0
        self.th2 = 0
        self.val = 0
        self.MinRatioSpeechPeakNoiseFloor = 0
        self.ThOffset = 0
        self.MinFeatureVal = 0

class Seg:
    def __init__(self):
        self.start = []
        self.stop = []
        self.label = []

def calPowerSum_segment(audio, fs):
    flength = int(fs * 0.025);
    hsize = int(fs * 0.01);
    ##dither the speech signal
    for i in range(len(audio)):
        audio[i] = audio[i] + numpy.random.randn()/(pow(2,32))
    ## DC removal ##
    num = numpy.array([0.999,-0.999])
    denum = numpy.array([1,-0.999])
    audio = scipy.signal.lfilter(num, denum, audio)
    ## Pre-emphasis ###
    tmp_audio = numpy.zeros(len(audio))
    for i in range(1, len(audio)):
        tmp_audio[i] = audio[i] - 0.97 * audio[i-1]
    tmp_audio[0] = audio[0]
    audio = tmp_audio
    ## Perform FFT ####
    num_frame = int(math.floor((len(audio)-flength)*1.0/hsize+1))
    print ("Num sampe = " + str(len(audio)) + ", Numframe = " + str(num_frame))
    spec = numpy.zeros((257, num_frame), dtype=numpy.complex)
    for i in range(num_frame):
        frame_dat = audio[i * hsize: i * hsize + flength]
        frame_dat = frame_dat * numpy.hamming(flength)
        result = scipy.fftpack.fft(frame_dat, 512)
        result = result[0:result.size // 2 + 1]
        spec[:, i] = result
    #spec = spectrogram(audio, framelength=flength, hopsize=hsize, window=numpy.hamming, transform=scipy.fftpack.fft)
    #tmp = numpy.conjugate(spec) * spec
    tmp = numpy.multiply(spec, numpy.conjugate(spec))
    power = tmp.real
    #powerSum = [sum([row[i] for row in power]) for i in range(0,len(power[0]))]
    powerSum = power.sum(axis=0)
    return powerSum

def calPowerSum(audio):
    audio, fs = sf.read(audio)
    flength = int(fs * 0.025);
    hsize = int(fs * 0.01);
    ##dither the speech signal
    for i in range(len(audio)):
        audio[i] = audio[i] + numpy.random.randn()/(pow(2,32))
    ## DC removal ##
    coff = 0.999
    num = numpy.array([0.999,-0.999])
    denum = numpy.array([1,-0.999])
    audio = scipy.signal.lfilter(num, denum, audio)
    ## Pre-emphasis ###
    tmp_audio = numpy.zeros(len(audio))
    for i in range(1, len(audio)):
        tmp_audio[i] = audio[i] - 0.97 * audio[i-1]
    tmp_audio[0] = audio[0]
    audio = tmp_audio
    ## Perform FFT ####
    num_frame = int(math.floor((len(audio)-flength)/hsize+1))
    spec = numpy.zeros((257, num_frame), dtype=numpy.complex)
    for i in range(num_frame):
        frame_dat = audio[i * hsize: i * hsize + flength]
        frame_dat = frame_dat * numpy.hamming(flength)
        result = scipy.fftpack.fft(frame_dat, 512)
        result = result[0:result.size // 2 + 1]
        spec[:, i] = result
    #spec = spectrogram(audio, framelength=flength, hopsize=hsize, window=numpy.hamming, transform=scipy.fftpack.fft)
    #tmp = numpy.conjugate(spec) * spec
    tmp = numpy.multiply(spec, numpy.conjugate(spec))
    power = tmp.real
    #powerSum = [sum([row[i] for row in power]) for i in range(0,len(power[0]))]
    powerSum = power.sum(axis=0)
    return powerSum

def norArr(arr1, sumArr):
    for i in range(len(arr1)):
        arr1[i] = arr1[i]/sumArr[i]
    return arr1


def batch_peridoc_pitch_count_fast(testWave, Fs, numFrame, nSamplePerFrame, nSampleForward):
    x_start   = 0;
    x_end     = nSamplePerFrame;
    act_frame_x = numpy.zeros((nSamplePerFrame*2,numFrame)) # A matrix 
    for j in range(numFrame):
        #act_frame_x(:,j) =  [testWave(x_start:x_end); zeros(x_end-x_start+1,1)];
        tmp_arr = numpy.concatenate([testWave[x_start:x_end],numpy.zeros(x_end-x_start)])
        act_frame_x[:,j] = tmp_arr
        
        x_start = x_start + nSampleForward;
        x_end   = x_end   + nSampleForward;
    
    frSize, nFr = act_frame_x.shape
    binWidth = Fs/frSize;
    hammWinFrame  = numpy.hamming(frSize);
    HighPassFreqUpper = 800;
    numHighPassFreqUpperBin = int(math.ceil(HighPassFreqUpper/binWidth))
    mapX= list(range(1,numHighPassFreqUpperBin + 1)) #[1:numHighPassFreqUpperBin];
    mapX = 1.0/numHighPassFreqUpperBin * numpy.array(mapX); #mapX = 1/numHighPassFreqUpperBin.*mapX;
    #mapX = mapX';
    #numpy.savetxt('mapX.txt', mapX, fmt = '%.5f') 
    frame_x = numpy.zeros((nSamplePerFrame*2,numFrame))
    for i in range(nFr):
        tmp_norm = numpy.linalg.norm(act_frame_x[:,i])
        if tmp_norm == 0:
            print("Warning (TUNG)! norm equals 0 at " + str(i))
            for j in range(frame_x.shape[0]):
                frame_x[j,i] = random.random() * 0.00001
            continue
        frame_x[:,i] = act_frame_x[:,i]/numpy.linalg.norm(act_frame_x[:,i]);
        # so that signal level is NOT at play here!!!
    
    ###frame_x_hamm  = bsxfun(@times, frame_x, hammWinFrame);
    frame_x_hamm = numpy.zeros((nSamplePerFrame*2,numFrame))
    for i in range(nFr):
        frame_x_hamm[:,i] = numpy.multiply(frame_x[:,i], hammWinFrame)
        
    ###abs_fft_x = abs(fft(frame_x_hamm)); % FFT on each column. Hence fft(frame_x_hamm) return a vector with size = num_of_column   
    abs_fft_x = numpy.absolute(numpy.fft.fft(frame_x_hamm, axis = 0)) ##PLEASE CHECK
    #numpy.savetxt('abs_fft_x_JustAfterAbsFFT.txt', abs_fft_x, fmt = '%.5f')
    ###abs_fft_x(1:numHighPassFreqUpperBin,:) = bsxfun(@times, abs_fft_x(1:numHighPassFreqUpperBin,:), mapX);
    for i in range(nFr):
        abs_fft_x[:numHighPassFreqUpperBin,i] = numpy.multiply(abs_fft_x[:numHighPassFreqUpperBin,i], mapX)
    #numpy.savetxt('abs_fft_x_JustAfterMulWithMapX.txt', abs_fft_x, fmt = '%.6f')
    
    ###abs_fft_x = bsxfun(@times, abs_fft_x, 1./max(abs_fft_x(1:2*HighPassFreqUpper/binWidth,:)));
    max_each_col = numpy.reciprocal(numpy.amax(abs_fft_x[:2*HighPassFreqUpper/binWidth,:], axis = 0)) #Equivalent to 1./max(abs_fft_x(1:2*HighPassFreqUpper/binWidth,:))
    for i in range(frSize):
        abs_fft_x[i, :] = numpy.multiply(abs_fft_x[i, :], max_each_col)
    #numpy.savetxt('abs_fft_x_ScaleWithReciporalMax.txt', abs_fft_x, fmt = '%.6f')    
    
    ## The above operation with mapX reduces low frequency power, basically
    ## frequency filtering!!!
    
    ## extract noise level from 2K-3K freq range
    abs_fft_x = numpy.power(abs_fft_x, 1.2); #abs_fft_x = abs_fft_x.^1.2;
    #numpy.savetxt('abs_fft_x_afterPower.txt', abs_fft_x, fmt = '%.6f')
    second_dim = abs_fft_x.shape[1]
    #print("Size of abs_fft_x = " + str(second_dim))
    L = math.floor(frSize/2);
    binWidth = (Fs/(L*2));
    PitchRangeLower = 80;
    PitchRangeUpper = 250;
    
    PitchIdxRange = int(math.ceil(PitchRangeUpper - PitchRangeLower)/binWidth)
    val_histPeak = numpy.zeros((second_dim, PitchIdxRange));
    val_histTrough = numpy.zeros((second_dim, PitchIdxRange));
    SumPeakVal = numpy.zeros((second_dim, PitchIdxRange, 6))
    SumTroughVal = numpy.zeros((second_dim, PitchIdxRange, 6))
    #print("Size of SumPeakVal = " + str(SumPeakVal.shape))
    beamWidth =2;
    
    for i in range(PitchIdxRange):
        pitchVal  = math.floor( ((PitchRangeLower/binWidth) +i)*binWidth);
    
        # number of harmonics, skipping the first harmonics bcos very noisy!!!
        for j in range(1,7):
            idx          = int(math.floor(((j +1 )*pitchVal)/binWidth)) ### PLEASE CHECK
            idx_trough   = int(math.floor(idx + (pitchVal/(2*binWidth))))
        
            #Pi  = sum(abs_fft_x(idx-beamWidth:idx+beamWidth,:));
            tmpMat_for_Pi = abs_fft_x[idx-beamWidth-1:idx+beamWidth,:]
            Pi  = tmpMat_for_Pi.sum(axis = 0);
            
            #Ti =  sum(abs_fft_x(idx_trough-beamWidth:idx_trough+beamWidth,:));
            tmpMat_for_Ti = abs_fft_x[idx_trough-beamWidth-1:idx_trough+beamWidth,:]
            Ti =  tmpMat_for_Ti.sum(axis = 0);
            #print("Size of PI" + str(Pi.shape) + " size Ti = " + str(Ti.shape))                                   
            SumPeakVal[:,i,j-1]   =  Pi;
            SumTroughVal[:,i,j-1] = Ti;
    
    
    SP = SumPeakVal.sum(axis = 2);
    ST = SumTroughVal.sum(axis = 2);
    val_histTrough  = numpy.power(ST, 2) #ST.^2;
    #numpy.savetxt('val_histTrough.txt', val_histTrough, fmt = '%.6f')
    
    for i in range(PitchIdxRange):
        #currSumPeakVal = squeeze(SumPeakVal(:,i,:))';
        SumPeakVal_ith = SumPeakVal[:,i,:]
        SumPeakVal_ith_squeezed = SumPeakVal_ith.squeeze();
        ##currSumPeakVal = SumPeakVal_ith_squeezed.conj().transpose()
        currSumPeakVal = SumPeakVal_ith_squeezed.transpose()
        #if i == 0:
        #    numpy.savetxt('currSumPeakVal_i0.txt', currSumPeakVal, fmt = '%.6f')
        
        #currSumTroughVal = squeeze(SumTroughVal(:,i,:))';
        SumTroughVal_ith = SumTroughVal[:,i,:]
        SumTroughVal_ith_squeezed = SumTroughVal_ith.squeeze();
        ##currSumTroughVal = SumTroughVal_ith_squeezed.conj().transpose()
        currSumTroughVal = SumTroughVal_ith_squeezed.conj().transpose()
        #if i == 0:
        #    numpy.savetxt('currSumTroughVal_i0.txt', currSumTroughVal, fmt = '%.6f')
        #val_histPeak(:,i)   = SP(:,i).^2 - var(currSumPeakVal)' - var(currSumTroughVal)';
        var_currSumPeakVal = numpy.var(currSumPeakVal, axis = 0, ddof = 1)
        var_currSumTroughVal = numpy.var(currSumTroughVal, axis = 0, ddof = 1)
        #if i == 0:
        #    numpy.savetxt('var_currSumPeakVal_i0.txt', var_currSumPeakVal, fmt = '%.6f')
        #    numpy.savetxt('var_currSumTroughVal_i0.txt', var_currSumTroughVal, fmt = '%.6f')
        #val_histPeak[:,i]   = numpy.power(SP[:,i], 2) - var_currSumPeakVal.transpose() - var_currSumTroughVal.transpose();
        tmp1 = numpy.power(SP[:,i], 2)
        tmp2 = var_currSumPeakVal.transpose()
        tmp3 = var_currSumTroughVal.transpose()
        tmp4 = tmp1 - tmp2 - tmp3;
        val_histPeak[:,i] = tmp4
        #if i == 0:
        #    numpy.savetxt('tmp1_i0.txt', tmp1, fmt = '%.6f')
        #    numpy.savetxt('tmp2_i0.txt', tmp2, fmt = '%.6f')
        #    numpy.savetxt('tmp3_i0.txt', tmp3, fmt = '%.6f')
        #    numpy.savetxt('tmp4_i0.txt', tmp4, fmt = '%.6f')
    
    #numpy.savetxt('val_histPeak.txt', val_histPeak, fmt = '%.6f')
    #[V1 iV1] = max(val_histPeak');
    val_histPeak_conjTransp = val_histPeak.conj().transpose()
    V1 = val_histPeak_conjTransp.max(0);
    iV1 = val_histPeak_conjTransp.argmax(0)
    V2 = numpy.zeros(nFr)                                     
    for i in range(nFr):
        V2[i] = abs(val_histTrough[i,iV1[i]]);
    #numpy.savetxt('V1.txt', V1, fmt = '%.6f') 
    #numpy.savetxt('V2.txt', V2, fmt = '%.6f') 
    valFeature      = numpy.divide(V1, V2);
    return valFeature


def energyVADVal(frame_x, Fs):
    return numpy.linalg.norm(frame_x)
    

def adaptThreshold(s_old, val):
    if s_old.frameIdx <= 1 :
        s_old.val = val

    #diffVal      = abs(val-s_old.val)
    s_new = MyClass()
    s_new     = s_old

    s_new.frameIdx = s_new.frameIdx+1


    if s_new.frameIdx < 3:
        s_new.vadFlag  = 0;
        s_new.countVADON = 0;
        if s_new.frameIdx == 1:
            s_new.nf = s_new.val;
            s_new.slow_nf = s_new.val;
        s_new.nf          = max(s_new.nf, s_new.val);
        s_new.slow_nf     = max(s_new.slow_nf, s_new.val);
        s_new.speechVar  =  s_new.MinRatioSpeechPeakNoiseFloor*s_new.nf + s_new.ThOffset;
        s_new.th           =  s_new.slow_nf     + s_new.ThOffset;
        s_new.th2          =  s_new.slow_nf*1.3 + s_new.ThOffset;


    s_new.val = val;

    if (s_new.frameIdx >= 3):

        s_new.speechVar = 0.8*s_new.speechVar + 0.2*s_new.val;
        if (s_new.speechVar < s_new.th2):
            s_new.speechVar = s_new.th2;
      
        if (s_new.val > s_new.th2):
            s_new.vadFlag = 1;       
            if (s_new.val > s_new.speechVar):
                s_new.speechVar = s_new.val;
        else:
            s_new.vadFlag = 0;        

        s_new.nf = 0.99*s_new.nf + 0.01*s_new.val;    
        s_new.slow_nf = 0.999*s_new.slow_nf + 0.001*s_new.val;    
        if (s_new.val < s_new.slow_nf):
            s_new.slow_nf = s_new.val; 
            if (s_new.slow_nf < s_new.MinFeatureVal):
                s_new.slow_nf  = s_new.MinFeatureVal;
  
        if (s_new.val < s_new.nf):
            s_new.nf = s_new.val;
            if (s_new.nf < s_new.MinFeatureVal):
                s_new.nf = s_new.MinFeatureVal;

  
        s_new.th         =   s_new.nf     + s_new.ThOffset;
        s_new.th2        =   s_new.nf*1.3 + s_new.ThOffset;

    return s_new
 


def CombVAD(wav, fs, DEBUG):

    max_nSampleTotal = len(wav);
    FrameSzTime     = 0.1 # ?? msec frame Window
    # the current wisdom is to use bin Bandwidth of 10 Hz for FFT resolution
    FrameForward    = 0.02 #  % ?? No overlap in this example

    nSamplePerFrame = int(FrameSzTime * fs)
    nSampleForward  = int(FrameForward * fs)

    numFrame  = int(math.floor((max_nSampleTotal - nSamplePerFrame)*1.0/nSampleForward))
    s_new = MyClass()
    s_new.frameIdx = 0;
    s_new.MinFeatureVal = 15;
    s_new.MinRatioSpeechPeakNoiseFloor = 2.5;
    s_new.ThOffset  = 10;

    periodicVADVal = batch_peridoc_pitch_count_fast(wav, fs, numFrame, nSamplePerFrame, nSampleForward);
    
    x_start   = 0;
    x_end     = nSamplePerFrame;

    energyVAD_flag = numpy.zeros(numFrame)
    vad_flag = numpy.zeros(numFrame)
    for j in range(numFrame):
        frame_x  =  wav[x_start:x_end];
        energyVAD_flag[j]    = energyVADVal(frame_x, fs);
    
        s_new = adaptThreshold(s_new,periodicVADVal[j]);
    
        #Test.th(j)       = s_new.th;
        #Test.th2(j)      = s_new.th2;
        #Test.speecVar(j) = s_new.speechVar;
        #Test.nf(j)       = s_new.nf;
        #Test.slow_nf(j)  = s_new.slow_nf;
        vad_flag[j]  = s_new.vadFlag;
    
        x_start = x_start + nSampleForward;
        x_end   = x_end   + nSampleForward;

    return vad_flag, energyVAD_flag, periodicVADVal

def MedianFilter(x,filter_len):
    nf = len(x);
    hs = (filter_len-1)/2
    y=deepcopy(x);
    for i in range(hs, nf-hs):
        y[i] = numpy.median(x[i-hs:i+hs+1]);
    return y

def printChannelsInfor(x):
    start = 0
    isSpeech = 0
    for i in range(len(x)):
        if x[i] > 0:
            if isSpeech == 0:
                isSpeech = 1
                start = i
        else:
            if isSpeech == 1:
                print("Start = " + str(start) + ", stop = " + str(i))
                isSpeech = 0


def PostProcessVAD(vad, buffer_len, filter_len = 40):
    vad = vad[:];
    
    # First filter the vad flat using a median filter
    vad_smoothed = MedianFilter(vad, 3);
    # Then grow any valid speech frame by 20 frames, as we think
    # anything immediately before and after the voiced frames are
    # speech.
    
    #if nargin<3
    #    filter_len = 40;
    #end
    bool_arr = numpy.greater(numpy.convolve(numpy.ones(filter_len), vad_smoothed), 0.5)
    vad_extended = bool_arr.astype(numpy.double)
    vad_extended = vad_extended[filter_len/2:]#vad_extended(1:filter_len/2) = [];
    vad_extended = vad_extended[:len(vad_extended)-filter_len/2+1]#vad_extended(end-filter_len/2+2:end) = [];
    
    # We don't want to have too many segments. Sometimes, we can merge
    # several segments into one.
    filter_len = buffer_len-40;
    if filter_len>0:
        bool_arr2 = numpy.greater(numpy.convolve(numpy.ones(filter_len), vad_extended), 0.5)
        vad_merged = bool_arr2.astype(numpy.double)
        vad_merged = vad_merged[filter_len/2:]#vad_merged(1:filter_len/2) = [];
        vad_merged = vad_merged[:len(vad_merged)-filter_len/2+1]#vad_merged(end-filter_len/2+2:end) = [];
    else:
        vad_merged = vad_extended;
      
    return vad_merged


def label2seg(label):
    # If the state is changed
    diff = label[1:] - label[0:len(label)-1];
    idx_tuple = numpy.nonzero(diff);
    idx = idx_tuple[0]
    N_seg = len(idx);
    #print ("Number of segment = " + str(N_seg))
    seg = Seg() 
    if N_seg ==0:
        seg.start.append(0);
        seg.stop.append(len(label)-1);
        seg.label.append(label[0])
        return seg  
    for i in range(N_seg):
        if i==0 :
            seg.start.append(0);
        else:
            seg.start.append(idx[i-1]+1)
        seg.stop.append(idx[i])
        seg.label.append(label[idx[i]])
    seg.start.append(idx[-1]+1)
    seg.stop.append(len(label)-1);
    seg.label.append(label[-1])
    
    return seg

def seg2label(seg, numFrames):
    label = numpy.zeros(numFrames, dtype=numpy.int)
    N_seg = len(seg.label);
    #print("Now we have " + str(N_seg))
    for i in range(N_seg):
        label[seg.start[i] : seg.stop[i] +1 ] = seg.label[i];
        #print("Set from " + str(seg.start[i]) + ", " + str(seg.stop[i]) + ", " + str(seg.label[i]))
    return label

def genFinalVAD(audio, fs, postfix, threshold):
    numSample = len(audio)
    #print ("check again numsample = " + str(numSample))
    hsize = int(fs * 0.01);
    flength = int(fs * 0.025);
    sample_based_vad = numpy.zeros(numSample, dtype = numpy.double)
    powerSum = calPowerSum_segment(audio, fs)  
    #numpy.savetxt('powersum.txt', powerSum, fmt = '%.6f')
    numOfFrame = len(powerSum)            
    print("Number of frame this segment = " + str(numOfFrame))    

    
    vad_indi = numpy.zeros(len(powerSum), dtype = numpy.int)
    vad_indi_smooth = numpy.zeros(len(powerSum), dtype = numpy.int)
    for j in range(len(powerSum)):
        if powerSum[j] > threshold:
            vad_indi[j] = 1
    #numpy.savetxt('vad_energy_threshold_' + str(threshold) + '_raw_' + postfix + '.txt', vad_indi, fmt = '%d')
    vad_indi_smooth = MedianFilter(vad_indi, 51)
    #numpy.savetxt('vad_energy_threshold_' + str(threshold) + '_smooth51_' + postfix + '.txt', vad_indi_smooth, fmt = '%d')
    
    print("Finish processing segment " + postfix + " ------")
    count = 0;

    f = open('segments_' + postfix, 'w')
    spkID_seg = label2seg(vad_indi_smooth) # An struct Segment{int start[]; int end[]; int channelId[]}
    for i in range(len(spkID_seg.label)):
        curr_start = spkID_seg.start[i];
        curr_start = curr_start * 0.01;
        curr_stop = spkID_seg.stop[i];
        curr_stop = curr_stop * 0.01;
        curr_spk = spkID_seg.label[i];
        f.write(str(curr_start) + " " + str(curr_stop) + " " + str(curr_spk) + "\n")  # python will convert \n to os.linesep
    f.close()  # you can omit in most cases as the destructor will call it
    return vad_indi_smooth

def modifySignal(tmp_audio, spkID_smooth_refined, hsize):
    print(" Modifying the signal ")
    audio = deepcopy(tmp_audio)
    numSample = len(tmp_audio[:])
    print("Number of sample = " + str(numSample))
    for j in range(len(spkID_smooth_refined)):
        activeChannel = spkID_smooth_refined[j] 
        start_frame = j * hsize
        end_frame = min(start_frame + flength, numSample)
        
        if activeChannel == 0:
            for k in range (start_frame,end_frame):
                if abs(audio[k]) == 0:
                    continue
                if (j > 0):
                	prev = spkID_smooth_refined[j-1]
                	if prev == 1 :
                		audio[k] = (random.random()*40-20)/32767;
                	else:
                		audio[k] = (random.random()*20-10)/32767;
                if (j < len(spkID_smooth_refined)-1):
                	next = spkID_smooth_refined[j+1]
                	if next == 1:
                		audio[k] = (random.random()*40-20)/32767;
                	else:
                		audio[k] = (random.random()*20-10)/32767;
    return audio

def combineSegment(name, numSegs, minSilence, lengthPerSeg, numFile, avgLength):
    bestEnds = numpy.zeros(numFile - 1, dtype = numpy.double)
    minDist = []
    for i in range (numFile-1):
        minDist.append(9999)
    
    for i in range(numSegs):
        with open('segments_seg' + str(i+1)) as f:
            content = f.readlines()
        # you may also want to remove whitespace characters like `\n` at the end of each line
        content = [x.strip() for x in content] 
        for j in range(len(content)):
            aSeg = content[j].split(' ')
            start = float(aSeg[0]) + lengthPerSeg * i
            end = float(aSeg[1]) + lengthPerSeg * i
            isspeech = aSeg[2]
            if isspeech == '0': #Only care if aSeg is a silence segment
                if end - start > minSilence:
                    print("Detect big silence at [" + str(start) + ", " + str(end) + "]")
                    midPoint = (start + end)/2;
                    for idx in range(numFile-1):
                        if abs(midPoint - avgLength * (idx+1)) < minDist[idx]:
                            minDist[idx] = abs(midPoint - avgLength * (idx+1))
                            bestEnds[idx] = midPoint
    for i in range(numFile-1):
        print("End of segment " + str(i+1) + ":" + str(bestEnds[i]))
    return bestEnds                    
############# Main #####################

print("=========== Segmentatation audio file into smaller parts for parallel DTW =============")
if len(sys.argv) != 1:
	print ("use: python my_utils_test.py input_wav")
	sys.exit()
closedTalkChans = sys.argv[1] # Input file i.e. 'testAudio4_16k.wav'


print("=== Input = " + closedTalkChans)
print("=== Threshold = " + str(threshold))
print("=== Length of each trunk (in seconds) = " + str(Seg_seconds))
print("=== minSilence = " + str(minSilence))
print("=== numFile = " + str(numFile))

name = os.path.splitext(closedTalkChans)[0]
dirname = os.path.dirname(name)
print ("Name = " + name + ", dir name = " + dirname)
nframeEachSeg = int(Seg_seconds * 100) #Number of frame

audio, fs = sf.read(closedTalkChans)
numSample = len(audio)
audioLength = numSample * 1.0/fs
avgLength = audioLength/numFile
if audioLength <= 120:
    print ("Audio is already short! Not need to segment it")
    sys.exit()
print ("Number of sample = " + str(numSample) + ", length in second = " + str(audioLength) + ", avgLength for each file = " + str(avgLength))


flength = int(fs * 0.025);
hsize = int(fs * 0.01);
               
numSampleEachSeg = (nframeEachSeg - 1) * hsize + flength
print ("Number of sample each segment = " + str(numSampleEachSeg))                   
sampleStartFrame = 0
sampleEndFrame = min(numSample, sampleStartFrame + numSampleEachSeg)


segCount = 1
while sampleStartFrame < numSample:
    print("******** Segment " + str(segCount) + ", Start sample = " + str(sampleStartFrame) + ", end sample = " + str(sampleEndFrame) + " *********")    
    curr_seg_audio = audio[sampleStartFrame:sampleEndFrame]
    spkID_smooth_refined = genFinalVAD(curr_seg_audio, fs, 'seg' + str(segCount), threshold)
    segCount = segCount + 1
    #audio[sampleStartFrame:sampleEndFrame] = modifySignal(audio[sampleStartFrame:sampleEndFrame], spkID_smooth_refined, hsize )   
    sampleStartFrame = sampleStartFrame + numSampleEachSeg
    sampleEndFrame = min(numSample, sampleEndFrame + numSampleEachSeg)

#sf.write(closedTalkChans_mod, audio[:], fs)
print ("Number of segment = " + str(segCount))
bestEnd = combineSegment(name, segCount - 1, minSilence, nframeEachSeg * 0.01, numFile, avgLength)

currStart = 0
currEnd = numSample
count = 0

ft = open(dirname + '/timeInfor.txt', 'a')

for i in range (numFile):
    if i > 0:
        currStart = bestEnd[i - 1]
    if i < numFile - 1:
        currEnd = bestEnd[i]
    else: 
        currEnd = audioLength   
    start_samp = int(currStart * fs)
    end_samp = int(min(currEnd * fs, numSample))
    if i == numFile - 1:
        end_samp = numSample
    print("Part " + str(i + 1) + ", start = " + str(currStart) + ", end = " + str(currEnd) + ", start sample = " + str(start_samp) + ", end sample = " + str(end_samp))
    if (end_samp - start_samp) < 0.01:
        continue
    basenameFile = basename(name + '-part' + str(count+1) + '.wav')
    basenameFile = os.path.splitext(basenameFile)[0]
    ft.write(basenameFile + " " + str(currStart) + "\n")
    sf.write(name + '-part' + str(count+1) + '.wav', audio[start_samp:end_samp], fs)
    count = count + 1
ft.close()
