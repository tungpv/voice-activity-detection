from __future__ import print_function
import kaldi_io_vesis84
import numpy
import os
import sys

file_ark = sys.argv[1]
numChan = int(sys.argv[2])
numSeg = int(sys.argv[3])
outdir = sys.argv[4]

dict_dat = {}
for key,mat in  kaldi_io_vesis84.read_mat_ark(file_ark):
    dict_dat[key] = mat

for i in range(1, numChan + 1):
    dat = numpy.empty((0,0), dtype=numpy.double)
    for j in range(1, numSeg + 1):
        key = "chan" + str(i) + "_seg" + str(j)
        mat = dict_dat[key]
        numpy.savetxt(outdir + "/dat_chan" + str(i) + "_seg" + str(j) + "_kaldi_mfcc_pitch.txt", mat, fmt="%.6f")
    
