from __future__ import print_function
import kaldi_io
import numpy
import os
import sys

file_str = sys.argv[1]
column_str = sys.argv[2]
ark_out = sys.argv[3]
key="unknown"
if len(sys.argv) > 4:
	key = sys.argv[4]

columns = column_str.split(",")
feats = map(int, columns)
for i in range(len(feats)):
    feats[i] = feats[i] - 1
mat = numpy.loadtxt(file_str)
mat = mat[:, feats]
with open(ark_out,'wb') as f:
	kaldi_io.write_mat(f, mat, key=key)
	
