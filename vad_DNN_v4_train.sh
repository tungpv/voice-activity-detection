#!/bin/bash
. path.sh
#Train the DNN

#### Setting
cmd='slurm.pl --quiet --gres=gpu:1'


#################### Input data ###################################################
#--- Audiofiles_arr: List of all recordings. Each recoding includes multiple channels, separated by the semicolon.
#--- vadLabels_arr: List of corresponding vad labels. Note that each vad file (for each channel) has 2-columns format <start> <end> in seconds.
#--- recName_arr: List of corresponding recording name. Different recording must have different names
declare -a audioFiles_arr=("train_data/Data_110659_m2/MNT1.WAV;train_data/Data_110659_m2/MNT2.WAV;train_data/Data_110659_m2/MNT3.WAV" "train_data/mml-12-sept-2017/170912_094129_1_16k.wav;train_data/mml-12-sept-2017/170912_094129_2_16k.wav" "train_data/mml-15-sep-11m/new_vad/mml-15-sept-2017-1_16k_11m23s.wav;train_data/mml-15-sep-11m/new_vad/mml-15-sept-2017-2_16k_11m23s.wav" "train_data/mml-07-b-nov-session1/mml-7-nov-2017-b-session1-1-12m13s.wav;train_data/mml-07-b-nov-session1/mml-7-nov-2017-b-session1-2-12m13s.wav");
declare -a vadLabels_arr=("train_data/Data_110659_m2/segment_r110659M2_chan1.txt;train_data/Data_110659_m2/segment_r110659M2_chan2.txt;train_data/Data_110659_m2/segment_r110659M2_chan3.txt" "train_data/mml-12-sept-2017/170912_094129_1_16k.txt;train_data/mml-12-sept-2017/170912_094129_2_16k.txt" "train_data/mml-15-sep-11m/new_vad/mml-15-sept-2017-1.txt;train_data/mml-15-sep-11m/new_vad/mml-15-sept-2017-2.txt" "train_data/mml-07-b-nov-session1/mml-7-nov-2017-b-session1-1.txt;train_data/mml-07-b-nov-session1/mml-7-nov-2017-b-session1-2.txt");
declare -a recName_arr=("r110659M2" "r12SepSess1" "r15Sep11min" "r07NovBSess112m");


############################# Step 1: Extracting features for all recording #########################

mkdir -p intermediate
for (( idx=3; idx<${#audioFiles_arr[@]}; idx=idx+1 ))
do
	audioFiles=${audioFiles_arr[$idx]} 
	recName=${recName_arr[$idx]} 
	echo "Process recording $recName ....."
	datadir=data_Tung/$recName
	mkdir -p $datadir

	./process_one_recording.sh $audioFiles $datadir $recName

	vadLabels=${vadLabels_arr[$idx]} 
	if [ 0 -eq 0 ]; then
  		feat-to-len scp:data/${recName}_addFeats/feats.scp ark,t:intermediate/feats_length_${recName}.txt
  		python create_label.py $recName $vadLabels intermediate/feats_length_${recName}.txt > intermediate/label_$recName.txt
	fi
done


################# Step 2: Combine data (feature, label,...) from all recordings #######
all_data=data/all_dat
mkdir -p $all_data

files=""
labels=""
for (( i = 0 ; i < ${#recName_arr[@]} ; i++ )) 
do
	recName=${recName_arr[$i]}
	files="$files data/${recName}_addFeats"
	labels="$labels intermediate/label_${recName}.txt"
done

echo "utils/combine_data.sh $all_data $files"
utils/combine_data.sh $all_data $files

mkdir -p $all_data/label

echo "cat $labels | ali-to-post ark,t:- ark:$all_data/label/post.ark"
cat $labels | ali-to-post ark,t:- ark:$all_data/label/post.ark

########## Step 3: Split data into train_dnn and validate_dnn to train the DNN ####
valid=validate_dnn
train=train_dnn


recns=""
datTung=""
for (( i = 0 ; i < ${#recName_arr[@]} ; i++ )) 
do
	recName=${recName_arr[$i]}
	recns="$recns;$recName"
	datTung="$datTung;data_Tung/${recName}"
done
recns=${recns:1:${#recns}}
datTung=${datTung:1:${#datTung}}

if [ 0 -eq 0 ]; then
  valid_utt=$all_data/utt_list_dev.txt
  train_utt=$all_data/utt_list_train.txt
  python create_train_dev_list.py $datTung $recns $train_utt $valid_utt
  utils/subset_data_dir.sh --utt-list $valid_utt $all_data $valid
  utils/subset_data_dir.sh --utt-list $train_utt $all_data $train
fi

########## Step 4:Train the DNN ####
# Note that different running might produce different result
srcdir=dnn
train_tool_opts="--minibatch-size=256 --randomizer-size=32768 --randomizer-seed=777"

if [ 0 -eq 0 ]; then
  echo "## LOG (step05): training nnet started @ `date`"
  $cmd $srcdir/dnn.log \
  steps/nnet/train.sh --learn-rate 0.008 \
  --train-tool-opts "$train_tool_opts" \
  --skip-lang-check true \
  --delta-opts "--delta-order=2" \
  --scheduler-opts "--max-iters 30" \
  --feat-type traps \
  --hid-layers 2 --hid-dim 1024 \
  --splice 5 \
  --num-tgt 2 \
  --labels ark:$all_data/label/post.ark \
  $train $valid dummy dummy dummy $srcdir 
fi


