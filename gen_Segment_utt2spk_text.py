import os
import sys
import time
import timeit

import numpy as np
from six.moves import xrange  # pylint: disable=redefined-builtin

def readDurationFile(durFile, segment_out, utt2spk_out, text_out):
	fw1 = open(segment_out,"w")
	fw2 = open(utt2spk_out,"w")
	fw3 = open(text_out,"w")
  	with open(durFile) as source_file:
  		line = source_file.readline()
  		while line:
  			content = line.strip().split()
  			spk = content[0].split('_')[0]
  			aud_length = str(content[1])
  			start = 0
  			end = start + 200
  			while start < aud_length:
  				if end > aud_length:
  					end = aud_length
  				fw1.write(content[0] + "_" + str(start) + "_" + str(end) + " " + content[0] + " " + str(start) + " " + str(end) + "\n")
  				fw2.write(content[0] + "_" + str(start) + "_" + str(end) + " " + spk + "\n")
  				fw3.write(content[0] + "_" + str(start) + "_" + str(end) + " text\n")
  				print(content[0] + "_" + str(start) + "_" + str(end) + " " + content[0] + " " + str(start) + " " + str(end) + "\n")
  				start = end
  				end = start + 200
  			line = source_file.readline()
  	fw1.close() 
  	fw2.close()
  	fw3.close()

durFile = sys.argv[1]
segment_out = sys.argv[2] 
utt2spk_out = sys.argv[3]
text_out = sys.argv[4]
readDurationFile(durFile, segment_out, utt2spk_out, text_out)



