'''
A Multilayer Perceptron implementation example using TensorFlow library.
'''

from __future__ import print_function

# Import MNIST data

import tensorflow as tf
import sys
import numpy
from sklearn import preprocessing
from sklearn.externals import joblib

def getData(dat_file):
    dat = numpy.loadtxt(dat_file, dtype=numpy.float)
    print("Number of columns = " + str(dat.shape[1]))
    numFeats = dat.shape[1] - 1
    dat_feats = dat[:, 1:]
    dat_label = dat[:, 0]
    dat_label.astype(int)
    print ("First 5 data items:")
    for i in range(5):
        print("Feats = " + str(dat_feats[i]) + ", label = " + str(dat_label[i]) + "\n")
    return dat_feats, dat_label, numFeats


hiddenSize = 200
trainFile = sys.argv[1]
modelDir = sys.argv[2]
scalerModelOut = sys.argv[3] # e.g. scaler.mdl
if len(sys.argv) > 4:
    hiddenSize = int(sys.argv[4])

train_feats, train_label, numFeats = getData(trainFile)
    
scaler = preprocessing.StandardScaler().fit(train_feats)
joblib.dump(scaler, scalerModelOut)     
train_feats_scaled = scaler.transform(train_feats)


feature_columns = [tf.contrib.layers.real_valued_column("", dimension=numFeats)]
classifier = tf.contrib.learn.DNNClassifier(feature_columns=feature_columns,
                                          hidden_units=[hiddenSize],
                                          optimizer=tf.train.ProximalAdagradOptimizer(
                                          	learning_rate=0.01,
                                          	l2_regularization_strength=0.00003
                                          ),
                                          n_classes=2,
                                          model_dir=modelDir)

# Train model.
classifier.fit(x=train_feats_scaled,
               y=train_label,
               batch_size=256,
               steps=5000)
