from __future__ import print_function
import kaldi_io_vesis84
import numpy
import os
import sys

labelList = sys.argv[1]
recIdList = sys.argv[2]
trainFile = sys.argv[3]
devFile = sys.argv[4]
labels = labelList.split(";")
recIds = recIdList.split(";")
wf = open(trainFile, "w")
wf2 = open(devFile, "w")
for i in range(len(labels)):
    recId = recIds[i]
    numChan = 0
    numSeg = 0
    with open(labels[i] + "/numChan") as infile:
        for line in infile:
            numChan = int(line.strip())
    with open(labels[i] + "/numSeg") as infile: 
        for line in infile:
            numSeg = int(line.strip())
    numSeg_dev = numSeg/5
    if numSeg_dev == 0 and numSeg > 2:
        numSeg_dev = 1
    print ("Number of chan = " + str(numChan) + ", number of seg = " + str(numSeg) + ", select dev = " + str(numSeg_dev))
    for i in range(1, numChan + 1):
        for j in range(1, numSeg_dev + 1):
            wf2.write(recId + "_chan" + str(i) + "_seg" + str(j) + "\n")
        for j in range(numSeg_dev + 1, numSeg + 1):
            wf.write(recId + "_chan" + str(i) + "_seg" + str(j) + "\n")
wf.close()
wf2.close()
    
