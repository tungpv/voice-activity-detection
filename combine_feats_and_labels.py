from __future__ import print_function
import kaldi_io
import numpy
import os
import sys

file_org_feats_with_label_str = sys.argv[1]
spliced_feats_str = sys.argv[2]
dat_out = sys.argv[3]

org_feats_with_labels = numpy.loadtxt(file_org_feats_with_label_str)
labels = org_feats_with_labels[:, 0]
spliced_feats = numpy.loadtxt(spliced_feats_str)
if len(labels) != len(spliced_feats):
	print ("Mismatch size. Label size = " + str(len(labels)) + " vs. feats size = " + str(len(spliced_feats)))
	sys.exit()
f = open(dat_out,'w')
for i in range(len(labels)):
	feats = " ".join(str(e) for e in spliced_feats[i, :]) # " ".join(spliced_feats[i, :])
	if labels[i] > 0.9999:
		labels[i] = 1	
	feats_with_label = str(int(labels[i])) + " " + feats
	f.write(feats_with_label + "\n")
f.close()
	
