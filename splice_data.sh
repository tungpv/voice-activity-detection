#!/bin/bash
. path.sh
dat=$1 # e.g. dat_allChan_Data_110659_m2.txt
feat_cols=$2 # e.g. "2,3,4,5,6,7,8" 
out_txt_spliced_with_labels=$3 # e.g. dat_allChan_Data_110659_m2_spliced_left7_right7_labels.txt
left_context=$4 # e.g. 7
right_context=$5 # e.g. 7

filename=$(basename "$dat")
extension="${filename##*.}"
key="${filename%.*}"

echo "Key = $key"



mkdir -p tmp

out_ark=tmp/${key}.ark
out_ark_spliced=tmp/${key}_spliced_left${left_context}_right${right_context}.ark
out_txt_spliced=tmp/${key}_spliced_left${left_context}_right${right_context}.txt

python convert_to_kaldi_v2.py $dat $feat_cols $out_ark $key
splice-feats --left-context=$left_context --right-context=$right_context ark:$out_ark ark:$out_ark_spliced
python convert_kaldi_ark_to_txt.py $out_ark_spliced $out_txt_spliced
python combine_feats_and_labels.py $dat $out_txt_spliced $out_txt_spliced_with_labels

#dat=dat_allChan_ProfChngAndrew.txt
#feat_cols="2,3,4,5,6,7,8" 
#out_ark=dat_allChan_ProfChngAndrew.ark
#key=dat_allChan_ProfChngAndrew
#python convert_to_kaldi_ark.py $dat $feat_cols $out_ark $key

