#include "base/kaldi-common.h"
#include "base/io-funcs.h"
#include "util/common-utils.h"
#include "hmm/posterior.h"
#include "nnet/nnet-utils.h"
namespace kaldi {
namespace nnet1{
class IndexToLabel {
 public:
  IndexToLabel() { }
  ~IndexToLabel() {}
  void Init(const std::string &str2int_rspecifier) {
    SequentialInt32Reader spk2index_reader(str2int_rspecifier);
    for(; !spk2index_reader.Done(); spk2index_reader.Next()) {
      std::string key = spk2index_reader.Key();
      int32 value = spk2index_reader.Value();
      it_ = map_.find(value); it_end_ = map_.end();
      if(it_ != it_end_) {
        KALDI_ERR << "duplicated key " << "'" << key << "'"
                  << " in file " << str2int_rspecifier;
      }
      map_.insert(std::pair<int32, std::string>(value, key));
    }
  }
  bool HasKey(const int32 value) {
    it_ = map_.find(value), it_end_ = map_.end();
    if(it_ == it_end_)
      return false;
    return true;
  }
  const std::string Value() {
    return it_->second;
  }
 private:
  std::map<int32, std::string> map_;
  std::map<int32, std::string>::iterator it_, it_end_;
};

}  /// namespace nnet
}  /// namespace kaldi
int main(int argc, char *argv[]) {
  using namespace kaldi;
  using namespace kaldi::nnet1;
  typedef kaldi::int32 int32;
  try {
    const char *usage =
    "Map utterance to speaker using accumulated  frame-base speaker posterior label.\n"
    "Usage: map-utterance-to-speaker <speaker-to-index-rspecifier> <utt2utt-rspecifier>  <feature-rspecifier> <score-file> \n"
    "utt2utt-rspecifier is the same with the kaldi spk2utt format, and score-file has the format like:\n"
    "utt speaker score\n"
    "..."
    "e.g.:\n"
    "map-utterance-to-speaker ark:spk2index ark:utt2utt ark:- score.txt\n";
    ParseOptions po(usage);
    po.Read(argc, argv);
    if(po.NumArgs() != 4) {
      po.PrintUsage();
      exit(1);
    }
    IndexToLabel index2label;
    index2label.Init(po.GetArg(1));
    std::string utt2utt_rspecifier = po.GetArg(2),
                feat_rspecifier = po.GetArg(3),
                score_filename  = po.GetArg(4);
    SequentialTokenVectorReader utt2utt_reader(utt2utt_rspecifier);
    RandomAccessBaseFloatMatrixReader feat_reader(feat_rspecifier);
    int32 num_of_total = 0, num_of_error = 0;
    bool binary = false;
    Output output(score_filename, binary);
    std::ostream &output_file = output.Stream();
    for(; !utt2utt_reader.Done(); utt2utt_reader.Next()) {
      std::string long_utt = utt2utt_reader.Key();
      std::vector<std::string> vec_short_utt = utt2utt_reader.Value();
      int32 num_row = 0; 
      Vector<BaseFloat> vec_row_sum;  
      for(int32 i = 0; i < vec_short_utt.size(); ++i) {
        std::string short_utt = vec_short_utt[i];
        if(!feat_reader.HasKey(short_utt)) {
          KALDI_WARN << "No feature for short utterance " << "'" << short_utt << "'";
          continue;
        }
        Matrix<BaseFloat> post_feat = feat_reader.Value(short_utt);
        if(num_row == 0) {
          vec_row_sum.Resize(post_feat.NumCols());
          vec_row_sum.AddRowSumMat(1.0, post_feat, 0);
        } else {
          vec_row_sum.AddRowSumMat(1.0, post_feat, 1.0);
        }
        num_row += post_feat.NumRows();
      }
      if(num_row == 0) {
        num_of_error ++;
        KALDI_WARN << "No feature for long utterance " << "'" << long_utt << "'";
        continue;
      }
      int32 index;    
      BaseFloat max_post_acc = vec_row_sum.Max(&index);
      if(!index2label.HasKey(index)) {
        KALDI_ERR << "No speaker label for index " << index;
      }
      std::string spk = index2label.Value();
      BaseFloat aver_post = max_post_acc / num_row;
      output_file << long_utt << " " << spk << " " << aver_post << std::endl;
      num_of_total ++;
    }
    output.Close();
    KALDI_LOG << "Total speaker " << num_of_total << " processed, total errors " << num_of_error;
    return 0;
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
