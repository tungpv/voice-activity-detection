#include "nnet/nnet-trnopts.h"
#include "nnet/nnet-lhuc-sat.h"
#include "nnet/nnet-nnet.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"

int main(int argc, char *argv[]) {
  using namespace kaldi;
  using namespace kaldi::nnet1;
  try { 
    const char *usage = 
      "Make an initial model set with utt2spk.\n"
      "Usage: nnet-lhuc-initialize [options] <utt2spk-rspecifier> <nnet-model> <lhuc-sat-nnet.init>\n"
      "e.g.:\n"
      " nnet-lhuc-initialize ark:utt2spk nnet.init  lhuc-sat-nnet.init\n";
    ParseOptions po(usage);
    NnetTrainOptions trn_opts;
    trn_opts.Register(&po);
    bool binary = true;
    po.Register("binary", &binary, "Write output in binary mode");
    std::string nnet_activation = "<Sigmoid>";
    po.Register("nnet-activation", &nnet_activation, "Nnet activation type");
    float lhuc_const = 2.0;
    po.Register("lhuc-const", &lhuc_const, "LHUC constant scaling factor");
    float lhuc_learn_rate_coef = 1.0;
    po.Register("lhuc-learn-rate-coef", &lhuc_learn_rate_coef, "LHUC learning rate coefficient");
    po.Read(argc, argv);
    if(po.NumArgs() != 3) {
      po.PrintUsage();
      exit(1);
    }
    std::string utt2spk_rspecifier = po.GetArg(1);
    std::string nnet_model_filename = po.GetArg(2);
    std::string lhuc_model_filename = po.GetArg(3);
    Nnet nnet;
    nnet.Read(nnet_model_filename);
    nnet.SetTrainOptions(trn_opts);
    LhucSat lhucSat(lhuc_const, lhuc_learn_rate_coef, nnet_activation, &nnet);
    SequentialTokenReader utt2spk_reader(utt2spk_rspecifier);
    int32 utt_num = 0;
    std::map<std::string, int32> map_unique;
    for(; !utt2spk_reader.Done(); utt2spk_reader.Next()) {
      std::string spk = utt2spk_reader.Value();
      map_unique.insert(std::pair<std::string, int32>(spk, utt_num));
      LhucNnet *lhuc_nnet = lhucSat.InsertLhuc(spk, true);
      KALDI_ASSERT(lhuc_nnet != NULL);
      utt_num ++;
    }
    KALDI_LOG << "Overall, " << utt_num << " utterances and  " 
              << map_unique.size() << " speakers have been read to initialize the model"; 
    lhucSat.Write(lhuc_model_filename, binary);
    return 0;   
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
