#include "base/kaldi-common.h"
#include "base/io-funcs.h"
#include "util/common-utils.h"
#include "hmm/posterior.h"
#include "nnet/nnet-utils.h"

int main(int argc, char *argv[]) {
  using namespace kaldi;
  using namespace kaldi::nnet1;
  typedef kaldi::int32 int32;
  try {
    const char *usage =
        "Make speaker label target to train speaker recognition dnn.\n"
        "Usage: make-speaker-label-target <utt2spk-rspecifier> <spklabel2index-rspecifier> <featlen-rspecifier> <spk-post-wspecifier>\n"
        "e.g.:\n"
        "make-speaker-label-target ark:utt2spk ark:spk2index 'ark:feat-to-len $feats ark,t:-|' ark:spk-post.art\n";
    ParseOptions po(usage);
    po.Read(argc, argv);
    if(po.NumArgs() < 4) {
      po.PrintUsage();
      exit(1);
    }
    std::string utt2spk_rspecifier = po.GetArg(1),
                spkindex_rspecifier = po.GetArg(2),
                featlen_rspecifier = po.GetArg(3),
                spkpost_wspecifier = po.GetArg(4);

    SequentialTokenReader utt2spk_reader(utt2spk_rspecifier);
    RandomAccessInt32Reader spkindex_reader(spkindex_rspecifier);
    RandomAccessInt32Reader featlen_reader(featlen_rspecifier);
    PosteriorWriter posterior_writer(spkpost_wspecifier);
    int32 num_done = 0, num_no_spk = 0, num_no_utt = 0;
    for(; !utt2spk_reader.Done(); utt2spk_reader.Next()) {
      std::string utt = utt2spk_reader.Key();
      std::string spk = utt2spk_reader.Value();
      if(!spkindex_reader.HasKey(spk)) {
        num_no_spk ++;
        num_no_utt ++;
        KALDI_WARN << "no speaker index for  speaker " << "'" 
                   << spk << "'" << " in spkindex file "
                   << "'" << spkindex_rspecifier << "'";
        continue;
      }
      int32 target_index = spkindex_reader.Value(spk);
      if(!featlen_reader.HasKey(utt)) {
        num_no_utt ++;
        KALDI_WARN << "no utterance " << "'"
                   << utt << "'" << " in feat2len file "
                   << featlen_rspecifier;
        continue;
      }
      int32 num_feat = featlen_reader.Value(utt);
      Posterior post;
      for(int32 i = 0; i < num_feat; ++i) {
        std::vector<std::pair<int32, BaseFloat> > vec_post;
        vec_post.push_back(std::make_pair(target_index, 1.0));
        post.push_back(vec_post);
      }
      posterior_writer.Write(utt, post);
      num_done ++;
    }
    KALDI_LOG << "Total utterances " << num_done << " processed, "
              << "missing speakers " << num_no_spk << ", missing utterances "
              << num_no_utt;
    return 0;
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
