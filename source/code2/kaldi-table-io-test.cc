// kaldi-table-io-test.cc

#include <limits>

#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "util/kaldi-table.h"
#include "util/kaldi-holder.h"
#include "util/table-types.h"

#include "nnet/nnet-nnet.h"
#include "nnet/nnet-loss.h"
#include "nnet/nnet-pdf-prior.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "base/timer.h"

namespace kaldi {
class HybridIo {
 public:
  HybridIo() {}
  ~HybridIo(){}
  void Read(const std::string &file) {
    bool binary;
    Input in(file, &binary);
    std::istream &is = in.Stream();
    KALDI_ASSERT(is.good());
    ExpectToken(is, binary, "<LhucConst>");
    ReadBasicType(is, binary, &lhuc_const_);
    ExpectToken(is, binary, "<LearnRateCoef>");
    ReadBasicType(is, binary, &lhuc_learn_rate_coef_);
    ExpectToken(is, binary, "<NetActType>");
    ReadToken(is, binary, &act_type_);
    KALDI_LOG << "act_type = " << act_type_;
    ReadTable(is, binary);
    in.Close();
  }
  void Write(const std::string &file, bool binary) {
    Output output(file, binary);
    std::ostream &os = output.Stream();
    KALDI_ASSERT(os.good());
    WriteToken(os, binary, "<LhucConst>");
    WriteBasicType(os, binary, lhuc_const_);
    WriteToken(os, binary, "<LearnRateCoef>");
    WriteBasicType(os, binary, lhuc_learn_rate_coef_);
    WriteToken(os, binary, "<NetActType>");
    WriteToken(os, binary, act_type_.c_str());
    WriteTable(os, binary);
    output.Close();
  }
 private:
  void FillTable(std::string &utt, Matrix<BaseFloat> &mat) {
    std::map<std::string, Matrix<BaseFloat> >::iterator it = table_.find(utt),
                                                        it_end = table_.end();
    if(it != it_end) {
      KALDI_WARN << "duplicated utt '" << utt << "'";
      return;
    }
    table_.insert(std::pair<std::string, Matrix<BaseFloat> > (utt, mat));
  }
  void ReadTable(std::istream &is, bool binary) {
    int32 num_done = 0;
    while(!is.eof()) {
     is.clear();
     std::string key;
     is >> key;
     if(key.empty()) break;
     Matrix<BaseFloat> mat;
     mat.Read(is, binary);
     FillTable(key, mat);
     num_done ++;
    }
  }
  void WriteTable(std::ostream &os, bool binary) {
    std::map<std::string, Matrix<BaseFloat> >::iterator it = table_.begin(),
                                                        it_end = table_.end();
    for(; it != it_end; ++it) {
      WriteToken(os, binary, it->first.c_str());
      Matrix<BaseFloat> &mat = it->second;
      mat.Write(os, binary);
    }
  }
 private:
  float lhuc_const_; 
  float lhuc_learn_rate_coef_;
  std::string act_type_;
  std::map<std::string, Matrix<BaseFloat> > table_;
};
} // namespace

int main(int argc, char *argv[]) {
  using namespace kaldi;
  try {
    const char *usage = 
               "Test how mixed io in kaldi works\n"
               "Usage: kaldi-io-test [options] <in-file> <out-file>\n"
               "e.g.: \n"
               "kaldi-table-io-test x-in.txt x-out.txt\n";
    ParseOptions po(usage);
    bool binary_out = false;
    po.Register("binary", &binary_out, "output file write mode");
    po.Read(argc, argv);
    if(po.NumArgs() != 2) {
      po.PrintUsage();
      exit(1);
    }
    const std::string file = po.GetArg(1);
    const std::string ofile = po.GetArg(2);
    HybridIo hybrid_itf;
    hybrid_itf.Read(file);
    hybrid_itf.Write(ofile, binary_out);
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
