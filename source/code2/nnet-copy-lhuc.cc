#include "nnet/nnet-lhuc-sat.h"
#include "nnet/nnet-nnet.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"

int main(int argc, char *argv[]) {
  using namespace kaldi;
  using namespace kaldi::nnet1;
  try {
    const char *usage =
      "nnet-copy-lhuc [options] <nnet> <in-model> <out-model>\n"
      "e.g.:\n"
      " nnet-copy-lhuc --binary=false nnet-model in-model out-model\n";
    ParseOptions po(usage);
    bool binary = true;
    po.Register("binary", &binary, "Write output in binary mode");
    po.Read(argc, argv);
    if(po.NumArgs() != 3) {
      po.PrintUsage();
      exit(1);
    }
    std::string nnet_model = po.GetArg(1);
    std::string in_model = po.GetArg(2);
    std::string out_model = po.GetArg(3);
    LhucSat lhucSat;
    Nnet nnet;
    nnet.Read(nnet_model);
    lhucSat.SetNnet(&nnet);
    lhucSat.Read(in_model);
    lhucSat.Write(out_model, binary);
    return 0;
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
