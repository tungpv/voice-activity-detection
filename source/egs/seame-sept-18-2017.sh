#!/bin/bash

. path.sh
. cmd.sh

# begin sub
cmd='slurm.pl --quiet'
nj=120
steps=
numleaves=7000
hidden_dim=1024
train_stage=-10
get_egs_stage=-10
frames_per_eg=150
minibatch_size=128
num_epochs=4
proportional_shrink=0
chainname=chain1024
num_jobs_initial=3
num_jobs_final=16
remove_egs=false
max_param_change=2.0
initial_effective_lrate=0.001
final_effective_lrate=0.0001
xent_regularize=0.1
remove_egs=false
get_egs_stage=-10
common_egs_dir=
# end sub

. parse_options.sh || exit 1

function Example {
 cat<<EOF
 $0 --steps 1 --cmd "$cmd" --nj $nj \
 /home2/hhx502/seame/data/local/dict-sil \
 ../seame-nnet3-feb-06-2017/data/train/mfcc-pitch \
 update-april-03-2017-with-pruned-lexicon/data/dict-sweet-pie \
 /home3/hhx502/w2017/seame-sept-18-2017
  

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 4 ]; then
  Example && exit 1
fi
srcdict=$1
srcdata=$2
bigdict=$3
tgtdir=$4

dictdir=$tgtdir/data/local/dict
[ -d $dictdir ] || mkdir -p $dictdir
if [ ! -z $step01 ]; then
  cp source/egs/sg-en-i2r/sge-phone-to-en-phone-map.txt  $dictdir/
  cat $srcdict/lexicon.txt | \
  source/egs/seame-sept-18-2017/filter-mandarin-word.pl $dictdir/man-lexicon.txt | \
  source/egs/seame-sept-18-2017/add-or-remove-lang-identifier-to-phone.pl --remove-identifier='_sge'| \
  source/egs/sg-en-i2r/convert-sge-lexicon.pl $dictdir/sge-phone-to-en-phone-map.txt | \
  source/egs/seame-sept-18-2017/add-or-remove-lang-identifier-to-phone.pl --add-identifier='_sge' > $dictdir/sge-lexicon.txt
  grep '<' $srcdict/lexicon.txt | \
  grep -v '<silence>' | \
  cat - <(grep -v '_sge' $dictdir/man-lexicon.txt) | \
  cat - <(grep -v phd $dictdir/sge-lexicon.txt) | \
      cat - <( echo -e "phd's\tP_sge IY_sge EY_sge CH_sge D_sge IY_sge Z_sge"
      echo -e "phd\tP_sge IY_sge EY_sge CH_sge D_sge IY_sge") | sort -u > $dictdir/lexicon.txt
  echo "## Done with '$dictdir'"
fi
if [ ! -z $step02 ]; then
  echo "## LOG (step02): making lexicon started @ `date`"
  grep -v '<' $dictdir/lexicon.txt | \
  cut -f2- | awk '{for(i=1; i<=NF; i++){print $i;}}' | sort -u > $dictdir/nonsilence_phones.txt
  cp $srcdict/{silence_phones.txt,optional_silence.txt,extra_questions.txt} $dictdir/
  cat $dictdir/silence_phones.txt | perl -ane 'chomp; print "$_ ";' > $dictdir/extra_questions.txt
  echo  >> $dictdir/extra_questions.txt
  grep '_man' $dictdir/nonsilence_phones.txt  | perl -ane 'chomp; print "$_ ";' >>  $dictdir/extra_questions.txt
  echo  >> $dictdir/extra_questions.txt
  grep '_sge' $dictdir/nonsilence_phones.txt   | perl -ane 'chomp; print "$_ ";'  >> $dictdir/extra_questions.txt
  echo >> $dictdir/extra_questions.txt
  utils/validate_dict_dir.pl $dictdir
  echo "## Done with '$dictdir'"
fi
train=$tgtdir/data/train
[ -d $train ] || mkdir -p $train
if [ ! -z $step03 ]; then
  cp $srcdata/{wav.scp,segments,utt2spk,spk2utt} $train/
  cat $srcdata/text | sed 's:<silence>::g' | awk '{if(NF>1){print;}}' > $train/text
  utils/fix_data_dir.sh $train
  echo "## LOG (step03): done with data preparation  '$train'"
fi
lang=$tgtdir/data/lang
if [ ! -z $step04 ]; then
  utils/prepare_lang.sh $dictdir "<unk>"  $lang/tmp $lang
  echo "## LOG (step04): done with '$lang'"
fi

if [ ! -z $step05 ]; then
  for sdata in $train; do
    data=$sdata/mfcc-pitch feat=$sdata/feat/mfcc-pitch/data log=$sdata/feat/mfcc-pitch/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step05): done with '$data' @ `date`"
    data=$sdata/mfcc-hires feat=$sdata/feat/mfcc-hires/data log=$sdata/feat/mfcc-hires/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    echo "## LOG (step05): done with '$data' @ `date`"
  done
fi
expdir=$tgtdir/exp
if [ ! -z $step06 ]; then
  echo "## LOG (step06): training started @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1-5 \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 7000 --pdf-num 140000 \
  $train/mfcc-pitch $lang $expdir  || exit 1
  echo "## LOG (step06): done with '$expdir/tri4a' @ `date`"
fi
lmdir=$tgtdir/data/local/lm-4gram
[ -d $lmdir ] || mkdir -p $lmdir
if [ ! -z $step07 ]; then
  cut -f1 $dictdir/lexicon.txt |sort -u | gzip -c > $lmdir/vocab.gz
  cut -d' ' -f2- $train/text | gzip -c > $lmdir/train.gz
  cat  ../seame-nnet3-feb-06-2017/data/dev-man-dominant/text \
  ../seame-nnet3-feb-06-2017/data/dev-sge-dominant/text | \
  cut -d' ' -f2- | gzip -c > $lmdir/dev.gz  
fi
if [ ! -z $step08 ]; then
  ngram-count -order 4 -kndiscount -interpolate\
   -vocab $lmdir/vocab.gz  -unk -sort -text $lmdir/train.gz -lm $lmdir/lm-4gram.gz
   ngram -order 4 -lm $lmdir/lm-4gram.gz -ppl $lmdir/dev.gz
fi
