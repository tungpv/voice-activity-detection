#!/bin/bash

. path.sh
. cmd.sh

# begin options
mling_dnn_fbank=false
mling_gen_lat=false
mling_hierbn=false
mling2_hierbn=false
# end options

. utils/parse_options.sh || \
{ echo "$0: ERROR, parse_options.sh expected !"; exit 1; }

function PrintOptions {
  cat<<END
  $0 [options]:
mling_dnn_fbank			  # value, $mling_dnn_fbank
mling_gen_lat                     # value, $mling_gen_lat
mling_hierbn                      # value, $mling_hierbn
mling2_hierbn			  # value, $mling2_hierbn

END
}

PrintOptions;

if $mling_dnn_fbank; then
  echo "$0: mling_dnn_fbank started @ `date`"
  mling_opts="--hid-dim 2048 --nn-depth 5 --splice 10" 
  langnames="cant:pash:taga:turk:viet:tami"
  langs="../cant101/flp2/data/lang:../pash104/flp2/data/lang:../taga106/flp2/data/lang:../turk105/flp2/data/lang:../viet107/flp2/data/lang:../tami204/flp2/data/lang"
  alidatas="../cant101/flp2/data/train/plp_pitch:../pash104/flp2/data/train/plp_pitch:../taga106/flp2/data/train/plp_pitch:../turk105/flp2/data/train/plp_pitch:../viet107/flp2/data/train/plp_pitch:../tami204/flp2/data/train/plp_pitch"
  sdirs="../cant101/flp2/exp/tri4a:../pash104/flp2/exp/tri4a:../taga106/flp2/exp/tri4a:../turk105/flp2/exp/tri4a:../viet107/flp2/exp/tri4a:../tami204/flp2/exp/tri4a"
  source/test/run-nnet-mling.sh --tgt-dir llp/exp/mling.flp6 \
  --mling-langnames "$langnames" \
  --mling-langs "$langs" \
  --mling-alidatas "$alidatas" \
  --mling-sdirs  "$sdirs" \
  --train-fbank true  --steps 1,2,3,4,5 $mling_opts
  
fi
if $mling_gen_lat; then
  source/test/run-nnet-mling.sh  --tgt-dir  llp/exp/mling.trap10 \
  --train-fbank true  --steps 9
fi

if $mling_hierbn; then
  source/test/run-nnet-mling.sh --tgt-dir llp/exp/mling.hierbn \
  --hierbn-train true --steps 1,2,3,4,5,6
fi

langnames="cant:pash:taga:turk:viet:tami"
langs="../cant101/flp2/data/lang:../pash104/flp2/data/lang:../taga106/flp2/data/lang:../turk105/flp2/data/lang:../viet107/flp2/data/lang:../tami204/flp2/data/lang"
alidatas="../cant101/flp2/data/train/plp_pitch:../pash104/flp2/data/train/plp_pitch:../taga106/flp2/data/train/plp_pitch:../turk105/flp2/data/train/plp_pitch:../viet107/flp2/data/train/plp_pitch:../tami204/flp2/data/train/plp_pitch"
sdirs="../cant101/flp2/exp/tri4a:../pash104/flp2/exp/tri4a:../taga106/flp2/exp/tri4a:../turk105/flp2/exp/tri4a:../viet107/flp2/exp/tri4a:../tami204/flp2/exp/tri4a"

if $mling2_hierbn; then
  echo "$0: mling2_hierbn started @ `date`"
  source/test/run-nnet-mling.sh --tgt-dir llp/exp/mling.flp6.hierbn \
  --mling-langnames "$langnames" \
  --mling-langs "$langs" \
  --mling-alidatas "$alidatas" \
  --mling-sdirs  "$sdirs" \
  --hierbn-train true --steps 1,2,3,4,5,6

fi
