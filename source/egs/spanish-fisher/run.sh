#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
cmd=run.pl
nj=20
steps=
# end options

. parse_options.sh

function Usage {
 cat<<END
 $0 [options] <source-data-dir> <tgtdir>
 [options]:
 [steps]:
 2 prepare fisher data (ldc2010t04, ldc2010s01)
 [examples]:
 
 $0  --steps 1  /data/users/hhx502/ldc-cts2016  spanish-fisher

END
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

source_data_dir=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  echo "## LOG: step01, lowercase files @ `date`"
  for xdir in /data/users/hhx502/ldc-cts2016/hub5-spanish-cts \
              /data/users/hhx502/ldc-cts2016/spanish-ldc2010t04 \
              /data/users/hhx502/ldc-cts2016/spanish-ldc2010s01; do
    for SRC in $(find $xdir -depth); do
      DST=`dirname "${SRC}"`/`basename "${SRC}" | tr '[A-Z]' '[a-z]'`
      if [ "${SRC}" != "${DST}" ]; then
        [ ! -e "${DST}" ] && mv -T "${SRC}" "${DST}" || echo "${SRC} was not renamed"
      fi
    done
  done
  echo "## LOG: step01, done @ `date`"
fi

esdata=$source_data_dir/spanish-ldc2010-callhome-merge/data/local
train_ldc2010=$esdata/ldc2010-train-all
if [ ! -z $step02 ]; then
  echo "## LOG: step02, prepare fisher data @ `date`"
  tmpdir=$esdata/tmp-ldc2010
  [ -d $tmpdir ] || mkdir -p $tmpdir
  find  $source_data_dir/spanish-ldc2010s01/*/data -name "*.sph" > $tmpdir/train_sph.flist
  find $source_data_dir/spanish-ldc2010t04 -name "*.tdf" > $tmpdir/train_transcripts.flist
  $tgtdir/local/fsp_make_trans.pl $tmpdir
  
  mkdir -p $train_ldc2010
  mv $tmpdir/reco2file_and_channel $train_ldc2010/
  sort $tmpdir/text.1 | grep -v '((' | \
  awk '{if (NF > 1){ print; }}' | \
  sed 's:<\s*[/]*\s*\s*for[ei][ei]g[nh]\s*\w*>::g' | \
  sed 's:<lname>\([^<]*\)<\/lname>:\1:g' | \
  sed 's:<lname[\/]*>::g' | \
  sed 's:<laugh>[^<]*<\/laugh>:[laughter]:g' | \
  sed 's:<\s*cough[\/]*>:[noise]:g' | \
  sed 's:<sneeze[\/]*>:[noise]:g' | \
  sed 's:<breath[\/]*>:[noise]:g' | \
  sed 's:<lipsmack[\/]*>:[noise]:g' | \
  sed 's:<background>[^<]*<\/background>:[noise]:g' | \
  sed -r 's:<[/]?background[/]?>:[noise]:g' | \
  #One more time to take care of nested stuff
  sed 's:<laugh>[^<]*<\/laugh>:[laughter]:g' | \
  sed -r 's:<[/]?laugh[/]?>:[laughter]:g' | \
  #now handle the exceptions, find a cleaner way to do this?
  sed 's:<foreign langenglish::g' | \
  sed 's:</foreign::g' | \
  sed -r 's:<[/]?foreing\s*\w*>::g' | \
  sed 's:</b::g' | \
  sed 's:<foreign langengullís>::g' | \
  sed 's:foreign>::g' | \
  sed 's:>::g' | \
  #How do you handle numbers?
  grep -v '()' | \
  #Now go after the non-printable characters
  sed -r 's:¿::g' > $tmpdir/text.2
  cp $tmpdir/text.2 $train_ldc2010/text
  #Create segments file and utt2spk file
  ! cat $train_ldc2010/text | \
  perl -ane 'm:([^-]+)-([AB])-(\S+): || die "Bad line $_;"; print "$1-$2-$3 $1-$2\n"; ' > $train_ldc2010/utt2spk \
  && echo "Error producing utt2spk file" && exit 1;

  cat $train_ldc2010/text | perl -ane 'm:((\S+-[AB])-(\d+)-(\d+))\s: || die; $utt = $1; $reco = $2;
  $s = sprintf("%.2f", 0.01*$3); $e = sprintf("%.2f", 0.01*$4); if ($s != $e) {print "$utt $reco $s $e\n"}; '\
  >$train_ldc2010/segments
  utils/utt2spk_to_spk2utt.pl <$train_ldc2010/utt2spk > $train_ldc2010/spk2utt
  sph2pipe=/opt/kaldi_updated/trunk/tools/sph2pipe_v2.5/sph2pipe
  cat $tmpdir/train_sph.flist | \
  perl -ane 'm:/([^/]+)\.sph$: || die "bad line $_; ";  print "$1 $_"; ' | \
  awk -v sph2pipe=$sph2pipe \
  '{printf("%s-A %s -f wav -p -c 1 %s |\n", $1, sph2pipe, $2); printf("%s-B %s -f wav -p -c 2 %s |\n", $1, sph2pipe, $2);}' | \
  sort -k1,1 -u  > $train_ldc2010/wav.scp || exit 1;
  source/egs/spanish-fisher/fsp_make_spk2gender.sh > $train_ldc2010/spk2gender
  echo "## LOG: step02, done @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "## LOG: step03, prepare callhome data @ `date`"
  tmpdir=$esdata/tmp-callhome
  [ -d $tmpdir ] || mkdir -p $tmpdir
  find $source_data_dir/hub5-spanish-cts/data -name "*.sph" > $tmpdir/callhome_train_sph.flist
  find $source_data_dir/hub5-spanish-cts/transcriptions -name "*.txt" > $tmpdir/callhome_train_transcripts.flist
  data_callhome=$esdata/callhome-data
  [ -d $data_callhome ] || mkdir -p $data_callhome
  source/egs/spanish-fisher/callhome_make_trans.pl $tmpdir
  cat $tmpdir/callhome.text.1 | \
  perl -ane 'use open qw(:std :utf8); use utf8; chomp; @A = split(/\s+/); $total = scalar @A; $num = 0;
    for($i=1; $i< @A; $i++){$w = $A[$i]; if($w =~ /[\[\<]/){$num++;} } if($num>=$total || $num/$total >0.4) {
      print STDERR "$_\n"; 
    } else { print "$_\n";} 
  '  > $tmpdir/text 2> $tmpdir/removed-text
  # echo "## DEBUG: $tmpdir/text, done" && exit 1
  CHARS=$(python -c 'print u"\u00BF\u00A1".encode("utf8")')
  # sed -i 's/['"$CHARS"']//g' $tmpdir/text
  # iconv -f iso-8859-15 -t utf8 < $tmpdir/text > $tmpdir/text.1
  
  ! cat $tmpdir/text | \
  perl -ane 'm:([^-]+)-([AB])-(\S+): || die "Bad line $_;"; print "$1-$2-$3 $1-$2\n"; ' \
  > $tmpdir/utt2spk && echo "Error producing utt2spk file" && exit 1;
  cat $tmpdir/text | \
  perl -ane 'm:((\S+-[AB])-(\d+)-(\d+))\s: || die; $utt = $1; $reco = $2;
 $s = sprintf("%.2f", 0.01*$3); $e = sprintf("%.2f", 0.01*$4); print "$utt $reco $s $e\n"; ' >$tmpdir/segments
  utils/utt2spk_to_spk2utt.pl < $tmpdir/utt2spk > $tmpdir/spk2utt

  sph2pipe=/opt/kaldi_updated/trunk/tools/sph2pipe_v2.5/sph2pipe
  cat $tmpdir/callhome_train_sph.flist | \
  perl -ane 'm:/([^/]+)\.sph$: || die "bad line $_; ";  print lc($1)," $_"; ' | \
  awk -v sph2pipe=$sph2pipe '{printf("%s-A %s -f wav -p -c 1 %s |\n", $1, sph2pipe, $2); printf("%s-B %s -f wav -p -c 2 %s |\n", $1, sph2pipe, $2);}' | \
  sort -k1,1 -u  > $tmpdir/wav.scp || exit 1;

  mv $tmpdir/callhome_reco2file_and_channel $data_callhome/reco2file_and_channel
  mv $tmpdir/{text,segments,utt2spk,spk2utt,wav.scp} $data_callhome/ 
  utils/fix_data_dir.sh $data_callhome
  utils/validate_data_dir.sh $data_callhome
  echo "## LOG: step03, done with $data_callhome  @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## LOG: step04, remove words from the texts @ `date`"
  for x in  $esdata/{callhome-data,ldc2010-train-all}/.text ; do
    cat $x | \
    perl -e '($dict) = @ARGV; open(D, "<$dict") or die "## ERROR, dict $dict cannot open\n"; while(<D>){chomp; @A = split(/\s+/); $w = shift @A;  $vocab{$w} = join(" ", @A);  } close D; while(<STDIN>) {chomp; @A = split(/\s+/); $u = shift @A; for($i = 0; $i < @A; $i++){$w= $A[$i]; if(exists $vocab{$w}){$w = $vocab{$w}; } $u .= " $w";   } print "$u\n";  }' $esdata/dict/map-typo.txt \
    >  $(dirname $x)/text
  done
  echo "## LOG: step04, $esdata/{callhome-data,ldc2010-train-all} done @ `date`"
fi
dictdir=$esdata/dict
if [ ! -z $step05 ]; then
  echo "## LOG: step05, prepare dict @ `date`"
  [ -d $dictdir ] || mkdir -p $dictdir
  tmpdir=/data/users/hhx502/ldc-cts2016/ldc-resource/callhome_spanish_lexicon_970908
  cat $esdata/{callhome-data,ldc2010-train-all}/text | \
  perl -ane 'chomp; @A = split(/\s+/); for($i=1; $i<@A; $i++){print $A[$i], "\n";}' | \
  sort -u  | \
  egrep -v '\[' > $tmpdir/uniquewords
  spanish-fisher/local/merge_lexicons.py $tmpdir /data/users/hhx502/ldc-cts2016/ldc-resource
  echo "## LOG: step05, dict preparation done tmpdir=$tmpdir @ `date`"
fi

if [ ! -z $step06 ]; then
  echo "## LOG: step06, make initial lex @ `date`"
  tmpdir=/data/users/hhx502/ldc-cts2016/ldc-resource/callhome_spanish_lexicon_970908
  perl spanish-fisher/local/find_unique_phones.pl $tmpdir $tmpdir 
  cat $tmpdir/uniquewords64k | spanish-fisher/local/spron.pl $tmpdir/preferences $tmpdir/basic_rules \
    | cut -f1 | sed -r 's:#\S+\s\S+\s\S+\s\S+\s(\S+):\1:g' \
    | awk -F '[/][/]' '{print $1}' \
    > $tmpdir/lexicon_raw
  local=spanish-fisher/local
  perl $local/isolate_phones.pl $tmpdir
  cat $tmpdir/phones_extended | sort | awk '{if ($1 != "") {print;}}' > $tmpdir/phones_extended.1
  mv $tmpdir/phones $tmpdir/phones.small
  mv $tmpdir/phones_extended.1 $tmpdir/phones
  sort $tmpdir/phones -o $tmpdir/phones
  paste -d ' ' $tmpdir/uniquewords64k $tmpdir/lexicon_one_column | sed -r 's:(\S+)\s#.*:\1 oov:g' > $tmpdir/lexicon.1  

  echo "## LOG: step06, done ($tmpdir) @ `date`"
fi

if [ ! -z $step07 ]; then
  echo "## LOG: step07, phone set @ `date`"
  tmpdir=/data/users/hhx502/ldc-cts2016/ldc-resource/callhome_spanish_lexicon_970908
  # silence phones, one per line. 
  for w in sil lau noise "<oov>"; do echo $w; done > $tmpdir/silence_phones.txt
  echo sil > $tmpdir/optional_silence.txt

  # An extra question will be added by including the silence phones in one class.
  cat $tmpdir/silence_phones.txt| awk '{printf("%s ", $1);} END{printf "\n";}' > \
  $tmpdir/extra_questions.txt || exit 1;

  # Remove [] chars from phones
  cat $tmpdir/phones | awk '{if ($1 != "_" && $1 != "[" && $1 != "]") {print;}}' > $tmpdir/phones.1
  rm $tmpdir/phones
  mv $tmpdir/phones.1 $tmpdir/phones
  cp $tmpdir/phones $tmpdir/nonsilence_phones.txt
  
  if [ -f $tmpdir/lexicon.2 ]; then rm $tmpdir/lexicon.2; fi
  cp "$tmpdir/lexicon.1" "$tmpdir/lexicon.2"
  
  # Add prons for laughter, noise, oov
  for w in $(echo "[noise] [laughter]"); do
    sed -i "/\[$w\]/d" $tmpdir/lexicon.2
  done
  echo -e "[noise]\tnoise\n[laughter]\tlau\n<unk>\t<oov>\nmm\tm" | \
  cat - $tmpdir/lexicon.2 | \
  sed 's:\s_::g' > $tmpdir/lexicon.txt
  

  echo "## LOG: step07, @ `date`"
fi

if [ ! -z $step08 ] ;then
  echo "## LOG: step08, @ `date`"

  tmpdir=/data/users/hhx502/ldc-cts2016/ldc-resource/callhome_spanish_lexicon_970908
  cat $esdata/{callhome-data,ldc2010-train-all}/text | \
  awk '{for (n=2;n<=NF;n++){ count[$n]++; } } END { for(n in count) { print count[n], n; }}' | \
  sort -nr > $tmpdir/word_counts

  awk '{print $1}' $tmpdir/lexicon.txt | \
  perl -e '($word_counts)=@ARGV;
   open(W, "<$word_counts")||die "opening word-counts $word_counts";             
   while(<STDIN>) { chop; $seen{$_}=1; }
   while(<W>) {
     ($c,$w) = split;
     if (!defined $seen{$w}) { print; }                                          
   } ' $tmpdir/word_counts > $tmpdir/oov_counts.txt                                    
                                                                                 
  echo "*Highest-count OOVs are:"                                                  
  head -n 20 $tmpdir/oov_counts.txt 

  echo "## LOG: step08, done @ `date`"
fi
tgt_dict_dir=$tgtdir/data/local/dict
tgt_lang=$tgtdir/data/lang
[ -d $tgt_lang ] || mkdir -p $tgt_lang
[ -d $tgt_dict_dir ] || mkdir -p $tgt_dict_dir
if [ ! -z $step09 ] ;then
  echo "## LOG: step09, prepare final dict @ `date`"
  tmpdir=/data/users/hhx502/ldc-cts2016/ldc-resource/callhome_spanish_lexicon_970908
  cat  $esdata/{callhome-data,ldc2010-train-all}/text  | \
  perl -ane 'chomp; @A = split(/\s+/); for($i=1; $i<@A; $i++){ print "$A[$i]\n";} ' | \
  sort -u | \
  perl -e '($dict) = @ARGV; while(<STDIN>) {chomp; $vocab{$_}++;} 
    open(D, "$dict") or die "## ERROR: step09, dict $dict cannot open\n";
    while(<D>) { chomp; @A = split(/\s+/);  $w = shift @A; 
      if(exists $vocab{$w}) {print "$_\n";}
    } close D;   
 ' $tmpdir/lexicon.txt > $dictdir/lexicon.txt
  cat $dictdir/lexicon.txt | \
  egrep -v '\[' | \
  perl -ane 'chomp; @A = split(/\s+/); shift @A; for($i = 0; $i<@A; $i++){print "$A[$i]\n";}' | \
  sort -u > $dictdir/nonsilence_phones.txt
  (
    echo "sil"
    echo "lau"
    echo "noise"
    echo "<oov>"
  ) > $dictdir/silence_phones.txt
  echo sil > $dictdir/optional_silence.txt
  echo -n > $dictcir/extra_questions.txt
  (
    echo -e "<unk>\t<oov>"
   ) |  cat - $dictdir/lexicon.txt  > $dictdir/lexicon.txt.1
   mv $dictdir/lexicon.txt.1 $dictdir/lexicon.txt
   utils/validate_dict_dir.pl $dictdir
   cp -r $dictdir/* $tgt_dict_dir/
   utils/prepare_lang.sh $tgt_dict_dir "<unk>" $tgt_lang/tmp $tgt_lang || exit 1
  echo "## LOG: step09, check ($dictdir, $tgt_lang) done @ `date`"
fi

if [ ! -z $step10 ]; then
  echo "## LOG: step10, prepare ldc2010-train-all data @ `date`"
  data=/data/users/hhx502/ldc-cts2016/spanish-ldc2010-callhome-merge/data/local/ldc2010-train-all
  tgt_data=$tgtdir/data/ldc2010-train-all
  [ -d $tgt_data ] || mkdir -p $tgt_data
  cat $data/text | \
  perl -ane 'use open qw(:std :utf8); use utf8; chomp; @A = split(/\s+/); $total = scalar @A; $num = 0;
    for($i=1; $i< @A; $i++){$w = $A[$i]; if($w =~ /[\[\<]/){$num++;} } if($num>=$total || $num/$total >0.4) {
      print STDERR "$_\n"; 
    } else { print "$_\n";} 
  '  > $tgt_data/text 2> $data/removed-text
  cat $data/segments | \
  perl -ane 'chomp; @A = split(/\s+/); if($A[2] + 0.1 < $A[3]){print "$_\n";} else {print STDERR "## WARN: $_ (too short)\n";}' \
  > $tgt_data/segments 
  cp $data/{reco2file_and_channel,utt2spk,spk2utt,wav.scp} $tgt_data/
  utils/fix_data_dir.sh $tgt_data
  utils/validate_data_dir.sh --no-feats $tgt_data
  source/egs/swahili/subset_data.sh --random true --subset-time-ratio 0.04 \
  --data2 $tgtdir/data/ldc2010-train  $tgt_data  $tgtdir/data/ldc2010-dev
  echo "## LOG: step10, check ($data, $tgt_data)done @ `date`"
fi

if [ ! -z $step11 ]; then
  echo "## LOG: step11, prepare callhome data @ `date`"
  data=/data/users/hhx502/ldc-cts2016/spanish-ldc2010-callhome-merge/data/local/callhome-data
  tgt_data=$tgtdir/data/callhome-data
  [ -d $tgt_data ] || mkdir -p $tgt_data
  cat $data/text | \
  perl -ane 'use open qw(:std :utf8); use utf8; chomp; @A = split(/\s+/); $total = scalar @A; $num = 0;
    for($i=1; $i< @A; $i++){$w = $A[$i]; if($w =~ /[\[\<]/){$num++;} } if($num>=$total || $num/$total >0.4) {
      print STDERR "$_\n"; 
    } else { print "$_\n";} 
  '  > $tgt_data/text 2> $data/removed-text.1
  cat $data/segments | \
  perl -ane 'chomp; @A = split(/\s+/); if($A[2] + 0.1 < $A[3]){print "$_\n";} else {print STDERR "## WARN: $_ (too short)\n";}' \
  > $tgt_data/segments 
  cp $data/{reco2file_and_channel,utt2spk,spk2utt,wav.scp} $tgt_data/
  utils/fix_data_dir.sh $tgt_data
  utils/validate_data_dir.sh --no-feats $tgt_data
  source/egs/swahili/subset_data.sh --random true --subset-time-ratio 0.05 \
  --data2 $tgtdir/data/callhome-train   $tgt_data  $tgtdir/data/callhome-dev 
  utils/combine_data.sh $tgtdir/data/train-merge $tgtdir/data/callhome-train $tgtdir/data/ldc2010-train
  utils/combine_data.sh $tgtdir/data/dev  $tgtdir/data/callhome-dev  $tgtdir/data/ldc2010-dev
  source/egs/spanish-fisher/prepare_stm.pl $tgtdir/data/dev
  echo "## LOG: step11, check ($data, $tgt_data)  done @ `date`"
fi
lmdir=$tgtdir/data/local/lm
if [ ! -z $step12 ]; then
  echo "## LOG: step12, train lm @ `date`"
  [ -d $lmdir ] || mkdir -p $lmdir
  source/egs/fisher-english/train-kaldi-lm.sh $tgtdir/data/local/dict/lexicon.txt \
  $tgtdir/data/train-merge $tgtdir/data/dev $lmdir  
  echo "## LOG: step12, done @ `date`"
fi
lang_test=$tgtdir/data/lang_test
arpa_lm=$lmdir/3gram-mincount/lm_unpruned.gz
if [ ! -z $step13 ]; then
  echo "## LOG: step13, build grammar G @ `date`"
  [ -d $lang_test ] || mkdir -p $lang_test
  cp -r $tgt_lang/* $lang_test
  gunzip -c "$arpa_lm" | \
  arpa2fst --disambig-symbol=#0 \
           --read-symbol-table=$lang_test/words.txt - $lang_test/G.fst
  echo  "Checking how stochastic G is (the first of these numbers should be small):"
  fstisstochastic $lang_test/G.fst
  ## Check lexicon.
  ## just have a look and make sure it seems sane.
  echo "First few lines of lexicon FST:"
  fstprint   --isymbols=$lang_test/phones.txt --osymbols=$lang_test/words.txt $lang_test/L.fst  | head
  
  echo Performing further checks

  # Checking that G.fst is determinizable.
  fstdeterminize $lang_test/G.fst /dev/null || echo Error determinizing G.

  # Checking that L_disambig.fst is determinizable.
  fstdeterminize $lang_test/L_disambig.fst /dev/null || echo Error determinizing L.
  # Checking that disambiguated lexicon times G is determinizable
  # Note: we do this with fstdeterminizestar not fstdeterminize, as
  # fstdeterminize was taking forever (presumbaly relates to a bug
  # in this version of OpenFst that makes determinization slow for
  # some case).
  fsttablecompose $lang_test/L_disambig.fst $lang_test/G.fst | \
   fstdeterminizestar >/dev/null || echo Error
  
  # Checking that LG is stochastic:
  fsttablecompose $lang_test/L_disambig.fst $lang_test/G.fst | \
  fstisstochastic || echo "[log:] LG is not stochastic"

  echo "## LOG: step13, done @ `date`"
fi
if [ ! -z $step14 ]; then
  echo "## LOG: step14, mfcc @ `date` "
  for x in train-merge dev; do 
    sdata=$tgtdir/data/$x; data=$sdata/mfcc; feat=$sdata/feat/mfcc
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc.sh --mfcc-config conf/mfcc.conf" \
    $sdata $data $feat || exit 1
  done
  echo "## LOG: step14, mfcc done `date`"
fi

if [ ! -z $step15 ]; then
  echo "## LOG: step15, gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd run.pl --nj 20 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 6000 --pdf-num 90000 \
  --devdata $tgtdir/data/dev/mfcc \
  $tgtdir/data/train-merge/mfcc $tgtdir/data/lang_test $tgtdir/exp || exit 1
  echo "## LOG: step15, done gmm-hmm @ `date`"
fi

if [ ! -z $step16 ]; then
  echo "## LOG: step16, fbank-pitch @ `date`"
  for x in train-merge dev; do 
    sdata=$tgtdir/data/$x; data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf" \
    $sdata $data $feat || exit 1
  done
  echo "## LOG: step16, fbank-pitch done @ `date`"
fi
