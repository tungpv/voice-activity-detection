#!/usr/bin/perl -w
use strict;
use open qw(:std :utf8);
print STDERR "$0: stdin expected \n";
while(<STDIN>) {
  my @A = split(/ /);
  for(my $i=0; $i <@A; $i++) {
    my $w = $A[$i];
    length($w) == 0 && continue;
    print $w, "\t", join(" ", split(//,$w)),  " ", length($w),"\n";
  }
}
print STDERR "$0: stdin ended\n";
