#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=run.pl
nj=20
hierbn_cross_tune=false
hierbn_cross_tune_opts="<validating_rate> <sdir> <feature_transform> <learn_rate> <nnet_sdir> <lang> <traindata> <alidir> <dir>"
bnName=llp.mono
subset_data=false
subset_data_opts="<sdata> <factor> <data>"
make_mfcc_pitch=false
make_mfcc_pitch_opts="<mfcc_conf> <pitch_conf> <sdata> <data> <feat>"
make_plp=false
make_plp_opts="<config> <sdata> <data> <feat>"
make_plp_pitch=false
make_plp_pitch_opts="<plp.conf> <pitch.conf> <sdata> <data> <feat>"
make_bnf2=false
make_bnf2_opts="<sdata> <bnf_dnn_sdir> <data_dir> <feat_dir>"
make_fmllr=false
make_fmllr_opts="<sdata> <sdir> <transform_dir> <data> <feat>"
append_delta=false
append_delta_opts="<sdata> <data> <feat>"
paste_feats=false
paste_feats_opts="<sdata1> <sdata2> <data> <feat>"
make_ali=false
make_ali_opts="<data-dir> <lang-dir> <src-dir> <align-dir>"
ali_cmd="steps/nnet/align.sh --nj 20"
make_denlats=false
make_denlats_opts="<acwt/0.0909> <config> <data> <lang> <sdir> <latdir>"
train_nnet_mpe=false
train_nnet_mpe_opts="<acwt> <data> <lang> <sdir> <alidir> <latdir> <dir>"
run_gmm=false
run_gmm2=false
cmvn_opts=
import_alidir=
import_tri5_alidir=
run_gmm2_opts="<trainId (a,b,etc)> <data> <dataName(train)> <lang> <states> <pdfs> <tgtdir> #7"
use_lda_mat=
lda_dim=40
run_tandem_nnet=false
preTrnCmd="steps/nnet/pretrain_dbn.sh --splice 5 --nn-depth 7 --hid-dim 1024"
tandem_nnet_opts="<train_data> <train_feat> <validating_feat> <sdir> <lang> <alidata> <dir> <trnCmd> <learn_rate>"
run_pnorm=false
pnorm_opts="<initial_learning_rate> <final_learning_rate> <pnorm_input_dim> <pnorm_output_dim> <samples_per_iter> <num_threads> <splice_width> <lda_dim> <data> <lang> <ali> <dir>"
decoding_eval=false
decoding_opts="<data> <lang> <sdir> <graph> <decode_dir> <decoding_cmd>"
scoring_opts="--min-lmwt 8 --max-lmwt 24"
lattice_opts="--beam 13.0 --lattice-beam 8.0 --max-mem 800000000"
run_vllp=false
run_flp=false
lattice_onebest=false
lattice_onebest_opts="<latdir> <lmwt> <lang> <dir>"
filter_onebest=false
filter_onebest_opts="<text> <new_text>"
make_fbank_pitch=false
make_fbank_pitch_opts="<sdata> <data> <feat>"
fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
sdataName="train:dev10h"
gmmtrain_opts="--train-id a --train-dataname train --tri-state 2500 --tri-pdf 40000"
score_combine=false
score_combine_opts="<min_lmwt> <max_lmwt> <stm> <data> <dataname> <lang> <dir>"
score_combine_latdirs="latdir1 latdir2"
decoding_rover=false
scoring_rover_opts="<stm> <dir>"
rover_opts="-m maxconf -T"
ctm_files="ctm1 ctm2 ctm3"
steps=

# begin options

echo
echo "$0 $@"
echo

. parse_options.sh

function PrintOptions {
  cmdName=$(echo $0| perl -pe 's/^.*\///g;')
  cat <<END

$cmdName [options]:
cmd					# value, "$cmd"
nj					# value, "$nj"
hierbn_cross_tune			# value, $hierbn_cross_tune
hierbn_cross_tune_opts			# value, "$hierbn_cross_tune_opts"
bnName					# value, "$bnName"
subset_data				# value, $subset_data
subset_data_opts			# value, "$subset_data_opts"
make_mfcc_pitch				# value, $make_mfcc_pitch
make_mfcc_pitch_opts			# value, "$make_mfcc_pitch_opts"
make_plp				# value, $make_plp
make_plp_opts				# value, "$make_plp_opts"
make_plp_pitch				# value, $make_plp_pitch
make_plp_pitch_opts			# value, "$make_plp_pitch_opts"
make_bnf2				# value, $make_bnf2
make_bnf2_opts				# value, "$make_bnf2_opts"
make_fmllr				# value, $make_fmllr
make_fmllr_opts				# value, "$make_fmllr_opts"
append_delta				# value, $append_delta
append_delta_opts			# value, "$append_delta_opts"
paste_feats				# value, $paste_feats
paste_feats_opts			# value, "$paste_feats_opts"
make_ali				# value, $make_ali
make_ali_opts				# value, "$make_ali_opts"
ali_cmd					# value, "$ali_cmd"
make_denlats				# value, $make_denlats
make_denlats_opts			# value, "$make_denlats_opts"
train_nnet_mpe				# value, $train_nnet_mpe
train_nnet_mpe_opts			# value, "$train_nnet_mpe_opts"
run_gmm					# value, $run_gmm
run_gmm2				# value, $run_gmm2
cmvn_opts                               # value, "$cmvn_opts"
import_alidir				# value, "$import_alidir"
import_tri5_alidir			# value, "$import_tri5_alidir"
run_gmm2_opts				# value, "$run_gmm2_opts"
use_lda_mat				# value, "$use_lda_mat"
lda_dim					# value, $lda_dim
run_tandem_nnet				# value, $run_tandem_nnet
preTrnCmd				# value, "$preTrnCmd"
tandem_nnet_opts			# value, "$tandem_nnet_opts"
run_pnorm				# value, $run_pnorm
pnorm_opts				# value, "$pnorm_opts"
decoding_eval				# value, $decoding_eval
decoding_opts				# value, "$decoding_opts"
scoring_opts				# value, "$scoring_opts"
lattice_opts				# value, "$lattice_opts"
run_vllp				# value, $run_vllp
run_flp					# value, $run_flp
lattice_onebest				# value, $lattice_onebest       # for semi-supervised training
lattice_onebest_opts			# value, "$lattice_onebest_opts"
filter_onebest				# value, $filter_onebest
filter_onebest_opts			# value, "$filter_onebest_opts"
make_fbank_pitch			# value, $make_fbank_pitch
make_fbank_pitch_opts			# value, "$make_fbank_pitch_opts"
fbankCmd				# value, "$fbankCmd"
sdataName				# value, "$sdataName"
gmmtrain_opts				# value, "$gmmtrain_opts"
score_combine				# value, $score_combine
score_combine_opts			# value, "$score_combine_opts"
score_combine_latdirs		        # value, "$score_combine_latdirs"
decoding_rover				# value, $decoding_rover
scoring_rover_opts			# value, "$scoring_rover_opts"
rover_opts				# value, "$rover_opts"
ctm_files				# value, "$ctm_files"
steps					# value, "$steps"

END
}

PrintOptions;
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if $subset_data; then
  echo "$0: subset_data started @ `date`"
  optNames="sdata factor data"
  . source/register_options.sh "$optNames" "$subset_data_opts" || \
  { echo "$0: subset_data: ERROR, @register_options.sh"; exit 1; }
  if [ $(echo "$factor <=0 ||$factor >1"|bc -l) -eq 1 ]; then
    echo "$0: subset_data: ERROR, for factor=$factor"; exit 1
  fi
  [ -d $data ] || mkdir -p $data
  iutils/mksubset_spklist.pl $factor $sdata/spk2utt "feat-to-len scp:$sdata/feats.scp ark,t:- |"  >$data/spklist || exit 1
  utils/subset_data_dir.sh --spk-list $data/spklist $sdata $data || exit 1
  utils/fix_data_dir.sh $data
  echo "$0: subset_data ended @ `date`"
fi
sdatadir="$rdir/data/train/plp_pitch:$rdir/data/dev/plp_pitch"
if $run_gmm; then
  echo "$0: run_llp started @ `date`" && exit 1
  source/test/run-bnf.sh --cmd "$cmd" --nj $nj \
  --tgt-dir $tgt_dir \
  ${step01:+--make-fbank true --fbankCmd "$fbankCmd"} --sdatadir "$sdatadir" --sdataName $sdataName \
  ${step02:+--make-bnf true --src-bndir $src_bndir} --bnName $bnName \
  ${step03:+--gmm-train true}  --gmmtrain-opts "$gmmtrain_opts"  \
  ${step04:+ --gmm-test true} \
  $rdir/data/lang  $rdir
  echo "$0: run_llp ended @ `date`"
fi
if $make_fbank_pitch; then
  echo "$0: make_fbank_pitch started @ `date`"
  NF=$(echo "$make_fbank_pitch_opts"|awk -F ':' '{print NF;}')
  for x in $(seq 1 $NF); do
    cur_opts=$(echo "$make_fbank_pitch_opts"| cut -d':' -f$x)
    optNames="sdata data feat"; 
    . source/register_options.sh "$optNames" "$cur_opts" || \
    { echo "$0: make_fbank_pitch: ERROR, register_options.sh for $cur_opts"; exit 1; }
    # [ -f $sdata/feats.scp ] || \
    # { echo "$0: make_fbank_pitch: ERROR, feats.scp expected in $sdata"; exit 1; }
    [ -d $data ] || mkdir -p $data
    cp $sdata/* $data/;   rm $data/{feats.scp,cmvn.scp} 2>/dev/null
    $fbankCmd --cmd "$cmd" --nj $nj $data $feat/log $feat/data  || exit 1
    steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1
    utils/fix_data_dir.sh $data
  done
  echo "$0: make_fbank_pitch ended @ `date`"
fi
if $hierbn_cross_tune; then
  echo "$0: hierbn_cross_tune started @ `date`"
  optNames="validating_rate sdir feature_transform learn_rate nnet_sdir lang traindata alidir dir"
  . source/register_options.sh "$optNames" "$hierbn_cross_tune_opts" || \
  { echo "$0: hierbn_cross_tune: ERROR, @register_options.sh"; exit 1; }
  source/run-nnet.sh --cmd "$cmd" --nj $nj --validating-rate $validating_rate \
  --sdir $sdir  --train-fbank-data $traindata --labeldir $alidir \
  ${step01:+--PrepareData true} \
  ${step02:+--tune-bn true --tunebn_feature_transform $feature_transform --tune_learn_rate $learn_rate --nnet-srcdir $nnet_sdir } \
  $lang alidata $dir || exit 1
  echo "$0: hierbn_cross_tune ended @ `date`" 
fi
if $make_bnf2; then
  echo "$0: make_bnf2 started @ `date`"
  NF=$(echo "$make_bnf2_opts" |awk -F ':' '{print NF;}')
  for x in $(seq 1 $NF); do
    cur_opts=$(echo "$make_bnf2_opts"| cut -d':' -f$x)
    . source/register_options.sh "sdata dnn_sdir data feat" "$cur_opts" || \
    { echo "$0: make_bnf2: ERROR, register_options.sh failed for $cur_opts"; exit 1; }
    [ -f $sdata/feats.scp ] || \
    { echo "$0: make_bnf2: ERROR, feats.scp expected in $sdata"; exit 1;  }
    [ -f $dnn_sdir/final.nnet ] || \
    { echo "$0: make_bnf2: ERROR, final.nnet expected in $dnn_sdir"; exit 1; }
    steps/nnet/make_bn_feats.sh --cmd "$cmd" --nj $nj $data $sdata $dnn_sdir $feat/log $feat/data || exit 1
    steps/compute_cmvn_stats.sh $data $feat/log $feat/data || exit 1
    utils/fix_data_dir.sh $data
  done
  echo "$0: make_bnf2 ended @ `date`"
fi
if $make_plp; then
  echo "$0: make_plp started @ `date`"
  optNames="conf sdata data feat"
  . source/register_options.sh "$optNames" "$make_plp_opts" || \
  { echo "$0: make_plp: ERROR for register_options on $make_plp_opts"; exit 1; }
  [ -d $data ] || mkdir -p $data
  cp $sdata/*  $data/  2>/dev/null;  rm $data/{feats.scp,cmvn.scp} 2>/dev/null
  steps/make_plp.sh --plp-config $conf $data $feat/log $feat/data || exit 1
  steps/compute_cmvn_stats.sh $data $feat/log  $feat/data || exit 1
  utils/fix_data_dir.sh $data
  echo "$0: make_plp ended @ `date`"
fi
if $make_plp_pitch; then
  echo "$0: make_plp_pitch started @ `date`"
  optNames="plp_conf pitch_conf sdata data feat"
  . source/register_options.sh "$optNames" "$make_plp_pitch_opts" || \
  { echo "$0: make_plp_pitch: ERROR for register_options on $make_plp_pitch_opts"; exit 1; }
  [ -d $data ] || mkdir -p $data
  cp $sdata/*  $data/  2>/dev/null;  rm $data/{feats.scp,cmvn.scp} 2>/dev/null
  steps/make_plp_pitch.sh --plp-config $plp_conf --pitch-config $pitch_conf \
  $data $feat/log $feat/data || exit 1
  steps/compute_cmvn_stats.sh $data $feat/log  $feat/data || exit 1
  utils/fix_data_dir.sh $data
  echo "$0: make_plp_pitch ended @ `date`"
fi
if $make_mfcc_pitch; then
  echo "$0: make_mfcc_pitch started @ `date`"
  optNames="mfcc_conf pitch_conf sdata data feat"
  . source/register_options.sh "$optNames" "$make_mfcc_pitch_opts" || \
  { echo "$0: make_mfcc_pitch: ERROR for register_options on $make_mfcc_pitch_opts"; exit 1; }
  [ -d $data ] || mkdir -p $data
  cp $sdata/*  $data/  2>/dev/null;  rm $data/{feats.scp,cmvn.scp} 2>/dev/null
  steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj --mfcc-config $mfcc_conf --pitch-config $pitch_conf \
  $data $feat/log $feat/data || exit 1
  steps/compute_cmvn_stats.sh $data $feat/log  $feat/data || exit 1
  utils/fix_data_dir.sh $data
  echo "$0: make_mfcc_pitch ended @ `date`"
fi

if $make_fmllr; then
  echo "$0: make_fmllr started @ `date`"
  optNames="sdata sdir transform_dir data feat"
  . source/register_options.sh "$optNames" "$make_fmllr_opts" || \
  { echo "$0: make_fmllr: ERROR for register_options on $make_fmllr_opts"; exit 1; }
  if [ ! -f $transform_dir/trans.1 ]; then
    transform_dir=
  else
    nj=$(ls $transform_dir/trans.*|grep trans| wc -l)
  fi
  steps/nnet/make_fmllr_feats.sh --cmd "$cmd" --nj $nj \
  ${transform_dir:+--transform-dir $transform_dir} \
  $data $sdata $sdir   $feat/log $feat/data || exit 1
  steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1
  utils/fix_data_dir.sh $data
  echo "$0: make_fmllr ended @ `date`"
fi
if $append_delta; then
  echo "$0: append_delta started @ `date`"
  optNames="sdata data feat"
  . source/register_options.sh "$optNames" "$append_delta_opts" || \
  { echo "$0: append_delta: ERROR for register_options.sh"; exit 1; }
  [ -d $data ] || mkdir -p $data
  cp $sdata/*  $data/; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
  [ -d $feat/data ] || mkdir -p $feat/data
  copy-feats "ark:add-deltas --delta-order=2 scp:$sdata/feats.scp ark:- |" ark,scp:$feat/data/feats.ark,$data/feats.scp || \
  { echo "$0: append_delta: ERROR, copy-feats failed"; exit 1; }
  steps/compute_cmvn_stats.sh $data $feat/log $feat/data
  utils/fix_data_dir.sh $data
  echo "$0: append_delta ended @ `date`"
fi
if $paste_feats; then
  echo "$0: paste_feats started @ `date`"
  NF=$(echo "$paste_feats_opts"|awk -F ':' '{print NF;}')
  for x in $(seq 1 $NF); do
    optNames="data1 data2 data feat"
    cur_opts=$(echo "$paste_feats_opts"| cut -d':' -f$x)
    . source/register_options.sh "$optNames" "$cur_opts" || \
    { echo "$0: paste_feats: ERROR for register_options on $paste_feats_opts"; exit 1; }
    steps/paste_feats.sh --nj $nj $data1 $data2 $data $feat/log $feat/data || exit 1
    steps/compute_cmvn_stats.sh $data $feat/log  $feat/data || exit 1
    utils/fix_data_dir.sh $data
  done
  echo "$0: paste_feats ended @ `date`"
fi
if $make_ali; then
  echo "$0: make_ali started @ `date`"
  optNames="data lang sdir alidir"
  . source/register_options.sh "$optNames" "$make_ali_opts" || \
  { echo "$0: make_ali: ERROR, @register_options.sh"; exit 1; }
  $ali_cmd --cmd "$cmd" $data $lang $sdir $alidir || \
  { echo "$0: make_ali: ERROR, make_ali failed"; exit 1; } 
  echo "$0: make_ali ended @ `date`"
fi
if $make_denlats; then
  echo "$0: make_denlats started @ `date`"
  optNames="acwt conf data lang sdir latdir"
  . source/register_options.sh "$optNames" "$make_denlats_opts" || \
  { echo "$0: make_denlats: ERROR, @register_options.sh"; exit 1;}
  steps/nnet/make_denlats.sh --nj $nj --cmd "$cmd" \
  --acwt $acwt --config $conf $data $lang $sdir $latdir || exit 1 
  echo "$0: make_denlats ended @ `date`"
fi
if $train_nnet_mpe; then
  echo "$0: train_nnet_mpe started @ `date`"
  optNames="acwt data lang sdir alidir latdir dir"
  . source/register_options.sh "$optNames" "$train_nnet_mpe_opts" || \
  { echo "$0: train_nnet_mpe: ERROR, @register_options.sh"; exit 1; }
  steps/nnet/train_mpe.sh --cmd "slurm.pl --gres=gpu:1" --acwt $acwt --do-smbr true \
  $data $lang $sdir $alidir $latdir $dir || exit 1
  echo "$0: train_nnet_mpe ended @ `date`"
fi
if $run_tandem_nnet; then
  echo "$0: run_tandem_nnet started @ `date`"
  optNames="train_data train_feat validating_feat sdir lang alidata dir trnCmd learn_rate"
  . source/register_options.sh "$optNames" "$tandem_nnet_opts" || \
  { echo "$0: run_tandem_nnet: ERROR @register_options.sh"; exit 1; }
  raw_data="$train_data $train_feat $validating_feat"
  source/run-nnet.sh --use_raw_data true --raw_data "$raw_data"  \
  ${step01:+--PrepareData true} \
  ${step02:+--DoAli true} \
  ${step03:+--preTrnCmd "$preTrnCmd" --PreTrain true} \
  ${step04:+--trnCmd "$trnCmd" --TrainNnet true} --learn_rate $learn_rate \
  --sdir $sdir $lang $alidata $dir || exit 1
  echo "$0: run_tandem_nnet ended @ `date`"
fi
if $run_gmm2; then
  echo "$0: run_gmm2 started @ `date`" 
  optName="trainId data dataName lang states pdfs tgtdir"
  . source/register_options.sh "$optName" "$run_gmm2_opts" || \
  { echo "$0: run_gmm2: ERROR, register_option error for $run_gmm2_opts"; exit 1; }
  opts="--train_id $trainId  --train_dataname $dataName --tri_state $states --tri_pdf $pdfs"
  opts="$opts $data $lang $tgtdir"
  echo "$0: run_gmm2: opts=$opts"
  if [ ! -z $import_alidir ]; then
    step01=
    step02=
    step03=true
    step04=true
    step05=true
  fi
  if [ ! -z $run_gmm2_import_tri5_alidir ]; then
    step01=; step02=; step03=; step04=; step05=true
  fi
  source/run-gmm-hmm.sh --cmd "$cmd" --nj $nj ${use_lda_mat:+--use_lda_mat $use_lda_mat} --lda_dim $lda_dim \
  ${import_alidir:+--import_alidir $import_alidir} \
  ${cmvn_opts:+--cmvn-opts "$cmvn_opts"} \
  ${import_tri5_alidir:+--import_tri5_alidir $import_tri5_alidir} \
  ${step01:+--mono_train true} \
  ${step02:+--tri1_train true} \
  ${step03:+--tri2_train true} \
  ${step04:+--tri3_train true} \
  ${step05:+--tri5_train true} $opts || exit 1
  echo "$0: run_gmm2 ended @ `date`"
fi
if $run_pnorm; then
  echo "$0: run_pnorm started @ `date`"
  optNames="initial_learning_rate final_learning_rate input_dim output_dim samples_periter num_threads splice_width lda_dim"
  optNames="$optNames data lang ali dir"
  . source/register_options.sh "$optNames" "$pnorm_opts"  || \
  { echo "$0: run_pnorm: ERROR for register_options.sh $pnorm_opts"; exit 1; }
  opts="--initial_learning_rate $initial_learning_rate --final_learning_rate $final_learning_rate --pnorm_input_dim $input_dim"
  opts="$opts --pnorm_output_dim $output_dim --samples_per_iter $samples_periter --num_threads $num_threads "
  opts="$opts --splice_width $splice_width --lda_dim $lda_dim"
  steps/nnet2/train_pnorm_simple2.sh $opts $data $lang $ali $dir || exit 1
  echo "$0: run_pnorm ended @ `date`"
fi
if $decoding_eval; then
  echo "$0: decoding_eval started @ `date`"
  optNames="data lang sdir graph decode_dir decoding_cmd"
  . source/register_options.sh "$optNames" "$decoding_opts" || \
  { echo "$0: decoding_eval: register_options: ERROR for $decoding_opts"; exit 1; }
  for f in $data/feats.scp $lang/words.txt $sdir/final.mdl; do
   [ -f $f ] || \
   { echo "$0: decoding_eval: ERROR, file $f expected"; exit 1; }
  done
  [ -f $graph/HCLG.fst ] ||  utils/mkgraph.sh $lang $sdir $graph
  max_nj=$(wc -l < $data/spk2utt)
  [ $nj -gt $max_nj ] && nj=$max_nj
  $decoding_cmd --cmd "$cmd" --nj $nj --scoring-opts "$scoring_opts" $lattice_opts \
  $graph $data $decode_dir  || exit 1 
  echo "$0: decoding_eval ended @ `date`"
fi
if $lattice_onebest; then
  echo "$0: lattice_onebest started @ `date`"
  optNames="latdir lmwt lang dir"
  . source/register_options.sh "$optNames" "$lattice_onebest_opts" || \
  { echo "$0: lattice_onebest: ERROR, @register_options.sh"; exit 1; }
  [ -d $dir ] || mkdir -p $dir
  $cmd $dir/best_path.$lmwt.log \
  lattice-best-path --lm-scale=$lmwt --word-symbol-table=$lang/words.txt \
  "ark:gzip -cd $latdir/lat.*.gz|" "ark,t:| utils/int2sym.pl -f 2- $lang/words.txt > $dir/$lmwt.tra" || exit 1
  echo "$0: lattice_onebest ende @ `date`"
fi
if $filter_onebest; then
  echo "$0: filter_onebest started @ `date`"
  optNames="text new_text"
  . source/register_options.sh "$optNames" "$filter_onebest_opts" || \
  { echo "$0: filter_onebest: ERROR, @ register_options.sh"; exit 1; }
  dir=$(dirname $new_text)
  [ -d $dir ] || mkdir -p $dir
  cat $text | \
  source/test/openkws_filter_decoded_text.pl > $new_text
  echo "$0: filter_onebest ended @ `date`"
fi
if $score_combine; then
  echo "$0: score_combine started @ `date`"
  optNames="min_lmwt max_lmwt stm data dataname lang dir"
  . source/register_options.sh "$optNames" "$score_combine_opts" || exit 1
  name=$dataname
  if [ ! -z $step01 ]; then
    for latdir in $score_combine_latdirs; do
      model=$(dirname $latdir)/final.mdl
      [ -f $latdir/lat.1.gz ] || { echo "$0: ERROR, latdir $latdir is not ready"; exit 1; }
      [ -f $model ] || { echo "$0: ERROR, model $model expected"; exit 1; }
      inlats="ark:gzip -cd $latdir/lat.JOB.gz|"
      nj=$(cat $latdir/num_jobs)
      tgtdir=$latdir/ali_words
      outlats="ark:|gzip > $tgtdir/lat.JOB.gz"
      [ -f $lang/phones/word_boundary.int ] || { echo "$0: ERROR, word_boundary.int expected"; exit 1; }
      $cmd JOB=1:$nj $tgtdir/log/lattice-align-words.JOB.log \
      lattice-align-words $lang/phones/word_boundary.int $model "$inlats" "$outlats" 
    done
  fi
  if [ ! -z $step02 ]; then
    wlats=
    for latdir in $score_combine_latdirs; do
      wlat=$latdir/ali_words
      [ -f $wlat/lat.1.gz ] || { echo "$0: ERROR, word_align_lattice is not ready in $wlat"; exit 1; }
      wlats="$wlats \"ark:gzip -cd $wlat/lat.*.gz|\""
    done
    $cmd LMWT=$min_lmwt:$max_lmwt $dir/score_LMWT/log/combine_lats.LMWT.log \
    lattice-combine --inv-acoustic-scale=LMWT $wlats ark:- \| \
    lattice-to-ctm-conf --decode-mbr=true ark:- - \| \
    utils/int2sym.pl -f 5 $lang/words.txt  \| tee $dir/score_LMWT/$name.utt.ctm \| \
    utils/convert_ctm.pl $data/segments $data/reco2file_and_channel \
    '>' $dir/score_LMWT/$name.ctm || exit 1;
  fi
  echo "$0: score_combine ended @ `date`"
fi
if $decoding_rover; then
  echo "$0: decoding_rover started @ `date`"
  optNames="stm dir"
  . source/register_options.sh "$optNames" "$scoring_rover_opts" || exit 1
  ctms=
  comp_num=0
  for ctm in $ctm_files; do
    [ -f $ctm ] || { echo "$0: ERROR, ctm file $ctm is not ready"; exit 1;}
    ctms="$ctms -h $ctm ctm"
    comp_num=$[comp_num+1]
  done
  if [ ! -z $step01 ]; then
        [ -d $dir ] || mkdir -p $dir
    cat $ctm_files | awk '{ print $1;}'|sort -u | \
    awk '{s=tolower($1);printf("%s %s\n",s, $1);}' > $dir/tmp.map
    rover $ctms $rover_opts -o $dir/tmp.ctm >/dev/null 2>&1
    cat $dir/tmp.ctm | \
    source/egs/fix_ctm.pl $dir/tmp.map > $dir/c.${comp_num}.ctm 
    echo "$0: decoding_rover ended @ `date`"
  fi
  if [ ! -z $step02 ]; then
    sclite -s -r $stm stm -h $dir/c.${comp_num}.ctm ctm -n c.${comp_num} \
    -f 0 -D -F -o sum rsum prf dtl sgml -e utf-8
    [ -f $dir/c.${comp_num}.sys ] ||{ echo "$0: ERROR, scoring with sclite failed"; exit 1; }
    head $dir/c.${comp_num}.sys | grep SPKR -B1
    grep Sum -F1 $dir/c.${comp_num}.sys
  fi
  echo "$0: decoding_rover ended @ `date`"
fi
