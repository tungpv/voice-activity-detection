#!/bin/bash

. path.sh
. cmd.sh


# begin options
cmd=run.pl
nj=20
run_fbank_nnet=false
hierbn_train=false
labeldir=
hierbn_train_opts="<validating_rate(0.1)> <learn_rate(0.008)> <bn1_splice(5)> <hid_dim(1500)> <hid_layers(2)> <bn1_dim(80)> <remove_last_layers(4)>  <bn2_splice(2)> <bn2_dim(30)> <bn2_splice_step(5)> <fbank_data> <alidata> <lang> <sdir> <tgtdir>"
select_subdata=false
test_fbank_nnet=false
fbanktest_prepare_data=false
fbanktest_decode=false
dnn_srcdir=
dotest_data=dev
fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
preTrnCmd="source/egs/pretrain_dbn_trap.sh --splice 10 --nn-depth 6 --hid-dim 1024"
train_nnet_cmd="source/egs/train.sh --copy-feats-tmproot /local/hhx502"
dotest_dataname=dev10h
run_fmllr_nnet=false
train_fmllr_nnet=false
train_fmllr_nnet_opts="<validating_rate> <learn_rate> <lang> <sdir> <alidata> <dir>"
cmvn_opts=
fmllr_pretrain_cmd="steps/nnet/pretrain_dbn.sh --splice 5 --nn-depth 7 --hid-dim 1024"
nnet_cross_train=false
use_data_opts="--usefbankdata true --train_fbank_data <fbank_data>" 
nnet_cross_train_opts="<validating_rate> <source_hmm_dir> <source_nnet_dir> <learn_rate> <lang> <alidata> <dir>"
crossling1_transfer=false
crossling1_hid_layers=0
dnn_datasrcdir=
crossling1_do_pretrain=
tune2_hierbn=false
fbank_tune_nnet=false
tune_2bndnn=false
bn2dnn_train=false
bn2dnn_opts="<lang> <sdir> <tgtdir> <sdata_dir> <nnet_sdir> <remove_last_layers> <more_hid_layers>"
bnf2train_dnn=false
bnf2train_dnn_opts="<nnet_sdir> <remove_last_components> <hid_layers> <hid_dim> <splice> <splice_step>"
fbank_limited_tune_nnet=false
tune_opts="--tune-learn-rate 0.002 --tune-layers 1"
tune_feature_transform=
hierbn_hid_dim=1500
fbank_cross_train_nnet=false
nnet_srcdir=
decoding_eval=false
decoding_opts="<data> <lang> <sdir> <graph> <decode_dir> <decoding_cmd>"
train_fbank_nnet=false
train_fbank_nnet_opts="validating_rate learn_rate fbank_data alidata lang sdir tgtdir"
cmvn_opts="--norm-means=true"
delta_opts="--delta-order=2"
rdir=
tgt_dir=
steps=

# end options
echo
echo "$0 $@"
echo
 . parse_options.sh

function PrintOptions {
  cmdName=$(echo $0| perl -pe 's/^.*\///g;')
  cat <<END
$cmdName [options]:
cmd					# value, "$cmd"
nj					# value, "$nj"
run_fbank_nnet				# value, $run_fbank_nnet
hierbn_train				# value, $hierbn_train
labeldir				# value, "$labeldir"
hierbn_train_opts			# value, "$hierbn_train_opts"
select_subdata				# value, $select_subdata
test_fbank_nnet 			# value, $test_fbank_nnet
fbanktest_prepare_data			# value, $fbanktest_prepare_data
fbanktest_decode			# value, $fbanktest_decode
dnn_srcdir				# value, "$dnn_srcdir"
dotest_data				# value, "$dotest_data"
fbankCmd				# value, "$fbankCmd"
preTrnCmd				# value, "$preTrnCmd"
train_nnet_cmd				# value, "$train_nnet_cmd"
dotest_dataname				# value, "$dotest_dataname"
crossling1_transfer 			# value, $crossling1_transfer	
crossling1_hid_layers			# value, $crossling1_hid_layers
dnn_datasrcdir		         	# value, "$dnn_datasrcdir"
crossling1_do_pretrain			# value, "$crossling1_do_pretrain"
run_fmllr_nnet                       	# value, $run_fmllr_nnet 
train_fmllr_nnet			# value, $train_fmllr_nnet                   # updated, use this now
train_fmllr_nnet_opts			# value, "$train_fmllr_nnet_opts"
cmvn_opts				# value, "$cmvn_opts"
fmllr_pretrain_cmd			# value, "$fmllr_pretrain_cmd"
nnet_cross_train			# value, $nnet_cross_train             # updated, use this now
use_data_opts				# value, "$use_data_opts"
nnet_cross_train_opts			# value, "$nnet_cross_train_opts"
tune2_hierbn				# value, $tune2_hierbn
fbank_tune_nnet				# value, $fbank_tune_nnet
tune_2bndnn				# value, $tune_2bndnn
bn2dnn_train				# value, $bn2dnn_train
bn2dnn_opts				# value, "$bn2dnn_opts"
bnf2train_dnn				# value, $bnf2train_dnn
bnf2train_dnn_opts			# value, "$bnf2train_dnn_opts"
fbank_limited_tune_nnet			# value, $fbank_limited_tune_nnet
tune_opts 				# value, "$tune_opts"
tune_feature_transform			# value, "$tune_feature_transform"
hierbn_hid_dim				# value, $hierbn_hid_dim
fbank_cross_train_nnet			# value, $fbank_cross_train_nnet
nnet_srcdir				# value, "$nnet_srcdir"
decoding_eval				# value, $decoding_eval
decoding_opts				# value, "$decoding_opts"
train_fbank_nnet			# value, $train_fbank_nnet
train_fbank_nnet_opts			# value, "$train_fbank_nnet_opts"
cmvn_opts				# value, "$cmvn_opts"
delta_opts				# value, "$delta_opts"
rdir	                         	# value, "$rdir"
tgt_dir					# value, "$tgt_dir"
steps					# value, "$steps"
END
}

PrintOptions;
echo
echo "$0 $@"
echo

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

sdir=$rdir/exp/tri4a
rexp=$rdir/exp
rdata=$rdir/data
if $run_fbank_nnet; then
  echo "$0: run_fbank_nnet started @ `date`"
  [ ! -z $rdir ] || \
  { echo "$0: run_fbank_nnet: ERROR, rdir is not specified"; exit 1;  }
  [ ! -z $tgt_dir ] || \
  { echo "$0: run_fbank_nnet: ERROR, tgt_dir not specified"; exit 1; }
  alidata=$rdata/train/plp_pitch
  splice=10; nn_depth=5
  # preTrnCmd="isteps/nnet/pretrain_dbn_trap.sh --feat-type trap --apply-cmvn true --norm-vars true  --splice $splice --delta-order 2  --nn-depth $nn_depth  --hid-dim 2048"
  sequence_datadir=$tgt_dir/nnet.mpe.data
  sequence_tgtdir=$tgt_dir/nnet.mpe
  sequence_opts="--sequence-acwt 0.0909 --sequence-datadir $sequence_datadir  --sequence-tgtdir $sequence_tgtdir"
  steps=1,2,3,4,5
  source/test/run-nnet.sh  --cmd "$train_cmd" --nj 20  --tgt-dir $tgt_dir \
  --crossling_fbank_train true   --preTrnCmd "$preTrnCmd" \
  --sequence-opts "$sequence_opts" \
  ${disabled:+--steps 1,2,3,4,5,7,8} --steps $steps   \
  $rdata  $alidata  $rexp $sdir  || exit 1 
  # dnn_srcdir="$tgt_dir/nnet $tgt_dir/nnet.mpe"
  dnn_srcdir="$tgt_dir/nnet"
  echo "$0: run_fbank_nnet ended @ `date`"
fi

if $crossling1_transfer; then
  echo "$0: crossling1_transfer started @ `date`"
  [ ! -z $rdir ] || \
  { echo "$0: crossling1_transfer: ERROR, rdir is not specified"; exit 1;  }
  [ ! -z $tgt_dir ] || \
  { echo "$0: crossling1_transfer: ERROR, tgt_dir not specified"; exit 1; }
  steps=1,2,3,6
  if [ ! -z $dnn_datasrcdir ]; then
    mkdir -p $tgt_dir 
    cp $dnn_datasrcdir/{train_fbank,train_feat,train_label,validating_fbank,validating_fbank,validating_feat,validating_label}  $tgt_dir/
   steps=6
  fi
  alidata=$rdata/train/plp_pitch
  splice=10
  nn_depth=5
  # preTrnCmd="isteps/nnet/pretrain_dbn_trap.sh --feat-type trap --apply-cmvn true --norm-vars true  --splice $splice --delta-order 0  --nn-depth $nn_depth  --hid-dim 2048"
  mling_dnn_dir=../viet107/llp/exp/mling.flp4/nnet
  transfer_opts="${crossling1_do_pretrain:+--do-pretrain} --learn-rate 0.008 --hid-dim 2048 --remove-last-layers 2 --hid-layers $crossling1_hid_layers"
  transfer_opts="$transfer_opts --mling-dnn-dir $mling_dnn_dir"
  source/test/run-nnet.sh  --tgt-dir $tgt_dir \
  --crossling_fbank_train true   --preTrnCmd "$preTrnCmd" \
  --fbankCmd "$fbankCmd" \
  --transfer-opts "$transfer_opts" \
  --steps $steps \
  --prepare-fbank-test false \
  --crossling_fbank_test false \
  $rdata  $alidata  $rexp $rexp/tri4a  || exit 1 
  dnn_srcdir="$dnn_srcdir $tgt_dir/";
  echo "$0: crossling1_transfer: done @ `date`"
fi
if $fbank_cross_train_nnet; then
  echo "$0: fbank_cross_train_nnet started @ `date`"
  [ ! -z $rdir ] || \
  { echo "$0: fbank_cross_train_nnet: ERROR, rdir is not specified"; exit 1;  }
  [ ! -z $tgt_dir ] || \
  { echo "$0: fbank_cross_train_nnet: ERROR, tgt_dir not specified"; exit 1; }
  [ ! -f $nnet_srcdir/final.nnet ] && \
  { echo "$0: fbank_cross_train_nnet: ERROR, final.nnet expected in $nnet_srcdir "; exit 1; }
  steps=1,2,3,4
  if [ ! -z $dnn_datasrcdir ]; then
    mkdir -p $tgt_dir 
    cp $dnn_datasrcdir/{train_fbank,train_feat,train_label,validating_fbank,validating_feat,validating_label}  $tgt_dir/
   steps=4
  fi
  alidata=$rdata/train/plp_pitch
  source/test/run-nnet.sh \
  --fbank-cross-train-nnet true --tgt-dir $tgt_dir \
  --fbankCmd "$fbankCmd" \
  --nnet-srcdir $nnet_srcdir \
  --steps $steps \
   $rdata $alidata $rexp $rexp/tri4a || exit 1
   dnn_srcdir="$dnn_srcdir $tgt_dir/"
  echo "$0: fbank_cross_train_nnet ended @ `date`"
fi
if $nnet_cross_train; then
  echo "$0: nnet_cross_train started @ `date`"
  optNames="validating_rate sdir nnet_sdir learn_rate lang alidata dir"
  . source/register_options.sh "$optNames" "$nnet_cross_train_opts" || \
  { echo "$0: nnet_cross_train: ERROR for register_options.sh"; exit 1; }
  
  source/run-nnet.sh --cmd "$cmd" --nj $nj --learn-rate $learn_rate \
  --validating-rate $validating_rate $use_data_opts  --sdir $sdir  \
  ${labeldir:+--labeldir $labeldir} \
  ${step01:+--PrepareData true} \
  ${step02:+--MakeFBankFeat true --fbankCmd "$fbankCmd" } \
  ${step03:+--MakefMLLRFeat true } \
  ${step04:+--DoAli true} \
  ${step05:+--cross-train-nnet true --nnet-srcdir $nnet_sdir } \
  ${step06:+--cross-train-nnet-full true --nnet-srcdir $nnet_sdir } \
  $lang $alidata $dir || exit 1
  echo "$0: nnet_cross_train ended @ `date`"
fi
if $fbank_tune_nnet; then
  echo "$0: fbank_tune_nnet started @ `date`"
  [ ! -z $rdir ] || \
  { echo "$0: fbank_tune_nnet: ERROR, rdir is not specified"; exit 1;  }
  [ ! -z $tgt_dir ] || \
  { echo "$0: fbank_tune_nnet: ERROR, tgt_dir not specified"; exit 1; }
  [ ! -f $nnet_srcdir/final.nnet ] && \
  { echo "$0: fbank_tune_nnet: ERROR, final.nnet not in $nnet_srcdir"; exit 1; }
  alidata=$rdata/train/plp_pitch
  source/test/run-nnet.sh --tgt-dir $tgt_dir \
  --fbank-tune-nnet true --tgt-dir $tgt_dir \
  --fbankCmd "$fbankCmd" \
  --nnet-srcdir $nnet_srcdir $tune_opts \
  --steps 1 \
   $rdata $alidata $rexp $rexp/tri4a || exit 1
   dnn_srcdir="$dnn_srcdir $tgt_dir/"
  echo "$0: fbank_tune_nnet ended @ `date`"
fi

if $fbank_limited_tune_nnet; then
  echo "$0: fbank_limited_tune_nnet started @ `date`"
  [ ! -z $rdir ] || \
  { echo "$0: fbank_limited_tune_nnet: ERROR, rdir is not specified"; exit 1;  }
  [ ! -z $tgt_dir ] || \
  { echo "$0: fbank_limited_tune_nnet: ERROR, tgt_dir not specified"; exit 1; }
  [ ! -f $nnet_srcdir/final.nnet ] && \
  { echo "$0: fbank_limited_tune_nnet: ERROR, final.nnet not in $nnet_srcdir"; exit 1; }
  alidata=$rdata/train/plp_pitch
  source/test/run-nnet.sh --tgt-dir $tgt_dir \
  --fbank-limited-tune-nnet true --tgt-dir $tgt_dir \
  --fbankCmd "$fbankCmd" \
  --nnet-srcdir $nnet_srcdir $tune_opts \
  --steps 1 \
   $rdata $alidata $rexp $rexp/tri4a || exit 1
   dnn_srcdir="$dnn_srcdir $tgt_dir/"
  echo "$0: fbank_limited_tune_nnet ended @ `date`"
fi
if $tune_2bndnn; then
  echo "$0: tune_2bndnn started @ `date`"
  lang=$(echo "$bn2dnn_opts"| awk '{print $1;}')
  [ ! -z $lang  ] && [ -f $lang/words.txt ] || \
  { echo "$0: tune_2bndnn: ERROR, lang ($lang) is not ready"; exit 1; }
  sdir=$(echo "$bn2dnn_opts"| awk '{print $2;}')
  [ ! -z $sdir ] && [ -f $sdir/final.mdl ] || \
  { echo "$0: tune2bndnn: ERROR, sdir($sdir) is not ready"; exit 1; } 
  tgtdir=$(echo "$bn2dnn_opts"| awk '{print $3;}')
  [ ! -z $tgtdir ] || \
  { echo "$0: tune2bndnn: ERROR, tgtdir($tgtdir) not specifiec"; exit 1; }
  [ -d $tgtdir ] || mkdir -p $tgtdir
  sdata_dir=$(echo "$bn2dnn_opts"| awk '{print $4;}')
  [ ! -z $sdata_dir ] && [ -f $sdata_dir/train_feat ] || \
  { echo "$0: tune2bndnn: ERROR, sdata_dir($sdata_dir) is not ready"; exit 1; }
  opts=$(echo "$bn2dnn_opts"| cut -d' ' -f5-)
  cp $sdata_dir/{train_fbank,train_feat,train_label,validating_fbank,validating_feat,validating_label}  $tgtdir/
  source/run-nnet.sh --cmd "$cmd" --usefbankdata true --sdir $sdir  --tune_2bndnn true  \
  --bn2dnn_opts "$opts" \
  --bn2dnn_pretrain_cmd "steps/nnet/pretrain_dbn.sh --rbm-iter 1 --splice 5" \
  --bn2dnn_train_cmd "steps/nnet/train.sh --learn-rate 0.008" \
  $lang alidata $tgtdir || exit 1
  echo "$0: tune_2bndnn ended @ `date`"
fi
if $bn2dnn_train; then
  echo "$0: bn2dnn_train started @ `date`"
  lang=$(echo "$bn2dnn_opts"| awk '{print $1;}')
  [ ! -z $lang  ] && [ -f $lang/words.txt ] || \
  { echo "$0: bn2dnn_train: ERROR, lang ($lang) is not ready"; exit 1; }
  sdir=$(echo "$bn2dnn_opts"| awk '{print $2;}')
  [ ! -z $sdir ] && [ -f $sdir/final.mdl ] || \
  { echo "$0: bn2dnn_train: ERROR, sdir($sdir) is not ready"; exit 1; } 
  tgtdir=$(echo "$bn2dnn_opts"| awk '{print $3;}')
  [ ! -z $tgtdir ] || \
  { echo "$0: bn2dnn_train: ERROR, tgtdir($tgtdir) not specifiec"; exit 1; }
  [ -d $tgtdir ] || mkdir -p $tgtdir
  sdata_dir=$(echo "$bn2dnn_opts"| awk '{print $4;}')
  [ ! -z $sdata_dir ] && [ -f $sdata_dir/train_feat ] || \
  { echo "$0: bn2dnn_train: ERROR, sdata_dir($sdata_dir) is not ready"; exit 1; }
  opts=$(echo "$bn2dnn_opts"| cut -d' ' -f5-)
  cp $sdata_dir/{train_fbank,train_feat,train_label,validating_fbank,validating_feat,validating_label}  $tgtdir/
  source/run-nnet.sh --cmd "$cmd" --usefbankdata true --sdir $sdir  --bn2dnn_train true  \
  --bn2dnn_opts "$opts" \
  --bn2dnn_pretrain_cmd "steps/nnet/pretrain_dbn.sh --rbm-iter 1 --splice 5" \
  --bn2dnn_train_cmd "steps/nnet/train.sh --learn-rate 0.008" \
  $lang alidata $tgtdir || exit 1
  echo "$0: bn2dnn_train ended @ `date`"
fi
if $bnf2train_dnn; then
  echo "$0: bnf2train_dnn started @ `date`"
  lang=$(echo "$bn2dnn_opts"| awk '{print $1;}')
  [ ! -z $lang  ] && [ -f $lang/words.txt ] || \
  { echo "$0: bnf2train_dnn: ERROR, lang ($lang) is not ready"; exit 1; }
  sdir=$(echo "$bn2dnn_opts"| awk '{print $2;}')
  [ ! -z $sdir ] && [ -f $sdir/final.mdl ] || \
  { echo "$0: bnf2train_dnn: ERROR, sdir($sdir) is not ready"; exit 1; } 
  tgtdir=$(echo "$bn2dnn_opts"| awk '{print $3;}')
  [ ! -z $tgtdir ] || \
  { echo "$0: bnf2train_dnn: ERROR, tgtdir($tgtdir) not specifiec"; exit 1; }
  [ -d $tgtdir ] || mkdir -p $tgtdir
  sdata_dir=$(echo "$bn2dnn_opts"| awk '{print $4;}')
  [ ! -z $sdata_dir ] && [ -f $sdata_dir/train_feat ] || \
  { echo "$0: bnf2train_dnn: ERROR, sdata_dir($sdata_dir) is not ready"; exit 1; }
  cp $sdata_dir/{train_fbank,train_feat,train_label,validating_fbank,validating_feat,validating_label}  $tgtdir/
  source/run-nnet.sh --cmd "$cmd" --usefbankdata true --sdir $sdir  --bnf2train_dnn true  \
  --bnf2train_dnn_opts "$bnf2train_dnn_opts" \
  --bn2dnn_pretrain_cmd "steps/nnet/pretrain_dbn.sh --rbm-iter 1 --splice 5" \
  --bn2dnn_train_cmd "steps/nnet/train.sh --learn-rate 0.008" \
  $lang alidata $tgtdir || exit 1
  echo "$0: bnf2train_dnn ended @ `date`"
fi
if $select_subdata; then
  echo "$0: dev_select started @ `date`"
  data=manual_vllp/data/dev/plp_pitch
  factor=0.3
  subdata=$(dirname $data)/plp_pitchf0.3
  [ -d $subdata ] || mkdir -p $subdata
  iutils/mksubset_spklist.pl $factor $data/spk2utt "feat-to-len scp:$data/feats.scp ark,t:- |"  >$subdata/spklist || exit 1
  utils/subset_data_dir.sh --spk-list $subdata/spklist $data $subdata || exit 1
  utils/fix_data_dir.sh $subdata
fi
if $test_fbank_nnet; then
  echo "$0: test_fbank_nnet started @ `date`"
  if $fbanktest_prepare_data; then
    echo "$0: fbanktest_prepare_data started @ `date`"
    [ ! -z $dotest_data ] || \
    { echo "$0: fbanktest_prepare_data: ERROR, dotest_data is not specified"; exit 1; }
    sdata=$dotest_data
    data=$(dirname $sdata)/fbank_pitch
    feat=$rexp/feature/fbank_pitch/$dotest_dataname
    [ -d $data ] || mkdir -p $data 
    cp $sdata/* $data/; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
    $fbankCmd --cmd "$cmd" --nj $nj $data $feat/log $feat/data  || exit 1
    steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1
    utils/fix_data_dir.sh $data
  fi
  for dnn_dir in $dnn_srcdir; do
    if $fbanktest_decode; then
      [ -f $dnn_dir/final.nnet ] || \
      { echo "$0: fbanktest_decode: ERROR, final.nnet expected in $dnn_dir"; exit 1; }
      echo "$0: fbanktest_decode started @ `date`"
      graph=$sdir/graph
      [ ! -z $dotest_data ] || \
      { echo "$0: fbanktest_decode: ERROR, dotest_data is not specified @"; exit 1; }
      decode_dir=$dnn_dir/decode_${dotest_dataname}
      data=$(dirname $dotest_data)/fbank_pitch
      steps/nnet/decode.sh --cmd "$cmd" --nj $nj $graph $data $decode_dir || exit 1  &
    fi
  done
fi
if $hierbn_train; then
  echo "$0: hierbn_train started @ `date`"
  optNames="validating_rate learn_rate bn1_splice hid_dim hid_layers bn1_dim remove_last_layers bn2_splice bn2_dim bn2_splice_step fbank_data alidata lang sdir tgtdir"
  . source/register_options.sh "$optNames" "$hierbn_train_opts" || exit 1
  [ ! -z $lang ] && [ -f $lang/words.txt ] || \
  { echo "$0: hierbn_train: ERROR, lang($lang) not ready"; exit 1; }
  [ ! -z $sdir ] && [ -f $sdir/final.mdl ] || \
  { echo "$0: hierbn_train: ERROR, sdir($sdir) not ready"; exit 1; }
  hierbn_opts="--hid-dim $hid_dim --hid-layers $hid_layers --learn-rate $learn_rate"
  hierbn1_opts="$hierbn_opts"
  hierbn1_opts="$hierbn1_opts --bn-dim $bn1_dim --feat-type traps --splice $bn1_splice "
  hierbn1_cmd="$train_nnet_cmd $hierbn1_opts"
  hierbn_splice_cmd="utils/nnet/gen_splice.py --fea-dim=$bn1_dim --splice=$bn2_splice --splice-step=$bn2_splice_step |"
  hierbn_copy_cmd="nnet-copy --remove-last-layers=$remove_last_layers --binary=false"
  hierbn2_opts="$hierbn_opts --bn-dim $bn2_dim "
  hierbn2_cmd="$train_nnet_cmd $hierbn2_opts"
  [ ! -z $labeldir ] || \
  { echo "$0: hierbn_train: ERROR, labeldir expected"; exit 1;}
  [ -f $labeldir/ali.1.gz ] || \
  { echo "$0: hierbn_train: ERROR, ali.1.gz expected from $labeldir"; exit 1; }
  [ -d $tgtdir ] || mkdir -p $tgtdir
  source/run-nnet.sh --cmd "$cmd" --nj $nj --validating-rate $validating_rate \
  --train_fbank_data $fbank_data \
  --labeldir $labeldir \
  ${cmvn_opts:+--cmvn_opts "$cmvn_opts"} \
  ${delta_opts:+--delta_opts "$delta_opts"} \
  --sdir $sdir  --usefbankdata true \
  ${step01:+--PrepareData true} \
  ${step02:+--hierbn1-train true --hierbn1_cmd "$hierbn1_cmd"} \
  ${step03:+--hierbn-feature-transform true --hierbn-splice-cmd "$hierbn_splice_cmd" --hierbn-copy-cmd "$hierbn_copy_cmd" } \
  ${step04:+--hierbn2-train true --hierbn2-cmd "$hierbn2_cmd"} \
  $lang $alidata $tgtdir || exit 1
  echo "$0: hierbn_train ended @ `date`"
fi
if $tune2_hierbn; then
  echo "$0: tune2_hierbn started @ `date`"
  [ -z $tgt_dir ] && \
  { echo "$0: tune2_hierbn: ERROR, tgt_dir expected"; exit 1; }
  steps=1,2,3,4
  if [ ! -z $dnn_datasrcdir ]; then
    mkdir -p $tgt_dir
    cp $dnn_datasrcdir/{train_fbank,train_feat,train_label,validating_fbank,validating_fbank,validating_feat,validating_label}  $tgt_dir/
    steps=4
  fi

  lang=$rdata/lang
  alidata=$rdata/train/plp_pitch
  source/test/run-nnet.sh --cmd "$cmd" --nj $nj \
  --tune2-hierbn true \
  --steps $steps  --tgt-dir $tgt_dir \
  $tune_opts \
  --tunebn-feature-transform $tune_feature_transform \
  $rdata $alidata  $rexp $sdir || exit 1
  echo "$0: tune2_hierbn ended @ `date`"
fi
if $train_fmllr_nnet; then
  echo "$0: train_fmllr_nnet started @ `date`"
  optNames="validating_rate learn_rate lang sdir alidata dir"
  . source/register_options.sh "$optNames" "$train_fmllr_nnet_opts" || \
  { echo "$0: train_fmllr_nnet: ERROR, @register_options.sh"; exit 1; }
  [ -d $dir ] || mkdir -p $dir 
  source/run-nnet.sh --validating-rate $validating_rate --learn_rate $learn_rate\
  --usefmllrdata true  --preTrnCmd "$fmllr_pretrain_cmd" ${cmvn_opts:+--cmvn_opts "$cmvn_opts"} \
  ${labeldir:+--labeldir $labeldir} \
  ${step01:+--PrepareData true} \
  ${step02:+--MakefMLLRFeat true} \
  ${step03:+--DoAli true} \
  ${step04:+--PreTrain true } \
  ${step05:+--TrainNnet true} --trnCmd "$train_nnet_cmd" --sdir $sdir\
  $lang $alidata $dir || exit 1
  echo "$0: train_fmllr_nnet ended @ `date`"
fi
if $run_fmllr_nnet; then
  echo "$0: run_fmllr_nnet started @ `date`"
  [ ! -z $rdir ] || \
  { echo "$0: run_fmllr_nnet: ERROR, rdir is not specified"; exit 1;  }
  tgt_dir=$rexp/nnet.fmllr
  alidata=$rdata/train/plp_pitch
  splice=5
  nn_depth=5
  doit=true
  preTrnCmd="steps/nnet/pretrain_dbn.sh --apply-cmvn true --splice $splice --nn-depth $nn_depth  --hid-dim 2048 --copy_feats_tmproot $tgt_dir "
 source/test/run-nnet.sh  --tgt-dir $tgt_dir \
  ${doit:+--crossling_fmllr_train true   --preTrnCmd "$preTrnCmd" --steps 1,2,3,4,5 --prepare-fmllr-test true} \
  --crossling_fmllr_test true --cmd "run.pl" \
  $rdata  $alidata  $rexp $rexp/tri4a  || exit 1 

fi
if $decoding_eval; then
  echo "$0: decoding_eval started @ `date`"
  optNames="data lang sdir graph decode_dir decoding_cmd"
  . source/register_options.sh "$optNames" "$decoding_opts" || \
  { echo "$0: decoding_eval: ERROR, for register_options.sh"; exit 1; }
  [ -f $data/feats.scp ] || \
  { echo "$0: decoding_eval: ERROR, file $data/feats.scp expected "; exit 1; }
  [ -f $graph/HCLG.fst ] || utils/mkgraph.sh $lang $sdir $graph
  if [ ! -z $decoding_cmd ]; then
    $decoding_cmd --cmd "$cmd" --nj $nj $graph $data $decode_dir  || exit 1 
  fi
  echo "$0: decoding_eval ended @ `date`"
fi

# dotest_opts="--fbanktest-prepare-data true --fbanktest-decode true"
# dotest_opts="$dotest_opts  --dotest-data $dotest_data --dotest-dataname $dotest_dataname --dotest-graph $dotest_graph"

if $train_fbank_nnet; then
  echo "$0: train_fbank_nnet started @ `date`"
  optNames="validating_rate learn_rate fbank_data alidata lang sdir tgtdir"
  . source/register_options.sh "$optNames" "$train_fbank_nnet_opts" || exit 1
  [ -f $fbank_data/feats.scp ] || \
  { echo "$0: train_fbank_nnet: ERROR, feats.scp expected in $fbank_data"; exit 1; }
  source/run-nnet.sh --validating_rate $validating_rate --learn_rate $learn_rate \
  --train_fbank_data $fbank_data \
  ${labeldir:+--labeldir $labeldir} \
  ${cmvn_opts:+--cmvn_opts "$cmvn_opts"} \
  ${delta_opts:+--delta_opts "$delta_opts"} \
  ${step01:+--PrepareData true} \
  ${step02:+--DoAli true} \
  ${step03:+--PreTrain true} --preTrnCmd "$preTrnCmd" \
  ${step04:+--TrainNnet true} --trnCmd "$train_nnet_cmd" --sdir $sdir \
  $lang $alidata $tgtdir || exit 1
  
  echo "$0: train_fbank_nnet ended @ `date`"
fi
