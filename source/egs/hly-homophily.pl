#!/usr/bin/perl
use strict;
use warnings;
# use open qw(:std :utf8);
use Data::Dumper qw(Dumper);

print STDERR "$0: stdin expected\n";
my %sameCountVocab = ();
my %diffCountVocab = ();
my %nodeVocab = ();
my %vocab = ();
my %authorVocab = ();
while(<STDIN>) {
  chomp;
  my @A = split(/[\t]/);
  # @A == 4 or die;
  $nodeVocab{$A[2]}{$A[0]} ++;
  $nodeVocab{$A[3]}{$A[1]} ++;
  $authorVocab{$A[0]} ++;
  $authorVocab{$A[1]}++;

  if("$A[2]" eq "$A[3]") {
    $sameCountVocab{$A[2]} ++;
  } else {
    $diffCountVocab{$A[2]}{$A[3]} ++;
    $diffCountVocab{$A[3]}{$A[2]} ++;
  }
}
print STDERR "$0: stdin ended\n";

my $nodeNum = 0;
foreach my $country (keys %nodeVocab) {
  my $curNodeNum = keys %{$nodeVocab{$country}};
  $nodeNum += $curNodeNum;
  # print STDERR "$country", "     $curNodeNum\n";
  print STDERR "$country\t $curNodeNum\n";
}
print STDERR "nodeNum=$nodeNum\n";
my $authorNum = keys %authorVocab;
print STDERR "authors=",$authorNum , "\n";
print Dumper (\%nodeVocab), "\n";
