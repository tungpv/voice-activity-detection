#!/usr/bin/perl
use warnings;
use strict;

if (@ARGV != 1) {
  die "\nExample: cat lexicon.txt | $0 vowel.txt\n\n"
}
my ($sFile) = @ARGV;
open(F, "$sFile") or die "$0: file $sFile cannot open\n";
my %vocab = ();
while(<F>) {
  chomp;
  s/\s+//g;
  $vocab{$_} ++;
}
close F;
sub CountVowelNum {
  my($ap, $vocab) = @_;
  my $num = 0;
  for(my $i = 0; $i < @$ap; $i ++) {
    my $sPhone = $$ap[$i];
    $num ++ if exists $$vocab{$sPhone};
  }
  return $num;
}
while(<STDIN>) {
  chomp;
  my @A = split(" ");
  my $w = shift @A;
  my $tot_num = scalar @A;
  my $vowel_num = CountVowelNum(\@A, \%vocab); 
  print "$w\t[ ",$tot_num - $vowel_num, " ", $vowel_num, " ]\n" ;
}
