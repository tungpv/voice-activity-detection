#!/bin/bash

. path.sh
. cmd.sh 

echo
echo "$0 $@"
echo 

# begin options
steps=
# end options

. parse_options.sh || exit 1


function Usage {
 cat<<END
 $0 [options] <sph-data-dir> <trans-data-dir> <tgtdir>
 [options]:
 [steps]:
 [examples]:

 $0 --steps 1 /data/users/hhx502/ldc-cts2016/larabicqt-ldc2006s29 \
 /data/users/hhx502/ldc-cts2016/larabicqt-set5-ldc2006t07  \
 /home2/hhx502/ldc-cts2016/levantine-arabic

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

sph_data_dir=$1
trans_data_dir=$2
tgtdir=$3


if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

localdir=$trans_data_dir/data/local/data-tmp
if [ ! -z $step01 ]; then
  echo "## LOG: step01, data preparation @ `date`"
  [ -d $localdir ] || mkdir -p $localdir
  sph2pipe=/opt/kaldi_updated/trunk/tools/sph2pipe_v2.5/sph2pipe
  find $sph_data_dir -name "*.sph"  > $localdir/overall-sph.flist
  find $trans_data_dir -name "*.txt" > $localdir/overall-trans.flist
  cat $localdir/overall-sph.flist | \
  perl -e '
    ($sph2pipe, $transflist, $wavscp, $tgtdir) = @ARGV;
    open(WAVSCP, "$wavscp") or die "## ERROR: wavscp $wavscp cannot open\n";
    while(<STDIN>) {
      chomp;
      $sph = $_;
      m/([^\/]+)\.sph$/ || die "## LOG: bad line $_\n"; $lab = $1;
      $lab_a = $lab . "-A";
      $lab_b = $lab . "-B";
      print WAVSCP "$lab_a $sph2pipe -f wav -p -c 1 $sph|\n";
      print WAVSCP "$lab_b $sph2pipe -f wav -p -c 2 $sph|\n";
    }
    close WAVSCP;
    open(TRANSLIST, "$transflist") or die "## ERROR: transflist $transflist cannot open\n";
    open(SEG, ">", "$tgtdir/segments") or die "## ERROR: segments $tgtdir/segments cannot open \n";
    open(UTT2SPK, ">", "$tgtdir/utt2spk") or die "## ERROR: utt2spk $tgtdir/utt2spk cannot open\n";
    open (TXT, ">", "$tgtdir/text") or die "## ERROR: text $tgtdir/text cannot open\n";
    while(<TRANSLIST>) {
      chomp;
      next if (! /\/fla_/g); $transFile = $_;
      open(TRANS, "$_") or die "## ERROR: Trans $_ cannot open\n";
      $transFile =~ m/([^\/]+)\.txt/;  $segId = $1;
      while(<TRANS>) {
        chomp;
        @A = split(/[:]/);
        next if scalar @A < 2;
        $A[0] =~ m/(\S+)\s+(\S+)\s+([A|B])/ or die "## ERROR: bad line $_ in $transFile\n";
        ($start, $end, $chanId) = ($1, $2, $3);
        next if ($start+0.1 >= $end);
        $timeIndex = sprintf("%05d-%05d", $start*100, $end*100);
        $segId_with_chanId = $segId . "-$chanId";
        $uttId = $segId_with_chanId . "-$timeIndex";
        print SEG "$uttId $segId_with_chanId $start $end \n";
        print UTT2SPK "$uttId $segId_with_chanId\n";
        $words = $A[1];
        $words =~ s:\(\((\s*)\)\):unclearwords:g;
        $words =~ s:\(\(([^\(]+)\)\):\1:g;
        $words =~ s/[[:punct:]]//g;
        $words =~ s/unclearwords/<unk>/g;
        $total = 0;  while($words =~ /\S+/g) {$total ++;}
        $nonNum = 0; while($words =~ /[<]/g) {$nonNum ++;}
        if($total == 0 || $nonNum / $total >0.75) {
          print STDERR "$uttId $words\n";
          next;
        }
        print TXT "$uttId $words\n";
        ## print STDERR "$start $end $chanId\n";
      }
      close TRANS;
    }
    close TRANSLIST; close SEG; close UTT2SPK; close TXT;
  ' $sph2pipe $localdir/overall-trans.flist  "|sort -k1,1 -u > $localdir/wav.scp" $localdir
  utils/utt2spk_to_spk2utt.pl < $localdir/utt2spk > $localdir/spk2utt
  utils/fix_data_dir.sh $localdir
  utils/validate_data_dir.sh --no-feats $localdir
  echo "## LOG: step01, done `date`"
fi
dict=$trans_data_dir/data/local/dict
if [ ! -z $step02 ]; then
  echo "## step02: get overall letter set @ `date`"
  [ -d $dict ] || mkdir -p $dict
  cat $localdir/text | \
  perl -e 'use open qw(:std :utf8); use utf8; while(<STDIN>){chomp; s/<unk>//g;  @A = split(/\s+/); shift @A;
    for($i = 0; $i< @A; $i++) { $w = $A[$i]; @B = split("", $w); for($j=0; $j<@B; $j++){
     $letter = $B[$j]; if(not exists $vocab{$letter}) { $vocab{$letter} ++; print "$letter\n"; }
    } }}' > $dict/overll-letter-set.txt
  echo "## step02, done @ `date`"
fi

if [ ! -z $step03 ]; then
  echo "## step03: make grapheme lexicon @ `date`"
  cat $localdir/text | \
  perl -e 'use open qw(:std :utf8); use utf8; ($l2p, $lex) = @ARGV; 
    open(L2P, "$l2p") or die "## ERROR: l2p $l2p cannot open\n";
    while(<L2P>) { chomp;
      m/(\S+)\s+(\S+)/ or die "## ERROR: bad line $_ in $l2p\n";
      $vocab{$1} = $2;
    } close L2P;
    open(L, ">$lex") or die "## ERROR: lex $lex cannot open\n";
    while(<STDIN>) { chomp; s/<unk>//g; @A = split(/\s+/); shift @A; 
      for($i = 0; $i < @A; $i++) { $w = $A[$i]; @B = split("", $w); $pron = ""; for($j = 0; $j < @B; $j++) {
        $letter = $B[$j]; if (not exists $vocab{$letter}) { die "## ERROR: letter $letter is unidentified\n";}
        $phone = $vocab{$letter};  $pron .= "$phone ";
      } $pron =~ s/\s+$//; $entry = "$w\t$pron"; if (not exists $v{$entry}) { $v{$entry} ++; print L "$entry\n"; }  }
    } close L; ' $dict/letter-to-phone.txt  $dict/lexicon-raw.txt
    (
      echo -e "<unk>\t<oov>" 
      echo -e "<noise>\t<ns>"
    ) | cat - $dict/lexicon-raw.txt > $dict/lexicon.txt
    cat $dict/letter-to-phone.txt | perl -ane 'm/(\S+)\s+(\S+)/; print $2, "\n";' > $dict/nonsilence_phones.txt
    echo -e "SIL\n<ns>\n<oov>"  > $dict/silence_phones.txt
    echo "SIL" > $dict/optional_silence.txt
    echo -n > $dict/extra_questions.txt
    utils/validate_dict_dir.pl $dict
  echo "## step03: done @ `date`"
fi
lang=$tgtdir/data/lang
if [ ! -z $step04 ]; then
  echo "## step04: make lang @ `date`"
  [ -d $lang ] || mkdir -p $lang
  utils/prepare_lang.sh $dict "<unk>" $lang/tmp $lang || exit 1
  echo "## step04: done @ `date`"
fi

if [ ! -z $step05 ]; then
  echo "## step05: prepare training and dev data @ `date`"
  source/egs/swahili/subset_data.sh --subset-time-ratio 0.05 --data2 $tgtdir/data/train  $localdir $tgtdir/data/dev
  echo "## step05: done @ `date`"
fi
if [ ! -z $step06 ]; then
  echo "## step06: train arpa lm @ `date`"
  source/egs/fisher-english/train-kaldi-lm.sh $dict/lexicon.txt $tgtdir/data/train $tgtdir/data/dev $tgtdir/data/lm || exit 1
  echo "## step06: done @ `date`"
fi
lang_test=$tgtdir/data/lang_test
arpa_lm=levantine-arabic/data/lm/3gram-mincount/lm_unpruned.gz
tgt_lang=$lang
if [ ! -z $step07 ]; then
  echo "## step07: build grammar G @ `date`"
  [ -d $lang_test ] || mkdir -p $lang_test
  cp -r $tgt_lang/* $lang_test
  gunzip -c "$arpa_lm" | \
  arpa2fst --disambig-symbol=#0 \
           --read-symbol-table=$lang_test/words.txt - $lang_test/G.fst
  echo  "Checking how stochastic G is (the first of these numbers should be small):"
  fstisstochastic $lang_test/G.fst
  ## Check lexicon.
  ## just have a look and make sure it seems sane.
  echo "First few lines of lexicon FST:"
  fstprint   --isymbols=$lang_test/phones.txt --osymbols=$lang_test/words.txt $lang_test/L.fst  | head
  
  echo Performing further checks

  # Checking that G.fst is determinizable.
  fstdeterminize $lang_test/G.fst /dev/null || echo Error determinizing G.

  # Checking that L_disambig.fst is determinizable.
  fstdeterminize $lang_test/L_disambig.fst /dev/null || echo Error determinizing L.
  # Checking that disambiguated lexicon times G is determinizable
  # Note: we do this with fstdeterminizestar not fstdeterminize, as
  # fstdeterminize was taking forever (presumbaly relates to a bug
  # in this version of OpenFst that makes determinization slow for
  # some case).
  fsttablecompose $lang_test/L_disambig.fst $lang_test/G.fst | \
   fstdeterminizestar >/dev/null || echo Error
  
  # Checking that LG is stochastic:
  fsttablecompose $lang_test/L_disambig.fst $lang_test/G.fst | \
  fstisstochastic || echo "[log:] LG is not stochastic"

  echo "## step07, done @ `date`"
fi
if [ ! -z $step08 ]; then
  echo "## step08: mfcc_pitch @ `date`"
  for x in train dev; do
    sdata=$tgtdir/data/$x; data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc.conf" \
    $sdata $data $feat || exit 1
    echo "## LOG: step08, done with mfcc-pitch for $sdata"
    data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf" \
    $sdata $data $feat || exit 1
  done
  echo "## step08: done with feature extraction @ `date`"
fi
if [ ! -z $step09 ]; then
  echo "## step09: gmm-hmm training @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd run.pl --nj 20 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 6000 --pdf-num 90000 \
  --devdata $tgtdir/data/dev/mfcc-pitch \
  $tgtdir/data/train/mfcc-pitch $tgtdir/data/lang_test $tgtdir/exp || exit 1

  echo "## step09: gmm-hmm training done @ `date`"
fi
if [ ! -z $step10 ]; then
  echo "## step10, nnet training @ `date`"
  source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 \
  --delta-opts "--delta-order=2" \
  --nnet-pretrain-cmd "steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048" \
  --use-partial-data-to-pretrain true --pretraining-rate 0.2 \
 --nnet-id nnet5a --graphdir $tgtdir/exp/tri4a/graph \
 --devdata $tgtdir/data/dev/fbank-pitch  \
 $tgtdir/data/train/fbank-pitch $tgtdir/data/lang $tgtdir/exp/tri4a/ali_train $tgtdir/exp || exit 1
  echo "## step10, done @ `date`"
fi
