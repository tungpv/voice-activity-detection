#!/bin/bash 

. path.sh
. cmd.sh

echo 
echo "## LOG: $0 $@"
echo

# begin options
steps=
# end options 


. parse_options.sh || exit 1

function Usage {
 cat<<END
 $0 [options] <source_data_dir> <tgtdir>
 [options]:
 [steps]:
 1: lowercase all files in source_data_dir
 3: make overall text including train mandarin dev folder
 4: prepare character lexicon
 6: train data
 7: prepare dev data
 9: mfcc-pitch extraction
 11: fbank-pitch

 [examples]:
 
 $0 --steps 1 /data/users/hhx502/ldc-cts2016/hkust-mandarin-tele-trans-ldc2005t32 \
 /home2/hhx502/ldc-cts2016/hkust-mandarin-cts
 
END
}
if [ $# -ne 2 ]; then
  Usage && exit 1
fi

source_data_dir=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi


if [ ! -z $step01 ]; then
  echo "## LOG: step01, lowercase files @ `date`"
  for SRC in `find $source_data_dir -depth`
do
    DST=`dirname "${SRC}"`/`basename "${SRC}" | tr '[A-Z]' '[a-z]'`
    if [ "${SRC}" != "${DST}" ]
    then
        [ ! -e "${DST}" ] && mv -T "${SRC}" "${DST}" || echo "${SRC} was not renamed"
    fi
done 
  echo "## LOG: step01, done @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "## LOG: convert gb18030 to utf8  @ `date`"
  for x in hub5_mandarin_trans_980113/mandarin hkust_mcts_p1tr/data/trans/train hkust_mcts_p1tr/data/trans/dev; do
    sdir=$source_data_dir/$x
    trans_dir=$source_data_dir/transcription/$(basename $x)
    [ -d $trans_dir ] || mkdir -p $trans_dir
    text=$trans_dir/text
    echo -n > $text
    for x1 in $(find $sdir/ -name "*.txt"); do
       iconv -f gb18030 -t utf8 < $x1 > $trans_dir/$(basename $x1)
       cat $trans_dir/$(basename $x1) | \
       perl -e ' 
         use open qw(:std :utf8);
         use utf8;
         ($sfile, $text) = @ARGV;
         $sfile =~ s/\.txt//;
         $sfile =~ m/(\S+)_(\d+)/;  $spkId = $2;
         open(F,">>", "$text") or die "## ERROR: file $text cannot open\n";
         while(<STDIN>) {
           next if(/^\#/);
           @A = split(/\s+/);
           next if @A <= 3;   $start = shift @A; $end = shift @A;
           $channelId = shift @A;  $channelId =~ s/://;
           next if($channelId !~ /[AB]/ );
           $spker = $spkId. "_" . $channelId;
           $utt = $spker . "-" . $sfile . " " . " $start $end ";
           for($i = 0; $i < @A; $i ++) {
             $s = $A[$i]; 
             if($s =~ m/\p{Han}+/) { 
               # @B = split("", $s); $utt .= join(" ", @B);
               $start = 0;  $tot_len = length($s);
               while($s =~ m/(\p{Han}+)/g) {
                 $ms = $1;  $len = length($ms);  $end = pos($s); $new_start = $end - $len;
                 if($new_start > $start) {
                   $utt .= " " . substr($s, $start, $new_start - $start); 
                 }
                 $start = $end;
                 @B = split("", $ms); $utt .= " " . join(" ", @B);
               }
               if($start > 0 && $start < $tot_len) {
                 $utt .= " " . substr($s, $start, $tot_len - $start);
               }
             } else { 
               $utt .= " $s"; 
             }
           }
           print F "$utt\n";
         }
         close F;
       ' $(basename $x1)   $text
    done
  done
  
  echo "## LOG: done @ `date` "
fi
data=$source_data_dir/data/local/data
[ -d $data ] || mkdir -p $data
if [ ! -z $step03 ]; then
  echo "## LOG: step03, filter text @ `date`"
  cat $source_data_dir/transcription/*/text | \
  perl -e 'use open qw(:std :utf8); use utf8
    ($mapfile, $textfile, $wlistfile, $filteredfile) = @ARGV;
    open (MAP, "<", "$mapfile") or die "## ERROR: failed to open $mapfile\n";
    open (FIL, ">", "$filteredfile") or die "## ERROR: failed to open $filteredfile\n";
    $index = 1;
    while(<MAP>) {
      chomp;
      @A = split(/\s+/);
      if (@A != 2) {
        $index ++; next;
      }
      if($index >= 22 && $index <= 41 ||
         $index >=44 && $index <= 159 || 
         $index >=161 && $index <= 185) {
        $vocab{$A[0]} = "<unk>";  
      } else {
        $vocab{$A[0]} =$A[1];
      }
      $index ++;
    }
    open(TEXT, ">", "$textfile") or die "## ERROR: failed to open $textfile\n";
    open(WLIST, "$wlistfile") or die "## ERROR: failed to open $wlistfile\n";
    while(<STDIN>) {
      chomp;
      @A = split(/\s+/); $utt = "$A[0] $A[1] $A[2] "; shift @A; shift @A; shift @A;
      $s = join(" ", @A);
      $s =~ s/[\%＋,\.]//g;
      $s =~ s/<Eng\S+\s+[^\<]+\>/<unk>/g;
      $s =~ s/<Eglish\s+[^\<]+>/<unk>/g;
      $s =~ s/<foreign.*foreign>/<unk>/g;
      $s =~ s/\(\([^\(]+\)\)/<unk>/g; 
      $s =~ s/\(\(\)\)/<unk>/g;
      $s =~ s/\[static\]/<noise>/g;
      $s =~ s/<\/noise>/<noise>/g;
      $s =~ s/<as>//g;
      $s =~ s/<\/as>//g;
      $s =~ s/\[[^\[]+\]//g;
      $s =~ s/\{[^\{]+\}/<noise>/g;
      $sMap = "";
      @A = split(/\s+/, $s);
      $total = scalar @A; $non_num = 0;
      for($i = 0; $i < @A; $i++) {
        die "no mapping word for $A[$i]" if not exists $vocab{$A[$i]};
        $s1 = $vocab{$A[$i]};
        if($s1 eq "NULL") { 
          next;
        } else {
          $non_num ++ if($s1 =~ /</); 
          $sMap .= " $s1";
        }
        print WLIST "$s1\n";
      }
      if($total ==0 || $non_num / $total > 0.3) {
        print FIL "$utt $sMap\n"; next;
      }
      print TEXT "$utt $sMap\n";     
    }  close MAP; close TEXT; close WLIST; close FIL;
  ' $data/word-map.txt  $data/overall-text "|sort -u > $data/overall-word-list.txt" $data/filtered-utterance.txt
  echo "## LOG: step03 ,done with  $data/overall-text, $data/overall-word-list.txt @ `date`"
fi
original_dict=/data/users/hhx502/ldc-cts2016/Callhome-Chlex-LDC96L15/ma_lex.v03-utf8
dict=$source_data_dir/data/local/dict
[ -d $dict ] || mkdir -p $dict
if [ ! -z $step04 ]; then
  echo "## LOG: step04, dict @ `date`"
  cat $original_dict | \
  awk -F '\t' '{print $1,"\t",$2; }' > $dict/lexicon-word-to-syl.txt
  cat $dict/lexicon-word-to-syl.txt | \
  perl -ane '@A = split(/\s+/); shift @A; for($i = 0; $i < @A; $i ++){print $A[$i], "\n";}' | \
  sort -u | \
  perl -e ' ($dfile) = @ARGV; 
    open(F, "$dfile") or die "file $dfile cannot open\n";
    while(<F>) {
      chomp;  @A = split(/\s+/); $vocab{$A[0]} ++;
    } close F;
    while(<STDIN>) {
      chomp;  if(not exists $vocab{$_}) {print "$_\t $_\n"; }
    }
   ' $dict/if.syl.dct \
   > $dict/lexicon-syl-oov.txt
   cat $dict/lexicon-word-to-syl.txt | \
   perl -e '($mapfile) = @ARGV; open(F, "$mapfile") or die "## ERROR: file $mapfile cannot open\n"; 
     while(<F>) {chomp; @A = split(/\s+/); $vocab{$A[0]} = $A[1]; } close F;
     while(<STDIN>){chomp; @A = split(/\s+/); $w = shift @A; $p=""; for($i = 0; $i < @A; $i ++){ $s = $A[$i]; 
       if(exists $vocab{$s}) { $s = $vocab{$s}; }  $p .= " $s";  } $p =~ s/^ //; 
       print "$w\t$p\n";
     }
   ' $dict/syl-oov-map.txt > $dict/lexicon-word-to-ifsyl.txt
   cat $dict/lexicon-word-to-ifsyl.txt | \
   perl -e 'use open qw(:std :utf8); while(<STDIN>) {chomp; @A = split(/\s+/); $w = shift @A; 
     @AW = split("", $w); if(scalar @AW != scalar @A) { print STDERR "bad:$_\n"; }
     for($i = 0; $i < @AW; $i ++) {
       print "$AW[$i]\t$A[$i]\n";
     }
   } ' | sort -u  > $dict/lexicon-char.txt
   cat $data/overall-text | \
   perl -e ' use open qw(:std :utf8); ($dict, $oov) = @ARGV; open(D, "$dict") or die "## ERROR: file $dict cannot open\n";
     open(F, "$oov") or die "## ERROR: file $oov cannot open\n";
     while(<D>) {chomp; @A = split(/\s+/); $vocab{$A[0]} ++;  } close D;
     while(<STDIN>) {chomp; @A = split(/\s+/); for($i=3; $i < @A; $i ++){ $w = $A[$i]; 
       if(not exists $vocab{$w}) {print F "$w\n"; }
     } } close F;
   ' $dict/lexicon-char.txt  "|sort -u >$dict/oov-overall-text-versus-lexicon-char.txt"
   cat $dict/lexicon-char.txt $dict/oov-overall-text-dict.txt | \
   sort -u > $dict/lexicon-char-overall.txt
   cat $data/overall-text | \
   perl -e ' use open qw(:std :utf8); ($dict, $oov) = @ARGV; open(D, "$dict") or die "## ERROR: file $dict cannot open\n";
     open(F, "$oov") or die "## ERROR: file $oov cannot open\n";
     while(<D>) {chomp; @A = split(/\s+/); $vocab{$A[0]} ++;  } close D;
     while(<STDIN>) {chomp; @A = split(/\s+/); for($i=3; $i < @A; $i ++){ $w = $A[$i]; 
       if(not exists $vocab{$w}) {print F "$w\n"; }
     } } close F;
   ' $dict/lexicon-char-overall.txt  "|sort -u >$dict/oov-overall-text-dict-should-be-zero.txt"
   cat $dict/lexicon-char-overall.txt | \
   perl -e '($dict) = @ARGV; open(F, "$dict") or die "## ERROR: file $dict cannot open\n";
   while(<F>) {chomp; @A = split(/\s+/); $w = shift @A; $vocab{$w} = join(" ", @A); } close F;
   while(<STDIN>) {chomp; @A = split(/\s+/); $w = shift @A;  die "## ERROR: unidentified word $A[0]\n" if not exists $vocab{$A[0]};
     print "$w\t $vocab{$A[0]}\n"; 
   }
   ' $dict/if.syl.dct |sort -u > $dict/lexicon-char-to-initial-final.txt
   echo "## LOG: step04, done with  $dict/lexicon-char.txt, $dict/lexicon-char-to-initial-final.txt  @ `date`"
fi
train_data=$data/train
[ -d $train_data ] || mkdir -p $train_data
if [ ! -z $step05 ]; then
  echo "## LOG: step05, training data preparation @ `date`"
  find $source_data_dir/hkust-part1-ldc2005s15/dvd2_of_2/data/audio/train \
       $source_data_dir/hkust-part1-ldc2005s15/dvd1_of_2/data/audio/train \
       $source_data_dir/hub5-mandarin-telephone-speech-data-ldc98s69/cd1-of-2/mandarin \
       $source_data_dir/hub5-mandarin-telephone-speech-data-ldc98s69/cd2-of-2/mandarin \
       -name "*.sph" |sort -u | \
   perl -e '($srctext, $wavscp, $text, $utt2spk, $segments) = @ARGV; 
     open(WAVSCP, ">$wavscp") or die "## ERROR: wavscp $wavscp cannot open\n";
     open(TEXT, ">$text") or die "## ERROR: text $text cannot open\n";
     open(UTT2SPK, ">$utt2spk") or die "## ERROR: utt2spk $utt2spk cannot open\n";
     open(SEG, ">$segments") or die "## ERROR: segments $segments cannot open\n";
     $sphcmdA = "/home2/hhx502/kaldi/tools/sph2pipe_v2.5/sph2pipe -f wav -p -c 1 ";
     $sphcmdB = "/home2/hhx502/kaldi/tools/sph2pipe_v2.5/sph2pipe -f wav -p -c 2 ";
     while(<STDIN>) {chomp; $sph = $_; s/.*\///g; s/\.sph//; $vocab{$_} = $sph;}
     open(F, "$srctext") or die "## ERROR: srctext $srctext cannot open\n";
     while(<F>) {chomp; @A = split(/\s+/); $utt = shift @A; $start = shift @A; $end = shift @A;
      ($spkId, $uttId) = split(/\-/, $utt); 
      $spkId =~ m/(\S+)_([AB])/; $chanId = $2;
      if (not exists $vocab{$uttId}) {
        # print STDERR "## WARNING: no sph file for $uttId\n";
        next ;
      } $sphfile = $vocab{$uttId};
      $newUttId = $uttId . "_" . $chanId;
      if(not exists $v_wavscp{$newUttId}) {
        $v_wavscp{$newUttId} ++;
        if($chanId eq "A") {
          print WAVSCP "$newUttId $sphcmdA $sphfile |\n";
        } elsif($chanId eq "B") {
          print WAVSCP "$newUttId $sphcmdB $sphfile|\n";
        } else { 
          die "## ERROR: unidentified channid $chanId\n";
        }
      }
      $time_start = sprintf("%.2f", $start); $time_end = sprintf("%.2f", $end);
      $new_start = sprintf("%05d", $time_start*100); $new_end = sprintf("%05d", $time_end*100);
      $new_utt_name = sprintf("%s-%s_%s_%s", $spkId, $newUttId, $new_start, $new_end);
      print SEG "$new_utt_name $newUttId $time_start $time_end\n";
      print TEXT "$new_utt_name ", join(" ", @A), "\n";
      print UTT2SPK "$new_utt_name $spkId\n";
     } close F; close WAVSCP; close TEXT; close $SEG; close UTT2SPK;
   ' $data/overall-text  $train_data/wav.scp $train_data/text $train_data/utt2spk $train_data/segments
   utils/utt2spk_to_spk2utt.pl     < $train_data/utt2spk > $train_data/spk2utt
   utils/fix_data_dir.sh $train_data 
  echo "## LOG: step05, training data preparation done $train_data @ `date` "
fi
dev_data=$data/dev
[ -d $dev_data ] || mkdir -p $dev_data
if [ ! -z $step06 ]; then
  echo "## LOG: step06, dev data preparation @ `date`"
  find $source_data_dir/hkust-part1-ldc2005s15/dvd2_of_2/data/audio/dev \
  -name "*.sph" |sort -u | \
  perl -e '($srctext, $wavscp, $text, $utt2spk, $segments) = @ARGV; 
     open(WAVSCP, ">$wavscp") or die "## ERROR: wavscp $wavscp cannot open\n";
     open(TEXT, ">$text") or die "## ERROR: text $text cannot open\n";
     open(UTT2SPK, ">$utt2spk") or die "## ERROR: utt2spk $utt2spk cannot open\n";
     open(SEG, ">$segments") or die "## ERROR: segments $segments cannot open\n";
     $sphcmdA = "/home2/hhx502/kaldi/tools/sph2pipe_v2.5/sph2pipe -f wav -p -c 1 ";
     $sphcmdB = "/home2/hhx502/kaldi/tools/sph2pipe_v2.5/sph2pipe -f wav -p -c 2 ";
     while(<STDIN>) {chomp; $sph = $_; s/.*\///g; s/\.sph//; $vocab{$_} = $sph;}
     open(F, "$srctext") or die "## ERROR: srctext $srctext cannot open\n";
     while(<F>) {chomp; @A = split(/\s+/); $utt = shift @A; $start = shift @A; $end = shift @A;
      ($spkId, $uttId) = split(/\-/, $utt); 
      $spkId =~ m/(\S+)_([AB])/; $chanId = $2;
      if (not exists $vocab{$uttId}) {
        # print STDERR "## WARNING: no sph file for $uttId\n";
        next ;
      } $sphfile = $vocab{$uttId};
      $newUttId = $uttId . "_" . $chanId;
      if(not exists $v_wavscp{$newUttId}) {
        $v_wavscp{$newUttId} ++;
        if($chanId eq "A") {
          print WAVSCP "$newUttId $sphcmdA $sphfile |\n";
        } elsif($chanId eq "B") {
          print WAVSCP "$newUttId $sphcmdB $sphfile|\n";
        } else { 
          die "## ERROR: unidentified channid $chanId\n";
        }
      }
      $time_start = sprintf("%.2f", $start); $time_end = sprintf("%.2f", $end);
      $new_start = sprintf("%05d", $time_start*100); $new_end = sprintf("%05d", $time_end*100);
      $new_utt_name = sprintf("%s-%s_%s_%s", $spkId, $newUttId, $new_start, $new_end);
      print SEG "$new_utt_name $newUttId $time_start $time_end\n";
      print TEXT "$new_utt_name ", join(" ", @A), "\n";
      print UTT2SPK "$new_utt_name $spkId\n";
     } close F; close WAVSCP; close TEXT; close $SEG; close UTT2SPK;
   ' $data/overall-text  $dev_data/wav.scp $dev_data/text $dev_data/utt2spk $dev_data/segments
   utils/utt2spk_to_spk2utt.pl     < $dev_data/utt2spk > $dev_data/spk2utt
   utils/fix_data_dir.sh $dev_data 

  echo "## LOG: step06, done with dev data preparation, $dev_data @ `date`"
fi
dictdir=$tgtdir/data/local/dict
lang=$tgtdir/data/lang
[ -d $dictdir ] || mkdir -p $dictdir
if [ ! -z $step07 ]; then
  echo "## LOG: step07, prepare lang @ `date`"
  echo -e "<noise>\t<sss>\n<unk>\t<oov>" | cat - $dict/lexicon-char-to-initial-final.txt > $dictdir/lexicon.txt
  cat $dictdir/lexicon.txt | perl -ane 'chomp; @A = split(/\s+/); $w = shift @A; 
    for($i= 0; $i < @A; ++$i){ $p = $A[$i]; print "$p\n";}
  ' | sort -u | egrep -v '<sss>|<oov>'> $dictdir/nonsilence_phones.txt
  echo -e "SIL\n<sss>\n<oov>" > $dictdir/silence_phones.txt
  echo  "SIL" > $dictdir/optional_silence.txt
  echo -n  > $dictdir/extra_questions.txt
  utils/prepare_lang.sh $dictdir "<unk>" $lang/tmp $lang
  echo "## LOG: step07, done with lang preparation, $dictdir, @ `date`"
fi
tgt_train=$tgtdir/data/train
[ -d $tgt_train ] || mkdir -p $tgt_train
tgt_dev=$tgtdir/data/dev
[ -d $tgt_dev ] || mkdir -p $tgt_dev
if  [ ! -z $step08 ]; then
  echo "## LOG: step08, prepare kaldi data @ `date`"
  cp $train_data/* $tgt_train
  cp $dev_data/* $tgt_dev
  
  echo "## LOG: step08, done with kaldi data preparation @ `date`"
fi
if [ ! -z $step09 ]; then
  echo "## LOG: step09, mfcc_pitch @ `date`"
  for x in train dev; do
    sdata=$tgtdir/data/$x; data=$sdata/mfcc-pitch feat=$sdata/feat/mfcc-pitch
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20 \
    --mfcc-for-ivector true --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc.conf --pitch-config conf/pitch.conf" \
    $sdata $data $feat || exit 1
  done
  echo "## LOG: step09, done with mfcc_pitch @ `date`"
fi
if [ ! -z $step10 ]; then
  echo "## LOG: step10, gmm-hmm training @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd run.pl --nj 20 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 5000 --pdf-num 75000 \
  --devdata $tgt_dev/mfcc-pitch \
  $tgt_train/mfcc-pitch $tgtdir/data/lang-char-lm-test $tgtdir/exp || exit 1
  echo "## LOG: step10, done with gmm-hmm training, @ `date`"
fi
if [ ! -z $step11 ]; then
  echo "## LOG: step11, fbank_pitch @ `date`"
  for x in train dev; do
    sdata=$tgtdir/data/$x; data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20 \
    --make-fbank true --fbank-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf" \
    $sdata $data $feat || exit 1
  done
  echo "## LOG: step11, done with fbank_pitch @ `date`"
fi
