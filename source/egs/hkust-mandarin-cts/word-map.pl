#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;

if($numArgs != 3) {
  print STDERR "\nExample: cat text | $0 <token-map-file> <from-field>\n";
  exit 1;
}
my ($token_map_file, $from) = @ARGV;

