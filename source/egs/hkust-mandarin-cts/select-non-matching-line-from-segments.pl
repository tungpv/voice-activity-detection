#!/usr/bin/perl
use strict;
use warnings;

if (@ARGV != 1) {
  print STDERR "\nExample: cat source-segments | $0 sub-segments\n\n";
  exit 1;
}
my ($sub_segments) = @ARGV;
open(SEG, "$sub_segments") or die "## ERROR: sub-segments $sub_segments cannot open\n";
my %sub_vocab = ();
while(<SEG>) {
  chomp;
  m/(\S+)\s+(.*)/ or die "## ERROR: bad line $_\n";
  $sub_vocab{$1} ++;
}
close SEG;
print STDERR "stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or die "## ERROR: bad line $_\n in stdin";
  next if exists $sub_vocab{$1}; 
  print "$_\n";
}

