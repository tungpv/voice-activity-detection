#!/usr/bin/perl 
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);

if (@ARGV != 1) {
  print STDERR "\nExample: cat segments | $0 <hours> \n\n";
  exit 1;
}
my ($hours) = @ARGV;

my $sec_length = $hours * 3600;

my $select_length = 0;
print STDERR "stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  die "## ERROR bad line $_\n" if @A != 4; 
  $select_length += $A[3] - $A[2];
  last if $select_length > $sec_length;
  print "$_\n";
} 
print STDERR "stdin ended \n";
