#!/usr/bin/perl
use utf8;
use open qw(:std :utf8);
use strict;
use warnings;

if (@ARGV != 4) {
  print STDERR "\nExamples cat word-list.txt | $0 source-lex.txt supplementary-lex.txt merge-lex.txt oov.txt\n\n";
  exit 1;
}
my ($source_lex_file, $sup_lex_file, $merge_lex_file, $oov_file) = @ARGV;

sub load_lexicon {
  my ($file, $vocab) = @_;
  print STDERR "file = $file\n";
  open(F, "$file") or die "## ERROR: file $file cannot open\n";
  while(<F>) {
    chomp; 
    my @A = split(/\s+/);  next if @A < 2;
    my $w = shift @A;  my $pron = join(" ", @A);
    if(not exists $$vocab{$w}) {
      my @B = ();
      $$vocab {$w} = [@B];
    }
    my $array = $$vocab{$w}; push(@$array, $pron);
  }
  my $num_keys = keys %$vocab;
  print STDERR  "key:", $num_keys, "\n";
  close F;
}
sub enlarge_lexicon {
  my ($source_vocab, $sup_vocab, $w) = @_;
  if(not exists $$source_vocab{$w}) {
    my @B = ();
    $$source_vocab{$w} = [@B];
  }
  my $source_array = $$source_vocab{$w};
  my $sup_array = $$sup_vocab{$w};
  for(my $i = 0; $i < @$sup_array; $i++) {
    my $pron = $$sup_array[$i];
    push(@$source_array, $pron);
  }
}

my %source_vocab = ();
my %sup_vocab = ();
load_lexicon($source_lex_file,\%source_vocab);
load_lexicon($sup_lex_file, \%sup_vocab);

my $oov = "";
while(<STDIN>) {
  chomp;
  /(\S+)\s*/ or next; my $w = $1;
  next if(exists $source_vocab{$w});
  if(exists $sup_vocab{$w}) {
    enlarge_lexicon(\%source_vocab, \%sup_vocab, $w);
    next;
  }
  $oov .= "$w\t$w\n";
}
open(F, "|sort -u > $oov_file") or die "## ERROR: file $oov_file cannot open\n";
print F $oov;
close F; 
open(F, "|sort -u > $merge_lex_file") or die "## ERROR: file $merge_lex_file cannot open\n";
my $words = "";
foreach my $w (keys %source_vocab) {
  my $array = $source_vocab{$w};
  for(my $i = 0; $i < @$array; $i++) {
    my $pron = $$array[$i];
    $words .= "$w\t$pron\n";
  }
} 
print F $words;
close F;
