#!/bin/bash 

. path.sh
. cmd.sh

echo 
echo "## LOG: $0 $@"
echo

# begin options
cmd=slurm.pl
steps=
source_mandarin_lexicon=/data/users/hhx502/ldc-cts2016/hkust-mandarin-tele-trans-ldc2005t32/data/local/dict/lexicon-char-to-initial-final.txt
source_english_lexicon=/home2/hhx502/ldc-cts2016/fisher-english/data/local/dict/lexicon.txt
lmtype=3gram-mincount
# end options 


. parse_options.sh || exit 1

function Usage {
 cat<<END
 $0 [options] <source_data_dir> <tgtdir>
 [options]:
 --cmd                                     # value, "$cmd"
 --steps                                   # value, "$steps"
 --source-mandarin-lexicon                 # value, "$source_mandarin_lexicon"
 --source-english-lexicon                  # value, "$source_english_lexicon"
 --lmtype                                  # value, "$lmtype"

 [steps]:
 1: lowercase all files in source_data_dir
 3: make overall text including train mandarin dev folder
 4: prepare character lexicon
 6: train data
 7: prepare dev data
 9: mfcc-pitch extraction
 11: fbank-pitch

 [examples]:
 
 $0 --steps 1 --source-mandarin-lexicon $source_mandarin_lexicon \
 --source-english-lexicon $source_english_lexicon \
 --lmtype $lmtype \
 /data/users/hhx502/ldc-cts2016/hkust-mandarin-tele-trans-ldc2005t32 \
 /home2/hhx502/ldc-cts2016/hkust-mandarin-cts
 
END
}
if [ $# -ne 2 ]; then
  Usage && exit 1
fi

source_data_dir=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi


if [ ! -z $step01 ]; then
  echo "## LOG: step01, lowercase files @ `date`"
  for SRC in `find $source_data_dir -depth`
do
    DST=`dirname "${SRC}"`/`basename "${SRC}" | tr '[A-Z]' '[a-z]'`
    if [ "${SRC}" != "${DST}" ]
    then
        [ ! -e "${DST}" ] && mv -T "${SRC}" "${DST}" || echo "${SRC} was not renamed"
    fi
done 
  echo "## LOG: step01, done @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "## LOG: convert gb18030 to utf8  @ `date`"
  for x in hub5_mandarin_trans_980113/mandarin hkust_mcts_p1tr/data/trans/train hkust_mcts_p1tr/data/trans/dev; do
    sdir=$source_data_dir/$x
    trans_dir=$source_data_dir/data/transcription/$(basename $x)
    [ -d $trans_dir ] || mkdir -p $trans_dir
    text=$trans_dir/text
    echo -n > $text
    for x1 in $(find $sdir/ -name "*.txt"); do
       iconv -f gb18030 -t utf8 < $x1 > $trans_dir/$(basename $x1)
       cat $trans_dir/$(basename $x1) | \
       perl -e ' 
         use open qw(:std :utf8);
         use utf8;
         ($sfile, $text) = @ARGV;
         $sfile =~ s/\.txt//;
         $sfile =~ m/(\S+)_(\d+)/;  $spkId = $2;
         open(F,">>", "$text") or die "## ERROR: file $text cannot open\n";
         while(<STDIN>) {
           next if(/^\#/);
           @A = split(/\s+/);
           next if @A <= 3;   $start = shift @A; $end = shift @A;
           $channelId = shift @A;  $channelId =~ s/://;
           next if($channelId !~ /[AB]/ );
           $spker = $spkId. "_" . $channelId;
           $utt = $spker . "-" . $sfile . " " . " $start $end ";
           for($i = 0; $i < @A; $i ++) {
             $s = $A[$i]; 
             if($s =~ m/\p{Han}+/) { 
               # @B = split("", $s); $utt .= join(" ", @B);
               $start = 0;  $tot_len = length($s);
               while($s =~ m/(\p{Han}+)/g) {
                 $ms = $1;  $len = length($ms);  $end = pos($s); $new_start = $end - $len;
                 if($new_start > $start) {
                   $utt .= " " . substr($s, $start, $new_start - $start); 
                 }
                 $start = $end;
                 @B = split("", $ms); $utt .= " " . join(" ", @B);
               }
               if($start > 0 && $start < $tot_len) {
                 $utt .= " " . substr($s, $start, $tot_len - $start);
               }
             } else { 
               $utt .= " $s"; 
             }
           }
           print F "$utt\n";
         }
         close F;
       ' $(basename $x1)   $text
    done
  done
  
  echo "## LOG: done ($trans_dir) @ `date` "
fi
overall_data=$source_data_dir/data/local/overall-data
dict=$source_data_dir/data/local/dict-mix
[ -d $data ] || mkdir -p $data
if [ ! -z $step03 ]; then
  echo "## LOG: step03, filter text @ `date`"
  [ -d $overall_data ] || mkdir -p $overall_data
  touch  $overall_data/token-map.txt
  cat $source_data_dir/data/transcription/*/text | \
  source/egs/hkust-mandarin-cts/text-cleaner.pl $overall_data/token-map-edited.txt  4 \
  $overall_data/text.1 $overall_data/token-map.1.txt  $overall_data/removed.txt
  cat $overall_data/text.1 | source/egs/hkust-mandarin-cts/token-map.pl $dict/oov-word-list.txt 4 $overall_data/text.2
  echo "## LOG: step03 ,done ($overall_data) @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## step04, prepare dict"
  [ -d $dict ] || mkdir -p $dict
  cat $source_mandarin_lexicon | \
  grep -v '<' | \
  cat - $dict/man-oov-dict.txt | \
  source/egs/hkust-mandarin-cts/append-langid-to-lexicon-phone.pl "man" > $dict/man-char-lex.txt
  cat $source_english_lexicon | \
  grep -v '<' | \
  source/egs/hkust-mandarin-cts/append-langid-to-lexicon-phone.pl "eng" > $dict/eng-lex.txt
  cat $overall_data/text.2 | \
  perl -ane 'chomp; @A = split(/\s+/); shift @A; shift @A; shift @A; 
    for($i = 0; $i < @A; $i++) {$w = $A[$i]; if(not exists $oov{$w}){$oov{$w}++; print "$w\n";  } }' | \
  source/egs/hkust-mandarin-cts/merge-lex.pl $dict/man-char-lex.txt $dict/eng-lex.txt $dict/merge-lex.txt \
  $dict/oov-word-list.txt
  echo "## step04, dict preparation done($dict)"
fi
old_train=$source_data_dir/data/local/old-train
if [ ! -z $step05 ]; then
  echo "## step05, prepare old-train data @ `date`"
  [ -d $old_train ] || mkdir -p $old_train
  find $source_data_dir/hkust-part1-ldc2005s15/dvd2_of_2/data/audio/train \
       $source_data_dir/hkust-part1-ldc2005s15/dvd1_of_2/data/audio/train \
       $source_data_dir/hub5-mandarin-telephone-speech-data-ldc98s69/cd1-of-2/mandarin \
       $source_data_dir/hub5-mandarin-telephone-speech-data-ldc98s69/cd2-of-2/mandarin \
       -name "*.sph" |sort -u | \
   source/egs/hkust-mandarin-cts/make-kaldi-data.pl \
   $overall_data/text.2  $old_train/wav.scp $old_train/text $old_train/utt2spk $old_train/segments
   utils/utt2spk_to_spk2utt.pl     < $old_train/utt2spk > $old_train/spk2utt
   utils/fix_data_dir.sh $old_train 


  echo "## step05, old-train data preparation done @ `date`"
fi
old_dev=$source_data_dir/data/local/old-dev
if [ ! -z $step06 ]; then
  echo "## step06, prepare old-dev data @ `date`"
  [ -d $old_dev ] || mkdir -p $old_dev
  find $source_data_dir/hkust-part1-ldc2005s15/dvd2_of_2/data/audio/dev \
  -name "*.sph" |sort -u | \
  source/egs/hkust-mandarin-cts/make-kaldi-data.pl \
  $overall_data/text.2  $old_dev/wav.scp $old_dev/text $old_dev/utt2spk $old_dev/segments
  utils/utt2spk_to_spk2utt.pl     < $old_dev/utt2spk > $old_dev/spk2utt
  utils/fix_data_dir.sh $old_dev
  echo "## step06, done ($old_dev) @ `date`"
fi
train=$tgtdir/data-mix/train
dev=$tgtdir/data-mix/dev
if [ ! -z $step07 ]; then
  echo "## step07, re-prepare new train and dev data @ `date`"
  data_merge=$tgtdir/data-mix/overall
  mixdata=$tgtdir/data-mix/overall-mix
  [ -d $mixdata ] || mkdir -p $mixdata
  utils/combine_data.sh $data_merge $old_train $old_dev
  cat $data_merge/text | \
  source/egs/hkust-mandarin-cts/select-text-contain-en-words.pl "<noise> <sp-noise> <unk>" \
  $data_merge/segments $mixdata/text $mixdata/segments 
  
  [ -d $dev ] || mkdir -p $dev
  cat $mixdata/segments | \
  utils/shuffle_list.pl  | \
  source/egs/hkust-mandarin-cts/subset-segments-with-length.pl 3.2 > $dev/uttlist
  utils/subset_data_dir.sh --utt-list $dev/uttlist  $data_merge  $dev
  cat $dev/utt2spk | perl -ane 'm/(\S+)\s+(.*)/; print "$1 $1\n";' > $dev/spk2utt;
  cp $dev/spk2utt $dev/utt2spk
  utils/fix_data_dir.sh $dev
  
  [ -d $train ] || mkdir -p $train
  cat $data_merge/segments | \
  source/egs/hkust-mandarin-cts/select-non-matching-line-from-segments.pl $dev/segments \
  > $train/uttlist
  utils/subset_data_dir.sh --utt-list $train/uttlist  $data_merge  $train
  utils/fix_data_dir.sh  $train
  echo "## step07, done ($train, $mixdata, $dev) @ `date`"
fi
lang=$tgtdir/data-mix/lang
tgtdict=$tgtdir/data-mix/local/dict
if [ ! -z $step08 ]; then
  echo "## step08, dict and lang preparation @ `date`"
  [ -d $tgtdict ] || mkdir -p $tgtdict
  echo -e "<noise>\t<ns>\n<unk>\t<oov>\n<sp-noise>\t<sns>" | \
  cat - $dict/merge-lex.txt > $tgtdict/lexicon.txt
  cat $dict/merge-lex.txt | \
  perl -ane 'chomp; @A = split(/\s+/); $w = shift @A; 
    for($i= 0; $i < @A; ++$i){ $p = $A[$i]; print "$p\n";}
  ' | sort -u > $tgtdict/nonsilence_phones.txt
  echo -e "SIL\n<ns>\n<oov>\n<sns>" > $tgtdict/silence_phones.txt
  echo "SIL" > $tgtdict/optional_silence.txt
  echo -n  > $tgtdict/extra_questions.txt
  utils/prepare_lang.sh $tgtdict "<unk>" $lang/tmp $lang
  echo "## step08, done ($lang) @ `date`"
fi
if [ ! -z $step09 ]; then
  echo "## step09, mfcc_pitch and fbank_pitch feature preparation @ `date`"
  for x in train dev; do
    sdata=$tgtdir/data-mix/$x; data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    source/egs/swahili/make_feats.sh --cmd "slurm.pl"  --nj 40 \
    --mfcc-for-ivector true --mfcc-cmd \
    "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc.conf --pitch-config conf/pitch.conf" \
    $sdata $data $feat || exit 1
    echo "## step09, mfcc-pitch done for $sdata"
    data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd slurm.pl --nj 40 \
    --make-fbank true --fbank-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf" \
    $sdata $data $feat || exit 1
    echo "## step09, fbank-pitch done for $sdata"
  done
  echo "## step09, ('$train', '$dev') done @ `date`"
fi
lmdir=$tgtdir/data-mix/local/lm
if [ ! -z $step10 ]; then
  echo "## step10, make arpa lm and FST grammar G @ `date`"
  mv $train/text  $train/text.1
  cat $train/text.1 | perl -ane 'use utf8; use open qw(:std :utf8);  s:<[^<]+>::g; print;' | \
  awk '{if(NF>1){print;}}' > $train/text
  source/egs/fisher-english/train-kaldi-lm.sh --lmtype $lmtype  $tgtdict/lexicon.txt \
  $train $dev  $lmdir
  arpa_lm_dir=$lmdir/$lmtype
  [ -f $arpa_lm_dir/lm_unpruned.gz ] || \
  { echo "## ERROR:  lm_unpruned.gz expected"; exit 1; }
  source/egs/fisher-english/arpa2G.sh $arpa_lm_dir/lm_unpruned.gz $lang $lang
  mv $train/text.1  $train/text
 
  echo "## step10, done ($lmdir) @ `date`"
fi
if [ ! -z $step11 ]; then
  echo "## step11, train gmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd slurm.pl --nj 40 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 7000 --pdf-num 210000 \
  --devdata $dev/mfcc-pitch \
  $train/mfcc-pitch $lang $tgtdir/exp-mix/mono || exit 1
  echo "## step11, done @ `date`"
fi
echo "Done for Overall !" && exit 0 

