#!/usr/bin/perl
use utf8;
use open qw(:std :utf8);

if (@ARGV != 1) {
  print STDERR "\nExample: cat lex.txt | $0 lang-identifier > new_lex.txt\n";
  exit 1;
}

($langId) = @ARGV;

while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $w = $1; $pron = $2;
  @A = split(/\s+/, $pron);
  $pron = $A[0] . "_$langId";
  for($i = 1; $i < @A; $i++) {
    $pron .= " $A[$i]" . "_$langId";
  }
  print "$w\t$pron\n";
}
