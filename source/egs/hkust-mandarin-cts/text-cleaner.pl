#!/usr/bin/perl 
use utf8;
use open qw(:std :utf8);

$numArgs = scalar @ARGV;

if($numArgs != 5) {
  print STDERR "\nUsage: cat text | $0 <token-map-file> <from-field> <new-text> <new-token-map-file> <removed-text>\n";
  exit 1;
}
sub ClearWords {
  local ($words) = @_;
  $$words =~ s#\(\(\)\)#tmpunknownwords#g;
  $$words =~ s#<as>##g;
  $$words =~ s#</as>##g;
  $$words =~ s#\{[^\{]+\}#tmpunknownnoise#g;
  $$words =~ s#</foreign>##g;
  $$words =~ s#\[[^\[]+\]##g;
  $$words =~ s#<English##g;
  $$words =~ s#<foreign##g;
  $$words =~ s#<noise>##g;
  $$words =~ s#</noise>##g;
  $$words =~ s#<background##g;
  $$words =~ s#<Eglish##g;
  $$words =~ s#<Englis##g;
  $$words =~ s#<ov##g;
  $$words =~ s#</ov##g;
  $$words =~ s#language=\"\S+\">##g;
  $$words =~ s#uh\-hm>|uh\-huh|un\-huh|Uh\-hm|Uh\-huh|mm\-hum|mm\-hm|hu\-huh|huh#tmpspeechnoise#g;
  $$words =~ s#\'#tmpsinglequote#g;
  $$words =~ s/[[:punct:]]//g;
  $$words =~ s/tmpunknownwords/<unk>/g;
  $$words =~ s/tmpunknownnoise/<noise>/g;
  $$words =~ s/tmpspeechnoise/<sp-noise>/g;
  $$words =~ s/tmpsinglequote/\'/g;
  $$words = lc $$words;
}
($token_map_file, $from, $text, $new_token_map_file, $removed_text) = @ARGV;
open(T, "$token_map_file") or die "## ERROR: token_map_file $token_map_file cannot open\n";
while(<T>) {
  chomp;
  @A = split(/\s+/);
  next if(@A <= 0);
  if(@A == 1) {
    $vocab{$A[0]} = "";
  } else {
    $w = shift @A;
    $vocab{$w} = join(" ", @A);
  }
}
close T;
open(F, ">$text") or die "## ERROR: text $text cannot open\n";
open(T, "|sort -u >$new_token_map_file") or die "## ERROR: new_token_map_file cannot open for writing\n";
open(R, ">", "$removed_text") or die "## ERROR: removed_text $removed_text cannot open \n";
while(<STDIN>) {
  chomp;
  @A = split(/\s+/);
  $utt = "";
  for($i = 0; $i < $from -1; $i++) {
    if($i == 0) { $utt = $A[0]; next; }
    $time = sprintf("%.2f", $A[$i]);
    $utt .= " $time"; 
  }
  for($i = 0; $i < $from -1; $i++) {
    shift @A;
  }
  $words = join(" ", @A);
  ClearWords(\$words);
  next if length $words <= 0;
  # print STDERR "$utt $words\n";
  @A = split(/\s+/, $words);
  $total_word = scalar @A;
  $words = "";  $num_nonword = 0;
  for($i = 0; $i < @A; $i++) {
    $w = $A[$i];
    if(exists $vocab{$w}) { $w = $vocab{$w};  }
    next if length $w <= 0;
    if($w =~ m/</) { $num_nonword ++; }
    if(length $words <= 0) {$words = $w; next;}
    $words .= " $w";
    if(not exists $v{$w}) {
       $v{$w} ++;
       print T "$w\t$t$w\n";
    }
  } 
  if ($num_nonword >= $total_word *.5) {
    print R "$utt $words\n"; next;
  }
  print F "$utt $words\n";
}
close F;
close T;
close R;
