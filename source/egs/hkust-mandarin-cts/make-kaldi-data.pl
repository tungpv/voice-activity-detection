#!/usr/bin/perl

$numArgs = scalar @ARGV;
if ($numArgs != 5) {
  print "\nExample: cat sph-list.txt | $0 <source-transcript> <wav.sp> <text> <utt2spk> <segments>\n\n";
  exit 1;
}
($srctext, $wavscp, $text, $utt2spk, $segments) = @ARGV; 
open(WAVSCP, ">$wavscp") or die "## ERROR: wavscp $wavscp cannot open\n";
open(TEXT, ">$text") or die "## ERROR: text $text cannot open\n";
open(UTT2SPK, ">$utt2spk") or die "## ERROR: utt2spk $utt2spk cannot open\n";
open(SEG, ">$segments") or die "## ERROR: segments $segments cannot open\n";
$sphcmdA = "/home2/hhx502/kaldi/tools/sph2pipe_v2.5/sph2pipe -f wav -p -c 1 ";
$sphcmdB = "/home2/hhx502/kaldi/tools/sph2pipe_v2.5/sph2pipe -f wav -p -c 2 ";
while(<STDIN>) { 
  chomp; 
  $sph = $_; 
  s/.*\///g; 
  s/\.sph//; 
  $vocab{$_} = $sph;
}
open(F, "$srctext") or die "## ERROR: srctext $srctext cannot open\n";
while(<F>) {
  chomp; @A = split(/\s+/); $utt = shift @A; $start = shift @A; $end = shift @A;
  ($spkId, $uttId) = split(/\-/, $utt); 
  $spkId =~ m/(\S+)_([AB])/; $chanId = $2;
  if (not exists $vocab{$uttId}) {
    next ;
  } 
  $sphfile = $vocab{$uttId};
  $newUttId = $uttId . "_" . $chanId;
  if(not exists $v_wavscp{$newUttId}) {
    $v_wavscp{$newUttId} ++;
    if($chanId eq "A") {
      print WAVSCP "$newUttId $sphcmdA $sphfile |\n";
    } elsif($chanId eq "B") {
      print WAVSCP "$newUttId $sphcmdB $sphfile|\n";
    } else { 
      die "## ERROR: unidentified channid $chanId\n";
    }
  }
  $time_start = sprintf("%.2f", $start); $time_end = sprintf("%.2f", $end);
  $new_start = sprintf("%05d", $time_start*100); $new_end = sprintf("%05d", $time_end*100);
  $new_utt_name = sprintf("%s-%s_%s_%s", $spkId, $newUttId, $new_start, $new_end);
  print SEG "$new_utt_name $newUttId $time_start $time_end\n";
  print TEXT "$new_utt_name ", join(" ", @A), "\n";
  print UTT2SPK "$new_utt_name $spkId\n";
} 
close F; close WAVSCP; close TEXT; close $SEG; close UTT2SPK;
    
