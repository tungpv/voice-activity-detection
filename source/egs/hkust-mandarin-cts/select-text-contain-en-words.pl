#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;

if ($numArgs != 4) {
  print STDERR "\nExample: cat text | $0  <noise-words> <source-segments> <new-text> <new-segments> \n\n";
  exit 1;
}
my ($noise_words, $source_segments, $new_text, $new_segments) = @ARGV;
open(F, "$source_segments") or die "## ERROR: segments $source_segments cannot open\n";
my %segments_vocab = ();
while(<F>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/ or next;
  $segments_vocab{$1} = $_;
}
close F;

my @A = split(/\s+/, $noise_words);
my $noise_str = "";
for(my $i = 0; $i < @A; $i++) {
  if($i < scalar @A -1) {
    $noise_str .= "$A[$i]|";
  } else {
    $noise_str .= $A[$i];
  }
}
open(TEXT, ">$new_text") or die "## ERROR: new_text $new_text cannot open\n";
open(SEG, ">$new_segments") or die "## ERROR: new_segments $new_segments cannot open\n";
while(<STDIN>) {
  chomp;
  my $utt = $_;
  $utt =~ s/$noise_str//g;
  my @B = split(/\s+/, $utt);
  my $segment_id = shift @B;
  $utt = join(" ", @B);
  if($utt =~ m/[a-zA-Z]/g) {
    next if not exists $segments_vocab{$segment_id};
    print TEXT "$_\n";
    print SEG "$segments_vocab{$segment_id}\n";
  }
}
close TEXT;
close SEG;

