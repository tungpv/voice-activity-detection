#!/bin/bash


. path.sh
. cmd.sh

# begin options
check_fields=false
# end options
echo 
echo "$0 $@"
echo 

. parse_options.sh

function PrintOptions {
 cat <<END
$0 [options]:
check_fields		# value, $check_fields

END
}

logdir=./log
[ -d $logdir ] || mkdir -p $logdir
if $check_fields; then
  echo "$0: check_fields started @ `date`"
  cat ./data.txt | \
  source/egs/hly-organize-data.pl 
fi
