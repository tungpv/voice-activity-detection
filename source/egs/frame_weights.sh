#!/bin/bash

feat-to-len scp:grapheme_vllp/data/train/plp_pitch/feats.scp  ark,t:- | perl -pe '@A=split(/\s+/);  $s="$A[0] [";for($i=0; $i<$A[1]; $i++){ $s .=" 1";} $s .= " ]"; $_="$s\n"; ' | gzip -c > grapheme_vllp/exp/nnet.mmi.bnf.tuned.mling6.fmllr/decode.untrans-graph.bd.0.05.201421/best_path/weights.trans.gz

lattice-to-post --acoustic-scale=$(echo "1/13"| bc -l) ark:"gzip -cd grapheme_vllp/exp/nnet.mmi.bnf.tuned.mling6.fmllr/decode.untrans-graph.bd.0.05.201421/lat.*.gz|" ark:- | source/code/post-to-weights --maximum=true ark:- ark,t:- | gzip -c > grapheme_vllp/exp/nnet.mmi.bnf.tuned.mling6.fmllr/decode.untrans-graph.bd.0.05.201421/best_path/weights.gz

