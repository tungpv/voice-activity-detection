#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

# begin sub
sub NormalizeText {
  my ($text) = @_;
  $$text =~ s:<[^<]+>::g;
  $$text =~ s:^\s+::g;
  $$text =~ s:\s+$::g;
}
# end sub
my $numArgs = scalar @ARGV;
if($numArgs != 3) {
  die "\nExample: $0 <big.txt> <small.txt> <tgtdir>\n\n";
}

my ($bigFile, $smallFile, $tgtdir) = @ARGV;

my %vocab = ();
open(F, "$smallFile") or die;
while(<F>) {
  chomp;
  m:^(\S+)\s*(.*)$:g;
  my $text = $2;
  NormalizeText(\$text);
  next if ($text =~ /^$/);
  $vocab{$1} = $text;
}
close F;

open(F, "$bigFile") or die;
open(H, ">$tgtdir/hyp.txt") or die;
open(R, ">$tgtdir/ref.txt") or die;
my %unique = ();
while(<F>) {
  chomp;
  m:^(\S+)\s*(.*):g;
  my ($labId, $text) = ($1, $2);
  NormalizeText(\$text);
  next if ($text =~ /^$/);
  print STDERR "oov labid '$labId' seen\n" and next if(not exists $vocab{$labId});
  next if(exists $unique{$labId});
  $unique{$labId} ++;
  print H "$text\n";
  print R "$vocab{$labId}\n";
}
close F;
close H;
close R;
