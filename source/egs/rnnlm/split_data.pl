#!/usr/bin/perl -w 
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;

if ($numArgs != 3) {
  die "\n[Example]: $0  <rnn_nbest_file> <ngram_nbest_file> splitNum\n\n";
}
# begin sub
sub SplitFileName {
  my ($fileName, $pathName, $baseName) = @_;
  $fileName =~ m:(^.*)\/([^\/]+)$:g;
  $$pathName = $1;
  $$baseName = $2;
}
sub SaveSplitFile {
  my ($ostream, $nShareIndex, $split_num, $fileName) = @_;
  $$nShareIndex ++;
  my ($tgtdir, $baseName);
  SplitFileName($fileName, \$tgtdir, \$baseName);
  my $outputFileName = sprintf("%s/split%d/%d/%s", $tgtdir, $split_num, $$nShareIndex, $baseName);
  SplitFileName($outputFileName, \$tgtdir, \$baseName);
  `[ -d $tgtdir ] || mkdir -p $tgtdir`;
  open(OUT, ">$outputFileName") or die;
  print OUT "$ostream";
  close OUT;
}
sub SplitFile {
  my ($fileName, $shareLineSegA) = @_;
  open(F, "$fileName") or die;
  my $lineNum = 0;
  my $splitNumIndex = 0;
  my $ostream = '';
  my $split_num = scalar @$shareLineSegA;
  while(<F>) {
    chomp;
    if($lineNum < $$shareLineSegA[$splitNumIndex]) {
      $ostream .= "$_\n";
    }else {
      SaveSplitFile($ostream, \$splitNumIndex, $split_num, $fileName);
      $ostream = "$_\n";
    }
    $lineNum ++;
  }
  close F;
  if ($ostream ne '') {
    SaveSplitFile($ostream, \$splitNumIndex, $split_num, $fileName);
  }
  return $lineNum;
}
# end sub
my ($rnn_nbest_file, $ngram_nbest_file, $split_num) = @ARGV;

open(R, "$rnn_nbest_file") or die;
my $rLineNum = 0;
my @A = ();
while(<R>) {
  chomp;
  $rLineNum ++;
  m:^(\d+)+(.*)$:g;
  $A[$1] ++;
}
close R;
my $uttNum = scalar @A;
my $x = 0;
for(my $i = 0; $i < $uttNum; $i ++) {
  $x += $A[$i];
}
if ($x != $rLineNum) {
  die "utterance lost $x <> $rLineNum\n";
}

my $residual = $uttNum % $split_num;
my $shareNum = int($uttNum / $split_num);
my @shareA = ();
for(my $i = 0; $i < $split_num -1; $i ++) {
  push @shareA, $shareNum;
}
push @shareA, ($shareNum + $residual);
my @shareUttSegA = ();
push @shareUttSegA, $shareA[0];
for(my $i = 1; $i < $split_num; $i ++) {
  $shareUttSegA[$i] = $shareUttSegA[$i-1] + $shareA[$i];
}
my @shareLineSegA = ();
my $uttIndex = 0;
for(my $i = 0; $i < $split_num; $i ++) {
  $shareLineSegA[$i] = 0;
  while($uttIndex <$shareUttSegA[$i]) {
    $shareLineSegA[$i] += $A[$uttIndex];
    $uttIndex ++;
  }
  if($i -1 >=0) {
    $shareLineSegA[$i] += $shareLineSegA[$i-1];
  }
}

SplitFile($rnn_nbest_file, \@shareLineSegA);
my $lineNum = SplitFile($ngram_nbest_file, \@shareLineSegA);
if ($lineNum != $rLineNum) {
  my ($tgtdir, $baseName);
  SplitFileName($ngram_nbest_file, \$tgtdir, \$baseName);
  $tgtdir = sprintf("%s/split%d", $tgtdir, $split_num);
  `rm -rf $tgtdir 2>/dev/null`;
  die "Lines of '$ngram_nbest_file' and '$rnn_nbest_file' are mismatched: $lineNum <> $rLineNum\n";
}
