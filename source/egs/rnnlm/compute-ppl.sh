#!/bin/bash 

. path.sh
. cmd.sh 


echo
echo "## LOG: $0 $@"
echo

# begin options
cmd='slurm.pl --quiet --gres=gpu:1'
nj=6
steps=
ngramlmfile=../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz
ngram_order=3
vocsize=15528
rnnlmtool=/home2/gpc705/workspace/cued-rnnlm/rnnlm.cued.v1.0
# end options

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 $0 [--steps|--ngramlmfile|--ngram-order|--vosize] <data>  <rnnlmdir> <tgtdir>
 
 [Example]:
 $0 --steps 1 --nj $nj --ngramlmfile $ngramlmfile \
  --ngram-order $ngram_order  \
  --vocsize $vocsize \
 ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/dev-man-dominant.txt \
 ../acumen/update-aug-17-with938-50hrs/rnnlm-seame-exp/exp \
 ../acumen/update-aug-17-with938-50hrs/rnnlm-seame-exp/ppl-dev-man-dominant

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 3 ]; then
  Example && exit 1
fi
data=$1
rnnlmdir=$2
tgtdir=$3

[ -d $tgtdir ] || mkdir -p $tgtdir
sdata=$tgtdir/split$nj
if [ ! -z $step01 ]; then
  cat $data | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; s:^\s+::g; s:\s+$::g; if(!/^$/){print "$_\n";}' > $tgtdir/text
  source/egs/rnnlm/split_data_general.pl $tgtdir/text $nj $sdata
  echo "## LOG (step01): done & check '$sdata' "
fi

if [ ! -z $step02 ]; then
  utils/slurm.pl --quiet JOB=1:$nj $tgtdir/log/ngram-ppl.JOB.log \
  ngram -order $ngram_order -lm $ngramlmfile \
  -ppl $sdata/JOB/text -debug 2 \| grep 'p\(' \| \
  awk '{print \$8;}' '>' $tgtdir/ngram.prob.st.JOB
  echo "## LOG (step02): done & check '$tgtdir/ngram.prob.st.*'"
fi

if [ ! -z $step03 ]; then
  rnnlmfile=$(ls $rnnlmdir/rnnlm.*| egrep -v 'input|output')
  inputwlist=$(ls $rnnlmdir/*.index|grep input)
  outputwlist=$(ls $rnnlmdir/*.index|grep output)
  utils/slurm.pl --quiet --gres=gpu:1 JOB=1:$nj \
  $tgtdir/log/rnnlm-ppl.JOB.log \
  $rnnlmtool -ppl -readmodel $rnnlmfile \
  -testfile $sdata/JOB/text -inputwlist $inputwlist -outputwlist $outputwlist \
    -debug 2 -fullvocsize $vocsize -nglmstfile $tgtdir/ngram.prob.st.JOB -lambda 0.5 -nthread 4 '>' $tgtdir/rnnlm-ppl.JOB
  grep Total -A 3 $tgtdir/rnnlm-ppl.* | source/egs/rnnlm/compute-ppl.pl > $tgtdir/rnnlm-ngram-ppl.txt
  # rm $tgtdir/rnnlm-ppl.*
  echo "## LOG (step03): done & check '$tgtdir/rnnlm-ngram-ppl.txt'";
fi


