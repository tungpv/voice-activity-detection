#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
# begin sub

# end sub
my $numArgs = scalar @ARGV;
if($numArgs != 2) {
  die "\nExample: cat word-list.1 | $0 word-list.2 30000 > word-list.merged\n\n";
}
my ($wordCountListFile, $expectedVocSize) = @ARGV;
open(F, "$wordCountListFile") or die;
my %vocab = ();
while(<F>) {
  chomp;
  m:^(\d+)\s+(\S+)$:g or next;
  $vocab{$2} = $1;
}
close F;
my $nVocabSize = keys %vocab;
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m:^(\d+)\s+(\S+)$:g or next;
  my $word = $2;
  if(not exists $vocab{$word} and $nVocabSize < $expectedVocSize) {
    $vocab{$word} = $nVocabSize-1;
    $nVocabSize ++;
  }
}
$vocab{'<OOS>'} = $nVocabSize - 1;
print STDERR "## LOG ($0): stdin ended\n";
foreach my $word  (sort {$vocab{$a} <=> $vocab{$b}} keys %vocab) {
  printf("%-5d %s\n", $vocab{$word}, $word);
}
