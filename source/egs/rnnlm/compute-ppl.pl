#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

print STDERR "## LOG ($0): stdin expected\n";
my $totalWords = 0;
my $oovWords = 0;
my $ngLogProb = 0;
my $rnnLogProb = 0;
my $intLogProb = 0;
while(<STDIN>) {
  chomp;
  if(/Total word:\s+(\d+)\s+OOV word:\s+(\d+)/) {
    $totalWords += $1;
    $oovWords += $2;
  }
  if(/Gram log probability:\s+(\-\d+)/) {
    $ngLogProb += $1;
  }
  if(/RNNLM  log probability:\s+(\-\d+)/) {
    $rnnLogProb += $1;
  }
  if(/Intplt log probability:\s+(\-\d+)/) {
    $intLogProb += $1;
  }
}
print STEDER "## LOG ($0): stdin ended\n";
print "Total word: $totalWords  OOV word: $oovWords\n";
print "N-Gram log probability: $ngLogProb\n";
print "RNNLM  log probability: $rnnLogProb\n";
print "Intplt log probability: $intLogProb\n";

my $effectiveWords = $totalWords - $oovWords;
if($ngLogProb != 0) {
  my $x = 10**(-$ngLogProb/$effectiveWords);
  my $ppl = sprintf("%.3f", $x);
  print"N-Gram PPL : $ppl\n";
}
if ($rnnLogProb != 0) {
  my $x = 10**(-$rnnLogProb/$effectiveWords);
  my $ppl = sprintf("%.3f", $x);
  print "RNNLM  PPL : $ppl\n";
}
if ($intLogProb != 0) {
  my $x = 10**(-$intLogProb/$effectiveWords);
  my $ppl = sprintf("%.3f", $x);
  print "Intplt PPL : $ppl\n";
}
