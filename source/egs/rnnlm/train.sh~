#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd='slurm.pl --quiet --gres=gpu:1'
steps=
ngramlmfile=
ngram_order=3
rnnlmtool=/home2/gpc705/workspace/cued-rnnlm/rnnlm.cued.v1.0
kaldi_text=false
validate_utterance_number=9000
input_dimension=20000
output_dimension=20000
layer_opts="200i:1024r"
batchsize=64
gzsource_data=
datadir=
expdir=exp
rnnlmtune=false
lmtunedir=
tune_data_opts=
# end options


. parse_options.sh || exit 1

function Example {
 cat<<EOF
 $0 [--steps|--ngramlmfile|--nbest|nbestdir] <srcdata> <tgtdir>

 [Example]: $0 --steps 1   \
  --input-dimension $input_dimension \
  --output-dimension $output_dimension \
  --validate-utterance-number $validate_utterance_number \
  --layer-opts "$layer_opts" \
 ../seame-nnet3-feb-06-2017/data/train/fbank-pitch \
  ../acumen/update-aug-17-with938-50hrs/rnnlm-seame-exp

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 2 ]; then
  Example && exit 1
fi

srcdata=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir

data=$datadir
[  -z $data ] && { data=$tgtdir/data; [ -d $data ] || mkdir -p $data; }

if [ ! -z $step01 ]; then
  if $kaldi_text; then
    cat $srcdata/text | perl -ane 'use utf8; use open qw(:std :utf8); m:^(\S+)\s*(.*)$:g; if($2 !~ m:^\s*$:g){print "$2\n";}' | \
    perl -ane 'use utf8; use open qw(:std :utf8); s:<[^<]+>::g; print;' | gzip -c > $data/text.gz
  else
    cat $srcdata/text | perl -ane 'use utf8; use open qw(:std :utf8); if(!/^\s*$/){print "$_";}' | \
    perl -ane 'use utf8; use open qw(:std :utf8); s:<[^<]+>::g; print;' | gzip -c > $data/text.gz
  fi
  gzip -cd $data/text.gz | \
  source/egs/show-word-count.pl | \
  perl -ane  'use utf8; use open qw(:std :utf8); m:(\S+)\s+(\S+):g; if($1 !~ m:^\s+$:g) {print "$2\t$1\n";}' | \
  gzip -c > $data/word-count.gz
  echo "## LOG (step01, $0): done with '$data/text.gz' & '$data/word-count.gz'"
fi

if [ ! -z $step02 ]; then
  gzip -cd $data/word-count.gz | \
  source/egs/rnnlm/make-wordlist.pl '<s>'  $input_dimension > $data/input-word-list.txt
  gzip -cd $data/word-count.gz | \
  source/egs/rnnlm/make-wordlist.pl '</s>' $output_dimension > $data/output-word-list.txt
  echo "## LOG (step02, $0): done with '$data/input-word-list.txt'"
fi
srcdata=$data/text.gz
[ -e $gzsource_data ] && srcdata=$gzsource_data
if [ ! -z $step03 ]; then
  gzip -cd $srcdata | \
  utils/shuffle_list.pl | head -$validate_utterance_number > $data/validate.txt
  gzip -cd $srcdata | \
  utils/shuffle_list.pl | tail -n +$[validate_utterance_number+1] > $data/train.txt
  echo "## LOG (step03, $0): done with '$data/validate.txt' & '$data/train.txt' "  
fi
expdir=$tgtdir/$expdir
[ -d $expdir ] || mkdir -p $expdir
if [ -e $data/input-word-list.txt ] && [ -e $data/output-word-list.txt ]; then
  input_dim=$(wc -l < $data/input-word-list.txt) || exit 1
  output_dim=$(wc -l < $data/output-word-list.txt) || exit 1
  layer_opts=$( perl -e '($input_dim, $output_dim, $layer_opts) = @ARGV; printf("%d:%s:%d", $input_dim, $layer_opts, $output_dim);' $input_dim $output_dim $layer_opts )
  echo "## LOG ($0): layer_opts=$layer_opts" | tee $expdir/layer_opts
fi
if [ ! -z $step04 ]; then
  echo "## LOG (step04): training started @ `date`"
  $cmd $expdir/train.log \
  $rnnlmtool \
  -train -trainfile $data/train.txt -validfile $data/validate.txt  \
  -device -1 -minibatch 64 -chunksize 6 -layers $layer_opts -traincrit ce -lrtune newbob \
  -inputwlist $data/input-word-list.txt \
  -outputwlist $data/output-word-list.txt \
  -debug 2 -randseed 1 -writemodel $expdir/rnnlm.txt -independent 1 -learnrate 1.0 
  echo "## LOG (step04): done @ `date`"
fi

if [ ! -z $step05 ]; then
  if $rnnlmtune; then
    echo "## LOG (step05): training started @ `date`"
    [ -e $expdir/rnnlm.txt ] || \
    { echo "## ERROR (step05): rnnlm.txt expected in '$expdir'"; exit 1;  }
    [ ! -z "$tune_data_opts" ] || \
    { echo "## ERROR (step05): tune_data_opts not specified"; exit 1; }
    [ -z $lmtunedir ] && lmtunedir=fine-tune; tunedir=$expdir/$lmtunedir;  [ -d $tunedir ] || mkdir -p $tunedir
    $cmd $tunedir/train.log \
    $rnnlmtool \
    --readmodel $expdir/rnnlm.txt \
    -train  $tune_data_opts  \
  -device -1 -minibatch $batchsize -chunksize 6 -layers $layer_opts -traincrit ce -lrtune newbob \
  -inputwlist $data/input-word-list.txt \
  -outputwlist $data/output-word-list.txt \
  -debug 2 -randseed 1 -writemodel $tunedir/rnnlm.txt -independent 1 -learnrate 0.5 
  echo "## LOG (step05): done @ `date`"
  fi
fi
