#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
# begin sub

# end sub

my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  die "\nExample: cat text | $0 sublabelname\n\n";
}
my ($fileName) = @ARGV;
my %vocab = ();
open(F, "$fileName") or die;
while(<F>) {
  chomp;
  s:\s+::g;
  $vocab{$_} ++;
}
my %unique = ();
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m:^(\S+)\s+(.*)$: or next;
  if($vocab{$1}) {
    if (not exists $unique{$1}) {
      $unique{$1} ++;
      print "$2\n";
    }
  }
}
print STDERR "## LOG ($0): stdin ended\n";
