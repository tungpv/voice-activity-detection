#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;

# begin sub
sub IsEngWord {
  my ($word) = @_;
  return 0 if ($word =~ /^\p{Han}+$/);
  return 1;
}
sub IsSgeManStartUtterance {
  my($utterance, $sgeFlag) = @_;
  my @A = split(/\s+/, $utterance);
  return 0 if scalar @A < 2;
  my $nCount = 0;
  my $nWindow = 2;
  for(my $i = 0; $i < $nWindow; $i++) {
    if(IsEngWord($A[$i]) == $sgeFlag) {
      $nCount = 0;
      last;
    }
    $nCount ++;
  }
  return 0 if ($nCount == 0);
  return 1;
}
sub IsSgeManEndUtterance {
  my ($utterance, $sgeFlag) = @_;
  my @A = split(/\s+/, $utterance);
  return 0 if scalar @A < 2;
  my $nCount = 0;
  my $nWindow = 2;  my $nSize = scalar @A;
  for(my $i = 0; $i < $nWindow; $i++) {
    if(IsEngWord($A[$nSize-$i-1]) == $sgeFlag) {
      $nCount = 0;
      last;
    }
    $nCount ++;
  }
  return 0 if ($nCount == 0);
  return 1;
}
sub GetLastWord {
  my ($utterance) = @_;
  my @A = split(/\s+/, $utterance);
  my $N = scalar @A;
  return $A[$N-1];
}
sub GetFirstWord {
  my ($utterance) = @_;
  my @A = split(/\s+/, $utterance);
  return  $A[0];
}
# end sub
my $get_hybrid_bigram;
my $min_bigram_count = 3;
GetOptions('get-hybrid-bigram|get_hybrid_bigram' => \$get_hybrid_bigram,
           'min-bigram-count|min_bigram_count=i' => \$min_bigram_count) || die;

my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  die "\n[Example]: $0 <text> <tgtdir>\n\n";
}
my($text, $tgtdir) = @ARGV;

my $nLineNum = 0;
my @utteranceA = ();
my @sgeStartA = ();
my @manStartA = ();
my @sgeEndA = ();
my @manEndA = ();
open(F, "$text") or die;
open(OF, "|cat -") or die;
my %vocab = ();
while(<F>) {
  chomp;
  my $utterance = $_;
  # if($utterance !~ /^\s*$/) {
  #   print OF "$utterance\n";
  # }
  push @utteranceA, $utterance;
  if(IsSgeManStartUtterance($utterance, 0) == 1) {
    push @sgeStartA, $nLineNum;
    # print STDERR "sge-start-utterance: $utterance\n";
  }
  if(IsSgeManEndUtterance($utterance, 0) ==1) {
    push @sgeEndA, $nLineNum;
    # print STDERR "sge-end-utterance: $utterance\n";
  }
  if (IsSgeManStartUtterance($utterance, 1) == 1) {
    push @manStartA, $nLineNum;
    # print STDERR "man-start-utterance: $utterance\n";
  }
  if (IsSgeManEndUtterance($utterance, 1) == 1) {
    push @manEndA, $nLineNum;
    # print STDERR "man-end-utterance: $utterance\n";
  }
  $nLineNum ++;
}
close F;
srand(777);
my $manStartNum = scalar @manStartA;
for(my $i = 0; $i < @sgeEndA; $i++) {
  my $seqIndex = $sgeEndA[$i];
  my $randIndex = int(rand($manStartNum));
  $randIndex = $manStartA[$randIndex];
  my $utterance;
  if($get_hybrid_bigram) {
    $utterance = GetLastWord($utteranceA[$seqIndex]) . ' ' . GetFirstWord($utteranceA[$randIndex]);
  } else {
    $utterance = $utteranceA[$seqIndex] . ' ' . $utteranceA[$randIndex];
  }
  $vocab{$utterance} ++;
}
my $sgeStartNum = scalar @sgeStartA;
for(my $i = 0; $i < @manEndA; $i++) {
  my $seqIndex = $manEndA[$i];
  my $randIndex = int(rand($sgeStartNum));
  $randIndex = $sgeStartA[$randIndex];
  my $utterance;
  if($get_hybrid_bigram) {
    $utterance = GetLastWord($utteranceA[$seqIndex]) . ' ' . GetFirstWord($utteranceA[$randIndex]);
  } else {
    $utterance = $utteranceA[$seqIndex] . ' ' . $utteranceA[$randIndex];
  }
  $vocab{$utterance} ++;
}
foreach my $utterance ( sort {$vocab{$b}<=>$vocab{$a}} keys%vocab) {
  if($vocab{$utterance} >= $min_bigram_count) {
    my @A = split(/\s+/, $utterance);
    my $s = '';
    for(my $i = 0; $i < @A; $i ++) {
      if(IsEngWord($A[$i]) == 0) {
        my @B = split('', $A[$i]);
        $s .= ' ' . join(' ', @B);
      } else {
        $s .= ' ' . $A[$i];
      }
    }
    print OF $s, "\n";
  }
}
close OF;
