#!/bin/bash
tgtdir=../seame-nnet3-feb-06-2017/exp-with-multicondition/learn-error-pattern_no-data-augmentation
mkdir -p  $tgtdir

source/egs/rnnlm/subset-sync-for-learn-error-pattern.pl \
../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-train-multicondition/decode-train-multicondition/scoring_kaldi/penalty_0.0/10.txt \
../seame-nnet3-feb-06-2017/data/train/fbank-pitch/text \
$tgtdir || exit 1

echo "## LOG ($0): done & check '$tgtdir'"
