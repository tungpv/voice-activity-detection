#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd='slurm.pl --quiet --gres=gpu:1'
steps=

# end options

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 [Example]: $0 --steps 1   \
 ../seame-nnet3-feb-06-2017/data/train/fbank-pitch \
 ../acumen/update-aug-17-with938-50hrs/exp/rnnlm

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 2 ]; then
  Example && exit 1
fi

srcdata=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir

data=$tgtdir/data
[ -d $data ] || mkdir -p $data

if [ ! -z $step01 ]; then
  cat $srcdata/text | perl -ane 'use utf8; use open qw(:std :utf8); m:^(\S+)\s*(.*)$:g; if($2 !~ m:^\s+$:g){print "$2\n";}' | \
  perl -ane 'use utf8; use open qw(:std :utf8); s:<[^<]+>::g; print;' | gzip -c > $data/text.gz
  gzip -cd $data/text.gz | \
  source/egs/show-word-count.pl | \
  perl -ane  'use utf8; use open qw(:std :utf8); m:(\S+)\s+(\S+):g; if($1 !~ m:^\s+$:g) {print "$2\t$1\n";}' | \
  gzip -c > $data/word-count.gz
  echo "## LOG (step01, $0): done with '$data/text.gz' & '$data/word-count.gz'"
fi

if [ ! -z $step02 ]; then
  gzip -cd $data/word-count.gz | \
  perl -pe 'use utf8; use open qw(:std :utf8); $i = 1; while(<STDIN>){chomp; m:(\S+)\s+(\S+):g or next; printf("%-5d %s\n", $i, $2); $i ++; }' > $data/input-word-list.txt
  cp $data/input-word-list.txt $data/output-word-list.txt
  echo "## LOG (step02, $0): done with '$data/input-word-list.txt'"
fi
if [ ! -z $step03 ]; then
  gzip -cd $data/text.gz | \
  utils/shuffle_list.pl | head -9000 > $data/validate.txt
  gzip -cd $data/text.gz | \
  utils/shuffle_list.pl | tail -n +9001 > $data/train.txt
  echo "## LOG (step03, $0): done with '$data/validate.txt' & '$data/train.txt' "  
fi
rnnlmdir=$tgtdir/h200.mb64.lr1.0
[ -d $rnnlmdir ] || mkdir -p $rnnlmdir
rnnlmtool=/home2/gpc705/workspace/cued-rnnlm/rnnlm.cued.v1.0
if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): started @ `date`"
  $cmd $rnnlmdir/train.log \
  $rnnlmtool \
  -train -trainfile $data/train.txt -validfile $data/validate.txt  \
  -device -1 -minibatch 64 -chunksize 6 -layers 15527:200i:200r:15527 -traincrit ce -lrtune newbob \
  -inputwlist $data/input-word-list.txt \
  -outputwlist $data/output-word-list.txt \
  -debug 2 -randseed 1 -writemodel $rnnlmdir/rnnlm.sigm.txt -independent 1 -learnrate 1.0 
  echo "## LOG (step04, $0): ended @ `date`"
fi
if [ ! -z $step05 ]; then
  cat $data/input-word-list.txt | \
  awk '{ print $2;}' > $data/words.txt
  cat ../seame-nnet3-feb-06-2017/data/dev-man-dominant/text | \
  perl -ane 'use utf8; use open qw(:std :utf8); m:^(\S+)\s*(.*)$:g; if($2 !~ m:^\s+$:g){print "$2\n";}' | \
  perl -ane 'use utf8; use open qw(:std :utf8); s:<[^<]+>::g; print;' > $data/dev-man-dominant.txt
  cat ../seame-nnet3-feb-06-2017/data/dev-sge-dominant/text | \
  perl -ane 'use utf8; use open qw(:std :utf8); m:^(\S+)\s*(.*)$:g; if($2 !~ m:^\s+$:g){print "$2\n";}' | \
  perl -ane 'use utf8; use open qw(:std :utf8); s:<[^<]+>::g; print;' > $data/dev-sge-dominant.txt
  echo "## LOG (step05, $0): done check '$data/dev-man-dominant.txt' & '$data/dev-sge-dominant.txt'"
fi
ngramlmfile=$data/ngram.3.gz
if [ ! -z $step06 ]; then
  ngram-count -text $data/text.gz -order 3 -lm $data/ngram.3.gz -interpolate -unk -vocab $data/words.txt
  echo "## LOG (step06, $0): done & check '$data/ngram.3.gz'"
fi
if [ ! -z $step07 ]; then
  for dev in dev-man-dominant dev-sge-dominant; do
    ngram -order 3 -lm $data/ngram.3.gz -ppl $data/${dev}.txt -debug 2 | grep 'p(' | \
    awk '{print $8;}' > $data/${dev}.ngram.prob.st
  done
fi
if [ ! -z $step08 ]; then
  for dev in dev-man-dominant dev-sge-dominant; do  
    $cmd $rnnlmdir/ppl-${dev}.log \
    $rnnlmtool -ppl -readmodel $rnnlmdir/rnnlm.sigm.txt \
    -testfile $data/${dev}.txt -inputwlist $data/input-word-list.txt -outputwlist $data/output-word-list.txt \
    -debug 2 -fullvocsize 15527 -nglmstfile $data/${dev}.ngram.prob.st -lambda 0.5 -nthread 4;
  done
  echo "## LOG (step08, $0): done ppl text "
fi
chaindir=../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain
nbest=10
acoustic_scale=0.1
lang=../seame-nnet3-feb-06-2017/data/lang
if [ ! -z $step09 ]; then
  for dev in dev-man-dominant dev-sge-dominant; do
    decodedir=$chaindir/decode-${dev}
    nbestdir=$decodedir/nbest${nbest}; [ -d $nbestdir ] || mkdir -p $nbestdir
    nj=$(cat $decodedir/num_jobs)
    slurm.pl --quiet JOB=1:$nj $nbestdir/log/JOB.log \
    lattice-to-nbest --acoustic-scale=$acoustic_scale --n=$nbest \
   "ark:gzip -cd $decodedir/lat.JOB.gz|" ark:-\|\
   nbest-to-linear ark:- ark:/dev/null  "ark,t:|int2sym.pl -f 2- $lang/words.txt > $nbestdir/text.JOB" \
  "ark,t:$nbestdir/lm-cost.JOB" "ark,t:$nbestdir/ac-cost.JOB" || exit 1
   echo " ## LOG: done with '$decodedir'"
  done
fi
graph=../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/graph
if [ ! -z $step10 ]; then
  for dataname in dev-man-dominant dev-sge-dominant; do
    data=../seame-nnet3-feb-06-2017/data/$dataname
    decodedir=$chaindir/decode-${dataname}
    ./utils/convert_slf_parallel.sh --cmd 'slurm.pl --quiet' $data   $graph $decodedir
  done
fi
nbest=50
if [ ! -z $step11 ]; then
  for dataname in dev-man-dominant dev-sge-dominant; do
    echo "## LOG (step11): started with data $dataname"
    htklatdir=$chaindir/decode-${dataname}/lats-in-htk-slf
    nbestdir=$htklatdir/nbest.$nbest
    [ -d $nbestdir ] || mkdir -p $nbestdir
    example.AMI.clean.v1.0/nbest/bin/lattice-tool.addpron2ac \
  -nbest-decode $nbest -read-htk -htk-logbase 2.718 -htk-lmscale 10  -htk-wdpenalty 0.0 \
  -in-lattice-list $htklatdir/lat_htk.scp -out-nbest-dir $nbestdir/
   echo "## LOG (step11): done with data $dataname"
  done
fi
if [ ! -z $step12 ]; then
  for dataname in dev-man-dominant dev-sge-dominant; do
    echo "## LOG (step12): started with data $dataname"
    nbestdir=$chaindir/decode-${dataname}/lats-in-htk-slf/nbest.$nbest
    gzip -d $nbestdir/*
   echo "## LOG (step12): done with data $dataname"
  done
fi
if [ ! -z $step13 ]; then
  for dataname in dev-man-dominant dev-sge-dominant; do
    echo "## LOG (step13): for data $dataname"
    htklatdir=$chaindir/decode-${dataname}/lats-in-htk-slf
    nbestdir=$htklatdir/nbest.$nbest
    ls $nbestdir/* > $htklatdir/dev.nbestlist
    i=0; 
    for file in `cat $htklatdir/dev.nbestlist` ; do
      cat $file | awk -v i=$i '{printf "%d %s\n", i, $0}';  
      i=`expr $i + 1`; 
    done > $htklatdir/dev.nbest.info.txt
    cat $htklatdir/dev.nbest.info.txt | awk '{printf "%d", $1; for (i=6; i<NF; i++) printf " %s", $i; print ""}' > $htklatdir/dev.nbest.txt
    awk '{for (i=2;i<=NF;i++) printf $i " "; print "";}' $htklatdir/dev.nbest.txt  > $htklatdir/dev.text.txt
    # need to add sentence boundaries here, to ensure the empty sentence could be calculated as well.
    cat $htklatdir/dev.text.txt | awk '{printf "<s> %s </s>\n", $0}' > $htklatdir/dev.text.txt.withsentbound
    echo "## LOG (step13): done with data $dataname"
  done
fi

if [ ! -z $step14 ]; then
  for dataname in dev-man-dominant dev-sge-dominant; do
    echo "## LOG (step14): started with data $dataname"
    htklatdir=$chaindir/decode-${dataname}/lats-in-htk-slf
    nbestdir=$htklatdir/nbest.$nbest
    ngram -order 3 -lm $ngramlmfile -ppl $htklatdir/dev.text.txt.withsentbound -debug 2 > $htklatdir/dev.ngram.scores.txt
    cat $htklatdir/dev.ngram.scores.txt | grep 'p(' | awk '{print $8}' > $htklatdir/dev.ngram.word.scores
    echo "## LOG (step14): done with data $dataname"
  done
fi
srcdata=../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data
if [ ! -z $step15 ]; then
  for dataname in dev-man-dominant dev-sge-dominant; do
    echo "## LOG (step15): started with data $dataname"
    htklatdir=$chaindir/decode-${dataname}/lats-in-htk-slf
    utils/slurm.pl --quiet --gres=gpu:1 $htklatdir/score.log \
    $rnnlmtool -nbest -readmodel $rnnlmdir/rnnlm.sigm.txt \
    -testfile $htklatdir/dev.nbest.info.txt -inputwlist $rnnlmdir/rnnlm.sigm.txt.input.wlist.index \
    -outputwlist $rnnlmdir/rnnlm.sigm.txt.output.wlist.index -debug 0 \
    -fullvocsize 15527 -nglmstfile $htklatdir/dev.ngram.word.scores -lambda 0.5  -nthread 8 '>'  $htklatdir/dev.rnn.1best.rescored.rnn+ngram.txt
    echo "## LOG (step15): ended with data $dataname"
  done
fi
bindir=example.AMI.clean.v1.0/nbest/bin
if [ ! -z $step16 ]; then
  datadir=../seame-nnet3-feb-06-2017/data
  for dataname in dev-man-dominant dev-sge-dominant; do
    echo "## LOG (step16): started with data '$dataname'"
    data=$datadir/$dataname
    htklatdir=$chaindir/decode-${dataname}/lats-in-htk-slf
    $bindir/genAlignmlf.pl $htklatdir/dev.rnn.1best.rescored.rnn+ngram.txt $htklatdir/dev.nbestlist $htklatdir/hypothesis.txt
    cat $htklatdir/hypothesis.txt | \
    local/wer_hyp_filter > $htklatdir/hypothesis_filtered
    cat $data/text | \
    local/wer_ref_filter > $htklatdir/text_filtered
    compute-wer --text --mode=present ark:$htklatdir/text_filtered ark:$htklatdir/hypothesis_filtered > $htklatdir/wer
    echo "## LOG (step16): ended with data '$dataname'"
  done
fi

