#!/usr/bin/perl
# The script is to generate the mlf file for following alignment step.
# usege: perl script.pl 1best.txt latlist align.mlf
if ($#ARGV != 2)
{
	print "Usage: $0 1best.txt latlist align.mlf\n";
	die;
}

$inmlf = shift @ARGV;
$latlist = shift @ARGV;
$outmlf = shift @ARGV;


open FILE_IN_MLF, "<", $inmlf or die "Failed to open $inmlf";
open FILE_LIST, "<", $latlist or die "Failed to open $latlist";
open FILE_OUT_MLF, ">", $outmlf or die "Failed to open $outmlf";
# print the head of mlf file.
# print FILE_OUT_MLF "#!MLF!#\n";

$numfiles = 0;
while (chomp ($line = <FILE_LIST>))
{
	if ($line =~/([^\/]+)$/)
	{
		# $fname = `basename $1`;
		unshift @filenames, $1;
		$numfiles ++;
	}
}
print "Reading $numfiles files totally.\n";


while (chomp ($line = <FILE_IN_MLF>))
{
	if ($line =~ /^\s*(.*?)\s*$/)
  	{
		$numfiles --;
		$content = $1;
		# @items = split /\s+/, $content;
		$filename = pop @filenames;
		# print FILE_OUT_MLF "\"${filename}.rec\"\n";
		# foreach (@items)
		# {
		# 	print FILE_OUT_MLF "$_\n"
		# }
		print FILE_OUT_MLF "$filename $content\n";
	}
}

if ($numfiles != 0) {
  die "Error: The line num in file:$latlist is different from that of file: $inmlf\n";
}



close FILE_IN_MLF;
close FILE_LIST;
close FILE_OUT_MLF;


