#!/usr/bin/perl

if (@ARGV != 2)
{
	print "$0 mlffile txtfile\n";
	die;
}

$mlffile = shift @ARGV;
$txtfile = shift @ARGV;

open FILE_MLF, "<", $mlffile or die "Failed to open $mlffile!\n";
open FILE_TXT, ">", $txtfile or die "Failed to open $txtfile!\n";
while (chomp ($line = <FILE_MLF>))
{
	if ($line =~ /#!MLF/)
	{
		next;
	}
	elsif ($line =~ /^".*\/(\S+).rec/)
	{
		$filename = $1;
		print FILE_TXT "$filename";
	}
	elsif ($line =~ /^\.$/)
	{
		print FILE_TXT "\n";
	}
	elsif ($line =~ /^(\S+)$/)
	{
		print FILE_TXT " $1";
	}
}

close FILE_MLF;
close FILE_TXT;
