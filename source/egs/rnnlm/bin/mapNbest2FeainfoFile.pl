#!/usr/bin/perl

# ../data.lda20/train+dev_lda.file dev.nbestlist dev.nbest.info.txt 
if (@ARGV != 3)
{
	print "Usage: $0 ldainfo nbestlist nbest.info.txt\n";
	die;
}
$ldainfo = shift @ARGV;
$nblist = shift @ARGV;
$nbestinfofile = shift @ARGV;

open FILE_LDAINFO, "<", $ldainfo or die "Failed to open $ldainfo!\n";
open FILE_LIST, "<", $nblist or die "Failed to open $nblist!\n";
open FILE_INFO, "<", $nbestinfofile or die "Failed to open $nbestinfofile!\n";

while (chomp ($line = <FILE_LDAINFO>))
{
	if ($line =~ /^(\d+)\s*(\S+)\s*/)
	{
		$id = $1;
		$show = $2;
		$show2id{$show} = $id;
	}
}

$fileid = 0;
while (chomp($line = <FILE_LIST>))
{
	if ($line =~ /AMI_(\S+?)_(\S+?)_\S+/)
	{
		$show = "$1$2";
		if (not exists $show2id{$show})
		{
			print "Failed to find the feature id for show: $show!\n";
			die;
		}
		$fileid2feaid{$fileid} = $show2id{$show};
		$fileid ++;
	}
}

while (chomp ($line = <FILE_INFO>))
{
	if ($line =~ /^(\d+)(\s+.*)/)
	{
		$fileid = $1;
		$text = $2;
		if (not exists $fileid2feaid{$fileid})
		{
			print "Failed to find the fea id for file id: $fileid!\n";
			die;
		}
		$feaid = $fileid2feaid{$fileid};
		print "$feaid $fileid $text\n";
	}
}



close FILE_LDAINFO;
close FILE_LIST;
close FILE_INFO;
