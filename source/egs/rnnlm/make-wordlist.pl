#!/usr/bin/perl -w
use utf8;
use open qw(:std :utf8);
my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  die "\n[Example]: cat word-count.txt | $0 '<s>|</s>' word-num > words.txt\n\n";
}

my ($boundaryWord, $numWord) = @ARGV;

my $count = 0;
printf("%-5d %s\n", 0, $boundaryWord);
$count ++;
while(<STDIN>) {
  chomp;
  m:^(\d+)\s*(\S+)$:g or next;
  my ($countWord, $word) = ($1, $2);
  if($count < $numWord - 1) {
    printf("%-5d %s\n", $count, $word);
  } else {
    last;
  }
  $count ++;
}
printf("%-5d %s\n", $count, '<OOS>');
