#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
# begin sub
sub NormalizeLine {
  my ($s) = @_;
  $$s =~ s:\s+: :g; $$s =~ s:^\s+::; $$s =~ s:\s+$::;
}
# end sub
my $numArgs = scalar @ARGV;
if($numArgs != 1) {
  die "\nExample: cat text1 | $0 text2 > text-merge\n\n";
}
my ($textFile) = @ARGV;
my %vocab = ();
open(F, "$textFile") or die;
while(<F>) {
  chomp;
  NormalizeLine(\$_);
  next if (exists $vocab{$_});
  $vocab{$_} ++;
  print "$_\n";
}
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  NormalizeLine(\$_);
  next if exists $vocab{$_};
  $vocab{$_} ++;
  print "$_\n";
}
print STDERR "## LOG ($0): stdin ended\n";
