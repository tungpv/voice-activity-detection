#!/bin/bash 

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd='slurm.pl --quiet'
nj=120
steps=
dataname=train-multicondition
# end options


. parse_options.sh || exit 1

function Example {
 cat<<EOF
  $0 <data> <ivector-extractor> <graph> <srcdir> <dir>
 [Example]: $0 --steps 1 --cmd "$cmd" --nj $nj \
 --dataname $dataname \
 ../seame-nnet3-feb-06-2017/data/train-multicondition/mfcc-hires \
 ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/extractor \
 ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/graph \
 ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain \
 ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-train-multicondition
EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 5 ]; then
  Example && exit 1
fi

data=$1;
ivector_extractor=$2
graphdir=$3
srcdir=$4
dir=$5
ivectordir=$dir/ivectors
if [ ! -z $step01 ]; then
  echo "## LOG (step01): ivector extraction for data '$data' started @ `date`"
   steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj \
    $data $ivector_extractor $ivectordir || touch $ivectordir/.error
  echo "## LOG (step01): done @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02): decodine started @ `date`"
   steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
          --nj $nj --cmd "$cmd" \
          --online-ivector-dir $ivectordir \
          --srcdir $srcdir \
          $graphdir $data  $dir/decode-${dataname} || exit 1;
  echo "## LOG (step02): done @ `date`"
fi
