#!/bin/bash 

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd='slurm.pl --quiet --gres=gpu:1'
nj=6
steps=
ngramlmfile=
nbest=50
nbestdir=
lmscale=10
ngram_order=3
rnnlmtool=/home2/gpc705/workspace/cued-rnnlm/rnnlm.cued.v1.0
vocsize=15528
rescoreid=rescore-01
# end options

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 $0 [--steps|--ngramlmfile|--nbest|nbestdir] <srcdata> <htklatdir> <rnnlmdir> <tgtdir>

 [Example]: $0 --steps 1   \
 --lmscale $lmscale \
 --nbest $nbest \
 --ngram-order $ngram_order \
 --vocsize $vocsize \
 --rescoreid $rescoreid \
 --nbestdir ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/nbest.50 \
 --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz \
 ../seame-nnet3-feb-06-2017/data/dev-man-dominant/fbank-pitch \
 ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf \
 ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/h200.mb64.lr1.0  \
 ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/rescore-01-nbest$nbest

  sbatch -o ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/rescore-01-nbest50/rescore-01.log  source/egs/rnnlm/rescore.sh --steps 4-6    --lmscale 10  --nbest 50  --ngram-order 3  --vocsize 15528  --rescoreid rescore-01  --nbestdir ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/nbest.50  --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz  ../seame-nnet3-feb-06-2017/data/dev-man-dominant/fbank-pitch  ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf  ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/h200.mb64.lr1.0   ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/rescore-01-nbest50

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 4 ]; then
  echo "# LOG: rest argument: $@"
  Example && exit 1
fi

srcdata=$1
htklatdir=$2
rnnlmdir=$3
tgtdir=$4

[ -d $tgtdir ] || mkdir -p $tgtdir
[ -z $nbestdir ] && nbestdir=$tgtdir/nbest.${nbest}
[ -d $nbestdir ] || mkdir -p $nbestdir
bindir=source/egs/rnnlm/bin
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): making nbest started @ `date`"
  [ -f $htklatdir/lat_htk.scp ] || \
  { echo "## ERROR (step01, $0): '$htklatdir/lat_htk.scp' expected"; exit 1;  }
  echo "## LOG (step01): bestdir=$nbestdir"
  $bindir/lattice-tool.addpron2ac \
  -nbest-decode $nbest -read-htk -htk-logbase 2.718 -htk-lmscale $lmscale  -htk-wdpenalty 0.0 \
  -in-lattice-list $htklatdir/lat_htk.scp -out-nbest-dir $nbestdir/ || exit 1
  echo "## LOG (step01, $0): done @ `date`, '$nbest'"
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): uncompress nbest file @ `date`"
  gzip  -d $nbestdir/*
  ls $nbestdir/* > $tgtdir/nbestlist
  echo "## LOG (step02, $0): done with uncompression @ `date` & check '$nbestdir'"
fi

if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): data preparation for rescoring @ `date`"
    i=0; 
    for file in `cat $tgtdir/nbestlist` ; do
      cat $file | awk -v i=$i '{printf "%d %s\n", i, $0}';  
      i=`expr $i + 1`; 
    done > $tgtdir/nbest.info.txt
    cat $tgtdir/nbest.info.txt | awk '{printf "%d", $1; for (i=6; i<NF; i++) printf " %s", $i; print ""}' > $tgtdir/nbest.txt
    awk '{for (i=2;i<=NF;i++) printf $i " "; print "";}' $tgtdir/nbest.txt  > $tgtdir/nbest-text
    # need to add sentence boundaries here, to ensure the empty sentence could be calculated as well.
    cat $tgtdir/nbest-text | awk '{printf "<s> %s </s>\n", $0}' > $tgtdir/nbest-text.withsentbound
  echo "## LOG (step03, $0): done & check '$htklatdir/nbest-text.withsentbound' @ `date`"
fi
resdir=$tgtdir/$rescoreid
sdata=$tgtdir/split$nj
if [ ! -d $sdata ]; then
  source/egs/rnnlm/split_data.pl $tgtdir/nbest.info.txt $tgtdir/nbest-text.withsentbound $nj
fi
[ -d $resdir ] || mkdir -p $resdir
if [ ! -z $step04 ]; then
  if [ ! -z $ngramlmfile ]; then
    utils/slurm.pl --quiet JOB=1:$nj $resdir/ngram.word.ppl.JOB \
    ngram -order $ngram_order -lm $ngramlmfile \
    -ppl $sdata/JOB/nbest-text.withsentbound -debug 2 
    for x in $(seq 1 $nj); do
      cat $resdir/ngram.word.ppl.$x |grep 'p(' | awk '{print $8}' > $resdir/ngram.word.scores.$x
    done
    rm $resdir/ngram.word.ppl.* 
    echo "## LOG (step04, $0): done '$tgtdir/ngram.word.scores'"
  fi
fi
ngramlm_opts='-lambda 1.0'
[ -f $resdir/ngram.word.scores.1 ] && ngramlm_opts="-nglmstfile $resdir/ngram.word.scores.JOB -lambda 0.5"
if [ ! -z $step05 ]; then
  rnnfile=$(ls $rnnlmdir/rnnlm.*| egrep -v 'input|output')
  inputwlist=$(ls $rnnlmdir/*.index|grep input)
  outputwlist=$(ls $rnnlmdir/*.index|grep output)
  for x in $rnnfile $inputwlist $outputwlist; do
    [ -f $x ] || \
    { echo "## ERROR (step05, $0): file '$x' expected"; exit 1; }
  done
  echo "## LOG (step05, $0): rescore started @ `date`"
  utils/slurm.pl --quiet --gres=gpu:1 JOB=1:$nj  $resdir/log/score.JOB.log \
  $rnnlmtool -nbest -readmodel $rnnfile \
  -testfile $sdata/JOB/nbest.info.txt -inputwlist $inputwlist \
  -outputwlist $outputwlist -debug 0 \
  -fullvocsize $vocsize $ngramlm_opts -nthread 1 '>' $resdir/rnn.1best.rescored.JOB.txt
  for x in $(seq 1 $nj); do
    cat $resdir/rnn.1best.rescored.$x.txt
  done > $resdir/rnn.1best.rescored.txt
  rm $resdir/rnn.1best.rescored.*.txt 2>/dev/null
  rm $resdir/ngram.word.scores.* 2>/dev/null
  echo "## LOG (step05, $0): rescore done & check '$tgtdir/rnn.1best.rescored.txt' @ `date`"
fi

if [ ! -z $step06 ]; then
  $bindir/genAlignmlf.pl $resdir/rnn.1best.rescored.txt $tgtdir/nbestlist $resdir/hypothesis.txt
  cat $resdir/hypothesis.txt | \
  local/wer_hyp_filter > $resdir/hypothesis_filtered
  cat $srcdata/text | \
  local/wer_ref_filter > $tgtdir/text_filtered
  compute-wer --text --mode=present ark:$tgtdir/text_filtered ark:$resdir/hypothesis_filtered > $resdir/wer
  echo "## LOG (step16, $0): done & '$resdir/wer'"
fi
