#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd='slurm.pl --quiet --gres=gpu:1'
steps=

# end options

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 [Example]: $0 --steps 1   \
 ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/train.txt \
 ../rnnlm/run-rnnlm-26-aug

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 2 ]; then
  Example && exit 1
fi

srcdata=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir

data=$tgtdir/data
[ -d $data ] || mkdir -p $data

if [ ! -z $step01 ]; then
  source/egs/rnnlm/permut-utterance.pl $srcdata $tgtdir/data
  echo "## LOG (step01): done & check '$tgtdir/data'"
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02): rnnlm training started @ `date`"
  source/egs/rnnlm/train.sh --steps 1-6 --kaldi-text false \
  --input-dimension 20000   --output-dimension 20000   --validate-utterance-number 14000   --layer-opts "200i:1024r"  $data   $tgtdir || exit 1
  echo "## LOG (step02): done @ `date`"  
fi

if [ ! -z $step03 ]; then
   echo "## LOG (step03): ppl test started @ `date`"
   source/egs/rnnlm/compute-ppl.sh --steps 1-3 --nj 6 --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz   --ngram-order 3\
   --vocsize 15528  ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/dev-man-dominant.txt  $tgtdir/exp  $tgtdir/exp/ppl-dev-man-dominant
   echo "## LOG (step03): ppl done '$tgtdir/exp/ppl-dev-man-dominant' @ `date`"
fi
data2=$tgtdir/data2
[ -d $data2 ] ||mkdir -p $data2
if [ ! -z $step04 ]; then
  cat ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/{train.txt,validate.txt} \
  /home3/hhx502/w2017/seame-nnet3-feb-06-2017/exp-with-multicondition/learn-error-pattern_dev-man-dominant/hyp.txt > $data2/text
fi

if [ ! -z $step05 ]; then
  echo "## LOG (step05): rnnlm training started @ `date`"
  source/egs/rnnlm/train.sh --steps 1-6 --kaldi-text false --datadir $data2 --expdir exp2-semi\
  --input-dimension 20000   --output-dimension 20000   --validate-utterance-number 10000   --layer-opts "200i:1024r"  $data2   $tgtdir || exit 1
  echo "## LOG (step05): done @ `date`"  
fi

if [ ! -z $step06 ]; then
   echo "## LOG (step06): ppl test started @ `date`"
   source/egs/rnnlm/compute-ppl.sh --steps 1-3 --nj 6 --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz   --ngram-order 3\
   --vocsize 15528  ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/dev-man-dominant.txt  $tgtdir/exp2-semi  $tgtdir/exp2-semi/ppl-dev-man-dominant
   echo "## LOG (step06): ppl done '$tgtdir/exp/ppl-dev-man-dominant' @ `date`"
fi

if [ ! -z $step07 ]; then
   echo "## LOG (step07): rescore started @ `date`"
  source/egs/rnnlm/rescore.sh --steps 4-6    --lmscale 10  --nbest 50  --ngram-order 3  --vocsize 15528  --rescoreid rescore-exp2-semi  --nbestdir ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/nbest.50  --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz  ../seame-nnet3-feb-06-2017/data/dev-man-dominant/fbank-pitch  ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf  $tgtdir/exp2-semi   ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/rescore-01-nbest50
   echo "## LOG (step07): rescore done '$tgtdir/exp/ppl-dev-man-dominant' @ `date`"
fi
data3=$tgtdir/data3-augmentation
[ -d $data3 ] || mkdir -p $data3
if [ ! -z $step08 ]; then
  echo "## LOG (step08): data preparation @ `date`"
  cat ../seame-nnet3-feb-06-2017/exp-with-multicondition/learn-error-pattern/ref.txt | \
  source/egs/rnnlm/merge_data.pl ../seame-nnet3-feb-06-2017/exp-with-multicondition/learn-error-pattern/hyp.txt > $data3/text
  echo "## LOG (step08): done with '$data3/text' @ `date`"
fi
if [ ! -z $step09 ]; then
  echo "## LOG (step09): rnnlm training started @ `date`"
  source/egs/rnnlm/train.sh --steps 1-6 --kaldi-text false --datadir $data3 --expdir exp3-augmentation-semi\
  --input-dimension 20000   --output-dimension 20000   --validate-utterance-number 30000   --layer-opts "200i:1024r"  $data3   $tgtdir || exit 1
  echo "## LOG (step09): done @ `date`"  
fi
if [ ! -z $step10 ]; then
   echo "## LOG (step10): ppl test started @ `date`"
   source/egs/rnnlm/compute-ppl.sh --steps 1-3 --nj 6 --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz   --ngram-order 3\
   --vocsize 15528  ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/dev-man-dominant.txt  $tgtdir/exp3-augmentation-semi  $tgtdir/exp3-augmentation-semi/ppl-dev-man-dominant
   echo "## LOG (step10): ppl done '$tgtdir/exp/ppl-dev-man-dominant' @ `date`"
fi
if [ ! -z $step11 ]; then
  echo "## LOG (step11): rescore started @ `date`"
  source/egs/rnnlm/rescore.sh --steps 4-6    --lmscale 10  --nbest 50  --ngram-order 3  --vocsize 15528  --rescoreid rescore-exp3-augumentation-semi  --nbestdir ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/nbest.50  --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz  ../seame-nnet3-feb-06-2017/data/dev-man-dominant/fbank-pitch  ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf  $tgtdir/exp3-augmentation-semi   ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/rescore-01-nbest50
  echo "## LOG (step11): rescore done @ `date`"
fi
data4=$tgtdir/data4-aug2
[ -d $data4 ] || mkdir -p $data4
if [ ! -z $step12 ]; then
  cat ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-train-multicondition/decode-train-multicondition/scoring_kaldi/penalty_0.0/10.txt | \
  source/egs/rnnlm/select_data.pl "cut -d' ' -f1 ../seame-nnet3-feb-06-2017/data/train/text|" | \
  cat - <(gzip -cd ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/text.gz) > $data4/text
  echo "## LOG (step12): done & check '$data4/text'"
fi
expid=exp4-aug2-semi
if [ ! -z $step13 ]; then
  echo "## LOG (step13): rnnlm training started @ `date`"
  source/egs/rnnlm/train.sh --steps 1-6 --kaldi-text false --datadir $data4 --expdir $expid\
  --input-dimension 20000   --output-dimension 20000   --validate-utterance-number 18000   --layer-opts "200i:1024r"  $data4   $tgtdir || exit 1
  echo "## LOG (step13): done @ `date`"  
fi

if [ ! -z $step14 ]; then
   echo "## LOG (step14): rescore started @ `date`"
  source/egs/rnnlm/rescore.sh --steps 4-6    --lmscale 10  --nbest 50  --ngram-order 3  --vocsize 15633  --rescoreid rescore-exp4-aug2-semi  --nbestdir ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/nbest.50  --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz  ../seame-nnet3-feb-06-2017/data/dev-man-dominant/fbank-pitch  ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf  $tgtdir/exp4-aug2-semi   ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/rescore-01-nbest50
  echo "## LOG (step14): rescore done @ `date`"
fi
srcdata15=../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data
data5=$tgtdir/data5-bigram
[ -d $data5 ] || mkdir -p $data5
if [ ! -z $step15 ]; then
  gzip -cd $srcdata15/text.gz| \
  source/egs/mandarin/update-april-03-2017-with-pruned-lexicon/segment-chinese-text.py --do-word-segmentation  | \
  gzip -c > $srcdata15/word-text.gz
  source/egs/rnnlm/permut-utterance.pl --get-hybrid-bigram --min-bigram-count=1 "gzip -cd $srcdata15/word-text.gz|" $srcdata15 | \
  cat - <(gzip -cd $srcdata15/text.gz) > $data5/text
  echo "## LOG ($step15): done & check '$data5/text'"
fi
expdir=exp5-aug-with-bigram
if [ ! -z $step16 ]; then
  echo "## LOG (step16): rnnlm training started @ `date`"
  source/egs/rnnlm/train.sh --steps 1-6 --kaldi-text false --datadir $data5 --expdir $expdir\
  --input-dimension 20000   --output-dimension 20000   --validate-utterance-number 13000   --layer-opts "200i:1024r"  $data5   $tgtdir || exit 1
  echo "## LOG (step16): done @ `date`"  
fi
if [ ! -z $step17 ]; then
   echo "## LOG (step17): rescore started @ `date`"
  source/egs/rnnlm/rescore.sh --steps 4-6  --nj 10   --lmscale 10  --nbest 50  --ngram-order 3  --vocsize 15633  --rescoreid $expdir  --nbestdir ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/nbest.50  --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz  ../seame-nnet3-feb-06-2017/data/dev-man-dominant/fbank-pitch  ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf  $tgtdir/$expdir   ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/rescore-01-nbest50
  echo "## LOG (step17): rescore done @ `date`"
fi
data6=$tgtdir/data6-for-more-layers
[ -d $data6 ] || mkdir -p $data6
if [ ! -z $step18 ]; then
  gzip -cd $srcdata15/text.gz > $data6/text
fi
expdir=exp6-for-more-layers
if [ ! -z $step19 ]; then
  echo "## LOG (step19): rnnlm training started @ `date`"
  source/egs/rnnlm/train.sh --steps 1-6 --kaldi-text false --datadir $data6 --expdir $expdir\
  --input-dimension 20000   --output-dimension 20000   --validate-utterance-number 8000   --layer-opts "200i:1024r:1024f"  $data6   $tgtdir || exit 1
  echo "## LOG (step19): done @ `date`"  
fi
if [ ! -z $step20 ]; then
  echo "## LOG (step20): rescore started @ `date`"
  source/egs/rnnlm/rescore.sh --steps 4-6  --nj 10   --lmscale 10  --nbest 50  --ngram-order 3  --vocsize 15633  --rescoreid $expdir  --nbestdir ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/nbest.50  --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz  ../seame-nnet3-feb-06-2017/data/dev-man-dominant/fbank-pitch  ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf  $tgtdir/$expdir   ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/rescore-01-nbest50
  echo "## LOG (step20): rescore done @ `date`"
fi

data7=$tgtdir/data7-using-more-data
[ -d $data7 ] || mkdir -p $data7
if [ ! -z $step21 ]; then
  echo "## LOG (step21): prepare data"
  cat /home2/zpz505/seame/formatted/hkust | \
  ./source/egs/seame/decompose-mandarin-word.pl|\
  cat - /home2/zpz505/seame/formatted/english | gzip -c > $data7/text.sge.hkust.gz
  echo "## LOG (step21): data preparation done"
fi
if [ ! -z $step22 ]; then
  echo "## LOG (step22): prepar word-count"
  gzip -cd $data7/text.sge.hkust.gz | \
  source/egs/show-word-count.pl | \
  perl -ane  'use utf8; use open qw(:std :utf8); m:(\S+)\s+(\S+):g; if($1 !~ m:^\s+$:g) {print "$2\t$1\n";}' | \
  gzip -c > $data7/word-count.sge-hkust.gz
  echo "## LOG (step22): done & check '$data7/word-count.sge-hkust.gz'"
fi
if [ ! -z $step23 ]; then
  gzip -cd $data7/word-count.sge-hkust.gz | \
  source/egs/rnnlm/merge-word-count-list.pl ../rnnlm/run-rnnlm-26-aug/data6-for-more-layers/input-word-list.txt 30000 > $data7/input-word-list.txt
  echo "## LOG (step23): done & check '$data7/input-word-list.txt'"
fi
if [ ! -z $step24 ]; then
  gzip -cd $data7/word-count.sge-hkust.gz | \
  source/egs/rnnlm/merge-word-count-list.pl ../rnnlm/run-rnnlm-26-aug/data6-for-more-layers/output-word-list.txt 20000 \
  >$data7/output-word-list.txt
  echo "## LOG (step24): done & check '$data7/output-word-list.txt'"
fi
if [ ! -z $step25 ]; then
  gzip -cd $data7/text.sge.hkust.gz ../rnnlm/run-rnnlm-26-aug/data6-for-more-layers/text.gz  | \
  gzip -c > $data7/text.gz
  echo "## LOG (step25): done & check '$data7/text.gz'"
fi
expdir7=exp7-sge-hkust-more-data
if [ ! -z $step26 ]; then
   echo "## LOG (step26): training started @ `date`"
   source/egs/rnnlm/train.sh --steps 3-4 --kaldi-text false --datadir $data7 --expdir $expdir7\
  --input-dimension 30000   --output-dimension 20000   --validate-utterance-number 40000   --layer-opts "200i:1024r:1024f"  $data7   $tgtdir || exit 1
  echo "## LOG (step26): done & check '$tgtdir/$expdir7'"
fi
if [ ! -z $step27 ]; then
  echo "## LOG (step27): rescore started @ `date`"
  source/egs/rnnlm/rescore.sh --steps 4-6  --nj 10   --lmscale 10  --nbest 50  --ngram-order 3  --vocsize 28287  --rescoreid $expdir7  --nbestdir ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/nbest.50  --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz  ../seame-nnet3-feb-06-2017/data/dev-man-dominant/fbank-pitch  ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf  $tgtdir/$expdir7   ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/rescore-01-nbest50
  echo "## LOG (step27): rescore done @ `date`"
fi

if [ ! -z $step28 ]; then
  echo "## LOG (step28): tuning started @ `date`"
   source/egs/rnnlm/train.sh --steps 5 --rnnlmtune true \
   --lmtunedir fine-tune \
   --datadir $data7 \
   --tune-data-opts "-trainfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/train.txt -validfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/validate.txt"  --expdir $expdir7\
  --input-dimension 30000   --output-dimension 20000   --validate-utterance-number 40000   --layer-opts "200i:1024r:1024f"  $data7   $tgtdir || exit 1
  echo "## LOG (step28): done & check '$tgtdir/$expdir7' @ `date`"
fi
expdir8=exp8-tune-sge-hkust-more-data
if [ ! -z $step29 ]; then
  echo "## LOG (step29): rescore started @ `date`"
  source/egs/rnnlm/rescore.sh --steps 4-6  --nj 10   --lmscale 10  --nbest 50  --ngram-order 3  --vocsize 28287  --rescoreid $expdir8  --nbestdir ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/nbest.50  --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz  ../seame-nnet3-feb-06-2017/data/dev-man-dominant/fbank-pitch  ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf  ../rnnlm/run-rnnlm-26-aug/exp7-sge-hkust-more-data/fine-tune   ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/rescore-01-nbest50
  echo "## LOG (step29): rescore done @ `date`"
fi

if [ ! -z $step30 ]; then
  echo "## LOG (step30): tuning started @ `date`"
   source/egs/rnnlm/train.sh --steps 5 --rnnlmtune true \
   --lmtunedir fine-tune-semi \
   --datadir $data7 \
   --tune-data-opts "-trainfile $data2/train.txt  -validfile  $data2/validate.txt"  --expdir $expdir7\
  --input-dimension 30000   --output-dimension 20000   --validate-utterance-number 40000   --layer-opts "200i:1024r:1024f"  $data7   $tgtdir || exit 1
  echo "## LOG (step30): done & check '$tgtdir/$expdir7/fine-tune-semi' @ `date`"
fi
expdir9=exp9-tune-semi-sge-hkust-more-data
if [ ! -z $step31 ]; then
  echo "## LOG (step31): rescore started @ `date`"
  source/egs/rnnlm/rescore.sh --steps 4-6  --nj 10   --lmscale 10  --nbest 50  --ngram-order 3  --vocsize 28287 \
  --rescoreid $expdir9  --nbestdir ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/nbest.50 \
  --ngramlmfile ../acumen/update-aug-17-with938-50hrs/exp/rnnlm/data/ngram.3.gz  ../seame-nnet3-feb-06-2017/data/dev-man-dominant/fbank-pitch  \
  ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf \
  ../rnnlm/run-rnnlm-26-aug/exp7-sge-hkust-more-data/fine-tune-semi  \
 ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-dev-man-dominant/lats-in-htk-slf/rescore-01-nbest50
  echo "## LOG (step29): rescore done @ `date`"
fi
