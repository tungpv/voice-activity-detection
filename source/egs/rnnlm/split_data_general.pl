#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;

if ($numArgs != 3) {
  die "\n[Example]: $0 <text> <nshare> <tgtdir>\n\n";
}

my ($text, $nShare, $tgtdir) = @ARGV;
# begin sub
sub SplitFileName {
  my ($fileName, $pathName, $baseName) = @_;
  $fileName =~ m:(^.*)\/([^\/]+)$:g;
  $$pathName = $1;
  $$baseName = $2;
}
sub SaveSplitFile {
  my ($ostream, $nShareIndex, $tgtdir) = @_;
  $$nShareIndex ++;
  my $outputFile = sprintf("%s/%d/text", $tgtdir, $$nShareIndex);
  my ($dir, $fileName);
  SplitFileName($outputFile, \$dir, \$fileName);
  `[ -d $dir ] || mkdir -p $dir`;
  open(O, ">$outputFile") or die;
  print O "$$ostream";
  close O;
}
# being sub
open(F, "$text") or die;
my $nLine = 0;
while(<F>) {
  chomp;
  $nLine ++;
}
close F;
my $nResidual = $nLine % $nShare;
my $nLinePerShare = ($nLine -$nResidual)/$nShare;
my @A = ();
for(my $i = 0; $i<$nShare-1; $i++) {
  push @A, $nLinePerShare;
}
push @A, ($nLinePerShare+$nResidual);
for(my $i = 1; $i < $nShare; $i++) {
  $A[$i] += $A[$i-1];
}
my $nLineIndex = 0;
my $nShareIndex = 0;
open(F, "$text") or die;
my $ostream = '';
while(<F>) {
  chomp;
  if($nLineIndex < $A[$nShareIndex]) {
    $ostream .= "$_\n";
  } else {
    SaveSplitFile(\$ostream, \$nShareIndex, $tgtdir);
    $ostream = "$_\n";
  }
  $nLineIndex ++;
}
close F;
if ($ostream ne '') {
  SaveSplitFile(\$ostream, \$nShareIndex, $tgtdir);
}
