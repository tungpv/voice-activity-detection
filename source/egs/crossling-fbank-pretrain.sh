#!/bin/bash

./source/test/run-nnet.sh  --mling-dnn-dir ../viet107/llp/exp/mling.flp4/nnet  --tgt-dir llp2/exp/crossling.bern.fbank_pretrain_h1 ${disabled:+--crossling_fbank_train true --prepare_fbank_test true} --crossling_fbank_test true --transfer-opts "--learn-rate 0.008 --hid-dim 2048 --remove-last-layers 2 --hid-layers 1 --do-pretrain"  --fbankCmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf" --preTrnCmd "steps/nnet/pretrain_dbn.sh --input-vis-type bern --splice 5" ${disabled:+--steps 1,2,3,6}  llp2/data llp2/data/train/plp_pitch  llp2/exp llp2/exp/tri4a
