#!/bin/bash
state_num=2000
gauss_num=30000
gmm_name=gmm-sem.0.9.bnf-2k-30k-revisit
steps=

. path.sh
. cmd.sh
echo
echo "LOG: $0 $@" 
echo

. parse_options.sh

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
[ -d ./log ] || mkdir ./log
data=./grapheme_vllp/exp/tuned.hierbn.mling6/sem.0.9.bnf
lang=./grapheme_vllp/data/lang.bd.0.05.201421
ali=./grapheme_vllp/exp/gmm.tuned.bnf.mling6/tri4a/ali-sem.0.9.bnf
if [ ! -z $step01 ]; then
  source/egs/run-bnf.sh --make_ali true --make_ali_opts \
  "$data $lang ./grapheme_vllp/exp/gmm.tuned.bnf.mling6/tri4a $ali" --ali_cmd "steps/align_fmllr.sh --nj 20" \
  > log/ali-VLLP-sem.0.9.bnf-gmm.tuned.bnf.mling6.log 2>&1
fi

sdir=grapheme_vllp/exp/$gmm_name/tri4a
if [ ! -z $step02 ]; then
  source/egs/run-bnf.sh --run_gmm2 true --run_gmm2_import_tri5_alidir $ali --run_gmm2_opts \
  "a $data sem.0.9.bnf $lang $state_num $gauss_num $(dirname $sdir)" > log/gmm-VLLP-${gmm_name}.log 2>&1
fi
graph=$sdir/graph-$(basename $lang)
if [ ! -z $step03 ]; then
  ./utils/mkgraph.sh $lang $sdir $graph
fi
if [ ! -z $step04 ]; then
  source/egs/run-bnf.sh --decoding_eval true --decoding_opts "grapheme_vllp/exp/tuned.hierbn.mling6/dev.bnf  $lang  $sdir  $graph  $sdir/decode-dev.bnf-$(basename $lang) steps/decode_fmllr.sh" \
  --lattice-opts ' '   > log/decode-VLLP-dev-${gmm_name}.log 2>&1 &
fi

