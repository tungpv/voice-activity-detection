#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=run.pl
nj=20
steps=
prepare_frame_weights=false
prepare_opts="<sup_data> <un_lmwt> <un_latdir> <dir>"
frame_weight_train=false
frame_weights=
frame_weight_train_opts="<feature_transform> <learn_rate> <dbn> <feats_tr> <feats_cv> <lang> <labels_tr> <labels_cv> <dir>"
prepare_semi_data=false
lattice_to_ctm_opts="<lmwt> <data> <lang> <sdir> <latdir>"
prepare_step02_opts="<conf_thresh> <merge_tolerance> <sdata> <utt.ctm> <dir>"

# end options

echo
echo "$0 $@"
echo

. parse_options.sh

function PrintOptions {
  cmdName=$(echo $0| perl -pe 's/^.*\///g;')
  cat <<END

$cmdName [options]:
cmd					# value, "$cmd"
nj					# value, "$nj"
steps					# value, "$steps"
prepare_frame_weights			# value, $prepare_frame_weights
prepare_opts				# value, "$prepare_opts"
frame_weight_train			# value, $frame_weight_train
frame_weights				# value, "$frame_weights"
frame_weight_train_opts			# value, "$frame_weight_train_opts"
prepare_semi_data			# value, $prepare_semi_data
lattice_to_ctm_opts			# value, "$lattice_to_ctm_opts"
prepare_semi_data			# value, $prepare_semi_data
prepare_step02_opts			# value, "$prepare_step02_opts"

END
}

PrintOptions;

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=true
  done
fi
if $prepare_frame_weights; then
  echo "$0: prepare_frame_weights started @ `date`"
  optNames="data lmwt latdir dir"
  . source/register_options.sh "$optNames" "$prepare_opts" || \
  { echo "$0: prepare_frame_weights: ERROR, @register_options.sh"; exit 1;}
  if [ ! -z $step01 ]; then
    echo "step01: prepare unsupervised part started @ `date`"
    [ -f $latdir/lat.1.gz ] || \
    { echo "step01: lat.1.gz expected "; exit 1;}
    nj=$(cat $latdir/num_jobs) 
    acwt=$(echo "1/$lmwt"|bc -l)
    $cmd JOB=1:$nj $latdir/frame_weights.JOB.log \
    lattice-to-post --acoustic-scale=$acwt ark:"gzip -cd $latdir/lat.JOB.gz|" ark:- \| \
    source/code/post-to-weights --maximum=true ark:- ark,t:- \| \
    gzip -c  ">&" $latdir/frame_weights.JOB.gz || exit 1
    echo "$0: prepare unsupervised part ended @ `date`"
  fi
  if [ ! -z $step02 ];  then
    echo "step02: preparing frame_weights for the supervised data"
    [ -f $data/feats.scp ] || \
    { echo "prepare_frame_weights: step02: feats.scp expected"; exit 1; }
    feat-to-len scp:$data/feats.scp ark,t:- | \
    perl -pe '@A=split(/\s+/);  $s="$A[0] [";for($i=0; $i<$A[1]; $i++){ $s .=" 1";} $s .= " ]"; $_="$s\n";' | \
    gzip -c > $data/frame_weights.gz
    echo "prepare_frame_weights: step03: done!"
  fi
  if [ ! -z $step03 ]; then
    [ -d $dir ] || mkdir -p $dir
    cat <(gzip -cd $latdir/frame_weights.*.gz) <(gzip -cd $data/frame_weights.gz) | \
    gzip -c > $dir/frame_weights.gz
  fi
  echo "$0: prepare_frame_weights ended @ `date`"
fi
if $frame_weight_train; then
  echo "$0: frame_weight_train started @ `date`"
  optNames="feature_transform learn_rate  dbn   feats_tr feats_cv lang labels_tr labels_cv dir"
  . source/register_options.sh "$optNames" "$frame_weight_train_opts"
  steps/nnet/train.sh \
  --feature-transform $feature_transform \
  --learn-rate $learn_rate \
  ${frame_weights:+ --frame-weights "$frame_weights"} \
  --hid-dim 1024 --hid-layers 0 --dbn $dbn "$feats_tr" "$feats_cv" $lang "$labels_tr" "$labels_cv" $dir || exit 1

  echo "$0: frame_weight_train ended @ `date`"
fi
if $prepare_semi_data; then
  echo "$0: prepare_semi_data started @ `date`"
  if [ ! -z $step01 ]; then
    optNames="lmwt data lang sdir latdir"
    . source/register_options.sh "$optNames" "$lattice_to_ctm_opts" || \
    { echo "$0: prepare_semi_data: ERROR, @register_options.sh"; exit 1; }
    acwt=$(echo "1/$lmwt"|bc -l)
    echo "$0: prepare_semi_data: acwt=$acwt"
    [ -f $sdir/final.mdl ] || \
    { echo "$0: prepare_semi_data: ERROR, final.mdl expected in $sdir"; exit 1; }
    model=$sdir/final.mdl
    name=$(basename $data)
    $cmd LMWT=$lmwt:$lmwt $latdir/score_LMWT/log/get_ctm.$lmwt.log \
    lattice-align-words $lang/phones/word_boundary.int $model "ark:gzip -cd $latdir/lat.*.gz|" ark:- \| \
    lattice-to-ctm-conf --acoustic-scale=$acwt  ark:- - \| \
    utils/int2sym.pl -f 5 $lang/words.txt  \| tee $latdir/score_LMWT/$name.utt.ctm \| \
    utils/convert_ctm.pl $data/segments $data/reco2file_and_channel \
    '>' $latdir/score_LMWT/$name.ctm || exit 1;
  fi
  if [ ! -z $step02 ]; then
    optNames="conf_thresh merge_tolerance sdata ctm dir"
    . source/register_options.sh "$optNames" "$prepare_step02_opts" || \
    { echo "$0: prepare_step02: ERROR, @ register_options.sh"; exit 1; }
    [ -f $sdata/segments ] || \
    { echo "$0: prepare_step02: ERROR, segments file expected"; exit 1; }
    [ -d $dir ] || mkdir -p $dir
    source/code/extract-sub-segment-with-conf --conf-thresh=$conf_thresh --merge-tolerance=$merge_tolerance \
    $sdata/segments  "egrep -v '<' $ctm |"  $dir/segments $dir/text
    cp $sdata/wav.scp $dir/wav.scp
    cat $dir/segments | perl -pe '@A = split(/\s+/); $s = $A[0]; $s =~ m/([^_]+_[^_])_*/; $_="$s $1\n";'  > $dir/utt2spk
    utils/utt2spk_to_spk2utt.pl < $dir/utt2spk > $dir/spk2utt
    utils/fix_data_dir.sh $dir
  fi
  echo "$0: prepare_semi_data ended @ `date`"
fi
