#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

# begin sub
sub RemoveTrailingPeriod {
  my ($text) = @_;
  $$text =~ s:\.\s+:\n:g;
  $$text =~ s:\.\":\n:g;
  $$text =~ s:\.”:\n:g;
}
sub RemoveQuotes {
  my ($text) = @_;
  $$text =~ s:[”“\"]: :g;
}
sub RemoveOthers {
  my ($text) = @_;
  $$text =~ s:[,!;\:]: :g;
}
sub RemoveSpace {
  my ($text) = @_;
  $$text =~ s:^ +::g;
  $$text =~ s: +$::g;
}
sub NormalizeText {
  my ($text) = @_;
  $$text = lc $$text;
  RemoveQuotes($text);
  RemoveOthers($text);
  RemoveTrailingPeriod($text);
  RemoveSpace($text);
}
# end sub

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  s:^[^,]+,"::g;
  NormalizeText(\$_);
  if(/^$/) {
    next;
  }
  print "$_\n";
}
print STDERR "## LOG ($0): stdin ended\n";


