#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);

my  $numArgs = scalar @ARGV;
if ($numArgs != 5) {
  die "\nUsage: cat text | $0 nDevLine lexicon.txt dev-text train-text oov-count.txt\n\n";
}

my ($nDevLine, $dictFile, $devTextFile, $trainTextFile, $oovCountFile) = @ARGV;

# begin sub
sub LoadDict {
  my ($dictFile, $vocab) = @_;
  open(D, "$dictFile") or die "## ERROR ($0): cannot open '$dictFile'\n";
  while(<D>) {
    chomp;
    m/(\S+)\s+(.*)/g or next;
    my $word = lc $1;
    $$vocab{$word} ++;
  }
  close D;
}
sub AddToDict {
  my ($vocab, $oovVocab, $sLine) = @_;
  my @A = split(/\s+/, $sLine);
  for(my $i = 0; $i < scalar @A; $i ++) {
    my $word = $A[$i];
    next if exists $$vocab{$word};
    $$oovVocab{$word} ++;
  }
}
# end sub

my %vocab = ();
LoadDict($dictFile, \%vocab);
open(DEV, "$devTextFile") or die "## ERROR ($0): cannot open '$devTextFile'\n";
open(TRN, "$trainTextFile") or die "## ERROR ($0): cannot open '$trainTextFile'\n";
open (OOV, "$oovCountFile") or die "## ERROR ($0): cannot open '$oovCountFile'\n";
my %oovVocab = ();
print STDERR "## LOG ($0): stdin expected\n";
my $nLine = 0;
while(<STDIN>) {
  chomp;
  AddToDict(\%vocab, \%oovVocab, $_);
  if($nLine < $nDevLine) {
    print DEV "$_\n";
  } else {
    print TRN "$_\n";
  }
  $nLine ++;
}
print STDERR "## LOG ($0): stdin ended\n";
close DEV;
close TRN;
foreach my $word  (keys%oovVocab) {
  print OOV "$word\t\t$oovVocab{$word}\n";
}
close OOV;
