#!/usr/bin/perl -w
use strict;

use utf8;
use open qw(:std :utf8);

while(<STDIN>) {
  chomp;
  next if(/^$/);
  print "$_\n";
}
