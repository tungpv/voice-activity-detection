#!/usr/bin/perl -w
use strict;


my $numArgs = scalar @ARGV;

if($numArgs != 1) {
  print STDERR "\ncat big-word-list.txt | $0 small-word-list.txt > subset-word-list.txt\n\n";
  exit 1;
}
my $wordList = shift @ARGV;
open(Instream,"$wordList") or die;
my %vocab = ();
while(<Instream>) {
  chomp;
  m:(^\S+): or next;
  $vocab{$1} ++;
}
close(Instream);
print STDERR "stdin expected ...\n";
while(<STDIN>) {
  chomp;
  m:(^\S+): or next;
  my $word = $1;
  if(not exists $vocab{$word}) {
    print("$_\n");
  }
}
print STDERR "stdin ended ...\n";
