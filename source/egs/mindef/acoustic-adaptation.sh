#!/bin/bash

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd=run.pl
steps=
nj=20
# end options

. parse_options.sh || exit 1


function Usage {
 cat<<EOF

 [Example]: $0 --steps 1 --cmd '$cmd' \
 /home2/hhx502/sge2017/data/manual-data-170223 \
 /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/lang_from_ali_redo \
 /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/exp/mpe-nnet5a-tl-nov28 \
 /home3/hhx502/w2017/acumen

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

sdata=$1
slang=$2
srcdir=$3
tgtdir=$4


datadir=$tgtdir/data
[ -d $datadir ] || mkdir -p $datadir

overall=$datadir/overall
[ -d $overall ] || mkdir -p $overall
dev=$datadir/dev
train=$datadir/train
[ -d $train ] || mkdir -p $train
if [ ! -z $step01 ]; then
  cp $sdata/{wav.scp,segments,text,utt2spk,spk2utt} $overall/
  utils/data/subset_data_dir.sh --per-spk $overall 8  $dev
  cut -d' ' -f1 $sdata/segments | \
  source/egs/mindef/subset-list.pl  "cut -d' ' -f1 $dev/segments|" > $train/uttlist
  utils/data/subset_data_dir.sh --utt-list $train/uttlist $overall $train
  echo "done & check '$dev' & '$train'"
fi
if [ ! -z $step02 ]; then
  for sdata in $train $dev; do
    data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    [ -d $data ] || mkdir -p $data
    cp $sdata/* $data/
    steps/make_fbank_pitch.sh --cmd "$cmd" --nj $nj \
    --fbank-config conf/fbank16k40.conf \
    --pitch-config conf/pitch16k.conf \
    $data $feat/log $feat/data
    steps/compute_cmvn_stats.sh $data $feat/log $feat/data
    utils/data/fix_data_dir.sh $data
    echo "## LOG (step02, $0): done with fbank-pitch & check '$data' @ `date`"
  done
fi
expdir=$tgtdir/exp
alidir=$expdir/ali-train
if [ ! -z $step03 ]; then
  steps/nnet/align.sh  --cmd "$cmd" --nj $nj \
  --use-gpu yes \
  $train/fbank-pitch $slang $srcdir $expdir/ali-train
  echo "## LOG (step03): done & check '$expdir/ali-train'"
fi

ntrain=$expdir/train
nvalid=$expdir/valid
validating_rate=0.1
if [ ! -z $step04 ]; then
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $ntrain \
  $train/fbank-pitch  $nvalid || exit 1
fi
nnetdir=$expdir/dnn
learn_rate=0.008
[ -d $nnetdir ] || mkdir -p $nnetdir
if [ ! -z $step05 ]; then
  num_tgt=$(hmm-info --print-args=false $srcdir/final.mdl | grep pdfs | awk '{ print $NF }')
  soft_max_proto=$srcdir/soft-max.proto
  front_nnet="nnet-copy --remove-last-layers=2 $srcdir/final.nnet -|"
  hid_dim=$(nnet-info "$front_nnet" | \
  grep output-dim | tail -1 |perl -pe 's/.*output-dim//g; m/(\d+)/g; $_=$1;') || \
  { echo "ERROR, hid_dim error"; exit 1; }
  utils/nnet/make_nnet_proto.py $hid_dim $num_tgt 0  $hid_dim >$soft_max_proto || exit 1
  soft_max_init=$srcdir/soft-max.init 
  log=$srcdir/sotf-max-initialize.log
  nnet-initialize $soft_max_proto $soft_max_init 2>$log || { cat $log; exit 1; } 
  nnet_init=$nnetdir/nnet.init
  nnet-concat "$front_nnet" $soft_max_init $nnet_init 2> $nnetdir/nnet-init.log
  feature_transform=$srcdir/final.feature_transform
  $cmd $expdir/dnn.log steps/nnet/train.sh --learn-rate $learn_rate --feature-transform $feature_transform \
  ${scheduler_opts:+ --scheduler-opts "$scheduler_opts"} \
  ${train_tool_opts:+ --train-tool-opts "$train_tool_opts"} \
  --hid-layers 0 --nnet-init $nnet_init $ntrain $nvalid $slang $alidir \
  $alidir $nnetdir
fi
graph=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938/exp/tri4a/graph-with-dict-from-ali
devdata=../acumen/data/dev/fbank-pitch
if [ ! -z $step06 ]; then
  steps/nnet/decode.sh --cmd "$cmd" --nj $nj \
  --use-gpu yes \
  $graph $devdata $nnetdir/decode-dev-938
fi

if [ ! -z $step07 ]; then
  echo "## LOG (step07): do alignment started @ `date`"
  steps/nnet/align.sh  --cmd "$cmd" --nj $nj \
  --use-gpu yes \
  $train/fbank-pitch $slang $nnetdir $nnetdir/ali-train
  echo "## LOG (step07): done with data alignment ('$nnetdir/ali-train') @ `date`"
fi
if [ ! -z $step08 ]; then
  steps/nnet/make_denlats.sh --cmd "$cmd" --nj $nj \
  --use-gpu yes \
  $train/fbank-pitch $slang $nnetdir $nnetdir/lattice-train
  echo "## LOG (step08): done with lattice generation ('$nnetdir/lattice-train') @ `date`"
fi
mpennetdir=$expdir/mpe-dnn
if [ ! -z $step09 ]; then
  steps/nnet/train_mpe.sh --cmd "$cmd" \
  $train/fbank-pitch $slang $nnetdir $nnetdir/ali-train $nnetdir/lattice-train \
  $mpennetdir 
fi
if [ ! -z $step10 ]; then
  steps/nnet/decode.sh --cmd "$cmd" --nj $nj \
  --use-gpu yes \
  $graph $devdata $mpennetdir/decode-dev-938
fi

if [ ! -z $step11 ]; then
  steps/nnet/decode.sh --cmd "$cmd" --nj $nj \
  --use-gpu yes \
   $graph $devdata $srcdir/decode-dev-938  
fi
## for lexicon and language model adaptation.

localdir=$tgtdir/data/local
mindef_oov_dict=../sge/name-entity/dict/singapore/mindef-oov/asr-lexicon-it01.txt
srcdict=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/dict_from_ali_redo/lexicon.txt
dictdir=$localdir/dict
lang=$tgtdir/data/lang
[ -d $dictdir ] || mkdir -p $dictdir
if [ ! -z $step20 ]; then
  cat $mindef_oov_dict | \
  source/egs/ted-libri-en/merge-dict.pl $srcdict >  $dictdir/lexicon.txt
  cp $(dirname $srcdict)/{extra_questions.txt,nonsilence_phones.txt,silence_phones.txt,optional_silence.txt} $dictdir/
  utils/validate_dict_dir.pl $dictdir
  utils/prepare_lang.sh $dictdir "<unk>" $lang/tmp $lang
fi
lmdir=$localdir/transcript-lm
[ -d $lmdir ] || mkdir -p $lmdir
if [ ! -z $step21 ]; then
  cat /home2/hhx502/sge2017/data/train-mix431/fbank-pitch/text \
  /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/data938/text | \
 source/egs/sge2017/local/build-lm/dump-kaldi-text.pl | gzip -c > $lmdir/train-text.gz

  cat /home2/hhx502/sge2017/data/dev-seame.28/16k/text \
  /home2/hhx502/sge2017/data/fstd-demo-updated/16k/text | \
  source/egs/sge2017/local/build-lm/dump-kaldi-text.pl | gzip -c > $lmdir/dev-text.gz

  cat $dictdir/lexicon.txt | \
  source/egs/sge2017/local/build-lm/print-word-list.pl | gzip -c > $lmdir/vocab.gz

  source/egs/sge2017/local/build-lm/train-srilm-v2.sh --steps 1,2 --lm-order-range "3 3" \
  --cutoff-csl "3,011,012"  $lmdir || exit 1
  
fi
web_lmdir=$localdir/web-lm-kn
[ -d $web_lmdir ] || mkdir -p $web_lmdir
if [ ! -z $step22 ]; then
  cp /home2/hhx502/sge2017/data/update-lm-nov-09-2016/lm-outdomain/train-text.gz $web_lmdir/
  cp $lmdir/{vocab.gz,dev-text.gz} $web_lmdir/
  source/egs/sge2017/local/build-lm/train-srilm-v2.sh --steps 1,2 --lm-order-range "3 3" \
  --cutoff-csl "3,011,012"  $web_lmdir || exit 1
fi
web_lmdir2=$localdir/web-lm-wb
[ -d $web_lmdir2 ] || mkdir -p $web_lmdir2
if [ ! -z $step23 ]; then
  gzip -cd /home2/hhx502/sge2017/data/update-lm-nov-09-2016/lm-outdomain/train-text.gz \
   $lmdir/train-text.gz | gzip -c > $web_lmdir2/train-text.gz
 ngram-count -order 3 -vocab $lmdir/vocab.gz -wbdiscount -unk -sort\
   -text $web_lmdir2/train-text.gz -lm $web_lmdir2/tri-wb-lm.gz 
fi
if [ ! -z $step24 ]; then
  ngram -order 3 -lm  $web_lmdir2/tri-wb-lm.gz -vocab $lmdir/vocab.gz -lambda 0.2 \
  -mix-lm $lmdir/lm.gz -write-lm $web_lmdir2/tri-int0.2.gz
  ngram -order 3 -lm $web_lmdir2/tri-int0.2.gz -ppl $lmdir/dev-text.gz  > $web_lmdir2/ppl-tri-int0.2.txt 
fi
lang_int=$tgtdir/data/lang-int0.2
if [ ! -z $step25 ]; then
  source/egs/fisher-english/arpa2G.sh $web_lmdir2/tri-int0.2.gz $lang $lang_int
fi
graph_int=$nnetdir/graph-int0.2
if [ ! -z $step26 ]; then
  utils/mkgraph.sh  $lang_int  $nnetdir \
  $graph_int
fi
