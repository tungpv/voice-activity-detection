#!/bin/bash

. path.sh || { echo "## ERROR: path.sh expected";  exit 1; }
. cmd.sh 

echo 
echo "## LOG : $0 $@"
echo

# begin options
steps=
mindef_data=
train_compressed=false
dev_compressed=false
prune_thresh=1.0e-8
# end options

. parse_options.sh || { echo "## ERROR: utils/parse_options.sh expected"; exit 1; }

function Usage {
 cat<<EOF
 
 [Usage]: $0  --steps [1-4]  decodeing-source-dir    new-text-data  dev-text-data   decoding-target-dirname

 [Example]: $0 --steps 1-4  --mindef-data update-lm/backup/testsample/SampleTranscriptLayout.csv  \
  systems/SingaporeEnglish_1601 ../simuData_oovData/simu_out.txt \
 ../simuData_oovData/simu_out_dev.txt  systems/SingaporeEnglish          

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ $# -ne 4 ]; then
  Usage && exit 1
fi
srcsystemdir=$1
train_text=$2
dev_text=$3
tgtsystemdir=$4

for x in graph/words.txt lang/L.fst fbank_nnet/{final.mdl,tree} lm/lm.gz; do
  xfile=$srcsystemdir/$x
  [ -f $xfile ] || { echo "## ERROR ($0): $xfile expected"; exit 1; }
done
date_suffix=$(echo  `date +%b-%d-%y` | tr '[A-Z]' '[a-z]')
tgtsystemdir=$(echo $tgtsystemdir | sed 's:/$::g')_${date_suffix}
[ -d $tgtsystemdir ] || mkdir -p $tgtsystemdir

lang=$tgtsystemdir/lang
srcdir=$tgtsystemdir/fbank_nnet
oldlm=$tgtsystemdir/lm

function Cleanup {
  echo "## LOG ($0): doing cleaning up work ..."
}


if [ ! -f $tgtsystemdir/.copy_done ]; then
  echo "## LOG ($0): copy is in progress ..."
  cp -rL $srcsystemdir/*  $tgtsystemdir/
  touch $tgtsystemdir/.copy_done
  echo "## LOG ($0): copy done & check '$tgtsystemdir/.copy_done'"
fi
newlm=$tgtsystemdir/new-lm
if [ ! -z $mindef_data ]; then
  echo "## LOG ($0): prepare train and dev data from the overall data $mindef_data"
  nTotal=$(cat $mindef_data |update-lm/source/egs/mindef/normalize-text.pl|update-lm/source/egs/mindef/remove-empty-line.pl | wc -l)
  [ $nTotal -gt 0 ] || { echo "## ERROR ($0): failed to normalize data $mindef_data"; exit 1; }
  nDev=$(echo $nTotal | perl -ane 'chomp; printf("%d", $_/10)')
  [ $nDev -gt 0 ] || { echo "## ERROR ($0): failed to get dev data ($nDev)"; exit 1; }
  [ -d $newlm ] || mkdir -p $newlm
  cat $mindef_data |update-lm/source/egs/mindef/normalize-text.pl| \
  update-lm/source/egs/mindef/remove-empty-line.pl | \
  utils/shuffle_list.pl | \
  update-lm/source/egs/mindef/split_data_and_print_oov.pl $nDev  $tgtsystemdir/graph/words.txt  "|gzip -c > $newlm/dev-text.gz"  "|gzip -c > $newlm/train-text.gz" \
  "|sort -k2nr |gzip -c > $newlm/oov-count.gz"
  for x in $newlm/dev-text.gz $newlm/train-text.gz $newlm/oov-count.gz; do
    [ -f $x ] || { echo "## ERROR ($0): file $x expected"; exit 1; }
  done
fi

if [ ! -z $step01 ]; then
  [ -d $newlm ] || mkdir -p $newlm
  if [ ! -f $newlm/train-text.gz ]; then
    if ! $train_compressed; then
      gzip -c $train_text > $newlm/train-text.gz || { echo "## ERROR ($0): train text preparation failed"; exit 1;  }
    else
      cp $train_text  $newlm/train-text.gz || { echo "## ERROR ($0): train text copy failed"; exit 1; }
    fi
  fi
  if [ ! -f $newlm/dev-text.gz ]; then
    if ! $dev_compressed; then
      gzip -c $dev_text > $newlm/dev-text.gz || { echo "## ERROR ($0): dev text preparation failed"; exit 1; }
    else
      cp $dev_text  $newlm/dev-text.gz || { echo "## ERROR ($0): dev text copy failed"; exit 1; }
    fi
  fi
  if [ ! -f $newlm/vocab.gz ]; then
     echo "## LOG ($0): make '$newlm/vocab.gz' ..."
     egrep -v '#|<eps>' $tgtsystemdir/graph/words.txt | cut -d' ' -f1 | gzip -c > $newlm/vocab.gz
  fi
  if [ ! -f $newlm/lm.gz ]; then
    update-lm/train-srilm-v2.sh --steps 1,2 --lm-order-range "3 3" \
    --cutoff-csl "3,011,012" $newlm
  fi
  [ -f $newlm/lm.gz ] || { echo "## ERROR (step01, $0): building '$newlm/vocab.gz' failed"; exit 1;  }
fi

intlm=$tgtsystemdir/int-lm
if [ ! -z $step02 ]; then
  for xfile in $oldlm/lm.gz  $newlm/lm.gz $newlm/dev-text.gz; do
    [ -f $xfile ] || { echo "## ERROR (step02, $0): file $xfile expected"; exit 1; }
  done
  [ -d $intlm ] || mkdir -p $intlm
  update-lm/lm-interpolate.sh 3 $oldlm/lm.gz  $newlm/lm.gz $newlm/dev-text.gz $intlm
  [ -f $intlm/lm.gz ] || { echo "## ERROR (step02, $0): failed to build '$intlm/lm.gz'"; exit 1; }
fi

if [ ! -z $step03 ]; then
  rm $lang/G.fst 2>/dev/null
  update-lm/arpa2G.sh $intlm/lm.gz $lang $lang 
fi
graph=$tgtsystemdir/graph
if [ ! -z $step04 ]; then
  [ -e $lang/G.fst ] || { echo "## ERROR (step04, $0): '$lang/G.fst' is not ready"; exit 1; }
  [ -e $graph/HCLG.fst ] && rm $graph/HCLG.fst 2>/dev/null
  utils/mkgraph.sh $lang $srcdir $graph
  [ -e $graph/HCLG.fst ] || { echo "# ERROR (step04, $0): failed to build '$graph/HCLG.fst'"; exit 1;  }
  echo "## LOG (step04, $0): done & check '$graph/HCLG.fst' @ `date`"
fi
