#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
my $remove_identifier = '';
my $add_identifier = '';
my $lower_word;
my $upper_word;
my $lower_phone;
my $upper_phone;

GetOptions('remove-identifier|remove_identifier=s' => \$remove_identifier,
	   'add-identifier|add_identifier=s' => \$add_identifier,
           'lower-word|lower_word' => \$lower_word,
           'upper_word|upper-word' => \$upper_word,
	   'lower_phone|lower-phone' => \$lower_phone,
           'upper-phone|upper_phone' => \$upper_phone) or die;

print STDERR "\n ## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m:^(\S+)\s+(.*)$:g  or next;
  my $word = $1;  my $phones = $2; 
  $phones = lc $phones if($lower_phone);
  $phones = uc $phones if ($upper_phone);
  my @phoneArray = split(" ", $phones);
  if($lower_word) {
    $word = lc $word;
  }
  if ($upper_word) {
    $word = uc $word;
  }

  if ($add_identifier ne '') {
    $phones = '';
    for(my $i = 0; $i < scalar @phoneArray; $i ++) {
      my $phone = $phoneArray[$i];
      $phone =~ s:\s::g;
      next if ($phone =~ /^$/);
      $phone = $phone . $add_identifier;
      $phones .= $phone . " ";
    }
    $phones =~ s: $::g;
  }
  if ($remove_identifier ne '') {
    $phones = '';
    for(my $i = 0; $i < scalar @phoneArray; $i ++) {
      my $phone = $phoneArray[$i];
      $phone =~ s:\s::g;
      next if ($phone =~ /^$/);
      $phone =~ s:$remove_identifier$::;
      $phones .= $phone . " ";
    }
    $phones =~ s: $::g;
  }
  print "$word\t$phones\n";
}
print STDERR "\n ## LOG ($0): stdin ended\n";
