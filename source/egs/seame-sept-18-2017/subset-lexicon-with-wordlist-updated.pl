#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;

my $exclude_wordlist;
my $oov_word_list = '';
GetOptions('exclude_wordlist|exclude-wordlist' => \$exclude_wordlist,
           'oov_word_list|oov-word-list=s' => \$oov_word_list) or die;
my $numArgs = scalar @ARGV;
if($numArgs != 1) {
  die "\n[Example]: cat dict.txt | $0 [--exclude-wordlist|oov-word-list=<oov-word-list>] wordlist.txt > subset-dict.txt\n\n";
}
my ($wordList) = @ARGV;
our %vocab = ();
# begin sub
sub LoadWordList {
  my($wordList) = @_;
  open(F, "$wordList") or die;
  while(<F>) {
    chomp;
    m/^(\S+)/g or next;
    $vocab{$1} ++;
  }
  close F;
}
# end sub
LoadWordList($wordList);

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  m/(\S+)\s+(.*)/ or next; my $word = $1;
  if($exclude_wordlist) {
    if(not exists $vocab{$word}) {
       print;
    }
  } else {
    if(exists $vocab{$word}) {
      $vocab{$word} ++;
      print;
    }
  }
}
print STDERR "## LOG ($0): stdin ended\n";

if ($oov_word_list ne '') {
  open(F, "|sort -u > $oov_word_list") or die;
  foreach my $word (keys %vocab) {
    if($vocab{$word} <=1) {
      print F "$word\n";
    }
  }
  close F;
}
