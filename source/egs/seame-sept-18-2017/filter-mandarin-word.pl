#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  die "\nExample: cat lexicon.txt | $0 <mandarin-lexicon.txt>\n\n";
}
my ($mlexOutputFile) = @ARGV;
open (F, ">$mlexOutputFile") or die;
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  if(/\p{Han}+/) {
    print F "$_";
  } else {
    print;
  }
}
print STDERR "## LOG($0): stdin ended\n";
