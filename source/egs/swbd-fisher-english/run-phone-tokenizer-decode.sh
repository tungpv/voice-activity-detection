#!/bin/bash

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh

# begin options
steps=
cmd="slurm.pl --nodelist=node03"
nj=20
unlabelled_data=/local/hhx502/ldc-cts2016/phone-swbd-fisher-english/data/sre16-unlabel
# end options

function Usage {
 cat<<END

 Usage: $0 [options] <source_lang> <dev-data> <source_dir> <tgtdir> 
 [options]:
 
 $0 --steps 1 --unlabelled-data $unlabelled_data /local/hhx502/ldc-cts2016/phone-swbd-fisher-english/data/lang \
 /local/hhx502/ldc-cts2016/phone-swbd-fisher-english/data/dev/fbank-pitch \
 /local/hhx502/ldc-cts2016/phone-swbd-fisher-english/exp/nnet5a-tl /local/hhx502/ldc-cts2016/phone-swbd-fisher-english/data/phone-loop

END
}

. parse_options.sh || exit 1

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

source_lang=$1
dev_data=$2
source_dir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $tgtdir/lang ] || mkdir -p $tgtdir/lang
graphdir=$source_dir/graph-loop
if [ ! -z $step01 ]; then
  echo "## LOG, step01, $0: make phone-loop test lang"
  cp -r $source_lang/* $tgtdir/lang
  rm $tgtdir/lang/G.fst 2>/dev/null
  source/egs/quesst/make-phone-loop.sh $tgtdir/lang/words.txt $tgtdir/loop || exit 1
  source/egs/fisher-english/arpa2G.sh $tgtdir/loop/lm.gz  $tgtdir/lang $tgtdir/lang || exit 1
  utils/mkgraph.sh $tgtdir/lang  $source_dir  $graphdir
  echo "## LOG, step01, $0: done ('$tgtdir/lang')"
fi
decode_dir=$source_dir/decode-dev
if [ ! -z $step02 ]; then
  echo "## LOG, step02, $0: decode on dev data @ `date`"
  steps/nnet/decode.sh --cmd "run.pl" --nj $nj --scoring-opts "--min-lmwt 8 --max-lmwt 15"  --acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false \
  $graphdir $dev_data $decode_dir || exit 1
  echo "## LOG, step02, $0: done @ `date`"
fi
sdata=$unlabelled_data
if [ ! -z $step03 ]; then
  echo "## LOG, step03, $0: prepare fbank+pitch feature @ `date`"
  data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 10 \
    --make-fbank true --fbank-cmd \
   "steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf" \
    $sdata $data $feat || exit 1
  echo "## LOG, step03, $0: done ('$unlabelled_data')"
fi
test_data=$sdata/fbank-pitch
decode_dir=$source_dir/decode-unlabelled-lmwt10-correct
if [ ! -z $step04 ]; then
  echo "## LOG, step04, $0: decode unlabelled_data ($test_data) @ `date`"
  steps/nnet/decode.sh --cmd "run.pl" --nj 10  --acwt 0.5 --beam 13 --lattice-beam 10 --max-mem 500000000 --skip-scoring true \
  $graphdir $test_data $decode_dir || exit 1
  echo "## LOG, step04, $0: done ('$test_data') @ `date`"
fi
if [ ! -z $step05 ]; then
  echo "## LOG, step05, $0: make unsupervised data "
  source/egs/swahili/lat_to_utt_ctm.sh --steps 1,2 --cmd run.pl \
  --silence-word-int 2 --lmwt 2 \
  $test_data  $graphdir $decode_dir || exit 1
  
  echo "## LOG, step05, $0: done"
fi
min_lmwt=2
max_lmwt=2
if [ ! -z $step06 ]; then
  echo "## LOG, step06, $0: make unsupervised data using one-best map decoding method"
  run.pl LMWT=$min_lmwt:$max_lmwt $decode_dir/scoring/log/best_path.LMWT.log \
  lattice-best-path --lm-scale=LMWT --word-symbol-table=$tgtdir/lang/words.txt \
    "ark:gunzip -c $decode_dir/lat.*.gz|" ark,t:$decode_dir/scoring/LMWT.tra || exit 1;
   echo "## LOG, step06, $0: done"
fi

if [ ! -z $step07 ]; then
 for lmwt in $(seq $min_lmwt $max_lmwt); do
    utils/int2sym.pl -f 2- $tgtdir/lang/words.txt <$decode_dir/scoring/$lmwt.tra  \
    > $decode_dir/scoring/$lmwt.txt
  done
fi
asr_labelled_data=$decode_dir/scoring/2.txt
tgtdata=$decode_dir/asr_labelled/fbank-pitch
if [ ! -z $step08 ]; then
   [ -d $tgtdata ] || mkdir -p $tgtdata
   cp $test_data/* $tgtdata/
   cp $asr_labelled_data $tgtdata/text
   utils/fix_data_dir.sh $tgtdata
fi
