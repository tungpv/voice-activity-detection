#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
steps=
cmd="slurm.pl --nodelist=node03"
nj=40
gmm_steps=1,2,3,4,5,6,7
full_train_data="/local/hhx502/ldc-cts2016/phone-swbd-fisher-english/data/train/mfcc"
# end options

function Usage {
cat <<END
 Usage: $0 [options] <tgtdir>
 [options]:
 --gmm-steps                          # value, "$gmm_steps"
 --full-train-data                    # value, "$full_train_data"                   
 [steps]:
 [examples]:

 $0 --steps 3 --gmm-steps "$gmm_steps" --full-train-data $full_train_data  \
   /local/hhx502/ldc-cts2016/phone-swbd-fisher-english

END
}
. parse_options.sh || exit 1
if [ $# -ne 1 ]; then
  Usage && exit 1
fi
tgtdir=$1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
train_data=$tgtdir/data/train
sub_rate=0.1
sub_train_data=$tgtdir/data/train-sub${sub_rate}/mfcc
if [ ! -z $step01 ]; then
  echo "## $0, step01, make subset data"
  source/egs/swahili/subset_data.sh --subset-time-ratio $sub_rate \
   $train_data/mfcc $sub_train_data
  echo "## $0, step02, done ('$sub_train_data')"
fi
dev_data=$tgtdir/data/dev/mfcc
expdir=$tgtdir/exp
if [ ! -z $step02 ]; then
  echo "## $0, step02, train gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps "$gmm_steps" --cmd "$cmd" \
  --train-id b \
  --cmvn-opts "--norm-means=true" --state-num 5000 --pdf-num 75000 \
  --devdata $dev_data \
  $sub_train_data $tgtdir/data/lang $expdir || exit 1

  echo "## $0, step02, done ('$expdir/tri4a') @ `date`"
fi
alidir=$expdir/tri4b/ali-full-train
if [ ! -z $step03 ]; then
  echo "## LOG: $0, step03, align full train data @ `date`"
  steps/align_fmllr.sh --cmd run.pl --nj 20 \
  $full_train_data $tgtdir/data/lang $expdir/tri4b $alidir || exit 1
  echo "## LOG: $0, step03, done '$alidir' @ `date`"
fi
if [ ! -z $step05 ]; then
  echo "## LOG: $0, step05, train gmm with full training data @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7 --cmd "$cmd" --nj 15 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 7000 --pdf-num 210000 \
  --alidir $alidir \
  --devdata $dev_data \
  $train_data/mfcc $tgtdir/data/lang $expdir || exit 1
  echo "## LOG: $0, step05, done @ `date`"
fi
