#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 $0 [steps] <tgtdir>
 [steps]:
 1: make dict 
 2: make lang
END
}
if [ $# -ne 1 ]; then
  Usage && exit 1
fi
tgtdir=$1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
dict=$tgtdir/data/local/dict
swbd_dict=swbd/data/local/dict/lexicon.txt
fisher_dict=fisher-english/data/local/dict/lexicon.txt
if [ ! -z $step01 ]; then
  echo "## LOG: step01, merge dict from  swbd and fisher corpus @ `date`"
  [ -d $dict ] || mkdir -p $dict
  cat $swbd_dict | \
  perl -pe 's/\[laughter\]\s+lau/<noise>\t<sss>/; 
            s/!sil\s+sil/<silence>\tsil/;
            s/\[noise\]\s+nsn/<noise>\t<sss>/;
            s/\[vocalized-noise\]\s+spn/<noise>\t<sss>/;
            s/<unk>\s+spn/<unk>\t<oov>/;' | \
  perl -pe 'chomp; @A = split(/\s+/); $w = shift @A; 
            $_ = "$w\t". join(" ", @A). "\n";
           '  | sort -u  > $dict/swbd-lexicon.txt
  cat $fisher_dict | \
  perl -pe 'chomp; @A = split(/\s+/); $w = shift @A; 
            $_ = "$w\t". join(" ", @A). "\n";
           '  | sort -u  > $dict/fisher-lexicon.txt
  cat $dict/{swbd,fisher}-lexicon.txt | \
  perl -e 'while(<STDIN>){chomp; if(not exists $vocab{$_}){print $_,"\n"; $vocab{$_}++;}}' > $dict/lexicon.txt
  cat $dict/lexicon.txt | \
  perl -e 'while(<STDIN>){chomp; @A = split(/\s+/); shift @A; for($i = 0; $i < @A; $i++){print $A[$i], "\n"; }  }'\
  | sort -u > $dict/overall-phone-list.txt
  cat $dict/overall-phone-list.txt | egrep -v 'sil|<sss>|<oov>' >$dict/nonsilence_phones.txt
  echo -e "sil\n<sss>\n<oov>"   > $dict/silence_phones.txt
  echo "sil" > $dict/optional_silence.txt
  echo "sil <sss> <oov>" > $dict/extra_questions.txt
  echo "## LOG: step01, done @ `date`"
fi
lang=$tgtdir/data/lang
if [ ! -z $step02 ]; then
  echo "## LOG: step02, make lang @ `date`"
  utils/prepare_lang.sh $dict "<unk>" $tgtdir/local/lang $lang
  echo "## LOG: step02, done @ `date`"
fi
train_data=$tgtdir/data/train
if [ ! -z $step03 ]; then
  echo "## LOG: step03, prepare training data @ `date`"
  [ -d $train_data/tmp-swbd ] || mkdir -p $train_data/tmp-swbd
  cp swbd/data/train/*  $train_data/tmp-swbd/
  cat swbd/data/train/text | \
  perl -ane 's/\[noise\]/<noise>/g;
    s/\[laughter\]/<noise>/g;
    s/\[vocalized\-noise\]/<noise>/g; print; ' > $train_data/tmp-swbd/text
  cat swbd/data/train/utt2spk | \
  perl -ane 'm/(\S+)\s+(\S+)/; $utt = $1; $utt =~ m/([^_]+)_(\S+)/; print "$utt $1\n";' > $train_data/tmp-swbd/utt2spk
  utils/utt2spk_to_spk2utt.pl < $train_data/tmp-swbd/utt2spk > $train_data/tmp-swbd/spk2utt
  utils/combine_data.sh $train_data/merge $train_data/tmp-swbd fisher-english/data/train
  utils/fix_data_dir.sh $train_data/merge
  mv $train_data/merge/* $train_data/
  echo "## LOG: step03, done @ `date`"
fi 
if [ ! -z $step04 ]; then
  echo "## LOG: step04, mfcc @ `date`"
  source/egs/swahili/make_feats.sh --nj 40 --cmd slurm.pl \
   --mfcc-for-ivector true --mfcc-cmd "steps/make_mfcc.sh --mfcc-config conf/mfcc.conf" \
   $train_data $train_data/mfcc $train_data/feat/mfcc || exit 1
  echo "## LOG: step04, done @ `date`"
fi
lmdir=$tgtdir/data/local/lm
if [ ! -z $step05 ]; then
  echo "## LOG: step05, lm @ `date`"
  [ -d $lmdir ] || mkdir -p $lmdir
  lexicon=$tgtdir/data/local/dict/lexicon.txt
  devdata=fisher-english/data/dev/text
  heldout_sent=$(wc -l < $devdata)
  [ $heldout_sent -gt 0 ] || { echo "ERROR: step16, heldout_sent error"; exit 1; }
  echo $heldout_sent > $lmdir/heldout_sent
  cat $train_data/text > $lmdir/kaldi_text
  cleantext=$lmdir/text.no-oov
  cat $lmdir/kaldi_text | awk -v lex=$lexicon 'BEGIN{while((getline<lex) >0){ seen[$1]=1; } } 
  {for(n=1; n<=NF;n++) {  if (seen[$n]) { printf("%s ", $n); } else {printf("<unk> ");} } printf("\n");}' \
  > $cleantext || exit 1;
  
  cat $cleantext | awk '{for(n=2;n<=NF;n++) print $n; }' | sort | uniq -c | \
   sort -nr > $lmdir/word.counts || exit 1;
  
  # Get counts from acoustic training transcripts, and add  one-count
  # for each word in the lexicon (but not silence, we don't want it
  # in the LM-- we'll add it optionally later).
  cat $cleantext | awk '{for(n=2;n<=NF;n++) print $n; }' | \
  cat - <(grep -w -v '<silence>' $lexicon | awk '{print $1}') | \
  sort | uniq -c | sort -nr > $lmdir/unigram.counts || exit 1;
  
  # note: we probably won't really make use of <unk> as there aren't any OOVs
  cat $lmdir/unigram.counts  | awk '{print $2}' | get_word_map.pl "<s>" "</s>" "<unk>" > $lmdir/word_map \
  || exit 1;
 
  # note: ignore 1st field of train.txt, it's the utterance-id.
  cat $cleantext | awk -v wmap=$lmdir/word_map 'BEGIN{while((getline<wmap)>0)map[$1]=$2;}
  { for(n=2;n<=NF;n++) { printf map[$n]; if(n<NF){ printf " "; } else { print ""; }}}' | gzip -c >$lmdir/train.gz \
   || exit 1;
  rm $lmdir/kaldi_text $cleantext 2>/dev/null 

  echo "## LOG: step05, done @ `date`"
fi
if [ ! -z $step06 ]; then
  echo "## LOG: train lm @ `date`"
  source/egs/fisher-english/train_lm.sh --arpa --lmtype 3gram-mincount  $lmdir
  echo "## LOG: done @ `date`"
fi
lang_test=$tgtdir/data/lang_test
lang=$tgtdir/data/lang
if [ ! -z $step07 ]; then
  echo "## LOG: prepare lang-test @ `date`"
  arpa_lm=$lmdir/3gram-mincount/lm_unpruned.gz
  [ ! -f $arpa_lm ] && echo No such file $arpa_lm && exit 1;
  mkdir -p $lang_test
  cp -r $lang/* $lang_test

  gunzip -c "$arpa_lm" | \
  arpa2fst --disambig-symbol=#0 \
           --read-symbol-table=$lang_test/words.txt - $lang_test/G.fst

  echo  "Checking how stochastic G is (the first of these numbers should be small):"
  fstisstochastic $lang_test/G.fst

  ## Check lexicon.
  ## just have a look and make sure it seems sane.
  echo "First few lines of lexicon FST:"
  fstprint   --isymbols=$lang/phones.txt --osymbols=$lang/words.txt $lang/L.fst  | head

  echo Performing further checks

  # Checking that G.fst is determinizable.
  fstdeterminize $lang_test/G.fst /dev/null || echo Error determinizing G.

  # Checking that L_disambig.fst is determinizable.
  fstdeterminize $lang_test/L_disambig.fst /dev/null || echo Error determinizing L.

# Checking that disambiguated lexicon times G is determinizable
# Note: we do this with fstdeterminizestar not fstdeterminize, as
# fstdeterminize was taking forever (presumbaly relates to a bug
# in this version of OpenFst that makes determinization slow for
# some case).
fsttablecompose $lang_test/L_disambig.fst $lang_test/G.fst | \
   fstdeterminizestar >/dev/null || echo Error

# Checking that LG is stochastic:
fsttablecompose $lang/L_disambig.fst $lang_test/G.fst | \
   fstisstochastic || echo "[log:] LG is not stochastic"

  echo "## LOG: done @ `date`"
fi
if [ ! -z $step08 ]; then
  echo "## LOG: subset data @ `date`"
  source/egs/swahili/subset_data.sh  --subset-time-ratio 0.1 \
  swbd-fisher-english/data/train/mfcc swbd-fisher-english/train-sub0.1/mfcc
  echo "## LOG: done @ `date`"
fi

expdir=/local/hhx502/ldc-cts2016/swbd-fisher-english
expdata=$expdir/data
expexp=$expdir/exp
if [ ! -z $step09 ]; then
  echo "## LOG: train gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd run.pl --nj 20 \
  --train-id b \
  --cmvn-opts "--norm-means=true" --state-num 5000 --pdf-num 75000 \
  --devdata fisher-english/data/dev/mfcc \
  $tgtdir/data/train-sub0.1/mfcc $tgtdir/data/lang_test $expexp || exit 1
  echo "## LOG: done @ `date`"
fi
alidir=$expexp/tri4b/ali-full-train
if [ ! -z $step10 ]; then
  echo "## LOG: align the entire data @ `date`"
  steps/align_fmllr.sh --cmd run.pl --nj 20 $tgtdir/data/train/mfcc $lang $expexp/tri4b $alidir || exit 1
  echo "## LOG: done @ `date`"
fi
if [ ! -z $step11 ]; then
  echo "## LOG: step11, train gmm-hmm with full-train data, `hostname`@`date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7 --cmd run.pl --nj 20 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 10000 --pdf-num 300000 \
  --alidir $alidir \
  --devdata fisher-english/data/dev/mfcc \
  $tgtdir/data/train/mfcc $lang_test $expexp || exit 1
  echo "## LOG: step11, done, `hostname`@`date`"
fi
