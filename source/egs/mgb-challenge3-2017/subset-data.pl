#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
use List::Util qw( shuffle );
my $get_head;
my $percentage = 10;
srand(777);
GetOptions('get_head|get-head' => \$get_head,
           'percentage=i' => \$percentage);
die "## ERROR ($0): illegal percentage number '$percentage' not in 0..100\n" if $percentage <= 0 or $percentage > 100;
my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\nExample: $0 [--get-head|percentage=10] <text>\n\n";
}
my ($iStreamName) = @ARGV;
# begin sub

# end sub
my $nTotalLine = 0;
open(F, "$iStreamName") or die;
my @Array = ();
while(<F>) {
  chomp;
  s:^\s+::g;
  next if(m:^$:g);
  push (@Array, $_);
  $nTotalLine ++;
}
close F;
die "## ERROR ($0): empty file '$iStreamName'" if $nTotalLine == 0;
my $nSubsetLine = int($nTotalLine * $percentage / 100);
my $outputFileName = sprintf("| head -%d",$nSubsetLine);
if (not $get_head) {
  $outputFileName = sprintf("|tail -n +%d", $nSubsetLine+1);
}
my @RandomArray = shuffle 0..$nTotalLine-1;
open(F, "$outputFileName") or die;
for(my $i = 0; $i < $nTotalLine; $i ++) {
  my $nIndex = $RandomArray[$i];
  print F "$Array[$nIndex]\n";
}
close F;

