#!/bin/bash 

. path.sh
. cmd.sh

# begin sub
cmd='slurm.pl --quiet'
nj=120
steps=
numleaves=7000
hidden_dim=1024
train_stage=-10
get_egs_stage=-10
frames_per_eg=150
minibatch_size=128
num_epochs=4
proportional_shrink=0
chainname=chain1024
num_jobs_initial=3
num_jobs_final=16
remove_egs=false
max_param_change=2.0
initial_effective_lrate=0.001
final_effective_lrate=0.0001
xent_regularize=0.1
remove_egs=false
get_egs_stage=-10
common_egs_dir=
# end sub

. parse_options.sh || exit 1

function Example {
 cat<<EOF
 $0 --steps 1 --cmd "$cmd" --nj $nj \
 ../mgb-challenge3-2017/data/train_pmer30/mfcc_pitch ../mgb-challenge3-2017/data/lang-silprob \
 ../mgb-challenge3-2017/data/dev-ykha ../mgb-challenge3-2017/exp-da
  

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 4 ]; then
  Example && exit 1
fi

srctraindata=$1
lang=$2
devdatadir=$3
expdir=$4
[ -d $expdir ] || mkdir -p train
# begin data preparation
train_pa=../mgb-challenge3-2017/data/train_pmer30-pa
if [ ! -z $step01 ]; then
  train=$srctraindata
  part01=$train_pa/p1.0
  utils/data/copy_data_dir.sh $train $part01
  part02=$train_pa/p0.9
  utils/data/perturb_data_dir_speed.sh 0.9 $train $part02
  part03=$train_pa/p1.1
  utils/data/perturb_data_dir_speed.sh 1.1 $train $part03
  utils/combine_data.sh $train_pa/combine $part01 $part02 $part03
  rm $train_pa/combine/{feats.scp,cmvn.scp}
  echo "## LOG (step01): data augmentation done with '$train_pa/combine' @ `date`"
fi
if [ ! -z $step02 ]; then
    for sdata in $train_pa/combine; do
      data=$sdata/mfcc-pitch; feat=$sdata/feat/data/mfcc-pitch; log=$sdata/feat/log/mfcc-pitch
      utils/data/copy_data_dir.sh $sdata $data
      steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
      steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
      utils/fix_data_dir.sh $data
      echo "## LOG (step02): done with feature extraction with '$data' @ `date`"
      data=$sdata/mfcc-hires; feat=$sdata/feat/data/mfcc-hires; log=$sdata/feat/log/mfcc-hires
      utils/data/copy_data_dir.sh $sdata $data
      steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
      --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
      steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
      utils/fix_data_dir.sh $data
      echo "## LOG (step02): done with feature extraction with '$data' @ `date`"
    done
fi
alidir=../mgb-challenge3-2017/exp/tri4a_train_pmer30/ali-train_pa
if [ ! -z $step03 ]; then
  echo "## LOG (step03): started @ `date`"
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj \
  $train_pa/combine/mfcc-pitch  $lang $(dirname $alidir) $alidir || exit 1
  echo "## LOG (step03): done @ `date`"
fi
srcdir=$expdir/tri4a
if [ ! -z $step04 ]; then
  echo "## LOG (step04): started @ `date`"
  steps/train_sat.sh --cmd "$cmd" \
  8000 100000 $train_pa/combine/mfcc-pitch  $lang  $alidir  $expdir/tri4a || exit 1
  echo "## LOG (step04): done @ `date`"
fi
alidir=$srcdir/ali-train_pa
if [ ! -z $step05 ]; then
  echo "## LOG (step05): started @ `date`"
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj \
  $train_pa/combine/mfcc-pitch $lang $srcdir $alidir  || exit 1
  echo "## LOG (step05): ended @ `date`"
fi
min_utt_len=5.0
if [ ! -z $step06 ]; then
  for sdata in $train_pa/combine/{mfcc-pitch,mfcc-hires}; do
    data=$train_pa/subset-to-train-ivector/$(basename $sdata)
    [ -d $data ] || mkdir -p $data
    cat $sdata/segments | \
    awk -v thresh=$min_utt_len '{ if($4-$3 >= thresh){print $1 ;} }' > $data/utt-list
    utils/subset_data_dir.sh --utt-list $data/utt-list $sdata $data
    echo "## LOG (step06): subset data '$data' generated"
  done
fi
data=$train_pa/subset-to-train-ivector/mfcc-pitch
alidir=$srcdir/ali-subset-to-train-ivector-extractor
if [ ! -z $step07 ]; then
  steps/align_fmllr.sh --nj $nj --cmd "$cmd" \
  $data  $lang $srcdir   $alidir || exit 1
  echo "## LOG (step07): done with doing alignment @ `date`"
fi
data=$train_pa/subset-to-train-ivector/mfcc-hires
nnetdir=$expdir/tdnn
transform_dir=$nnetdir/lda-mllt-transform-for-ivector-extractor
if [ ! -z $step08 ]; then
  steps/train_lda_mllt.sh --cmd "$cmd"  --num-iters 13 \
  --realign-iters "" \
  --splice-opts "--left-context=3 --right-context=3" \
  8000 80000 $data  $lang \
  $alidir $transform_dir || exit 1
  echo "## LOG (step08): done with '$transform_dir'"
fi
if [ ! -z $step09 ]; then
  steps/online/nnet2/train_diag_ubm.sh --cmd "$cmd" --nj $nj \
  --num-threads 2 \
  --num-frames 720000 $data  512  $transform_dir $nnetdir/diag_ubm || exit 1
  echo "## LOG (step09): done with '$nnetdir/diag_ubm'"
fi
ivector_extractor=$nnetdir/ivector-extractor
if [ ! -z $step10 ]; then
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$cmd" --nj $nj \
  --num-threads 1 --num-processes 1 \
  $data  $nnetdir/diag_ubm $ivector_extractor || exit 1;
  echo "## LOG (step10): done with '$ivector_extractor'"
fi
train_ivectors=$nnetdir/ivectors-train_pa
if [ ! -z $step11 ]; then
  data=$train_pa/combine/mfcc-hires
  data2=$train_pa/combine/mfcc-hires-max2
  steps/online/nnet2/copy_data_dir.sh --utts-per-spk-max 2 $data  $data2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data2 $ivector_extractor $train_ivectors || exit 1
  echo "## LOG (step11): done with ivectors generation with data '$train_ivectors'"
fi
if [ ! -z $step12 ]; then
  for data in $devdatadir/mfcc-hires; do
    data_name=$(basename $(dirname $data))
    data_ivectors=$nnetdir/ivectors-${data_name}
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data $ivector_extractor $data_ivectors || exit 1
  done
fi
latdir=$srcdir/ali_lattice-train
alidir=$srcdir/ali-train_pa
if [ ! -z $step13 ]; then
  nj=$(cat $alidir/num_jobs) || exit 1;
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $train_pa/combine/mfcc-pitch \
  $lang $srcdir  $latdir || exit 1
  rm $latdir/fsts.*.gz # save space
  echo "## LOG (step13): done & check '$latdir' @ `date`"
fi

chain_lang=$nnetdir/lang-chain
if [ ! -z $step14 ]; then
  rm -rf $chain_lang
  cp -r $lang $chain_lang
  silphonelist=$(cat $chain_lang/phones/silence.csl) || exit 1;
  nonsilphonelist=$(cat $chain_lang/phones/nonsilence.csl) || exit 1;
  # Use our special topology... note that later on may have to tune this
  # topology.
  steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >$chain_lang/topo
  echo "## LOG (step14): done with lang-chain ('$chain_lang') @ `date`"
fi
leftmost_questions_truncate=-1
treedir=$nnetdir/chain-tree
numleaves=8000
if [ ! -z $step15 ]; then
  # Build a tree using our new topology. This is the critically different
  # step compared with other recipes.
  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
  --leftmost-questions-truncate $leftmost_questions_truncate \
  --context-opts "--context-width=2 --central-position=1" \
  --cmd "$cmd" $numleaves $train_pa/combine/mfcc-pitch $chain_lang $alidir $treedir
  echo "## LOG (step15): done with tree building '$treedir' @ `date`"
fi

chaindir=$nnetdir/$chainname

if [ ! -z $step16 ]; then
  num_targets=$(tree-info $treedir/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)

  mkdir -p $chaindir/configs
  cat <<EOF > $chaindir/configs/network.xconfig
  input dim=100 name=ivector
  input dim=40 name=input
  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor
  fixed-affine-layer name=lda input=Append(-1,0,1,ReplaceIndex(ivector, t, 0)) affine-transform-file=$chaindir/configs/lda.mat
  # the first splicing is moved before the lda layer, so no splicing here
  relu-renorm-layer name=tdnn1 dim=$hidden_dim
  relu-renorm-layer name=tdnn2 input=Append(-1,0,1) dim=$hidden_dim
  relu-renorm-layer name=tdnn3 input=Append(-1,0,1) dim=$hidden_dim
  relu-renorm-layer name=tdnn4 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn5 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn6 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn7 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn8 input=Append(-3,0,3) dim=$hidden_dim
  ## adding the layers for chain branch
  relu-renorm-layer name=prefinal-chain input=tdnn8 dim=$hidden_dim target-rms=0.5
  output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5
  # adding the layers for xent branch
  # This block prints the configs for a separate output that will be
  # trained with a cross-entropy objective in the 'chain' models... this
  # has the effect of regularizing the hidden parts of the model.  we use
  # 0.5 / args.xent_regularize as the learning rate factor- the factor of
  # 0.5 / args.xent_regularize is suitable as it means the xent
  # final-layer learns at a rate independent of the regularization
  # constant; and the 0.5 was tuned so as to make the relative progress
  # similar in the xent and regular final layers.
  relu-renorm-layer name=prefinal-xent input=tdnn8 dim=$hidden_dim target-rms=0.5
  relu-renorm-layer name=prefinal-lowrank-xent input=prefinal-xent dim=64 target-rms=0.5
  output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF
  steps/nnet3/xconfig_to_configs.py --xconfig-file $chaindir/configs/network.xconfig --config-dir $chaindir/configs/  
  echo "## LOG (step16): done with xconfig-file generation ('$chaindir/configs') @ `date`"
fi

traindata=$train_pa/combine/mfcc-hires
if [ ! -z $step17 ]; then
  echo "## LOG (step17): training started @ `date`"
  steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$cmd" \
    --feat.online-ivector-dir $train_ivectors \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize $xent_regularize \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --egs.dir "$common_egs_dir" \
    --egs.stage $get_egs_stage \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width $frames_per_eg \
    --trainer.num-chunk-per-minibatch $minibatch_size \
    --trainer.frames-per-iter 1500000 \
    --trainer.num-epochs $num_epochs \
    --trainer.optimization.num-jobs-initial $num_jobs_initial \
    --trainer.optimization.num-jobs-final $num_jobs_final \
    --trainer.optimization.initial-effective-lrate $initial_effective_lrate \
    --trainer.optimization.final-effective-lrate $final_effective_lrate \
    --trainer.optimization.proportional-shrink $proportional_shrink \
    --trainer.max-param-change $max_param_change \
    --cleanup.remove-egs $remove_egs \
    --feat-dir $traindata \
    --tree-dir $treedir \
    --lat-dir $latdir \
    --dir $chaindir 
  echo "## LOG (step17): tdnn done (chaindir='$chaindir') @ `date`"
fi
graph=$chaindir/graph_silprob_4gram
if [ ! -z $step18 ]; then
  echo "## LOG (step18): started graph building @ `date`"
  utils/mkgraph.sh --self-loop-scale 1.0 $chain_lang $chaindir $graph
  echo "## LOG (step18): done with graph building ('$graph') @ `date`"
fi

if [ ! -z $step19 ]; then
  for dataName in dev-ykha; do
    data=../mgb-challenge3-2017/data/$dataName/mfcc-hires
    steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
    --nj 15 --cmd "$cmd" \
    --online-ivector-dir $nnetdir/ivectors-${dataName} \
    $graph $data $chaindir/decode-${dataName} || exit 1;
  done
fi
if [ ! -z $step20 ]; then
  for data in ../mgb-challenge3-2017/data/train_overall/mfcc-hires; do
    data_name=$(basename $(dirname $data))
    data_ivectors=$nnetdir/ivectors-${data_name}
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data $ivector_extractor $data_ivectors || exit 1
  done
fi

if [ ! -z $step21 ]; then
  echo "## LOG (step21): decoding train_overall started @ `date`"
  for dataName in train_overall; do
    data=../mgb-challenge3-2017/data/$dataName/mfcc-hires
    steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
    --nj 15 --cmd "$cmd" \
    --online-ivector-dir $nnetdir/ivectors-${dataName} \
    $graph $data $chaindir/decode-${dataName} || exit 1;
  done
  echo "## LOG (step21): done @ `date`"
fi
