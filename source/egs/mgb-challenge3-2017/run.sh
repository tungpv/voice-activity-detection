#!/bin/bash 

. path.sh
. cmd.sh

# begin sub
cmd='slurm.pl --quiet'
nj=120
steps=
numleaves=7000
hidden_dim=1024
train_stage=-10
get_egs_stage=-10
frames_per_eg=150
minibatch_size=128
num_epochs=4
proportional_shrink=0
chainname=chain1024
num_jobs_initial=3
num_jobs_final=16
remove_egs=false
rnnlm_train_step=4
max_param_change=2.0
initial_effective_lrate=0.001
final_effective_lrate=0.0001
xent_regularize=0.1
remove_egs=false
get_egs_stage=-10
common_egs_dir=
# end sub

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 [Example]: $0 --steps 1   \
  ./transcript/train17a.transcript.txt \
  ./transcript/dev17b.transcript.txt \
  ./train17a.list ./dev17b.list \
  ./wav/train  ./wav/dev \
  ./data

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 7 ]; then
  Example && exit 1
fi

train_transcript=$1
dev_transcript=$2
train_list=$3
dev_list=$4
train_wav=$5
dev_wav=$6
data=$7

train=$data/train_overall
[ -d $train ] || mkdir -p $train
if [ ! -z $step01 ]; then
  echo "## make training data"
  source/egs/mgb-challenge3-2017/make-data.pl $train_transcript /data/users/hhx502/mgb-challenge3-2017/wav/train \
  $train
  utils/utt2spk_to_spk2utt.pl < $train/utt2spk > $train/spk2utt
  utils/fix_data_dir.sh $train
  echo "## done & check '$train'"
fi
dev=$data/dev
[ -d $dev ] || mkdir -p $dev
if [ ! -z $step02 ]; then
  echo "## make dev data"
  source/egs/mgb-challenge3-2017/make-data.pl $dev_transcript /data/users/hhx502/mgb-challenge3-2017/wav/dev \
  $dev
  utils/utt2spk_to_spk2utt.pl < $dev/utt2spk > $dev/spk2utt
  utils/fix_data_dir.sh $dev
  echo "## done & check '$dev'"

fi
pmer=30
train_pmer30=$data/train_pmer$pmer
[ -d $train_pmer30 ] || mkdir -p $train_pmer30
if [ ! -z $step03 ]; then
  echo "## make training "
  source/egs/mgb-challenge3-2017/make-data.pl --pmer=30  $train_transcript /data/users/hhx502/mgb-challenge3-2017/wav/train \
  $train_pmer30
  utils/utt2spk_to_spk2utt.pl < $train_pmer30/utt2spk > $train_pmer30/spk2utt
  utils/fix_data_dir.sh $train_pmer30
  echo "## done & check '$train_pmer30'"
fi
dict=data/dict
[ -d $dict ] || mkdir -p $dict
if [ ! -z $step04 ]; then
  (
    echo -e "<unk>\tspn"
    cat ./dict/crpx.dct | perl -ane 'use utf8; use open qw(:std :utf8); chomp; @A = split(/\s/); $w = shift @A; 
    $pron = join(" ", @A); printf("%s\t%s\n", $w, $pron); '| sort
   ) > $dict/lexicon.txt
  echo -e "sil\nspn" > $dict/silence_phones.txt
  cat ./dict/crpx_phonelist.txt | egrep -v "(sil|sp|spn)" > $dict/nonsilence_phones.txt
  echo sil > $dict/optional_silence.txt
  echo "sil spn" > $dict/extra_questions.txt
  utils/validate_dict_dir.pl $dict
fi
lang=data/lang
[ -d $lang ] || mkdir -p $lang
if [ ! -z $step05 ]; then
  utils/prepare_lang.sh $dict "<unk>" $lang/tmp $lang 
fi
lmdir=data/lm-4gram
[ -d $lmdir ] || mkdir -p $lmdir
if [ ! -z $step06 ]; then
  cut -f1 $dict/lexicon.txt |sort -u | gzip -c > $lmdir/vocab.gz
  cut -d' ' -f2- $train/text | gzip -c > $lmdir/trans.gz
  cat ./lm/mgb.normalized.lm | gzip -c > $lmdir/mgb.gz
  cut -d' ' -f2- $dev/text | gzip -c > $lmdir/dev.gz
  echo "## LOG: done with data preparation in '$lmdir'"
fi

if [ ! -z $step07 ]; then
  echo "## LOG (step07): started @ `date`"
  ngram-count -order 4 -wbdiscount -interpolate\
  -vocab $lmdir/vocab.gz  -unk -sort -text $lmdir/merge.gz -lm $lmdir/lm-wb-merge.gz
  ngram-count -order 4 -kndiscount -interpolate\
  -vocab $lmdir/vocab.gz  -unk -sort -text $lmdir/merge.gz -lm $lmdir/lm-kn-merge.gz
  for lambda in $(seq 0 0.1 0.6); do
    ngram -order 4  -lm $lmdir/lm-wb-merge.gz -vocab $lmdir/vocab.gz -lambda $lambda \
    -mix-lm $lmdir/lm-kn-merge.gz -ppl $lmdir/dev.gz
  done
  echo "## LOG (step07): ended @ `date`"
fi
lang_4gram=../mgb-challenge3-2017/data/lang-4gram
[ -d $lang_4gram ] || mkdir $lang_4gram
if [ ! -z $step08 ]; then
  echo "## LOG (step08): started @ `date`"
  lmdir=/data/users/hhx502/mgb-challenge3-2017/data/lm-4gram
  if [ ! -e $lmdir/prune8-lm-kn4-merge.gz ] ;then
    ngram -order 4 -lm $lmdir/lm-kn-merge.gz -prune 1.0e-8 -write-lm $lmdir/prune8-lm-kn4-merge.gz
  fi
  source/egs/fisher-english/arpa2G.sh $lmdir/prune8-lm-kn4-merge.gz /data/users/hhx502/mgb-challenge3-2017/data/lang \
  $lang_4gram
  echo "## LOG (step08): done & check '$lang_4gram' @ `date`"
fi

if [ ! -z $step09 ]; then
  for sdata in  /data/users/hhx502/mgb-challenge3-2017/data/{dev,train_overall}; do
    dataName=$(basename $sdata)
    data=../mgb-challenge3-2017/data/$dataName/mfcc-pitch
    feat=../mgb-challenge3-2017/data/$dataName/feat/data/mfcc-pitch
    log=../mgb-challenge3-2017/data/$dataName/feat/log/mfcc-pitch
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step09, $0): done with feature extraction with '$data' @ `date`"
    data=../mgb-challenge3-2017/data/$dataName/mfcc-hires; 
    feat=../mgb-challenge3-2017/data/$dataName/feat/data/mfcc-hires; 
    log=../mgb-challenge3-2017/data/$dataName/feat/log/mfcc-pitch/mfcc-hires
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step09, $0): done with data '$data' @ `date`"
  done  
fi
expdir=../mgb-challenge3-2017/exp
if [ ! -z $step10 ]; then
  echo "## LOG (step15): training started @ `date`"
  lang=/data/users/hhx502/mgb-challenge3-2017/data/lang
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1-5 \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 10000 --pdf-num 100000 \
  ../mgb-challenge3-2017/data/train_overall/mfcc-pitch $lang $expdir  || exit 1
  echo "## LOG (step15, $0): done with '$expdir/tri4a' @ `date`"
fi
graph=$expdir/tri4a/graph-4gram
if [ ! -z $step11 ]; then
  echo "## LOG (step11): started @ `date`"
  utils/mkgraph.sh $lang_4gram $expdir/tri4a  $graph
  echo "## LOG (step11): done @ `date`"
fi
if [ ! -z $step12 ]; then
  echo "## LOG (step12): decoding started @ `date`"
  steps/decode_fmllr.sh --cmd "$cmd" --nj 10 \
  $graph \
  ../mgb-challenge3-2017/data/dev/mfcc-pitch  $expdir/tri4a/decode-dev || exit 1
  echo "## LOG (step12): done @ `date`"
fi
alidir=$expdir/tri4a/ali_train
data=../mgb-challenge3-2017/data/train_overall/mfcc-hires
nnetdir=$expdir/tdnn
transform_dir=$nnetdir/lda-mllt-transform-for-ivector-extractor
if [ ! -z $step13 ]; then
  steps/train_lda_mllt.sh --cmd "$cmd"  --num-iters 13 \
  --realign-iters "" \
  --splice-opts "--left-context=3 --right-context=3" \
  8000 80000 $data  $lang_4gram \
  $alidir $transform_dir || exit 1
  echo "## LOG (step13, $0): done with '$transform_dir'"
fi
if [ ! -z $step14 ]; then
  steps/online/nnet2/train_diag_ubm.sh --cmd "$cmd" --nj $nj \
  --num-threads 2 \
  --num-frames 720000 $data  512  $transform_dir $nnetdir/diag_ubm || exit 1
fi
ivector_extractor=$nnetdir/ivector-extractor
if [ ! -z $step15 ]; then
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$cmd" --nj $nj \
  --num-threads 1 --num-processes 1 \
  $data  $nnetdir/diag_ubm $ivector_extractor || exit 1;
  echo "## LOG (step15): done with '$ivector_extractor'"
fi
train_ivectors=$nnetdir/ivectors-train
if [ ! -z $step16 ]; then
  data=../mgb-challenge3-2017/data/train_overall/mfcc-hires
  data2=../mgb-challenge3-2017/data/train_overall/mfcc-hires-max2
  steps/online/nnet2/copy_data_dir.sh --utts-per-spk-max 2 $data  $data2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data2 $ivector_extractor $train_ivectors || exit 1
  echo "## LOG (step16): done with ivectors generation with data '$train_ivectors' @ `date`"
fi
if [ ! -z $step17 ]; then
  for data in ../mgb-challenge3-2017/data/dev/mfcc-hires; do
    data_name=$(basename $(dirname $data))
    data_ivectors=$nnetdir/ivectors-${data_name}
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data $ivector_extractor $data_ivectors || exit 1
  done
fi
srcdir=$expdir/tri4a
latdir=$srcdir/ali_lattice-train
alidir=$srcdir/ali_train
if [ ! -z $step18 ]; then
  echo "## LOG (step18): ali_fmllr_lats started @ `date`"
  data=../mgb-challenge3-2017/data/train_overall/mfcc-pitch
  nj=$(cat $alidir/num_jobs) || exit 1;
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $data \
  $lang_4gram $srcdir  $latdir || exit 1
  rm $latdir/fsts.*.gz # save space
  echo "## LOG (step18): done & check '$latdir' @ `date`"
fi
lang=$lang_4gram
nnetdir=$expdir/tdnn
chain_lang=$nnetdir/lang-chain
if [ ! -z $step19 ]; then
  rm -rf $chain_lang
  cp -r $lang $chain_lang
  silphonelist=$(cat $chain_lang/phones/silence.csl) || exit 1;
  nonsilphonelist=$(cat $chain_lang/phones/nonsilence.csl) || exit 1;
  # Use our special topology... note that later on may have to tune this
  # topology.
  steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >$chain_lang/topo
  echo "## LOG (step19): done with lang-chain ('$chain_lang') @ `date`"
fi
leftmost_questions_truncate=-1
treedir=$nnetdir/chain-tree
datadir=../mgb-challenge3-2017/data/train_overall/mfcc-pitch
alidir=$expdir/tri4a/ali_train
numleaves=7000
if [ ! -z $step20 ]; then
  # Build a tree using our new topology. This is the critically different
  # step compared with other recipes.
  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
  --leftmost-questions-truncate $leftmost_questions_truncate \
  --context-opts "--context-width=2 --central-position=1" \
  --cmd "$cmd" $numleaves $datadir $chain_lang $alidir $treedir
  echo "## LOG (step20): done with tree building '$treedir' @ `date`"
fi
data_gmm=../mgb-challenge3-2017/data/train_pmer30/mfcc_pitch
data_tdnn=../mgb-challenge3-2017/data/train_pmer30/mfcc_hires
if [ ! -z $step21 ]; then
  utils/subset_data_dir.sh --utt-list /data/users/hhx502/mgb-challenge3-2017/data/train_pmer30/segments \
  ../mgb-challenge3-2017/data/train_overall/mfcc-pitch   $data_gmm
  utils/subset_data_dir.sh --utt-list /data/users/hhx502/mgb-challenge3-2017/data/train_pmer30/segments \
  ../mgb-challenge3-2017/data/train_overall/mfcc-hires  $data_tdnn
fi
alidir_data_gmm=$expdir/tri4a/ali_train_pmer30
# begin group01
if [ ! -z $step22 ]; then
  echo "## LOG (step22): align started @ `date`"
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj \
  $data_gmm $lang $expdir/tri4a $alidir_data_gmm || exit 1
  echo "## LOG (step22): done @ `date`"
fi
srcdir_gmm=$expdir/tri4a_train_pmer30
if [ ! -z $step23 ]; then
  echo "## LOG (step23): training started @ `date`"
  steps/train_sat.sh --cmd "$cmd" \
  8000 100000 $data_gmm  $lang  $alidir_data_gmm $srcdir_gmm || exit 1
  echo "## LOG (step23): done @ `date`"
fi
graph_gmm=$srcdir_gmm/graph-4gram
if [ ! -z $step24 ]; then
  echo "## LOG (step24): started @ `date`"
  utils/mkgraph.sh $lang_4gram $srcdir_gmm  $graph_gmm
  echo "## LOG (step24): done @ `date`"
fi
if [ ! -z $step25 ]; then
  echo "## LOG (step25): decoding started @ `date`"
  steps/decode_fmllr.sh --cmd "$cmd" --nj 10 \
  $graph_gmm \
  ../mgb-challenge3-2017/data/dev/mfcc-pitch  $srcdir_gmm/decode-dev || exit 1
  echo "## LOG (step25): done @ `date`"
fi
# end group01

# begin group02
if [ ! -z $step26 ]; then
  for sdata in  ../mgb-challenge3-2017/data/dev-ykha; do
    data=$sdata/mfcc-pitch
    feat=$sdata/feat/data/mfcc-pitch
    log=$sdata/feat/log/mfcc-pitch
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step26): done with feature extraction with '$data' @ `date`"
    data=$sdata/mfcc-hires; 
    feat=$sdata/feat/data/mfcc-hires; 
    log=$sdata/feat/log/mfcc-pitch/mfcc-hires
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step26): done with data '$data' @ `date`"
  done  
fi

if [ ! -z $step27 ]; then
  echo "## LOG (step27): decoding started @ `date`"
  steps/decode_fmllr.sh --cmd "$cmd" --nj 15 \
  $graph_gmm \
  ../mgb-challenge3-2017/data/dev-ykha/mfcc-pitch  $srcdir_gmm/decode-dev-ykha || exit 1
  echo "## LOG (step27): done @ `date`"
fi
# begin gropu02

# begin group03 silprob model
lang_silprob=../mgb-challenge3-2017/data/lang-silprob
olddictdir=/data/users/hhx502/mgb-challenge3-2017/data/dict
datadir=../mgb-challenge3-2017/data
dictdir=$datadir/dict-silprob
if [ ! -z $step28 ]; then
  echo "## LOG (step28): making silprob started @ `date`"
  [ -d $dictdir ] || mkdir -p $dictdir  
  steps/get_prons.sh --cmd "$cmd" \
  $data_gmm $lang_4gram $srcdir_gmm || exit 1
  utils/dict_dir_add_pronprobs.sh --max-normalize true \
  $olddictdir \
  $srcdir_gmm/pron_counts_nowb.txt $srcdir_gmm/sil_counts_nowb.txt \
  $srcdir_gmm/pron_bigram_counts_nowb.txt $dictdir || exit 1
  
  utils/prepare_lang.sh $dictdir "<unk>" $lang_silprob/tmp $lang_silprob || exit 1;
  echo "## LOG (step28): done & check '$lang_silprob' @ `date`"
fi
graph_silprob_4gram=$srcdir_gmm/graph-silprob-4gram
if [ ! -z $step29 ]; then
  echo "## LOG (step29): started @ `date`"
  cp $lang_4gram/G.fst $lang_silprob/
  utils/mkgraph.sh $lang_silprob $srcdir_gmm  $graph_silprob_4gram  
  echo "## LOG (step29): ended @ `date`"
fi
if [ ! -z $step30 ]; then
    echo "## LOG (step30): decoding started @ `date`"
  steps/decode_fmllr.sh --cmd "$cmd" --nj 15 \
  $graph_silprob_4gram \
  ../mgb-challenge3-2017/data/dev-ykha/mfcc-pitch  $srcdir_gmm/decode-dev-yhha-silprob-4gram || exit 1
  echo "## LOG (step30): done @ `date`"
fi

# end group03
# begin group04: tdnn
train_ivectors30=$nnetdir/ivectors-train-pmer30
if [ ! -z $step31 ]; then
  data=../mgb-challenge3-2017/data/train_pmer30/mfcc_hires
  data2=../mgb-challenge3-2017/data/train_pmer30/mfcc_hires-max2
  steps/online/nnet2/copy_data_dir.sh --utts-per-spk-max 2 $data  $data2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data2 $ivector_extractor $train_ivectors30 || exit 1
  echo "## LOG (step31): done with ivectors generation with data '$train_ivectors' @ `date`"
fi
latdir_gmm=$srcdir_gmm/ali_lattice_train
data=../mgb-challenge3-2017/data/train_pmer30/mfcc_pitch
if [ ! -z $step32 ]; then
  echo "## LOG (step32): ali_fmllr_lats started @ `date`"
  nj=$(cat $alidir/num_jobs) || exit 1;
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $data \
  $lang_silprob $srcdir_gmm  $latdir_gmm || exit 1
  rm $latdir_gmm/fsts.*.gz # save space
  echo "## LOG (step32): done & check '$latdir' @ `date`"
fi
alidir=$srcdir_gmm/ali_train
if [ ! -z $step33 ]; then
  echo "## LOG (step33): ali_fmllr started @ `date`"
  steps/align_fmllr.sh --nj $nj --cmd "$cmd" \
  $data $lang_silprob $srcdir_gmm $alidir || exit 1
  echo "## LOG (step33): done @ `date`"
fi
lang=$lang_4gram
nnetdir=$expdir/tdnn
chain_lang=$nnetdir/lang-chain
if [ ! -z $step34 ]; then
  rm -rf $chain_lang
  cp -r $lang $chain_lang
  silphonelist=$(cat $chain_lang/phones/silence.csl) || exit 1;
  nonsilphonelist=$(cat $chain_lang/phones/nonsilence.csl) || exit 1;
  # Use our special topology... note that later on may have to tune this
  # topology.
  steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >$chain_lang/topo
  echo "## LOG (step34): done with lang-chain ('$chain_lang') @ `date`"
fi
leftmost_questions_truncate=-1
treedir=$nnetdir/chain-tree
datadir=../mgb-challenge3-2017/data/train_pmer30
numleaves=7000
if [ ! -z $step35 ]; then
  # Build a tree using our new topology. This is the critically different
  # step compared with other recipes.
  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
  --leftmost-questions-truncate $leftmost_questions_truncate \
  --context-opts "--context-width=2 --central-position=1" \
  --cmd "$cmd" $numleaves $datadir/mfcc_pitch $chain_lang $alidir $treedir
  echo "## LOG (step35): done with tree building '$treedir' @ `date`"
fi

chaindir=$nnetdir/$chainname

if [ ! -z $step37 ]; then
  num_targets=$(tree-info $treedir/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)

  mkdir -p $chaindir/configs
  cat <<EOF > $chaindir/configs/network.xconfig
  input dim=100 name=ivector
  input dim=40 name=input
  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor
  fixed-affine-layer name=lda input=Append(-1,0,1,ReplaceIndex(ivector, t, 0)) affine-transform-file=$chaindir/configs/lda.mat
  # the first splicing is moved before the lda layer, so no splicing here
  relu-renorm-layer name=tdnn1 dim=$hidden_dim
  relu-renorm-layer name=tdnn2 input=Append(-1,0,1) dim=$hidden_dim
  relu-renorm-layer name=tdnn3 input=Append(-1,0,1) dim=$hidden_dim
  relu-renorm-layer name=tdnn4 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn5 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn6 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn7 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn8 input=Append(-3,0,3) dim=$hidden_dim
  ## adding the layers for chain branch
  relu-renorm-layer name=prefinal-chain input=tdnn8 dim=$hidden_dim target-rms=0.5
  output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5
  # adding the layers for xent branch
  # This block prints the configs for a separate output that will be
  # trained with a cross-entropy objective in the 'chain' models... this
  # has the effect of regularizing the hidden parts of the model.  we use
  # 0.5 / args.xent_regularize as the learning rate factor- the factor of
  # 0.5 / args.xent_regularize is suitable as it means the xent
  # final-layer learns at a rate independent of the regularization
  # constant; and the 0.5 was tuned so as to make the relative progress
  # similar in the xent and regular final layers.
  relu-renorm-layer name=prefinal-xent input=tdnn8 dim=$hidden_dim target-rms=0.5
  relu-renorm-layer name=prefinal-lowrank-xent input=prefinal-xent dim=64 target-rms=0.5
  output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF
  steps/nnet3/xconfig_to_configs.py --xconfig-file $chaindir/configs/network.xconfig --config-dir $chaindir/configs/  
  echo "## LOG (step37): done with xconfig-file generation ('$chaindir/configs') @ `date`"
fi

traindata=$datadir/mfcc-hires
latdir=$expdir/tri4a/ali_lattice-train
if [ ! -z $step38 ]; then
  echo "## LOG (step38): training started @ `date`"
  steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$cmd" \
    --feat.online-ivector-dir $train_ivectors30 \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize $xent_regularize \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --egs.dir "$common_egs_dir" \
    --egs.stage $get_egs_stage \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width $frames_per_eg \
    --trainer.num-chunk-per-minibatch $minibatch_size \
    --trainer.frames-per-iter 1500000 \
    --trainer.num-epochs $num_epochs \
    --trainer.optimization.num-jobs-initial $num_jobs_initial \
    --trainer.optimization.num-jobs-final $num_jobs_final \
    --trainer.optimization.initial-effective-lrate $initial_effective_lrate \
    --trainer.optimization.final-effective-lrate $final_effective_lrate \
    --trainer.optimization.proportional-shrink $proportional_shrink \
    --trainer.max-param-change $max_param_change \
    --cleanup.remove-egs $remove_egs \
    --feat-dir $datadir/mfcc_hires \
    --tree-dir $treedir \
    --lat-dir $latdir_gmm \
    --dir $chaindir 
  echo "## LOG (step38): tdnn done (chaindir='$chaindir') @ `date`"
fi
graph=$chaindir/graph_silprob_4gram
if [ ! -z $step39 ]; then
  echo "## LOG (step39): started graph building @ `date`"
  utils/mkgraph.sh --self-loop-scale 1.0 $lang_silprob $chaindir $graph
  echo "## LOG (step39): done with graph building ('$graph') @ `date`"
fi
if [ ! -z $step40 ]; then
  for data in ../mgb-challenge3-2017/data/dev-ykha/mfcc-hires; do
    data_name=$(basename $(dirname $data))
    data_ivectors=$nnetdir/ivectors-${data_name}
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data $ivector_extractor $data_ivectors || exit 1
  done
fi
if [ ! -z $step41 ]; then
  for dataName in dev-ykha; do
    data=../mgb-challenge3-2017/data/$dataName/mfcc-hires
    steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
    --nj 15 --cmd "$cmd" \
    --online-ivector-dir $nnetdir/ivectors-${dataName} \
    $graph $data $chaindir/decode-${dataName} || exit 1;
  done
fi
rnnlmdata=../mgb-challenge3-2017/data/rnnlm-data
[ -d $rnnlmdata ] || mkdir -p $rnnlm
if [ ! -z $step50 ]; then
  echo "## LOG (step50): prepare data for rnnlm language modeling"
  gzip -cd /data/users/hhx502/mgb-challenge3-2017/data/lm-4gram/merge.gz | \
  source/egs/sge2017/local/normalize-text/word-count.pl | gzip -c > $rnnlmdata/word-count.gz  
  echo "## LOG (step50): done & check '$rnnlmdata/word-count.gz'"
fi
nVocSize=50000
input_word_list=$rnnlmdata/input-word-list.txt
if [ ! -z $step51 ]; then
  gzip -cd $rnnlmdata/word-count.gz | \
  source/egs/mgb-challenge3-2017/select-word-list.pl --expected-size=$nVocSize \
  --boundary-word='<s>' \
  ../mgb-challenge3-2017/data/lang-4gram/words.txt  > $input_word_list || exit 1
  echo "## LOG (step51): done with '$input_word_list'"
fi
nVocSize=30000
output_word_list=$rnnlmdata/output-word-list.txt
if [ ! -z $step52 ]; then
  gzip -cd $rnnlmdata/word-count.gz | \
  source/egs/mgb-challenge3-2017/select-word-list.pl --expected-size=$nVocSize \
  --boundary-word='</s>' \
  ../mgb-challenge3-2017/data/lang-4gram/words.txt  >$output_word_list || exit 1
  echo "## LOG (step52) done with '$output_word_list'"
fi
if [ ! -z $step53 ]; then
   echo "## LOG (step53): rnnlm training started @ `date`"
   source/egs/rnnlm/train.sh --steps $rnnlm_train_step \
   --cmd "$cmd" \
   --batchsize 128 \
   --datadir $rnnlmdata \
   --expdir exp-rnnlm \
   --gzsource_data /data/users/hhx502/mgb-challenge3-2017/data/lm-4gram/merge.gz \
   --validate-utterance-number 7000000 \
    $rnnlmdata  ../mgb-challenge3-2017 
   echo "## LOG (step53): done & check '../mgb-challenge3-2017/exp-rnnlm' @ `date`"
fi
percent=40
if [ ! -z $step54 ]; then
  echo "## LOG (step54): started @ `date`"
  source/egs/mgb-challenge3-2017/subset-data.pl --get-head  --percentage=$percent \
  "gzip -cd /data/users/hhx502/mgb-challenge3-2017/data/lm-4gram/merge.gz|" |  \
  gzip -c > /data/users/hhx502/mgb-challenge3-2017/data/lm-4gram/subset-${percent}-percent.gz
  echo "## LOG (step54): done @ `date`"
fi

if [ ! -z $step55 ]; then
  source/egs/mgb-challenge3-2017/subset-data.pl --get-head  --percentage=10 \
  "gzip -cd /data/users/hhx502/mgb-challenge3-2017/data/lm-4gram/subset-40-percent.gz|" \
  >  ../mgb-challenge3-2017/data/rnnlm-data/validate.txt
 
 source/egs/mgb-challenge3-2017/subset-data.pl --percentage=10 \
  "gzip -cd /data/users/hhx502/mgb-challenge3-2017/data/lm-4gram/subset-40-percent.gz|" \
  >  ../mgb-challenge3-2017/data/rnnlm-data/train.txt 
fi
