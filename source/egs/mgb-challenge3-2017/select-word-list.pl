#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

use Getopt::Long;
my $expected = 20000;
my $boundary = '<s>';
GetOptions('expected-size|expected_size=i' => \$expected,
	   'boundary-word|boundary_word=s' => \$boundary) or die;

# begin sub
sub InsertVocab {
  my ($vocab, $word, $pron) = @_;
  $pron =~ s:\s+: :g; $pron =~ s:^\s+::g; $pron =~ s:\s+$::g; 
  if (exists $$vocab{$word}) {
    my $pronVocab = $$vocab{$word};
    if (not exists $$pronVocab{$pron}) {
       $$pronVocab{$pron} ++;
    }
  } else {
    my %temp = ();
    my $pronVocab = $$vocab{$word} = \%temp;
    $$pronVocab{$pron} ++;
  }
}
sub LoadVocab {
  my ($inFile, $vocab) = @_;
  open (F, "$inFile") or die;
  while(<F>) {
    chomp;
    m:^(\S+)\s*(.*)$:g or next;
    my ($word, $pron) = ($1, $2);
    InsertVocab($vocab, $word, $pron);
  }
  close F;
}
# end sub

my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  die "\nExample: cat word-count.gz | $0 [--expected-size=20000|--boundary-word='<s>'] <vocab>\n\n";
}
my ($vocabFile) = @ARGV;
my %vocab = ();
LoadVocab($vocabFile, \%vocab);
print STDERR "## LOG ($0): stdin expected\n";
my $nSelected = 0;
while(<STDIN>) {
  chomp;
  m:(\S+)\s+(\S+): or next;
  my ($word, $count) = ($1, $2);
  if(exists $vocab{$word}) {
    if($nSelected < $expected) {
      $word = $boundary if ($nSelected == 0);
      my $line = sprintf("%-5d %s", $nSelected, $word);
      print "$line\n";
      $nSelected ++;
    } else {
      my $line = sprintf("%-5d %s", $nSelected, '<OOS>');
      print "$line\n";
      last;
    }
  }
}
print STDERR "## LOG ($0): stdin ended\n";
