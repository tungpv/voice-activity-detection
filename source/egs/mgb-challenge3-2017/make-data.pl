#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
my $pmer=100;
my $wmer=100;
GetOptions('pmer=i' => \$pmer,
           'wmer=i' => \$wmer) or die;
# begin sub

# end sub

my $numArgs = @ARGV;

if ($numArgs != 3) {
  die "\nExample $0 transcript.txt wavdir tgtdir\n\n";
}
my ($transFile, $wavDir, $tgtdir) = @ARGV;

open(TRAN, "$transFile") or die;
my $nIndex = 0;
my $fileName = '';
my $line = '';
my $nLine = 0;
open(WAV, ">", "$tgtdir/wav.scp") or die;
open(TEXT, ">$tgtdir/text") or die;
open(SEG, "> $tgtdir/segments") or die;
open(U2S, ">$tgtdir/utt2spk") or die;
while (<TRAN>) {
  chomp;
  $line = $_;
  $line =~ s:\s*\S+\.xml$::g;  $line =~ s:^\s+::g;
  if(m:\s*(\S+).xml$:g) {
    my $oldFileName = $fileName; my $oldIndex = $nIndex;
    $fileName = $1;
    $fileName =~ s:^.*\/([^\/]+)$:$1:g;
    my $wavFile = sprintf("%s/%s.wav", $wavDir, $fileName);
    if( !-e $wavFile) {
      die "wave file '$wavFile' does not exist\n";
    }
    print WAV "$fileName $wavFile\n";
    $nIndex ++;
    if($line ne '') {
      $nLine ++;
      # print $oldFileName, ' ',  $line, "\n";
      my @A = split('\|', $line);
      die "bad line $line\n" if scalar @A != 7;
      my $speaker = lc $A[0];
      $speaker = sprintf("mgb17-%04d-%s", $oldIndex, $speaker);
      my $start = $A[1]; my $end = $A[2];
      next if($A[3] > $pmer);
      next if($A[4] > $wmer);
      my $utt = sprintf("%s-%07d-%07d-%04d-%04d-%04d", $speaker, $start*100, $end*100, $A[3]*100, $A[4]*100, $A[5]*100);
      print TEXT "$utt $A[6]\n";
      print SEG "$utt $oldFileName $start $end\n";
      print U2S "$utt $speaker\n";
    } else {
      print STDERR "empty line\n";
    }
    next;
  }
   my @A = split('\|');  my $nField = scalar @A;
   die "bad line2: '$_', $nField\n" if $nField != 7;
   my $speaker = lc $A[0];
   $speaker = sprintf("mgb17-%04d-%s", $nIndex, $speaker);
   my $start = $A[1]; my $end = $A[2];
   my $utt = sprintf("%s-%07d-%07d-%04d-%04d-%04d", $speaker, $start*100, $end*100, $A[3]*100, $A[4]*100, $A[5]*100);
   next if($A[3] > $pmer);
   next if($A[4] > $wmer);
   $nLine ++;
   print TEXT "$utt  $A[6]\n";
   print SEG "$utt $fileName $start $end\n";
   print U2S "$utt $speaker\n";
}
print STDERR "totalLine = $nLine\n";
close WAV;
close TEXT;
close SEG;
close U2S;

