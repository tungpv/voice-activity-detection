#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=20
cmd=run.pl
steps=
prepare_semi_data=false
prepare_semi_opts="<lmwt> <conf_thresh> <merge_tolerance> <lang> <unlabeled_data> <sdir> <latdir> <supdata>  <bnfedir> <bnfe_name> <featdir>"
utt_ctm=
fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
bnf_append_fmllr=false
bnf_append_fmllr_opts="bnfdata specdata dataname sdir transform_dir fmllrdata featdir appenddata"
gmm_test=false
gmm_test_opts="alidir trndata trnname lang graphname state mixture tgtdir devdata devname"
import_ali_opts="import_tri5_alidir"
lda_dim=40

fmllr_nnet_test=false
fmllr_nnet_test_opts="validating_rate learn_rate lang trnfmllr trnname gmmdir graphname labeldir  xtgtdir comb_test_steps comb_test(bool) devfmllr devname decodename acwt(0.0909) denlat_conf stgtdir "
fmllr_nnet_pretrain_cmd="steps/nnet/pretrain_dbn.sh --copy-feats-tmproot /local/hhx502 --splice 5 --nn-depth 5 --hid-dim 2048"
train_nnet_cmd="source/egs/train.sh --copy-feats-tmproot /local/hhx502"
tuning_data="tunedata tunename"
scoring_opts="--min-lmwt 8 --max-lmwt 24"
lattice_opts="--beam 13 --lattice-beam 8 --max-mem 500000000"

fbank_mnnet_xtrain=false
fbank_mnnet_xtrain_opts="validating_rate learn_rate lang gmmdir graphname mnnet_sdir trnfbank trnaname labeldir xtgtdir comb_test_steps comb_test devfbank devname decodename acwt denlat_conf stgtdir"
comb_test=false
cmvn_opts="--norm-means=true"
delta_opts="--delta-order=2"

fbank_nnet_test=false
fbank_nnet_pretrain_cmd="source/egs/pretrain_dbn_trap.sh --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 5 --hid-dim 2048"
fbank_nnet_test_opts="validating_rate learn_rate lang gmmdir graphname trnfbank trnname labeldir xtgtdir comb_test_steps comb_test devfbank devname decodename acwt denlat_conf stgtdir"

bnf_upto_gmm_test=false # steps=1,2,3,4,5 for bnf, 6,7 for bnfe generation
bnf_upto_gmm_test_opts="trnfbdata trnname alidir lang bnfedir bnfgmmdir graphname state mixture devfbdata devname featdir"
hierbn_train_opts="validating_rate learn_rate bn1_splice hid_dim hid_layers bn1_dim remove_last_layers bn2_splice bn2_dim bn2_splice_step"

bnf_nnet2=false
bnf_nnet2_opts="samples_per_iter numb_jobs_nnet  num_threads minibatch_size init_learning_rate final_learning_rate num_hidden_layers bottleneck_dim hidden_layer_dim train trainname lang alidir tgtdir devdata devname bnfdir state mixture bnfgmmdir graphname"


# end options

echo
echo "$0 $@"
echo

. parse_options.sh

function PrintOptions {
  cmdName=$(echo $0| perl -pe 's/^.*\///g;')
  cat <<END

$cmdName [options]:
cmd					# value, "$cmd"
nj					# value, "$nj"
steps					# value, "$steps"
prepare_semi_data			# value, $prepare_semi_data
prepare_semi_opts			# value, "$prepare_semi_opts"
utt_ctm					# value, "$utt_ctm"
fbankCmd				# value, "$fbankCmd"
bnf_append_fmllr			# value, $bnf_append_fmllr
bnf_append_fmllr_opts                   # value, "$bnf_append_fmllr_opts"
gmm_test				# value, $gmm_test, steps=1,2
gmm_test_opts				# value, "$gmm_test_opts"
import_ali_opts				# value, "$import_ali_opts", import_alidir/run_gmm2_import_tri5_alidir
lda_dim					# value, "$lda_dim"

fmllr_nnet_test				# value, $fmllr_nnet_test, steps=1,2,3,4,5,6
fmllr_nnet_test_opts			# value, "$fmllr_nnet_test_opts"
fmllr_nnet_pretrain_cmd		        # value, "$fmllr_nnet_pretrain_cmd"
train_nnet_cmd				# value, "$train_nnet_cmd"
tuning_data				# value, "$tuning_data"
scoring_opts				# value, "$scoring_opts"
lattice_opts				# value, "$lattice_opts"

fbank_mnnet_xtrain			# value, $fbank_mnnet_xtrain
fbank_mnnet_xtrain_opts			# value, "$fbank_mnnet_xtrain_opts"
comb_test				# value, $comb_test

cmvn_opts				# value, "$cmvn_opts"
delta_opts				# value, "$delta_opts"
fbank_nnet_test				# value, $fbank_nnet_test, "steps=1,2,3,4"
fbank_nnet_pretrain_cmd			# value, "$fbank_nnet_pretrain_cmd"
fbank_nnet_test_opts			# value, "$fbank_nnet_test_opts"

bnf_upto_gmm_test			# value, $bnf_upto_gmm_test, # steps=1,2,3,4,5 for bnf, 6,7 for bnfe generation
bnf_upto_gmm_test_opts			# value, "$bnf_upto_gmm_test_opts"
hierbn_train_opts			# value, "$hierbn_train_opts"

bnf_nnet2				# value, $bnf_nnet2
bnf_nnet2_opts				# value, "$bnf_nnet2_opts"

END
}

PrintOptions;

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=true
  done
fi
if $prepare_semi_data; then
  echo "$0: prepare_semi_data started @ `date`"
  optNames="lmwt conf_thresh merge_tolerance lang unlabeled_data sdir latdir supdata bnfedir bnfe_name featdir"
  . source/register_options.sh "$optNames" "$prepare_semi_opts" || exit 1
  [ ! -z $utt_ctm ] && step01=
  if [ ! -z $step01 ]; then
    acwt=$(echo "1/$lmwt"|bc -l)
    echo "$0: prepare_semi_data: acwt=$acwt"
    [ -f $sdir/final.mdl ] || \
    { echo "$0: prepare_semi_data: ERROR, final.mdl expected in $sdir"; exit 1; }
    [ -f $unlabeled_data/segments ] || \
    { echo "$0: segments file expected in unsupdata $unsupdata"; exit 1; }
    model=$sdir/final.mdl
    name=$lmwt
    $cmd LMWT=$lmwt:$lmwt $latdir/label_LMWT/log/get_ctm.$lmwt.log \
    lattice-align-words $lang/phones/word_boundary.int $model "ark:gzip -cd $latdir/lat.*.gz|" ark:- \| \
    lattice-to-ctm-conf --acoustic-scale=$acwt  ark:- - \| \
    utils/int2sym.pl -f 5 $lang/words.txt  \| tee $latdir/label_LMWT/$name.utt.ctm \| \
    utils/convert_ctm.pl $unlabeled_data/segments $unlabeled_data/reco2file_and_channel \
    '>' $latdir/label_LMWT/$name.ctm || exit 1;
  fi
  labeled_data=$sdir/labeled.$conf_thresh
  if [ -z $utt_ctm ]; then
    ctm=$latdir/label_$lmwt/${lmwt}.utt.ctm
  else
    ctm=$utt_ctm
  fi
  if [ ! -z $step02 ]; then
    [ -f $ctm ] || \
    { echo "$0: ctm file $ctm expected"; exit 1; }
    dir=$labeled_data
    [ -d $dir ] || mkdir -p $dir
    source/code/extract-sub-segment-with-conf --conf-thresh=$conf_thresh --merge-tolerance=$merge_tolerance \
    $unlabeled_data/segments  "egrep -v '<' $ctm |"  $dir/segments $dir/text
    cp $unlabeled_data/wav.scp $dir/wav.scp
    cat $dir/segments | perl -pe '@A = split(/\s+/); $s = $A[0]; $s =~ m/([^_]+_[^_])_*/; $_="$s $1\n";'  > $dir/utt2spk
    utils/utt2spk_to_spk2utt.pl < $dir/utt2spk > $dir/spk2utt
    utils/fix_data_dir.sh $dir
  fi
  semidata_name=mixlabeled.$conf_thresh
  semidata=$sdir/$semidata_name
  if [ ! -z $step03 ]; then
    tmpdir=$(mktemp -d -p `pwd`)
    trap "rm -rf $tmpdir" EXIT
    cp $supdata/* $tmpdir/
    rm $tmpdir/{feats.scp,cmvn.scp}
    utils/combine_data.sh $semidata $tmpdir $labeled_data
    utils/fix_data_dir.sh $semidata
  fi
   
  if [ ! -z $step04 ]; then
    source/egs/run-bnf.sh --nj $nj --cmd "$cmd" --make_fbank_pitch true \
    --make_fbank_pitch_opts "$semidata $semidata/fbank_pitch $featdir/fbank_pitch.${semidata_name}" --fbankCmd "$fbankCmd" || exit 1
  fi
  if [ ! -z $step05 ]; then
    [ -f $bnfedir/final.nnet ] || \
    { echo "$0: final.nnet expected from dir $bnfedir"; exit 1; }
    source/egs/run-bnf.sh --make_bnf2 true --make_bnf2_opts "$semidata/fbank_pitch $bnfedir $semidata/bnf.$bnfe_name $featdir/bnf.${semidata_name}.${bnfe_name}"
  fi
  echo "$0: prepare_semi_data ended @ `date`"
fi
if  $bnf_append_fmllr; then
  echo "$0: bnf_append_fmllr started @ `date`"
  optNames="bnfdata specdata dataname sdir transform_dir fmllrdata featdir appenddata"
  . source/register_options.sh "$optNames" "$bnf_append_fmllr_opts" || exit 1
  if [ ! -z $step01 ]; then
    source/egs/run-bnf.sh  --make_fmllr true --make_fmllr_opts \
    "$specdata $sdir $transform_dir $fmllrdata $featdir/fmllr.$dataname"
  fi
  if [ ! -z $step02 ]; then
   [ -f $fmllrdata/feats.scp ] || \
   { echo "$0: bnf_append_fmllr: ERROR, feats.scp expected from $fmllrdata"; exit 1; }
   source/egs/run-bnf.sh --paste_feats true --paste_feats_opts \
   "$bnfdata  $fmllrdata $appenddata  $featdir/bnf_append_fmllr.$dataname"
  fi
  echo "$0: bnf_append_fmllr ended @ `date`"
fi
if $fmllr_nnet_test; then
  echo "$0: fmllr_nnet_test started @ `date`"
  optNames="validating_rate learn_rate lang trnfmllr trnname gmmdir graphname labeldir xtgtdir comb_test_steps comb_test devfmllr devname decodename acwt denlat_conf stgtdir "
  . source/register_options.sh "$optNames" "$fmllr_nnet_test_opts" || exit 1
  if [ ! -z $step01 ]; then
    echo "fmllr_nnet_test: xnnet training started @ `date`"
    source/run-nnet.sh --validating-rate $validating_rate --learn_rate $learn_rate\
    --usefmllrdata true  --preTrnCmd "$fmllr_nnet_pretrain_cmd" ${cmvn_opts:+--cmvn_opts "$cmvn_opts"} \
    ${labeldir:+--labeldir $labeldir} \
    ${step02:+--PrepareData true} \
    ${step03:+--PreTrain true } \
    ${step04:+--TrainNnet true} --trnCmd "$train_nnet_cmd" --sdir $gmmdir\
    $lang $trnfmllr $xtgtdir || exit 1
   echo "fmllr_nnet_test: xnnet training ended @ `date`"
  fi
  xsdir=$xtgtdir/nnet
  optNames="tunedata tunename"
  . source/register_options.sh "$optNames" "$tuning_data" || exit 1
  if [ ! -f $tunedata/feats.scp ]; then
    echo "fmllr_nnet_test: separate tuning data used"
    tunedata=$trnfmllr
    tunename=$trnname
  fi
  devdata=$devfmllr
  echo "$0: fmllr_nnet_test ended @ `date`"
fi
if $fbank_nnet_test; then
  echo "$0: fbank_nnet_test started @ `date`"
  optNames="validating_rate learn_rate lang gmmdir graphname trnfbank trnname labeldir xtgtdir comb_test_steps comb_test devfbank devname decodename acwt denlat_conf stgtdir"
  . source/register_options.sh "$optNames" "$fbank_nnet_test_opts" || exit 1
  if [ ! -z $step01 ]; then
    source/run-nnet.sh --validating_rate $validating_rate --learn_rate $learn_rate \
    --train_fbank_data $trnfbank \
    --labeldir $labeldir \
    ${cmvn_opts:+--cmvn_opts "$cmvn_opts"} \
    ${delta_opts:+--delta_opts "$delta_opts"} \
    ${step02:+--PrepareData true} \
    ${step03:+--PreTrain true} --preTrnCmd "$fbank_nnet_pretrain_cmd" \
    ${step04:+--TrainNnet true} --trnCmd "$train_nnet_cmd" --sdir $gmmdir \
    $lang alidata $xtgtdir || exit 1
  fi
  xsdir=$xtgtdir/nnet
  optNames="tunedata tunename"
  . source/register_options.sh "$optNames" "$tuning_data" || exit 1
  if [ ! -f $tunedata/feats.scp ]; then
    echo "fmllr_nnet_test: separate tuning data used"
    tunedata=$trnfbank
    tunename=$trnname
  fi
  devdata=$devfbank
  
  echo "$0: fbank_nnet_test ended @ `date`"
fi
if $fbank_mnnet_xtrain; then
  optNames="validating_rate learn_rate lang gmmdir graphname mnnet_sdir trnfbank trnname labeldir xtgtdir comb_test_steps comb_test devfbank devname decodename acwt denlat_conf stgtdir"
  . source/register_options.sh "$optNames" "$fbank_mnnet_xtrain_opts" || exit 1
  if [ ! -z $step01 ]; then
    echo "tune_fbank_mnnet: step01: cross-training started @ `date`"
    source/run-nnet.sh --cmd "$cmd" --nj $nj --learn-rate $learn_rate \
    --validating-rate $validating_rate --usefbankdata true \
    --train_fbank_data $trnfbank --labeldir $labeldir \
    --sdir $gmmdir  \
    ${step02:+--PrepareData true} \
    ${step03:+--cross-train-nnet-full true --nnet-srcdir $mnnet_sdir} \
    $lang alidata $xtgtdir || exit 1
    echo "tune_fbank_mnnet: step01: cross-training ended @ `date`"
  fi
  optNames="tunedata tunename"
  . source/register_options.sh "$optNames" "$tuning_data" || exit 1
  if [ ! -f $tunedata/feats.scp ]; then
    tunedata=$trnfbank
    tunename=$trnname
  fi
  xsdir=$xtgtdir 
  devdata=$devfbank
fi

if $comb_test; then
  source/egs/comb_test.sh --cmd "$cmd" --nj $nj \
  --steps $comb_test_steps --nnet_comb_test true \
  --scoring_opts "$scoring_opts" \
  --lattice_opts "$lattice_opts" \
  --nnet_comb_opts \
  "$lang $tunedata $tunename $gmmdir $graphname $xsdir $devdata $devname $decodename $acwt $denlat_conf $stgtdir" || exit 1
fi

if $bnf_upto_gmm_test; then
  echo "$0: bnf_upto_gmm_test started @ `date`"
  optNames="trnfbdata trnname alidir lang bnfedir bnfgmmdir graphname state mixture devfbdata devname featdir"
  . source/register_options.sh "$optNames" "$bnf_upto_gmm_test_opts" || exit 1
  if [ ! -z $step01 ]; then
    echo "$0: hierbn_train started @ `date`"
    optNames="validating_rate learn_rate bn1_splice hid_dim hid_layers bn1_dim remove_last_layers bn2_splice bn2_dim bn2_splice_step"
    . source/register_options.sh "$optNames" "$hierbn_train_opts" || exit 1
    [ ! -z $lang ] && [ -f $lang/words.txt ] || \
    { echo "$0: hierbn_train: ERROR, lang($lang) not ready"; exit 1; }
    gmmdir=$(dirname $alidir)
    [ ! -z $gmmdir ] && [ -f $gmmdir/final.mdl ] || \
    { echo "$0: hierbn_train: ERROR, gmmdir($gmmdir) not ready"; exit 1; }
    hierbn_opts="--hid-dim $hid_dim --hid-layers $hid_layers --learn-rate $learn_rate"
    hierbn1_opts="$hierbn_opts"
    hierbn1_opts="$hierbn1_opts --bn-dim $bn1_dim --feat-type traps --splice $bn1_splice "
    hierbn1_cmd="$train_nnet_cmd $hierbn1_opts"
    hierbn_splice_cmd="utils/nnet/gen_splice.py --fea-dim=$bn1_dim --splice=$bn2_splice --splice-step=$bn2_splice_step |"
    hierbn_copy_cmd="nnet-copy --remove-last-layers=$remove_last_layers --binary=false"
    hierbn2_opts="$hierbn_opts --bn-dim $bn2_dim "
    hierbn2_cmd="$train_nnet_cmd $hierbn2_opts"
    [ -f $alidir/ali.1.gz ] || \
    { echo "$0: hierbn_train: ERROR, ali.1.gz expected from $alidir"; exit 1; }
    [ -d $bnfedir ] || mkdir -p $bnfedir
    source/run-nnet.sh --cmd "$cmd" --nj $nj --validating-rate $validating_rate \
    --train_fbank_data $trnfbdata \
    --labeldir $alidir \
    ${cmvn_opts:+--cmvn_opts "$cmvn_opts"} \
    ${delta_opts:+--delta_opts "$delta_opts"} \
    --sdir $gmmdir  --usefbankdata true \
    ${step02:+--PrepareData true} \
    ${step03:+--hierbn1-train true --hierbn1_cmd "$hierbn1_cmd"} \
    ${step04:+--hierbn-feature-transform true --hierbn-splice-cmd "$hierbn_splice_cmd" --hierbn-copy-cmd "$hierbn_copy_cmd" } \
    ${step05:+--hierbn2-train true --hierbn2-cmd "$hierbn2_cmd"} \
    $lang alidata $bnfedir || exit 1
    echo "$0: hierbn_train ended @ `date`"
  fi
  trnbnfdata=$bnfedir/$trnname
  if [ ! -z $step06 ]; then
    echo "bnf_upto_gmm_test: making bnf for $trnfbdata started @ `date`"
    source/egs/run-bnf.sh --make_bnf2 true --cmd "$cmd" --nj $nj \
    --make_bnf2_opts "$trnfbdata $bnfedir/hierbn2 $trnbnfdata $featdir/$(basename $bnfedir)/$trnname" || exit 1
    echo "bnf_upto_gmm_test: making bnf for $trnfbdata ended @ `date`"
  fi
  devbnfdata=$bnfedir/$devname
  if [ ! -z $step07 ]; then
    echo "bnf_upto_gmm_test: making bnf for $devfbdata started @ `date`"
    source/egs/run-bnf.sh --make_bnf2 true --cmd "$cmd" --nj $nj \
    --make_bnf2_opts "$devfbdata $bnfedir/hierbn2 $devbnfdata $featdir/$(basename $bnfedir)/$devname" || exit 1
    echo "bnf_upto_gmm_test: making bnf for $devfbdata ended @ `date`"
  fi
  gmm_test=true
  gmm_test_opts="$alidir $trnbnfdata $trnname $lang $graphname $state $mixture $bnfgmmdir $devbnfdata $devname"
  step01=true;   step02=true
  echo "$0: bnf_upto_gmm_test ended @ `date`"
fi

if $bnf_nnet2; then
  optNames="samples_per_iter num_jobs_nnet num_threads minibatch_size init_learning_rate final_learning_rate num_hidden_layers bottleneck_dim hidden_layer_dim train trnname lang alidir tgtdir dev devname bnfdir state mixture bnfgmmdir graphname" 
  . source/register_options.sh "$optNames" "$bnf_nnet2_opts" || exit 1
  echo "$0: bnf_nnet2 test started @ `date`"
  if [ ! -z $step01 ]; then
    echo "$0: training started @ `date`"
    steps/nnet2/train_tanh_bottleneck.sh \
    --samples_per_iter $samples_per_iter \
    --num-jobs-nnet $num_jobs_nnet  \
    --num-threads $num_threads \
    --minibatch-size $minibatch_size \
    --initial-learning-rate $init_learning_rate \
    --final-learning-rate $final_learning_rate \
    --num-hidden-layers $num_hidden_layers \
    --bottleneck-dim $bottleneck_dim --hidden-layer-dim $hidden_layer_dim \
    --cmd "$cmd" \
    $train $lang $alidir $tgtdir || exit 1
    echo "$0: training ended @ `date`"
  fi
  trnbnfdata=$tgtdir/$trnname
  if [ ! -z $step02 ]; then
    echo "$0: making $train nnet2_bnf started @ `date`"
    max_nj=$(wc -l < $train/spk2utt)
    [ $nj -gt $max_nj ] && nj=$max_nj
    steps/nnet2/dump_bottleneck_features.sh --cmd "$cmd" --nj $nj $train  $trnbnfdata \
    $tgtdir $bnfdir/$trainname/data $bnfdir/$trainname/log || exit 1
    echo "$0: making $train nnet2_bnf ended @ `date` "
  fi
  devbnfdata=$tgtdir/$devname
  if [ ! -z $step03 ]; then
    echo "$0: making $dev nnet2_bnf started @ `date`"
    max_nj=$(wc -l < $dev/spk2utt)
    [ $nj -gt $max_nj ] && nj=$max_nj
    steps/nnet2/dump_bottleneck_features.sh --cmd "$cmd" --nj $nj $dev  $devbnfdata \
    $tgtdir $bnfdir/$devname/data $bnfdir/$devname/log || exit 1
    echo "$0: making $dev nnet2_bnf ended @ `date`"
  fi
  gmm_test=true
  gmm_test_opts="$alidir $trnbnfdata $trnname $lang $graphname $state $mixture $bnfgmmdir $devbnfdata $devname"
  step01=true;   step02=true

  echo "$0: bnf_nnet2 test ended @ `date`"
fi

if $gmm_test; then
  optNames="alidir trndata trnname lang graphname state mixture tgtdir devdata devname"
  . source/register_options.sh "$optNames" "$gmm_test_opts" || exit 1
  echo "$0: gmm_train & test started @ `date`"
  if [ ! -z $step01 ]; then
    echo "gmm_test: training started @ `date` "
    import_ali_opts=
    [ -f $alidir/ali.1.gz ] && import_ali_opts="--$import_ali_opts $alidir --lda_dim $lda_dim"
    source/egs/run-bnf.sh  --cmd "$cmd" --nj $nj --run_gmm2 true ${steps:+--steps $steps} $import_ali_opts  --run_gmm2_opts \
   "a $trndata $trnname $lang $state $mixture $tgtdir" || exit 1
    echo "gmm_test: training ended @ `date`"
  fi
  sdir=$tgtdir/tri4a
  if [ ! -z $step02 ]; then
    source/egs/run-bnf.sh --decoding_eval true --decoding_opts \
    "$devdata  $lang  $sdir $sdir/graph-$graphname  $sdir/decode-${devname}-${graphname} steps/decode_fmllr.sh" \
    --lattice-opts ' '  || exit 1
  fi
  echo "$0: gmm_train & test ended @ `date`"
fi

