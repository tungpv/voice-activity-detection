#!/bin/bash

data=/home2/hhx502/ivector-test/mono/swahili-frame25-delta/exp-remove-short-utterance-5.0s/data/dev/mfcc-pitch
new_data=/home2/hhx502/ivector-test/mono/swahili-frame25-delta/exp-remove-short-utterance-5.0s/data/dev/mfcc-pitch-partial7.0
new_feat=/home2/hhx502/ivector-test/mono/swahili-frame25-delta/exp-remove-short-utterance-5.0s/data/dev/feat/mfcc-pitch-partial7.0

[ -d $new_data ] || mkdir -p $new_data
[ -d $new_feat ] || mkdir -p $new_feat
echo "## LOG ($0): merge started @ `date`"
source/code/splice-feats-by-speaker --min-samples=700 --partial-sequential-merge=true  \
ark:$data/spk2utt scp:$data/feats.scp scp:$data/vad.scp $new_data/utt2spk  ark,scp:$new_feat/mfcc-pitch.ark,$new_data/feats.scp \
 ark,scp:$new_feat/vad.ark,$new_data/vad.scp || exit 1
echo "## LOG ($0): merge ended ($new_data)  @ `date`"
