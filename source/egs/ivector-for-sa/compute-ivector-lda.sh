#!/bin/bash 

. path.sh
. cmd.sh 

# begin options 
steps=
dim=70
total_covariance_factor=0.1

# end options 

function Usage {
 cat <<EOF

 Usage: $0 <data> 
 [options]:
 [examples]:

 $0 --steps 1

EOF

}

. parse_options.sh || exit 1

if [ $# -ne 1 ]; then
  Usage && exit 1
fi

data=$1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

for x in ivector.scp utt2spk; do
  file=$data/$x
  [ -e $file ] || { echo "## ERROR ($0): ivector.scp file epected from $data"; exit 1; }
done

ivector-compute-lda --dim=$dim --total-covariance-factor=$total_covariance_factor \
"ark:copy-vector scp:$data/ivector.scp ark:-|" \
ark:$data/utt2spk $data/ivector-lda-transform.mat || exit 1


echo "## LOG ($0): ivector-lda-transform estimate done !" && exit 0
