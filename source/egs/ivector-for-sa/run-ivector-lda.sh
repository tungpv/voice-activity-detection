#!/bin/bash

. path.sh
. cmd.sh 

# begin options
steps=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF
 [examples]:

 $0 /home2/hhx502/ivector-test/mono/swahili-frame25-delta/exp-remove-short-utterance-5.0s/data/train/mfcc-pitch \
 /home2/hhx502/ivector-test/mono/swahili-frame25-delta/exp-remove-short-utterance-5.0s/data/dev/mfcc-pitch  \
 /home2/hhx502/ivector-test/mono/swahili-frame25-delta/exp-remove-short-utterance-5.0s/ivector-splice-no-2048-100/extractor

EOF
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

train_mfcc=$1
dev_mfcc=$2
iextractor=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): make lda transform mat for training data"
  source/egs/ivector-for-sa/compute-ivector-lda-2.sh --cmd slurm.pl --nj 40 \
  --steps 1,2,3,4 \
  $train_mfcc train_mfcc $iextractor || exit 1
  echo "## LOG (step01, $0): done"
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): train nnet @ `date`"
  source/egs/ivector-for-sa/run-nnet.sh --steps 3,4 --train-tool-opts "--minibatch-size=1024 --randomizer-size=32768 --randomizer-seed=777" \
  --delta-opts "--delta-order=2" --cmvn-opts "--norm-means=true" \
  --ivector-data-csl /home2/hhx502/ivector-test/mono/swahili-frame25-delta/exp-remove-short-utterance-5.0s/ivector-splice-no-2048-100/extractor/train_mfcc_overall/ivector.scp:/home2/hhx502/ivector-test/mono/swahili-frame25-delta/exp-remove-short-utterance-5.0s/ivector-splice-no-2048-100/extractor/dev_overall/ivector.scp \
 --ivector-lda-transform /home2/hhx502/ivector-test/mono/swahili-frame25-delta/exp-remove-short-utterance-5.0s/ivector-splice-no-2048-100/extractor/train_mfcc_overall/ivector-lda-transform.mat \
 --pretraining-rate 0.5 --nnet-id nnet5a-splice-no-2048-lda70 --devdata kws2016/vllp-grapheme/data/dev/fbank_pitch --graphdir kws2016/flp-grapheme/exp/mono/tri4a/graph kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang kws2016/flp-grapheme/exp/mono/tri4a/ali_train /home2/hhx502/ivector-test/mono/swahili-frame25-delta/exp-remove-short-utterance-5.0s
  echo "## LOG (step02, $0): done @ `date`"
fi
