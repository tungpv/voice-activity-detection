#!/bin/bash

. path.sh
. cmd.sh 

echo 
echo "## LOG: $0 $@"
echo

# begin options
steps=
cmd=run.pl
nj=17
utterance_min_sec=
tmproot=/local/hhx502
dev_data=kws2016/vllp-grapheme/data/dev/fbank_pitch
data_name="train dev"
train_ivector_opts="--num-components 2048  --ivector-dim 100 --num-iters 10"
run_nnet_opts="--pretraining-rate 0.5 --nnet-id nnet5a --devdata  kws2016/vllp-grapheme/data/dev/fbank_pitch --graphdir kws2016/flp-grapheme/exp/mono/tri4a/graph kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang kws2016/flp-grapheme/exp/mono/tri4a/ali_train"
# end options
runName=$(basename $0)
function Usage {
cat<<EOF
 $runName [options] <source_data_dir>  <tgtdir>
 [options]:
 [examples]:

 $0 --steps 1 --cmd "$cmd" --nj $nj \
 --data-name "$data_name" \
 --tmproot /local/hhx502 --utterance-min-sec 5.0 \
 --run-nnet-opts "$run_nnet_opts" \
 --train-ivector-opts "$train_ivector_opts" \
 --dev-data $dev_data  kws2016/flp-grapheme/data/train/fbank_pitch /home2/hhx502/ivector-test/mono/swahili 

$0 --steps 1,3,4,5,6 --cmd "$cmd" --nj $nj \
 --data-name "$data_name" \
 --tmproot /local/hhx502 --utterance-min-sec 5.0 \
 --run-nnet-opts "$run_nnet_opts" \
 --train-ivector-opts "$train_ivector_opts" \
 --dev-data $dev_data  kws2016/flp-grapheme/data/train/fbank_pitch /home2/hhx502/ivector-test/mono/swahili 

  

EOF
}

. parse_options.sh

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
source_train_data=$1
tgtdir=$2
tgtdata=$tgtdir/data
nameArray=($data_name)
tmpdir=$(mktemp -d -p $tmproot)
trap "rm -rf $tmpdir 2>/dev/null" EXIT
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): make mfcc-pitch for data @ `date`"
  i=0
  for sdata in $source_train_data $dev_data; do
    dataName=${nameArray[$i]}; i=$[i+1]
    data=$tgtdata/$dataName/mfcc-pitch
    feat=$tgtdata/$dataName/feat/mfcc-pitch
   
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj \
    --mfcc-for-ivector true --mfcc-cmd \
    "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc-sre.conf --pitch-config conf/pitch.conf" \
    $sdata  $data $feat 
    source/egs/swahili/ivector/sre08-sid/compute_vad_decision.sh --nj $nj \
    --cmd "$cmd" --nj $nj \
    $data  $feat/log $feat || exit 1
    echo "## LOG (step01, $0): done for data ($sdata) @ `date`"
  done
  echo "## LOG (step01, $0):  Done  @ `date`"
fi
overall_train=$tgtdata/${nameArray[0]}/mfcc-pitch
subset_train=$tgtdata/${nameArray[0]}$utterance_min_sec/mfcc-pitch
if [ ! -z $step02 ]; then
  if [ ! -z $utterance_min_sec ]; then
    [ -d $subset_train ] || mkdir -p $subset_train
    cat $overall_train/segments | \
    awk -v thresh=$utterance_min_sec '{ if($4-$3 >= thresh){print $1 ;} }' > $subset_train/utt-list
    subset_data_dir.sh --utt-list $subset_train/utt-list $overall_train $subset_train 
  fi
fi
subset_feat=$tgtdata/${nameArray[0]}$utterance_min_sec/feat/mfcc-pitch
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): partial sequential splice data"
  min_samples=$(perl -e '($sec, $factor) = @ARGV; print $sec*$factor' $utterance_min_sec 100)
  splice_opts="--min-samples=$min_samples --partial-sequential-merge=true"
  source/egs/swahili/splice-feats-by-speaker.sh --cmd "$cmd" --splice-opts "$splice_opts" \
  --logdir $subset_train/log \
  $overall_train $subset_train $subset_feat || exit 1
  echo "## LOG (step03, $0): done"
fi
train_name=${nameArray[0]}
set -e
ivector_id=$(echo "$train_ivector_opts" | perl -e '($spk_splice) = @ARGV; $opts = <STDIN>;  $splice="splice-no"; if ($spk_splice) {$splice="splice-yes"; } 
 $opts =~ m/.*num\-components\s+(\d+)\s+.*ivector\-dim\s+(\d+)\s+.*/ or die "## illegal opts ($opts)\n"; printf("%s-%d-%d", $splice, $1, $2); ' $(x=0; echo -n $x ))

echo "## LOG ($0): ivector_id=$ivector_id";

ivector_extractor=$tgtdir/ivector-${ivector_id}
if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): train  ivector extractor @ `date`"
  source/egs/ivector-for-sa/train-ivector-extractor.sh --steps 1,2,3,4 \
  --cmd "$cmd" --nj $nj  $train_ivector_opts  \
  --cmvn-opts "--norm-means=true"  $subset_train  $ivector_extractor
  echo "## LOG (step04, $0): ivector extractor  done @ `date`"
fi

if [ ! -z $step05 ]; then
  echo "## LOG (step05, $0): ivector extraction @ `date`"
  for i in $(seq 0 1); do
    data_name=${nameArray[$i]}
    data=$tgtdata/$data_name/mfcc-pitch
    source/egs/ivector-for-sa/extract-ivectors-2.sh --steps 1,2 --cmd $cmd --nj $nj \
    --splice-opts "--overall-merge=true" --ivector-id "overall" \
    $data $data_name $ivector_extractor/extractor || exit 1
  done
  echo "## LOG (step05, $0): done with ivector extraction @ `date`"
fi
dev_name=${nameArray[1]}
if ! echo "$run_nnet_opts" | grep -q 'nnet-id'; then
  echo "## ERROR ($0): nnet-id option expected in ('$run_nnet_opts')"  && exit 1
fi
run_nnet_opts="$(perl -e '($id, $opts) = @ARGV; $opts =~ s#(.*--nnet-id\s+)(\S+)(\s+.*$)#\1\2\-$id\3#g; print $opts; ' $ivector_id "$run_nnet_opts")"
echo "## LOG ($0): run_nnet_opts="$run_nnet_opts""
if [ ! -z $step06 ]; then
  echo "## LOG (step06, $0): run nnet @ `date`"
  source/egs/ivector-for-sa/run-nnet.sh --steps 1,2,3,4 \
  --train-tool-opts "--minibatch-size=1024 --randomizer-size=32768 --randomizer-seed=777" \
  --delta-opts "--delta-order=2" \
  --cmvn-opts "--norm-means=true"  \
  --ivector-data-csl "$ivector_extractor/extractor/${train_name}_overall/ivector.scp:$ivector_extractor/extractor/${dev_name}_overall/ivector.scp"  \
  $run_nnet_opts $tgtdir  || exit 1
  echo "## LOG (step06, $0): donw with nnet running @ `date`"
fi
