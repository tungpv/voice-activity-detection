#!/bin/bash

. path.sh
. cmd.sh 

# begin options
nj=40
cmd="slurm.pl"
steps=

validating_rate=0.1
learn_rate=0.008
cmvn_opts="--norm-means=true"
delta_opts=                           # "--delta-order=2"
nnet_train_cmd=/home/hhx502/w2016/steps/nnet/train.sh
nnet_pretrain_cmd="/home/hhx502/w2016/steps/nnet/pretrain_dbn.sh --feat-type traps  --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048"
pretraining_rate=
train_tool_opts="--minibatch-size=2048 --randomizer-size=32768 --randomizer-seed=777"
ivector_data_csl=
ivector_lda_transform=
append_vector_to_feats=source/code/append-vector-to-feats
nnet_id=nnet5a

devdata=
graphdir=
decodename=decode-dev
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15 "
# end options

echo
echo LOG: $0 $@
echo

. parse_options.sh || exit 1

function Usage {
 cat <<END
 
 Usage: $(basename $0) [options] <data> <lang> <alidir> <dir>
 [options]:
 --nj				# value, $nj
 --cmd				# value, "$cmd"
 --steps			# value, "$steps"

 --validating_rate		# value, $validating_rate
 --learn-rate			# value, $learn_rate
 --cmvn_opts			# value, "$cmvn_opts"
 --delta_opts			# value, "$delta_opts"
 --nnet-train-cmd		# value, "$nnet_train_cmd"
 --nnet-pretrain-cmd		# value, "$nnet_pretrain_cmd"
 --pretraining-rate             # value, $pretraining_rate
 --train-tool-opts              # value, "$train_tool_opts"
 --ivector-data-csl             # value, "$ivector_data_csl"
 --append-vector-to-feats       # value, "$append_vector_to_feats"
 --nnet-id			# value, "$nnet_id"

 --devdata			# value, "$devdata"
 --graphdir                     # value, "$graphdir"
 --decodename			# value, "$decodename"
 --decode-opts			# value, "$decode_opts"
 --scoring-opts			# value, "$scoring_opts"

 [steps]:
 1: prepare data 
 2: pretraining
 3: training
 4: test dev data, if any
 
[examples]:
  
 $0 --steps 1,2,3,4 --cmvn-opts "--norm-means=true" \
 --pretraining-rate 0.5 \
 --ivector-data-csl "/home2/hhx502/kws2016/exp-ivector-swahili/data/train/ivector.scp:/home2/hhx502/kws2016/exp-ivector-swahili/data/dev/ivector.scp" \
--devdata kws2016/vllp-grapheme/data/dev/fbank_pitch \
--graphdir kws2016/flp-grapheme/exp/mono/tri4a/graph \
kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang \
kws2016/flp-grapheme/exp/mono/tri4a/ali_train \
/home2/hhx502/kws2016/exp-ivector-swahili   

 source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 --delta-opts "--delta-order=2"  --nnet-pretrain-cmd "steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048" \
 --pretraining-rate 0.2   --nnet-id nnet5a --graphdir /local/hhx502/ldc-cts2016/fisher-english/exp/tri4a/graph --devdata fisher-english/data/dev/fbank-pitch \
 fisher-english/data/train/fbank-pitch fisher-english/data/lang  /local/hhx502/ldc-cts2016/fisher-english/exp/tri4a/ali_train  /local/hhx502/ldc-cts2016/fisher-english/exp
 
 $0  --steps 1,2,3,4 --delta-opts "--delta-order=2"  --nnet-pretrain-cmd "steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048" \
 --use-partial-data-to-pretrain true --pretraining-rate 0.3 --nnet-id nnet5a --graphdir hkust-mandarin-cts/exp/tri4a/graph \
 --devdata hkust-mandarin-cts/data/dev/fbank-pitch \
 hkust-mandarin-cts/data/train/fbank-pitch hkust-mandarin-cts/data/lang hkust-mandarin-cts/exp/tri4a/ali_train  hkust-mandarin-cts/exp 

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
alidir=$3
dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

train_ivector=
dev_ivector=
if [ ! -z "$ivector_data_csl" ]; then
  array=($(echo "$ivector_data_csl" | perl -pe 'chomp; s#[,;:]# #g;'))
  [ ${#array[@]} -eq 2 ] || \
  { echo "## ERROR ($0): illegal ivector_data_csl ('$ivector_data_csl')"; exit 1; }
  train_ivector=${array[0]}
  [ -e $train_ivector ] || \
  { echo "## ERROR ($0): $train_ivector expected"; exit 1; }
  dev_ivector=${array[1]}
  [ -e $dev_ivector ] || \
  { echo "## ERROR ($0): $dev_ivector expected"; exit 1; }
fi

tgtdir=$dir/$nnet_id
train=$tgtdir/train
valid=$tgtdir/valid
if [ ! -z $step01 ]; then
 source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
 --random true \
 --data2 $train \
 $data  $valid || exit 1
fi
pretrain_dir=$tgtdir/pretrain_dbn
if [ ! -z $step02 ]; then
  train_pretrain=$train
  if [ ! -z $pretraining_rate ]; then
     train_pretrain=$pretrain_dir/train
     source/egs/swahili/subset_data.sh --subset_time_ratio $pretraining_rate \
    --random true \
    $train  $train_pretrain || exit 1
  fi
  $nnet_pretrain_cmd ${cmvn_opts:+--cmvn_opts "$cmvn_opts"} ${delta_opts:+--delta-opts "$delta_opts"} \
  ${train_ivector:+ --ivector scp:$train_ivector --append-vector-to-feats "$append_vector_to_feats --utt2spk=ark:$train/utt2spk" } \
  ${ivector_lda_transform:+ --ivector-lda-transform $ivector_lda_transform} \
   --rbm-iter 1 $train_pretrain  $pretrain_dir || exit 1;
fi
nnetdir=$tgtdir/dnn
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): nnet training started @ `date`"
  hid_dim=$(echo "$nnet_pretrain_cmd" | perl -pe 'if(m/--hid-dim\s+(\d+)/){$_=$1;}else{exit 1;}')
  nn_depth=$(echo "$nnet_pretrain_cmd" | perl -pe 'if(m/--nn-depth\s+(\d+)/){$_=$1;}else{exit 1;}')
  dbn=$pretrain_dir/$nn_depth.dbn
  feature_transform=$pretrain_dir/final.feature_transform
  [ -f $dbn ] && [ -f $feature_transform ] || \
  { echo "## ERROR (step03, $0): dbn ($dbn) and feature_transform ($feature_transform) expected"; exit 1; }  
  $nnet_train_cmd --hid-dim $hid_dim \
  --feature-transform $feature_transform \
  ${train_ivector:+ --ivector scp:$train_ivector --append-vector-to-feats "$append_vector_to_feats --utt2spk=ark:$data/utt2spk"} \
  ${ivector_lda_transform:+ --ivector-lda-transform $ivector_lda_transform} \
  --dbn $dbn \
  --hid-layers 0  \
  --learn-rate $learn_rate \
  --train-tool-opts "$train_tool_opts" \
  $train $valid $lang $alidir \
  $alidir $nnetdir || exit 1; 
  echo "## LOG (step03, $0): ended @ `date`"
fi

decode_dir=$nnetdir/$decodename
if [ ! -z $step04 ] && [ ! -z $devdata ]; then
  echo "## LOG (step04, $0): decoding started @ `date`"
  [ ! -z $graphdir ] || { echo "## ERROR (step04, $0):, graphdir not specified"; exit 1; }
  effective_nj=$(wc -l < $devdata/spk2utt)
  [ $effective_nj -gt $nj ] && effective_nj=$nj
  steps/nnet/decode.sh --cmd "$cmd" --nj $effective_nj \
  ${train_ivector:+ --ivector scp:$dev_ivector --append-vector-to-feats "$append_vector_to_feats --utt2spk=ark:$devdata/utt2spk" } \
  ${ivector_lda_transform:+ --ivector-lda-transform $ivector_lda_transform} \
  --scoring-opts "$scoring_opts"  $decode_opts \
  $graphdir $devdata $decode_dir || exit 1
  echo "## LOG (step04, $0): ended @ `date`"
fi

echo "## LOG ($0): Done !"
