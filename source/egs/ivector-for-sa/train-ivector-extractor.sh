#!/bin/bash

echo
echo "## LOG: $0 $@"
echo

. path.sh
. cmd.sh

# begin options
steps=
cmd="slurm.pl --nodelist=node05,node06"
nj=34
num_components=2048
ivector_dim=100
num_iters=10
cmvn_opts="--norm-means=true"
# end options

. parse_options.sh || exit 1
function Usage {
 cat<<EOF
 $0 [options] 
 [options]:

 [examples]:

 $0 --steps 1 --cmd "$cmd" --nj $nj \
 --num-components $num_components \
 --ivector-dim 100 --num-iters 10 \
 --cmvn-opts "$cmvn_opts" \
 /home2/hhx502/sre08/data/train/mfcc  /home2/hhx502/ivector-for-sa

EOF
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

data=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
dubm_dir=$tgtdir/diag_ubm
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): diag ubm training started @ `date`"
  source/egs/ivector-for-sa/train_diag_ubm.sh  --nj $nj --cmd "$cmd" \
  --cmvn-opts "$cmvn_opts" \
  $data $num_components \
  $dubm_dir  || exit 1
  echo "## (step01, $0): done @ `date`"
fi
fubm=$tgtdir/full_ubm
if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): full ubm training started @ `date`"
  source/egs/ivector-for-sa/train_full_ubm.sh --cmd "$cmd" --nj $nj  \
  --cmvn-opts "$cmvn_opts" \
  --remove-low-count-gaussians false \
  $data  \
  $dubm_dir $fubm || exit 1
  echo "## (step02, $0): Done @ `date`"
fi
ivector_extractor_dir=$tgtdir/extractor
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): train ivector_extractor started @ `date`"
  source/egs/ivector-for-sa/train_ivector_extractor.sh --cmd "$cmd" \
  --cmvn-opts "$cmvn_opts" \
  --ivector-dim $ivector_dim \
  --num-processes 1 --num-threads 2 \
  --num-iters $num_iters $fubm/final.ubm $data \
  $ivector_extractor_dir || exit 1
  echo "$cmvn_opts" > $ivector_extractor_dir/cmvn_opts  
  echo "## LOG (step03, $0): done ($ivector_extractor_dir) @ `date`"
fi
