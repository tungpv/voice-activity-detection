#!/bin/bash

. path.sh
. cmd.sh 

# begin options
steps=
cmd=slurm.pl
nj=40
# end options

. parse_options.sh || exit 1

function Usage {
 cat <<EOF
 
 Usage: $0 [options]  <data> <ivector-extractor-dir>
 [options]:
 [examples]:

 $0  --cmd "$cmd" --nj $nj \
 --steps 1,2 /home2/hhx502/ivector-test/mono/swahili-frame25-delta/data/dev/mfcc-pitch \
 dev /home2/hhx502/ivector-test/mono/swahili-frame25-delta/ivector-splice-no-2048-100/extractor

EOF
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

data=$1
data_name=$2
iextractor=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): ivector extraction for partially spliced data"
  source/egs/ivector-for-sa/extract-ivectors-2.sh --cmd "$cmd" --nj $nj \
  --steps 1,2 --ivector-id "partial" \
  --splice-opts "--max-samples=3000 --min-samples=1500 --overall-merge=false" \
  $data $data_name $iextractor || exit 1 
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): ivector extraction for overall spliced data"
  source/egs/ivector-for-sa/extract-ivectors-2.sh --cmd "$cmd" --nj $nj \
  --steps 1,2 --ivector-id "overall" \
  --splice-opts "--overall-merge=true" \
  $data $data_name $iextractor || exit 1
fi
tmpdir=$(mktemp -d -p  $iextractor)
trap "rm -rf $tmpdir" EXIT
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): merge partial and overall data"
  for tag in partial overall; do
     for x in utt2spk ivector.scp; do
        xfile=$iextractor/${data_name}_$tag/$x
        [ -f $xfile ] || { echo "## ERROR ($0): $xfile expected"; exit 1; }
     done
  done
  cat $iextractor/${data_name}_{partial,overall}/utt2spk | \
  perl -ane 'chomp; m/(\S+)\s+(\S+)/g or next; printf ("%s %s-%04d\n", $1, $2, 0);'  > $tmpdir/utt2spk || exit 1
  cat $iextractor/${data_name}_{partial,overall}/ivector.scp >$tmpdir/ivector.scp
  
fi

if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): estimate LDA"
  source/egs/ivector-for-sa/compute-ivector-lda.sh $tmpdir || exit 1
  cp $tmpdir/ivector-lda-transform.mat $iextractor/${data_name}_overall/  || exit 1
  echo "## LOG (step04, $0): LDA estimate done ($tmpdir is to be removed, check $iextractor/${data_name}_overall)"
fi
