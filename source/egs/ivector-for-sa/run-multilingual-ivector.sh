#!/bin/bash 

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
cmd=run.pl
nj=17
# end options

. parse_options.sh || exit 1


function Usage {
 cat<<EOF

 Usage: $(basename $0) [options] <source_data_dir> <tgtdir>
 [options]:
 [examples]:

 $0 --steps 1 /home2/hhx502/kws2016/mling30-dnn/data-resource \
 /home2/hhx502/ivector-mling30

EOF

}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

source_data_dir=$1
tgtdir=$2

tgtdata=$tgtdir/data


if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

script=$(basename $0)
mling_train=$source_data_dir/combined-tr0.1
mling_cv=$source_data_dir/combined-cv0.1
train=$tgtdata/train
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $script): merge data"
  utils/combine_data.sh $train $mling_train $mling_cv
  utils/fix_data_dir.sh $train
  rm $train/{feats.scp,cmvn.scp}
  echo "## LOG (step01, $script): merging data done ('$train') @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02, $script): train ivector extractor @ `date`"
  echo "## LOG (step02, $script): done @ `date`"
fi
