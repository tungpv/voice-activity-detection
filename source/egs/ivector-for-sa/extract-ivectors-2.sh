#!/bin/bash

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo 

# begin options
steps=
cmd=slurm.pl
nj=40
splice_opts="--overall-merge=false"
ivector_id="partial-splice"
# end options
function Usage {
cat<<END

 Usage: $0 [options] <data> <data-name> <ivector-extractor-dir>
 [options]:
 [steps]:
 [examples]:

  $0 --steps 1,2 --ivector-id partial-splice  --splice-opts "$splice_opts"  kws2016/vllp-grapheme/data/dev dev  \
  /home2/hhx502/extractor_mfcc_2048_GenderMix 
 
END
}
. parse_options.sh || exit 1

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

data=$1
data_name=$2
ivector_srcdir=$3

[ -d $tgtdir ] || mkdir -p $tgtdir

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

speaker_data=$data/../$ivector_id
speaker_feat=$data/../feat/$ivector_id
trap "echo '## LOG ($0): removing speaker_data $speaker_data ...'; rm -rf $speaker_data $speaker_feat" EXIT
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): make speaker data @ `date`"
  source/egs/swahili/splice-feats-by-speaker.sh --cmd "$cmd" --splice-opts "$splice_opts" \
  --logdir $data/mfcc_speaker/log \
  $data $speaker_data $speaker_feat || exit 1
  echo "## LOG (step01, $0): done @ `date`"
fi
ivector_data=$ivector_srcdir/${data_name}_${ivector_id}
if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): ivector extraction @ `date`"
  rm -rf $ivector_data/* 2>/dev/null
  [ -d $ivector_data ] || mkdir -p $ivector_data
  cp $speaker_data/utt2spk  $ivector_data/
  source/egs/ivector-for-sa/extract_ivectors.sh --cmd "$cmd" --nj $nj \
  $ivector_srcdir $speaker_data  $ivector_data || exit 1
  tgtdir=$data/$ivector_id
  [ -d $tgtdir ] || mkdir $tgtdir
  cp $ivector_data/{utt2spk,ivector.scp} $tgtdir/  || { echo "## ERROR (step04, $0): ivector.scp expected from $ivector_data"; exit 1; }
  echo "## LOG (step02, $0): done @ `date`"
fi
