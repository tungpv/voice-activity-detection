#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=run.pl
nj=20
steps=
prepare_vllp=false
stmfile=
prepare_tuning=false
flp_prepare_train=false
flp_prepare_train_opts="babel_data babel_lex trainlist datadir"
prepare_script=false
prepare_script_opts="babel_data trainlist datadir"
prepare_untrans=false
prepare_untrans_opts="sdata list data"
prep_unscripted=false
prep_unscripted_opts="babel_audio_dir data"
prep_G=false
prep_G_opts="train_text dev_text datadir"

# end options

echo
echo "$0 $@"
echo

. parse_options.sh

function PrintOptions {
  cmdName=$(echo $0| perl -pe 's/^.*\///g;')
  cat <<END

$cmdName [options]:
cmd					# value, "$cmd"
nj					# value, "$nj"
steps					# value, "$steps"
prepare_vllp				# value, $prepare_vllp
stmfile					# value, "$stmfile"
prepare_tuning				# value, $prepare_tuning
flp_prepare_train			# value, $flp_prepare_train
flp_prepare_train_opts			# value, "$flp_prepare_train_opts"
prepare_script				# value, $prepare_script
prepare_script_opts			# value, "$prepare_script_opts"
prepare_untrans				# value, $prepare_untrans
prepare_untrans_opts			# value, "$prepare_untrans_opts"
prep_unscripted				# value, $prep_unscripted
prep_unscripted_opts			# value, "$prep_unscripted_opts"
prep_G					# value, $prep_G
prep_G_opts				# value, "$prep_G_opts"

END
}

PrintOptions;
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

data=grapheme_vllp/data
featdir=grapheme_vllp/feat/vllp
if $prepare_vllp; then
  echo "$0: prepare_vllp started @ `date`"
  sdata=/data/users/kws15/IARPA-babel202b-v1.0d-build/BABEL_OP2_202/conversational/vllp-train
  dev_data_dir=/data/users/kws15/IARPA-babel202b-v1.0d-build/BABEL_OP2_202/conversational/dev
  grapheme_lexicon_file=$(ls $sdata/transcription/*.txt | source/babel_make_raw_trans.pl  | source/babel_print_word_list.pl  | sort -u | egrep -v '[<\(-]' | source/babel_split_word.pl | sort -u >$sdata/grapheme_lexicon.txt; ls $sdata/grapheme_lexicon.txt)
  lang=conf/lang/107-vietnamese-limitedLP.official.conf
  exp=grapheme_vllp/exp
  ilocal/vllp_prepare_babel_data.sh --cmd "$cmd" --user-lexicon-file $grapheme_lexicon_file \
  --flp false ${stmfile:+--stmfile $stmfile} --featdir $featdir  --dev-data-dir $dev_data_dir \
  $lang $sdata $data $exp

  echo "$0: prepare_vllp ended @ `date`"
fi
if $prepare_tuning; then
  echo "$0: prepare_tuning started @ `date`"
  tuning_data_dir=/data/users/kws15/IARPA-babel202b-v1.0d-build/BABEL_OP2_202/conversational/tuning
  tuning_list=$tuning_data_dir/tuning.list
  stmfile=$tuning_data_dir/tuning.stm
  if [ ! -f $data/raw_tuning_data/prepare_done ]; then
    source/local/make_corpus_subset.sh "$tuning_data_dir" "$tuning_list" \
    $data/raw_tuning_data || exit 1
    touch $data/raw_tuning_data/prepare_done
  fi
  mkdir -p $data/tuning
  source/local/prepare_acoustic_training_data.pl \
  --fragmentMarkers \-\*\~ \
  `pwd`/$data/raw_tuning_data $data/tuning > $data/tuning/skipped_utts.log || exit 1
  local/vllp_augment_original_stm.pl $stmfile $data/tuning
  for x in tuning; do
    sdata1=$data/$x; data1=$sdata1/plp_pitch 
    [ ! -z $featdir ] && feat=$featdir/$x/plp_pitch
    if [ ! -f $data1/plp_pitch_done ]; then
      [ -d $data1 ] || mkdir -p $data1;     cp $sdata1/* $data1/
      utils/fix_data_dir.sh $data1
      echo "$0: make plp feature for $data1"
      steps/make_plp_pitch.sh --cmd "$cmd" --nj $nj $data1 $feat/log  $feat/data || exit 1
      utils/fix_data_dir.sh $data1
      steps/compute_cmvn_stats.sh $data1 $feat/log $feat/data || exit 1
      utils/fix_data_dir.sh $data1
      touch $data1/plp_pitch_done
    fi
  done

  echo "$0: prepare_tuning ended @ `date`"
fi
# flp_prepare_train_opts="babel_data babel_lex  trainlist datadir"
if $flp_prepare_train; then
  optNames="babel_data babel_lex trainlist datadir"
  . source/register_options.sh "$optNames" "$flp_prepare_train_opts" || exit 1
  if [ ! -z $step01 ]; then
    [ -d $datadir ] || mkdir -p $datadir
    local/make_corpus_subset.sh "$babel_data" "$trainlist" $datadir/raw_train_data
  fi
  if [ ! -z $step02 ]; then
    lexiconFlags="--oov <unk>"
    [ -d $datadir/local ] || mkdir -p $datadir/local
    local/make_lexicon_subset.sh $babel_data/transcription $babel_lex $datadir/local/filtered_lexicon.txt
    local/prepare_lexicon.pl ${romanized:+--romanized}  --phonemap "$phoneme_mapping" \
    $lexiconFlags $datadir/local/filtered_lexicon.txt $datadir/local
  fi
  if [ ! -z $step03 ]; then
    oovSymbol="<unk>"
    utils/prepare_lang.sh --share-silence-phones true \
    $datadir/local $oovSymbol $datadir/local/tmp.lang $datadir/lang
  fi
  if [ ! -z $step04 ]; then
    mkdir -p $datadir/train
    local/prepare_acoustic_training_data.pl \
    --vocab $datadir/local/lexicon.txt --fragmentMarkers \-\*\~ \
    `pwd`/$datadir/raw_train_data  $datadir/train > $datadir/train/skipped_utts.log
  fi
fi
if $prepare_script; then
  optNames="babel_data trainlist datadir"
  . source/register_options.sh "$optNames" "$prepare_script_opts" || exit 1
  if [ ! -z $step01 ]; then
    [ -d $datadir ] || mkdir -p $datadir
    local/make_corpus_subset.sh "$babel_data" "$trainlist" $datadir/raw_script_data
  fi
  if [ ! -z $step02 ]; then
    mkdir -p $datadir/script
    local/prepare_acoustic_training_data.pl \
    --vocab $datadir/local/lexicon.txt --fragmentMarkers \-\*\~ \
    `pwd`/$datadir/raw_script_data  $datadir/script > $datadir/script/skipped_utts.log
  fi
fi
if $prepare_untrans; then
  optNames="sdata ulist data"
  . source/register_options.sh "$optNames" "$prepare_untrans_opts" || exit 1
 [ -d $data ] || mkdir -p $data
 if [ ! -z $step01 ]; then
   cat $sdata/segments  | \
   source/egs/kws_filter_seg.pl $ulist | sort -u > $data/uttlist
   utils/subset_data_dir.sh --utt-list $data/uttlist $sdata  $data 
   fix_data_dir.sh $data
 fi
fi
if $prep_unscripted; then
  optNames="sdir data"
  . source/register_options.sh "$optNames" "$prep_unscripted_opts" || exit 1;
  [ -d $data ] || mkdir -p $data
  if [ ! -z $step01 ]; then
    ls $sdir/*.sph | sort -u | \
    perl -e '$sph="/opt/kaldi_updated/trunk/tools/sph2pipe_v2.5/sph2pipe -f wav -p -c 1"; 
      while(<STDIN>) { chomp; $lab = $_; $lab =~ s/.*\///g; $lab =~ s/\.sph//g; print "$lab $sph $_|\n"; }'| sort -u >$data/wav.scp
  fi
  if [ ! -z $step02 ]; then
    wav-to-duration scp:$data/wav.scp ark,t:$data/wav.dur.txt
  fi
  if [ ! -z $step03 ]; then
    cat $data/wav.dur.txt | \
    perl -pe 'chomp; @A=split(" "); $s=$A[0]; $s =~ s/BABEL_OP2_202_//g; $s =~ s/_scripted//g; $_="$s $A[0] 0 $A[1]\n";' > $data/segments
  fi
  if [ ! -z $step04 ]; then
    cat $data/segments |\
    awk '{print $1;}' | \
    perl -pe 'chomp; m/([^_]+)_/; $s="$1_A"; $_="$_ $s\n";' > $data/utt2spk  
    utils/utt2spk_to_spk2utt.pl < $data/utt2spk > $data/spk2utt
    fix_data_dir.sh $data
  fi
fi
if $prep_G; then
   optNames="train_text dev_text datadir"
  . source/register_options.sh "$optNames" "$prep_G_opts" || exit 1
  if [ ! -z $step01 ]; then
    local/train_lms_srilm.sh --dev-text $dev_text \
    --train-text $train_text $datadir $datadir/srilm 
  fi
  if [ ! -z $step02 ]; then
    local/arpa2G.sh $datadir/srilm/lm.gz $datadir/lang $datadir/lang
  fi
fi
