#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
lang_code=man
language=mandarin
lang_code_csl=man,can,jan,kor,rus,viet,indo
ali_csl=mandarin/exp/tri4a/ali_train,cantonese/exp/tri4a/ali_train,japanese/exp/tri4a/ali_train,korean/exp/tri4a/ali_train,russian/exp/tri4a/ali_train,vietnamese/exp/tri4a/ali_train,indonesian/exp/tri4a/ali_train
data_csl=mandarin/data/train/fbank-pitch,cantonese/data/train/fbank-pitch,japanese/data/train/fbank-pitch,korean/data/train/fbank-pitch,russian/data/train/fbank-pitch,vietnamese/data/train/fbank-pitch,indonesian/data/train/fbank-pitch
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <data> <ali_dir>  <tgtdir>
 [options]:
 --steps                              # value, "$steps"
 --lang-code                          # value, "$lang_code"
 --language                           # value, "$language"
 --lang-code-csl                      # value, "$lang_code_csl"
 --data-csl                           # value, "$data_csl"
 --ali-csl                            # value, "$ali_csl"
 [steps]:
 [examples]:

 $0 --steps 1,2,3,4 --lang-code man --language mandarin  $language/data/train/fbank-pitch \
 $language/exp/tri4a/ali_train multilingual-bnfe-621

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi
data=$1
ali_dir=$2
tgtdir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi 

lang_code_csl=$(perl -e '($lang, $lang_csl) = @ARGV; $lang_csl =~ s/$lang//g; $lang_csl =~ s/,,/,/g; $lang_csl =~ s/^,//g; $lang_csl =~ s/,$//g; print $lang_csl ' $lang_code $lang_code_csl)
echo "## LOG: lang_code_csl=$lang_code_csl"
ali_csl=$(perl -e '($lang, $lang_csl) = @ARGV; $lang_csl =~ s/$lang//g; $lang_csl =~ s/,,/,/g; $lang_csl =~ s/^,//g; $lang_csl =~ s/,$//g; print $lang_csl ' $ali_dir $ali_csl)
echo "## LOG: ali_csl=$ali_csl"
data_csl=$(perl -e '($lang, $lang_csl) = @ARGV; $lang_csl =~ s/$lang//g; $lang_csl =~ s/,,/,/g; $lang_csl =~ s/^,//g; $lang_csl =~ s/,$//g; print $lang_csl ' $data $data_csl)
echo "## LOG: data_csl=$data_csl"

bnfe_dir=$tgtdir/without-${language}-bnfe
source_nnet_dir=$bnfe_dir/bnf-extractor-part2-sec 
lang=$language/data/lang
if [ ! -z $step01 ]; then
  echo "## LOG: step01, multilingual bnfe training @ `date`"
  source/egs/kws2016/run-multilingual-bnfe.sh --steps 1,2,3,4,5,6,7,8,9,10,11 \
  --first-learn-rate 0.001 --second-learn-rate 0.001 --first-iter-num 1 \
  --first-train-tool-opts "--minibatch-size=1024 --randomizer-size=32768 --randomizer-seed=777" \
 --second-train-tool-opts "--minibatch-size=2048 --randomizer-size=32768 --randomizer-seed=777" \
 $lang_code_csl 1.0,1.0,1.0,1.0,1.0,1.0 $ali_csl $data_csl  $bnfe_dir
  echo "## LOG: step01, done with multilingual bnfe training @ `date`"
fi
source_bnfe_dir=$tgtdir/${language}-cl-bnfe
if [ ! -z $step02 ]; then
  echo "## LOG: step02, transfer learning @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2,3,4 --nj 20 \
  --graphdir $language/exp/tri4a/graph --devdata $language/data/dev/fbank-pitch \
  --decodename decode_dev $data $lang $source_nnet_dir $ali_dir  $source_bnfe_dir || exit 1
  echo "## LOG: step02, transfer learning done @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "## LOG: step03, making monolingual bnf @ `date`"
  for x in train dev; do
    xdata=$language/data/$x
    sdata=$xdata/fbank-pitch 
    data=$xdata/multilingual-bnf-621 
    feat=$xdata/feat/multilingual-bnf-621
    nj=$(wc -l < $sdata/spk2utt)
    source/egs/swahili/make_feats.sh --cmd run.pl --nj $nj --make-bnf true \
    --bnf-extractor-dir $source_bnfe_dir    $sdata $data $feat || exit 1
  done
  echo "## LOG: step03, done @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## LOG: step04, monolingual-bnf-gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd run.pl --nj 14 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 2000 --pdf-num 30000 \
  --devdata $language/data/dev/multilingual-bnf-621 \
  --decode-opts "--acwt 0.05 --skip-scoring false" \
  $language/data/train/multilingual-bnf-621 \
  $lang  $tgtdir/${language}-multilingual-bnf || exit 1
  echo "## LOG: step04, done @ `date`"

fi
