#!/bin/bash

lang=russian
nohup source/egs/swahili/run-sequential-nnet.sh --steps 1,2,3,4 --cmd "slurm.pl --exclude=node05" --train-cmd run.pl  --devdata /home2/hhx502/ss-apsipa2016/$lang/data/dev/fbank-pitch --graphdir /home2/hhx502/ss-apsipa2016/$lang/exp/tri4a/graph /home2/hhx502/ss-apsipa2016/$lang/data/train/fbank-pitch /home2/hhx502/ss-apsipa2016/$lang/data/lang /home2/hhx502/ss-apsipa2016/multilingual-dnn/${lang}-cl-exp /home2/hhx502/ss-apsipa2016/multilingual-dnn/${lang}-cl-exp-smbr >/home2/hhx502/ss-apsipa2016/multilingual-dnn/${lang}-cl-exp-smbr.log 2>&1 &
