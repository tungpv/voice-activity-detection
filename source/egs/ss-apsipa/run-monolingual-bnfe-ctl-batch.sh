#!/bin/bash

language=$1;
source_languages=$(echo $language | perl -e ' $tlang=<STDIN>;   @A=@ARGV; for($i=0; $i < @A; $i++){if($A[$i] ne $tlang) {print "$A[$i] ";} } '  mandarin cantonese japanese korean russian vietnamese indonesian )
for source_language in $source_languages ; do
  if [ "$language" != "$source_language" ]; then
    echo "$source_language $language"
    source/egs/ss-apsipa/run-monolingual-bnfe-ctl.sh --steps 2,3,4 $source_language $language
  fi
done
