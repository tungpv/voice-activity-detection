#!/bin/bash

echo 
echo "$0 $@"
echo

# begin options
speaker_number=10
speaker_list=
tgtdata2=

# end options

. utils/parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <sdata> <tgtdata>
 [options]
 --speaker-number                      # value, $speaker_number
 --speaker-list                        # value, "$speaker_list"
 --tgtdata2                            # value, "$tgtdata2"
END
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

sdata=$1
data=$2
[ -d $data ] || mkdir -p $data
if [ -z $speaker_list ]; then
  [ -f $sdata/spk2utt ] || { echo "## ERROR: spk2utt expected from sdata $sdata"; exit 1; }
  cat $sdata/spk2utt | \
  perl -ane 'chomp; m:(\S+)\s+(.*):; print $1, "\n";' | \
  perl -e 'use List::Util qw/shuffle/;  ($spkNum) = @ARGV; $num = 0; @A = shuffle <STDIN>;
    while($num ++ < $spkNum) { print $A[$num]; }
  ' $speaker_number > $data/spklist
  speaker_list=$data/spklist
fi

utils/subset_data_dir.sh --spk-list $speaker_list  $sdata $data || exit 1
utils/fix_data_dir.sh $data
if [ -f $data/segments ]; then
  echo "## LOG: data selected: "
  source/data-len.sh  $data
fi

if [ ! -z $tgtdata2 ]; then
  echo "## LOG: making tgtdata2"
  [ -d $tgtdata2 ] || mkdir -p $tgtdata2
  cat $sdata/spk2utt | \
  perl -ane 'chomp; m:(\S+)\s+(.*):; print $1, "\n";' | \
  perl -e '($spklist) = @ARGV; open(F, "$spklist") or die "## ERROR: spklist $spklist cannot open\n";
    while(<F>) {chomp; $vocab{$_} ++; } close F;
    while(<STDIN>) {chomp; if(not exists $vocab{$_}) { print "$_\n"; }  }' $speaker_list > $tgtdata2/spklist
  utils/subset_data_dir.sh --spk-list $tgtdata2/spklist  $sdata $tgtdata2 || exit 1
  echo "## LOG: making tgtdata2 ($tgtdata2) done"
fi
