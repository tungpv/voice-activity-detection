#!/bin/bash
echo 
echo "$0 $@"
echo 

. path.sh
. cmd.sh

# begin options
cmd=run.pl
nj=20
steps=
third_party_data=
nnet_forward_opts="--no-softmax=true --prior-scale=1.0"
language=can
dataname=dev
# end options
language_name=cantonese
function Usage {
 cat<<END
 Usage: $0 [options] <lang-code-csl>  <vad-data-dir> <lre-nnet-dir> <tgtdir> 
 [options]:
 --cmd                                # value, "$cmd"
 --nj                                 # value, $nj
 --steps                              # value, "$steps"
 --third-party-data                   # value, "$third_party_data"
 --language                           # value, "$language"
 --dataname                           # value, "$dataname"
 [steps]:
 [examples]:

 $0 --steps 1  man,can,jan,kor,rus,viet,indo  --nnet-forward-opts "$nnet_forward_opts" \
 $language_name/phone/data/${language_name}-vad/$dataname/vad-${dataname} \
 lre-exp/dnn lre-exp/dnn/$language_name

 $0 --steps 1,2 --third-party-data japanese/data/dev/fbank-pitch-nospk \
 lang_code_csl_dummy vad_data_dummy lre-exp/dnn lre-exp/dnn/japanese-novad

END
}
. parse_options.sh || exit 1

if [ $# -ne 4 ]; then
  Usage && exit 1;
fi

lang_code_csl=$1
vad_data=$2
lre_nnet_dir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

expdir=$tgtdir/$language
[ -d $expdir ] || mkdir -p $expdir

data=$expdir/$dataname
[ ! -z $third_party_data ] && step01=
if [ ! -z $step01 ]; then
  echo "## LOG: step01, fix cantonese data @ `date`"
  [ -d $data ] || mkdir -p $data
  if [ "$language" == can ]; then
    cat $vad_data/segments | \
    perl -e ' ($segFile, $utt2spkFile) = @ARGV;
      open (SEG, ">$segFile") or die "## ERROR: segFile $segFile cannot open\n";
      open(U, ">$utt2spkFile") or die "## ERROR: utt2spkFile $utt2spkFile cannot open\n";
      while(<STDIN>) {chomp; @A = split(/\s+/);  
      $segId = $A[0]; $segId =~ m/(\S+)\-(\S+)_(\S+)/ || die "## ERROR: bad line for segId $_\n";
      $segId = sprintf("%s-%03d_%02d", $1, $2, $3); $spk = sprintf("%s-%03d", $1, $2);
      print U "$segId $spk\n";
      $fileId = $A[1];  $fileId =~ m/(\S+)-(\S+)/ or die "## ERROR: bad line for fileId $_\n";
      $fileId = sprintf("%s-%03d", $1, $2);
      print SEG "$segId $fileId $A[2] $A[3]\n";
    } close SEG; close U; ' $data/segments $data/utt2spk
    utils/utt2spk_to_spk2utt.pl < $data/utt2spk > $data/spk2utt
    cat $vad_data/wav.scp | \
    perl -ane 'chomp; m/(\S+)\s+(.*)/ || die "## ERROR: bad line in wav.scp\n";   $specifier = $2;
             $1 =~ m/(\S+)-(\S+)/ or die "## ERROR: bad fileId $1\n"; $fileId = sprintf("%s-%03d", $1, $2);
      print "$fileId $specifier\n"; ' > $data/wav.scp
  else
    cp $vad_data/* $data
  fi
  utils/fix_data_dir.sh $data
  utils/validate_data_dir.sh --no-text --no-feats $data  || exit 1
  sdata=$data; xdata=$data/fbank-pitch feat=$xdata/feat/fbank-pitch
  source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
   $sdata $xdata $feat || exit 1

  echo "## LOG: step01, done  @ `date`"
fi
# data=$data/fbank-pitch
# [ ! -z $third_party_data ] && data=$third_party_data
decode_dir=$tgtdir/decode-${dataname}
sdata=$data/split$nj;
[[ -d $sdata && $data/feats.scp -ot $sdata ]] || split_data.sh $data $nj || exit 1;
trap "rm $decode_dir/utt2utt 2>/dev/null" EXIT 
if [ ! -z $step02 ]; then
  echo "## LOG: step02, decode && score @ `date`"
  [ -d $decode_dir ] || mkdir -p $decode_dir
  echo $nj > $decode_dir/num_jobs
  nnet=$lre_nnet_dir/final.nnet
  feature_transform=$lre_nnet_dir/final.feature_transform
  lang2index=$lre_nnet_dir/lang2index
  for f in $sdata/1/feats.scp $nnet $feature_transform $spk2index; do
    [ -f $f ] || { echo "## ERROR: $0, missing file $f"; exit 1; }
  done
  # import config,
  cmvn_opts=
  delta_opts=
  D=$lre_nnet_dir
  [ -e $D/norm_vars ] && cmvn_opts="--norm-means=true --norm-vars=$(cat $D/norm_vars)" # Bwd-compatibility,
  [ -e $D/cmvn_opts ] && cmvn_opts=$(cat $D/cmvn_opts)
  [ -e $D/delta_order ] && delta_opts="--delta-order=$(cat $D/delta_order)" # Bwd-compatibility,
  [ -e $D/delta_opts ] && delta_opts=$(cat $D/delta_opts)
  
  # Create the feature stream,
  feats="ark,s,cs:copy-feats scp:$data/feats.scp ark:- |"

  # apply-cmvn (optional),
  [ ! -z "$cmvn_opts" -a ! -f $sdata/1/cmvn.scp ] && echo "$0: Missing $sdata/1/cmvn.scp" && exit 1
  [ ! -z "$cmvn_opts" ] && feats="$feats apply-cmvn $cmvn_opts --utt2spk=ark:$data/utt2spk scp:$data/cmvn.scp ark:- ark:- |"
  # add-deltas (optional),
  [ ! -z "$delta_opts" ] && feats="$feats add-deltas $delta_opts ark:- ark:- |"
  cat $data/utt2spk | \
  awk '{printf("%s %s\n", $1, $1);}' > $decode_dir/utt2utt
  $cmd $decode_dir/log/decode.log \
  nnet-forward $nnet_forward_opts --feature-transform=$feature_transform --use-gpu=yes "$nnet" "$feats" ark,t:-  \| \
 source/code2/map-utterance-to-speaker ark:$decode_dir/utt2utt  ark:- ark,t:$decode_dir/score.txt || exit 1
  echo "## LOG: step02, decode && score done ($decode_dir) @ `date`"
fi
