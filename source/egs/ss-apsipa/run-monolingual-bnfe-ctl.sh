#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <source_language> <target_language>
 [options]:
 --steps                              # value, "$steps"
 [steps]:
 [examples]:

 $0 --steps 2,3,4  cantonese mandarin

END
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

source_language=$1
language=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi 

tgtdir=$language/exp
source_nnet_dir=monolingual-bnfe/${source_language}-exp/h2
source_bnfe_dir=$tgtdir/${source_language}-cl-bnfe
lang=$language/data/lang
ali_dir=$tgtdir/tri4a/ali_train
if [ ! -z $step02 ]; then
  echo "## LOG: step02, transfer learning @ `date`"
  if [ ! -f $source_bnfe_dir/final.nnet ] ;then
    source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2,3,4 --nj 20 \
    --graphdir $language/exp/tri4a/graph --devdata $language/data/dev/fbank-pitch \
    --decodename decode_dev $language/data/train/fbank-pitch  $lang $source_nnet_dir $ali_dir  $source_bnfe_dir || exit 1
  fi
  echo "## LOG: step02, transfer learning done @ `date`"
fi
train_data=$language/data/train/${source_language}-bnf
dev_data=$language/data/dev/${source_language}-bnf
if [ ! -z $step03 ]; then
  echo "## LOG: step03, making monolingual bnf @ `date`"
  for x in train dev; do
    xdata=$language/data/$x
    sdata=$xdata/fbank-pitch 
    data=$xdata/${source_language}-bnf 
    feat=$xdata/feat/${source_language}-bnf
    nj=$(wc -l < $sdata/spk2utt)
    source/egs/swahili/make_feats.sh --cmd run.pl --nj $nj --make-bnf true \
    --bnf-extractor-dir $source_bnfe_dir    $sdata $data $feat || exit 1
  done
  echo "## LOG: step03, done @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## LOG: step04, monolingual-bnf-gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd run.pl --nj 14 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 2000 --pdf-num 30000 \
  --devdata $dev_data \
  --decode-opts "--acwt 0.05 --skip-scoring false" \
  $train_data \
  $lang  $tgtdir/${source_language}-bnf || exit 1
  echo "## LOG: step04, done @ `date`"

fi
