#!/bin/bash

. path.sh
. cmd.sh

echo 
echo "$0 $@"
echo

# begin options
text=
nospk=false
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <sdata> <tdata>
 [options]:
 --text                     # value, "$text"
 --nospk                    # value, $nospk

END
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

sdata=$1
tdata=$2

[ -d $tdata ] || mkdir -p $tdata

cp $sdata/* $tdata/
if $nospk; then
  echo "## LOG: remove speaker info from data @ `date`"
  cat $sdata/utt2spk | \
  awk '{printf ("%s %s\n", $1, $1);}' > $tdata/utt2spk
  cp $tdata/utt2spk $tdata/spk2utt
  if [ -f $tdata/cmvn.scp ] && [ -f $tdata/feats.scp ]; then
    rm $tdata/cmvn.scp
    feat=$tdata/feat-cmvn
    steps/compute_cmvn_stats.sh $tdata $feat/log $feat || exit 1
  fi
  echo "## LOG: done with speaker info removal @ `date`" 
fi
if [ ! -z $text ]; then
  echo "## LOG: new text provided, and overwriting started @ `date`"
  cp $text $tdata/text
fi
utils/fix_data_dir.sh $tdata


