#!/bin/bash

. path.sh
. cmd.sh 

echo
echo "$0 $@"
echo

# begin options
steps=
cmd=run.pl
nj=20
phone_graphdir=
dataname=train
lmwt=10
frame_shift=0.01
# end options

. parse_options.sh || exit 1

function Usage {
cat<<END
 Usage: $0 [options] <data> <phone-lang>  <phone-tokenizer-dir> <tgtdir>
 [options]:
 --steps                               # value, "$steps"
 --cmd                                 # value, "$cmd"
 --nj                                  # value, $nj
 --phone-graphdir                      # value, "$phone_graphdir"
 --dataname                            # value, "$dataname"
 --lmwt                                # value, $lmwt
 --frame-shift                          # value, $frame_shift

 [steps]:
 1: copy data remove speaker information (spk2utt is reversed from utt2spk)
 2: decode the copied data with phone tokenizer
 3: make ctm file from decoded lattices
 4: make new data with ctm files, removing those non-content words 
 [examples]:
 $0  --steps 1,2 --lmwt 5 --phone-graphdir cantonese/phone/exp/tri4a/graph --dataname dev \
 cantonese/phone/data/dev/fbank-pitch cantonese/phone/data/lang \
 cantonese/phone/exp/nnet5a-multilingual-tl cantonese/phone/data/cantonese-vad/dev

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

data=$1
lang=$2
sdir=$3
tgtdir=$4

[ -d $tgtdir ] || mkdir -p $tgtdir
tdata=$tgtdir/data
if [ ! -z $step01 ]; then
  echo "## LOG: step01, copy data and remove speaker info @ `date`"
  source/egs/ss-apsipa/copy-data.sh --nospk true $data $tdata || exit 1  
  echo "## LOG: step01, done @ `date`"
fi
latdir=$tgtdir/decode-${dataname}
if [ ! -z $step02 ]; then
  echo "## LOG: step02, decode data @ `date`"
  [ -f $sdir/final.nnet ] || \
  { echo "## ERROR: final.nnet ($sdir/final.nnet)  expected"; exit 1; }
  [ ! -z $phone_graphdir ] || \
  { echo "## ERROR: phone_graphdir not specified"; exit 1; }
  steps/nnet/decode.sh --cmd "$cmd" --nj $nj \
  --beam 7.0 --lattice-beam 6.0 --max-mem 500000000 \
  --skip-scoring true --srcdir $sdir \
  $phone_graphdir $tdata $latdir || exit 1
  echo "## LOG: step02, done @ `date`"
fi

ctmdir=$tgtdir/ctm-${dataname}
if [ ! -z $step03 ]; then
  echo "## LOG: step03, prepare ctm file @ `date`"
  word_boundary_int=$lang/phones/word_boundary.int
  if [ ! -f $word_boundary_int ]; then
     pdir=$lang/phones;
     for x in silence.int nonsilence.int; do
       [ -f $pdir/$x ] || { echo "## ERROR: file $pdir/$x expected"; exit 1; }
     done
     cat $pdir/silence.int $pdir/nonsilence.int | \
     perl -ane 'chomp; print "$_\tsingleton\n";' > $word_boundary_int
  fi
  [ -d $ctmdir ] || mkdir -p $ctmdir
  lattice-1best --lm-scale=$lmwt "ark:gunzip -c $latdir/lat.*.gz|" ark:- | \
  lattice-align-words $word_boundary_int $sdir/final.mdl ark:- ark:- | \
  nbest-to-ctm --frame-shift=$frame_shift ark:- - | \
  utils/int2sym.pl -f 5 $lang/words.txt | \
  awk '{printf ("%s 1\n",$0);}'  > $ctmdir/${dataname}.ctm || exit 1;

  echo "## LOG: step03, done with ctm file preparation ($latdir) @ `date`"
fi
vad_data=$tgtdir/vad-${dataname}
if [ ! -z $step04 ]; then
  echo "## LOG: step04, making vad data @ `date`"
  [ -d $vad_data ] || mkdir -p $vad_data
  source/code/extract-sub-segment-with-conf --conf-thresh=0 --merge-tolerance=0.02 \
  $tdata/segments  "grep -v -E '<' $ctmdir/${dataname}.ctm|"  $vad_data/segments $vad_data/text
  cp $tdata/wav.scp $vad_data/wav.scp
  
  cat $vad_data/segments | \
  perl -e '($utt2spk) = @ARGV; open(F, "<", "$utt2spk") or die "## ERROR: utt2spk $utt2spk cannot open\n";
    while(<F>) {chomp; @A = split(/[\s+]/); if(@A == 2){ $vocab{$A[0]} = $A[1]; } } close F;
    while(<STDIN>) {chomp; @A = split(/[\s+]/); if(@A == 4) { $new_utt = $A[0]; $utt = $new_utt; $utt =~ s/_\d+$//; 
      if(not exists $vocab{$utt}) {die "## ERROR: no see utt $utt\n";}  print "$new_utt $vocab{$utt}\n";
    }}
  ' $tdata/utt2spk  > $vad_data/utt2spk || { echo "## ERROR: failed to generate utt2spk"; exit 1; }
  utils/utt2spk_to_spk2utt.pl < $vad_data/utt2spk > $vad_data/spk2utt
  utils/fix_data_dir.sh $vad_data
  echo "## LOG: step04, making vad data done @ `date`"
fi
