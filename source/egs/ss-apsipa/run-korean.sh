#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
cmd=run.pl
nj=20
steps=
language=korean
# end options

. parse_options.sh

function Usage {
 cat<<END
 $0 [options] <source-data-dir> <tgtdir>
 [options]:
 [steps]:
 1 lowercase data files
 
 [examples]:
 
 $0  --steps 2  /data/users/hhx502/esmc-apsipa16-ss  /home2/hhx502/ss-apsipa2016/korean

END
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

source_data_dir=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if false && [ ! -z $step01 ]; then
  echo "## LOG: step01, lowercase data files @ `date`"
  for xdir in $source_data_dir; do
    for SRC in $(find $xdir -depth); do
      DST=`dirname "${SRC}"`/`basename "${SRC}" | tr '[A-Z]' '[a-z]'`
      if [ "${SRC}" != "${DST}" ]; then
        [ ! -e "${DST}" ] && mv -T "${SRC}" "${DST}" || echo "${SRC} was not renamed"
      fi
    done
  done
  echo "## LOG: step01, done @ `date`"
fi

curdir=$source_data_dir/train/ko-kr-wav
tgtdata=$tgtdir/data
ctgtdir=$tgtdata/local/overall-data
if [ ! -z $step02 ]; then
  echo "## LOG: step02, prepare mandarin data @ `date`"
  [ -d $ctgtdir ] || mkdir -p $ctgtdir
  find $curdir -name "*.wav" | \
  perl -e ' ($wavscp, $utt2spk) = @ARGV;
    open(W, ">", "$wavscp") or die "## ERROR: cannot open wavscp $wavscp\n";
    open(U, ">", "$utt2spk") or die "## ERROR: cannot open utt2spk $utt2spk\n";
    while(<STDIN>) { chomp; @A = split("/"); $I = scalar @A; $spk = $A[$I-3];  $session = $A[$I-2]; $fname = $A[$I-1];
      $chanId = "a"; if($session =~ /1$/g) { $chanId = "b"; } 
      $spk =~ m/([^\d]+)([\d]+)$/ || die "## ERROR: bad speaker $spk\n";
      $spk = sprintf("spk%s-%s", $2, $chanId);  $fname =~ s/\.wav//g; 
      $lab = sprintf("%s-%03d", $spk, $fname);
      print W "$lab $_\n";
      print U "$lab $spk\n";
    }
    close W; close U; ' $ctgtdir/wav.scp $ctgtdir/utt2spk
    utils/utt2spk_to_spk2utt.pl < $ctgtdir/utt2spk > $ctgtdir/spk2utt
  wav-to-duration scp:$ctgtdir/wav.scp ark,t:- | \
  perl -ane 'chomp; @A = split(/\s+/); print "$A[0] $A[0] 0 $A[1]\n";' > $ctgtdir/segments
  echo "## LOG: step02, done @ `date`"
fi
transcript_dir=$source_data_dir/script/ko-kr.script
if [ ! -z $step03 ]; then
  echo "## LOG: step03, prepare transcripts @ `date`"
  find $transcript_dir -name "*.txt" | sort -u | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; $fname = $_; m/([^\/]+)\.txt$/; $speaker = $1; 
    $speaker =~ m/([^\d]+)([\d]+)/; $chId = "a"; if($2 ==1){$chId = "b";}  
    @A = split(/[\/]/); $speaker = $A[@A-2];  $speaker =~ m/([^\d]+)([\d]+)/;
    $spk = sprintf("spk%s-%s", $2, $chId);
    open(T, "<:encoding(utf8)", "$_") or die "## ERROR: file $_ cannot open\n";
    while(<T>) { chomp; s/[\r\n]//g; tr/\x{feff}//d; next if (/^$/); 
      $line = $_; 
      print STDERR "## $fname, SHOW: $_\n"; m/(\S+)\s+(.*)/g or die "## ERROR: bad line $_\n";
      $lab = sprintf("%s-%03d", $spk, $1);
      print "$lab $2\n";
    } close T; 
  ' > $ctgtdir/raw-text
  echo "## LOG: step03, done @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## LOG: step04, filter transcripts @ `date`"
  cat $ctgtdir/raw-text | \
  perl -e 'use utf8; use open qw(:std :utf8);
    ($text, $map) = @ARGV;
    open(T, ">$text") or die "## ERROR: text $text cannot open\n";
    open(M, "$map") or die "## ERROR: map $map cannot open\n";
    while(<STDIN>) {chomp; m:(\S+)\s+(.*)$: or die "ERROR: bad line $_\n";
      $uttName = $1; $words = $2;
      $words =~ s/[[:punct:]]//g;
      $utt = $words;
      
      print T "$uttName $utt\n";
      @B = split(/\s+/, $utt);
      for($j = 0; $j < @B; $j ++) {
        $w = $B[$j];
        if(not exists $vocab{$w}) {
          print M "$w\t$w\n";
          $vocab{$w} ++;
        }
      }
    } close T; close M; ' $ctgtdir/raw-text.1 "| sort >$ctgtdir/char-map.txt"
  echo "## LOG: step04, done @ `date`"
fi
if  [ ! -z $step05 ]; then
  echo "## LOG: step05, make training text @ `date` "
  cat $ctgtdir/raw-text.1 | \
  perl -e '  use utf8; use open qw(:std :utf8);
    ($mapfile) = @ARGV; 
    open(F, "$mapfile") or die "## ERROR: mapfile $mapfile cannot open\n";
    while(<F>) {
      chomp;  m:(\S+)\s+(.*): || die "## ERROR: bad line $_\n";  $vocab{$1} = $2; } close F;
    while(<STDIN>) {  chomp;
      m:(\S+)\s+(.*): or die "## ERROR: bad stdin line $_\n";   $utt = $1; $words = $2;
      @A = split(/\s+/, $words);
      for($i = 0; $i < @A; $i ++) {  $w = $A[$i];
        if(exists $vocab{$w}) { $utt .= " $vocab{$w}";  } else {
          print STDERR "## WARNING: unidentified word $w\n";
          $utt .= " $w";
        }
      }   print "$utt\n";
    }
  ' $ctgtdir/char-map.txt > $ctgtdir/text
  echo "## LOG: step05, done @ `date`"
fi
train_data=$tgtdata/train
dev_data=$tgtdata/dev
if [ ! -z $step06 ]; then
  echo "## LOG: step06, making training and dev data @ `date`"
  source/egs/ss-apsipa/subset-skrdata-dir.sh --speaker-number 4 --tgtdata2 \
  $train_data $ctgtdir $dev_data 
  echo "## LOG: step06, done @ `date`"
fi
sdict=$source_data_dir/lex
ldict=$tgtdata/local/dict
lang=$tgtdata/lang
if [ ! -z $step07 ]; then
  echo "## LOG: prepare lexicon @ `date`"
  [ -d $ldict ] || mkdir -p $ldict
  cat $sdict/ko-kr.lex | \
  perl -e 'use utf8; use open qw(:std :utf8); ($mapFile) = @ARGV;
    while(<STDIN>) {chomp; s:[\r]::g; m:(\S+)\s+(.*)$: or die "## ERROR: bad line $_\n"; 
      $w = $1; $prn = $2; $w =~ s:\(\S*\)::g; # print STDERR "## DEBUG: $c\n";
      $prn =~ s/\s+//g;
      @A = split("", $prn); $prn = join(" ", @A);
      print "$w\t $prn\n";
    }
  ' $syl2phn | sort -u > $ldict/lexicon.1  
  cat $ldict/lexicon.1 |\
  perl -ane 'm:(\S+)\s+(.*)$:g; @A = split(/\s+/, $2); for($i=0;$i<@A; $i++){print "$A[$i]\n";} '  | sort -u > $ldict/nonsilence_phones.txt
  echo -e "ns\n<oov>\nSIL" > $ldict/silence_phones.txt
  echo "SIL" > $ldict/optional_silence.txt
  echo -n > $ldict/extra_questions.txt
  echo -e "<noise>\tns\n<unk>\t<oov>" | cat - $ldict/lexicon.1 > $ldict/lexicon.txt
  utils/validate_dict_dir.pl $ldict
  utils/prepare_lang.sh $ldict "<unk>" $lang/tmp $lang 
  echo "## LOG: step07, done with $ldict/lexicon.1 @ `date`"
fi
if [ ! -z $step08 ]; then
  echo "## LOG: step08, make lm & grammar @ `date`"
  lmtype=3gram-mincount
  source/egs/fisher-english/train-kaldi-lm.sh --lmtype $lmtype  $ldict/lexicon.txt \
  $train_data $dev_data  $tgtdata/local/lm
  arpa_lm_dir=$tgtdata/local/lm/$lmtype
  [ -f $arpa_lm_dir/lm_unpruned.gz ] || \
  { echo "## ERROR:  lm_unpruned.gz expected"; exit 1; }
  source/egs/fisher-english/arpa2G.sh $arpa_lm_dir/lm_unpruned.gz $lang $lang
  echo "## LOG: step08, done @ `date`"
fi

if [ ! -z $step09 ]; then
  echo "## LOG: step09, mfcc_pitch & fbank_pitch @ `date`"
  for x in train dev; do
    sdata=$tgtdata/$x;  data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    utils/fix_data_dir.sh $sdata
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc16k.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
  done
  echo "## LOG: step09, mfcc_pitch & fbank_pitch done @ `date`"
fi
if [ ! -z $step10 ]; then
  echo "## LOG: step10, gmm-hmm training @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd run.pl --nj 14 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 2000 --pdf-num 30000 \
  --devdata $dev_data/mfcc-pitch \
  $train_data/mfcc-pitch $lang $tgtdir/exp || exit 1
  echo "## LOG: step10, done @ `date`"
fi

if [ ! -z $step11 ]; then
  echo "## LOG: step11, dnn-hmm @ `date`"
  source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 --delta-opts "--delta-order=2" --cmvn-opts "--norm-means=true --norm-vars=true" \
  --use-partial-data-to-pretrain false \
  --nnet-pretrain-cmd "/home/hhx502/w2016/steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048"  \
  --nnet-id nnet5a --graphdir $tgtdir/exp/tri4a/graph --devdata $dev_data/fbank-pitch \
  $train_data/fbank-pitch $lang $tgtdir/exp/tri4a/ali_train  $tgtdir/exp || exit 1
  echo "## LOG: step11, done @ `date`"
fi
if [ ! -z $step12 ]; then
  echo "## LOG: step12, transfer @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2,3,4 --nj 20 --graphdir $tgtdir/exp/tri4a/graph \
  --devdata $dev_data/fbank-pitch \
  --decodename decode_dev \
  $train_data/fbank-pitch $lang multilingual-dnn/overall-7-exp/dnn-m7-layers6 \
  $tgtdir/exp/tri4a/ali_train multilingual-dnn/${language}-cl-exp || exit 1
  echo "## LOG: step12, done @ `date`"
fi

if [ ! -z $step13 ]; then
  echo "## LOG: step13, bnfe nnet transfer learning @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2,3,4 --nj 20 --graphdir $tgtdir/exp/tri4a/graph \
  --devdata $dev_data/fbank-pitch \
  --decodename decode_dev \
  $train_data/fbank-pitch $lang multilingual-bnfe/overall-7-exp/bnf-extractor-part2-sec \
  $tgtdir/exp/tri4a/ali_train multilingual-bnfe/${language}-cl-exp || exit 1
  echo "## LOG: step13, done @ `date`"
fi

if [ ! -z $step14 ]; then
  echo "## LOG: step14, transfer @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2,3,4 --nj 20 --graphdir $tgtdir/exp/tri4a/graph \
  --devdata $dev_data/fbank-pitch \
  --decodename decode_dev \
  $train_data/fbank-pitch $lang multilingual-dnn/without-${language}-6-exp/dnn-m6-layers6 \
  $tgtdir/exp/tri4a/ali_train multilingual-dnn/without-${language}-cl-exp || exit 1
  echo "## LOG: step14, done @ `date`"
fi
if [ ! -z $step15 ]; then
  echo "## LOG: step15, making bnf @ `date`"
  for x in train dev; do
    sdata=$tgtdata/$x/fbank-pitch; data=$tgtdata/$x/bnf; feat=$tgtdata/$x/feat/bnf
    nj=$(wc -l < $sdata/spk2utt)
    source/egs/swahili/make_feats.sh --cmd run.pl --nj $nj --make-bnf true \
    --bnf-extractor-dir multilingual-bnfe/${language}-cl-exp \
    $sdata $data $feat || exit 1
  done
  echo "## LOG: step15, done @ `date`"
fi
if [ ! -z $step16 ]; then
  echo "## LOG: step16, bnf-gmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd run.pl --nj 14 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 2000 --pdf-num 30000 \
  --devdata $tgtdata/dev/bnf \
  --decode-opts "--acwt 0.05 --skip-scoring false" \
  $tgtdata/train/bnf $lang multilingual-bnfe/${language}-bnf-exp || exit 1
  echo "## LOG: step16, done @ `date`"
fi

if [ ! -z $step17 ]; then
  echo "## LOG: step17, making monolingual bnf @ `date`"
  for x in train dev; do
    sdata=$tgtdata/$x/fbank-pitch; data=$tgtdata/$x/monolingual-bnf; feat=$tgtdata/$x/feat/monolingual-bnf
    nj=$(wc -l < $sdata/spk2utt)
    source/egs/swahili/make_feats.sh --cmd run.pl --nj $nj --make-bnf true \
    --bnf-extractor-dir monolingual-bnfe/${language}-exp/h2 \
    $sdata $data $feat || exit 1
  done
  echo "## LOG: step17, done @ `date`"
fi
if [ ! -z $step18 ]; then
  echo "## LOG: step18, monolingual-bnf-gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd run.pl --nj 14 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 2000 --pdf-num 30000 \
  --devdata $tgtdata/dev/monolingual-bnf \
  --decode-opts "--acwt 0.05 --skip-scoring false" \
  $tgtdata/train/monolingual-bnf $lang monolingual-bnfe/${language}-bnf-exp || exit 1
  echo "## LOG: step18, done @ `date`"

fi
