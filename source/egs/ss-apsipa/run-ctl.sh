#!/bin/bash

slang=russian; tlang=cantonese; nohup source/egs/swahili/run-cross-train-nnet-v2.sh --cmd run.pl --steps 1,2,3,4 --devdata $tlang/data/dev/fbank-pitch --graphdir $tlang/exp/tri4a/graph  --decodename decode-dev $tlang/data/train/fbank-pitch $tlang/data/lang $slang/exp/nnet5a/dnn $tlang/exp/tri4a/ali_train $tlang/exp/${slang}-ctl > $tlang/exp/${slang}-ctl.log 2>&1 &
