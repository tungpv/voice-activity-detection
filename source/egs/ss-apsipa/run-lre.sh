#!/bin/bash

. path.sh
. cmd .sh

echo 
echo "$0 $@"
echo

# begin options
steps=
dataname=dev
language=mandarin
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options]  <data> <phone-lang> <phone-tokenizer>
 [options]:
 --steps                               # value, "$steps"
 --dataname                            # value, "$dataname"
 --language                            # value, "$language"
 [steps]:
 [examples]:
 $0 --steps 1,2 --language $language  --dataname $dataname  $language/data/dev/fbank-pitch cantonese/phone/data/lang \
 cantonese/phone/exp/nnet5a-multilingual-tl 

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

data=$1
phone_lang=$2
phone_sdir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
vad_data=cantonese/phone/data/${language}-vad/$dataname
if [ ! -z $step01 ]; then
  echo "## LOG: step01, do vad on $language @ `date`"
  source/egs/ss-apsipa/run-vad-with-phone-tokenizer.sh  \
  --steps 1,2,3,4 --lmwt 5 --phone-graphdir cantonese/phone/exp/tri4a/graph --dataname $dataname \
  $data $phone_lang $phone_sdir  $vad_data || exit 1
  echo "## LOG: step02, done "
fi
vad_data=$vad_data/vad-dev
if [ ! -z $step02 ]; then
  echo "## LOG: step02, do lre on $language @ `date`"
  source/egs/ss-apsipa/run-lre-cantonese.sh --steps 1,2 \
  --language $language \
  language_code_csl_dummy $vad_data lre-exp/dnn lre-exp/dnn/$language || exit 1
  echo "## LOG: step02, lre done on $language @ `date`"
fi
