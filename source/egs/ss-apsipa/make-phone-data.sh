#!/bin/bash

echo 
echo "$0 $@"
echo

. path.sh
. cmd.sh

# begin options
steps=
cmd=slurm.pl
nj=40
dataname=train
ali_dir=
data_csl=
phone2word_map_csl=
# end options
. parse_options.sh || exit 1

datadir=cantonese/data
function Usage {
 cat<<END
 Usage: $0 [options]  <data> <word-lang> <source-word-dir> <tgtdir> 
 [options]:
 --steps                               # value, "$steps" 
 --cmd                                 # value, "$cmd"
 --nj                                  # value, $nj
 --dataname                            # value, "$dataname"
 --ali-dir                             # value, "$ali_dir"
 --data-csl                            # value, "$data_csl"
 --phone-map-csl                       # value, "$phone2word_map_csl"

 [steps]:
 1: align data, if ali_dir is not specified
 2: make text data from alignment
 3: make phone data
 [examples]:

 $0 --steps 1 --dataname dev \
  --phone2word-map-csl "SIL,<silence>:ns,<noise>:<oov>,<unk>" \
  --data-csl $datadir/dev/mfcc-pitch,dev/mfcc-pitch:$datadir/dev/fbank-pitch,dev/fbank-pitch \
  cantonese/data/dev/mfcc-pitch cantonese/data/lang \
 cantonese/exp/tri4a cantonese/phone/data

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
sdir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ ! -z $ali_dir ]; then
  step01=
  echo "## LOG: use ali_dir $ali_dir"
else
  ali_dir=$tgtdir/ali-${dataname}
fi
if [ ! -z $step01 ]; then
  echo "## LOG: step01, FA @ `date`"
  max_nj=$(wc -l < $data/spk2utt) 
  effective_nj=$max_nj;  [ $nj -lt $effective_nj ]  && effective_nj=$nj
  steps/align_fmllr.sh --cmd "$cmd" --nj $effective_nj \
  $data $lang $sdir $ali_dir || exit 1
  echo "## LOG: step01, done ($ali_dir) @ `date`"
fi
phone_tmp=$tgtdir/tmp
if [ ! -z $step02 ]; then
  echo "## LOG: step02, ali-to-phone conversion @ `date`"
  [ -d $phone_tmp ] || mkdir -p $phone_tmp
  phone_map=$phone_tmp/phone_map.txt
  [ -f $lang/phones.txt ] || \
  { echo "## ERROR: phones.txt $lang/phones.txt expected"; exit 1; }
  cat $lang/phones.txt | egrep -v '\#' | awk '{print $1;}' | \
  perl -ane 'chomp; $pp = $_;  s/_[SBIE]$//g; print "$pp $_\n";' | \
  sort -u > $phone_map
  ali-to-phones $ali_dir/final.mdl "ark:gzip -cd $ali_dir/ali.*.gz|" ark,t:- | \
  utils/int2sym.pl -f 2- $lang/phones.txt | \
  perl -e '($phone_map) = @ARGV; open(F, "$phone_map") or die "## ERROR: phone_map $phone_map cannot open\n";
    while(<F>) {chomp; m/(\S+)\s+(\S+)/; $vocab{$1} = $2; } close F;
    while(<STDIN>){chomp; m/(\S+)\s+(.*)/g; $utt = $1; $phones= $2; 
      @A = split(/\s+/, $phones);  for($i = 0; $i < @A; $i++){$s = $A[$i]; 
        die "## ERROR: unidentified phone $s\n" if not exists $vocab{$s};
        $utt .= " $vocab{$s}";
      }
      print "$utt\n";
    } 
  ' $phone_map | \
  perl -e ' ($map) = @ARGV; @A = split(/[:]/, $map);
    for($i = 0; $i< @A; $i++) { @B = split(/[,]/,$A[$i]); if(@B != 2){ die "## ERROR: bad phone2word_map $map\n"; } 
      $vocab{$B[0]} = $B[1];  
    } 
    while (<STDIN>) {chomp; m/(\S+)\s+(.*)/; $utt = $1; $words = $2; 
      @A = split(/\s+/, $words);  for($i = 0; $i < @A; $i++) { $phone = $A[$i]; 
      if(exists $vocab{$phone}) { $phone = $vocab{$phone}; } $utt .= " $phone"; }
      print "$utt\n"; }' "$phone2word_map_csl"  > $phone_tmp/${dataname}-text
 
  echo "## LOG: step02, check ($phone_tmp/${dataname}-text), done @ `date`"
fi

if [ ! -z $step03 ]; then
  echo "## LOG: step03, word-to-phone data conversion @ `date`"
  [ -z $data_csl ] && { echo "## step03, data_csl not specified"; exit 1; }
  [ -f $phone_tmp/${dataname}-text ] || { echo "## ERROR: text is not ready in phone_tmp $phone_tmp"; exit 1; }
  for xpair in $(echo $data_csl | tr ':' ' '); do
    A=( $(echo $xpair| tr ',' ' '))
    [ ${#A[@]} -eq 2 ]  || { echo "## ERROR: illegal pair $xpair in $data_csl"; exit 1; }
    sdata=${A[0]}; data=$tgtdir/${A[1]}
    [ -f $sdata/text ] || { echo "## ERROR: text is not read for data $sdata"; exit 1; }
    utils/subset_data_dir.sh --utt-list $phone_tmp/${dataname}-text $sdata $data  
    cp $phone_tmp/${dataname}-text $data/text
    utils/fix_data_dir.sh $data
    echo "## step03, done ($data)"
  done
  echo "## LOG: step03,  done @ `date`"
fi
