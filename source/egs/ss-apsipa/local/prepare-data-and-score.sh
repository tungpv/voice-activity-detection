#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=run.pl
steps=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF
 
 [examples]:

 $0 --steps 1  can /home2/hhx502/ss-apsipa2016/lre-exp/dnn/lang2index \
 /home2/hhx502/ss-apsipa2016/cantonese/data/dev/fbank-pitch \
 /home2/hhx502/ss-apsipa2016/lre-exp/dnn/lre-decode-cantonese-posterior/results.txt
 
 $0 --steps 1  indo /home2/hhx502/ss-apsipa2016/lre-exp/dnn/lang2index \
 /home2/hhx502/ss-apsipa2016/indonesian/data/dev/fbank-pitch \
 /home2/hhx502/ss-apsipa2016/lre-exp/dnn/lre-decode-indonesian-likelihood/results.txt

EOF
}

if [ $# -ne 4 ]; then
  Usage &&  exit 1;  
fi

langid=$1
lang2index=$2
data=$3
resultFile=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  cat $data/utt2spk | \
  source/egs/ss-apsipa/local/prepare-data-and-score/make-utt2lang.pl $langid > $data/utt2lang
  echo "## LOG (step01, $0): done & check '$data/utt2lang'"
fi

if [ ! -z $step02 ]; then
  cat $data/utt2lang | \
  source/egs/ss-apsipa/local/prepare-data-and-score/make-utt2langid.pl $lang2index > $data/utt2langid
  echo "## LOG (step02, $0): done & check '$data/utt2langid'"
fi
tgtdir=$(dirname $4)
if [ ! -z $step03 ]; then
  cat $resultFile | \
  source/egs/ss-apsipa/local/prepare-data-and-score/score.pl $data/utt2langid  > $tgtdir/score.txt
  echo "## LOG (step03, $0): done & check '$tgtdir/score.txt'"
fi
