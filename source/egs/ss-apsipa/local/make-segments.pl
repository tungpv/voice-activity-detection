#!/usr/bin/perl -w
use strict;

while(<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)/g or next;
  my $end = sprintf("%.2f", $2);
  print "$1 $1 0 $end\n";
}
