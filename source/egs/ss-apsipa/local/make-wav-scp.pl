#!/usr/bin/perl -w
use strict;

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  my $label = $_;
  $label =~ s/.*\///g;  $label =~ s/\.wav//g;
  print "$label $_\n";
}
print STDERR "## LOG ($0): stdin ended\n";
