#!/bin/bash

. path.sh
. cmd.sh 

# begin options
steps=
# end options

function Usage {
 cat<<EOF
 
 [examples]:

 $0 --steps 1 /data/users/hhx502/esmc-apsipa16-ss/test/lre_testset  /home2/hhx502/ss-apsipa2016/data/lre-eval

EOF
}

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

srcdata=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z $step01 ]; then
  find $srcdata -name "*.wav" | \
  sort | source/egs/ss-apsipa/local/make-wav-scp.pl > $tgtdir/wav.scp
fi

if [ ! -z $step02 ]; then
  wav-to-duration scp:$tgtdir/wav.scp ark,t:- > $tgtdir/wav-to-dur.txt
fi

if [ ! -z $step03 ]; then
  cat $tgtdir/wav-to-dur.txt | \
  source/egs/ss-apsipa/local/make-segments.pl > $tgtdir/segments
fi

if [ ! -z $step04 ]; then
  cat $tgtdir/segments | \
  awk '{print $1, " ", $1;}' > $tgtdir/utt2spk
  cp $tgtdir/utt2spk $tgtdir/spk2utt
  utils/fix_data_dir.sh $tgtdir
fi

if [ ! -z $step05 ]; then
  sdata=$tgtdir; data=$tgtdir/fbank-pitch; feat=$tgtdir/feat
  source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1  
fi
