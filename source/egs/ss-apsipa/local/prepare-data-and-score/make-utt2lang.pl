#!/usr/bin/perl -w

use strict;
my $numArgs = scalar @ARGV;
if($numArgs != 1) {
  die "\nExample: cat utt2spk | $0 can\n\n";
}
my ($langid) = @ARGV;
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  print "$1 $langid\n";
}
print STDERR "## LOG ($0): stdin ended\n";
