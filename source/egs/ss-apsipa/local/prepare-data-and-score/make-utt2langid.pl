#!/usr/bin/perl -w
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\nExample: cat utt2lang | $0 lang2index > utt2langid\n\n";
}

my ($lang2indexFile) = @ARGV;
# begin sub
sub LoadDict {
  my ($sFile, $vocab) = @_;
  open(F, "$sFile") or die "## ERROR (LoadDict): cannot open $sFile\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(\S+)/g or next;
    $$vocab{$1} = $2;
  }
  close F;
}
# end sub

my %vocab = ();
LoadDict($lang2indexFile, \%vocab);
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)/g or next;
  my ($utt, $lang) = ($1, $2);
  die if(not exists $vocab{$lang});
  print "$utt $vocab{$lang}\n";
}
print STDERR "## LOG ($0): stdin ended\n";
