#!/usr/bin/perl -w
use strict;

my $numArgs = @ARGV;

if ($numArgs != 1) {
  die "\n## Example: cat results.txt | $0 utt2langid > score.txt\n\n";
}
my ($utt2langid) = @ARGV;

# begin sub
sub LoadDict {
  my ($sFile, $vocab) = @_;
  open(F, "$sFile") or die "## ERROR (LoadDict): cannot open $sFile\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(\S+)/ or next;
    $$vocab{$1} = $2;
  }
  close F;
}
# end sub
my %vocab = ();
LoadDict($utt2langid, \%vocab);
print STDERR "## LOG ($0): stdin expected\n";
my $accNum = 0;
my $totalNum = 0;
while(<STDIN>) {
  chomp;
  s/[\[\]]/ /g;
  m/(\S+)\s+(.*)/;
  my ($key, $value) = ($1, $2);
  my @A = split(/\s+/, $value);
  my $maxValue = 0;
  my $maxIndex = 0;
  for(my $i = 0; $i < @A; $i ++) {
    if($A[$i] > $maxValue) {
      $maxValue = $A[$i];
      $maxIndex = $i;
    }
  }
  die if not exists $vocab{$key};
  if($vocab{$key} == $maxIndex) {
    $accNum ++;
  }
  $totalNum ++;
}
print STDERR "## LOG ($0): stdin ended\n";

my $accRate = 0;
if($accNum != 0) {
  $accRate = sprintf("%.2f", $accNum/$totalNum);
}
print "\nAcc=$accRate\n\n";
