#!/bin/bash

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh

# begin options
cmd=run.pl
nj=20
steps=
nnet_forward_opts="--no-softmax=true --prior-scale=1.0"
# end options

function Usage {
 cat<<EOF
 Usage: $0 [options] <data> <srcdir> <tgtdir>
 [examples]:

 $0 --steps 1 --nnet-forward-opts "$nnet_forward_opts" \
 /home2/hhx502/ss-apsipa2016/cantonese/data/dev/fbank-pitch \
 /home2/hhx502/ss-apsipa2016/lre-exp/dnn /home2/hhx502/ss-apsipa2016/lre-exp/dnn/lre-decode-cantonese-likelyhood
 
 $0 --steps 1 --nj 4  --nnet-forward-opts "$nnet_forward_opts" \
 /home2/hhx502/ss-apsipa2016/indonesian/data/dev/fbank-pitch \
 /home2/hhx502/ss-apsipa2016/lre-exp/dnn /home2/hhx502/ss-apsipa2016/lre-exp/dnn/lre-decode-indonesian-likelihood

 $0 --steps 1 --nj 20  --nnet-forward-opts "$nnet_forward_opts" \
 data/lre-eval/fbank-pitch \
 /home2/hhx502/ss-apsipa2016/lre-exp/dnn /home2/hhx502/ss-apsipa2016/lre-exp/dnn/lre-decode-eval

EOF
}

. utils/parse_options.sh || exit 1

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

data=$1
lre_nnet_dir=$2
tgtdir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

sdata=$data/split$nj;
[[ -d $sdata && $data/feats.scp -ot $sdata ]] || split_data.sh $data $nj || exit 1;
trap "rm $tgtdir/utt2utt 2>/dev/null" EXIT 
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): decode @ `date`"
  [ -d $tgtdir ] || mkdir -p $tgtdir
  echo $nj > $tgtdir/num_jobs
  nnet=$lre_nnet_dir/final.nnet
  feature_transform=$lre_nnet_dir/final.feature_transform
  for f in $sdata/1/feats.scp $nnet $feature_transform; do
    [ -f $f ] || { echo "## ERROR: $0, missing file $f"; exit 1; }
  done
  # import config,
  cmvn_opts=
  delta_opts=
  D=$lre_nnet_dir
  [ -e $D/norm_vars ] && cmvn_opts="--norm-means=true --norm-vars=$(cat $D/norm_vars)" # Bwd-compatibility,
  [ -e $D/cmvn_opts ] && cmvn_opts=$(cat $D/cmvn_opts)
  [ -e $D/delta_order ] && delta_opts="--delta-order=$(cat $D/delta_order)" # Bwd-compatibility,
  [ -e $D/delta_opts ] && delta_opts=$(cat $D/delta_opts)
  
  # Create the feature stream,
  feats="ark,s,cs:copy-feats scp:$sdata/JOB/feats.scp ark:- |"

  # apply-cmvn (optional),
  [ ! -z "$cmvn_opts" -a ! -f $sdata/1/cmvn.scp ] && echo "$0: Missing $sdata/1/cmvn.scp" && exit 1
  [ ! -z "$cmvn_opts" ] && feats="$feats apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp ark:- ark:- |"
  # add-deltas (optional),
  [ ! -z "$delta_opts" ] && feats="$feats add-deltas $delta_opts ark:- ark:- |"
  for x in $(seq $nj);do
    cat $sdata/$x/utt2spk | \
    awk '{printf("%s %s\n", $1, $1);}' > $sdata/$x/utt2utt  
  done
    
  $cmd JOB=1:$nj $tgtdir/log/decode.JOB.log \
  nnet-forward $nnet_forward_opts --feature-transform=$feature_transform --use-gpu=yes "$nnet" "$feats" ark,t:-  \| \
 source/code2/map-utterance-to-speaker ark:$sdata/JOB/utt2utt  ark:- ark,t:$tgtdir/results.JOB.txt || exit 1
  for x in $(seq $nj); do
    cat $tgtdir/results.$x.txt
  done > $tgtdir/results.txt
  rm $tgtdir/results.*.txt 2>/dev/null
  echo "## LOG (step01, $0): decode done & check ('$tgtdir') @ `date`"
fi
