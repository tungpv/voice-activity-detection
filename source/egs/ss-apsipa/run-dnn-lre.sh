#!/bin/bash

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh

# begin options
cmd=run.pl
nj=20
steps=
validating_rate=0.1
learn_rate=0.008
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <lang-code-csl>  <data-csl> <ali-csl> <lang-csl> <source-nnet-dir> <tgtdir>
 [options]:
 --steps                            # value, "$steps"
 --validating-rate                  # value, $validating_rate
 --learn-rate                       # value, $learn_rate
 [steps]:
 [examples]:

 $0 --steps 1 man:can:jan:kor:rus:viet:indo  mandarin/data/train/fbank-pitch,\
cantonese/data/train/fbank-pitch,japanese/data/train/fbank-pitch,korean/data/train/fbank-pitch,\
russian/data/train/fbank-pitch,vietnamese/data/train/fbank-pitch,indonesian/data/train/fbank-pitch \
mandarin/exp/tri4a/ali_train,cantonese/exp/tri4a/ali_train,japanese/exp/tri4a/ali_train,\
korean/exp/tri4a/ali_train,russian/exp/tri4a/ali_train,vietnamese/exp/tri4a/ali_train,\
indonesian/exp/tri4a/ali_train \
mandarin/data/lang,cantonese/data/lang,japanese/data/lang,korean/data/lang,\
russian/data/lang,vietnamese/data/lang,indonesian/data/lang \
multilingual-dnn/overall-7-exp/dnn-m7-layers6 lre-exp 

END
}

if [ $# -ne 6 ]; then
  Usage && exit 1
fi

lang_code_csl=$1
data_csl=$2
ali_csl=$3
lang_csl=$4
source_nnet_dir=$5
tgtdir=$6

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

lang_code=($(echo $lang_code_csl | tr ',:' ' '));  num_langs=${#lang_code[@]}
data_dir=($(echo $data_csl | tr ',:' ' ')); num_data=${#data_dir[@]}
ali_dir=($(echo $ali_csl | tr ',:' ' '));  num_alis=${#ali_dir[@]}
lang_dir=($(echo $lang_csl | tr ',:' ' ')); num_klangs=${#lang_dir[@]}

if ! [ $num_langs -eq $num_data ] && \
     [ $num_data -eq $num_alis ] && \
     [ $num_alis -eq $num_klangs ]; then
  echo "## ERROR, non-matching number of 'csl': $num_langs, $num_data, $num_alis, $num_klangs";
  exit 1
fi 

if [ ! -z $step01 ]; then
  echo "## LOG: reorganize data @ `date`"
  for i in $(seq 0 $[num_langs-1]); do
    echo "## LOG: step01, process data ${data_dir[$i]}"
    source/egs/swahili/reco2file_and_channel.sh ${data_dir[$i]}
    /home/hhx502/w2016/steps/get_train_ctm.sh --cmd "$cmd" ${data_dir[$i]} \
    ${lang_dir[$i]} ${ali_dir[$i]} || { echo "## ERROR: get_train_ctm failed for ${data_dir[$i]}"; exit 1; }
    source/egs/kws2016/filter-and-reseg-data.sh  --steps 1 ${data_dir[$i]} \
    ${ali_dir[$i]}/utt_ctm ${data_dir[$i]}-vad || exit 1
  done
  echo "## LOG: step01, done @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "## LOG: step02, fbank-pitch @ `date`"
  for i in $(seq 0 $[num_langs-1]); do
    data=${data_dir[$i]}-vad; feat=$data/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $data $data $feat || exit 1
    featlen="ark:feat-to-len scp:$data/feats.scp ark,t:- |"
  done
  echo "## LOG: step02, done fbank-pitch @ `date`"
fi
tgtdata_dir=$tgtdir/data-resource
train_data=$tgtdata_dir/combined-train
cv_data=$tgtdata_dir/combined-cv$validating_rate
train_label=$tgtdata_dir/train-label
cv_label=$tgtdata_dir/cv-label
if [ ! -z $step03 ]; then
  echo "## LOG: step03, making dnn training sample @ `date`"
  train_x=""
  cv_x=""
  for i in $(seq 0 $[num_langs-1]); do
    code=${lang_code[$i]}
    sdata1=${data_dir[$i]}; sdata=$tgtdata_dir/$code
    utils/copy_data_dir.sh --utt-prefix ${code}_ --spk-prefix ${code}_ $sdata1 $sdata
    cat $sdata/utt2spk | awk -v langId=$i '{printf("%s %d\n", $1, langId);}' > $sdata/utt2lang
    cur_tr=$tgtdata_dir/${code}_train
    cur_cv=$tgtdata_dir/${code}_cv
    source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
    --random true \
    --data2 $cur_tr \
    $sdata  $cur_cv || exit 1
    train_x="$train_x $cur_tr"
    cv_x="$cv_x $cur_cv"
  done
  # Merge the datasets
  utils/combine_data.sh $train_data $train_x
  utils/combine_data.sh $cv_data $cv_x
  # Validate
  utils/validate_data_dir.sh $train_data
  utils/validate_data_dir.sh $cv_data
  cat $train_data/utt2spk | awk '{printf("%s %s\n", $1, $1);}' > $train_data/utt2utt
  cat $cv_data/utt2spk | awk '{printf("%s %s\n", $1, $1);}' > $cv_data/utt2utt
  featlen="ark:feat-to-len scp:$train_data/feats.scp ark,t:- |" # get number of frames for every utterance,
  [ -d $train_label ] || mkdir -p $train_label
  source/code2/make-speaker-label-target ark:$train_data/utt2utt ark:$train_data/utt2lang "$featlen" ark:$train_label/label.ark  || exit 1  
  featlen="ark:feat-to-len scp:$cv_data/feats.scp ark,t:- |"
  [ -d $cv_label ] || mkdir -p $cv_label
  source/code2/make-speaker-label-target ark:$cv_data/utt2utt ark:$cv_data/utt2lang "$featlen" ark:$cv_label/label.ark  || exit 1  
  echo "## LOG: step03, done @ `date`"
fi
tgtnnet_dir=$tgtdir/dnn
if [ ! -z $step04 ]; then
  echo "## LOG: step04, initialize softmax layer @ `date`"
  [ -d $tgtnnet_dir ] || mkdir -p $tgtnnet_dir
  front_nnet="nnet-copy --remove-last-layers=2 $source_nnet_dir/final.nnet -|"
  hid_dim=$(nnet-info "$front_nnet" | grep output-dim |tail -1 | perl -pe 's/.*output-dim//g; m/(\d+)/g; $_=$1;') \
  || { echo "## ERROR: failed to get hid_dim"; exit 1; }
  num_tgt=$num_langs
  [ $num_tgt -gt 0 ] || { echo "## ERROR: number of target is zero"; exit 1; }
  utils/nnet/make_nnet_proto.py $hid_dim $num_tgt 0  $hid_dim >$tgtnnet_dir/softmax.proto || exit 1 
  log=$tgtnnet_dir/softmax-nnet-initialize.log
  nnet-initialize $tgtnnet_dir/softmax.proto $tgtnnet_dir/softmax-nnet.init \
  2>$log || { cat $log; exit 1; }
  nnet_init=$tgtnnet_dir/nnet.init
  nnet-concat "$front_nnet" $tgtnnet_dir/softmax-nnet.init $nnet_init 2> $tgtnnet_dir/nnet-init.log
  echo "## LOG: step04, done @ `date`"
fi
feature_transform=$source_nnet_dir/final.feature_transform
if [ ! -z $step05 ]; then
  echo "## LOG: step05, training @ `date`"
  steps/nnet/train.sh --learn-rate $learn_rate \
  --feature-transform $feature_transform \
  --labels "ark:cat $train_label/label.ark $cv_label/label.ark|" \
  --hid-layers 0 --nnet-init $tgtnnet_dir/nnet.init $train_data $cv_data lang-dummy ali-dummy \
  ali-dummy $tgtnnet_dir || exit 1
  echo "## LOG: step05, done @ `date`"
fi
lang2index=$tgtnnet_dir/lang2index
if [ ! -f $lang2index ]; then
  perl -e '@A = @ARGV; for($i=0; $i<@A; $i++){print "$A[$i]\t$i\n";}' \
  $(echo $lang_code_csl| tr '[,:]' ' ') > $lang2index
fi
