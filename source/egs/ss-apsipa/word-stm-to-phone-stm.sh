#!/bin/bash

echo
echo "## LOG: $0 $@"
echo 

# begin options
from=5
# end options
function Usage {
 cat<<END
 Usage: $0 [options] <dict> <word-stm> <tgtdir> 
 [options]:
 --from                           # value, $from

 [ WARNING: this is inexact, since there might be multiple pronunciations for a word ]

END
}
. parse_options.sh || exit 1

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

dict=$1
word_stm=$2
tgtdir=$3

[ -d $tgtdir ] || mkdir -p $tgtdir

cat $word_stm | \
perl -e '($from, $dict) = @ARGV; open(D, "$dict") or die "## ERROR: dict $dict cannot open\n"; 
  while(<D>) {chomp; @A = split(/\s+/); die "## ERROR: bad line $_\n" if @A < 2; $w = shift @A; $prn = join(" ", @A);
    $vocab{$w} = $prn;
  } close D;
  while(<STDIN>) { chomp; @A = split(/\s+/); $u = $A[0]; for($i = 1; $i < $from; $i ++){ $u .= " $A[$i]"; } 
    for($i=$from; $i < @A; $i ++){ $w = $A[$i]; if(exists $vocab{lc $w}){ $u .= " $vocab{lc $w}"; } else { $u .= " $w";  }  }
    print "$u\n";
  }
' $from $dict | \
perl -e '($from) = @ARGV; while(<STDIN>){chomp; @A = split(/\s+/); 
  $u = $A[0]; for($i = 1; $i < $from; $i++) { $u .= " $A[$i]";   }
  for($i=$from; $i < @A; $i ++) { if($A[$i] =~ /^$/){next;}  if($A[$i] =~ m/[<\(]/g || $A[$i] =~ /IGNORE_TIME_SEGMENT_IN_SCORING/) { $u .= " $A[$i]";} else {$A[$i] =~ s/[[:punct:]]//g; @B = split("", $A[$i]); $s = join(" ", @B); $u .= " $s" ; }  }
  print "$u\n";
}' $from  > $tgtdir/stm
