#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
my $dump_noise_word = 0;
my $transfer_word = "";

GetOptions(
  "dump-noise-word|dump_noise_word" => \$dump_noise_word,
  "transfer_word|transfer-word=s" => \$transfer_word
);

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\n Example: cat transcript.txt | $0 dir\n\n";
}
# begin sub
sub DumpNoiseWord {
  my ($vocab, $words) = @_;
  while($words =~ m/(\{[^\{]+\})/g) {
    $$vocab{$1} ++;
  }
}
sub DumpDotWord {
  my ($vocab, $words) = @_;
  while($words =~ m/(\S\.)/g) {
    $$vocab{$1} ++;
  }
}
sub LoadTransferDict {
  my ($dictFile, $vocab) = @_;
  open(F, "$dictFile") or die "## ERROR (LoadDict, ", __LINE__, "): cannot open $dictFile\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    $$vocab{$1} = $2;
  }
  close F;
}
sub TransferWord {
  my ($vocab, $words) = @_;
  my $utt = "";
  my @A = split(/\s+/, $$words);
  for(my $i = 0; $i < @A; $i ++) {
    my $word = $A[$i];
    if(exists $$vocab{$word}) {
      $word = $$vocab{$word};
    }
    $utt .= " $word";
  }
  $utt =~ s/^ //g;
  $$words = $utt;
}
sub RemoveNoiseWord {
  my ($words) = @_;
  $$words =~ s/\{pause\}//g;
  $$words =~ s/\{whew\}/<s-noise>/g;
  $$words =~ s/\{breath\}/<s-noise>/g;
  $$words =~ s/\{cough\}/<s-noise>/g;
  $$words =~ s/\{chuckle\}/<s-noise>/g;
  $$words =~ s/\{clears throat\}/<s-noise>/g;
  $$words =~ s/\{gasp\}/<s-noise>/g;
  $$words =~ s/\{lipsmack\}/<s-noise>/g;
  $$words =~ s/\{crying\}/<s-noise>/g;
  $$words =~ s/\{clear throat\}/<s-noise>/g;
  $$words =~ s/\{thumping\}/<s-noise>/g;
  $$words =~ s/\{sigh\}/<s-noise>/g;
  $$words =~ s/\{ch\-\}/<s-noise>/g;
  $$words =~ s/\{squeak\}/<s-noise>/g;
  $$words =~ s/\{sign\}/<s-noise>/g;
  $$words =~ s/\{noise\}/<noise>/g;
  $$words =~ s/\{beep\}/<noise>/g;
  $$words =~ s/\{whistling sound\}/<noise>/g;
  $$words =~ s/\{clearing throat\}/<s-noise>/g;
  $$words =~ s/\{whistle\}/<noise>/g;
  $$words =~ s/\{swallow\}/<s-noise>/g;
  $$words =~ s/\{sheep baah\}/<noise>/g;
  $$words =~ s/\{throat clear\}/<s-noise>/g;
  $$words =~ s/\{laugh\}/<s-noise>/g;
  $$words =~ s/\{sniff\}/<s-noise>/g;
  $$words =~ s/\{gulp\}/<s-noise>/g;
  $$words =~ s/ +/ /g;
  $$words =~ s/^ //g;
  $$words =~ s/ $//g;
}
sub IsEmpty {
  my ($s) = @_;
  $s =~ s/[^[:print:]]+//g;
  $s =~ s/^ //g;
  $s =~ s/ $//g;
  return 1 if($s eq "");
  return 0;
}
# end sub
my ($tgtdir) = @ARGV;
print STDERR "## LOG ($0): stdin expected\n";
open(SEG, ">", "$tgtdir/segments") or die "## ERROR ($0): segments cannot open\n";
open(T, ">", "$tgtdir/text") or die "## ERROR ($0): text cannot open\n";
open(U, ">", "$tgtdir/utt2spk") or die "## ERROR ($0): utt2spk cannot open\n";
my %vocab = ();
my %dotWordVocab = ();
my %transferVocab = ();
if($transfer_word ne "") {
  LoadTransferDict($transfer_word, \%transferVocab);
}
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(.*)$/ or next;
  my ($label, $start, $end, $words) = ($1, $2, $3, $4);  
  my $newLabel = sprintf("%s-%05d-%05d", $label, $start*100, $end*100);
 
  my @A = split(/\-/, $label);

  print SEG "$newLabel $A[0] $start $end\n";
  print U "$newLabel $label\n";
  $words =~ s/[#\(\)]//g;
  if($dump_noise_word) {
    DumpNoiseWord(\%vocab, $words);
    DumpDotWord(\%dotWordVocab, $words);
  }
  if($transfer_word) {
    TransferWord(\%transferVocab, \$words);
  }
  # RemoveNoiseWord(\$words);
  $words =~ s/@/ at /g;
  $words =~ s/\{[^\{]+\}/VERBALNOISE/g;
  $words =~ s/\[[^\[]+\]//g;
  $words =~ s/\[\S+//g;
  $words =~ s/\'/SINGLEQUOTE/g;   
  $words =~ s/[[:punct:]]/ /g;
  $words =~ s/VERBALNOISE/<v-noise>/g;
  $words =~ s/SINGLEQUOTE/\'/g;
  next if(IsEmpty($words));
  print T "$newLabel $words\n";
  # die "## LOG ($0): $label\n" if scalar @A != 3;
  
}
print STDERR "## LOG ($0): stdin ended\n";
close SEG;
close T;
close U;
open (F, ">", "$tgtdir/noise-word-list.txt") or
die "## ERROR ($0): cannot open $tgtdir/noise-word-list.txt\n";
foreach my $word (keys %vocab) {
  print F "$word\n";
}
print STDERR "## LOG ($0): check '$tgtdir/noise-word-list.txt'\n";
close F;

open(F, ">", "$tgtdir/dot-word-list.txt") or
die "## ERROR ($0): cannot open $tgtdir/dot-word-list.txt\n";
foreach my $word (keys %dotWordVocab) {
  print F "$word\n";
}
close F;
