#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

# begin sub
sub AddSegInfo {
  my ($href, $s) = @_;
  $s =~ s/<segment//g;
  $s =~ s/>//g;
  my @A = split(/\s+/, $s);
  for(my $i = 0; $i < @A; $i ++) {
    my $pair = $A[$i];
    next if IsEmpty($pair);
    my @B = split(/\=/, $pair);
    print STDERR "## ERROR (AddSegInfo, ", __LINE__, "): pair=$pair ($_)\n" 
    if not defined $B[0];
    $href->{$B[0]} = $B[1];
  }
}
sub IsEmpty {
  my ($s) = @_;
  $s =~ s/[^[:print:]]+//g;
  $s =~ s/^ //g;
  $s =~ s/ $//g;
  return 1 if($s eq "");
  return 0;
}
sub AddTrans {
  my ($turnRef, $trans, $endTime) = @_;
  if(IsEmpty($$trans)) {
    return;
  }
  my @A = split(/\s+/, $$trans);
  if(scalar @A == 1) {
    return;
  }
  # print STDERR "## LOG (AddTrans, ", __LINE__, "): trans=$$trans\n";
  if($$trans =~ m/(\S+)\s+(.*)$/g) {
    my ($startTime, $words) = ($1, $2);
    if($startTime > $endTime) {
      die "## ERROR (AddTrans, ", __LINE__, "): bad time stamp startTime ($startTime), endTime ($endTime)\n";
    }
    if($startTime == $endTime) {
      return;
    }
    $$trans = sprintf("%.2f %.2f %s", $startTime, $endTime, $words);
    # print STDERR "## DEBUG (AddTrans, ", __LINE__, "): trans=$$trans\n";
    if (defined $turnRef->{trans}) {
      my $aref = $turnRef->{trans};
      my $utt = $$trans;
      push(@$aref, \$utt);
      # print Dumper($turnRef);
      $aref = $turnRef->{trans};
      # print "## DEBUG (AddTrans, ", __LINE__, "): trans=$$trans\n";
     } else {
      my @A = ();
      $turnRef->{trans} = [@A];
      my $aref = $turnRef->{trans};
      # print STDERR "## DEBUG (AddTrans, ", __LINE__, "): trans=$$trans\n";
      my $utt = $$trans;
      push(@$aref, \$utt);
      # print Dumper($turnRef);
    }
    return;
  }
  die "## ERROR (AddTrans, ", __LINE__, "): unexpected trans '$$trans', length=", length($$trans), "\n";
}
sub DumpSegment {
  my ($href, $segName) = @_;
  my $speaker = $href->{speaker};
  $speaker =~ s/\-/_/g;  $speaker =~ s/[\"]//g;
  my $utt = sprintf("%s-%s", $segName, $speaker);
  my $aref = $href->{trans};
  for(my $i = 0; $i < scalar @$aref; $i++) {
    my $words = $$aref[$i];
    print "$utt $$words\n";
  }
}
# end sub

while(<STDIN>) {
  chomp;
  my $label = $_; $label =~ s/.*\///g; $label =~ s/\.txt//g;  $label =~ s/_$//g;
  print STDERR "## LOG (main, ", __LINE__, "): $_ \n";
  open(F, "$_") or die "## ERROR ($0): cannot open $_\n";
  my %vocab = ();
  my $words = "";
  my $href = \%vocab;
  while(<F>) {
    chomp;
    $_ = lc $_;
    next if(/<episode/ || /<\/episode/);
    next if(/<section/ || /<\/section/);
    next if(/<background/);
    next if(/<comment/);
    if(/<segment/) {
      AddSegInfo($href, $_);
      $words = $href->{s_time};
      next;
    }
    if(/<sync\s+time=(\S+)\s*>/) {
      my $end = $1;
      AddTrans($href, \$words, $end);
      $words = $end;
      next;
    }
    if(/<\/segment/) {
      if($words ne "") {
        AddTrans($href, \$words, $href->{e_time});
      }
      $words = "";
      if(defined $href->{trans}) {
        DumpSegment($href, $label);
      }
      undef $href->{trans};
      next;
    }
    $words .= " $_";
  }
  close F;
}
