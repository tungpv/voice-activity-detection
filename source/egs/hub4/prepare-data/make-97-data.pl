#!/usr/bin/perl -w

use strict;
use utf8;
use open qw(:std :utf8);
use Data::Dumper;
# begin sub
sub IsEmpty {
  my ($s) = @_;
  $s =~ s/[^[:print:]]+//g;
  $s =~ s/^ //g;
  $s =~ s/ $//g;
  return 1 if($s eq "");
  return 0;
}
sub AddTrans {
  my ($turnRef, $trans, $endTime) = @_;
  if(IsEmpty($$trans)) {
    return;
  }
  # print STDERR "## LOG (AddTrans, ", __LINE__, "): trans=$$trans\n";
  if($$trans =~ m/(\S+)\s+(.*)$/g) {
    my ($startTime, $words) = ($1, $2);
    if($startTime > $endTime) {
      die "## ERROR (AddTrans, ", __LINE__, "): bad time stamp startTime ($startTime), endTime ($endTime)\n";
    }
    if($startTime == $endTime) {
      return;
    }
    $$trans = sprintf("%.2f %.2f %s", $startTime, $endTime, $words);
    # print STDERR "## DEBUG (AddTrans, ", __LINE__, "): trans=$$trans\n";
    if (defined $turnRef->{trans}) {
      my $aref = $turnRef->{trans};
      my $utt = $$trans;
      push(@$aref, \$utt);
      # print Dumper($turnRef);
      $aref = $turnRef->{trans};
      # print "## DEBUG (AddTrans, ", __LINE__, "): trans=$$trans\n";
     } else {
      my @A = ();
      $turnRef->{trans} = [@A];
      my $aref = $turnRef->{trans};
      # print STDERR "## DEBUG (AddTrans, ", __LINE__, "): trans=$$trans\n";
      my $utt = $$trans;
      push(@$aref, \$utt);
      # print Dumper($turnRef);
    }
    return;
  }
  die "## ERROR (AddTrans, ", __LINE__, "): unexpected trans '$$trans', length=", length($$trans), "\n";
}
sub ReadTurn {
  my ($href, $turn, $trans) = @_;
  
  if($turn =~ m/<turn\s+/g) {
    $turn =~ s/<turn//g;
    $turn =~ s/>//g;
    my %vocab = ();
    my $turnRef = $href->{turn} = \%vocab;
    my @A = split(/\s+/, $turn);
    for(my $i = 0; $i < scalar @A; $i ++) {
      next if IsEmpty($A[$i]);
      my @pair = split(/\=/, $A[$i]);
      if (not defined $pair[0]) {
        die "## ERROR: abnormal turn: $turn, A_i=$A[$i]\n";
      }
      $turnRef->{$pair[0]} = $pair[1];
    }
    return;
  }
  if($turn =~ m/<time\s+sec=(\S+)>/g) {
    my $time = $1;
    # print STDERR  "## DEBUG (ReadTurn, ", __LINE__, "): trans=$$trans\n";
    AddTrans($href->{turn}, $trans, $time);
    $$trans = $time;
    return;
  }
  if($turn =~ /^<overlap/) {
    return;
  }
  $$trans .= " $turn";
}
sub DumpTurn {
  my ($turnRef, $segName) = @_;
  my $speaker = $turnRef->{speaker};
  $speaker =~ s/\-/_/g;
  my $utt = sprintf("%s-%s-%s", $speaker, $turnRef->{spkrtype}, $segName);
  my $aref = $turnRef->{trans};
  my $numUtt = scalar @$aref;
  # print STDERR "## DEBUG (DumpTurn, ", __LINE__, "): numUtt=$numUtt\n";
  for(my $i = 0; $i < $numUtt; $i ++) {
    my $words = $$aref[$i];
    print "$utt $$words\n";
  }
}
# end sub
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  open(SGM, "iconv  -t utf8 $_ |") or die "## ERROR ($0): cannot open file $_\n";
  my %vocab = ();
  my $href = \%vocab;
  my $trans = "";
  print STDERR "## DEBUG (main, ", __LINE__, "): $_\n";
  my $segName = $_; $segName =~ s/.*\///g; $segName =~ s/\.sgml//g;  $segName =~ s/\.txt//g;
  while(<SGM>) {
    chomp;
    $_ = lc $_;
    if(/<episode/ || /<\/episode/) {
      next;
    }
    if(/<section/ || /<\/section/) {
      $trans = "";
      next;
    }
    if(/<overlap/ || /<\/overlap/) {
      next;
    }
    if(/<section\s+type=(\S+)\s+starttime=(\S+)\s+endtime=(\S+)>/g) {
      $href->{type} = $1;
      $href->{start} = $2;
      $href->{end} = $3;
      next;
    }
    if(/<\/turn/) {
      if($trans  ne "") {
        AddTrans($href->{turn}, \$trans, $href->{turn}->{endtime});
      }
      $trans = "";
      DumpTurn($href->{turn}, $segName);
      undef $href->{turn};
      next;
    }
    if(/<\/section/) {
     $trans = "";
      next;
    }
    ReadTurn($href, $_, \$trans);
  }
  close SGM;
}
print STDERR "## LOG ($0): stdin ended\n";
