#!/usr/bin/perl -w

use strict;

while(<STDIN>) {
  chomp;
  my $label = $_;
  $label =~ s/.*\///g;  $label =~ s/\.sph//g; $label=~ s/_$//g;
  printf("%s /home2/hhx502/kaldi/tools/sph2pipe_v2.5/sph2pipe -f wav -p -c 1 %s |\n", $label, $_);
}
