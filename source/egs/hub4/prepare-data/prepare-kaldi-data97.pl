#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

# begin sub
sub IsEmpty {
  my ($s) = @_;
  $s =~ s/[^[:print:]]+//g;
  $s =~ s/^ //g;
  $s =~ s/ $//g;
  return 1 if($s eq "");
  return 0;
}
# end sub
my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\n Example: cat transcript.txt | $0 dir\n\n";
}

my ($tgtdir) = @ARGV;
print STDERR "## LOG ($0): stdin expected\n";
open(SEG, ">", "$tgtdir/segments") or die "## ERROR ($0): segments cannot open\n";
open(T, ">", "$tgtdir/text") or die "## ERROR ($0): text cannot open\n";
open(U, ">", "$tgtdir/utt2spk") or die "## ERROR ($0): utt2spk cannot open\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(.*)$/ or next;
  my ($label, $start, $end, $words) = ($1, $2, $3, $4);
  next if ($words =~ /\[.*\]/);
  my $newLabel = sprintf("%s-%05d-%05d", $label, $start*100, $end*100);
 
  my @A = split(/\-/, $label);

  print SEG "$newLabel $A[2] $start $end\n";
  print U "$newLabel $label\n";

  $words =~ s/\{[^{]+\}/VERBNOISE/g;
  $words =~ s/\'/SINGLEQUOTE/g;
  $words =~ s/[[:punct:]]/ /g;
  $words =~ s/VERBNOISE/<v-noise>/g;
  $words =~ s/SINGLEQUOTE/\'/g;
  next if (IsEmpty($words));
  print T "$newLabel $words\n"; 
  die "## LOG ($0): $label\n" if scalar @A != 3;
  
}
print STDERR "## LOG ($0): stdin ended\n";
close SEG;
close T;
close U;
