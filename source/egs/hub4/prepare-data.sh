#!/bin/bash

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh 

# begin options
steps=
# end options

. parse_options.sh || exit 1

function Usage {
cat<<EOF

 [example]:

 $0 --steps 1 /data/users/hhx502/english-corpus/hub4  /home2/hhx502/sge2017/data/hub4

EOF

}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

srcdata=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

tgtdata=$tgtdir/data96
[ -d $tgtdata ] || mkdir -p $tgtdata
if [ ! -z $step01 ]; then
  find $srcdata/1996/csrv-hub4-ldc97s44/h4eng_sp -name "*.sph" > $tgtdata/1996csrv-sph.list
  find $srcdata/1996/hub4-eng-ldc97t22 -name "*.txt" > $tgtdata/1996csrv-sgm.txt
  echo "## LOG (step01, $0): done & checking '$tgtdata' for 1996csrv file list"
fi

if [ ! -z $step02 ]; then
  cat $tgtdata/1996csrv-sph.list | \
  source/egs/hub4/prepare-data/make-1996-wavscp.pl > $tgtdata/wav.scp
  echo "## LOG (step02, $0): check '$tgtdata'"
fi

if [ ! -z $step03 ]; then
  cat $tgtdata/1996csrv-sgm.txt | \
  source/egs/hub4/prepare-data/make-96-data.pl > $tgtdata/trans.txt
  echo "## LOG (step03, $0): check '$tgtdata/trans.txt'"
 
  cat $tgtdata/trans.txt | \
  source/egs/hub4/prepare-data/prepare-kaldi-data96.pl  $tgtdata
 :
  utils/utt2spk_to_spk2utt.pl < $tgtdata/utt2spk > $tgtdata/spk2utt
  utils/fix_data_dir.sh $tgtdata
  utils/validate_data_dir.sh --no-feats $tgtdata
fi
tgtdata=$tgtdir/data97
[ -d $tgtdata ] || mkdir -p $tgtdata
if [ ! -z $step04 ]; then
  find $srcdata/1997/1997-hub4-eng-ldc98s71 -name "*.sph" > $tgtdata/sphlist.txt
  find $srcdata/1997/hub4-eng-ldc98t28/transcrp -name "*.sgml" > $tgtdata/sgmlist.txt
  echo "## LOG (step04, $0): done & check '$tgtdata'"
fi

if [ ! -z $step05 ]; then
  cat $tgtdata/sphlist.txt | \
  source/egs/hub4/prepare-data/make-1996-wavscp.pl > $tgtdata/wav.scp
  echo "## LOG (step05, $0): check '$tgtdata'"
fi

if [ ! -z $step06 ]; then
  cat $tgtdata/sgmlist.txt | \
  source/egs/hub4/prepare-data/make-97-data.pl  | \
  grep -v '\[' | \
  source/egs/hub4/prepare-data/prepare-kaldi-data97.pl  $tgtdata
  utils/utt2spk_to_spk2utt.pl < $tgtdata/utt2spk > $tgtdata/spk2utt
  utils/fix_data_dir.sh $tgtdata
  utils/validate_data_dir.sh --no-feats $tgtdata
fi
