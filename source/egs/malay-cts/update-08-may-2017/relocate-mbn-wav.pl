#!/usr/bin/perl -w
use utf8;
use open qw(:std :utf8);
use strict;

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  if(/eval/) {
    s:/media/lychee_w2/hhx502/magord6/s5b/data/raw_eval24_data/audio:/data/users/hhx502/mbn/eval24/audio:g;
  } elsif(/trn/) {
    s:/media/lychee_w2/hhx502/magord6/s5b/data/raw_train_data/audio:/data/users/hhx502/mbn/train50h/audio:g;
  } else {
    die;
  }
  print;
}
print STDERR "## LOG ($0): stdin ended\n";
