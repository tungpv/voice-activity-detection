#!/bin/bash 

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
cmd='slurm.pl --quiet'
nj=40
# end options


. parse_options.sh || exit 1

function Usage {
  cat <<EOF
  
  [Example]: $0 --cmd "$cmd" --nj $nj  --steps 1 /data/users/hhx502/w2015/mbn/data/train80h ./update-08-may-2017/data

EOF
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi
srcdata=$1
tgtdata=$2

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi


mbndata=$tgtdata/malay-bn-80h
[ -d $dict ] || mkdir -p $dict
if [ ! -z $step01 ]; then
  [ -d $mbndata ] || mkdir -p $mbndata
  cat $srcdata/wav.scp | \
  source/egs/malay-cts/update-08-may-2017/relocate-mbn-wav.pl > $mbndata/wav.scp
  cp $srcdata/{utt2spk,spk2utt} $mbndata/
  wav-to-duration scp:$mbndata/wav.scp ark,t:$mbndata/wav2dur
  echo "## LOG (step01, $0), done with '$mbndata' preparation"
fi
if [ ! -z $step02 ]; then
 cat $mbndata/wav2dur | \
 perl -ane 'chomp; @A = split(/\s+/); print "$A[0] $A[0] 0 $A[1]\n";'  > $mbndata/segments
 cat $srcdata/text | \
 perl -ane 's:<v-noise>:<unk>:g; s:<noise>:<event>:g; print;' > $mbndata/text
 utils/fix_data_dir.sh $mbndata
fi

malay_sweet_pie=$tgtdata/malay-sweet-pie
if [ ! -z $step03 ]; then
  part01=$malay_sweet_pie/p1.0
  utils/data/copy_data_dir.sh $mbndata $part01
  part02=$malay_sweet_pie/p0.9
  utils/data/perturb_data_dir_speed.sh 0.9 $mbndata $part02
  part03=$malay_sweet_pie/p1.1
  utils/data/perturb_data_dir_speed.sh 1.1 $mbndata $part03
  utils/combine_data.sh $malay_sweet_pie/combine $part01 $part02 $part03
  echo "## LOG (step03, $0): check '$train_sweet_pie/combine'"
fi

if [ ! -z $step04 ]; then
  for sdata in $tgtdata/malay-sweet-pie/combine; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch; log=$sdata/log/mfcc-pitch
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step04, $0): done with feature extraction with '$data'"
    data=$sdata/mfcc-hires; feat=$sdata/feat/mfcc-hires; log=$sdata/log/mfcc-hires
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step04, $0): done with feature extraction with '$data'"
  done
fi
merge_pie=$tgtdata/merge-sweet-pie
if [ ! -z $step05 ]; then
  utils/combine_data.sh $merge_pie/mfcc-pitch $malay_sweet_pie/combine/mfcc-pitch \
  $tgtdata/train-sweet-pie/combine/mfcc-pitch
  utils/combine_data.sh $merge_pie/mfcc-hires $malay_sweet_pie/combine/mfcc-hires \
  $tgtdata/train-sweet-pie/combine/mfcc-hires
fi
expdir=$tgtdata/../exp-merge
lang=$tgtdata/lang
if [ ! -z $step06 ]; then
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1-7 \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 7000 --pdf-num 140000 \
  --devdata $tgtdata/test/mfcc-pitch \
  $merge_pie/mfcc-pitch $lang $expdir  || exit 1
  echo "## LOG (step06, $0): done with '$expdir'"
fi
srcdir=$expdir/tri4a
latdir=$srcdir/ali_lattice-train
alidir=$srcdir/ali_train
if [ ! -z $step07 ]; then
  nj=$(cat $alidir/num_jobs) || exit 1;
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $merge_pie/mfcc-pitch \
  $lang $srcdir  $latdir || exit 1
  rm $latdir/fsts.*.gz # save space
  echo "## LOG (step07, $0): done & check '$latdir' @ `date`"
fi
min_utt_len=5.0
if [ ! -z $step08 ]; then
  for sdata in $merge_pie/{mfcc-pitch,mfcc-hires}; do
    data=$merge_pie/subset-to-train-ivector/$(basename $sdata)
    [ -d $data ] || mkdir -p $data
    cat $sdata/segments | \
    awk -v thresh=$min_utt_len '{ if($4-$3 >= thresh){print $1 ;} }' > $data/utt-list
    utils/subset_data_dir.sh --utt-list $data/utt-list $sdata $data
    echo "## LOG (step08, $0): subset data '$data' generated"
  done
fi
data_subset=$merge_pie/subset-to-train-ivector/mfcc-pitch
alidir_subset=$srcdir/ali-subset-to-train-ivector-extractor
if [ ! -z $step09 ]; then
  steps/align_fmllr.sh --nj $nj --cmd "$cmd" \
  $data_subset  $lang $srcdir   $alidir_subset || exit 1
  echo "## LOG (step09, $0): done with doing alignment @ `date`"
fi

data=$merge_pie/mfcc-hires
nnetdir=$expdir/tdnn
transform_dir=$nnetdir/lda-mllt-transform-for-ivector-extractor
if [ ! -z $step10 ]; then
  steps/train_lda_mllt.sh --cmd "$cmd"  --num-iters 13 \
  --realign-iters "" \
  --splice-opts "--left-context=3 --right-context=3" \
  5000 80000 $data  $lang \
  $alidir $transform_dir || exit 1
  echo "## LOG (step10, $0): done with '$transform_dir'"
fi
nnetdir=$expdir/tdnn
if [ ! -z $step11 ]; then
  steps/online/nnet2/train_diag_ubm.sh --cmd "$cmd" --nj $nj \
  --num-threads 2 \
  --num-frames 720000 $data  512  $transform_dir $nnetdir/diag_ubm || exit 1
  echo "## LOG (step11, $0): done with '$nnetdir/diag_ubm'"
fi

data=$merge_pie/subset-to-train-ivector/mfcc-hires
ivector_extractor=$nnetdir/ivector-extractor
if [ ! -z $step12 ]; then
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$cmd" --nj $nj \
  --num-threads 1 --num-processes 1 \
  $data  $nnetdir/diag_ubm $ivector_extractor || exit 1;
  echo "## LOG (step12, $0): done with '$ivector_extractor'"
fi
train_ivectors=$nnetdir/ivectors-merge-pie
if [ ! -z $step13 ]; then
  data=$merge_pie/mfcc-hires
  data2=$merge_pie/mfcc-hires-max2
  steps/online/nnet2/copy_data_dir.sh --utts-per-spk-max 2 $data  $data2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data2 $ivector_extractor $train_ivectors || exit 1
  echo "## LOG (step13, $0): done with ivectors generation with data '$train_ivectors'"
fi
if [ ! -z $step14 ]; then
  for data in $tgtdata/train_dev/mfcc-hires $tgtdata/test/mfcc-hires; do
    data_name=$(basename $(dirname $data))
    data_ivectors=$nnetdir/ivectors-${data_name}
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data $ivector_extractor $data_ivectors || exit 1
  done
  echo "## LOG (step14, $0): done with ivector extraction @ `date`"
fi
