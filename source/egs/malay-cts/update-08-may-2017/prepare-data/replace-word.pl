#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
my $do_unique;
GetOptions('do_unique|do-unique' => \$do_unique);
my %vocab = ();
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  s#!SIL#<silence>#g;
  s#<UNK>#<unk>#g;
  s#\[event\]#<event>#g;
  s#\[filler_ah\]#<filler_ah>#g;
  s#\[filler_er\]#<filler_er>#g;
  s#\[filler_ha\]#<filler_ha>#g;
  s#\[filler_oh\]#<filler_oh>#g;
  s#\[filler_uhhuh\]#<filler_uhhuh>#g;
  if($do_unique) {
    s# +# #g;
    if (exists $vocab{$_}) {
      next;
    } else {
      $vocab{$_} ++;
    }
  }
  print;
}
print STDERR "## LOG ($0): stdin ended\n";
