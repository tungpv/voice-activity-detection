#!/bin/bash


. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
cmd='slurm.pl --quiet'
nj=40
# end options


. parse_options.sh || exit 1

function Usage {
  cat <<EOF
  
  [Example]: $0 --cmd "$cmd" --nj $nj  --steps 1 /data/users/hhx502/malay-read2017 \
             ./update-08-may-2017/data/read-speech

EOF
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi
srcdata=$1
tgtdata=$2

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

ntusrcdata=$srcdata/ntu22k
tgtdir=$tgtdata/ntu-train
[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z $step01 ]; then
  wavlist=$tgtdir/wavlist.txt
  [ -f $wavlist ] || { find $ntusrcdata/audio -name "*.wav" | sort -u > $wavlist;  }
  cat $wavlist | \
  ./source/egs/malay-cts/update-08-may-2017/prepare-malay-read-data/make-ntu-wav-scp.pl $ntusrcdata/transcript  $tgtdir
  utils/utt2spk_to_spk2utt.pl < $tgtdir/utt2spk > $tgtdir/spk2utt
  echo "## LOG (step01, $0): done & check '$tgtdir'"
fi
if [ ! -z $step02 ]; then
  if [ ! -z $tgtdir/wav2dur ]; then
    wav-to-duration scp:$tgtdir/wav.scp ark,t:$tgtdir/wav2dur
  fi
  cat $tgtdir/wav2dur | \
  perl -ane 'chomp; @A = split(/\s+/); print "$A[0] $A[0] 0 $A[1]\n";'  > $tgtdir/segments 
fi
if [ ! -z $step03 ]; then
  [ -f $tgtdir/history-text ] || { cp $tgtdir/text $tgtdir/history-text;  }
  cat $tgtdir/history-text | \
  source/egs/malay-cts/update-08-may-2017/prepare-malay-read-data/make-word-count.pl --vocab-file=./update-08-may-2017/data/dict/lexicon.txt --norm --decompose --from=2 --write-text=$tgtdir/text
  utils/fix_data_dir.sh $tgtdir
  echo "## LOG (step03, $0): done & check '$tgtdir'"
fi
umsrcdata=$srcdata/um22k
tgtdir=$tgtdata/um-train
[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z $step04 ]; then
  wavlist=$tgtdir/wavlist.txt
  [ -f $wavlist ] || { find $umsrcdata/audio -name "*.wav" | sort -u > $wavlist; }
  cat $wavlist | \
  ./source/egs/malay-cts/update-08-may-2017/prepare-malay-read-data/prepare-um-data.pl $umsrcdata/transcript/mass.transcription  $tgtdir 
fi
if [ ! -z $step05 ]; then
  if [ ! -z $tgtdir/wav2dur ]; then
    wav-to-duration scp:$tgtdir/wav.scp ark,t:$tgtdir/wav2dur
  fi
  cat $tgtdir/wav2dur | \
  perl -ane 'chomp; @A = split(/\s+/); print "$A[0] $A[0] 0 $A[1]\n";'  > $tgtdir/segments 
fi
mmusrcdata=$srcdata/mmu
tgtdir=$tgtdata/mmu-train
[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z $step06 ]; then
  wavlist=$tgtdir/wavlist.txt
  [ -f $wavlist ] || { find $mmusrcdata/audio -name "*.wav" | sort -u > $wavlist; }
  cat $wavlist | \
  ./source/egs/malay-cts/update-08-may-2017/prepare-malay-read-data/prepare-mmu-data.pl $mmusrcdata/audio/malay.transcription  $tgtdir
fi

if [ ! -z $step07 ]; then
  if [ ! -z $tgtdir/wav2dur ]; then
    wav-to-duration scp:$tgtdir/wav.scp ark,t:$tgtdir/wav2dur
  fi
  cat $tgtdir/wav2dur | \
  perl -ane 'chomp; @A = split(/\s+/); print "$A[0] $A[0] 0 $A[1]\n";'  > $tgtdir/segments 
fi
merge_dir=$tgtdata/train3
if [ ! -z $step08 ]; then
  utils/combine_data.sh $merge_dir  $tgtdata/{ntu-train,um-train,mmu-train}
  echo "## LOG (step08, $0): done & check '$merge_dir'"
fi
hand_pie=$tgtdata/hand-pie
data=$merge_dir
if [ ! -z $step09 ]; then
  part01=$hand_pie/p1.0
  utils/data/copy_data_dir.sh $data $part01
  part02=$hand_pie/p0.9
  utils/data/perturb_data_dir_speed.sh 0.9 $data $part02
  part03=$hand_pie/p1.1
  utils/data/perturb_data_dir_speed.sh 1.1 $data $part03
  utils/combine_data.sh $hand_pie/combine $part01 $part02 $part03
  echo "## LOG (step09, $0): check '$hand_pie/combine'"
fi

if [ ! -z $step10 ]; then
  for sdata in $tgtdata/hand-pie/combine; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch; log=$sdata/log/mfcc-pitch
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step10, $0): done with feature extraction with '$data'"
    data=$sdata/mfcc-hires; feat=$sdata/feat/mfcc-hires; log=$sdata/log/mfcc-hires
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step10, $0): done with feature extraction with '$data'"
  done
fi
pie01=update-08-may-2017/data/merge-sweet-pie
pie02=$tgtdata/hand-pie/combine
bigpie=update-08-may-2017/data/big-pie
if [ ! -z $step11 ]; then
  utils/combine_data.sh $bigpie/mfcc-pitch $pie01/mfcc-pitch $pie02/mfcc-pitch
  utils/combine_data.sh $bigpie/mfcc-hires $pie01/mfcc-hires $pie02/mfcc-hires
fi
expdir=update-08-may-2017/exp-bigpie
lang=update-08-may-2017/data/lang
if [ ! -z $step12 ]; then
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1-7 \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 9000 --pdf-num 180000 \
  $bigpie/mfcc-pitch $lang $expdir  || exit 1
  echo "## LOG (step12, $0): done with '$expdir'"
fi
srcdir=$expdir/tri4a
latdir=$srcdir/ali_lattice-train
alidir=$srcdir/ali_train
if [ ! -z $step13 ]; then
  nj=$(cat $alidir/num_jobs) || exit 1;
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $bigpie/mfcc-pitch \
  $lang $srcdir  $latdir || exit 1
  rm $latdir/fsts.*.gz # save space
  echo "## LOG (step13, $0): done & check '$latdir' @ `date`"
fi
min_utt_len=5.0
if [ ! -z $step14 ]; then
  for sdata in $bigpie/{mfcc-pitch,mfcc-hires}; do
    data=$bigpie/subset-to-train-ivector/$(basename $sdata)
    [ -d $data ] || mkdir -p $data
    cat $sdata/segments | \
    awk -v thresh=$min_utt_len '{ if($4-$3 >= thresh){print $1 ;} }' > $data/utt-list
    utils/subset_data_dir.sh --utt-list $data/utt-list $sdata $data
    echo "## LOG (step14, $0): subset data '$data' generated"
  done
fi
data_subset=$bigpie/subset-to-train-ivector/mfcc-pitch
alidir_subset=$srcdir/ali-subset-to-train-ivector-extractor
if [ ! -z $step15 ]; then
  steps/align_fmllr.sh --nj $nj --cmd "$cmd" \
  $data_subset  $lang $srcdir   $alidir_subset || exit 1
  echo "## LOG (step15, $0): done with doing alignment @ `date`"
fi

data=$bigpie/mfcc-hires
nnetdir=$expdir/tdnn
transform_dir=$nnetdir/lda-mllt-transform-for-ivector-extractor
if [ ! -z $step16 ]; then
  steps/train_lda_mllt.sh --cmd "$cmd"  --num-iters 13 \
  --realign-iters "" \
  --splice-opts "--left-context=3 --right-context=3" \
  5000 80000 $data  $lang \
  $alidir $transform_dir || exit 1
  echo "## LOG (step16, $0): done with '$transform_dir'"
fi
nnetdir=$expdir/tdnn
if [ ! -z $step17 ]; then
  steps/online/nnet2/train_diag_ubm.sh --cmd "$cmd" --nj $nj \
  --num-threads 2 \
  --num-frames 720000 $data  512  $transform_dir $nnetdir/diag_ubm || exit 1
  echo "## LOG (step17, $0): done with '$nnetdir/diag_ubm'"
fi
data=$bigpie/subset-to-train-ivector/mfcc-hires
ivector_extractor=$nnetdir/ivector-extractor
if [ ! -z $step18 ]; then
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$cmd" --nj $nj \
  --num-threads 1 --num-processes 1 \
  $data  $nnetdir/diag_ubm $ivector_extractor || exit 1;
  echo "## LOG (step18, $0): done with '$ivector_extractor'"
fi
train_ivectors=$nnetdir/ivectors-bigpie
if [ ! -z $step19 ]; then
  data=$bigpie/mfcc-hires
  data2=$bigpie/mfcc-hires-max2
  steps/online/nnet2/copy_data_dir.sh --utts-per-spk-max 2 $data  $data2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data2 $ivector_extractor $train_ivectors || exit 1
  echo "## LOG (step19, $0): done with ivectors generation with data '$train_ivectors'"
fi
if [ ! -z $step20 ]; then
  for data in update-08-may-2017/data/train_dev/mfcc-hires update-08-may-2017/data/test/mfcc-hires; do
    data_name=$(basename $(dirname $data))
    data_ivectors=$nnetdir/ivectors-${data_name}
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data $ivector_extractor $data_ivectors || exit 1
  done
  echo "## LOG (step20, $0): done with ivector extraction @ `date`"
fi
