#!/usr/bin/perl -w

use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
my $help;
my $normalize;
my $dump_special_word;
my $vocab_file = '';
my $dump_oov = 0;
my $decompose_word;
my $from = 1;
my $write_text = '';
GetOptions('help' => \$help,
           'normalize' => \$normalize,
           'dump-special-word|dump_special_word' => \$dump_special_word,
           'decompose_word|decompose-word' => \$decompose_word,
           'vocab-file|vocab_file=s' => \$vocab_file,
           'from=i' => \$from,
           'write-text|write_text=s' => \$write_text) or die;
die if $from < 1;
# begin sub
sub Help {
  die "\nUsage: $0\n",
      "    [--normalize]\n",
      "    [--dump-special-word]\n",
      "    [--vocab-file=<lexicon.txt>]\n",
      "    [--decompose-word]\n",
      "    [--from=<field-index>]\n",
      "    [--write-text=<text>]\n\n";
}
sub RemoveDash {
  my ($str) = @_;
  $$str =~ s: - ::g;
  $$str =~ s: -: :g;
  $$str =~ s:- : :g;
}
sub RemovePuncts {
  my ($str) = @_;
  $$str =~ s:[\.,\`\'\!\?\(\)\*\&\"\:;¥\+\%\/\$\@\[\]]: :g;
  $$str =~ s:[<>=_]: :g;
  RemoveDash($str);
  $$str =~ s:\s+: :g;
}
sub DecomposeWord {
  my ($str) = @_;
  $$str =~ s:-: :g;
}
sub Normalize {
  my ($strPtr) = @_;
  RemovePuncts($strPtr);
}
sub LoadVocab {
  my ($vocab, $vocabFile) = @_;
  open(V, "$vocabFile") or die;
  while(<V>) {
    chomp;
    my @A = split(/\s+/);
    $$vocab{$A[0]} ++;
  }
  close V;
}
sub ReserveField {
  my ($str, $from, $array) = @_;
  my @A = split(/\s+/, $$str);
  @$array = ();
  my $x = 0;
  while($x < $from - 1) {
    push @$array, shift @A;
    $x ++;
  }
  $$str = join(" ", @A);
}
# end sub

my %inVocab = ();
if ($vocab_file ne '') {
  print STDERR "vocab_file=$vocab_file\n";
  LoadVocab(\%inVocab, $vocab_file);
  $dump_oov = 1;
}
if($help) {
  Help();
}
my $Output;
if ($write_text ne '') {
  open($Output, ">$write_text") or die;
}
print STDERR "## LOG ($0): stdin expected\n";
my %vocab = ();
while(<STDIN>) {
  chomp;
  my @B = ();
  if($from > 1) {
    ReserveField(\$_, $from, \@B);
  }
  if($normalize) {
    Normalize(\$_);
  }
  if($decompose_word) {
    DecomposeWord(\$_);
  }
  my @A = split(/\s+/);
  for(my $i = $from-1; $i < @A; $i++) {
    my $word = $A[$i];
    if($word ne '') {
      $vocab{$word} ++;
    }
  }
  if($write_text ne '') {
    my $text = '';
    if (@B != 0) {
      $text = join(" ", @B) . ' ';
    }
    $text .= join(" ", @A);
    print $Output "$text\n";
  }
}
print STDERR "## LOG ($0): stdin ended\n";
if ($write_text ne '') {
  close $Output;
}
open (O, "|sort -k2nr") or die;
foreach my $word (keys%vocab) {
  if($dump_special_word) {
    if($word =~ m:[^a-z]:g) {
      print O "$word\t$vocab{$word}\n";
    }
  }
  if($dump_oov) {
    if(not exists $inVocab{$word}) {
      print O "oov:$word\t$vocab{$word}\n";
    }
  }
}
close O;
