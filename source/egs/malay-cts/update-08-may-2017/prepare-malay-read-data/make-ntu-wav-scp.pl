#!/usr/bin/perl -w
use strict;
use utf8;
use open qw (:std :utf8);
my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  die "\nExample: cat wavlist.txt | $0 transcript-dir tgtdir\n\n";
}
my ($transcriptdir, $tgtdir) = @ARGV;

# begin sub
sub SplitString {
  my ($str) = @_;
  $str =~ m:([^\d]*)(\d+):g;
  # die "str=$str, strstr=$1, strdigit=$2\n";
  return ($1, $2);
}
# end sub

print STDERR "## LOG ($0): stdin expected\n";
open (WAVSCP, "|sort -u > $tgtdir/wav.scp") or die;
open (UTT2SPK, "|sort -u > $tgtdir/utt2spk") or die;
while (<STDIN>) {
  chomp;
  my $wav = $_;
  my @A = split(/\//, $wav);  my $num = scalar @A;
  my $wavfile = $A[$num-1]; $wavfile =~ m:([^\.]+)\.wav:g;
  my $wavbase = $1;
  my ($strstr, $strdigit) = SplitString($wavbase);
  $strstr = '' if not defined $strstr;
  my $wavlabel = sprintf("%s%04d", $strstr, $strdigit);
  my $speaker = $A[$num-2];
  ($strstr, $strdigit) = SplitString($speaker);
  my $speakerlabel = sprintf("ntu22k-%s%04d", $strstr, $strdigit);
  my $uttlabel = sprintf("%s-%s", $speakerlabel, $wavlabel);
  print UTT2SPK "$uttlabel $speakerlabel\n";
  print WAVSCP "$uttlabel /usr/bin/sox $wav -r 16000 -c 1 -b 16 -t wav - downsample |\n";
}
print STDERR "## LOG ($0): stdin ended\n";
close WAVSCP;
close UTT2SPK;

open(TRANS, "find $transcriptdir -name '*.txt'|") or die;
open(OUTTEXT, "| sort -u > $tgtdir/text") or die;
while(<TRANS>) {
  chomp;
  my @A = split(/\//); my $num = scalar @A;
  my $txtfile = $A[$num-1];
  $txtfile =~ m:([^\.]+)\.txt:g;
  my $speakerlabel = sprintf("ntu22k-speaker%04d", $1);
  open(TXT, "$_") or die "## ERROR (", __LINE__, "): failed to open $_\n";
  while(<TXT>) {
    chomp;
    m:(^[\d]+)[\.]*\s+(.*):g or next;
    my ($index, $text) = ($1, lc $2);
    my $textlabel = sprintf("%04d", $index);
    my $uttlabel = sprintf("%s-%s", $speakerlabel, $textlabel);
    print OUTTEXT "$uttlabel $text\n";
  }
  close TXT;
}
close TRANS;
close OUTTEXT;
