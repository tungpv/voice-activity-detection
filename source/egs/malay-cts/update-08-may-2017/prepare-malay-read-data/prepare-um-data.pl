#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
my $numArgs = scalar @ARGV;
if($numArgs != 2) {
  die "\nExample: cat wavlist.txt | $0 transcript.txt tgtdir\n\n";
}
my ($transcript, $tgtdir) = @ARGV;

# begin sub

# end sub

open (WAVSCP, "|sort -u > $tgtdir/wav.scp") or die;
open (UTT2SPK, "|sort -u > $tgtdir/utt2spk") or die;
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\//); my $num = scalar @A;
  my $wavfile = $A[$num - 1];  $wavfile =~ s:\.wav$::g;  $wavfile =~ m:(.*)_(\d+)$:g;
  my $indexId = $2;
  if(not defined $indexId) {
    $wavfile =~ m:(^\d+):g;
    $indexId = $1;
  }
  my $speakerId = $A[$num - 2]; $speakerId =~ s:_::g;
  $speakerId = sprintf("um22k-%s", $speakerId);
  if (not defined $indexId) {
    die "utterance= $_,  indexId = $indexId\n";
  }
  my $uttId = sprintf("%s-%04d", $speakerId, $indexId);
  print UTT2SPK "$uttId $speakerId\n";
  # print STDERR "file=$_, speakerId =$speakerId, uttId = $uttId\n";
  print WAVSCP "$uttId /usr/bin/sox $_ -r 16000 -c 1 -b 16 -t wav - downsample |\n";
}
print STDERR "## LOG ($0): stdin ended\n";
close WAVSCP;
close UTT2SPK;
`utils/utt2spk_to_spk2utt.pl < $tgtdir/utt2spk > $tgtdir/spk2utt`;
open(TRANS, "$transcript") or die "## ERROR ($0): failed ot open $transcript\n";
open(TEXT, "|sort -u > $tgtdir/text") or die;
while(<TRANS>) {
  chomp;
  m:(^<s>)\s+(.*)\s+(<\/s>)\s+\((.*)\)$:g or die "## ERROR ($0): $_\n";
  my ($label, $text) = ($4, lc $2);
  $label =~ m:(.*)_(\d+)$:g;
  my $speakerId = $1; my $indexId = $2;  $speakerId =~ s:_::g;
  $speakerId = sprintf("um22k-%s", $speakerId);
  my $uttId = sprintf("%s-%04d", $speakerId, $indexId);
  print TEXT "$uttId $text\n";
}
close TRANS;
close TEXT;
