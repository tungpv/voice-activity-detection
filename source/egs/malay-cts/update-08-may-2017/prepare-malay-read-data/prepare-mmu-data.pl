#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;
if($numArgs != 2) {
  die "\n Example : cat wavlist.txt | $0 transcript.txt tgtdir\n\n";
}
my ($transcript, $tgtdir) = @ARGV;

# begin sub

# end sub
open (WAVSCP, "|sort -u >$tgtdir/wav.scp") or die;
open (UTT2SPK, "|sort -u > $tgtdir/utt2spk") or die;
my %vocab = ();
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\//); my $num = scalar @A;
  my $indexId = $A[$num -1]; $indexId =~ s:\.wav$::g;
  my $speakerId = $A[$num - 2];
  $speakerId = sprintf("mmu16k-%s", $speakerId);
  my $uttId = sprintf("%s-%04d", $speakerId, $indexId);
  $indexId = sprintf("%04d", $indexId);
  $vocab{$indexId} = $uttId;
  print UTT2SPK "$uttId $speakerId\n";
  print WAVSCP "$uttId $_\n";
}
print STDERR "## LOG ($0): stdin ended\n";
close WAVSCP;
close UTT2SPK;
`utils/utt2spk_to_spk2utt.pl < $tgtdir/utt2spk > $tgtdir/spk2utt`;
open(TRANS, "$transcript") or die "## ERROR ($0): failed ot open $transcript\n";
open(TEXT, "|sort -u > $tgtdir/text") or die;
while(<TRANS>) {
  chomp;
  m:(^<s>)\s+(.*)\s+(<\/s>)\s+\((.*)\)$:g or die "## ERROR ($0): $_\n";
  my ($speakerId, $text) = ($4, $2);
  $speakerId = sprintf("%04d", $speakerId);
  if (not exists $vocab{$speakerId}) {
    print STDERR "## WARNING ($0): no utterance id for '$speakerId'\n"; next;
  }
  my $uttId = $vocab{$speakerId};
  print TEXT "$uttId $text\n";
}
close TRANS;
close TEXT;
