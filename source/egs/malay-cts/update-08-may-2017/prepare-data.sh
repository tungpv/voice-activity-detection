#!/bin/bash 

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
cmd='slurm.pl --quiet'
nj=40
# end options


. parse_options.sh || exit 1

function Usage {
  cat <<EOF
  
  [Example]: $0 --cmd "$cmd" --nj $nj  --steps 1 ./data ./update-08-may-2017/data

EOF
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi
srcdata=$1
tgtdata=$2

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi


dict=$tgtdata/dict
[ -d $dict ] || mkdir -p $dict
if [ ! -z $step01 ]; then
  sdict=$srcdata/local/dict
  cat $sdict/lexicon.txt | \
  source/egs/malay-cts/update-08-may-2017/prepare-data/replace-word.pl | \
  source/egs/malay-cts/update-08-may-2017/prepare-data/normalize-dict.pl | \
  grep -v '<silence>' > $dict/lexicon.txt
  touch $dict/extra_questions.txt 
  cp $sdict/{silence_phones.txt,optional_silence.txt,nonsilence_phones.txt}  $dict/
  utils/validate_dict_dir.pl $dict
  echo "## LOG (step01, $0), done with '$dict' preparation"
fi
lang=$tgtdata/lang
if [ ! -z $step02 ]; then
  utils/prepare_lang.sh $dict '<unk>' $lang/tmp $lang
  echo "## LOG (step02, $0): done '$lang'"
fi
lm=$tgtdata/lm
srclm=$srcdata/local/lm/srilm
if [ ! -z $step03 ]; then
   [ -d $lm ] || mkdir -p $lm
   cat $srclm/{train_google,train_allforumFiller,train_written,train_web,train_allforum,train_forum,allforumFillerText,writtenText,train_trans} | \
  source/egs/malay-cts/update-08-may-2017/prepare-data/replace-word.pl --do-unique  | \
  gzip -c > $lm/train-text.gz
  echo " LOG (step03, $0): training text '$lm/train.gz' preparation done"
fi
if [ ! -z $step04 ]; then
  cat $srcdata/train/text  | \
  cut -d' ' -f2- | gzip -c > $lm/dev-hist-text.gz
  cat $srcdata/train_dev/text  | \
  cut -d' ' -f2- | source/egs/malay-cts/update-08-may-2017/prepare-data/replace-word.pl | \
  gzip -c > $lm/dev-text.gz
  cut -f1  $dict/lexicon.txt|sort -u | grep -v '<silence>' |gzip -c > $lm/vocab.gz
  source/egs/sge2017/local/build-lm/train-srilm-v2.sh --steps 1-3 --lm-order-range "3 3" \
  --cutoff-csl "3,011,012"  $lm
  echo "## LOG (step04, $0): done with lm training in '$lm'"
fi
if [ ! -z $step05 ]; then
  gzip -cd $srclm/Written0.15AllForumFiller0.05AllForum0.05Google0.05Trans.kn.gz | \
  source/egs/malay-cts/update-08-may-2017/prepare-data/replace-word.pl | \
  gzip -c > $lm/lm-history.gz
  echo "## LOG (step05, $0): done with '$lm/lm-history.gz'"
fi
if [ ! -z $step06 ]; then
  source/egs/fisher-english/arpa2G.sh $lm/lm-history.gz $lang $lang
fi
train=$tgtdata/train
if [ ! -z $step10 ]; then
  [ -d $train ] || mkdir -p $train
  cat $srcdata/train/mfcc_pitch/text | \
  source/egs/malay-cts/update-08-may-2017/prepare-data/replace-word.pl > $train/text
  cp $srcdata/train/mfcc_pitch/{spk2utt,utt2spk,segments,wav.scp} $train
  utils/fix_data_dir.sh $train
  echo "## LOG (step10, $0): done with kaldi data '$train'"
fi
train_dev=$tgtdata/train_dev
if [ ! -z $step11 ]; then
  [ -d $train_dev ] || mkdir -p $train_dev
  cat ./data/train_dev/text | \
  source/egs/malay-cts/update-08-may-2017/prepare-data/replace-word.pl > $train_dev/text
  cp ./data/train_dev/{spk2utt,utt2spk,segments,wav.scp} $train_dev
  utils/fix_data_dir.sh $train_dev
  echo "## LOG (step11, $0): done with kaldi data '$train_dev'"
fi
dev_data=$tgtdata/test
if [ ! -z $step12 ]; then
  [ -d $dev_data ] || mkdir -p $dev_data
  cat ./data/test/mfcc_pitch/text | \
  source/egs/malay-cts/update-08-may-2017/prepare-data/replace-word.pl > $dev_data/text
  cat ./data/test/mfcc_pitch/stm | \
  source/egs/malay-cts/update-08-may-2017/prepare-data/replace-word.pl > $dev_data/stm
  cp ./data/test/mfcc_pitch/{glm,spk2utt,utt2spk,segments,wav.scp} $dev_data/
  utils/fix_data_dir.sh $dev_data
  echo "## LOG (step12, $0): done with kaldi data '$dev_data'"  
fi
train_sweet_pie=$tgtdata/train-sweet-pie
if [ ! -z $step13 ]; then
  part01=$train_sweet_pie/p1.0
  utils/data/copy_data_dir.sh $train $part01
  part02=$train_sweet_pie/p0.9
  utils/data/perturb_data_dir_speed.sh 0.9 $train $part02
  part03=$train_sweet_pie/p1.1
  utils/data/perturb_data_dir_speed.sh 1.1 $train $part03
  utils/combine_data.sh $train_sweet_pie/combine $part01 $part02 $part03
  echo "## LOG (step13, $0): check '$train_sweet_pie/combine'"
fi

if [ ! -z $step14 ]; then
  for sdata in $tgtdata/train-sweet-pie/combine $tgtdata/train_dev $tgtdata/test; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch; log=$sdata/log/mfcc-pitch
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step14, $0): done with feature extraction with '$data'"
    data=$sdata/mfcc-hires; feat=$sdata/feat/mfcc-hires; log=$sdata/log/mfcc-hires
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step14, $0): done with feature extraction with '$data'"
  done
fi
expdir=$tgtdata/../exp
if [ ! -z $step15 ]; then
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1-7 \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 5000 --pdf-num 80000 \
  --devdata data/dev-msra/mfcc-pitch \
  $train_sweet_pie/combine/mfcc-pitch $lang $expdir  || exit 1
  echo "## LOG (step15, $0): done with '$expdir/tri4a'"
fi
min_utt_len=5.0
if [ ! -z $step16 ]; then
  for sdata in $train_sweet_pie/combine/{mfcc-pitch,mfcc-hires}; do
    data=$train_sweet_pie/subset-to-train-ivector/$(basename $sdata)
    [ -d $data ] || mkdir -p $data
    cat $sdata/segments | \
    awk -v thresh=$min_utt_len '{ if($4-$3 >= thresh){print $1 ;} }' > $data/utt-list
    utils/subset_data_dir.sh --utt-list $data/utt-list $sdata $data
    echo "## LOG (step16, $0): subset data '$data' generated"
  done
fi
srcdir=$expdir/tri4a
data=$train_sweet_pie/combine/mfcc-pitch
alidir=$srcdir/ali-subset-to-train-ivector-extractor
if [ ! -z $step17 ]; then
  steps/align_fmllr.sh --nj $nj --cmd "$cmd" \
  $data  $lang $srcdir   $alidir || exit 1
  echo "## LOG (step17, $0): done with doing alignment @ `date`"
fi
data=$train_sweet_pie/combine/mfcc-hires
nnetdir=$expdir/tdnn
transform_dir=$nnetdir/lda-mllt-transform-for-ivector-extractor
if [ ! -z $step18 ]; then
  steps/train_lda_mllt.sh --cmd "$cmd"  --num-iters 13 \
  --realign-iters "" \
  --splice-opts "--left-context=3 --right-context=3" \
  5000 80000 $data  $lang \
  $alidir $transform_dir || exit 1
  echo "## LOG (step18, $0): done with '$transform_dir'"
fi

if [ ! -z $step19 ]; then
  steps/online/nnet2/train_diag_ubm.sh --cmd "$cmd" --nj $nj \
  --num-threads 2 \
  --num-frames 720000 $data  512  $transform_dir $nnetdir/diag_ubm || exit 1
  echo "## LOG (step19, $0): done with '$nnetdir/diag_ubm'"
fi
data=$train_sweet_pie/subset-to-train-ivector/mfcc-hires
ivector_extractor=$nnetdir/ivector-extractor
if [ ! -z $step20 ]; then
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$cmd" --nj $nj \
  --num-threads 1 --num-processes 1 \
  $data  $nnetdir/diag_ubm $ivector_extractor || exit 1;
  echo "## LOG (step20, $0): done with '$ivector_extractor'"
fi
train_ivectors=$nnetdir/ivectors-train-sweet-pie
if [ ! -z $step21 ]; then
  data=$train_sweet_pie/combine/mfcc-hires
  data2=$train_sweet_pie/combine/mfcc-hires-max2
  steps/online/nnet2/copy_data_dir.sh --utts-per-spk-max 2 $data  $data2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data2 $ivector_extractor $train_ivectors || exit 1
  echo "## LOG (step21, $0): done with ivectors generation with data '$train_ivectors'"
fi
if [ ! -z $step22 ]; then
  for data in $tgtdata/train_dev/mfcc-hires $tgtdata/test/mfcc-hires; do
    data_name=$(basename $(dirname $data))
    data_ivectors=$nnetdir/ivectors-${data_name}
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data $ivector_extractor $data_ivectors || exit 1
  done
fi
latdir=$srcdir/ali_lattice-train
alidir_overall=$srcdir/ali_train
if [ ! -z $step23 ]; then
  nj=$(cat $alidir_overall/num_jobs) || exit 1;
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $train_sweet_pie/combine/mfcc-pitch \
  $lang $srcdir  $latdir || exit 1
  rm $latdir/fsts.*.gz # save space
  echo "## LOG (step23, $0): done & check '$latdir' @ `date`"
fi
