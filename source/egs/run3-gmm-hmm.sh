#!/bin/bash

. path.sh
. cmd.sh


# begin options
cmd=run.pl
nj=20
dev_select=false
run3_gmm_hmm=false
run1_gmm_hmm=false

# end options

. parse_options.sh

function PrintOptions {
  cat<<END

$0 [options]:
cmd			# value, "$cmd"
nj			# value, $nj
dev_select		# value, $dev_select
run3_gmm_hmm            # value, $run3_gmm_hmm
run1_gmm_hmm            # value, $run1_gmm_hmm

END
}
PrintOptions


if $dev_select; then
  echo "$0: dev_select started @ `date`"
  data=llp2/data/dev/plp_pitch
  factor=0.3
  subdata=$(dirname $data)/plp_pitchf0.3
  [ -d $subdata ] || mkdir -p $subdata
  iutils/mksubset_spklist.pl $factor $data/spk2utt "feat-to-len scp:$data/feats.scp ark,t:- |"  >$subdata/spklist || exit 1
  utils/subset_data_dir.sh --spk-list $subdata/spklist $data $subdata || exit 1
  utils/fix_data_dir.sh $subdata
fi
  lang=llp2/data/lang
  dotest_opts="--dotest-data llp2/data/dev/plp_pitchf0.3 --dotest-lang $lang --dotest-dataname devf0.3" 

 data=llp2/data/train/plp_pitch
if $run3_gmm_hmm; then
  echo "$0: run3_gmm_hmm started @ `date`"
  factor1_opts="--factor1 0.4 --factor1-state 1500 --factor1-pdf 15000"
  factor2_opts="--factor2 0.7 --factor2-state 2100 --factor2-pdf 21000"
  full_opts="--full-state 2500 --full-pdf 36000"
  rdir=llp2/exp.test2.run3_gmm_hmm
  source/test/run3-gmm-hmm.sh --run3-gmm-hmm true --factor1-opts "$factor1_opts" \
  --factor2-opts "$factor2_opts"  --full-opts "$full_opts" \
  --dotest-opts "$dotest_opts" --steps 3,5,6  \
  $data $lang $rdir || exit 1
fi

if $run1_gmm_hmm; then
  echo "$0: run1_gmm_hmm started @ `date`"
  rdir=llp2/exp.test.run1_gmm_hmm
  run1_opts="--full-state 2500 --full-pdf 36000 --dataname train"
  source/test/run3-gmm-hmm.sh --run1-gmm-hmm true --run1-opts "$run1_opts" \
  --dotest-opts "$dotest_opts" \
  $data $lang $rdir || exit 1
fi
