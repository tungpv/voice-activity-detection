#!/bin/bash

if [ $# -ne 1 ]; then
  echo
  echo "## Usage: $0 <tgtdir>"
  echo && exit 1
fi

tgtdir=$1

echo "## LOG ($0): started @ `date`"

for SRC in `find $tgtdir -depth`; do
    DST=`dirname "${SRC}"`/`basename "${SRC}" | tr '[A-Z]' '[a-z]'`
    if [ "${SRC}" != "${DST}" ]
    then
        [ ! -e "${DST}" ] && mv -T "${SRC}" "${DST}" || echo "${SRC} was not renamed"
    fi
  done 

echo "## LOG ($0): done @ `date`"
