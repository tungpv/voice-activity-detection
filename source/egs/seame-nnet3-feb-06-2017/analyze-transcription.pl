#!/usr/bin/perl
use utf8;
use open qw(:std :utf8);
use strict;
use warnings;

# begin sub
sub AccHistStat {
  my ($array, $mRate) = @_;
  my $index;
  if(0<= $mRate && $mRate < 0.1) {
    $index = 0;
  } elsif (0.1 <= $mRate && $mRate < 0.2) {
    $index = 1;
  } elsif (0.2 <= $mRate && $mRate < 0.3) {
    $index = 2;
  } elsif (0.3 <= $mRate && $mRate < 0.4) {
    $index = 3;
  } elsif (0.4 <= $mRate && $mRate < 0.5) {
    $index = 4;
  } elsif (0.5 <= $mRate && $mRate < 0.6) {
    $index = 5;
  } elsif (0.6 <= $mRate && $mRate < 0.7) {
    $index = 6;
  } elsif (0.7 <= $mRate && $mRate < 0.8) {
    $index = 7
  } elsif (0.8 <= $mRate && $mRate < 0.9) {
    $index = 8;
  } elsif (0.9 <= $mRate && $mRate <= 1.0) {
    $index = 9;
  } else {
    die "## ERROR ($0, ", __LINE__, "): illegal rate $mRate\n";
  }
  $$array[$index] ++;
}
#
sub AccSpkStat {
  my ($vocab, $spk, $mRate) = @_;
  if(exists $$vocab{$spk}) {
    my $array = $$vocab{$spk};
    $$array[0] += $mRate;
    $$array[1] ++;
  } else {
    $$vocab{$spk} = [my @A];
    my $array = $$vocab{$spk};
    push @$array, $mRate;
    push @$array, 1;
  }
}
# end sub

my $numArgs = scalar @ARGV;

if($numArgs != 1) {
  die "\n\nExample: $0 data\n\n";
}

my $data = shift @ARGV;

foreach my $filename ("text", "utt2spk") {
  my $file = $data . '/' . $filename;
  die "## ERROR ($0): file $file expected\n" if ( not -e $file );
}
my %vocab_utt2spk = ();
open(F, "$data/utt2spk") or die "## ERROR ($0): $data/utt2spk cannot open\n";
while(<F>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  $vocab_utt2spk{$1} = $2;
}
close F;

open (TEXT, "$data/text") or die "## ERROR ($0): $data/text cannot open\n";
my $tgtdir="$data/transcription-analysis";
`[ -d $tgtdir ] || mkdir -p $tgtdir`;

my $utt_statistics = "$tgtdir/utterance-statistics";
open(US, ">$utt_statistics") or die "## ERROR ($0): failed to open file $utt_statistics\n";
my @UttAccu = ();  # array with 10 elements (0-0.1, 0.1-0.2, ..., 0.9-1) to accumulate utterance statistics,
my %SpkStats = ();

my $totalUtt = 0;

while (<TEXT>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($utt, $words) = ($1, $2);
  my $spk = $vocab_utt2spk{$utt};
  die "## ERROR ($0): unknown utterance $utt\n" if (not defined $spk);
  my @A = split (/\s+/, $words);
  my ($total, $mnum, $enum) = (0, 0 , 0);
  for(my $i = 0; $i < @A; $i ++) {
    my $w = $A[$i];
    next if($w =~ /^</);   # ignore those 'silence' words
    $total ++;
    if($w =~ m/\p{Han}+/) {
      $mnum ++;
    } else {
      $enum ++;
    }
  }
  next if ($total ==0);
  $totalUtt ++;
  my $man_rate = $mnum / $total;
  my $str_man_rate = sprintf("%.2f", $man_rate);
  print US "$utt $str_man_rate\n";
  AccHistStat(\@UttAccu, $man_rate);
  AccSpkStat(\%SpkStats, $spk, $man_rate);
}
close TEXT;
close US;
my $utt_distribution = "$tgtdir/utterance-distribution";
open(UD, "> $utt_distribution") or die "## ERROR ($0): failed to open file $utt_distribution\n";
if($totalUtt != 0) {  # get utterance based distribution, a kind of normalized
  for(my $i = 0; $i < @UttAccu; $i ++) {
    my $num = $UttAccu[$i];
    my $rate = sprintf("%.2f", $num / $totalUtt);
    print UD "$rate\n";
  }
}
close UD;
my $spk_statistics = "$tgtdir/speaker-statistics";
open(SS, "| sort -k2nr >$spk_statistics") or die "## ERROR ($0): failed to open file $spk_statistics\n";
my $totalSpk = 0;
my @SpkAccu = ();
if($totalUtt != 0) {
  foreach my $spk (keys %SpkStats) {
    my $array = $SpkStats{$spk};
    my $rate = sprintf("%.2f", $$array[0] / $$array[1]);
    # print STDERR "## Debug (", __LINE__,"): spk=$spk, total_rate = $$array[0], total_utt= $$array[1]\n";
    print SS "$spk $rate\n";
    AccHistStat(\@SpkAccu, $rate);
    $totalSpk ++;
  }
}
close SS;
my $spk_distribution = "$tgtdir/speaker-distribution";
open(SD, "> $spk_distribution") or die "## ERROR ($0): failed to open file $spk_distribution\n";
if($totalSpk != 0) {
  for(my $i = 0; $i < @SpkAccu; $i ++) {
    my $num = $SpkAccu[$i];
    my $rate = sprintf("%.2f", $num / $totalSpk);
    print SD "$rate\n";
  }
}
close SD;
