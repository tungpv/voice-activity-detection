#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
steps=
cmd=slurm.pl
nj=40
experimental_name=mfcc-hires
# end options

. parse_options.sh || exit 1

function Usage {
  cat<<EOF
 
 [Examples]: $0 --steps 1 --cmd "$cmd" --nj $nj \
 --experimental-name $experimental_name \
 data  exp

EOF
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

srcdata=$1
tgtdir=$2

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

expdir=$tgtdir/$experimental_name
[ -d $expdir ] || mkdir -p $expdir
langdir=data/lang
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): run gmm with kaldi mfcc-hires features @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 2-7 \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 5000 --pdf-num 100000 \
  --devdata $srcdata/dev-sge-dominant/mfcc-hires \
  $srcdata/train/mfcc-hires-cleaned $langdir  $expdir || exit 1  
  echo "## LOG (step01, $0): done @ `date`"  
fi

if [ ! -z $step02 ]; then
  for x in train/mfcc-hires-cleaned dev-sge-dominant/mfcc-pitch dev-man-dominant/mfcc-pitch; do
    sdata=data/$x
    data=data/$(dirname $x)/mfcc-hires-online-pitch
    utils/copy_data_dir.sh $sdata $data
    feat=$data/feat; log=$data/log
    steps/make_mfcc_pitch_online.sh --nj $nj --mfcc-config conf/mfcc_hires.conf \
    --online-pitch-config conf/online_pitch-no-delay.conf \
      --cmd "$cmd" $data $log $feat;
    steps/compute_cmvn_stats.sh $data $log $feat;
    utils/fix_data_dir.sh $data
  done
  echo "## LOG (step02, $0): mfcc-hires-online-pitch extraction done @ `date`"
fi

if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): run gmm with kaldi mfcc-hires-online-pitch features @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1-7 \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 5000 --pdf-num 100000 \
  --devdata $srcdata/dev-sge-dominant/mfcc-hires-online-pitch \
  $srcdata/train/mfcc-hires-online-pitch $langdir  $expdir || exit 1  
  echo "## LOG (step03, $0): done @ `date`"
fi
