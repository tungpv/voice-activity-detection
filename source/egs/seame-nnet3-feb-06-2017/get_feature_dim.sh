#!/bin/bash 

echo
echo "## LOG: $0 $@"
echo

. path.sh


if [ $# -ne 2 ]; then
  echo "Usage: $0 <feature_specifier> <dir>"  
  exit 1
fi

feats=$1
dir=$2

[ -d $dir ] || mkdir -p $dir 
# echo "## DEBUG($0): $-"
# set +o | grep pipefail

# utils/filter_scp.pl --exclude exp-baseline/tdnn/tdnn-d/egs/valid_uttlist data/train-augment-overall/train/mfcc-hires/split6/1/feats.scp | apply-cmvn --norm-means=false --norm-vars=false --utt2spk=ark:data/train-augment-overall/train/mfcc-hires/split6/1/utt2spk scp:data/train-augment-overall/train/mfcc-hires/split6/1/cmvn.scp scp:- ark:$dir/tmp.ark 

trap "rm $dir/tmp.ark;" INT QUIT TERM EXIT

echo "$feats" | \
perl -e '$output=shift @ARGV; while(<STDIN>){chomp; s#^[^:]+:##; s#ark:-$#ark:#; $cmd = $_ . $output; print "cmd=$cmd\n";  system("$cmd");  }' $dir/tmp.ark

feat-to-dim ark:$dir/tmp.ark - > $dir/feat_dim

feat_dim=$(cat $dir/feat_dim)
echo "feat_dim=$feat_dim"
if [ -z $feat_dim ] || [ $feat_dim -le 0 ]; then
  echo "## ERROR ($0): illegal feat_dim ('$feat_dim')"
  exit 1
fi

exit 0
