#!/bin/bash

echo
echo "## LOG: $0 $@"
echo

. path.sh
. cmd.sh

# begin options
steps=
# end options

. parse_options.sh || exit

function Usage {
cat<<EOF

 [Example]: $0 --steps 1 data/train/mfcc-pitch/mfcc-hires  data/train/mfcc-hires-cleaned

EOF
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

srcdata=$1
tgtdata=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $tgtdata ] || mkdir -p $tgtdata

if [ ! -z $step01 ]; then
  cat $srcdata/text | \
  awk '{if(NF==1){print;}}' > $tgtdata/empty-uttlist
  cat $srcdata/text | \
  awk '{if(NF>1){print $1;}}' > $tgtdata/uttlist
  utils/subset_data_dir.sh  --utt-list $tgtdata/uttlist $srcdata  $tgtdata
  utils/fix_data_dir.sh $tgtdata
fi

