#!/usr/bin/perl

use warnings;
use strict;

while (<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  my @B = split(/\-/, $A[0]);
  my $spk = "";
  if($B[1] =~ /^sp/) {
    $spk = $B[0] . '-' . $B[1] . '-' . $B[2];
  } else {
    $spk = $B[0] . '-' . $B[1];
  }
  print "$A[0] $spk\n";
}
