#!/usr/bin/perl

use warnings;
use strict;

while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my $label = "rev-" . $1;
  printf("%s %s\n", $label, $2);
}
