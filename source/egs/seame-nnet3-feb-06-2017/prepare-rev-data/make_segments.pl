#!/usr/bin/perl

use warnings;
use strict;

while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  printf("rev-%s rev-%s %.2f %.2f\n", $A[0], $A[1], $A[2], $A[3]);
}
