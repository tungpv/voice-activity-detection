#!/usr/bin/perl
use warnings;
use strict;

while(<STDIN>) {
  chomp;
  m/.*\/([^\/]+)\.wav/g or next;
  my $lab = "rev-" . $1;
  print "$lab $_\n";
}
