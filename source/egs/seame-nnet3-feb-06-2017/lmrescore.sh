#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd='slurm.pl --quiet'
lmscale=1.0
kaldi_lmscale=10
acoustic_scale=1.0
word_penalty=0
effective_nbest=10000
inter_weight=0.6
nbest=1
steps=1
save_lmlogprob_file=
lmlogprob_file=
# end options

function Usage {
 cat<<EOF
 
 [example]: 
 $0 --steps 1  \
 --lmscale $lmscale \
 --nbest $nbest \
 --effective-nbest $effective_nbest \
 --word-penalty $word_penalty \
 --kaldi-lmscale $kaldi_lmscale \
 --lmscale $lmscale \
 --acoustic-scale  $acoustic_scale \
 --inter-weight $inter_weight \
 /home3/hhx502/w2017/seame-nnet3-feb-06-2017/data/dev-man-dominant/mfcc-hires \
 /home3/hhx502/w2017/seame-nnet3-feb-06-2017/update-may-27-with-mandarin-word/exp/tdnn/lang-chain \
 /home3/hhx502/w2017/seame-nnet3-feb-06-2017/update-may-27-with-mandarin-word/exp/tdnn/chain-may-31/decode-dev-man-dominant \
 /home3/hhx502/w2017/seame-nnet3-feb-06-2017/update-may-27-with-mandarin-word/exp/tdnn/chain-may-31/decode-dev-man-dominant/lmrescore

EOF
}

. parse_options.sh || exit 1

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
latdir=$3
tgtdir=$4


[ -d $tgtdir/log ] || mkdir -p $tgtdir/log

inv_lm_scale=$(perl -e '($kaldi_lmscale) = @ARGV; printf("%.2f", 1/$kaldi_lmscale);' $kaldi_lmscale)
nj=$(cat $latdir/num_jobs) || exit 1
if [ ! -z $step01 ]; then  
  $cmd JOB=1:$nj $tgtdir/log/make-${nbest}.JOB.log \
  lattice-to-nbest --n=$nbest --acoustic-scale=$inv_lm_scale \
  "ark:gzip -cd $latdir/lat.JOB.gz|" ark:- \| \
  nbest-to-linear ark:-   "ark,t:|gzip -c > $tgtdir/ali.$nbest.JOB.gz" \
  "ark,t:| int2sym.pl -f 2- $lang/words.txt | local/wer_hyp_filter|  gzip -c > $tgtdir/tra.$nbest.JOB.gz" \
  "ark,t:|gzip -c >$tgtdir/lmcost.$nbest.JOB.gz" \
  "ark,t:|gzip -c >$tgtdir/amcost.$nbest.JOB.gz"  || exit 1
fi

if [ ! -z $step02 ]; then
  for i in $(seq 1 $nj); do
    gzip -cd $tgtdir/amcost.$nbest.$i.gz 
  done | gzip > $tgtdir/amcost.$nbest.gz  
  for i in $(seq 1 $nj); do
    gzip -cd $tgtdir/tra.$nbest.$i.gz
  done | gzip > $tgtdir/tra.$nbest.gz
  echo "## LOG (step02, $0): done & check '$tgtdir/amcost.$nbest.gz' & '$tgtdir/tra.$nbest.gz'"
fi

if [ ! -z $step03 ]; then
  $cmd JOB=1:$nj $tgtdir/log/lmrescore.JOB.log \
  source/egs/seame-nnet3-feb-06-2017/lmrescore/lmrescore.py \
  --word-penalty=$word_penalty \
  --effective-nbest=$effective_nbest \
  --lmscale=$lmscale \
  --inter-weight=$inter_weight \
  ${save_lmlogprob_file:+--save-lmlogprob-file=$tgtdir/lmlogprob.JOB.txt} \
  ${lmlogprob_file:+--lmlogprob-file=$tgtdir/lmlogprob.txt} \
  --acoustic-scale=$acoustic_scale \
  /home2/zpz505/seame/formatted-seame/trainvocab.txt \
  /home2/zpz505/seame/formatted-seame/train-train-vocab.3g.gz \
  /home2/zpz505/seame/apsipa2017/seame-vocab-and-class-reclass-on-mix-data-exp/class500-count-1-10/clm.3.g.gz \
  /home2/zpz505/seame/apsipa2017/seame-vocab-and-class-reclass-on-mix-data-exp/class500-count-1-10/word.500.class \
  $tgtdir/amcost.$nbest.JOB.gz \
  $tgtdir/tra.$nbest.JOB.gz \
  $tgtdir/best.JOB.txt
fi

ref_filtering_cmd="perl local/wer_ref_filter"
if [ ! -z $step04 ]; then
  for i in $(seq 1 $nj); do
      cat $tgtdir/lmlogprob.$i.txt
  done > $tgtdir/lmlogprob.txt
  for i in $(seq 1 $nj); do
      cat $tgtdir/best.$i.txt
  done > $tgtdir/best.txt
  [ -f $tgtdir/best.txt ] || \
  { echo "## LOG (step04, $0): ERROR '$tgtdir/best.txt' expected"; exit 1;  }
  cat $data/text | $ref_filtering_cmd > $dir/$tgtdir/test_filt.txt
  cat $tgtdir/best.txt | \
  source/egs/mandarin/update-april-03-2017-with-pruned-lexicon/segment-chinese-text.py --do-character-segmentation | \
  compute-wer --text --mode=present \
  ark:$tgtdir/test_filt.txt  ark,t:- >$tgtdir/wer-best-lmrescore.ascale${acoustic_scale}.lmscale${lmscale}_wpen${word_penalty}
  echo "## LOG (step04, $0): wer is"
  cat $tgtdir/wer-best-lmrescore.ascale${acoustic_scale}.lmscale${lmscale}_wpen${word_penalty}
fi

if [ ! -z $step05 ]; then
  $cmd JOB=1:$nj $tgtdir/log/oracle.JOB.log \
  source/egs/seame-nnet3-feb-06-2017/lmrescore/compute-nbest-oracle-wer.py \
  $tgtdir/test_filt.txt $tgtdir/tra.$nbest.JOB.gz $tgtdir/oracle.JOB.txt
  for i in $(seq 1 $nj); do
      cat $tgtdir/oracle.$i.txt
  done | source/egs/seame-nnet3-feb-06-2017/lmrescore/get-oracle-wer.py |tee $tgtdir/oracle.txt
  
fi
