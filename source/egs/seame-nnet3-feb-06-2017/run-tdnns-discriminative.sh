#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd='slurm.pl --quiet'
nj=40
steps=1
online_ivector_dir=
train_data_name=train
max_jobs_for_degs=6
criterion=smbr
one_silence_class=true

## Egs options
frames_per_eg=150
frames_overlap_per_eg=30
truncate_deriv_weights=10

## Nnet training options
effective_learning_rate=0.0000125
max_param_change=1
num_jobs_nnet=4
num_epochs=4
regularization_opts=          # Applicable for providing --xent-regularize and --l2-regularize options 
minibatch_size=64
adjust_priors=true            # May need to be set to false 
                              # because it does not help in some setups
modify_learning_rates=true
last_layer_factor=0.1
train_stage=-3
## Decode options
decode_start_epoch=1 # can be used to avoid decoding all epochs, e.g. if we decided to run more.

# end options

. parse_options.sh || exit 1

function Usage {
  cat<<EOF

  [Examples]: $0 --steps 1 --cmd $cmd \
  --nj $nj \
  --train-stage $train_stage \
  --train-data-name $train_data_name \
  --max-jobs-for-degs $max_jobs_for_degs \
  --online-ivector-dir exp-baseline/tdnn/ivectors-train \
  data/train-augment-overall/train/mfcc-hires \
  data/lang \
  exp-baseline/tdnn/done-tdnn-d \
  exp-baseline/tdnn/smbr-tdnn
  
EOF
}

if [ $# -ne 4 ]; then
  Usage && exit 1;
fi

traindata=$1
lang=$2
srcdir=$3
dir=$4

steps=$(echo $steps | perl -e '$steps=<STDIN>; if($steps =~ m:(\d+)\-:g){$start = $1; $steps=$start; 
        for($i=$start+1; $i < $start + 10; $i++){$steps .=":$i"; } } print $steps;')
## echo "## LOG ($0): steps=$steps"  && exit 0
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi


if $use_gpu; then
  if ! cuda-compiled; then
    cat <<EOF && exit 1 
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA 
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.  Otherwise, call this script with --use-gpu false
EOF
  fi
  num_threads=1
else
  # Use 4 nnet jobs just like run_4d_gpu.sh so the results should be
  # almost the same, but this may be a little bit slow.
  num_threads=4
fi

if [ ! -f ${srcdir}/final.mdl ]; then
  echo "$0: expected ${srcdir}/final.mdl to exist; first run run_tdnn.sh or run_lstm.sh"
  exit 1;
fi

alidir=$srcdir/ali-${train_data_name}
if [ ! -z $step01 ]; then
  nj=200
  spknum=$(wc -l < $traindata/spk2utt)
  [ $spknum -le $nj ] && nj=$spknum
  steps/nnet3/align.sh  --cmd "$cmd" --use-gpu false \
    --online-ivector-dir $online_ivector_dir \
     --nj $nj $traindata $lang $srcdir ${srcdir}_ali ;
  echo "## LOG (step01, $0): done with alignment generation @ `date`"
fi

latdir=$srcdir/denlats-${train_data_name}
if [ ! -z $step02 ]; then
  nj=100
  spknum=$(wc -l < $traindata/spk2utt)
  [ $spknum -le $nj ] && nj=$spknum
  num_threads_denlats=1
  subsplit=1
   steps/nnet3/make_denlats.sh --cmd "$decode_cmd" --determinize true \
   --online-ivector-dir $online_ivector_dir \
   --nj $nj --sub-split $subsplit \
   --num-threads "$num_threads_denlats" --config conf/decode.config \
    $traindata $lang $srcdir $latdir
  echo "## LOG (step02, $0): done with lattice generation @ `date`"
fi

model_left_context=`nnet3-am-info $srcdir/final.mdl | grep "left-context:" | awk '{print $2}'` 
model_right_context=`nnet3-am-info $srcdir/final.mdl | grep "right-context:" | awk '{print $2}'` 

left_context=$[model_left_context + extra_left_context]
right_context=$[model_right_context + extra_right_context]

frame_subsampling_opt=
if [ -f $srcdir/frame_subsampling_factor ]; then
  frame_subsampling_opt="--frame-subsampling-factor $(cat $srcdir/frame_subsampling_factor)"
fi

cmvn_opts=`cat $srcdir/cmvn_opts`

degsdir=$srcdir/degs-${train_data_name}
if [ ! -z $step03 ]; then

  degs_opts="--determinize true --minimize true --remove-output-symbols true --remove-epsilons true --collapse-transition-ids true"
  get_egs_stage=-10
  steps/nnet3/get_egs_discriminative.sh \
    --cmd "$cmd --max-jobs-run $max_jobs_for_degs --mem 20G" --stage $get_egs_stage --cmvn-opts "$cmvn_opts" \
   --adjust-priors $adjust_priors \
   --online-ivector-dir $online_ivector_dir \
   --left-context $left_context --right-context $right_context \
   $frame_subsampling_opt \
   --frames-per-eg $frames_per_eg \
   --frames-overlap-per-eg $frames_overlap_per_eg ${degs_opts} \
   $traindata $lang $alidir $latdir $srcdir/final.mdl $degsdir ;
   echo "## LOG (step03, $0): done with degs generation @ `date`"
fi

if [ ! -z $step04 ]; then
  steps/nnet3/train_discriminative.sh --cmd "$cmd" \
    --stage $train_stage \
    --effective-lrate $effective_learning_rate --max-param-change $max_param_change \
    --criterion $criterion --drop-frames true \
    --num-epochs $num_epochs --one-silence-class $one_silence_class --minibatch-size $minibatch_size \
    --num-jobs-nnet $num_jobs_nnet --num-threads $num_threads \
    --regularization-opts "$regularization_opts" \
    --truncate-deriv-weights $truncate_deriv_weights --adjust-priors $adjust_priors \
    --modify-learning-rates $modify_learning_rates --last-layer-factor $last_layer_factor \
    $degsdir $dir 
  echo "## LOG (step04, $0): done with smbr training @ `date`"
fi

wait;
if [ ! -z $step05 ]; then
  # if you run with "--cleanup true --stage 6" you can clean up.
  rm $latdir/lat.*.gz || true
  rm $alidir/ali.*.gz || true
  steps/nnet2/remove_egs.sh $degsdir || true
  echo "## LOG (step05, $0): done with cleanup @ `date`"
fi
graphdir=exp-baseline/tri4a/graph
if [ ! -z $step06 ]; then
  for dataName in dev-man-dominant dev-sge-dominant; do
    decode_set=data/$dataName/mfcc-hires
    spknum=$(wc -l < $decode_set/spk2utt)
    [ $spknum -lt $nj ] && nj=$spknum
    steps/nnet3/decode.sh \
          --nj $nj --cmd "$cmd" \
          --online-ivector-dir $(dirname $srcdir)/ivectors-${dataName} \
          $graphdir $decode_set $dir/decode-${dataName} || exit 1; 
    echo "## (step06, $0): done with decode on data '$dataName' @ `date`"
  done
fi

