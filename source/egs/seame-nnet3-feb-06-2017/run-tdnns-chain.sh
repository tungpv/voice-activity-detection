#!/bin/bash 

echo
echo "## LOG: $0 $@"
echo

. path.sh
. cmd.sh 

# begin options
steps=
cmd='slurm.pl --quiet --exclude=node06'
nj=40

# end options

function Usage {
 cat<<EOF

 [Examples]: $0 --steps 1 --cmd "$cmd" --nj $nj \
 

EOF
}

