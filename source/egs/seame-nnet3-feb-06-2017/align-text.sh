#!/bin/bash 

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
cmd='slurm.pl --quiet'
scoring_dir=scoring_kaldi
nj=40
# end options
function Usage {
 cat <<EOF

 [Examples]: $0 --steps 1 \
  --cmd "$cmd" \
  --scoring-dir $scoring_dir \
  data/dev-man-dominant/mfcc-hires data/lang \
  exp-baseline/tdnn/chain-no-data-augmentation/decode-dev-man-dominant

EOF
}

. parse_options.sh || exit 1

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
latdir=$3

steps=$(echo $steps | perl -e '$steps=<STDIN>; if($steps =~ m:(\d+)\-:g){$start = $1; $steps=$start; 
        for($i=$start+1; $i < $start + 10; $i++){$steps .=":$i"; } } print $steps;')
## echo "## LOG ($0): steps=$steps"  && exit 0
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi



if [ ! -z $step01 ]; then
  local/score.sh  --cmd "$cmd" \
  --scoring-kaldi $scoring_dir  $data $lang $latdir
fi
if [ ! -z $step02 ]; then
  tgtdir=$latdir/$scoring_dir/wer_details
  for x in ops; do
    [ -e $tgtdir/$x ] ||{ echo "## ERROR ($0): file $tgtdir/$x expected"; exit 1; }
  done
  cat $tgtdir/$x | \
  source/egs/seame-nnet3-feb-06-2017/align-text/get-eng-man-subs.pl $tgtdir
  cat $tgtdir/per_utt | source/egs/seame-nnet3-feb-06-2017/align-text/get-word-error.pl > $tgtdir/per_language
  echo "## LOG (step02, $0): done with getting substitution statistics ('$tgtdir') @ `date`"
fi

