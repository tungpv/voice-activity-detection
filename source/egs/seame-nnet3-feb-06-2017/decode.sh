#!/bin/bash

cmd='slurm.pl --quiet --exclude=node01,node02'
dir=exp/nnet3/chain/tdnn_7l-frames-per-eg150-no-ivector
if false; then
 for dataName in dev-man-dominant dev-sge-dominant; do
    decode_set=data/$dataName/mfcc-hires
    steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
          --nj 10 --cmd "$cmd" \
          $dir/graph $decode_set $dir/decode-${dataName} || exit 1;
  done
fi

dir=exp/nnet3/chain/tdnn_7l-frames-per-eg150-with-nnet1-ali
 for dataName in dev-man-dominant dev-sge-dominant; do
    decode_set=data/$dataName/mfcc-hires
    steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
          --nj 10 --cmd "$cmd" \
       --online-ivector-dir exp/nnet3/ivectors-${dataName}  \
          $dir/graph $decode_set $dir/decode-${dataName} || exit 1;
  done
