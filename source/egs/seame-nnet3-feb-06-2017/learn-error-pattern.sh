#!/bin/bash 

. path.sh
. cmd.sh

# begin options
steps=
cmd='slurm.pl --quiet'
nj=40
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF

 [Example]: $0 --steps 1 --cmd "$cmd" \
    ../seame-nnet3-feb-06-2017/exp-with-multicondition/tdnn/chain/decode-train-multicondition/decode-train-multicondition/scoring_kaldi/penalty_0.0/10.txt \
 ../seame-nnet3-feb-06-2017/data/train-multicondition/mfcc-hires/text \
 ../seame-nnet3-feb-06-2017/exp-with-multicondition/learn-error-pattern
EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

hyptext=$1
reftext=$2
tgtdir=$3

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z $step01 ]; then
  source/egs/seame-nnet3-feb-06-2017/learn-error-pattern.pl $hyptext $reftext $tgtdir
  echo "## LOG (step01): done & check '$tgtdir'"
fi
