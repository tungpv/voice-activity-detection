#!/bin/bash

echo
echo "## LOG: $0 $@"
echo 

. path.sh
. cmd.sh 

# begin options
steps=
cmd='slurm.pl --quiet --exclude=node01'
nj=40
feature_type=mfcc
feature_config=conf/mfcc_hires.conf
ivector_extractor_dir=exp/nnet3/extractor
srcdir=exp/nnet3/chain/tdnn_7l
# end options

function Usage {
 cat<<EOF

 [Example]: $0 --steps 1 --cmd "$cmd" --nj $nj \
  --feature-type $feature_type  --feature-config $feature_config \
  ${ivector_extractor_dir:+--ivector-extractor-dir $ivector_extractor_dir} \
  ${srcdir:+--srcdir $srcdir} \
  data/dev-man-dominant/mfcc-hires  data/lang exp/nnet3/chain/tdnn_7l/graph \
  exp/nnet3/chain/tdnn_7l/online-decode-dev-man-dominant

EOF
}

. parse_options.sh || exit 1

# echo "## LOG ($0): arguments=$# ($*)"
if [ $# -ne 4 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
graph=$3
dir=$4

steps=$(echo $steps | perl -e '$steps=<STDIN>; if($steps =~ m:(\d+)\-:g){$start = $1; $steps=$start; 
        for($i=$start+1; $i < $start + 10; $i++){$steps .=":$i"; } } print $steps;')
## echo "## LOG ($0): steps=$steps"  && exit 0
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -z $srcdir ] && srcdir=$(dirname $dir)
online_config=$srcdir/online-conf
if [ ! -z $step01 ]; then
  steps/online/nnet3/prepare_online_decoding.sh ${feature_type:+--feature-type $feature_type} \
  ${feature_config:+--mfcc-config $feature_config}  $lang  \
  ${ivector_extractor_dir:+$ivector_extractor_dir} $srcdir $online_config || exit 1
  echo "## LOG (step01, $0): done (online_config='$online_config') with online configure generation"
fi
cat <<'EOF' > $online_config/conf/nnet3.conf
 
EOF

if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): online decoding started @ `date`"
  steps/online/nnet3/decode.sh --cmd "$cmd" --nj $nj \
  --online-config $online_config/conf/online.conf \
  --per-utt true \
  --acwt 0.1 \
  --online true \
  ${srcdir:+--srcdir $srcdir} \
  $graph $data $dir || exit 1
  echo "## LOG (step02, $0): done ('$dir')"
fi
