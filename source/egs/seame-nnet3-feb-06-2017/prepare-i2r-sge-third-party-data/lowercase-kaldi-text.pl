#!/usr/bin/perl -w
use strict;
use utf8;
use open qw (:std :utf8);
# begin sub
sub IsCnWord {
  my ($word) = @_;
  if($word =~ /\p{Han}+/g) {
    return 1;
  }
  return 0;
}
# end sub
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my $utt = $1;
  my @A = split(/\s+/, $2);
  for(my $i = 0; $i < @A; $i ++) {
    my $word = \$A[$i];
    if(IsCnWord($$word) == 0) {
      $$word = lc $$word;
    }
  }
  print "$utt " . join(" ", @A) . "\n";
}
print STDERR "## LOG ($0): stdin ended\n";
