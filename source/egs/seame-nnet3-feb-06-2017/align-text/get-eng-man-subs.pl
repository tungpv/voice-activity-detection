#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\n[Example]: cat kaldi-ops | $0 dir\n\n";
}
my $tgtdir = shift @ARGV;

# begin sub
sub IsChineseChar {
  my ($str) = @_;
  return 1 if($str =~ m:\p{Han}+:);
  return 0;
}
# end sub

print STDERR "## LOG ($0): stdin expected\n";
open(F, ">$tgtdir/sub0.txt") or die "## ERROR ($0): failed to open $tgtdir/sub0.txt\n";
open(F1, "|sort -nrk3 > $tgtdir/sub1.txt") or die "## ERROR ($0): failed to open $tgtdir/sub1.txt\n";
open(F2, "|sort -nrk3 > $tgtdir/sub2.txt") or die "## ERROR ($0): failed to open $tgtdir/sub2.txt\n";
open(F3, "|sort -nrk3 > $tgtdir/sub3.txt") or die "## ERROR ($0): failed to open $tgtdir/sub3.txt\n";
open(F4, "|sort -nrk3 > $tgtdir/sub4.txt") or die "## ERROR ($0): failed to open $tgtdir/sub4.txt\n";
my ($total, $num1, $num2, $num3, $num4) = (0, 0, 0, 0);
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  die if scalar @A != 4;
  next if (/\</g);
  if($A[0] =~ /^substitution/) {
     shift @A;
     $total += $A[2];
    my ($word1, $word2) = ($A[0], $A[1]);
    my $str = sprintf("%-10s %-10s %-4d", $A[0], $A[1], $A[2]);
    my $is1 = IsChineseChar($word1);
    my $is2 = IsChineseChar($word2);
    if($is1 == 0 && $is2 == 0) {
      $num1 += $A[2];
      print F1 "$str\n";
    } elsif ($is1 ==0 && $is2 == 1) {
      $num2 += $A[2];
      print F2 "$str\n";
    } elsif ($is1 == 1 && $is2 == 0) {
      $num3 += $A[2];
      print F3 "$str\n";
    } else {
      $num4 += $A[2];
      print F4 "$str\n";
    }
  }
}
die if($total != $num1 + $num2 + $num3 + $num4);
print F "$total $num1 $num2 $num3 $num4\n";
print STDERR "## LOG ($0): stdin ended\n";
close (F);
close (F1);
close (F2);
close (F3);
close (F4);
