#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;

my $ignore_eng_to_man = 0;
my $ignore_man_to_eng = 0;

GetOptions('ignore_eng_to_man|ignore-eng-to-man=i' => \$ignore_eng_to_man,
           'ignore_man_to_eng|ignore-man-to-eng=i' => \$ignore_man_to_eng);

# begin sub
sub IsChineseChar {
  my ($word) = @_;
  return 1 if($word =~ /\p{Han}+/);
  return 0;
}
sub get_utt_ali_statistics {
  my ($uttis, $array, $E, $M, $S, $T) = @_;
  if(scalar @$array != 4) {
    die "## ERROR (", __LINE__, "): array error\n";
  }
  my $str_ref = $$array[0];
  my @R = split(/\s+/, $$str_ref); my $keyid = shift @R;
  die if $keyid ne 'ref';
  $str_ref = $$array[1];
  my @H = split(/\s+/, $$str_ref); $keyid = shift @H;
  die if $keyid ne 'hyp';
  $str_ref = $$array[2];
  my @O = split(/\s+/, $$str_ref); $keyid = shift @O;
  die if $keyid ne 'op';
  die if scalar @R != scalar @H || scalar @H != scalar @O;
  for(my $i = 0; $i < @O; $i ++) {
    my $op = $O[$i];
    my $rWord = $R[$i];
    my $hWord = $H[$i];
    my $isRChinese = IsChineseChar($rWord);
    my $isHChinese = IsChineseChar($hWord);
    if($rWord =~ /\*\*\*/) {  ## insertion errors
      if($isHChinese == 1) {
        $$M[2] ++;
        # die "hword=$hWord\n";
      } else {
        $$E[2] ++;
      }
      $$T[2] ++;
      die if $op ne 'I';
      next;
    }
    if($op eq 'C') {
      $$T[0] ++;
      if($isRChinese == 1) {
        $$M[0] ++;
      } else {
        $$E[0] ++;
      }
    } elsif($op eq 'S') {
      $$T[1] ++;
      if($isRChinese == 1) {
        $$M[1] ++;
        if($isHChinese == 1) {
          $$S[3] ++;
        } else {
          $$S[2] ++;
        }
      } else {
        $$E[1] ++;
        if($isHChinese == 0) {
          $$S[0] ++;
        } else {
          $$S[1] ++;
        }
      }
    } elsif ($op eq 'D') {
      $$T[3] ++;
      if($isRChinese == 1) {
        $$M[3] ++;
      } else {
        $$E[3] ++;
      }
    } else {
      die "## ERROR (", __LINE__, "): unknown op $op\n";
    }
  }
}
sub compute_wer {
  my ($array, $serror, $ignore) = @_;
  my $total = $$array[0];
  $total += $$array[1];
  $total += $$array[3];
  my $error = $$array[1] + $$array[2] + $$array[3] - $ignore;
  print STDERR "## LOG (", __LINE__, "): ignore substitutions are $ignore\n";
  $$serror = 0;
  if($total != 0) {
    $$serror = $error*100/$total;
  }
  $$serror = sprintf("%.2f", $$serror);
}
# end sub
print STDERR "## LOG ($0): stdin expected\n";
my $uttid = '';
my @A = ();
my @E = ();
my @M = ();
my @S = ();
my @T = ();
while(<STDIN>) {
  chomp;
  m:(\S+)\s+(.*):g or next;
  my ($curr_uttid, $curr_value) = ($1, $2);
  if($curr_uttid ne $uttid && $uttid ne '') {
    get_utt_ali_statistics($uttid, \@A, \@E, \@M, \@S, \@T);
    @A = ();
    $uttid = $curr_uttid;
    push @A, \$curr_value;
  } else {
    push @A, \$curr_value;
    $uttid = $curr_uttid;
  }
}
if ($uttid ne '') {
  get_utt_ali_statistics($uttid, \@A, \@E, \@M, \@S, \@T);
}
print STDERR "## LOG ($0): stdin ended ...\n";

print "Total: " . join(" ", @T) . "\n";
my $serror = 0;
compute_wer(\@M, \$serror, $ignore_man_to_eng);
print "Man: $serror " . join(" ", @M) . "\n";
compute_wer(\@E, \$serror, $ignore_eng_to_man);
print "Sge: $serror " . join(" ", @E) . "\n";
print "Sub: " . join(" ", @S) . "\n";
my $sum = 0;
for(my $i = 0; $i < @S; $i ++) {
  $sum += $S[$i];
}
printf("SRate: ");
for(my $i = 0; $i < @S; $i ++) {
  my $r = $S[$i] / $sum;
  printf("%.4f ", $r);
}
printf("\n");
# ignore mandarin-to-english substitution for mandarin
$sum = $M[0] + $M[1] + $M[3];
my $e = $M[1] - $S[2] + $M[2] + $M[3];
printf("Mandarin-oracle-WER: ");
printf("%.4f\n", $e/$sum);
# ignore english-to-mandarin substitution for english
$sum = $E[0] + $E[1] + $E[3];
$e = $E[1] - $S[1] + $E[2] + $E[3];
printf("English-oracle-WER: ");
printf("%.4f\n", $e/$sum);
# get  oracle  error rate 
$sum = $T[0] + $T[1] + $T[3];
$e = $S[0] + $S[3] + $T[2] +$T[3];

printf("Overall-oracle-WER: ");
printf("%.4f\n", $e/$sum);

# get WER ignoring modal particle words
$e = $T[1] + $T[2] + $T[3] - $ignore_man_to_eng - $ignore_eng_to_man;
printf("WER-ignore-particle: ");
printf("%.4f\n", $e/$sum);
