#!/usr/bin/perl
use strict;
use warnings;
# begin sub
sub IsChineseChar {
  my ($str) = @_;
  return 1 if($str =~ m:\p{Han}+:);
  return 0;
}
sub get_utt_ali_statistics {
  my ($uttid, $array, $T, $S, $errorArray) = @_;
  if(scalar @$array != 4) {
    die "## ERROR (", __LINE__, "): array error\n";
  }
  my $str_ref = $$array[3];
  my @E = split(/\s+/, $$str_ref);
  shift @E;   # remove the first field
  for(my $i = 0; $i < scalar @E; $i ++) {
    $$T[$i] += $E[$i];
  }
  $str_ref = $$array[0];
  my @R = split(/\s+/, $$str_ref); my $keyid = shift @R;
  die if $keyid ne 'ref';
  $str_ref = $$array[1];
  my @H = split(/\s+/, $$str_ref); $keyid = shift @H;
  die if $keyid ne 'hyp';
  $str_ref = $$array[2];
  my @O = split(/\s+/, $$str_ref); $keyid = shift @O;
  die if $keyid ne 'op';
  die if scalar @R != scalar @H || scalar @R != scalar @O;

  if ($E[1] > 0) {  # we only focus on substitution errors
                    # we define 00 (en-to-en error), 01 (en-to-man), 10 (man-to-en), 11 (man-to-man)
    for(my $i = 0; $i < scalar @E; $i ++) {
      if($E[$i] eq 'S') {
        die if($R[$i] eq $H[$i]);
        my $enR = IsChineseChar($R[$i]);
        my $enH = IsChineseChar($H[$i]);
        if($enR == 0 && $enH == 0) {
          $$S[0] ++;  my $vocab = $$errorArray[0];
          $$vocab{$R[$i]}{$H[$i]} ++;
        } elsif ($enR == 0 && $enH == 1) {
          $$S[1] ++; my $vocab = $$errorArray[1];
          $$vocab{$R[$i]}{$H[$i]} ++;
        } elsif ($enR == 1 && $enH == 0) {
          $$S[2] ++; my $vocab = $$errorArray[2];
          $$vocab{$R[$i]}{$H[$i]} ++;
        } else {
          $$S[3] ++; my $vocab = $$errorArray[3];
          $$vocab{$R[$i]}{$H[$i]} ++;
        }
      }
    }
  }
}
# end sub
# main entrance
if (scalar @ARGV != 1) {
  die "\n[Example]: cat utt-ali.txt | $0 tgtdir\n\n";
}

my $tgtdir = shift @ARGV;

print STDERR "## LOG ($0): stdin expected ...\n";
my $uttid = '';
my @A = (); my @T = (0, 0, 0, 0); my @errorArray = ();
for(my $i = 0; $i < scalar @errorArray; $i ++) {
  my %vocab = ();
  push @errorArray, \%vocab;
}
my @S = (0, 0, 0, 0);
while(<STDIN>) {
  chomp;
  m:(\S+)\s+(.*):g or next;
  my ($curr_uttid, $curr_value) = ($1, $2);
  if($curr_uttid ne $uttid && $uttid ne '') {
    get_utt_ali_statistics($uttid, \@A, \@T, \@S, \@errorArray);
    @A = ();
    $uttid = $curr_uttid;
    push @A, \$curr_value;
  } else {
    push @A, \$curr_value;
    $uttid = $curr_uttid;
  }
}
if ($uttid ne '') {
  get_utt_ali_statistics($uttid, \@A, \@T, \@S, \@errorArray);
}
print STDERR "## LOG ($0): stdin ended ...\n";

my $werFile = "$tgtdir/WER";
open(F, ">$werFile") or die "## ERROR (", __LINE__, "): cannot open werFile $werFile\n";
my $total = 0;
for(my $i = 0; $i < scalar @T; $i ++) {
  $total += $T[$i];
}
print STDERR "total=$total\n";
close F;
