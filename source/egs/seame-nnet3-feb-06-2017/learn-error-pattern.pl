#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = @ARGV;
if($numArgs != 3) {
  die "\n[Example]: $0 <hyptext> <reftext> <tgtdir>\n\n";
}

my ($hyptext, $reftext, $tgtdir) = @ARGV;

# begin sub
sub NormalizeText {
  my ($text) = @_;
  $$text =~ s:<[^<]+>::g;
  $$text =~ s:^\s+::g;
  $$text =~ s:\s+$::g;
}
# end sub

my %vocab = ();
open(F, "$reftext") or die;
while(<F>) {
  chomp;
  m:^(\S+)\s*(.*)$:g;
  my $text = $2;
  NormalizeText(\$text);
  next if ($text =~ /^$/);
  $vocab{$1} = $text;
}
close F;

open(F, "$hyptext") or die;
open(H, ">$tgtdir/hyp.txt") or die;
open(R, ">$tgtdir/ref.txt") or die;
while(<F>) {
  chomp;
  m:^(\S+)\s*(.*):g;
  my ($labId, $text) = ($1, $2);
  NormalizeText(\$text);
  next if ($text =~ /^$/);
  print STDERR "oov labid '$labId' seen\n" and next if(not exists $vocab{$labId});
  print H "$text\n";
  print R "$vocab{$labId}\n";
}
close F;
close H;
close R;
