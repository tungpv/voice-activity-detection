#!/bin/bash 

if [ $# -ne 1 ]; then
  echo -e "\nUsage: $0 <nnet3-train-logdir>\n" && exit 1
fi 

dir=$1

for x in $(seq 0 2000); do 
  xfile=$dir/log/compute_prob_valid.$x.log  
  [ -f $xfile ] &&  grep output-xent  $xfile | perl -e ' $x = shift @ARGV; while(<STDIN>){s/^LOG.*\)//g; print "iter($x): $_"; $x++; }' $x
done
