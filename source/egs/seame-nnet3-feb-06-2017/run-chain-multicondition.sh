#!/bin/bash 

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd='slurm.pl --quiet'
nj=40
steps=
train_stage=-10
get_egs_stage=-10
frames_per_eg=150
minibatch_size=128
num_epochs=4
num_jobs_initial=3
num_jobs_final=16
remove_egs=false
max_param_change=2.0
initial_effective_lrate=0.001
final_effective_lrate=0.0001
experiment_name=tdnn_7l
disabled=false
min_utt_len=5.0    # minimum utterance length to train ivector
xent_regularize=0.1
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF

 Example: $0 --steps 1 --cmd "$cmd" --nj $nj  --train-stage $train_stage \
 --frames-per-eg $frames_per_eg \
 --minibatch-size $minibatch_size \
 --num-epochs $num_epochs \
 --num-jobs-initial $num_jobs_initial \
 --num-jobs-final $num_jobs_final \
 --remove-egs true \
 --max-param-change $max_param_change \
 --initial-effective-lrate $initial_effective_lrate \
 --final-effective-lrate $final_effective_lrate \
 data/train-multicondition/mfcc-pitch \
 data/train-multicondition/mfcc-hires \
 data/lang \
 exp-with-multicondition/tri4a  \
 exp-with-multicondition/tdnn

EOF
}

if [ $# -ne 5 ]; then
  Usage && exit 1
fi

alidata=$1
traindata=$2
lang=$3
srcdir=$4
nnetdir=$5

steps=$(echo $steps | perl -e '$steps=<STDIN>; if($steps =~ m:(\d+)\-:g){$start = $1; $steps=$start; 
        for($i=$start+1; $i < $start + 10; $i++){$steps .=":$i"; } } print $steps;')
## echo "## LOG ($0): steps=$steps"  && exit 0
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi


latdir=$srcdir/ali-train-lattice
if [ ! -z $step01 ]; then
  nj=$(cat $srcdir/ali_train/num_jobs) || exit 1;
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $alidata \
  $lang $(dirname $latdir)  $latdir || exit 1
  rm $latdir/fsts.*.gz # save space
  echo "## LOG (step01, $0): done lattice from alignment @ `date`"
fi
chain_lang=$nnetdir/lang-chain
if [ ! -z $step02 ]; then
  rm -rf $chain_lang
  cp -r $lang $chain_lang
  silphonelist=$(cat $chain_lang/phones/silence.csl) || exit 1;
  nonsilphonelist=$(cat $chain_lang/phones/nonsilence.csl) || exit 1;
  # Use our special topology... note that later on may have to tune this
  # topology.
  steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >$chain_lang/topo
  echo "## LOG (step02, $0): done with lang-chain ('$chain_lang') @ `date`"
fi

alidir=$srcdir/ali_train
leftmost_questions_truncate=-1
treedir=$nnetdir/chain-tree
if [ ! -z $step03 ]; then
  # Build a tree using our new topology. This is the critically different
  # step compared with other recipes.
  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
  --leftmost-questions-truncate $leftmost_questions_truncate \
  --context-opts "--context-width=2 --central-position=1" \
  --cmd "$cmd" 7000 $alidata $chain_lang $alidir $treedir
  echo "## LOG (step02, $0): done with tree building @ `date`"
fi

chaindir=$nnetdir/chain
if [ ! -z $step04 ]; then
  
  num_targets=$(tree-info $treedir/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)

  mkdir -p $chaindir/configs
  cat <<EOF > $chaindir/configs/network.xconfig
  input dim=100 name=ivector
  input dim=40 name=input
  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor
  fixed-affine-layer name=lda input=Append(-1,0,1,ReplaceIndex(ivector, t, 0)) affine-transform-file=$chaindir/configs/lda.mat
  # the first splicing is moved before the lda layer, so no splicing here
  relu-renorm-layer name=tdnn1 dim=625
  relu-renorm-layer name=tdnn2 input=Append(-1,0,1) dim=625
  relu-renorm-layer name=tdnn3 input=Append(-1,0,1) dim=625
  relu-renorm-layer name=tdnn4 input=Append(-3,0,3) dim=625
  relu-renorm-layer name=tdnn5 input=Append(-3,0,3) dim=625
  relu-renorm-layer name=tdnn6 input=Append(-3,0,3) dim=625
  relu-renorm-layer name=tdnn7 input=Append(-3,0,3) dim=625
  ## adding the layers for chain branch
  relu-renorm-layer name=prefinal-chain input=tdnn7 dim=625 target-rms=0.5
  output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5
  # adding the layers for xent branch
  # This block prints the configs for a separate output that will be
  # trained with a cross-entropy objective in the 'chain' models... this
  # has the effect of regularizing the hidden parts of the model.  we use
  # 0.5 / args.xent_regularize as the learning rate factor- the factor of
  # 0.5 / args.xent_regularize is suitable as it means the xent
  # final-layer learns at a rate independent of the regularization
  # constant; and the 0.5 was tuned so as to make the relative progress
  # similar in the xent and regular final layers.
  relu-renorm-layer name=prefinal-xent input=tdnn7 dim=625 target-rms=0.5
  relu-renorm-layer name=prefinal-lowrank-xent input=prefinal-xent dim=64 target-rms=0.5
  output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF
  steps/nnet3/xconfig_to_configs.py --xconfig-file $chaindir/configs/network.xconfig --config-dir $chaindir/configs/  
  echo "## LOG (step04, $0): done with xconfig-file generation ('$chaindir/configs') @ `date`"
fi

common_egs_dir=
get_egs_stage=-10
if [ ! -z $step05 ]; then
  steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$cmd" \
    --feat.online-ivector-dir $nnetdir/ivectors-train \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize $xent_regularize \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --egs.dir "$common_egs_dir" \
    --egs.stage $get_egs_stage \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width $frames_per_eg \
    --trainer.num-chunk-per-minibatch $minibatch_size \
    --trainer.frames-per-iter 1500000 \
    --trainer.num-epochs $num_epochs \
    --trainer.optimization.num-jobs-initial $num_jobs_initial \
    --trainer.optimization.num-jobs-final $num_jobs_final \
    --trainer.optimization.initial-effective-lrate $initial_effective_lrate \
    --trainer.optimization.final-effective-lrate $final_effective_lrate \
    --trainer.max-param-change $max_param_change \
    --cleanup.remove-egs $remove_egs \
    --feat-dir $traindata \
    --tree-dir $treedir \
    --lat-dir $latdir \
    --dir $chaindir  || exit 1;
  echo "## LOG (step05, $0): tdnn done (chaindir='$chaindir') @ `date`"
fi
graph=$chaindir/graph
if [ ! -z $step06 ]; then
  utils/mkgraph.sh --self-loop-scale 1.0 data/lang $chaindir $chaindir/graph
  echo "## LOG (step06, $0): done with graph building ('$graph')"
fi

if [ ! -z $step07 ]; then
  for dataName in dev-man-dominant dev-sge-dominant; do
    decode_set=data/$dataName/mfcc-hires
    spknum=$(wc -l < $decode_set/spk2utt)
    [ $spknum -lt $nj ] && nj=$spknum
    steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
          --nj $nj --cmd "$cmd" \
          --online-ivector-dir $nnetdir/ivectors-${dataName} \
          $chaindir/graph $decode_set $chaindir/decode-${dataName} || exit 1;
  done 
  echo "## LOG (step07, $0): done with decoding @ `date`"
fi

