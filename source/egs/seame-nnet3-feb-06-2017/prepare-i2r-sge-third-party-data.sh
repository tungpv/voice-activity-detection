#!/bin/bash 

echo 
echo "## LOG: $0 $@"
echo

. path.sh
. cmd.sh 

# begin options
steps=
cmd='slurm.pl --quiet'
nj=40
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF
  
 [Examples]: $0 --steps 1 --cmd "$cmd" --nj $nj \
 /home2/hhx502/sg-en-i2r/data/overall-train \
 data/i2r-sge-16k-with-upsampling

EOF
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

srcdata=$1
tgtdir=$2

steps=$(echo $steps | perl -e '$steps=<STDIN>; if($steps =~ m:(\d+)\-:g){$start = $1; $steps=$start; 
        for($i=$start+1; $i < $start + 10; $i++){$steps .=":$i"; } } print $steps;')
## echo "## LOG ($0): steps=$steps"  && exit 0
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
overall=$tgtdir/overall
tmpdir=$tgtdir/temp
[ -d $overall ] || mkdir -p $overall
[ -d $tmpdir ] || mkdir -p $tmpdir
if [ ! -z $step01 ]; then
  cat $srcdata/text | \
  source/egs/seame-nnet3-feb-06-2017/prepare-i2r-sge-third-party-data/lowercase-kaldi-text.pl | \
  local/wer_ref_filter > $tmpdir/text
fi

if [ ! -z $step02 ]; then
  cat $srcdata/wav.scp | \
  source/egs/sge2017/local/upsample-cts-ref291-wav.pl > $tmpdir/wav.scp
  cp $srcdata/{utt2spk,spk2utt,segments} $tmpdir
  utils/fix_data_dir.sh $tmpdir
  utils/copy_data_dir.sh --spk-prefix i2r- --utt-prefix i2r- $tmpdir $overall
  rm -r $tmpdir
  echo "## LOG (step02, $0): done with overall data preparation '$overall'"
fi
if [ ! -z $step03 ]; then
  for sdata in $overall; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch; log=$sdata/log/mfcc-pitch
    utils/copy_data_dir.sh $sdata $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1
    utils/fix_data_dir.sh $data
    echo "## LOG (step03, $0): done with '$data' @ `date`"
    data=$sdata/mfcc-hires; feat=$sdata/feat/mfcc-hires; log=$sdata/log/mfcc-hires
    utils/copy_data_dir.sh $sdata $data 
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step03, $0): done with '$data' @ `date`" 
  done
fi
english_part=$tgtdir/en-data-overall
[ -d $english_part ] || mkdir $english_part
if [ ! -z $step04 ]; then
  for sdata in $overall/mfcc-pitch $overall/mfcc-hires; do
    data=$english_part/$(basename $sdata)
    [ -d $data ] || mkdir $data
    cat $sdata/text | \
    grep -v -P "[\x80-\xFF]" | awk '{print $1; }' > $data/uttlist
    utils/data/subset_data_dir.sh --utt-list $data/uttlist \
    $overall/$(basename $sdata) $data 
  done
  echo "## LOG (step04, $0): done '$english_part'"
fi
Array=(0.8 0.75 0.67 0.5);
if [ ! -z $step05 ]; then
  sdata=$tgtdir/en-data1.0
  i=0
  for x in en-data0.8 en-data0.6 en-data0.4 en-data0.2; do
    factor=${Array[$i]}
    data=$tgtdir/$x
    echo "## LOG (step05, $0): sdata=$sdata, factor=$factor, data=$data"
    source/egs/swahili/subset_data.sh --subset-time-ratio $factor \
    --random true $sdata/mfcc-pitch $data/mfcc-pitch || exit 1
    utils/subset_data_dir.sh --utt-list $data/mfcc-pitch/segments \
    $sdata/mfcc-hires $data/mfcc-hires
    sdata=$data
    i=$[i+1]
  done
fi
