#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
cmd='slurm.pl --quiet'
nj=40
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF
 [Example]: $0 --steps 1 --cmd "$cmd" --nj $nj \
 /home3/hhx502/w2017/seame-nnet3-feb-06-2017/data/train-multicondition/mfcc-pitch \
 /home3/hhx502/w2017/seame-nnet3-feb-06-2017/data/lang \
 /home3/hhx502/w2017/seame-nnet3-feb-06-2017/exp-with-multicondition/tri4a \
 /home3/hhx502/w2017/seame-nnet3-feb-06-2017/exp-with-multicondition/update-lexion-may-27-2017
EOF
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi
mfcc_pitch_data=$1
lang=$2
srcdir=$3
tgtdir=$4


steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
alidir=$tgtdir/ali_train
if [ ! -z $step01 ]; then
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj \
  $mfcc_pitch_data $lang $srcdir $alidir || exit 1
fi
if [ ! -z $step02 ]; then
   steps/get_prons.sh $mfcc_pitch_data $lang $alidir || exit 1
fi
analysisdir=$tgtdir/analysis
if [ ! -z $step03 ]; then
  if [ -z $alidir/pron_perutt_nowb.txt ]; then
    echo "## ERROR (step03, $0): '$alidir/pron_perutt_nowb.txt' expected"; exit 1;
  fi
  [ -d $analysisdir ] || mkdir -p $analysisdir
  source/egs/mandarin/update-with-nnet3/update-lexicon-lm-march-30-2017.py ${debug:+--debug=$debug} \
--pron-perutt-nowb=$alidir/pron_perutt_nowb.txt \
--target-dir=$analysisdir   || exit 1

fi
debug=true
if [ ! -z $step04 ]; then
  [ -d $tgtdir/lexicon_analysis ] || mkdir $tgtdir/lexicon_analysis
  source/egs/mandarin/update-with-nnet3/analyze-lexicon.py ${debug:+--debug=$debug} \
   --pronunciation-count-file=$analysisdir/temp/lexicon01.txt \
   --tgtdir=$tgtdir/lexicon_analysis
fi

pruned_dict=$tgtdir/lexicon_analysis
tgtdir=/home3/hhx502/w2017/seame-nnet3-feb-06-2017/update-may-27-with-mandarin-word
dictdir=$tgtdir/data/dict
[ -d $dictdir ] || mkdir -p $dictdir
lang=$tgtdir/data/lang
if [ ! -z $step05 ]; then
  srcdictdir=/home2/hhx502/seame/data/local/dict-sil
  cp $srcdictdir/{silence_phones.txt,optional_silence.txt,nonsilence_phones.txt,extra_questions.txt} $dictdir/
  cat $pruned_dict/pruned-lexicon.txt | sort -u > $dictdir/lexicon.txt
  utils/validate_dict_dir.pl $dictdir
  utils/prepare_lang.sh $dictdir "<unk>" $lang/tmp  $lang
fi
lmdir=$tgtdir/data/lm
[ -d $lmdir ] || mkdir -p $lmdir
if [ ! -z $step06 ]; then
  cut -d' ' -f2- $analysisdir/temp/text | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; s:<silence>::g; s:<v-noise>::g;  s: +: :g; print "$_\n"; ' \
  | sort -u | gzip -c > $lmdir/train-text.gz
  cut -d ' ' -f2- ../seame-nnet3-feb-06-2017/data/dev-man-dominant/text \
  ../seame-nnet3-feb-06-2017/data/dev-sge-dominant/text | \
  source/egs/mandarin/update-april-03-2017-with-pruned-lexicon/segment-chinese-text.py | \
  gzip -c >$lmdir/dev-text.gz
  cut -f1 $dictdir/lexicon.txt | sort -u | gzip -c > $lmdir/vocab.gz
fi
if [ ! -z $step07 ]; then
 source/egs/sge2017/local/build-lm/train-srilm-v2.sh --steps 1,2 --lm-order-range "3 3" \
  --cutoff-csl "3,011,012"  $lmdir || exit 1
fi
train=$tgtdir/data/train
if [ ! -z $step08 ]; then
  source/egs/fisher-english/arpa2G.sh $lmdir/lm.gz $lang $lang
fi
aligned_text=$analysisdir/temp/text
if [ ! -z $step09 ]; then
  sdata=../seame-nnet3-feb-06-2017/data/train-multicondition
  utils/data/subset_data_dir.sh --utt-list $aligned_text   $sdata/mfcc-hires  $train/mfcc-hires
  cp $aligned_text $train/mfcc-hires || exit 1
  utils/data/fix_data_dir.sh $train/mfcc-hires
  echo "## LOG (step09, $0): data '$train/mfcc-hires' preparation done"
  utils/data/subset_data_dir.sh --utt-list $aligned_text  $sdata/mfcc-pitch  $train/mfcc-pitch
  cp $aligned_text $train/mfcc-pitch
  utils/data/fix_data_dir.sh $train/mfcc-pitch
fi
alidir=../seame-nnet3-feb-06-2017/exp-with-multicondition/tri4a/ali_train-with-pruned-mandarin-word-lexicon
if [ ! -z $step10 ]; then
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj $train/mfcc-pitch $lang $(dirname $alidir) $alidir || exit 1 
fi
srcdir=$tgtdir/exp/tri4a
senones=6000
num_leaves=100000
srcalidir=$alidir
alidir=$srcdir/ali_train
if [ ! -z $step11 ]; then
  data=$train/mfcc-pitch
  steps/train_sat.sh  --cmd "$cmd"  $senones $num_leaves   $data  $lang $srcalidir $srcdir || exit 1
  echo "## LOG (step11, $0): training is done '$srcdir' @ `date`"
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj $data  $lang $srcdir  $alidir || exit 1
fi
latdir=$srcdir/ali_lattice-train
if [ ! -z $step12 ]; then
  nj=$(cat $alidir/num_jobs) || exit 1;
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $train/mfcc-pitch  $lang $srcdir  $latdir || exit 1
  rm $latdir/fsts.*.gz
fi
min_utt_len=5.0
if [ ! -z $step13 ]; then
  for sdata in $train/{mfcc-pitch,mfcc-hires}; do
    data=$train/subset-to-train-ivector/$(basename $sdata);
    [ -d $data ] || mkdir -p $data 
    echo "## LOG (step13, $0): segments=$sdata/segments"
    cat $sdata/segments | \
    awk -v thresh=$min_utt_len '{ if($4-$3 >= thresh){print $1 ;} }' > $data/utt-list
    subset_data_dir.sh --utt-list $data/utt-list $sdata $data
    echo "## LOG (step13, $0): subset data '$data' generated"
  done
fi
alidir_ivectors=$srcdir/ali_train-subset-to-train-ivector
alidir=$srcdir/ali_train
if [ ! -z $step14 ]; then
  data=$train/subset-to-train-ivector/mfcc-pitch
  steps/align_fmllr.sh --nj $nj --cmd "$cmd"   $data  $lang $srcdir   $alidir_ivectors || exit 1
  echo "## LOG (step14, $0): done & check '$alidir_ivectors'"
fi
nnetdir=$tgtdir/exp/tdnn
transform_dir=$nnetdir/lda-mllt-transform-for-ivector-extractor
if [ ! -z $step15 ]; then
  data=$train/subset-to-train-ivector/mfcc-hires
  steps/train_lda_mllt.sh --cmd "$cmd"  --num-iters 13 \
  --realign-iters "" \
  --splice-opts "--left-context=3 --right-context=3" \
  7000 10000 $data  $lang   $alidir_ivectors $transform_dir || exit 1
  echo "## LOG (step15, $0): done with lda-mllt-transform ('$transform_dir') @ `date`"
fi
if [ ! -z $step16 ]; then
  data=$train/subset-to-train-ivector/mfcc-hires
  steps/online/nnet2/train_diag_ubm.sh --cmd "$cmd" --nj $nj --num-threads 2 \
  --num-frames 720000 $data  1024  $transform_dir $nnetdir/diag_ubm || exit 1
  echo "## LOG (step16, $0): done with diag_ubm ('$nnetdir/diag_ubm') @ `date`"
fi
ivector_extractor=$nnetdir/ivector-extractor
if [ ! -z $step17 ]; then
  data=$train/subset-to-train-ivector/mfcc-hires
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$cmd" --nj $nj \
  --num-threads 1 --num-processes 1 \
  $data  $nnetdir/diag_ubm $ivector_extractor || exit 1;
  echo "## LOG (step17, $0): done with ivector-extractor training ('$ivector_extractor') @ `date`"
fi
train_ivectors=$nnetdir/ivectors-train
if [ ! -z $step18 ]; then
  data=$train/mfcc-hires
  data2=$train/mfcc-hires-max2
  utils/data/modify_speaker_info.sh --utts-per-spk-max 2 $data   $data2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj  $data2 $ivector_extractor $train_ivectors || exit 1;
  echo "## LOG (step18, $0): done with ivector generation '$train_ivectors' @ `date`"
fi
if [ ! -z $step19 ]; then
  for dataName in  dev-man-dominant dev-sge-dominant; do
    data=/home3/hhx502/w2017/seame-nnet3-feb-06-2017/data/$dataName/mfcc-hires
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj 8 \
    $data $ivector_extractor $nnetdir/ivectors-${dataName} || touch exp/nnet3/.error
    echo "## LOG (step19, $0): done with ivector generations with data ('$nnetdir/ivectors-${dataName}') @ `date`"
   done
fi
