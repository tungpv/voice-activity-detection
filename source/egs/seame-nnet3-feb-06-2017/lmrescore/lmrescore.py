#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu

""" This program is intended for ngram lm rescoring for lattice
"""
import argparse
import logging
import os
import pprint
import shutil
import sys
import traceback
import gzip
import re
sys.path.insert(0, 'steps')
import libs.common as common_lib

logger = logging.getLogger('libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting lm rescoring lmrescore.py')

def get_args():
    parser = argparse.ArgumentParser(description='lmrescore arguments parser')

    parser.add_argument('--stage', type=int, dest='lmrescore_stage', default=-1, help="""prepare and do lattice rescoring stage by stage.""")
    parser.add_argument('--ngram-order', dest='ngram_order', type=int, default=3, help="""lm scale.""")
    parser.add_argument('--effective-nbest', dest='effective_nbest', type=int, default=1000000, help="specify actual nbest to perform rescoring")
    parser.add_argument('--acoustic-scale', dest='acoustic_scale', type=float, default=1.0, help="acoustic scaling factor")
    parser.add_argument('--lmscale', dest='lmscale', type=float, default=1.0, help='lm scaling factor')
    parser.add_argument("--inter-weight", dest='inter_weight', type=float, default=0.6, help="lambda.")
    parser.add_argument("--word-penalty", dest='word_penalty', type=float, default=0.0, help="word insertion penalty")
    parser.add_argument("--lmlogprob-file", dest='lmlogprob_file', type=str, default='', help="file from which to get logprob of each utterance, the format is as 'utterance logprob'")
    parser.add_argument("--save-lmlogprob-file", dest='save_lmlogprob_file', type=str, default='', help="file to save utterance logprob from srilm")
    parser.add_argument('word_vocab', type=str, help="""word-vocab file.""")
    parser.add_argument('word_lm_file', type=str, help="""word-lm-file.""")
    parser.add_argument('class_lm_file', type=str, help="""class-lm-file.""")
    parser.add_argument('class_count_file', type=str, help="""class-count-file.""")
    parser.add_argument("amcost_file", type=str, help="""amcost-file.""")
    parser.add_argument("nbest_trans_file", type=str, help="nbest-trans-file.")
    parser.add_argument("best_res_file", type=str, help="best result file.")
    print(' '.join(sys.argv))
    print(sys.argv)

    return parser.parse_args()

# [args, run_opts] = process_args(args)
def LoadAmCostFile(sAmCostFile, dictAmCost):
    # logger.info('sAmCostFile={0}'.format(sAmCostFile))
    try:
        inFile = gzip.open(sAmCostFile, 'rb')
        for sLine in inFile:
            myList = sLine.split()
            if len(myList) != 2:
                logger.error("abnormal line '{0}' in '{1}'".format(sLine, sAmCostFile))
                sys.exit(1)
            if myList[0] in dictAmCost:
                logger.error("duplicated line '{0}' in '{1}'".format(sLine, sAmCostFile))
                sys.exit(1)
            dictAmCost[myList[0]] = myList[1]            
    except IOError:
        raise Exception("cannot open file {0}".format(sAmCostFile))
        sys.exit(1)
def LmScoring(args, listUtterance):
    """ Use srilm to score the given utterance 
    """
    text = ''
    listLmScore = list()
    for utterance in listUtterance:
        text += utterance + '\n'
    order = args.ngram_order
    vocab = args.word_vocab
    word_lm_file = args.word_lm_file
    class_lm_file = args.class_lm_file
    class_count = args.class_count_file
    weight = args.inter_weight
    cmd = """ echo -e "{text}" | ngram -order {order} -vocab {vocab} \
        -lm {word_lm_file} -mix-lm {class_lm_file} \
       -classes {class_count} -lambda {weight} \
       -ppl - -debug 1 2>/dev/null""".format(text=text, order=order, vocab=vocab, 
                                 word_lm_file=word_lm_file, class_lm_file=class_lm_file, 
                                 class_count=class_count, weight=weight)
    stdout = common_lib.get_command_stdout(cmd)
    logger.debug('stdout={}'.format(stdout))
    listLmScore = re.findall(r'.*\slogprob=\s+(\S+)\s+ppl.*', stdout)
    del listLmScore[-1]
    return listLmScore
def OutputBest(args, outputHandle, uttLabel,  uttList, dictAmCost, dictLmLogprob):
    bestUtterance = ''
    listAmScore = list()
    listUtterance = list()
    listLabel = list()
    word_penalty = float(args.word_penalty)
    effective_nbest=int(args.effective_nbest)
    acoustic_scale=float(args.acoustic_scale)
    lmscale = float(args.lmscale)
    for nIndex, utterance in enumerate(uttList):
        logger.debug('utterance={0}'.format(utterance))
        utterance = utterance.strip()
        m = re.search(r'(^\S+)(.*$)', utterance)
        labelWithIndex = m.group(1)
        utterance = m.group(2)
        if( not utterance and nIndex == 0):
            logger.info("the first hypothesis is empty for utterance {0}".format(uttLabel))
            outputHandle.write("{0}\n".format(uttLabel))
            return
        if utterance:
            if labelWithIndex not in dictAmCost:
                raise Exception("unidentified label {0}".format(labelWithIndex))
            listAmScore.append(dictAmCost[labelWithIndex])
            listUtterance.append(utterance)
            listLabel.append(labelWithIndex)
    realNbest = min(len(listUtterance), effective_nbest)
    logger.debug('realNbest={0}'.format(realNbest))
    curNbest= len(listUtterance)
    if realNbest < curNbest:
        x = realNbest
        while x < curNbest:
            del listUtterance[-1]
            del listAmScore[-1]
            del listLabel[-1]
            x += 1
    logger.debug('length of list utterance is {0}, effective_nbest={1}'.format(len(listUtterance), effective_nbest))
    listLmScore = list()
    if not args.lmlogprob_file:
        listLmScore = LmScoring(args, listUtterance)
        if args.save_lmlogprob_file:
            if len(listLabel) != len(listLmScore):
                raise Exception("listLabel length {0} <> list lm score length {1}".format(len(listLabel), len(listLmScore)))
            for nIndex in range(0, len(listLabel)):
                dictLmLogprob[listLabel[nIndex]] = listLmScore[nIndex]
    else:
        for label in listLabel:
            if label not in dictLmLogprob:
                raise Exception("utterance {0} is not in dictLmLogprob".format(label))
            listLmScore.append(dictLmLogprob[label])

    if len(listLmScore) != len(listAmScore):
        raise Exception("LM list length is {0} <> AM list length {1}".format(len(listLmScore), len(listAmScore)))
    bestScore = acoustic_scale*float(listAmScore[0]) + lmscale*float(listLmScore[0]) + len(listUtterance[0])*word_penalty
    logger.info('best score is {0}, acoustic score is {1}, lm score is {2}'.format(bestScore, listAmScore[0], listLmScore[0]))
    bestIndex = 0
    for nIndex in range(1, len(listAmScore)):
        curScore = acoustic_scale*float(listAmScore[nIndex]) + lmscale*float(listLmScore[nIndex])  +  len(listUtterance[nIndex])*word_penalty
        logger.info('current score is {0}, acoustic score is {1}, lm score is {2}'.format(curScore, listAmScore[nIndex], listLmScore[nIndex]))
        if( curScore > bestScore):
            bestScore = curScore
            bestIndex = nIndex
    logger.info("bestIndex = {0}, effective_nbest={1}, acoustic_scale={2},  bestScore = {3}, wpen={4}, utterance = {5} {6}".format(bestIndex, len(listLmScore), acoustic_scale, bestScore, word_penalty,  uttLabel, listUtterance[bestIndex]))
    outputHandle.write("{0} {1}\n".format(uttLabel, listUtterance[bestIndex]))
        
def ProcessNBest(args, outputHandle, dictAmCost, dictLmLogprob):
    sPrevUttLabel = ''
    uttList = list()
    with gzip.open(args.nbest_trans_file, 'r') as inputFile:
        for sLine in inputFile:
            logger.debug('utterance is {0}'.format(sLine))
            sLine = sLine.strip()
            myList = sLine.split()
            sUttLabel = re.sub('\-\d+$', '', myList[0])
            if sPrevUttLabel != sUttLabel:
                if sPrevUttLabel:
                    OutputBest(args, outputHandle, sPrevUttLabel, uttList, dictAmCost, dictLmLogprob)
                    uttList[:] = []
            sPrevUttLabel = sUttLabel
            words = ' '.join(myList)
            uttList.append(words)
    if uttList:
        OutputBest(args, outputHandle, sPrevUttLabel,  uttList, dictAmCost, dictLmLogprob)

def LoadLmLogProbFile(lmlogprob_file, dictLmLogprob):
    try:
        with open(lmlogprob_file,'r') as f:
            for Lmline in f:
                myLmlist = Lmline.split()
                if len(myLmlist) != 2:
                    raise Exception("abnormal line {0}".format(Lmline))
                if myLmlist[0] in dictLmLogprob:
                    raise Exception("duplicated line '{0}' in '{1}'".format(Lmline, lmlogprob_file))
                dictLmLogprob[myLmlist[0]] = myLmlist[1]            
    except IOError:
        raise Exception("cannot open file {0}".format(lmlogprob_file))

def SaveLmLogProbFile(lmlogprob_file, dictLmLogprob):
    try:
        outputFile = open(lmlogprob_file, 'w')
        for utterance in dictLmLogprob:
            outputFile.write("{0} {1}\n".format(utterance, dictLmLogprob[utterance]))
    except IOError:
        raise Exception("cannot open file {0} to write".format(lmlogprob_file))
    outputFile.close()
def main():
    args = get_args()
    amcost_dict = dict()
    LoadAmCostFile(args.amcost_file, amcost_dict)
    dictLmLogprob = dict()
    if args.lmlogprob_file:
        LoadLmLogProbFile(args.lmlogprob_file, dictLmLogprob)
    outputFile = args.best_res_file
    outputHandle = open(outputFile, 'w')
    try:
        ProcessNBest(args, outputHandle,  amcost_dict, dictLmLogprob)
    except IOError:
        raise Exception("cannot open file {0}".format(outputFile))
    outputHandle.close()
    if args.save_lmlogprob_file:
        SaveLmLogProbFile(args.save_lmlogprob_file, dictLmLogprob)
if __name__ == "__main__":
    main()
