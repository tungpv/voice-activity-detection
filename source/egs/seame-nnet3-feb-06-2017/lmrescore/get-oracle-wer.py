#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu

import sys
import os
import re
import numpy as np
import logging

oracleWer = np.zeros(shape=(7))
# print ("oracleWer={0}".format(oracleWer))
for line in sys.stdin:
    line = re.sub('total=|oracle=', '', line)
    line = re.sub('\.', 'dot', line)
    line = re.sub(r'[\[\],]', '', line)
    line = re.sub('dot', '.', line)
    listWer = [w for w in line.split() if w.strip()]
    listWer = map(float, listWer)
    # print ("listWer={0}".format(listWer))
    oracleWer = np.add(oracleWer, listWer)
print ("oracleWer={0}".format(oracleWer))
print ("%OWER={0:.2f}".format(100*oracleWer[2]/oracleWer[3]))
