#! /bin/bash

echo 
echo "LOG: $0 $@"
echo

. path.sh
. cmd.sh 

# begin options
steps=
cmd='slurm.pl --quiet'
nj=40
# end options

function Usage {
 cat<<EOF

 [Examples ]: $0 --steps 1  data/train-augment-overall/combined \
 /data/users/tungpham/Add_reverb/Haihua_Feb15/out_dir data/rev-with-augmented-data

EOF
}

. parse_options.sh || exit 1

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

srcdata=$1
wavdir=$2
tgtdata=$3


steps=$(echo $steps | perl -e '$steps=<STDIN>; if($steps =~ m:(\d+)\-:g){$start = $1; $steps=$start; 
        for($i=$start+1; $i < $start + 10; $i++){$steps .=":$i"; } } print $steps;')
## echo "## LOG ($0): steps=$steps"  && exit 0
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
tmpdir=$tgtdata/temp
[ -d $tmpdir ] || mkdir -p $tmpdir

if [ ! -z  $step01 ]; then
  find $wavdir -name "*.wav" > $tmpdir/wavlist.txt
  echo "## LOG (step01, $0): wavlist.txt generated '$tmpdir'"
fi

tgtwav=$tgtdata/wavlink
[ -d $tgtwav ] ||  mkdir -p $tgtwav
if [ ! -z $step02 ]; then
  for x in $(cat $tmpdir/wavlist.txt); do
     wavname=$(echo $x | perl -pe 'chomp; s/.*\///g; $_= "rev-" . $_;')
     (cd $tgtwav; [ -h $wavname ] || ln -s $x $wavname)
     newwavfile=$(cd $tgtwav; pwd)/$wavname
     echo $newwavfile
  done  | \
  perl -pe 'chomp; $label = $_; $label =~ s/.*\///g; $label =~ s/\.wav//g; $_= "$label $_\n";' |\
  sort -u > $tgtdata/wav.scp
  echo "## LOG (step02, $0): done"
fi

if [ ! -z $step03 ]; then
  cat $srcdata/segments | \
  source/egs/seame-nnet3-feb-06-2017/prepare-rev-data/make_segments.pl | \
  sort -u > $tgtdata/segments
fi

if [ ! -z $step04 ]; then
  cat $tgtdata/segments | \
  source/egs/seame-nnet3-feb-06-2017/prepare-rev-data/make_utt2spk_from_segments.pl | \
  sort -u > $tgtdata/utt2spk
  utils/utt2spk_to_spk2utt.pl < $tgtdata/utt2spk > $tgtdata/spk2utt
  cat $srcdata/text | \
  source/egs/seame-nnet3-feb-06-2017/prepare-rev-data/rewrite-text.pl > $tgtdata/text
  source/egs/kws2016/georgian/reco2file_and_channel.sh $tgtdata
  utils/fix_data_dir.sh  $tgtdata
fi


if [ ! -z $step05 ]; then
  for sdata  in data/rev-with-augmented-data; do
    data=$sdata/mfcc-pitch; feat=$data/feat/mfcc-pitch; log=$data/log/mfcc-pitch
    utils/copy_data_dir.sh $sdata $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1 
    utils/fix_data_dir.sh $data
    echo "## LOG (step05, $0): done with '$data' generation @ `date`"
    data=$sdata/mfcc-hires; feat=$data/feat/mfcc-pitch; log=$data/log/mfcc-pitch
    utils/copy_data_dir.sh $sdata $data 
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
  done
  echo "## LOG (step05, $0): done with feature generation @ `date`"
fi
if [ ! -z $step06 ]; then
  data=data/train-multicondition/mfcc-pitch
  utils/combine_data.sh $data data/train-augment-overall/combined/mfcc-pitch \
  data/rev-with-augmented-data/mfcc-pitch
  echo "## LOG (step06, $0): done with mfcc-pitch combination"
  data=data/train-multicondition/mfcc-hires
  utils/combine_data.sh $data data/train-augment-overall/combined/mfcc-hires \
  data/rev-with-augmented-data/mfcc-hires
fi
sdir=exp-with-data-augmentation/tri4a
alidir=$sdir/ali-data-multicondition
if [ ! -z $step07 ]; then
  data=data/train-multicondition/mfcc-pitch
  steps/align_fmllr.sh --nj $nj --cmd "$cmd" \
  $data  data/lang $sdir   $alidir || exit 1
fi
expdir=exp-with-multicondition
if [ ! -z $step08 ]; then
  data=data/train-multicondition/mfcc-pitch
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1-7 \
  --alidir $alidir \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 8000 --pdf-num 100000 \
  --devdata data/dev-sge-dominant/mfcc-pitch \
  $data data/lang $expdir  || exit 1
  echo "## LOG (step08, $0): gmm training done @ `date`"
fi

min_utt_len=5.0
if [ ! -z $step09 ]; then
  for sdata in data/train-multicondition/mfcc-pitch data/train-multicondition/mfcc-hires; do
    data=data/train-multicondition/subset-to-train-ivector/$(basename $sdata);
    [ -d $data ] || mkdir -p $data 
    echo "## LOG (step09, $0): segments=$sdata/segments"
    cat $sdata/segments | \
    awk -v thresh=$min_utt_len '{ if($4-$3 >= thresh){print $1 ;} }' > $data/utt-list
    subset_data_dir.sh --utt-list $data/utt-list $sdata $data
  done
fi

# make alignment
sdir=$expdir/tri4a
data=data/train-multicondition/subset-to-train-ivector/mfcc-pitch
alidir=$sdir/ali-combined-subset-to-train-ivector
if [ ! -z $step10 ]; then
  steps/align_fmllr.sh --nj $nj --cmd "$cmd" \
  $data  data/lang $sdir   $alidir || exit 1
  echo "## LOG (step10, $0): done with doing alignment @ `date`"
fi
data=data/train-multicondition/subset-to-train-ivector/mfcc-hires
nnetdir=$expdir/tdnn
transform_dir=$nnetdir/lda-mllt-transform
if [ ! -z $step11 ]; then
  steps/train_lda_mllt.sh --cmd "$cmd"  --num-iters 13 \
  --realign-iters "" \
  --splice-opts "--left-context=3 --right-context=3" \
  7000 10000 $data  data/lang \
  $alidir $transform_dir || exit 1
  echo "## LOG (step11, $0): done with lda-mllt-transform ('$transform_dir') @ `date`"
fi

if [ ! -z $step12 ]; then
  steps/online/nnet2/train_diag_ubm.sh --cmd "$cmd" --nj $nj --num-threads 2 \
  --num-frames 500000 $data  512  $transform_dir $nnetdir/diag_ubm || exit 1
  echo "## LOG (step12, $0): done with diag_ubm ('$nnetdir/diag_ubm') @ `date`"
fi
ivector_extractor=$nnetdir/extractor
if [ ! -z $step13 ]; then
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$cmd" --nj $nj \
  --num-threads 1 --num-processes 1 \
  $data  $nnetdir/diag_ubm $nnetdir/extractor || exit 1;
  echo "## LOG (step13, $0): done with ivector-extractor training ('$nnetdir/extractor') @ `date`"
fi

train_ivectors=$nnetdir/ivectors-train
if [ ! -z $step14 ]; then
  data=data/train-multicondition/mfcc-hires
  data2=data/train-multicondition/mfcc-hires-max2
  steps/online/nnet2/copy_data_dir.sh --utts-per-spk-max 2 $data \
  $data2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj \
  $data2 $ivector_extractor $train_ivectors || exit 1;
  echo "## LOG (step14, $0): done with train-ivectors generation ('$train_ivectors') @ `date`"
fi
if [ ! -z $step15 ]; then
  rm $nnetdir/.error 2>/dev/null
  for dataName in  dev-man-dominant dev-sge-dominant; do
    data=data/$dataName/mfcc-hires
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj 8 \
    $data $ivector_extractor $nnetdir/ivectors-${dataName} || touch exp/nnet3/.error
    echo "## LOG (step15, $0): done with ivector generations with data ('$nnetdir/ivectors-${dataName}') @ `date`"
   done
fi




