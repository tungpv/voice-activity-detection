#!/bin/bash

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd=cmd.sh
nj=40
steps=
train_stage=-10
get_egs_stage=-10
frames_per_eg=150
minibatch_size=128
num_epochs=4
num_jobs_initial=3
num_jobs_final=16
remove_egs=false
max_param_change=2.0
initial_effective_lrate=0.001
final_effective_lrate=0.0001
experiment_name=tdnn_7l
disabled=false
# end options

. parse_options.sh || exit 1

function Usage  {
 cat<<EOF

 Example: $0 --steps 1 --disabled $disabled  --train-stage $train_stage \
 --experiment-name $experiment_name \
 --get-egs-stage $get_egs_stage \
 --frames-per-eg $frames_per_eg \
 --minibatch-size $minibatch_size \
 --num-epochs $num_epochs \
 --num-jobs-initial $num_jobs_initial \
 --num-jobs-final $num_jobs_final \
 --remove-egs $remove_egs \
 --max-param-change $max_param_change \
 --initial-effective-lrate $initial_effective_lrate \
 --final-effective-lrate $final_effective_lrate \
 --cmd 'slurm.pl --quiet --exclude=node01' --nj $nj  /home2/hhx502/seame/data \
 /home3/hhx502/w2017/seame-nnet3-feb-06-2017

EOF
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

srcdir=$1
tgtdir=$2
steps=$(echo $steps | perl -e '$steps=<STDIN>; if($steps =~ m:(\d+)\-:g){$start = $1; $steps=$start; 
        for($i=$start+1; $i < $start + 10; $i++){$steps .=":$i"; } } print $steps;')

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
datadir=$tgtdir/data
[ -d $datadir ] || mkdir -p $datadir
if [ ! -z $step01 ]; then
 ( cd $datadir 
   [ -L ./lang ] || ln -s $srcdir/lang-sil lang 
   [ -L ./train ] || ln -s $srcdir/train-sil train 
   [ -L ./dev-man-dominant ] || ln -s $srcdir/high-dev dev-man-dominant
   [ -L ./dev-sge-dominant ] || ln -s $srcdir/low-dev  dev-sge-dominant
 )
fi
expdir=$tgtdir/exp
langdir=$datadir/lang
if [ ! -z $step02 ]; then
  echo "## LOG ($0, step02): training started @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1,2,3,4,5,6,7 \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 5000 --pdf-num 100000 \
  --devdata $datadir/dev-sge-dominant/mfcc-pitch \
  $datadir/train/mfcc-pitch $langdir  $expdir || exit 1
  echo "## LOG ($0, step02): done @ `date`"
fi

# step03 - step10 for ivector training
sourceTrain=$datadir/train/mfcc-pitch
ivectorTrain=$datadir/ivectorTrain
min_sec_of_utt=5.0
if [ ! -z $step03 ]; then
  echo "## LOG ($0, step03): for data preparation  for ivector-training @ `date`"
  [ -d $ivectorTrain ] || mkdir -p $ivectorTrain
  cat $sourceTrain/segments | \
  awk -v thresh=$min_sec_of_utt '{ if($4-$3 >= thresh){print $1 ;} }' > $ivectorTrain/utt-list
  subset_data_dir.sh --utt-list $ivectorTrain/utt-list $sourceTrain $ivectorTrain
  rm $ivectorTrain/{feats.scp,cmvn.scp}
    
  echo "## LOG ($0, step03): done @ `date`"
fi

if [ ! -z $step04 ]; then
  echo "## LOG ($0, step04): doing data perturbance @ `date`"
  for data in $ivectorTrain; do
     utils/perturb_data_dir_speed.sh 0.9 $data $data/temp0.9
     utils/perturb_data_dir_speed.sh 1.1 $data $data/temp1.1
     utils/combine_data.sh $data/temp-combined $data/temp0.9 $data/temp1.1
     utils/validate_data_dir.sh --no-feats $data/temp-combined
     rm -r $data/{temp0.9,temp1.1}
    
     mfccdir_pert=$data/mfcc_perturbed
     [ -d $mfccdir_pert ] || mkdir -p $mfccdir_pert
     cp  $data/temp-combined/*  $mfccdir_pert/
     steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
     $mfccdir_pert $mfccdir_pert/log $mfccdir_pert/data || exit 1;
     steps/compute_cmvn_stats.sh $mfccdir_pert $mfccdir_pert/log $mfccdir_pert/data || exit 1;
     utils/fix_data_dir.sh $mfccdir_pert
    
     mfccdir=$data/mfcc_original
     [ -d $mfccdir ] || mkdir -p $mfccdir
     cp $data/*  $mfccdir
     steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
     $mfccdir $mfccdir/log $mfccdir/data || exit 1;
     steps/compute_cmvn_stats.sh $mfccdir $mfccdir/log $mfccdir/data || exit 1;
     utils/fix_data_dir.sh $mfccdir
     mfccdir_all=$data/mfcc_merge_sp
     utils/copy_data_dir.sh --spk-prefix sp1.0- --utt-prefix sp1.0- $mfccdir $data/mfcc_orig_copy
     utils/combine_data.sh $mfccdir_all $mfccdir_pert  $data/mfcc_orig_copy
     utils/fix_data_dir.sh $mfccdir_all
     rm -r $data/mfcc_orig_copy
     echo "## (step04): loop for $data done @ `date`"
  done 
  echo "## LOG ($0, step04): done with data perturbance @ `date`"
fi
traindata=$ivectorTrain/mfcc_merge_sp

if [ ! -z $step05 ]; then
  for dataName in dev-man-dominant dev-sge-dominant ivectorTrain; do
    srcdata=$tgtdir/data/$dataName
    data=$srcdata/mfcc-hires
    feat=$data/feat log=$data/log
    [ -d $data ] || mkdir -p $data
    utils/copy_data_dir.sh $srcdata $data
    steps/make_mfcc.sh --nj 40 --mfcc-config conf/mfcc_hires.conf \
    --cmd "$cmd" $data $log $feat || exit 1;
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;     
  done
fi
sourceMfcc=$sourceTrain
subMfcc=$datadir/train/sub-mfcc-pitch
if [ ! -z $step06 ]; then
  echo "## LOG ($0, step06): for data preparation  for alignment @ `date`"
  [ -d $subMfcc ] || mkdir -p $subMfcc
  cat $sourceMfcc/segments | \
  awk -v thresh=$min_sec_of_utt '{ if($4-$3 >= thresh){print $1 ;} }' > $subMfcc/utt-list
  subset_data_dir.sh --utt-list $subMfcc/utt-list $sourceMfcc $subMfcc
  echo "## LOG ($0, step03): done @ `date`"
fi

trainData=$datadir/ivectorTrain/mfcc-hires
lang=$datadir/lang
sourcedir=$tgtdir/exp/tri4a
alidir=$sourcedir/ali-to-train-lda-mllt-transform
if [ ! -z $step07 ]; then
  steps/align_fmllr.sh --nj $nj --cmd "$cmd" \
  $subMfcc  $lang $sourcedir   $alidir
fi
transform_dir=exp/tri5b
if [ ! -z $step08 ]; then
  echo # Train a small system just for its LDA+MLLT transform.  We use --num-iters 13
  echo # because after we get the transform (12th iter is the last), any further
  echo # training is pointless.
  steps/train_lda_mllt.sh --cmd "$cmd" --num-iters 13 \
  --realign-iters "" \
  --splice-opts "--left-context=3 --right-context=3" \
  5000 10000 $trainData $lang \
  $alidir $transform_dir
fi
dir=exp/nnet3
if [ ! -z $step09 ]; then
  steps/online/nnet2/train_diag_ubm.sh --cmd "$cmd" --nj $nj --num-threads 2 \
  --num-frames 400000 $trainData  512  $transform_dir $dir/diag_ubm
fi

if [ ! -z $step10 ]; then
    # even though $nj is just 10, each job uses multiple processes and threads.
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$cmd" --nj $nj \
  --num-threads 1 --num-processes 1 \
  $trainData  exp/nnet3/diag_ubm exp/nnet3/extractor || exit 1;
fi

trainData2=$datadir/ivectorTrain/mfcc-hires-max2
train_ivectors=exp/nnet3/ivectors_train
if [ ! -z $step11 ]; then
  steps/online/nnet2/copy_data_dir.sh --utts-per-spk-max 2 $trainData \
  $trainData2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj $nj \
  $trainData2 exp/nnet3/extractor $train_ivectors || exit 1;
fi

if [ ! -z $step12 ]; then
  rm exp/nnet3/.error 2>/dev/null
  for dataName in  dev-man-dominant dev-sge-dominant; do
    data=$tgtdir/data/$dataName/mfcc-hires
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj 8 \
    $data exp/nnet3/extractor exp/nnet3/ivectors-${dataName} || touch exp/nnet3/.error
   done
fi
traindata2=$datadir/train/mfcc-hires-max2
train_ivectors=exp/nnet3/ivectors-train
if [ ! -z $step13 ]; then
  for dataName in train; do
    srcdata=$tgtdir/data/$dataName/mfcc-pitch
    data=$srcdata/mfcc-hires
    feat=$data/feat log=$data/log
    [ -d $data ] || mkdir -p $data
    utils/copy_data_dir.sh $srcdata $data
    steps/make_mfcc.sh --nj 40 --mfcc-config conf/mfcc_hires.conf \
    --cmd "$cmd" $data $log $feat || exit 1;
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1; 
    data2=$srcdata/mfcc-hires-max2
    ivectors=exp/nnet3/ivectors-${dataName}
    steps/online/nnet2/copy_data_dir.sh --utts-per-spk-max 2 $data \
  $data2
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj \
  $data2 exp/nnet3/extractor $ivectors || exit 1; 
    
  done
fi
# start to train chain models
latticedir=exp/tri4a/ali-lats-train
if [ ! -z $step20 ]; then
  echo "## LOG ($0, step20): data preparation @ `date`"
 # Get the alignments as lattices (gives the LF-MMI training more freedom).
  # use the same num-jobs as the alignments
  nj=$(cat exp/tri4a/ali_train/num_jobs) || exit 1;
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $tgtdir/data/train/mfcc-pitch \
  $lang exp/tri4a  $latticedir || exit 1
  rm $latticedir/fsts.*.gz # save space
fi
chainLang=$tgtdir/data/lang-chain
if [ ! -z $step21 ]; then
  echo "## LOG ($0, step21): create lang for chain model"
  # Create a version of the lang/ directory that has one state per phone in the
  # topo file. [note, it really has two states.. the first one is only repeated
  # once, the second one has zero or more repeats.]
  rm -rf $chainLang
  cp -r $lang $chainLang
  silphonelist=$(cat $chainLang/phones/silence.csl) || exit 1;
  nonsilphonelist=$(cat $chainLang/phones/nonsilence.csl) || exit 1;
  # Use our special topology... note that later on may have to tune this
  # topology.
  steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >$chainLang/topo
  echo "## LOG ($0, step21): done @ `date`"
fi

alidir=exp/tri4a/ali_train
leftmost_questions_truncate=-1
treedir=exp/nnet3/tri5a_tree
if [ ! -z $step22 ]; then
  echo "## LOG ($0, step22): build tree for chain model"
  # Build a tree using our new topology. This is the critically different
  # step compared with other recipes.
  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
      --leftmost-questions-truncate $leftmost_questions_truncate \
      --context-opts "--context-width=2 --central-position=1" \
      --cmd "$cmd" 7000 data/train/mfcc-pitch $chainLang $alidir $treedir
fi
# here, we want to investigate how performance is affected with alignment
# To do this, we use nnet1 to produce higher qualified alignment, then we 
# use such alginment to build better tree, hopefully.
if ! $disabled; then
alidir=/home2/hhx502/seame/exp/nnet5a/dnn/ali-train-for-nnet3
if [ ! -z $step40 ] ;then
  echo "## LOG (step40, $0): align @ `date`"
  steps/nnet/align.sh --cmd "$cmd" --nj $nj \
  data/train/fbank-pitch data/lang $(dirname $alidir) $alidir || exit 1
  echo "## LOG (step40, $0): done @ `date`"
fi
treedir=exp/nnet3/tri5a-tree-with-nnet1
if [ ! -z $step41 ]; then
  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
      --leftmost-questions-truncate $leftmost_questions_truncate \
      --context-opts "--context-width=2 --central-position=1" \
      --cmd "$cmd" 7000 data/train/mfcc-pitch $chainLang $alidir $treedir
fi
fi
dir=exp/nnet3/chain/$experiment_name
xent_regularize=0.1
[ -d $dir ] || mkdir -p $dir
if [ ! -z $step23 ]; then
  echo "## LOG ($0, step23): make nnet3 topology @ `date`"
    echo "$0: creating neural net configs using the xconfig parser";

  num_targets=$(tree-info $treedir/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)

  mkdir -p $dir/configs
  cat <<EOF > $dir/configs/network.xconfig
  input dim=100 name=ivector
  input dim=40 name=input
  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor
  fixed-affine-layer name=lda input=Append(-1,0,1,ReplaceIndex(ivector, t, 0)) affine-transform-file=$dir/configs/lda.mat
  # the first splicing is moved before the lda layer, so no splicing here
  relu-renorm-layer name=tdnn1 dim=625
  relu-renorm-layer name=tdnn2 input=Append(-1,0,1) dim=625
  relu-renorm-layer name=tdnn3 input=Append(-1,0,1) dim=625
  relu-renorm-layer name=tdnn4 input=Append(-3,0,3) dim=625
  relu-renorm-layer name=tdnn5 input=Append(-3,0,3) dim=625
  relu-renorm-layer name=tdnn6 input=Append(-3,0,3) dim=625
  relu-renorm-layer name=tdnn7 input=Append(-3,0,3) dim=625
  ## adding the layers for chain branch
  relu-renorm-layer name=prefinal-chain input=tdnn7 dim=625 target-rms=0.5
  output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5
  # adding the layers for xent branch
  # This block prints the configs for a separate output that will be
  # trained with a cross-entropy objective in the 'chain' models... this
  # has the effect of regularizing the hidden parts of the model.  we use
  # 0.5 / args.xent_regularize as the learning rate factor- the factor of
  # 0.5 / args.xent_regularize is suitable as it means the xent
  # final-layer learns at a rate independent of the regularization
  # constant; and the 0.5 was tuned so as to make the relative progress
  # similar in the xent and regular final layers.
  relu-renorm-layer name=prefinal-xent input=tdnn7 dim=625 target-rms=0.5
  relu-renorm-layer name=prefinal-lowrank-xent input=prefinal-xent dim=64 target-rms=0.5
  output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF
  steps/nnet3/xconfig_to_configs.py --xconfig-file $dir/configs/network.xconfig --config-dir $dir/configs/
fi
if [ ! -z $step30 ]; then
  echo "## LOG ($0, step30): make nnet3 topology @ `date`"
    echo "$0: creating neural net configs using the xconfig parser";

  num_targets=$(tree-info $treedir/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)

  mkdir -p $dir/configs
  cat <<EOF > $dir/configs/network.xconfig
  input dim=40 name=input
  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor
  fixed-affine-layer name=lda input=Append(-1,0,1) affine-transform-file=$dir/configs/lda.mat
  # the first splicing is moved before the lda layer, so no splicing here
  relu-renorm-layer name=tdnn1 dim=625
  relu-renorm-layer name=tdnn2 input=Append(-1,0,1) dim=625
  relu-renorm-layer name=tdnn3 input=Append(-1,0,1) dim=625
  relu-renorm-layer name=tdnn4 input=Append(-3,0,3) dim=625
  relu-renorm-layer name=tdnn5 input=Append(-3,0,3) dim=625
  relu-renorm-layer name=tdnn6 input=Append(-3,0,3) dim=625
  relu-renorm-layer name=tdnn7 input=Append(-3,0,3) dim=625
  ## adding the layers for chain branch
  relu-renorm-layer name=prefinal-chain input=tdnn7 dim=625 target-rms=0.5
  output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5
  # adding the layers for xent branch
  # This block prints the configs for a separate output that will be
  # trained with a cross-entropy objective in the 'chain' models... this
  # has the effect of regularizing the hidden parts of the model.  we use
  # 0.5 / args.xent_regularize as the learning rate factor- the factor of
  # 0.5 / args.xent_regularize is suitable as it means the xent
  # final-layer learns at a rate independent of the regularization
  # constant; and the 0.5 was tuned so as to make the relative progress
  # similar in the xent and regular final layers.
  relu-renorm-layer name=prefinal-xent input=tdnn7 dim=625 target-rms=0.5
  relu-renorm-layer name=prefinal-lowrank-xent input=prefinal-xent dim=64 target-rms=0.5
  output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF
  steps/nnet3/xconfig_to_configs.py --xconfig-file $dir/configs/network.xconfig --config-dir $dir/configs/
fi
common_egs_dir=
if [ ! -z $step24 ]; then
  echo "## LOG ($0, step24): started chain nnet training @ `date`"
  steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$cmd" \
    --feat.online-ivector-dir exp/nnet3/ivectors-train \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize $xent_regularize \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --egs.dir "$common_egs_dir" \
    --egs.stage $get_egs_stage \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width $frames_per_eg \
    --trainer.num-chunk-per-minibatch $minibatch_size \
    --trainer.frames-per-iter 1500000 \
    --trainer.num-epochs $num_epochs \
    --trainer.optimization.num-jobs-initial $num_jobs_initial \
    --trainer.optimization.num-jobs-final $num_jobs_final \
    --trainer.optimization.initial-effective-lrate $initial_effective_lrate \
    --trainer.optimization.final-effective-lrate $final_effective_lrate \
    --trainer.max-param-change $max_param_change \
    --cleanup.remove-egs $remove_egs \
    --feat-dir data/train/mfcc-hires-cleaned \
    --tree-dir $treedir \
    --lat-dir $latticedir \
    --dir $dir  || exit 1;

  echo "## LOG ($0, step24): done @ `date`"
fi

if [ ! -z $step31 ]; then
  echo "## LOG ($0, step31): started chain nnet training @ `date`"
  steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$cmd" \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize $xent_regularize \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --egs.dir "$common_egs_dir" \
    --egs.stage $get_egs_stage \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width $frames_per_eg \
    --trainer.num-chunk-per-minibatch $minibatch_size \
    --trainer.frames-per-iter 1500000 \
    --trainer.num-epochs $num_epochs \
    --trainer.optimization.num-jobs-initial $num_jobs_initial \
    --trainer.optimization.num-jobs-final $num_jobs_final \
    --trainer.optimization.initial-effective-lrate $initial_effective_lrate \
    --trainer.optimization.final-effective-lrate $final_effective_lrate \
    --trainer.max-param-change $max_param_change \
    --cleanup.remove-egs $remove_egs \
    --feat-dir data/train/mfcc-hires-cleaned \
    --tree-dir $treedir \
    --lat-dir $latticedir \
    --dir $dir  || exit 1;

  echo "## LOG ($0, step24): done @ `date`"
fi

if [ ! -z $step25 ]; then
  utils/mkgraph.sh --self-loop-scale 1.0 data/lang $dir $dir/graph
fi

if [ ! -z $step26 ]; then
  for dataName in dev-man-dominant dev-sge-dominant; do
    decode_set=data/$dataName/mfcc-hires
    steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
          --nj 10 --cmd "$cmd" \
          --online-ivector-dir exp/nnet3/ivectors-${dataName} \
          $dir/graph $decode_set $dir/decode-${dataName} || exit 1;
  done 
fi

