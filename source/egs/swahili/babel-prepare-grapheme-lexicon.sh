#!/bin/bash

. path.sh
. cmd.sh

# begin options
trainlist=
# end options

. parse_options.sh

if [ $# -ne 2 ]; then
  echo
  echo "Usage: $(basename $0) <trans_dir> <localdir>"
  echo && exit 1
fi

trans_dir=$1
dir=$2
fileNum=$(ls $trans_dir/*.txt 2>/dev/null|wc -l)
if [ ! $fileNum -gt 0 ]; then
  echo "ERROR, no txt file found in folder $trans_dir" && exit 1
fi
[ -d $dir ] || mkdir -p $dir
if [ -z $trainlist ]; then
  ls $trans_dir/*.txt | \
  source/egs/swahili/babel_make_raw_trans.pl  | \
  source/egs/swahili/babel_print_word_list.pl  | sort -u | \
  egrep -v '[<\(-]' | \
  source/egs/swahili/babel_split_word.pl |\
  sort -u >$dir/grapheme_lexicon.txt
else
  ls $trans_dir/*.txt | \
  perl -e '$trainList = shift @ARGV; open(F, "$trainList") or die "file $trainList cannot open";
    while(<F>) {
      chomp; $vocab{$_} ++;
    } close F;
    while(<STDIN>) {
      chomp;
      $line = $_;
      $line =~ s/.*\///g; $line =~ s/\.txt//g;
      if(exists $vocab{$line}) { print "$_\n"; }
    }' $trainlist | \
  source/egs/swahili/babel_make_raw_trans.pl  | \
  source/egs/swahili/babel_print_word_list.pl  | sort -u | \
  egrep -v '[<\(-]' | \
  source/egs/swahili/babel_split_word.pl |\
  sort -u >$dir/grapheme_lexicon.txt
fi
