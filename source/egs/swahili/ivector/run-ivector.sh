#!/bin/bash

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo 

# begin options
steps=
cmd=slurm.pl
nj=40

# end options
function Usage {
cat<<END

 Usage: $0 [options] <data> <dataName> <mfcc.conf> <ivector-extractor-dir> <tgtdir>
 [options]:
 [steps]:
 [examples]:

  $0 --steps 1 kws2016/vllp-grapheme/data/dev dev source/egs/swahili/ivector/mfcc_20.conf \
  /home2/hhx502/extractor_mfcc_2048_GenderMix \
  /home2/hhx502/kws2016/exp-ivector-swahili

END
}
. parse_options.sh || exit 1

if [ $# -ne 5 ]; then
  Usage && exit 1
fi

source_data=$1
source_data_name=$2
mfcc_conf=$3
ivector_srcdir=$4
tgtdir=$5

[ -d $tgtdir ] || mkdir -p $tgtdir

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
data=$tgtdir/data/$source_data_name
data_mfcc=$tgtdir/data/$source_data_name/mfcc
featdir=$data/feat/mfcc
feat=$featdir/data; log=$featdir/log

if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): make mfcc feat @ `date` "
  [ -d $data_mfcc ] || mkdir -p $data_mfcc
  cp $source_data/* $data_mfcc/
  steps/make_mfcc.sh --mfcc-config $mfcc_conf  --nj $nj --cmd "$cmd" \
  $data_mfcc $log $feat || exit 1
  echo "## LOG (step01, $0): done ('$data') @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): make vad @ `date`"
  source/egs/swahili/ivector/sre08-sid/compute_vad_decision.sh --nj $nj \
  --cmd "$cmd" \
  $data_mfcc  $log $feat || exit 1
  echo "## LOG (step02, $0): done @ `date`"
fi
trap "echo '## LOG ($0): removeing speaker_data $speaker_data ...'; rm -rf $data/mfcc_speaker" EXIT
speaker_data=$data/mfcc_speaker/data
speaker_feat=$data/mfcc_speaker/feat
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): make speaker data @ `date`"
  source/egs/swahili/splice-feats-by-speaker.sh --cmd "$cmd" --splice-opts "--overall-merge=true" \
  --logdir $data/mfcc_speaker/log \
  $data_mfcc $speaker_data $speaker_feat || exit 1
  echo "## LOG (step03, $0): done @ `date`"
fi
 ivector_data=$data/ivector
if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): ivector extraction @ `date`"
  source/egs/swahili/ivector/extract_ivectors.sh --cmd "$cmd" --nj $nj \
  $ivector_srcdir $speaker_data  $ivector_data || exit 1
  echo "## LOG (step04, $0): done @ `date`"
fi
