#!/usr/bin/perl
use warnings;
use strict;

use Getopt::Long;
my $from=1;
my $left_to_right="true";

my $usage = <<END ;
[options]:
--from			: from which field to map (default = $from)
--left-to-right		: to do left-to-right map if true (default = $left_to_right)
END
GetOptions("from=s" => \$from,
	   "left-to-right=s" => \$left_to_right);
if($from <= 0) {
  die "ERROR, from($from) should be over zero\n";
} 
my $str = $left_to_right;
$left_to_right = lc $left_to_right;
if ($left_to_right ne "false" && $left_to_right ne "true" ) {
  die "ERROR, illegal left_to_right options $str\n";
}
sub MapSym {
  my ($vocab, $sym, $sym_mapped) = @_;
  $$sym_mapped = $sym;
  if (not exists $$vocab{$sym}) {
    print STDERR "WARNING: sym ($sym) is not mapped \n";
  } else {
    $$sym_mapped = $$vocab{$sym};
  } 
}
my $cmdName = $0;
$cmdName =~ s/.*\///g;
if (@ARGV != 1) {
  print STDERR "\n\nExample:cat text | $cmdName [options] <lexicon.txt> > mapped_text\n";
  die "$usage\n\n";
}

my ($lex_fname) = @ARGV;
my $file_handle;
open($file_handle, "<$lex_fname") or die "ERROR, file $lex_fname cannot open\n";
my %vocab = ();
while(<$file_handle>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  my ($left_str, $right_str) = ($1, $2);
  if ($left_to_right eq "true") {
    $vocab{$left_str} = $right_str;
  } else {
    $vocab{$right_str} = $left_str;
  }
}
close $file_handle;

$from --;
print STDERR "$0: stdin expected ...\n";
while(<STDIN>) {
  chomp;
  my @A = split(" ");
  my $s = "";
  my $i = 0;
  for($i = 0; $i < $from; $i++) {
    $s .= "$A[$i] ";
  }
  my $s_mapped;
  my $j = $i;
  for(my $j = $i; $j + 1 < @A; $j++) {
    MapSym(\%vocab, $A[$j], \$s_mapped);
    $s .= "$s_mapped "; 
  }
  MapSym(\%vocab, $A[$j], \$s_mapped);
  $s .= "$s_mapped";
  print  "$s\n";
}
print STDERR "$0: stdin ended !\n";
