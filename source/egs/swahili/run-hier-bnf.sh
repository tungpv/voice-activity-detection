#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=slurm.pl
nj=40
steps=

validating_rate=0.1
train_bn1_cmd="steps/nnet/train.sh --hid-dim 1500 --hid-layers 2 --learn-rate 0.008 --bn-dim 80 --feat-type traps --splice 5"
h1_copy_cmd="nnet-copy --remove-last-layers=4 --binary=false"
h1_splice_cmd="utils/nnet/gen_splice.py --fea-dim=80 --splice=2 --splice-step=5 |"
train_bn2_cmd="steps/nnet/train.sh --hid-layers 2 --hid-dim 1500 --bn-dim 30 --learn-rate 0.008"
# end options

echo
echo LOG: $0 $@
echo

. parse_options.sh || exit 1

function PrintOptions {
  cat<<END
[options]:
--cmd 				# value, "$cmd"
--nj				# value, "$nj"
--steps				# value, "$steps"

--validating-rate		# value, "$validating_rate"
--train_bn1_cmd			# value, "$train_bn1_cmd"
--h1-copy-cmd			# value, "$h1_copy_cmd"
--h1-splice-cmd			# value, "$h1_splice_cmd"
--train-bn2-cmd			# value, "$train_bn2_cmd"

[steps]:
1: prepare data
2: train bn first
3: make feat transform for bn second
4: train bn second
END
}

if [ $# -ne 4 ]; then
  echo
  echo "Usage: $(basename $0) [options] <data> <lang> <alidir> <dir>"
  PrintOptions
  echo && exit 1
fi

data=$1
lang=$2
alidir=$3
dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
train=$dir/train
valid=$dir/valid
if [ ! -z $step01 ]; then
  echo "making data to train @ `date`"
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $train \
  $data  $valid || exit 1
  echo "Done @ `date`"
fi
hierbn1_dir=$dir/h1
if [ ! -z $step02 ]; then
  echo "hierbn1 training started @ `date`"
  $cmd $dir/hierbn1.log $train_bn1_cmd  \
  --cmvn-opts "--norm-means=true" \
  --delta_opts "--delta-order=2" \
  $train $valid $lang $alidir $alidir $hierbn1_dir || exit 1
  echo "Done @ `date`"
fi

feat_transform=$hierbn1_dir/final.feature_transform.bn1
if [ ! -z $step03 ]; then
  echo "making feature transform from bn1 started @ `date`"
  hierbn_copy_cmd="$h1_copy_cmd $hierbn1_dir/final.nnet - |"
  nnet-concat --binary=false $hierbn1_dir/final.feature_transform \
  "$h1_copy_cmd $hierbn1_dir/final.nnet - |" "$h1_splice_cmd"  $feat_transform || exit 1
  echo "Done @ `date`"
fi
hierbn2_dir=$dir/h2
if [ ! -z $step04 ]; then
  echo "hierbn2 training started @ `date`"
  $cmd $dir/hierbn2.log $train_bn2_cmd  \
  --cmvn-opts "--norm-means=true" \
  --feature-transform $feat_transform \
  $train $valid $lang $alidir $alidir $hierbn2_dir || exit 1;
  echo "Done @ `date`"
fi
