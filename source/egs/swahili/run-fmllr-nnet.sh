#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=40
cmd="slurm.pl --exclude=node01,node02"
steps=

validating_rate=0.1
learn_rate=0.008
cmvn_opts="--norm-means=true"
gmmdir=
nnet_train_cmd=source/egs/train.sh
nnet_pretrain_cmd="steps/nnet/pretrain_dbn.sh --copy_feats_tmproot /local/hhx502 --splice 5 --nn-depth 5 --hid-dim 2048"
nnet_id=5

devdata=
devname=dev
trainname=train
graphname=graph
decodename=decode_dev
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15 "
# end options

echo
echo LOG: $0 $@
echo

. parse_options.sh || exit 1

function PrintOptions {
  cat <<END
[options]:
--nj				# value, $nj
--cmd				# value, "$cmd"
--steps				# value, "$steps"

--validating_rate		# value, $validating_rate
--learn-rate			# value, $learn_rate
--cmvn_opts			# value, "$cmvn_opts"
--gmmdir			# value, "$gmmdir"
--nnet-train-cmd		# value, "$nnet_train_cmd"
--nnet-pretrain-cmd		# value, "$nnet_pretrain_cmd"
--nnet-id			# value, "$nnet_id"

--devdata			# value, "$devdata"
--devname			# value, "$devname"
--trainname			# value, "$trainname"
--graphname			# value, "$graphname"
--decodename			# value, "$decodename"
--decode-opts			# value, "$decode_opts"
--scoring-opts			# value, "$scoring_opts"

 [steps]:
 1: train dnn  
   2: prepare data 
   3: pretraining
   4: training
 5: test dev data, if any
END
}

if [ $# -ne 4 ]; then
  echo
  echo "Usage: $(basename $0) [options] <data> <lang> <alidir> <dir>"
  PrintOptions
  echo && exit 1
fi

data=$1
lang=$2
alidir=$3
dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -z $gmmdir ] && gmmdir=$(dirname $alidir)
if [ ! -z $step01 ]; then
  echo "nnet training started @ `date`"
  source/run-nnet.sh  --usefmllrdata true \
  ${nnet_id:+--nnet-id $nnet_id} \
  --labeldir $alidir \
  --preTrnCmd "$nnet_pretrain_cmd" \
  ${cmvn_opts:+--cmvn_opts "$cmvn_opts"} \
  ${step02:+--PrepareData true} \
  ${step03:+--PreTrain true } \
  ${step04:+--TrainNnet true} --trnCmd "$nnet_train_cmd" --sdir $gmmdir \
  $lang $data  $dir || exit 1

  echo "ended @ `date`"
fi
nnetsdir=$dir/nnet$nnet_id
graphdir=$gmmdir/$graphname
if [ ! -z $step05 ] && [ ! -z $devdata ]; then
  echo "decoding started @ `date`"
  source/egs/swahili/decode.sh --cmd "$cmd" --nj $nj \
  --steps 1,2 \
  --scoring-opts "$scoring_opts" \
  --decode-opts "$decode_opts" \
  $devdata $lang $graphdir $nnetsdir/$decodename || exit 1
  echo "ended @ `date`"
fi

echo "Done !"
