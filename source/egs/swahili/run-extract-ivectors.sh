#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=40
cmd="slurm.pl"
steps=
apply_spk_cmvn=true
raw_feat=false
cmvn_opts=
num_gselect=20 
min_post=0.025 
posterior_scale=1.0 
tempdir_prefix=
ivector_dir=
# end options
echo
echo "$0 $@"
echo

. parse_options.sh || exit 1

function Usage {
  cat<<END

 Usage: $(basename $0) [option] <extractor-dir> <source-data> <dataname>
 [options]:
 --nj                                   # value, $nj
 --cmd                                  # value, "$cmd"
 --steps                                # value, "$steps"
 --apply-spk-cmvn                       # value, $apply_spk_cmvn
 --raw-feat                             # value, $raw_feat
 --cmvn-opt                             # value, "$cmvn_opts"
 --num-gselect                          # value, $num_gselect
 --min-post                             # value, $min_post
 --posterior-scale                      # value, $posterior_scale
 --tempdir_prefix                       # value, "$tempdir_prefix"
 --ivector-dir                          # value, "$ivector_dir"

 [steps]:
 1: convert utterance based feature stream into speaker based feature stream
 2: ivector extraction

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

srcdir=$1
data=$2
dataname=$3

tempdir_prefix=$(pwd)
dir=$srcdir/$dataname
[ ! -z $ivector_dir ] && dir=$ivector_dir

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

tempdir=$(mktemp -d -p $tempdir_prefix)
[ -d $tempdir ] ||\
{ echo "ERROR, making tempdir $tempdir failed"; exit 1; }
trap " echo \"removing dir $tempdir ...\"; rm -rf $tempdir; " EXIT
data_spk=$tempdir/data
feat_spk=$tempdir/feat
if [ ! -z $step01 ]; then
  echo "splice-feats-by-speaker started @ `date`"
  source/egs/swahili/splice-feats-by-speaker.sh --cmd "$cmd" \
  --logdir $dir/log \
  $data $data_spk $feat_spk || exit 1
  echo "Done @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "ivector extraction started"
  for x in feats.scp vad.scp;  do
    [ -e $data_spk/$x ] || \
    { echo "ERROR, file $x expected in dir $data_spk"; exit 1; }
  done
  source/egs/swahili/extract_ivectors.sh --cmd "$cmd" \
  --nj $nj \
  --apply-spk-cmvn $apply_spk_cmvn \
  --raw-feat $raw_feat \
  $srcdir $data_spk $dir || exit 1
  echo "Done @ `date`"
fi
