#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=run.pl
# end options

. parse_options.sh || exit 1
function Usage {
  cat<<END

$(basename $0) [options] <logdir>
[options]:
--cmd                                 # value, "$cmd"

END
}
if [ $# -ne 1 ]; then
  Usage && exit 1;  
fi

logdir=$1

perl -e '
  ($cmd, $file) = @ARGV;
  open(F, "$file") or die "ERROR, file $file cannot open\n";
  while(<F>) {
    chomp; 
    if(/\(code\s+(\d+)\)/) {
      $num = $1;
      if($num != 0) {
        $logfile = $_;  $logfile =~ s/log:\#.*/log/g;       
        $newlogfile = $logfile; $newlogfile =~ s/(\d+).log/$1\.RE\.log/;
        my $taskline = "head -1 $logfile |";
        open(LOG, "$taskline") or die "ERROR, taskline \"$taskline\" cannot open\n";
        my $taskcmd = <LOG>; $taskcmd =~ s/^#//;  chomp $taskcmd; 
        print "$taskcmd >$newlogfile 2>&1 &\n";
        close LOG;
      }
    }
  }
  close F; 
' "$cmd" "grep Ended $logdir/decode.* |"  > $logdir/batch.sh

bash $logdir/batch.sh
