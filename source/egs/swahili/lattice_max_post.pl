#!/usr/bin/perl
use warnings;
use strict;

print STDERR "\n\nLOG: $0 ", join(" ", @ARGV), "\n";
## dump utterance to the standard output
sub DumpUtterance {
  my ($vocab) = @_;
  foreach my $utt (keys %$vocab) {
    my $vocab_ptr01 = $$vocab{$utt};
    foreach my $wordId (keys %$vocab_ptr01) {
      my $vocab_ptr02 = $$vocab_ptr01{$wordId};
      foreach my $start (keys %$vocab_ptr02) {
        my $vocab_ptr03 = $$vocab_ptr02{$start};
        foreach my $end (keys %$vocab_ptr03) {
          print $$vocab_ptr03{$end}, "\n";
        }
      }
    } 
  }
}
## insert a word into the vocab
sub InsertWord {
  my ($vocab, $sLine) = @_;
  my @A = split(/[\s\t]+/, $sLine);
  my ($utt, $wordId, $start, $end) = ($A[0], $A[1], $A[2], $A[3]);
  if(exists $$vocab{$utt}{$wordId}{$start}{$end}) {
    my $saved = $$vocab{$utt}{$wordId}{$start}{$end};
    my @B = split(/[\s\t]+/, $saved);
    if($A[6] > $B[6]) {
      $$vocab{$utt}{$wordId}{$start}{$end} = $sLine;
    }
  } else {
    $$vocab{$utt}{$wordId}{$start}{$end} = $sLine;
  }
}
print STDERR "stdin expected\n";
my %vocab = ();
my $prev_utt = "";
while(<STDIN>) {
  chomp;
  my @A  = split(/[\s\t]+/);
  if($prev_utt ne "" && $prev_utt ne $A[0]) {
    DumpUtterance(\%vocab);
    %vocab = ();
  }
  InsertWord(\%vocab, $_);
  $prev_utt = $A[0];
}
DumpUtterance(\%vocab);
print STDERR "stdin ended \n";
