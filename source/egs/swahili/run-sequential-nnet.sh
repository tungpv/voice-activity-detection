#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=40
cmd=slurm.pl
train_cmd="slurm.pl --gres=gpu:1"
steps=
gmm_like_cmd=
acwt=0.0909
conf=conf/decode_dnn.config
train_ivector_data=
append_vector_to_feats=
append_dec_ivector=
denLatId=
devdata=
trainname=train
graphdir=
decodename=decode_dev
decode_cmd=steps/nnet/decode.sh
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15"
# end options

echo
echo LOG: $0 $@
echo

. parse_options.sh || exit 1

function Usage {
  cat <<END

 $(basename $0) [options] <data> <lang> <srcdir> <dir>
 [options]:
 --nj                           # value, $nj
 --cmd                          # value, "$cmd"
 --train-cmd                    # value, "$train_cmd"
 --steps                        # value, "$steps"

 --gmm-like-cmd                 # value, "$gmm_like_cmd"
 --acwt                         # value, $acwt
 --conf                         # value, "$conf"
 --train-ivector-data           # value, "$train_ivector_data"
 --append-vector-to-feats       # value, "$append_vector_to_feats"
 --append-dec-ivector           # value, "$append_dec_ivector"
 --denLatId                     # value, "$denLatId"
 --devdata                      # value, "$devdata"
 --trainname                    # value, "$trainname"
 --graphdir                     # value, "$graphdir"
 --decodename                   # value, "$decodename"
 --decode-cmd                   # value, "$decode_cmd"
 --decode-opts                  # value, "$decode_opts"
 --scoring-opts                 # value, "$scoring_opts"
 
 [steps]:
 1: make alignment  
 2: make lattice 
 3: sequential training
 4: test dev data, if any
 [examples]:

 $0 --steps 1,2,3,4 --cmd slurm.pl --train-cmd "run.pl" \
 --devdata /home2/hhx502/sge2017/data/fstd-demo-updated/8k/fbank-pitch \
 --graphdir /home2/hhx502/sge2017/exp/update-oct-27-2016/8k/tri4a/graph\
 /home2/hhx502/sge2017/data/train-mix431/8k/fbank-pitch /home2/hhx502/sge2017/data/update-oct-27-2016/lang \
 /home2/hhx502/sge2017/exp/update-oct-27-2016/8k/nnet5a-tl-train291 /home2/hhx502/sge2017/exp/update-oct-27-2016/8k/mpe-nnet5a-tl-train291

 $0 --steps 1,2,3,4 --devdata  /home/hhx502/w2016/kws2016/flp-grapheme-phone/data/dev/fbank_pitch \
 --graphdir /home/hhx502/w2016/kws2016/flp-grapheme-phone/exp/tri4a/graph \
 /home/hhx502/w2016/kws2016/flp-grapheme-phone/data/train/fbank_pitch   \
 /home/hhx502/w2016/kws2016/flp-grapheme-phone/data/lang  /home/hhx502/w2016/kws2016/flp-grapheme-phone/exp/nnet5a-mxl \
 /home/hhx502/w2016/kws2016/flp-grapheme-phone/exp/nnet5a-mxl-smbr

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
srcdir=$3
dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
use_gmm_likes=
if [ ! -z "$gmm_like_cmd" ]; then
  use_gmm_likes=true
fi
alidir=$srcdir/ali-${trainname}
if [ ! -z $step01 ]; then
  echo "making alignment started @ `date`"
  max_spk=$(wc -l < $data/spk2utt)
  echo "## LOG: max_spk=$max_spk"
  [ $nj -gt $max_spk ] && nj=$max_spk
  steps/nnet/align.sh --cmd "$cmd" --nj $nj \
  ${train_ivector_data:+--ivector $train_ivector_data} \
  ${append_vector_to_feats:+--append-vector-to-feats "$append_vector_to_feats"} \
  $data $lang $srcdir $srcdir/ali-${trainname} || exit 1
  echo "ended @ `date`"
fi
latdir=$srcdir/denlat-${trainname}${denLatId}
if [ ! -z $step02 ]; then
  echo "making lattice started @ `date`"
  max_spk=$(wc -l < $data/spk2utt)
  [ $nj -gt $max_spk ] && nj=$max_spk
  steps/nnet/make_denlats.sh --cmd "$cmd" --nj $nj \
  ${train_ivector_data:+--ivector $train_ivector_data} \
  ${append_vector_to_feats:+--append-vector-to-feats "$append_vector_to_feats"} \
  --acwt $acwt --config $conf $data $lang $srcdir $latdir || exit 1 
  echo "ended @ `date`"
fi

if [ ! -z $step03 ]; then
  echo "sequential training started @ `date`"
  steps/nnet/train_mpe.sh --cmd "$train_cmd"  --acwt $acwt --do-smbr true \
  ${train_ivector_data:+--ivector $train_ivector_data} \
  ${append_vector_to_feats:+--append-vector-to-feats "$append_vector_to_feats"} \
  $data $lang $srcdir $alidir $latdir $dir || exit 1
  echo "ended @ `date`"
fi

if [ ! -z $step04 ]; then
  echo "Decoding started @ `date`"
  max_spk=$(wc -l < $devdata/spk2utt)
  [ $nj -gt $max_spk ] && nj=$max_spk
  [ -f $graphdir/HCLG.fst ] || \
  { echo "ERROR, HCLG.fst expected from $graphdir"; exit 1; }
  $decode_cmd ${append_dec_ivector:+--append-vector-to-feats "$append_dec_ivector"} --cmd "$cmd" --nj $nj \
  ${use_gmm_likes:+--gmm-like-cmd "$gmm_like_cmd"} \
  --scoring-opts "$scoring_opts" \
  $decode_opts $graphdir $devdata $dir/$decodename || exit 1
fi


echo "Done !"
