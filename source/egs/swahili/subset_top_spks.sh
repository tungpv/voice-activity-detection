#!/bin/bash

. path.sh
. cmd.sh

# begin options
hour_length=0
# end options

. parse_options.sh || exit 1

function Usage {
  cat<<END

Usage:
  $(basename $0) [options] <sdata> <data>
[options]:
--hour-length                      # value, $hour_length

END
}
if [ $# -ne 2 ]; then
  Usage && exit 1
fi
sdata=$1
data=$2
if [ $(echo "$hour_length > 0" | bc -l) -eq 0 ]; then
  echo "Selected hour length should be over zero ($hour_length)" && exit 1
fi
for x in segments utt2spk; do
  [ -f $sdata/$x ] || \
  { echo "ERROR, file $sdata/$x expected"; exit 1; }
done
tempdir=$(mktemp -d )
trap "echo removing $tempdir ...; rm -rf $tempdir; rm $dir/perplexities" EXIT
if [ $hour_length -gt 0 ]; then
  source/egs/swahili/subset_top_spks.pl $sdata/segments $sdata/utt2spk $hour_length > $tempdir/uttlist
  utils/subset_data_dir.sh --utt-list $tempdir/uttlist $sdata $data || exit 1
fi
