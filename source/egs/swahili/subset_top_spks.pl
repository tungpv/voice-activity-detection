#!/usr/bin/perl
use strict;
use warnings;

my $usage =<<END;

  $0  <segments> <utt2spk> <hours>

END

if (@ARGV != 3) {
  die $usage;
}
my ($segfile, $utt2spk, $hours) = @ARGV;
if ($hours <= 0) {
  die "ERROR, no data will be selected, due to hours=$hours\n";
}
sub make_utt2spk_spk2utt_map {
  my ($utt2spk_vocab, $spk2utt_vocab, $utt2spk) = @_;
  open(F, "$utt2spk") or die "ERROR, file $utt2spk cannot open\n";
  while (<F>) {
    chomp;
    my @A = split(/\s+/);
    $$utt2spk_vocab{$A[0]} = $A[1];
    if (exists $$spk2utt_vocab{$A[1]}) {
      $$spk2utt_vocab{$A[1]} .= " $A[0]";
    } else {
      $$spk2utt_vocab{$A[1]} = $A[0];
    }
  }
  close F;
}
sub make_utt2dur_map {
  my ($utt2spk_vocab, $spk2dur_vocab, $segfile) = @_;
  open(F, "$segfile") or die "ERROR, file $segfile cannot open\n";
  while(<F>) {
    chomp;
    my @A = split(/\s+/);
    @A == 4 or die "ERROR, bad line $_\n in file $segfile\n";
    my $dur = $A[3] - $A[2];
    die "ERROR, utt \"$A[0]\" is not in utt2spk file" if not exists $$utt2spk_vocab{$A[0]};
    my $spk = $$utt2spk_vocab{$A[0]};
    if (exists $$spk2dur_vocab{$spk}) {
      $$spk2dur_vocab{$spk} += $dur;
    } else {
      $$spk2dur_vocab{$spk} = $dur;
    }
  }
  close F;
}
my %utt2spk_vocab = ();
my %spk2utt_vocab = ();
make_utt2spk_spk2utt_map(\%utt2spk_vocab, \%spk2utt_vocab, $utt2spk);
my %spk2dur_vocab = ();  
make_utt2dur_map(\%utt2spk_vocab, \%spk2dur_vocab, $segfile);
my $required_sec = $hours * 3600;
my $total_sec = 0; 
my $selected_sec = 0;
for my $spk (sort { $spk2dur_vocab{$b}<=> $spk2dur_vocab{$a} } keys %spk2dur_vocab) {
  my $s = $spk2utt_vocab{$spk};
  $s =~ s/\s+/\n/g;
  $total_sec += $spk2dur_vocab{$spk};
  if($total_sec <= $required_sec + 360) {
    print $s ."\n";
    $selected_sec = $total_sec;
  }
}
my $s = sprintf("%.2f", $selected_sec/3600);
print STDERR "total selected hour: ", $s, "\n";


