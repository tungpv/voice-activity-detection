#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=slurm.pl
nj=40
steps=
apply_cmvn="apply-cmvn-sliding --norm-vars=false --center=true --cmn-window=300 ark:- ark:-"
apply_spk_cmvn=true
raw_feat=false
splice_opts="--min-samples=1500 --max-samples=4000 --times=1.0 --overall-merge=false"
utt2spk=
cmvn_opts="--norm-vars=false"
num_components=2048
ivector_dim=100
num_iters=5
extractor_dirname=extractor
ivec_extractor_dir=
ivec_feat_dir=
# end options
echo 
echo LOG: $0 $@
echo

. parse_options.sh || exit 1

function PrintOptions {
  cat <<END
 $(basename $0) [options] <data> <dataname>  <dir>
 [options]:
 --cmd				# value, "$cmd"
 --nj				# value, $nj
 --steps                        # value, "$steps"
 --apply-cmvn                   # value, "$apply_cmvn"
 --apply-spk-cmvn               # value, "$apply_spk_cmvn"
 --raw-feat                     # value, $raw_feat
 --splice-opts                  # value, "$splice_opts"
 --utt2spk                      # value, "$uttspk"
 --cmvn-opts                    # value, "$cmvn_opts"
 --num-components		# value, $num_components
 --ivector-dim			# value, $ivector_dim
 --num-iters			# value, $num_iters
 --extractor-dirname            # value, "$extractor_dirname"
 --ivec-extractor-dir           # value, "$ivec_extractor_dir"
 --ivec-feat-dir                # value, "$ivec_feat_dir"
[steps]:
1: merge data by spkr
2: train diag ubm
3: train full ubm
4: train ivector extractor
5: extract ivector for training data

END
}
if [ $# -ne 3 ]; then
  echo
  PrintOptions && exit 1
fi

data=$1
dataname=$2
dir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

tempdir_prefix=$(pwd)
tempdir=$(mktemp -d -p $tempdir_prefix)
[ -d $tempdir ] ||\
{ echo "ERROR, making tempdir $tempdir failed"; exit 1; }
trap " echo \"removing dir $tempdir ...\"; rm -rf $tempdir; " EXIT
if [ ! -z "$utt2spk" ]; then
  splice_opts="$splice_opts $utt2spk"
  tgtdir=$(dirname $(echo $utt2spk | perl -pe 's/.*\=//;'))
  if [ -z $tgtdir ]; then
    echo "ERROR, tgtdir is empty" && exit 1
  fi
  [ -d $tgtdir ] || mkdir -p $tgtdir
fi
if [ ! -z $step01 ]; then
  echo "splice-feats-by-speaker started @ `date`"
  data_spk=$tempdir/data
  feat_spk=$tempdir/feat
  source/egs/swahili/splice-feats-by-speaker.sh --cmd "$cmd" \
  --logdir $dir/log \
  --splice-opts "$splice_opts" \
  $data $data_spk $feat_spk || exit 1
  data=$data_spk
  echo "Done @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "train_diag_ubm started @ `date`"
  sid/train_diag_ubm.sh --nj $nj --cmd "$cmd" \
  --apply-cmvn "$apply_cmvn" \
  --raw-feat $raw_feat \
  --apply-spk-cmvn $apply_spk_cmvn --cmvn-opts "$cmvn_opts" \
  $data $num_components \
  $dir/diag_ubm_$num_components  || exit 1
  echo "Done @ `date`"
fi

if [ ! -z $step03 ]; then
  echo "train_full_ubm started @ `date`"
  sid/train_full_ubm.sh --nj $nj --apply-cmvn "$apply_cmvn" \
  --apply-spk-cmvn $apply_spk_cmvn --cmvn-opts "$cmvn_opts" \
  --raw-feat $raw_feat \
  --remove-low-count-gaussians false \
  --cmd "$cmd" $data  \
  $dir/diag_ubm_$num_components $dir/full_ubm_$num_components || exit 1
  echo "Done @ `date`"
fi
[ -z $ivec_extractor_dir ] && ivec_extractor_dir=$dir/$extractor_dirname
if [ ! -z $step04 ]; then
  echo "train_ivector_extractor started @ `date`"
  sid/train_ivector_extractor.sh --cmd "$cmd" \
  --apply-spk-cmvn $apply_spk_cmvn --cmvn-opts "$cmvn_opts" \
  --apply-cmvn "$apply_cmvn" \
  --raw-feat $raw_feat \
  --ivector-dim $ivector_dim \
  --num-iters $num_iters $dir/full_ubm_$num_components/final.ubm $data \
  $ivec_extractor_dir || exit 1
  echo "$cmvn_opts" > $dir/extractor/cmvn_opts
  echo "Done @ `date`"
fi
[ -z $ivec_feat_dir ] && ivec_feat_dir=$ivec_extractor_dir/$dataname
if [ ! -z $step05 ]; then
  echo "extract_ivectors started @ `date`"
  for x in feats.scp vad.scp;  do
    [ -e $data/$x ] || \
    { echo "ERROR, file $x expected in dir $data"; exit 1; }
  done
  source/egs/swahili/extract_ivectors.sh --cmd "$cmd" --nj $nj \
  --apply-spk-cmvn $apply_spk_cmvn  --raw-feat $raw_feat \
  $ivec_extractor_dir $data $ivec_feat_dir || exit 1
  echo "Done @ `date`"
fi

echo "Done"
