#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd="slurm.pl"
steps=1,2

validating_rate=0.1
gmmdir=

learn_rate=0.008
# end options

. parse_options.sh || exit 1

function PrintOptions {
  cat<<END
[options]:
--cmd			# value, "$cmd"
--steps			# value, "$steps"

--validating_rate	# value, $validating_rate
--gmmdir		# value, "$gmmdir"

--learn-rate		# value, $learn_rate

[steps]:
1: dividing data
2: tuning the nnet
END
} 

if [ $# -ne 5 ]; then
  echo
  echo "Usage: $(basename $0) [options] <data> <lang> <alidir> <sdir> <dir>"
  PrintOptions
  echo && exit 1
fi

data=$1
lang=$2
alidir=$3
sdir=$4
dir=$5

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $dir ] || mkdir -p $dir

train=$dir/train
valid=$dir/valid
if [ ! -z $step01 ]; then
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $train \
  $data  $valid || exit 1
fi 

if [ ! -z $step02 ]; then
  [ ! -z $gmmdir ]|| \
  { echo "ERROR, gmmdir not specified"; exit 1; }
  for x in $gmmdir/final.mdl $sdir/final.nnet; do
    [ -f $x ] || { echo "ERROR, file $x expected"; exit 1; }
  done 
  num_tgt=$(hmm-info --print-args=false $gmmdir/final.mdl | grep pdfs | awk '{ print $NF }')
  soft_max_proto=$sdir/soft-max.proto
  front_nnet="nnet-copy --remove-last-layers=2 $sdir/final.nnet -|" 
  hid_dim=$(nnet-info "$front_nnet" | \
                  grep output-dim | tail -1 |perl -pe 's/.*output-dim//g; m/(\d+)/g; $_=$1;') || \
  { echo "ERROR, hid_dim error"; exit 1; }
  utils/nnet/make_nnet_proto.py $hid_dim $num_tgt 0  $hid_dim >$soft_max_proto || exit 1 
  soft_max_init=$sdir/soft-max.init
  log=$sdir/sotf-max-initialize.log
  nnet-initialize $soft_max_proto $soft_max_init 2>$log || { cat $log; exit 1; }
  nnet_init=$dir/nnet.init
  nnet-concat "$front_nnet" $soft_max_init $nnet_init 2> $dir/nnet-init.log
  feature_transform=$sdir/final.feature_transform
  steps/nnet/train.sh --learn-rate $learn_rate --feature-transform $feature_transform \
  --hid-layers 0 --nnet-init $nnet_init $train $valid $lang $alidir \
  $alidir $dir || exit 1
  echo "Ended @ `date`"
fi
