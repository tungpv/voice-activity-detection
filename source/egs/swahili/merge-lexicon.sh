#!/bin/bash

. path.sh
. cmd.sh

# begin options

# end options

. parse_options.sh
if [ $# -ne 3 ]; then
  echo
  echo "Usage: $(basename $0) <src_lex01> <src_lex02> <dir>"
  echo && exit 1
fi

src_lex01=$1
src_lex02=$2
dir=$3

[ -d $dir ] || mkdir -p $dir
cat $src_lex01 $src_lex02 | \
perl -e '
  while(<STDIN>) {
    chomp;
    @A = split(/[ \t]/);
    $word = shift @A;
    $s = $word . "\t" . join(" ", @A);
    if(not exists $vocab{$s}) {
      $vocab{$s} ++;
      print $s, "\n";
    }
  }
' | grep -v '<' > $dir/lexicon_raw.txt


local/prepare_lexicon.pl --oov "<unk>"  $dir/lexicon_raw.txt $dir

