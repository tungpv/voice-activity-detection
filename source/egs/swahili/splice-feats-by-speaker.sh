#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=slurm.pl
remove_no_vad=false
splice_opts="--min-samples=1500 --max-samples=4000 --times=1.0 --overall-merge=false"
logdir=
# end options

. parse_options.sh || exit 1
function Usage {
  cat<<END

 $(basename $0) [options] <sdata> <data> <featdir>
 [options]:
 --cmd                   # value, "$cmd"
 --remove-no-vad         # value, $remove_no_vad
 --splice-opts           # value, "$splice_opts"
 --logdir                # value, "$logdir"
 [examples]:
  
 $0  --splice-opts "--overall-merge=true"  /home2/hhx502/kws2016/exp-ivector-swahili/data/dev/mfcc \
 /home2/hhx502/kws2016/exp-ivector-swahili/data/dev/mfcc_speaker \
 /home2/hhx502/kws2016/exp-ivector-swahili/data/dev/mfcc_speaker/feat

END
}
if [ $# -ne 3 ]; then
  Usage && exit 1
fi

sdata=$1
data=$2
featdir=$3

[ -d $data ] || mkdir -p $data
[ -d $featdir ] || mkdir -p $featdir
tempdir=$(mktemp -d -p $data)
trap  "echo removing $tempdir ...; rm -rf $tempdir;" EXIT
# remove those utterances that have no vad information
if $remove_no_vad; then
  subset_data_dir.sh --utt-list $sdata/vad.scp $sdata $tempdir
else
  cp $sdata/{spk2utt,feats.scp,vad.scp} $tempdir
fi
sdata=$tempdir
featdir=$(cd $featdir; pwd)
xlogdir=$featdir/log
[ ! -z $logdir ] && xlogdir=$logdir
$cmd $xlogdir/splice-feats-by-speaker.log \
source/code/splice-feats-by-speaker $splice_opts ark:$sdata/spk2utt  scp:$sdata/feats.scp \
scp:$sdata/vad.scp $data/utt2spk ark,scp:$featdir/feats.ark,$data/feats.scp \
ark,scp:$featdir/vad.ark,$data/vad.scp || exit 1

utils/utt2spk_to_spk2utt.pl < $data/utt2spk > $data/spk2utt
steps/compute_cmvn_stats.sh $data  $xlogdir $featdir/data || exit 1
