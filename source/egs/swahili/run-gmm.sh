#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=40
cmd="slurm.pl"
steps=

gmmid=a
trainname=train
pdfs=2000
mixtures=30000
cmvn_opts=
alidir=
lda_dim=40

devdata=
devname=dev
graphname=graph
decodename=decode_dev
decode_opts="--acwt 0.1 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 25"

# end options

echo
echo "LOG: $0 $@"

. parse_options.sh || exit 1

function Usage {
  cat <<END

Usage: $(basename $0) [options] <data> <lang> <dir>
[options]:
--nj				# value, $nj
--cmd				# value, "$cmd"
--steps				# value, "$steps"

--gmmid				# value, "$gmmid"
--trainname			# value, "$trainname"
--pdfs				# value, "$pdfs"
--mixtures			# value, "$mixtures"
--cmvn-opts                     # value, "$cmvn_opts"
--alidir			# value, "$alidir"
--lda_dim			# value, "$lda_dim"

--devdata			# value, "$devdata"
--devname			# value, "$devname"
--graphname			# value, "$graphname"
--decodename			# value, "$decodename"
--decode_opts			# value, "$decode_opts"
--scoring_opts			# value, "$scoring_opts"

[steps]:
1: train gmm 
2: test gmm, if any
3: make fmllr for training data
4: make fmllr for dev data, if any

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
dir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  echo "gmm training started @ `date`"
  source/egs/run-bnf.sh  --steps 1,2,3,4,5 --cmd "$cmd" --nj $nj --run_gmm2 true \
  ${cmvn_opts:+--cmvn-opts "$cmvn_opts"} \
  ${alidir:+--import-alidir $alidir} --lda-dim $lda_dim \
  --run_gmm2_opts \
  "$gmmid $data $trainname $lang $pdfs $mixtures $dir" || exit 1
  echo "ended @ `date`"
fi
graphdir=$dir/tri4$gmmid/$graphname
decode_dir=$dir/tri4$gmmid/$decodename
if [ ! -z $step02 ] && [ ! -z $devdata ]; then
  echo "gmm test started @ `date`"
  source/egs/swahili/decode.sh --cmd "$cmd" --nj $nj \
  --steps 1,2 \
  --scoring-opts "$scoring_opts" \
  --decode-opts "$decode_opts" \
  --decode-cmd "steps/decode_fmllr.sh" \
  $devdata $lang $graphdir $decode_dir || exit 1
  echo "ended @ `date`"
fi
srcdir=$dir/tri4$gmmid
if [ ! -z $step03 ]; then
  [ -f $srcdir/final.mdl ] || \
  { echo "ERROR, final.mdl expected"; exit 1; }
  source/egs/swahili/make_feats.sh  --cmd "$cmd" --make-fmllr true \
  --srcdir $srcdir --transform-dir $srcdir/ali_${trainname} $data  $srcdir/$trainname $srcdir/feats/$trainname || exit 1
fi
if [ ! -z $step04 ]; then
  [ -z $devdata ] && \
  { echo "ERROR, devdata expected"; exit 1; }
  [ -f $decode_dir/lat.1.gz ] || \
  { echo "ERROR, decoding is not ready"; exit 1; }
  source/egs/swahili/make_feats.sh  --cmd "$cmd" --make-fmllr true \
  --srcdir $srcdir --transform-dir $srcdir/$decodename $devdata  $srcdir/$devname $srcdir/feats/$devname || exit 1
fi
