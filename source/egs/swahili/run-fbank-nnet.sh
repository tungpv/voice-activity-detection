#!/bin/bash

. path.sh
. cmd.sh 

# begin options
nj=40
cmd="slurm.pl"
steps=

validating_rate=0.1
learn_rate=0.008
cmvn_opts="--norm-means=true"
delta_opts="--delta-order=2"
no_delta=false
valid_data_source=
gmmdir=
convert_ali=false
# nnet_train_cmd=source/egs/train.sh
nnet_train_cmd=steps/nnet/train.sh
# nnet_pretrain_cmd="source/egs/pretrain_dbn_trap.sh --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 5 --hid-dim 2048"
nnet_pretrain_cmd="steps/nnet/pretrain_dbn.sh --feat-type trap --copy_feats_tmproot $(pwd)  --splice 10 --nn-depth 5 --hid-dim 2048"
gmm_like_cmd=
ivector_train=
append_vector_to_feats=append-vector-to-feats
nnet_id=nnet5a

decode_cmd=steps/nnet/decode.sh
devdata=
append_ivector_dev=
trainname=train
graphname=graph
decodename=decode_dev
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15 "
# end options

echo
echo LOG: $0 $@
echo

. parse_options.sh || exit 1

function PrintOptions {
  cat <<END
[options]:
--nj                            # value, $nj
--cmd                           # value, "$cmd"
--steps                         # value, "$steps"

--validating_rate               # value, $validating_rate
--learn-rate                    # value, $learn_rate
--cmvn_opts                     # value, "$cmvn_opts"
--delta_opts                    # value, "$delta_opts"
--no-delta                      # value, $no_delta
--valid-data-source             # value, "$valid_data_source"
--gmmdir                        # value, "$gmmdir"
--convert_ali                   # value,  $convert_ali
--nnet-train-cmd                # value, "$nnet_train_cmd"
--nnet-pretrain-cmd             # value, "$nnet_pretrain_cmd"
--gmm-like-cmd                  # value, "$gmm_like_cmd"
--ivector_train                 # value, "$ivector_train"
--append-vector-to-feats        # value, "$append_vector_to_feats"
--nnet-id                       # value, "$nnet_id"

--decode_cmd                    # value, "$decode_cmd"
--devdata                       # value, "$devdata"
--append-ivector-dev            # value, "$append_ivector_dev"
--trainname                     # value, "$trainname"
--graphname                     # value, "$graphname"
--decodename                    # value, "$decodename"
--decode-opts                   # value, "$decode_opts"
--scoring-opts                  # value, "$scoring_opts"

 [steps]:
 1: prepare data 
 2: pretraining
 3: training
 4: make decoding graph, if any
 5: decode data, if any
END
}

if [ $# -ne 4 ]; then
  echo
  echo "Usage: $(basename $0) [options] <data> <lang> <alidir> <dir>"
  PrintOptions
  echo && exit 1
fi

data=$1
lang=$2
alidir=$3
dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if $convert_ali; then
  [ ! -z $gmmdir ] || \
  { echo "ERROR, you want to do alignment conversion, but your gmmdir is not specified"; exit 1; }
  source/egs/convert-ali.sh $alidir $gmmdir $gmmdir/$(basename $alidir)_converted
  alidir=$gmmdir/$(basename $alidir)_converted
fi

[ -z $gmmdir ] && gmmdir=$(dirname $alidir)
if $no_delta; then
  delta_opts=
fi
use_gmm_likes=
if [ ! -z "$gmm_like_cmd" ]; then
  use_gmm_likes=true
  if [ ! -f $gmmdir/final.mdl ]; then
    echo "ERROR, file $gmmdir/final.mdl expected" && exit 1
  fi
  gmm_like_cmd="$gmm_like_cmd $gmmdir/final.mdl ark:- ark:- |"
fi

nnetdir=$dir/$nnet_id
train=$nnetdir/train
valid=$nnetdir/valid
if [ ! -z $step01 ]; then
  if [ ! -z $valid_data_source ]; then
    for f in $valid_data_source/segments $data/segments; do
      [ -f $f ] || \
      { echo "ERROR, segments file $f expected"; exit 1; }
    done
    tot_hour=$(cat $data/segments| awk '{x+=$4-$3;}END{print x/3600;}')
    sel_hour=$(perl -e "print $tot_hour*$validating_rate")
    tot_hour=$(cat $valid_data_source/segments|awk '{x+=$4-$3;}END{print x/3600;}')
    new_time_ratio=$(perl -e "print $sel_hour/$tot_hour")
    echo "new_time_ration=$new_time_ratio"
    if [ $(echo "$new_time_ratio >= 1" | bc -l) -eq 1 ]; then
      cp -r $valid_data_source $valid
    else
      source/egs/swahili/subset_data.sh --subset_time_ratio $new_time_ratio \
      --random true \
      $valid_data_source  $valid || exit 1
    fi
    source/egs/swahili/subset_data.sh --exclude-uttlist $valid/segments $data $train || exit 1
  else
    source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
    --random true \
    --data2 $train \
    $data  $valid || exit 1
  fi
fi
pretrain_dir=$nnetdir/pretrain_dbn
if [ ! -z $step02 ]; then
  $nnet_pretrain_cmd ${cmvn_opts:+--cmvn_opts "$cmvn_opts"} ${delta_opts:+--delta-opts "$delta_opts"} \
  ${use_gmm_likes:+--gmm-like-cmd "$gmm_like_cmd"} \
   --rbm-iter 1 ${ivector_train:+--ivector $ivector_train} --append-vector-to-feats "$append_vector_to_feats"  $train  $pretrain_dir || exit 1;
fi
if [ ! -z $step03 ]; then
  hid_dim=$(echo "$nnet_pretrain_cmd" | perl -pe 'if(m/--hid-dim\s+(\d+)/){$_=$1;}else{exit 1;}')
  nn_depth=$(echo "$nnet_pretrain_cmd" | perl -pe 'if(m/--nn-depth\s+(\d+)/){$_=$1;}else{exit 1;}')
  dbn=$pretrain_dir/$nn_depth.dbn
  feature_transform=$pretrain_dir/final.feature_transform
  $nnet_train_cmd ${ivector_train:+--ivector $ivector_train} --append-vector-to-feats "$append_vector_to_feats"  --hid-dim $hid_dim \
  ${use_gmm_likes:+--gmm-like-cmd "$gmm_like_cmd"} \
  --feature-transform $feature_transform \
  --dbn $dbn \
  --hid-layers 0  \
  --learn-rate $learn_rate \
  $train $valid $lang $alidir \
  $alidir $nnetdir || exit 1; 
fi

graphdir=$gmmdir/$graphname
if [ ! -z $step04 ]; then
  echo "Making graph started @ `date`"
  srcdir=$(dirname $graphdir)
  [ -f $srcdir/final.mdl ] || \
  { echo "ERROR, final.mdl expected from srcdir $srcdir"; exit 1; }
  if [ ! -f $graphdir/HCLG.fst ]; then
    utils/mkgraph.sh $lang $srcdir  $graphdir
  fi
  echo "Done @ `date`"
fi
if [ ! -z $step05 ]; then
  echo "Decoding started @ `date`"
  effect_nj=$(wc -l < $devdata/spk2utt)
  [ $effect_nj -gt $nj ] && effect_nj=$nj
  [ -f $graphdir/HCLG.fst ] || \
  { echo "ERROR, HCLG.fst expected from $graphdir"; exit 1; }
  $decode_cmd ${append_ivector_dev:+--append-vector-to-feats "$append_ivector_dev"} --cmd "$cmd" --nj $effect_nj \
  ${use_gmm_likes:+--gmm-like-cmd "$gmm_like_cmd"} \
  --scoring-opts "$scoring_opts" \
  $decode_opts $graphdir $devdata $nnetdir/$decodename || exit 1
fi

echo "Done @ `date` !"
