#!/bin/bash

. path.sh
. cmd.sh 

# begin options
nj=40
cmd=slurm.pl
steps=

fbank_pitch=false
fbank_pitch_cmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
compute_fbank_feats=compute-fbank-feats
compute_kaldi_pitch_feats=compute-kaldi-pitch-feats
make_fbank=false
fbank_cmd="steps/make_fbank.sh --fbank-config conf/fbank40.conf"
plp_pitch=false
plp_pitch_cmd="steps/make_plp_pitch.sh --plp-config conf/plp.conf --pitch-config conf/pitch.conf"

make_fmllr=false
srcdir=
transform_dir=

make_bnf=false
make_bnf_opts="--use-gpu no"
bnf_extractor_dir=
append_bnf=false
append_data=

mfcc_for_ivector=false
mfcc_cmd="steps/make_mfcc.sh --mfcc-config conf/mfcc-sre.conf"

compute_vad_decision=false

borrow_energy_vad=false
dataname=unknown

# end options
echo
echo "LOG: $0 $@"
echo
. parse_options.sh || exit 1

function Usage {
 cat <<END

 Usage: $0 [options] <sdata> <data> <featdir>
 [options]:
 --nj                                   # value, $nj
 --cmd                                  # value, "$cmd"
 --step                                 # value, "$steps"

 --fbank-pitch                          # value, $fbank_pitch
 --fbank-pitch-cmd                      # value, "$fbank_pitch_cmd"
 --compute-fbank-feats                  # value, "$compute_fbank_feats"
 --compute-kaldi-pitch-feats            # value, "$compute_kaldi_pitch_feats"

 --make-fbank                           # value, $make_fbank
 --fbank-cmd                            # value, "$fbank_cmd"

 --plp-pitch                            # value, $plp_pitch
 --plp-pitch-cmd                        # value, "$plp_pitch_cmd"

 --make-fmllr                           # value, $make_fmllr
 --srcdir                               # value, "$srcdir"
 --transform-dir                        # value, "$transform_dir"

 --make_bnf                             # value, $make_bnf
 --make-bnf-opts                        # value, "$make_bnf_opts"
 --bnf-extractor-dir                    # value, "$bnf_extractor_dir"

 --append-bnf                           # value, $append_bnf
 --append-data                          # value, "$append_data"

 --mfcc-for-ivector                     # value, $mfcc_for_ivector
 --mfcc-cmd                             # value, "$mfcc_cmd"

 --compute-vad-decision                 # value, $compute_vad_decision
 --borrow-energy-vad                    # value, $borrow_energy_vad
 --dataname                             # value, "$dataname"

 [steps]:
 1: make mfcc features
 2: make vad features using ctm files

 [examples]:
 $0 --append-bnf true --append-data mandarin/data/train/fbank-pitch  mandarin/data/train/bnf mandarin/data/train/bnf-fbank-pitch  mandarin/data/train/feat/bnf-fbank-pitch

 $0 --make-bnf true --cmd run.pl --nj 1 --make-bnf-opts "--use-gpu yes --post-feature true" \
 --bnf-extractor-dir /home2/hhx502/seame/exp/lang-dnn-feat-extractor/dnn /home2/hhx502/seame/data/dev/fbank-pitch \
 /home2/hhx502/seame/data/dev/lpf-dnn /home2/hhx502/seame/data/dev/feat/lpf-dnn 

 $0 --append-bnf true --append-data /home2/hhx502/seame/data/train/lpf-dnn /home2/hhx502/seame/data/train/fbank-pitch \
 /home2/hhx502/seame/data/train/append-fbank-pitch-lpf-dnn /home2/hhx502/seame/data/train/feat/append-fbank-pitch-lpf-dnn 

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

sdata=$1
data=$2
featdir=$3

[ -d $data ] || mkdir -p $data
if $fbank_pitch; then
  echo "making fbank_pitch started @ `date`"
  cp $sdata/* $data/;   rm $data/{feats.scp,cmvn.scp,vad.scp} 2>/dev/null
  $fbank_pitch_cmd --compute-fbank-feats "$compute_fbank_feats" --compute-kaldi-pitch-feats "$compute_kaldi_pitch_feats" \
  --cmd "$cmd" --nj $nj $data $featdir/log $featdir/data  || exit 1
  steps/compute_cmvn_stats.sh $data  $featdir/log  $featdir/data || exit 1
  utils/fix_data_dir.sh $data
  echo "Ended @ `date`"
fi
if $make_fmllr; then
  sdir=$srcdir
  [ -z $sdir ] && sdir=$(dirname $data)
  [ -f $sdir/final.mdl ] || \
  { echo "ERROR, final.mdl expected from srcdir $sdir"; exit 1; }
  echo "making fmllr feats started @ `date`"
  nj_x=$(wc -l < $sdata/spk2utt)
  [ $nj_x -lt $nj ] && nj=$nj_x
  [ -z $transform_dir ] && \
  echo "WARNING: transform_dir is not provided"
  if [ ! -z $transform_dir ]; then
    nj=$(cat $transform_dir/num_jobs)
  fi
  steps/nnet/make_fmllr_feats.sh --cmd "$cmd" --nj $nj \
  ${transform_dir:+--transform-dir $transform_dir} \
  $data $sdata $sdir   $featdir/log $featdir/data || exit 1
  steps/compute_cmvn_stats.sh $data  $featdir/log  $featdir/data || exit 1
  utils/fix_data_dir.sh $data
  echo "Ended @ `date`"
fi
if $make_bnf; then
  echo "making bnf started @ `date`"
  [ ! -z $bnf_extractor_dir ] && [ -f $bnf_extractor_dir/final.nnet ] || \
  { echo "ERROR, bnf_extractor_dir is not specified or not ready"; exit 1; }
  source/egs/seame/make_bn_feats.sh --cmd "$cmd" --nj $nj $make_bnf_opts \
  $data $sdata $bnf_extractor_dir $featdir/log $featdir/data || exit 1
  steps/compute_cmvn_stats.sh $data $featdir/log $featdir/data || exit 1
  utils/fix_data_dir.sh $data
  echo "end @ `date`"
fi

if $make_fbank; then
  echo "started @ `date`"
  cp $sdata/* $data/;   rm $data/{feats.scp,cmvn.scp} 2>/dev/null
  $fbank_cmd  --cmd "$cmd" --nj $nj $data $featdir/log $featdir/data  || exit 1
  steps/compute_cmvn_stats.sh $data  $featdir/log  $featdir/data || exit 1
  utils/fix_data_dir.sh $data
  echo "ended @ `date`"
fi

if $plp_pitch; then
  echo "started @ `date`"
  cp $sdata/* $data/;   rm $data/{feats.scp,cmvn.scp} 2>/dev/null
  $plp_pitch_cmd --cmd "$cmd" --nj $nj \
  $data $featdir/log $featdir/data || exit 1
  steps/compute_cmvn_stats.sh $data $featdir/log  $featdir/data || exit 1
  utils/fix_data_dir.sh $data
  echo "ended @ `date`"
fi
if $mfcc_for_ivector; then
  cp $sdata/* $data ; rm $data/{feats.scp,cmvn.scp,vad.scp} 2>/dev/null
  $mfcc_cmd --nj $nj --cmd "$cmd" $data $featdir/log $featdir/data || exit 1
  steps/compute_cmvn_stats.sh $data  $featdir/log  $featdir/data || exit 1
fi

if $append_bnf; then
  [ -z $append_data ] && \
  { echo "ERROR, append_data not specified"; exit 1; }
  steps/append_feats.sh $sdata $append_data $data $featdir/log $featdir/data || exit 1
  steps/compute_cmvn_stats.sh $data $featdir/log $featdir/data || exit 1
  utils/fix_data_dir.sh $data
fi

if $compute_vad_decision; then
  sid/compute_vad_decision.sh --cmd "$cmd" --nj $nj $data $featdir/log $featdir/data || exit 1
fi

if $borrow_energy_vad; then
  [ -f $data/feats.scp ] || \
  { echo "ERROR, file $data/feats.scp expected"; exit 1; }
  [ -f $data/vad.scp ] && \
  { echo "ERROR, file vad.scp already exist in data dir $data"; exit 1; }
  vad_featdir=$(head -1 $data/feats.scp | awk '{print $2;}' | perl -pe 'use File::Basename; chomp; $_ = dirname($_);')
  src_vad_dir=$sdata
  if [ -z $src_vad_dir ] || [ ! -f $src_vad_dir/vad.scp ]; then
    echo "ERROR, src_vad_dir or vad.scp is not ready"; exit 1
  fi
  vad_logdir=$(dirname $vad_featdir)/log
  echo "vad_featdir=$vad_featdir"
  echo "vad_logdir=$vad_logdir"
  $cmd $vad_logdir/${dataname}_vad.log \
  source/code/align-vad-to-feats scp:$data/feats.scp scp:$src_vad_dir/vad.scp \
  ark,scp:$vad_featdir/${dataname}_vad.ark,$data/vad.scp || exit 1
fi
