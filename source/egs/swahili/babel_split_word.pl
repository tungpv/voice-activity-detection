#!/usr/bin/perl -w
use strict;
use open qw(:std :utf8);
print STDERR "$0: stdin expected \n";
while(<STDIN>) {
  chomp;
  my @A = split(/ /);
  for(my $i=0; $i <@A; $i++) {
    my $w = $A[$i];
    $w =~ s/[\*\~]//g;
    length($w) == 0 && next;
    my $split_w = $w;  $split_w =~ s/[_\']//g;
    print $w, "\t", join(" ", split(//, $split_w)),  "\n";
  }
}
print STDERR "$0: stdin ended\n";
