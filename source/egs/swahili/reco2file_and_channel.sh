#!/bin/bash


if [ $# -ne 1 ]; then
  echo
  echo "Usage: $(basename $0) <data>"
  echo && exit 1
fi

data=$1

[ -f $data/segments ] || \
{ echo "ERROR, segments file expected from data $dir"; exit 1; }

cat $data/segments | \
awk '{print $2;}' | \
awk '{printf("%s %s 1\n", $1, $1);}' > $data/reco2file_and_channel
