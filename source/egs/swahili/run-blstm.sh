#!/bin/bash

. path.sh
. cmd.sh

# begin options
train_cmd="slurm.pl --gres=gpu:1"
decode_cmd="slurm.pl"
nj=40
steps=
nnet_id="nnet5a"
validating_rate=0.1
learn_rate=0.0001
cmvn_opts="--norm-means=true --norm-vars=true"
splice=0
train_tool=nnet-train-blstm-streams
momentum=0.9
num_streams=4
network_type=blstm
targets_delay=5
num_cells=512
num_recurrent=200
num_layers=2
clip_gradient=20.0

devdata=
dnn_decode_conf=conf/decode_dnn.config
graphdir=
decodename=decode_dev
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15 "

trainname=train
user_lang_dir=
acwt=0.0909
mpe_learn_rate=0.00001
# end options
echo
echo " LOG: $0 $@"
echo
. utils/parse_options.sh || exit 1;

function Usage {
  cat <<END

 $(basename $0) [options] <data> <lang> <ali> <dir>
 [options]:
 --train-cmd                              # value, "$train_cmd"
 --decode-cmd                             # value, "$decode_cmd"
 --nj                                     # value, $nj
 --steps                                  # value, "$steps"
 --nnet-id                                # value, "$nnet_id"
 --validating-rate                        # value, $validating_rate
 --learn-rate                             # value, $learn_rate
 --cmvn-opts                              # value, "$cmvn_opts"
 --splice                                 # value, $splice
 --train-tool                             # value, "$train_tool"
 --momentum                               # value, $momentum
 --network-type                           # value, "$network_type"
 --num-streams                            # value, $num_streams
 --targets-delay                          # value, $targets_delay
 --num-cells                              # value, $num_cells
 --num-recurrent                          # value, $num_recurrent
 --num-layers                             # value, $num_layers
 --clip-gradient                          # value, $clip_gradient

 [decode opts]:
 --devdata                                # value, "$devdata"
 --dnn-decode-conf                        # value, "$dnn_decode_conf"
 --graphdir                               # value, "$graphdir"
 --decodename                             # value, "$decodename"
 --decode-opts                            # value, "$decode_opts"
 --scoring-opts                           # value, "$scoring_opts"

 [sequential training]:
 --trainname                              # value, "$trainname"
 --user-lang-dir                          # value, "$user_lang_dir", if this provided, 
                                          # use user-provided grammar to generate den lattice, not normal unigram grammar instead
 --acwt                                   # value, $acwt
 --mpe-learn-rate                         # value, $mpe_learn_rate
 [steps]:
 1: split data for training
 2: train bi-directional lstm acoustic models
 3: test, if dev-data specified
 4: make alignment for sequential training
 5: make den-lattice for sequential training
 6: do smbr-mpe training
 7: test, if dev-data specified

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1;
fi

data=$1
lang=$2
ali=$3
dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

nnetdir=$dir/$nnet_id
train=$nnetdir/train
valid=$nnetdir/valid
if [ ! -z $step01 ]; then
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $train \
  $data  $valid || exit 1
fi
if [ ! -z $step02 ]; then
  echo "LOG: training started @ `date`"
  $train_cmd $dir/log/train_nnet.log \
    steps/nnet/train.sh --network-type $network_type --learn-rate $learn_rate \
      --cmvn-opts "$cmvn_opts" --feat-type plain --splice $splice \
      --train-tool "$train_tool --num-stream=$num_streams" \
      --train-tool-opts "--momentum=$momentum --targets-delay=$targets_delay" \
      --proto-opts "--num-cells $num_cells --num-recurrent $num_recurrent --num-layers $num_layers --clip-gradient $clip_gradient" \
    $train $valid $lang $ali $ali $nnetdir || exit 1;
  echo "LOG: training ended @ `date`"
fi

if [ ! -z $step03 ]; then
  echo "LOG: decoding started @ `date`"
  if [ -z $devdata ]; then
    echo "ERROR, devdata not ready" && exit 1
  fi
  decode_nj=$(wc -l < $devdata/spk2utt)
  [ $decode_nj -gt $nj ] && decode_nj=$nj
  steps/nnet/decode.sh --nj $decode_nj --cmd "$decode_cmd" --config $dnn_decode_conf $decode_opts \
  --scoring-opts "$scoring_opts" \
  $graphdir $devdata  $nnetdir/$decodename || exit 1;
  echo "LOG: decoding ended @ `date`"
fi
alidir=$nnetdir/ali_${trainname}
if [ ! -z $step04 ]; then
  echo "LOG: making alignment started @ `date`"
  steps/nnet/align.sh --cmd "$decode_cmd" --nj $nj $data $lang $nnetdir $alidir || exit 1
  echo "LOG: done with alignment generation @ `date`" 
fi
new_lang=$lang
[ ! -z $user_lang_dir ] && new_lang=$user_lang_dir
use_unigram_grammar=true
[ ! -z $user_lang_dir ] && use_unigram_grammar=false
denlatdir=$nnetdir/denlats-${trainname}
if [ ! -z $step05 ]; then
  echo "LOG: making den-lattice started @ `date`"
  steps/nnet/make_denlats.sh --cmd "$decode_cmd" --nj $nj \
  --use-unigram-grammar $use_unigram_grammar \
  $data $new_lang $nnetdir $denlatdir || exit 1
  echo "LOG: done with den-lattice generation @ `date`"
fi
mpe_dir=$dir/${nnet_id}-mpe
if [ ! -z $step06 ]; then
  echo "LOG: do mpe training started @ `date`"
  steps/nnet/train_mpe.sh --cmd "$train_cmd" \
  --acwt $acwt --learn-rate $mpe_learn_rate --do-smbr true \
  $data $lang $nnetdir $alidir $denlatdir $mpe_dir || exit 1
  echo "LOG: mpe training done @ `date`"
fi
if [ ! -z $step07 ]; then
  echo "LOG: decoding using mpe model started @ `date`"
  decode_nj=$(wc -l < $devdata/spk2utt)
  [ $decode_nj -gt $nj ] && decode_nj=$nj

  steps/nnet/decode.sh --nj $decode_nj --cmd "$decode_cmd" --config $dnn_decode_conf $decode_opts \
  --scoring-opts "$scoring_opts" \
  $graphdir $devdata  $mpe_dir/$decodename || exit 1;
  echo "LOG: decoding ended @ `date`"
fi

echo "$0: Done for all !"
