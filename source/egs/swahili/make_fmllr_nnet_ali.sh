#!/bin/bash

. path.sh
. cmd.sh 

# begin options
nj=40
cmd=slurm.pl
steps=
dataname=train
# end options
echo 
echo "LOG: $0 $@"
echo

. parse_options.sh || exit 1

function PrintOptions {
  cat <<END
 --nj			# value, $nj
 --cmd			# value, "$cmd"
 --steps		# value, "$steps"
 --dataname		# value, "$dataname"

[steps]:
 1: make alignment within gmmdir
 2: make fmllr feature within gmmdir
 3: make alignment within nnetdir
END
}

if [ $# -ne 4 ]; then
  echo
  echo "Usage: $(basename $0) [options] <data> <lang> <gmmdir> <nnetdir>"
  PrintOptions;
  echo && exit 1
fi

data=$1
lang=$2
gmmdir=$3
nnetdir=$4

for x in $gmmdir/final.mdl $nnetdir/final.nnet; do
  [ -f $x ] || \
  { echo "ERROR, file $x is not ready in $(dirname $x)"; exit 1; }
done
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  steps/align_fmllr.sh  --cmd "$cmd" --nj $nj \
  $data $lang $gmmdir $gmmdir/ali_$dataname || exit 1
fi
if [ ! -z $step02 ]; then
  transform_dir=
  if [ -f $gmmdir/ali_$dataname/ali.1.gz ]; then
    transform_dir=$gmmdir/ali_$dataname
  fi
  source/egs/swahili/make_feats.sh --make-fmllr true --srcdir $gmmdir ${transform_dir:+--transform-dir $transform_dir} \
  $data $gmmdir/$dataname $gmmdir/feats/$dataname || exit 1
fi
if [ ! -z $step03 ]; then
  steps/nnet/align.sh --cmd "$cmd" --nj $nj $gmmdir/$dataname $lang $nnetdir $nnetdir/ali_$dataname || exit 1
fi
