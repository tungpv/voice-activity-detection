#!/bin/bash

. path.sh
. cmd.sh

# begin options

# end options

. parse_options.sh || exit 1

function PrintOptions {
  cat<<END

END
}

if [ $# -ne 1 ]; then
  echo 
  echo "Usage: $(basename $0) [options] <logdir>"
  PrintOptions
  echo && exit 1
fi

logdir=$1

grep Ended $logdir/* | \
perl -e ' $x = 1; while(<STDIN>){ 
   chomp; if(/\(code (\d+)\)/) {
   if($1 != 0) {
     s/:\#.*//g;
     $logfile = $_;
     $new_logfile = $logfile . ".$x";
     $cmd = `head -1 $_ |sed 's/#//'`;
     open(B, "|bash") || die "Error opening shell command\n";
     print B "( " . $cmd . ") > $new_logfile 2>&1 &"; 
     close (B);
     $ret = $?;
     
     $x ++;
   }
  }
} '
