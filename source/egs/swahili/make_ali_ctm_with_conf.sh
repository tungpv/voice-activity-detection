#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=slurm.pl
nj=40
steps=
alicmd=steps/nnet/align.sh
data_name=unlabelled
overlap_rate=0.5
# end options

. parse_options.sh || exit 1
function PrintOptions {
  cat<<END
[options]:
--cmd				# value, "$cmd"
--steps				# value, "$steps"
--alicmd			# value, "$alicmd"
--data-name			# value, "$data_name"
--overlap-rate			# value, "$overlap_rate"

[steps]:
1: utt_ctm to text
2: make unsupervised data
3: align the data
4: align to ctm
5: add conf score to the aligned ctm
END
}
if [ $# -ne 5 ]; then
  echo
  echo "Usage: $(basename $0) [options] <data> <lang> <utt_ctm> <sdir> <new_ctm_dir>"
  PrintOptions;
  echo && exit 1
fi

data=$1
lang=$2
utt_ctm=$3
sdir=$4
dir=$5

[ -d $dir ] || mkdir -p $dir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  cat $utt_ctm | \
  source/egs/swahili/ctm_to_text.pl > $dir/text
fi

if [ ! -z $step02 ]; then
  utils/subset_data_dir.sh --utt-list $dir/text $data $dir
  utils/fix_data_dir.sh $dir 
fi
if [ ! -z $step03 ]; then
  $alicmd --cmd "$cmd" --nj $nj $dir $lang $sdir $sdir/ali_${data_name} || exit 1
fi
if [ ! -z $step04 ]; then
  source/egs/swahili/get_train_ctm.sh --cmd "$cmd" $dir  $lang  $sdir/ali_${data_name} || exit 1
fi
if [ ! -z $step05 ]; then
  [ -f $sdir/ali_${data_name}/utt.ctm ] || \
  { echo "ERROR, file utt.ctm expected from $sdir/ali_${data_name}"; exit 1; }
  source/code/ctm-borrow-conf-score --overlap-thresh=$overlap_rate $sdir/ali_${data_name}/utt.ctm \
  $utt_ctm $dir/utt.ctm > $dir/utt.ctm.log 2>&1 
  tail -50 $dir/utt.ctm.log
fi
