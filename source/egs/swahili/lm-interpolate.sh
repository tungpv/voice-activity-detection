#!/bin/bash

. path.sh
. cmd.sh

# begin options

# end options

. parse_options.sh
if [ $# -ne 4 ]; then
  echo
  echo "Usage: $(basename $0) <out_lm> <in_lm> <dev_text> <dir>"
  echo && exit 1
fi

out_lm=$1
in_lm=$2
dev_text=$3
dir=$4

[ -d $dir ] || mkdir -p $dir

cat $dev_text | \
perl -pe 'chomp; @A = split(/[ \t]/); shift @A; $_ = join(" ", @A); $_ .= "\n";' > $dir/dev.txt; 
dev_text=$dir/dev.txt

tmpdir=$(mktemp -d -p $dir)
trap  "echo removing $tmpdir ...; rm -rf $tmpdir; rm $dir/perplexities" EXIT

best_ppl=10000000
best_lm=
for ifactor in $(seq  0 0.1 0.6); do
  echo "interpolating, factor=$ifactor ..."
  tlm=$tmpdir/lm.int.${ifactor}.gz
  ngram -order 3 -lm  $out_lm  -lambda $ifactor \
  -mix-lm $in_lm  -write-lm $tlm
  ppl=$(ngram -order 3 -lm $tlm -unk -ppl $dev_text | paste -s -d ' '| tee -a $dir/perplexities | awk '{print $14;}')
  echo "LOG: Current PPL, $ppl"
  [ -z $ppl ] && \
  { echo "ERROR, lm $tlm ppl is problematic "; exit 1; }
  if [ $(echo "$ppl < $best_ppl"| bc -l) -eq 1 ]; then
    best_lm=$tlm
    best_ppl=$ppl
  fi
done 
[ -z $best_lm ] && \
{ echo "ERROR, best_lm not found by interpolating"; exit 1; }
cp $best_lm $dir/$(basename $best_lm)

exit 0

