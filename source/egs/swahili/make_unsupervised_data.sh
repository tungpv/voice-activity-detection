#!/bin/bash

. path.sh
. cmd.sh

echo
echo "LOG: $0 $@"
echo 

# begin options
steps=
conf_thresh=0.9
word_merge_tolerance=0.02
utt_merge_tolerance=0.02
ratio_selected=
select_utterance=false
labelled_data=
semi_data=
# end options

. parse_options.sh || exit 1
function PrintOptions {

  cat<<END
 $(basename $0) [options] <sdata> <utt_ctm> <data>"
 [options]:
 --steps                         # value, "$steps"
 --conf-thresh                   # value, $conf_thresh
 --word-merge-tolerance          # value, $word_merge_tolerance
 --utt-merge-tolerance           # value, $utt_merge_tolerance
 --ratio-selected                # value, "$ratio_selected"
 --select-utterance              # value, $select_utterance
 --labelled-data                 # value, "$labelled_data"
 --semi-data                     # value, "$semi_data"

 [steps]:
 1: make unsupervised data
 2: merge with labelled_data to make semi data, if any labelled_data given

END
}
if [ $# -ne 3 ]; then
  echo 
  PrintOptions
  echo && exit 1
fi

sdata=$1
utt_ctm=$2
data=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $data ] || mkdir -p $data
if [ ! -z $step01 ]; then
  if [ ! -z $ratio_selected ]; then
    if [ $(echo "$ratio_selected <0 || $ratio_selected >1"|bc ) -eq 1 ]; then
      echo "$0: ERROR, ratio_selected($ratio_selected) is illegal"; exit 1;
    fi
    if ! $select_utterance; then
      echo "$0: word: ratio method"
      source/code/extract-sub-segment-with-conf --ratio-selected=$ratio_selected --select-utterance=false --merge-tolerance=$word_merge_tolerance \
    $sdata/segments  "egrep -v '<' $utt_ctm |"  $data/segments $data/text
    else
      echo "$0: utt: ratio method"
      source/code/extract-sub-segment-with-conf --ratio-selected=$ratio_selected --select-utterance=true --merge-tolerance=$utt_merge_tolerance \
    $sdata/segments  "egrep -v '<' $utt_ctm |"  $data/segments $data/text
    fi
  else
    echo "$0: use word confidence to select the data"
    source/code/extract-sub-segment-with-conf --conf-thresh=$conf_thresh --merge-tolerance=$word_merge_tolerance \
    $sdata/segments  "grep -v -E '<' $utt_ctm |"  $data/segments $data/text
  fi

  cp $sdata/wav.scp $data/wav.scp
  # cat $data/segments | perl -pe '@A = split(/\s+/); $s = $A[0]; $s =~ m/([^_]+_[^_]+)_*/; $_="$s $1\n";'  > $data/utt2spk
  cat $data/segments | \
  perl -e '($utt2spk) = @ARGV; open(F, "<", "$utt2spk") or die "## ERROR: utt2spk $utt2spk cannot open\n";
    while(<F>) {chomp; @A = split(/[\s+]/); if(@A == 2){ $vocab{$A[0]} = $A[1]; } } close F;
    while(<STDIN>) {chomp; @A = split(/[\s+]/); if(@A == 4) { $new_utt = $A[0]; $utt = $new_utt; $utt =~ s/_\d+$//; 
      if(not exists $vocab{$utt}) {die "## ERROR: no see utt $utt\n";}  print "$new_utt $vocab{$utt}\n";
    }}
  ' $sdata/utt2spk  > $data/utt2spk || { echo "## ERROR: failed to generate utt2spk"; exit 1; }

  utils/utt2spk_to_spk2utt.pl < $data/utt2spk > $data/spk2utt
  utils/fix_data_dir.sh $data
fi

if [ ! -z $step02 ]; then
  [ -z $labelled_data ] && { echo "ERROR, labelled_data expected"; exit 1; }
  tmpdir=$(mktemp -d)
  mkdir -p $tmpdir
  trap "echo removing $tmpdir ...; rm -rf $tmpdir" EXIT
  cp $labelled_data/*  $tmpdir/
  rm $tmpdir/{reco2file_and_channel,cmvn.scp,feats.scp} 2>/dev/null
  [ -z $semi_data ] && { echo "ERROR, semi_data expected"; exit 1; }
  utils/combine_data.sh $semi_data $tmpdir $data 
  utils/fix_data_dir.sh $semi_data
fi

echo "## Done @ `date`"
exit 0

