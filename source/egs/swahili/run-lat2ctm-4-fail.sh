#!/bin/bash

. path.sh

for x in $(seq 1 40); do 
  head -1 exp/mfcc_pitch.nopos/x.nnet/decode_t14.dev/ctm_10/log/get_ctm.$x.log  | \
  sed 's/#//g' | bash > exp/mfcc_pitch.nopos/x.nnet/decode_t14.dev/ctm_10/log/get_ctm.failed${x}.log 2>&1 
done 
