#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=40
cmd="slurm.pl"
steps=
modeldir=
decode_cmd="steps/nnet/decode.sh"
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 20"
silence_word_int=0
lmwt=10
prune_beam=5
# end options
echo
echo "LOG: $0 $@"
echo
. parse_options.sh || exit 1

function PrintOptions {
  cat<<END
[options]:
--nj				# value, $nj
--cmd				# value, "$cmd"
--steps				# value, "$steps"

--modeldir			# value, "$modeldir"
--decode-cmd			# value, "$decode_cmd"
--decode-opts			# value, "$decode_opts"
--scoring-opts			# value, "$scoring_opts"

--silence-word-int		# value, $silence_word_int
--lmwt				# value, $lmwt 
--prune-beam			# value, $prune_beam

[steps]:
1: make graph
2: decode & scoring
3: lattice to ctm conversion
END
}

if [ $# -ne 4 ]; then
  echo
  echo "Usage: $(basename $0) [options] <data> <lang> <graphdir> <dir>"
  PrintOptions
  echo && exit 1
fi

data=$1
lang=$2
graphdir=$3
dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ] && [ ! -f $graphdir/HCLG.fst ]; then
  echo "Making graph started @ `date`"
  srcdir=$modeldir
  [ -z $srcdir ] && srcdir=$(dirname $graphdir)
  [ -f $srcdir/final.mdl ] || \
  { echo "ERROR, final.mdl expected from srcdir $srcdir"; exit 1; }
  utils/mkgraph.sh $lang $srcdir  $graphdir
  echo "Done @ `date`"
fi
effect_nj=$(wc -l < $data/spk2utt)
[ $effect_nj -gt $nj ] && effect_nj=$nj
if [ ! -z $step02 ]; then
  [ -f $graphdir/HCLG.fst ] || \
  { echo "ERROR, HCLG.fst expected from $graphdir"; exit 1; }
  echo "Decoding started @ `date`"
  $decode_cmd --cmd "$cmd" --nj $effect_nj \
  --scoring-opts "$scoring_opts" \
  $decode_opts \
  $graphdir $data $dir || exit 1
  echo "Done @ `date`"
fi

if [ ! -z $step03 ]; then
  source/egs/swahili/lat_to_utt_ctm.sh --cmd "$cmd" --lmwt $lmwt --steps 1,2 \
  --silence-word-int $silence_word_int \
  --prune-beam $prune_beam \
  $data $lang $dir || exit 1
fi
