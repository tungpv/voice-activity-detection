#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=20
cmd=run.pl
# end options

echo 
echo "$0 $@"
echo

. parse_options.sh

if [ $# -ne 3 ]; then
  echo
  echo "usages: $0 <old_alidir> <new_sdir> <new_alidir>"
  echo
  exit 1
fi

old_alidir=$1
new_sdir=$2
new_alidir=$3

for x in ali.1.gz final.mdl; do
  file=$old_alidir/$x
  [ -f $file ] || \
  { echo "$0: ERROR, file $file expected"; exit 1; }
done
nj=$(cat $old_alidir/num_jobs)
for x in final.mdl tree; do
  file=$new_sdir/$x
  [ -f $file ] || \
  { echo "$0: ERROR, file $file expected"; exit 1; }
done

logdir=$new_alidir/log
[ -d $logdir ] || mkdir -p $logdir

$cmd JOB=1:$nj $logdir/conver_ali.JOB.log \
convert-ali $old_alidir/final.mdl \
$new_sdir/final.mdl  $new_sdir/tree \
"ark:gzip -cd $old_alidir/ali.JOB.gz|" "ark:|gzip > $new_alidir/ali.JOB.gz" 

cp $new_sdir/{final.mdl,tree} $new_alidir
echo $nj > $new_alidir/num_jobs
echo "$0: Done with convert-ali @ `date`" 
exit 0
