#!/usr/bin/perl

use strict;
use warnings;
use utf8;
use open qw(:std :utf8);

use Getopt::Long "GetOptions";
my $cmdName = $0;  $cmdName =~ s/^.*\///g;
print STDERR "\nLOG $cmdName ", join (" ", @ARGV), "\n";

sub Usage {
  my $cmd = $0; $cmd =~ s/.*\///g;
  my $sUsage =<<EOU;

Usage Example:
  $cmd [options] wordlist.txt/lexicon.txt  text 
  e.g.: $cmd --from=2 data/lang/words.txt data/train/text

EOU
  print $sUsage;
  exit 1;
}

my $from=1;
my $isWordList="false";
GetOptions ( 
  'iswordlist=s' =>\$isWordList,
  'from=i' =>\$from
);

Usage unless @ARGV == 2;
my ($sWFile, $sTextFile) = @ARGV;

my %vocab = ();
open (F, "$sWFile") or die "$0: ERROR,file $sWFile cannot open\n";
while(<F>) {
  chomp;
  s/^\s+//g;
  m/(^\S+)/ or next;
  my $str = $1;
  $vocab{$str} ++;
}
close F;
my %oov = (); my $nTotal = 0;
open(F, "$sTextFile") or die "$0: ERROR,file $sTextFile cannot open\n";
while (<F>) {
  chomp;
  my @Array = split(/\s+/);
  $nTotal += scalar @Array;
  for(my $i = $from -1; $i< @Array; $i++) {
    my $sWord =  $Array[$i];
    # print STDERR "$sWord=", length($sWord), "$Array[$i]=", length($Array[$i]), "\n";
    $oov{$sWord} ++ if not exists $vocab{$sWord};
  } 
}
close F;
my $nOovs = 0; my $s = "";
open(F, "|sort -k2nr") or die;
foreach my $sWord (keys %oov) {
  $nOovs += $oov{$sWord};
  my $nCount = $oov{$sWord};
  # my $nLen = length($sWord);
  # my $sUtf8 =$sWord;
  my $line = sprintf("%s\t%d\n", $sWord, $nCount);
  print F "$line";
  # my @A = split(//, $sUtf8);
  # my $s1 = join(" ", @A); 
  # $nLen = length($sUtf8);
  # $s .= "$sUtf8($nLen, $s1) ";
}
close F;
my $fRate = 0;
$fRate = sprintf("%.4f", $nOovs/$nTotal) if $nTotal > 0;
print STDERR "$s\noovs=$nOovs, total=$nTotal, oovRate=$fRate\n";

