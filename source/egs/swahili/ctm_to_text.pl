#!/usr/bin/perl 
use warnings;
use strict;

print STDERR "$0: stdin expected \n";
my $prev_utt_name = "";
my $utt_str = "";
while(<STDIN>) {
  chomp;
  my @A = split(" ");
  die "$0: ERROR, not identified ctm line $_\n" if(@A != 5 && @A != 6);
  my $curr_utt_name = $A[0];
  if($prev_utt_name ne "" && $prev_utt_name ne $curr_utt_name) {
    print STDOUT $prev_utt_name, " $utt_str\n";
    $utt_str = "";
  }
  $prev_utt_name = $curr_utt_name;
  $utt_str .= "$A[4] ";
}
print STDOUT $prev_utt_name, " $utt_str\n";
print STDERR "$0: stdin ended \n";
