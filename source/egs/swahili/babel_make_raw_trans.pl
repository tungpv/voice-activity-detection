#!/usr/bin/perl
use warnings;
use strict;
use open qw(:std :utf8);

print STDERR "$0: stdin expected\n";
my ($start, $end) = (0, 0);
my $sUtterance = "";
while(<STDIN>) {
  chomp;
  open(FILE, "$_") or die "$0: ERROR, file $_ cannot open\n";
  my $sFileName = $_;
  $sFileName =~ s/.*\///g;
  $sFileName =~ s/\.txt//g;
  while(<FILE>) {
    chomp;
    if(/\[(.*)\]/) {
      if($start == 0) {
        $start = $1;
      } else {
        $end = $1;
        if(  $sUtterance !~ m/\<untranscribed\>/) {
          print $sFileName, " ", $start, " ", $end, " ", $sUtterance, "\n";
        }
        $start = $end;
      }
    } else {
      $sUtterance = $_;
    }  
  }
  close FILE;
}
print STDERR "$0: stdin ended\n";
