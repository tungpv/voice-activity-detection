#!/bin/bash

. path.sh
. cmd.sh

# begin options

# end options

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  echo 
  echo "Usage: $(basename $0) alidir dir"
  echo && exit 1
fi

alidir=$1
dir=$2

labels_tr_pdf="ark:ali-to-pdf $alidir/final.mdl \"ark:gunzip -c $alidir/ali.*.gz |\" ark:- |" # for analyze-counts.

# get pdf-counts, used later to post-process DNN posteriors
  analyze-counts --verbose=1 --binary=false "$labels_tr_pdf" $dir/ali_train_pdf.counts 2>$dir/log/analyze_counts_pdf.log || exit 1
  # copy the old transition model, will be needed by decoder
  copy-transition-model --binary=false $alidir/final.mdl $dir/final.mdl || exit 1
  # copy the tree
  cp $alidir/tree $dir/tree || exit 1

