#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=40
cmd="slurm.pl --exclude=node01,node02"
steps=

validating_rate=0.1
cmvn_opts="--norm-means=true"
delta_opts="--delta-order=2"
gmmdir=
train_nnet_cmd=steps/nnet/train.sh
train_bn_opts="--hid-dim 1500 --hid-layers 2 --learn-rate 0.008 --bn-dim 40 --feat-type traps --splice 5"

devdata=
devname=dev
trainname=train
featdir=
convert_ali=false
lda_dim=40
gmmid=a
train_gmm_opts="<tied_state_num> <gaussian_num>"
graphname=graph
decodename=decode_dev
lattice_opts="--acwt 0.05 --skip-scoring false"

fmllr_nnet_pretrain_cmd="steps/nnet/pretrain_dbn.sh --copy-feats-tmproot /local/hhx502 --splice 5 --nn-depth 5 --hid-dim 2048"
fmllr_nnet_train_cmd="source/egs/train.sh --copy-feats-tmproot /local/hhx502"
fmllr_nnet_train_opts="--validating-rate 0.1 --learn_rate 0.008"
fmllr_nnet_id=a
scoring_opts="--min-lmwt 8 --max-lmwt 15"
nnet_decode_opts="--beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"

# end options

echo
echo LOG: $0 $@
echo

. parse_options.sh || exit 1

function PrintOptions {
  cat <<END
[options]:
--nj				# value, $nj
--cmd				# value, "$cmd"
--steps				# value, "$steps"

--validating_rate		# value, $validating_rate
--cmvn_opts			# value, "$cmvn_opts"
--delta_opts			# value, "$delta_opts"
--gmmdir			# value, "$gmmdir"
--train_nnet_cmd		# value, "$train_nnet_cmd"
--train_bn_opts			# value, "$train_bn_opts"

--devdata			# value, "$devdata"
--devname			# value, "$devname"
--trainname			# value, "$trainname"
--featdir			# value, "$featdir"
--convert-ali			# value, $convert_ali
--lda-dim			# value, $lda_dim
--gmmid				# value, "$gmmid"
--train-gmm-opts		# value, "$train_gmm_opts"
--graphname			# value, "$graphname"
--decodename			# value, "$decodename"
--lattice-opts			# value, "$lattice_opts"

--fmllr_nnet_pretrain_cmd	# value, "$fmllr_nnet_pretrain_cmd"
--fmllr_nnet_train_cmd		# value, "$fmllr_nnet_train_cmd"
--fmllr_nnet_id			# value, "$fmllr_nnet_id"
--scoring-opts			# value, "$scoring_opts"
--nnet_decode_opts		# value, "$nnet_decode_opts"

 [steps]:
 1: train bn dnn 
  2: train bn dnn, prepare data 
  3: train bn dnn, training
 4: make bnf for training data
 5: make bnf for dev data, if any
 6: train bnf gmm
 7: test bnf gmm, if any
 8: make fmllr for train bnf data
 9: make fmllr for dev bnf data, if any
 10: train fmllr+bnf dnn
  11: prepare data to train fmllr+bnf dnn
  12: pretrain
  13: train dnn
 14: test dnn, if any 
END
}

if [ $# -ne 4 ]; then
  echo
  echo "Usage: $(basename $0) [options] <data> <lang> <alidir> <dir>"
  PrintOptions
  echo && exit 1
fi

data=$1
lang=$2
alidir=$3
dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if $convert_ali; then
  [ ! -z $gmmdir ] || \
  { echo "ERROR: gmmdir not specified for alignment conversion"; exit 1; }
  [ -d $gmmdir/ali_${trainname}-conv ] || \
  source/egs/convert-ali.sh $alidir $gmmdir $gmmdir/ali_${trainname}-conv
  alidir=$gmmdir/ali_${trainname}-conv
fi
[ -z $gmmdir ] && gmmdir=$(dirname $alidir)

if [ ! -z $step01 ]; then
  echo "BN DNN training started @ `date`"
  source/run-nnet.sh --validating-rate $validating_rate \
  --train-fbank-data $data \
  --labeldir $alidir \
  ${cmvn_opts:+--cmvn_opts "$cmvn_opts"} \
  ${delta_opts:+--delta_opts "$delta_opts"} \
  --sdir $gmmdir --usefbankdata true \
  ${step02:+--PrepareData true} \
  ${step03:+--hierbn1-train true --hierbn1_cmd "$train_nnet_cmd $train_bn_opts"}  \
  $lang alidata $dir || exit 1
  echo "BN DNN trainined ended @ `date`"
fi
trnbnfdata=$dir/hierbn1/$trainname
bnfeatdir=$featdir/$(basename $dir).hierbn1
[ -z $featdir ] && bnfeatdir=$dir/hierbn1/feats
if [ ! -z $step04 ]; then
  echo "making bnf data for $trainname started @ `date`"
  source/egs/run-bnf.sh --make_bnf2 true --cmd "$cmd" --nj $nj \
  --make_bnf2_opts "$data $dir/hierbn1 $trnbnfdata $bnfeatdir/$trainname" || exit 1
  echo "ended @ `date`"
fi
devbnfdata=$dir/hierbn1/$devname
if [ ! -z $step05 ] && [ ! -z $devdata ]; then
  echo "making bnf data for $devname started @ `date`"
  source/egs/run-bnf.sh --make_bnf2 true --cmd "$cmd" --nj $nj \
  --make_bnf2_opts "$devdata $dir/hierbn1 $devbnfdata $bnfeatdir/$devname" || exit 1
  echo "ended @ `date`"
fi

if [ ! -z $step06 ]; then
  echo "gmm training started @ `date`"
  source/egs/run-bnf.sh  --cmd "$cmd" --nj $nj --run_gmm2 true \
  --import-alidir $alidir --lda-dim $lda_dim \
  --run_gmm2_opts \
  "$gmmid $trnbnfdata $trainname $lang $train_gmm_opts $dir" || exit 1
  echo "ended @ `date`"
fi
sdir=$dir/tri4$gmmid
graphdir=$sdir/$graphname
effect_nj=$(wc -l < $devbnfdata/spk2utt)
[ $effect_nj -gt $nj ] && effect_nj=$nj
if [ ! -z $step07 ] && [ ! -z $devdata ]; then
  echo "gmm testing started @ `date`"
  source/egs/run-bnf.sh --decoding_eval true --cmd "$cmd" --nj $effect_nj --decoding_opts \
  "$devbnfdata  $lang  $sdir $sdir/$graphname \
  $sdir/$decodename steps/decode_fmllr.sh" \
  --lattice-opts "$lattice_opts"  || exit 1
  echo "ended @ `date`"
fi
fmllrtrndata=$sdir/$trainname
fmllrfeatdir=$sdir/feats
labeldir=$sdir/ali_$trainname
if [ ! -z $step08 ]; then
  source/egs/run-bnf.sh --make_fmllr true --make_fmllr_opts \
  "$trnbnfdata $sdir $sdir/align_$trainname $fmllrtrndata $fmllrfeatdir/$trainname" || exit 1
fi
fmllrdevdata=$sdir/$devname
if [ ! -z $step09 ] && [ ! -z $devdata ]; then
  source/egs/run-bnf.sh --make_fmllr true --make_fmllr_opts \
  "$devbnfdata $sdir $sdir/$decodename $fmllrdevdata $fmllrfeatdir/$trainname" || exit 1
fi
nnetsdir=$dir/nnet$fmllr_nnet_id
if [ ! -z $step10 ]; then
  echo "nnet training started @ `date`"
  source/run-nnet.sh  --usefmllrdata true \
  ${fmllr_nnet_id:+--nnet-id $fmllr_nnet_id} \
  --preTrnCmd "$fmllr_nnet_pretrain_cmd" \
  ${cmvn_opts:+--cmvn_opts "$cmvn_opts"} \
  ${labeldir:+--labeldir $labeldir} \
  ${step11:+--PrepareData true} \
  ${step12:+--PreTrain true } \
  ${step13:+--TrainNnet true} --trnCmd "$fmllr_nnet_train_cmd" --sdir $sdir\
  $lang $fmllrtrndata $dir || exit 1
   echo "ended @ `date`"
fi
if [ ! -z $step14 ]; then
  echo "nnet test started @ `date`"
  source/egs/swahili/decode.sh --cmd "$cmd" --nj $nj \
  --step01 true  --step02 true \
  --scoring-opts "$scoring_opts" \
  --decode-opts "$nnet_decode_opts" \
  $fmllrdevdata $lang $graphdir $nnetsdir/$decodename || exit 1
  echo "ended @ `date`"
fi
