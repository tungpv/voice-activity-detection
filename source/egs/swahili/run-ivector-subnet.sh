#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=slurm.pl
nj=40
steps=
nnet_pretrain_cmd="steps/nnet/pretrain_dbn.sh --copy_feats_tmproot $(pwd)  --splice 0  --nn-depth 3 --hid-dim 512"
use_source_nnet=false
ivector_data=
ivector_dim=100
nn_depth=2
hid_dim=512
validating_rate=0.1
learn_rate_subnet=0.008
learn_rate=0.008
train_tool=source/code2/nnet-subnet-train-frmshuff
copy_feats=true
gmmdir=
change_source_nnet_softmax=false

graphdir=
testdata=
ivector_testdata=
decodename=decode_dev
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15"

train_cmd=run.pl
train_data_name=train
acwt_make_denlats=0.0909
conf_make_denlats=conf/decode_dnn.config

# end options
echo 
echo "LOG: $0 $@"
echo
. parse_options.sh || exit 1

function Usage() {
  cat<<END
 $(basename $0) [options] <data> <lang> <alidir> <source_nnet_dir> <dir>
 [options]:
 --cmd                                      # value, "$cmd"
 --nj                                       # value, "$nj"
 --steps                                    # value, "$steps", e.g.: --steps "1,2,3,4,5,6" for overall test.
 --nnet-pretrain-cmd                        # value, "$nnet_pretrain_cmd"
 [<train opts>]:
 --use-source-nnet                          # value, $use_source_nnet, if true, no need to retrain the master dnn.
 --ivector-data                             # value, "$ivector_data"
 --ivector-dim                              # value, $ivector_dim
 --nn-depth                                 # value, $nn_depth
 --hid-dim                                  # value, $hid_dim
 --validating-rate                          # value, "$validating_rate"
 --learn-rate-subnet                        # value, $learn_rate_subnet
 --learn-rate                               # value, "$learn_rate"
 --train-tool                               # value, "$train_tool"
 --copy-feats                               # value, "$copy_feats"
 --gmmdir                                   # value, "$gmmdir"
 --change-source-nnet-softmax               # value, "$change_source_nnet_softmax"
 [<decode opts>]:
 --graphdir                                 # value, "$graphdir"
 --testdata                                 # value, "$testdata"
 --ivector-testdata                         # value, "$ivector_testdata"
 --decodename                               # value, "$decodename"
 --decode-opts                              # value, "$decode_opts"
 --scoring-opts                             # value, "$scoring_opts"
 [<DNN sequential training>]:
 --train-cmd                                # value, "$train_cmd"
 --train-data-name                          # value, "$train_data_name"
 --acwt-make-denlats                        # value, $acwt_make_denlats
 --conf_make_denlats                        # value, "$conf_make_denlats"
 [steps]:
 1: initialize the ivector nn without pretrainining, if this is done, we skip step 2
 2: initialize the ivector nn with pretraining
 3: prepare train and cross-validating data
 4: train ivector nn, fixing the main nn
 5: train the main nn fixing ivector nn
 6: test the recipe with the testdata
 7: align the training data for the DNN sequential training
 8: make denominator lattice for the DNN sequential training
 9: do the DNN mpe-sequential training
 10: test the mpe-sequential training model

END
}

if [ $# -ne 5 ]; then
  Usage && exit 1;
fi 

data=$1
lang=$2
alidir=$3
src_mainnnet_dir=$4
dir=$5

[ -d $dir ] || mkdir -p $dir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
ivector_subnnet_dir=$dir/ivector_dnn
[ -d $ivector_subnnet_dir ] || mkdir -p $ivector_subnnet_dir

pretrain_dir=$ivector_subnnet_dir/pretrain_dbn
ivector_feature_transform="--parallel-feature-transform=$pretrain_dir/final.feature_transform"
num_hid=$(echo "$nnet_pretrain_cmd"| perl -pe 'm/(.*)\s+--nn-depth\s+(\d+)\s+(.*)/; $_ = $2;')
dbn=$pretrain_dir/${num_hid}.dbn
nnet_init=$ivector_subnnet_dir/nnet_$(basename $dbn)_dnn.init
if [ ! -z $step01 ] ; then
  echo "LOG: initialize subnnet started @ `date`"
  output_dim=$(nnet-info $src_mainnnet_dir/final.feature_transform | grep -m 1 output-dim | awk '{print $2;}')
  utils/nnet/make_nnet_proto.py --no-softmax $ivector_dim  $output_dim  $nn_depth  $hid_dim > $ivector_subnnet_dir/nnet.proto
  nnet-initialize $ivector_subnnet_dir/nnet.proto $ivector_subnnet_dir/nnet.init
  nnet_init=$ivector_subnnet_dir/nnet.init
  step02=
  echo "LOG: Done"
fi
if [ ! -z $step02 ]; then
  [ -z $ivector_data ] && \
  { echo "ERROR, ivector_data is not specified" && exit 1; }
  $nnet_pretrain_cmd  --rbm-iter 1  $ivector_data  $pretrain_dir || exit 1;
  echo "LOG: number of hidden layers is $num_hid"
  [ -e $dbn ] || \
  { echo "ERROR, dbn $dbn is not ready"; exit 1; }
  input_dim=$(nnet-info $dbn | grep -m 1 output-dim | awk '{print $2;}')
  echo "LOG: input_dim is $input_dim"
  [ -z $src_mainnnet_dir/final.feature_transform ] && \
  { echo "ERROR, final.feature_transform expected from dir $src_mainnnet_dir"; exit 1; }
  output_dim=$(nnet-info $src_mainnnet_dir/final.feature_transform | grep -m 1 output-dim | awk '{print $2;}')
  echo "LOG: output_dim is $output_dim"
  utils/nnet/make_nnet_proto.py --no-softmax $input_dim  $output_dim 0  $input_dim > $ivector_subnnet_dir/nnet.proto
  nnet-initialize $ivector_subnnet_dir/nnet.proto $ivector_subnnet_dir/nnet.init
  nnet-concat $dbn $ivector_subnnet_dir/nnet.init $nnet_init || exit 1 
  echo "LOG: ivector dnn initialization $nnet_init done @ `date`"
fi

train=$dir/train
valid=$dir/valid
if [ ! -z $step03 ]; then
  echo "LOG: make data to train nnet @ `date`"
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $train \
  $data  $valid || exit 1
  echo "LOG: done @ `date`"
fi
[ -f $ivector_feature_transform ] || ivector_feature_transform=
function check_train_ivector_data() {
  [ -f $ivector_data/ivector.scp ] || \
  { echo "ERROR, ivector.scp expected from folder $ivector_data"; exit 1; }
  (cd $ivector_data ; [ -f feats.scp ] || ln -s  ivector.scp feats.scp )
}
parallel_nnet_opts="--parallel-feature=scp:$ivector_data/feats.scp $ivector_feature_transform"

parallel_utt2spk="--parallel-utt2spk=ark:$data/utt2spk"
parallel_nnet_opts="$parallel_nnet_opts --parallel-nnet-level=1 $parallel_utt2spk"
feature_transform=$src_mainnnet_dir/final.feature_transform
main_nnet=$src_mainnnet_dir/final.nnet
[ -z $gmmdir ] && gmmdir=$(dirname $alidir)
if $change_source_nnet_softmax; then
  echo "change source nnet softmax started @ `date`"
  [ ! -f $gmmdir/final.mdl ] && \
  { echo "ERROR, final.mdl is not ready in $gmmdir "; exit 1; } 
  num_tgt=$(hmm-info --print-args=false $gmmdir/final.mdl | grep pdfs | awk '{ print $NF }')
  soft_max_proto=$src_mainnnet_dir/soft-max.proto
  front_nnet="nnet-copy --remove-last-layers=2 $src_mainnnet_dir/final.nnet -|" 
  hid_dim=$(nnet-info "$front_nnet" | \
                  grep output-dim | tail -1 |perl -pe 's/.*output-dim//g; m/(\d+)/g; $_=$1;') || \
  { echo "ERROR, hid_dim error"; exit 1; }
  cross_dir=$dir/cross_master
  [ -d $cross_dir ] || mkdir -p $cross_dir
  utils/nnet/make_nnet_proto.py $hid_dim $num_tgt 0  $hid_dim >$soft_max_proto || exit 1 
  soft_max_init=$src_mainnnet_dir/soft-max.init
  log=$src_mainnnet_dir/sotf-max-initialize.log
  nnet-initialize $soft_max_proto $soft_max_init 2>$log || { cat $log; exit 1; }
  nnet-concat "$front_nnet" $soft_max_init $cross_dir/nnet.init 2> $cross_dir/nnet-init.log
  (cd $cross_dir ; [ -f ./final.nnet ] && unlink final.nnet;  ln -s nnet.init final.nnet)
  main_nnet=$cross_dir/final.nnet
  src_mainnnet_dir=$cross_dir
  echo "done @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "train ivector-sat sub-nnet started @ `date`"
  check_train_ivector_data;
  [ -z $nnet_init ] && \
  { echo "ERROR, nnet_init $nnet_init expected"; exit 1; }
  [ -z $feature_transform ] && \
  { echo "ERROR, final.feature_transform expected from dir $src_mainnnet_dir"; exit 1; }
   steps/nnet/train_ivector_sat.sh --copy-feats $copy_feats \
  --learn-rate $learn_rate_subnet --feature-transform $feature_transform \
  --train-tool $train_tool \
  --freeze-update true \
  --parallel-nnet-opts "$parallel_nnet_opts" \
  --parallel-nnet $nnet_init \
  --hid-layers 0 --nnet-init $main_nnet $train $valid $lang $alidir \
  $alidir $ivector_subnnet_dir || exit 1
  # (cd $ivector_subnnet_dir; [ -f final.feature_transform ] && unlink final.feature_transform)
  echo "done @ `date`"
fi
if $use_source_nnet; then
  master_dir=$src_mainnnet_dir
  step05=
else
  master_dir=$dir/master_nn
fi
[ -d $master_dir ] || mkdir $master_dir
if [ ! -z $step05 ]; then
  echo "train main nnet started @ `date`"
  check_train_ivector_data;
  [ -f $ivector_subnnet_dir/final.nnet ] || \
  { echo "ERROR, final.nnet expected from ivector_subnnet_dir $ivector_subnnet_dir"; exit 1; }
  [ ! -f $gmmdir/final.mdl ] && \
  { echo "ERROR, final.mdl is not ready in $gmmdir "; exit 1; } 
  [ ! -f $src_mainnnet_dir/final.nnet ] && \
  { echo "ERROR, final.nnet is not ready in srcnnet $src_mainnnet_dir "; exit 1; }
  num_tgt=$(hmm-info --print-args=false $gmmdir/final.mdl | grep pdfs | awk '{ print $NF }')
  soft_max_proto=$src_mainnnet_dir/soft-max.proto
  front_nnet="nnet-copy --remove-last-layers=2 $src_mainnnet_dir/final.nnet -|" 
  hid_dim=$(nnet-info "$front_nnet" | \
                  grep output-dim | tail -1 |perl -pe 's/.*output-dim//g; m/(\d+)/g; $_=$1;') || \
  { echo "ERROR, hid_dim error"; exit 1; }

  utils/nnet/make_nnet_proto.py $hid_dim $num_tgt 0  $hid_dim >$soft_max_proto || exit 1 
  soft_max_init=$src_mainnnet_dir/soft-max.init
  log=$src_mainnnet_dir/sotf-max-initialize.log
  nnet-initialize $soft_max_proto $soft_max_init 2>$log || { cat $log; exit 1; }
  main_nnet=$master_dir/nnet.init
  nnet-concat "$front_nnet" $soft_max_init $main_nnet 2> $master_dir/nnet-init.log

  steps/nnet/train_ivector_sat.sh --copy-feats $copy_feats \
  --learn-rate $learn_rate --feature-transform $feature_transform \
  --train-tool $train_tool \
  --freeze-update false \
  --parallel-nnet-opts "$parallel_nnet_opts" \
  --parallel-nnet $ivector_subnnet_dir/final.nnet \
  --hid-layers 0 --nnet-init $main_nnet $train $valid $lang $alidir \
  $alidir $master_dir || exit 1
  echo "done @ `date`"
fi
function decode_check() {
  if [ -z $graphdir ] || [ -z $testdata ] || [ -z $ivector_testdata ]; then
    echo "ERROR, you want to do decode test, but graphdir or testdata or ivector_testdata has not been specified"
    exit 1
  fi
  for f in $ivector_subnnet_dir/final.nnet  $testdata/utt2spk  $ivector_testdata/ivector.scp; do
    [ -f $f ] || { echo "ERROR, file $f expected"; exit 1;  }
  done
}
if [ ! -z $step06 ]; then
  echo "LOG: decode started @ `date`"
  decode_check;
  parallel_nnet_opts="--parallel-net=$ivector_subnnet_dir/final.nnet"
  parallel_nnet_opts="$parallel_nnet_opts --parallel-feature=scp:$ivector_testdata/ivector.scp"
  parallel_nnet_opts="$parallel_nnet_opts --parallel-nnet-level=1 --parallel-utt2spk=ark:$testdata/utt2spk"
  steps/nnet/decode_subnnet.sh --cmd "$cmd" --nj $nj  --parallel-nnet-opts "$parallel_nnet_opts" \
  $decode_opts --scoring-opts "$scoring_opts" \
  $graphdir $testdata $master_dir/$decodename || exit 1
  echo "LOG: done @ `date`"
fi
alidir=$master_dir/ali_${train_data_name}
if [ ! -z $step07 ]; then
  echo "LOG: making numerator alignment for the nnet sequential training started @ `date`"
  srcdir=$master_dir
  parallel_nnet_opts="--parallel-net=$ivector_subnnet_dir/final.nnet"
  parallel_nnet_opts="$parallel_nnet_opts --parallel-feature=scp:$ivector_data/ivector.scp"
  parallel_nnet_opts="$parallel_nnet_opts --parallel-nnet-level=1 --parallel-utt2spk=ark:$data/utt2spk"
  steps/nnet/align_subnnet.sh --cmd "$cmd" --nj $nj --parallel-nnet-opts "$parallel_nnet_opts" \
  $data $lang $master_dir $alidir || exit 1
  echo "ended @ `date`"
fi
latdir=$master_dir/denlat_${train_data_name}
if [ ! -z $step08 ]; then
  echo "LOG: making denominator lattice for the nnet sequential training started @ `date`"
  parallel_nnet_opts="--parallel-net=$ivector_subnnet_dir/final.nnet"
  parallel_nnet_opts="$parallel_nnet_opts --parallel-feature=scp:$ivector_data/ivector.scp"
  parallel_nnet_opts="$parallel_nnet_opts --parallel-nnet-level=1 --parallel-utt2spk=ark:$data/utt2spk"
  steps/nnet/make_denlats_subnnet.sh --cmd "$cmd" --nj $nj  --parallel-nnet-opts "$parallel_nnet_opts" \
  --acwt $acwt_make_denlats --config $conf_make_denlats $data $lang $master_dir $latdir || exit 1 
  echo "ended @ `date`"
fi
master_mpe_dir=$dir/master_mpe_nn
if [ ! -z $step09 ]; then
  echo "LOG: mpe-sequential training started @ `date`"
  parallel_nnet_opts="--parallel-net=$ivector_subnnet_dir/final.nnet --parallel-freeze-update=true"
  parallel_nnet_opts="$parallel_nnet_opts --parallel-feature=scp:$ivector_data/ivector.scp"
  parallel_nnet_opts="$parallel_nnet_opts --parallel-nnet-level=1 --parallel-utt2spk=ark:$data/utt2spk"
  steps/nnet/train_mpe_subnnet.sh --cmd "$train_cmd"  --acwt $acwt_make_denlats --do-smbr true \
  --parallel-nnet-opts "$parallel_nnet_opts" \
  $data $lang $master_dir $alidir $latdir $master_mpe_dir || exit 1
  echo "ended @ `date`"
fi
if [ ! -z $step10 ]; then
  echo "LOG: decode started @ `date`"
  decode_check;
  parallel_nnet_opts="--parallel-net=$ivector_subnnet_dir/final.nnet"
  parallel_nnet_opts="$parallel_nnet_opts --parallel-feature=scp:$ivector_testdata/ivector.scp"
  parallel_nnet_opts="$parallel_nnet_opts --parallel-nnet-level=1 --parallel-utt2spk=ark:$testdata/utt2spk"
  steps/nnet/decode_subnnet.sh --cmd "$cmd" --nj $nj  --parallel-nnet-opts "$parallel_nnet_opts" \
  $decode_opts --scoring-opts "$scoring_opts" \
  $graphdir $testdata $master_mpe_dir/$decodename || exit 1
  echo "done @ `date`"
fi
