#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=40
cmd=slurm.pl
steps=
graphdir=
learn_rate=0.008
validating_rate=0.1
conv_ali=false
gmmdir=
append_vector_to_feats=append-vector-to-feats

decode_cmd=steps/nnet/decode.sh
devdata=
devname=dev
decodename=decode_dev
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15 "

# end options

echo 
echo LOG: $0 $@
echo

. parse_options.sh || exit 1

function Usage {
  cat <<END

$(basename $0) [options] <data> <lang> <srcnnet> <alidir> <dir>
[options]:
--nj			# value, $nj
--cmd			# value, "$cmd"
--steps			# value, "$steps"
--graphdir		# value, "$graphdir"
--learn-rate		# value, $learn_rate
--validating-rate	# value, $validating_rate
--conv-ali              # value, $conv_ali
--gmmdir                # value, "$gmmdir" (if conv-ali specified, gmmdir should be specified as well)

--decode-cmd            # value, "$decode_cmd"
--devdata		# value, "$devdata"
--devname		# value, "$devname"
--decodename		# value, "$decodename"
--decode_opts		# value, "$decode_opts"
--scoring_opts		# value, "$scoring_opts"

[steps]:
1: split data
2: train dnn
3: making graph, if necessary
4: test dev data, if any
END
}

if [ $# -ne 5 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
srcnnet=$3
alidir=$4
dir=$5

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $dir ] || mkdir -p $dir

train=$dir/train
valid=$dir/valid
if [ ! -z $step01 ]; then
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $train \
  $data  $valid || exit 1
fi

if $conv_ali; then
  [ -z $gmmdir ] && { echo "ERROR, you mean to do conv_ali, but no gmmdir specified"; exit 1; } 
  for x in $alidir/final.mdl $gmmdir/final.mdl $gmmdir/tree; do
    [ -f $x ] || { echo "ERROR, file $x expected"; exit 1; }
  done
  echo "conv_ali started @ `date`"
  aliname=$(echo $alidir|perl -pe 'use File::Basename; chomp; s/\/$//; $_=basename($_);')
  newali=$gmmdir/${aliname}-converted
  source/egs/swahili/convert-ali.sh $alidir  $gmmdir $newali || exit 1
  alidir=$newali
  echo "conv_ali done @ `date`"
fi
[ -z $gmmdir ] && gmmdir=$(dirname $alidir)
[ ! -f $gmmdir/final.mdl ] && \
{ echo "ERROR, final.mdl is not ready in $gmmdir $alidir"; exit 1; } 
[ ! -f $srcnnet/final.nnet ] && \
  { echo "ERROR, final.nnet is not ready in srcnnet $srcnnet "; exit 1; }
  num_tgt=$(hmm-info --print-args=false $gmmdir/final.mdl | grep pdfs | awk '{ print $NF }')
  soft_max_proto=$srcnnet/soft-max.proto
  front_nnet="nnet-copy --remove-last-layers=2 $srcnnet/final.nnet -|" 
  hid_dim=$(nnet-info "$front_nnet" | \
                  grep output-dim | tail -1 |perl -pe 's/.*output-dim//g; m/(\d+)/g; $_=$1;') || \
  { echo "ERROR, hid_dim error"; exit 1; }

if [ ! -z $step02 ]; then
  utils/nnet/make_nnet_proto.py $hid_dim $num_tgt 0  $hid_dim >$soft_max_proto || exit 1 
  soft_max_init=$srcnnet/soft-max.init
  log=$srcnnet/sotf-max-initialize.log
  nnet-initialize $soft_max_proto $soft_max_init 2>$log || { cat $log; exit 1; }
  nnet_init=$dir/nnet.init
  nnet-concat "$front_nnet" $soft_max_init $nnet_init 2> $dir/nnet-init.log
  feature_transform=$srcnnet/final.feature_transform
  steps/nnet/train.sh --learn-rate $learn_rate --feature-transform $feature_transform \
  --hid-layers 0 --nnet-init $nnet_init $train $valid $lang $alidir \
  $alidir $dir || exit 1
fi

if [ ! -z $step03 ]; then
  echo "Making graph started @ `date`"
  [ -z $graphdir ] && \
  { echo "ERROR, graphdir not specified"; exit 1; }
  srcdir=$(dirname $graphdir)
  [ -f $srcdir/final.mdl ] || \
  { echo "ERROR, final.mdl expected from srcdir $srcdir"; exit 1; }
  if [ ! -f $graphdir/HCLG.fst ]; then
    utils/mkgraph.sh $lang $srcdir  $graphdir
  fi
  echo "Done @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "Decoding started @ `date`"
  effect_nj=$(wc -l < $devdata/spk2utt)
  [ $effect_nj -gt $nj ] && effect_nj=$nj
  [ -f $graphdir/HCLG.fst ] || \
  { echo "ERROR, HCLG.fst expected from $graphdir"; exit 1; }
  $decode_cmd ${append_ivector_dev:+--append-vector-to-feats "$append_ivector_dev"} --cmd "$cmd" --nj $effect_nj \
  --scoring-opts "$scoring_opts" \
  $decode_opts $graphdir $devdata $dir/$decodename || exit 1
fi

echo "Done @ `date` !"
