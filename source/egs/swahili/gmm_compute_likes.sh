#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd="slurm.pl"
nj=40
steps=

model=
compress=true
transform_dir=
thread_opts=
# end options

. parse_options.sh || exit 1

function PrintOptions {
  cat<<END
[options]:
--cmd				# value, "$cmd"
--nj				# value, $nj
--steps				# value, "$steps"

--model				# value, "$model"
--compress			# value, $compress
--transform_dir			# value, "$transform_dir"
--thread_opts			# value, "$thread_opts"
[e.g]:
source/egs/swahili/gmm_compute_likes.sh  --cmd run.pl --nj 10 \\
data/train/plp_pitch exp/gmm.plp_pitch/tri4a  data/train/gmmlikes  feats/gmmlikes/train
END
} 

if [ $# -ne 4 ]; then
  echo
  echo "Usage: $(basename $0) [options] <srcdata> <srcdir> <data> <featdir>"
  PrintOptions
  echo && exit 1
fi

srcdata=$1
srcdir=$2
data=$3
featdir=$4

max_nj=$(wc -l < $srcdata/spk2utt)
[ ! -z $max_nj ] || \
{ echo "ERROR, source data $srcdata is not ready"; exit 1; }
[ $nj -gt $max_nj ] && nj=$max_nj
[ -f $srcdir/final.mdl ] || \
{ echo "ERROR, file final.mdl expected from srcdir $srcdir"; exit 1; }
[ -z $transform_dir ] && transform_dir=$srcdir
if [ -f $transform_dir/trans.1 ]; then
  nj=$(cat $transform_dir/num_jobs)
  [ ! -z $nj ] ||\
  { echo "ERROR, we found trans.1 but no num_jobs"; exit 1; }
fi

x=$featdir
featdir=$x/data
logdir=$x/log
mkdir -p $x/{data,log}

split_data.sh $srcdata $nj || exit 1;
sdata=$srcdata/split$nj
splice_opts=`cat $srcdir/splice_opts 2>/dev/null` # frame-splicing options.
cmvn_opts=`cat $srcdir/cmvn_opts 2>/dev/null`
delta_opts=`cat $srcdir/delta_opts 2>/dev/null`
[ -d $data ] || mkdir -p $data
cp $srcdata/*  $data
rm $data/{feats.scp,cmvn.scp}

## Set up the unadapted features "$sifeats"
if [ -f $srcdir/final.mat ]; then feat_type=lda; else feat_type=delta; fi
echo "$0: feature type is $feat_type";
case $feat_type in
  delta) feats="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- | add-deltas $delta_opts ark:- ark:- |";;
  lda) feats="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- | splice-feats $splice_opts ark:- ark:- | transform-feats $srcdir/final.mat ark:- ark:- |";;
  *) echo "Invalid feature type $feat_type" && exit 1;
esac
##
if [ -f $transform_dir/trans.1 ]; then
  feats="$feats transform-feats --utt2spk=ark:$sdata/JOB/utt2spk ark:$transform_dir/trans.JOB ark:- ark:- |"
fi
[ -z $model ] && model=$srcdir/final.mdl
$cmd JOB=1:$nj $logdir/gmm-compute-likes.JOB.log \
source/code/gmm-compute-likes$thread_opts $model "$feats" ark:- \| \
copy-feats --compress=$compress ark:- ark,scp:$featdir/gmm-likes.JOB.ark,$featdir/gmm-likes.JOB.scp || exit 1

# concatenate the .scp files together.
for n in $(seq $nj); do
  cat $featdir/gmm-likes.$n.scp || exit 1;
done > $data/feats.scp

steps/compute_cmvn_stats.sh  $data $logdir $featdir || exit 1

utils/fix_data_dir.sh $data 

echo "Done @ `date` !"
