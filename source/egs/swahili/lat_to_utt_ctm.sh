#!/bin/bash

. path.sh
. cmd.sh
echo 
echo "LOG: $0 $@"
echo
# begin options
cmd=run.pl
steps=
prune_beam=5
word_ins_penalty=0.5
lmwt=10
model=
silence_word_int=0
# end options

. parse_options.sh || exit 1

function Usage {
  cat<<END
Usage: $(basename $0) [options] <data> <lang|graph> <latdir>
[options]:
--cmd			# value, "$cmd"
--steps			# value, "$steps"
--prune-beam		# value, $prune_beam
--word-ins-penalty	# value, $word_ins_penalty
--lmwt			# value, $lmwt
--model			# value, "$model"
--silence-word-int	# value, $silence_word_int

[steps]:
1: lattice to ctm 
2: remove silence words to generate utterance ctm
3: generate the final ctm file
[example]:

END
}
if [ $# -ne 3 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
latdir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
[ -z $model ] && model=$(dirname $latdir)/final.mdl

if [ ! -f $data/reco2file_and_channel ]; then
  if [ ! -f $data/segments ]; then
    echo "ERROR, file segments is needed"; exit 1
  fi
  echo "no reco2file_and_chanlle found, we are making it from segments"
  cat $data/segments | \
  awk '{print $2}' | sort -u | \
  awk '{printf("%s %s 1\n", $1, $1);}' > $data/reco2file_and_channel
fi

for f in $lang/words.txt $lang/phones/word_boundary.int \
      $model $data/segments  $data/reco2file_and_channel  $latdir/lat.1.gz; do
  [ -f $f ] || \
  { echo "ERROR, expecting file $f to exist"; exit 1; }
done

ctmdir=$latdir/ctm_${lmwt}
mkdir -p $ctmdir
num_jobs=$(cat $latdir/num_jobs)
name=$(basename $data)
if [ ! -z $step01 ]; then
  echo "lattice to ctm started @ `date`"
  $cmd JOB=1:$num_jobs $ctmdir/log/get_ctm.JOB.log \
  set -e -o pipefail \; \
  lattice-scale --inv-acoustic-scale=$lmwt "ark:gunzip -c $latdir/lat.JOB.gz|" ark:- \| \
  lattice-add-penalty --word-ins-penalty=$word_ins_penalty ark:- ark:- \| \
  lattice-prune --beam=$prune_beam ark:- ark:- \| \
  lattice-align-words --silence-label=$silence_word_int $lang/phones/word_boundary.int $model ark:- ark:- \| \
  lattice-to-ctm-conf --decode-mbr=true ark:- - \| \
  utils/int2sym.pl -f 5 $lang/words.txt  \| tee $ctmdir/$name.JOB.ctm || exit 1
  echo "Done @ `date`!"
fi

if [ ! -z $step02 ]; then
  cat $ctmdir/$name.*.ctm | grep -v -E '\[NOISE|LAUGHTER|VOCALIZED-NOISE\]' | \
  grep -v -E '<UNK>|%HESITATION|\(\(\)\)' | \
  grep -v -E '<eps>' | \
  grep -v -E '<noise>' | \
  grep -v -E '<silence>' | \
  grep -v -E '<hes>' | \
  grep -v -E '<unk>' | \
  grep -v -E '<v-noise>' | \
  grep -v '<' | \
  perl -e '@list = (); %list = ();
    while(<>) {
      chomp; 
      @col = split(" ", $_); 
      push(@list, $_);
      $key = "$col[0]" . " $col[1]"; 
      $list{$key} = 1;
    } 
    foreach(sort keys %list) {
      $key = $_;
      foreach(grep(/$key/, @list)) {
        print "$_\n";
      }
    }' > $ctmdir/$name.utt.ctm
fi

if [ ! -z $step03 ]; then
  cat $ctmdir/$name.utt.ctm | \
  utils/convert_ctm.pl $data/segments $data/reco2file_and_channel \
  > $ctmdir/$name.ctm || exit 1;
fi

echo "Done @ `date` !"
