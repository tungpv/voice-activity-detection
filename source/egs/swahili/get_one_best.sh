#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=slurm.pl
steps=
min_lmwt=9
max_lmwt=20
# end options

echo 
echo LOG: $0 $@
echo

. parse_options.sh || exit 1
function PrintOptions {
  cat<<END
[options]:
--cmd			# value, "$cmd"
--steps			# value, "$steps"
--min-lmwt		# value, $min_lmwt
--max-lmwt		# value, $max_lmwt

[steps]:
1: generate integer based transcription
2: convert integet to text based transcription
END
}
if [ $# -ne 3 ]; then
  echo
  echo "Usage: $(basename $0) [options] <data> <lang> <decode-dir>"
  PrintOptions;
  echo && exit 1
fi

data=$1
lang=$2
dir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $dir ] || mkdir -p $dir/scoring/log

symtab=$lang/words.txt 
for f in $symtab $dir/lat.1.gz; do
  [ -f $f ] || \
  { echo "ERROR, file $f expected"; exit 1; }
done
if [ ! -z $step01 ]; then
  $cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring/log/best_path.LMWT.log \
  lattice-best-path --lm-scale=LMWT --word-symbol-table=$symtab \
    "ark:gunzip -c $dir/lat.*.gz|" ark,t:$dir/scoring/LMWT.tra || exit 1;
fi

if [ ! -z $step02 ]; then
  $cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring/log/score.LMWT.log \
  cat $dir/scoring/LMWT.tra \| \
  utils/int2sym.pl -f 2- $symtab  ">&" $dir/scoring/LMWT.txt || exit 1
fi
