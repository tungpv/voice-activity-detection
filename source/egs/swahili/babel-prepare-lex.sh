#!/bin/bash

[ ! -f ./lang.conf ] && echo 'Language configuration does not exist! Use the configurations in conf/lang/* as a startup' && exit 1
[ ! -f ./conf/common_vars.sh ] && echo 'the file conf/common_vars.sh does not exist!' && exit 1

. conf/common_vars.sh || exit 1;
. ./lang.conf || exit 1;

[ -f local.conf ] && . ./local.conf

. ./utils/parse_options.sh

if [ $# -ne 4 ]; then
  echo
  echo "Usage: $(basename $0) <babel_train_data_dir> <babel_lexicon_file> <localdir> <langdir>"
  echo && exit 1
fi

train_data_dir=$1
lexicon_file=$2
localdir=$3
langdir=$4

[ -d $localdir ] || mkdir -p $localdir
if [[ ! -f $localdir/lexicon.txt ]]; then
  echo ---------------------------------------------------------------------
  echo "Preparing lexicon in data/local on" `date`
  echo ---------------------------------------------------------------------
  source/egs/swahili/make_lexicon_subset.sh $train_data_dir/transcription $lexicon_file \
  $localdir/filtered_lexicon.txt
  local/prepare_lexicon.pl  --phonemap "$phoneme_mapping" \
    $lexiconFlags $localdir/filtered_lexicon.txt $localdir
fi

mkdir -p $langdir
if [[ ! -f $langdir/L.fst ]]; then
  echo ---------------------------------------------------------------------
  echo "Creating L.fst etc in data/lang on" `date`
  echo ---------------------------------------------------------------------
  utils/prepare_lang.sh \
    --share-silence-phones true \
    $localdir $oovSymbol $localdir/tmp.lang  $langdir
fi


