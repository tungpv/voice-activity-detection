#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo 

# begin options
cmd=slurm.pl
nj=40
steps=
# end options

. parse_options.sh

function Usage {
 cat<<END
 Usage: $0 [options] <src-dict-dir>  <tgtdir>
 [options]:
 [steps]:
 1: prepare word dict (remove unwanted phone)
 2: prepare lang (no position dependent phone)
 3: train gmm-hmm to tri4a
 4: convert triphone alignment to mono-phone alignment
 5: nnet transfer learning (lstm)
 6: make phone dict and lang
 7: make phone loop grammar
 [examples]:
 $0 --steps 1  kws2016/flp-grapheme/data/local-merge kws2016/flp-grapheme/exp/mono/phone-lstm

END
}

if [ $# -ne  2 ]; then
  Usage && exit 1
fi

sdictdir=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
dictdir=$tgtdir/dict
[ -d $dictdir ] || mkdir -p $dictdir
if [ ! -z $step01 ]; then
  echo "## LOG: step01,prepare dict @`date`"
  cp $sdictdir/silence_phones.txt $dictdir/silence_phones.txt
  cp $sdictdir/optional_silence.txt $dictdir/optional_silence.txt
  egrep  -v 'á|é' $sdictdir/nonsilence_phones.txt > $dictdir/nonsilence_phones.txt  
  cat $dictdir/silence_phones.txt | awk '{printf ("%s ",$1);}END{printf "\n";}'\
  > $dictdir/extra_questions.txt
  cat $dictdir/nonsilence_phones.txt | awk '{printf ("%s ",$1);}END{printf "\n";}'\
  >> $dictdir/extra_questions.txt
  egrep -v 'á|é' $sdictdir/lexicon.txt > $dictdir/lexicon.txt
  original_num=$(wc -l < $sdictdir/lexicon.txt)
  update_num=$(wc -l < $dictdir/lexicon.txt)
  echo "## LOG: step01, vocabulary from #$original_num to #$update_num "
  echo "## LOG: ended @ `date`"
fi
word_lang=$tgtdir/word-lang
if [ ! -z $step02 ]; then
  echo "## LOG: step02, prepare lang @ `date`"
  utils/prepare_lang.sh --position-dependent-phones false $dictdir "<unk>" $word_lang/tmp $word_lang || exit 1
  echo "## LOG: step02, done @ `date`"
fi

if [ ! -z $step03 ]; then
  echo "## LOG: step03, train gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd slurm.pl --nj 40  --steps 2,3,4,5,6,7 \
  --pdf-num 5000 --state-num 75000 --devdata kws2016/flp-grapheme/data/dev/plp_pitch \
  kws2016/flp-grapheme/data/train/plp_pitch $word_lang $tgtdir || exit 1
  echo "## LOG: step03, done @ `date`"
fi
mono_alidir=$tgtdir/mono0a/ali-train-converted
if [ ! -z $step04 ]; then
  echo "## LOG: step04, convert ali @ `date`"
  source/egs/swahili/convert-ali.sh $tgtdir/tri4a/ali_train $tgtdir/mono0a $mono_alidir || exit 1
  echo "## LOG: step04, done @ `date`"
fi
srcnnetdir=kws2016/flp-grapheme/exp/mono/lstm-c800-r512/lstm-nnet
tgtnnetdir=$tgtdir/lstm-nnet
if [ ! -z $step05 ]; then
  echo "## LOG: step05, transfer learning @ `date`"
  if [ -z $srcnnetdir ] || [ ! -f $srcnnetdir/final.nnet ]; then 
     echo "step05, srcnnetdir expected"; exit 1; 
  fi
  source/egs/swahili/run-cross-train-nnet-v2.sh \
  --steps 1,2,3  --learn-rate 0.0001 --train-tool "nnet-train-lstm-streams" \
  --train-tool-opts "--num-stream=32  --targets-delay=5" \
  --scheduler-opts  "--momentum 0.9 --halving-factor 0.5" \
  kws2016/flp-grapheme/data/train/fbank_pitch  $word_lang \
  $srcnnetdir $mono_alidir $tgtnnetdir || exit 1 
  echo "## LOG: step05, done @ `date`"
fi

if [ ! -z $step06 ]; then
  echo "## LOG: step06, make phone dict and lang @ `date`"
  source/egs/quesst/make-phone-dict.sh --noise-word "<noise> <silence> <unk> <v-noise>" \
  kws2016/flp-grapheme/exp/mono/phone-lstm/dict kws2016/flp-grapheme/exp/mono/phone-lstm/phone-dict || exit 1
  utils/prepare_lang.sh --position-dependent-phones false \
  kws2016/flp-grapheme/exp/mono/phone-lstm/phone-dict "<unk>"  kws2016/flp-grapheme/exp/mono/phone-lstm/phone-lang/tmp \
  kws2016/flp-grapheme/exp/mono/phone-lstm/phone-lang || exit 1
  echo "## LOG: step06, done @ `date`"
fi

if [ ! -z $step07 ]; then
  echo "## LOG: step07, make phone loop grammar @ `date`"
  source/egs/quesst/make-phone-loop.sh kws2016/flp-grapheme/exp/mono/phone-lstm/phone-dict/lexicon.txt \
  kws2016/flp-grapheme/exp/mono/phone-lstm/phone-loop || exit 1
  local/arpa2G.sh kws2016/flp-grapheme/exp/mono/phone-lstm/phone-loop/lm.gz kws2016/flp-grapheme/exp/mono/phone-lstm/phone-lang \
  kws2016/flp-grapheme/exp/mono/phone-lstm/phone-lang || exit 1
  utils/mkgraph.sh --mono kws2016/flp-grapheme/exp/mono/phone-lstm/phone-lang \
  $tgtdir/mono0a $tgtdir/mono0a/phone-graph || exit 1
  echo "## LOG: step07, done @ `date`"
fi
if [ ! -z $step08 ]; then
  echo "## LOG: step08, decode for phone lattice @ `date`"
  steps/nnet/decode.sh --nj 40 --cmd slurm.pl --acwt 0.1 --beam 13 --lattice-beam 8 --max-mem 500000000 --skip-scoring true \
  $tgtdir/mono0a/phone-graph ../w2015/quesst/q1/data/q14.dev/fbank_pitch $tgtnnetdir/decode-q14.dev || exit 1
  echo "## LOG: step08, done @ `date`"
fi 
