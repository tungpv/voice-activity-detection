#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
sdir=
phone_lexicon=
# end options

. parse_options.sh || exit 1

function PrintOptions {
  cat<<END
[options]:
--steps			# value, "$steps"
--sdir			# value, "$sdir"
--phone-lexicon		# value, "$phone_lexicon"
END
}

if [ $# -ne 4 ]; then
  echo
  echo "Usage: $(basename $0) [options] <sdata> <slang> <alidir> <data>"
  PrintOptions;
  echo && exit 1
fi

sdata=$1
slang=$2
alidir=$3
data=$4

[ -z $sdir ] && sdir=$(dirname $alidir)
for x in $slang/phones.txt $sdir/final.mdl $alidir/ali.1.gz; do
  [ -f $x ] || \
  { echo "ERROR, file $x expected"; exit 1; }
done
[ -d $data ] || mkdir -p $data

cp $sdata/* $data/ 
rm $data/text 2>/dev/null

ali-to-phones  $sdir/final.mdl \
"ark:gzip -cd $alidir/ali.*.gz|" ark,t:- | \
int2sym.pl -f 2- $slang/phones.txt - > $data/.text || exit 1

if [ ! -z $phone_lexicon ]; then
  [ -f $phone_lexicon ] || \
  { echo "ERROR, file phone_lexicon does not exist"; exit 1; }
  cat $data/.text | \
  source/egs/swahili/map_sym.pl --from=2 --left-to-right=false \
  $phone_lexicon  > $data/text
fi

utils/fix_data_dir.sh $data 

echo "Done !"
