#!/bin/bash

[ ! -f ./lang.conf ] && echo 'Language configuration does not exist! Use the configurations in conf/lang/* as a startup' && exit 1
[ ! -f ./conf/common_vars.sh ] && echo 'the file conf/common_vars.sh does not exist!' && exit 1
. conf/common.limitedLP.sh
. conf/common_vars.sh || exit 1;
. ./lang.conf || exit 1;

[ -f local.conf ] && . ./local.conf

. ./utils/parse_options.sh

set -e           #Exit on non-zero return code from any command
set -o pipefail  #Exit if any of the commands in the pipeline will 

if [ $# -ne 5 ]; then
  echo
  echo "Usage: $(basename $0) <train_data_dir> <train_data_list> <link_datadir> <lexicon_file> <data>" 
  echo && exit 1
fi

train_data_dir=$1
train_data_list=$2
link_datadir=$3
lexicon_file=$4
data=$5

local/make_corpus_subset.sh "$train_data_dir" "$train_data_list" $link_datadir
train_data_dir=`readlink -f $link_datadir`

train_data_dir=`readlink -f $link_datadir`

[ -d $data ] || mkdir -p $data
local/prepare_acoustic_training_data.pl \
--vocab $lexicon_file --fragmentMarkers \-\*\~ \
$train_data_dir $data > $data/skipped_utts.log

