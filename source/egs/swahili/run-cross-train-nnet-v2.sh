#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=40
cmd=slurm.pl
steps=
graphdir=
learn_rate=0.008
validating_rate=0.1
conv_ali=false
scheduler_opts=
train_tool="nnet-train-frmshuff"
train_tool_opts=
train_data_dir=
gmmdir=
append_vector_to_feats=append-vector-to-feats
softmax_layer_tuning=false
decode_cmd=steps/nnet/decode.sh
devdata=
devname=dev
decodename=decode_dev
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15 "

# end options

echo 
echo LOG: $0 $@
echo

. parse_options.sh || exit 1

function Usage {
  cat <<END
 Usage: $0 [options] <data> <lang> <srcnnet> <alidir> <dir>
 [options]:
 --nj			     # value, $nj
 --cmd		       	     # value, "$cmd"
 --steps		     # value, "$steps"
 --graphdir		     # value, "$graphdir"
 --learn-rate		     # value, $learn_rate
 --validating-rate	     # value, $validating_rate
 --conv-ali                  # value, $conv_ali

 --scheduler-opts            # value, "$scheduler_opts"
 --train-tool                # value, "$train_tool"
 --train_tool_opts           # value, "$train_tool_opts"
 --train-data-dir            # value, "$train_data_dir"

 --gmmdir                    # value, "$gmmdir" (if conv-ali specified, gmmdir should be specified as well)
 --append-vector-to-feats    # value, "$append_vector_to_feats"
 --softmax-layer-tuning      # value, $softmax_layer_tuning

 --decode-cmd                # value, "$decode_cmd"
 --devdata		     # value, "$devdata"
 --devname		     # value, "$devname"
 --decodename		     # value, "$decodename"
 --decode_opts		     # value, "$decode_opts"
 --scoring_opts		     # value, "$scoring_opts"

 [steps]:
 1: split data
 2: train dnn
 3: making graph, if necessary
 4: test dev data, if any

 [example]:

 $0 --steps 1,2,3,4 --devdata /home/hhx502/w2016/sg-en-i2r/data/fstd-demo-updated/fbank-pitch \
  --graphdir /home/hhx502/w2016/sg-en-i2r/exp-merge/tri4a/graph \
  --decodename decode-fstd-demo-updated \
  /home/hhx502/w2016/sg-en-i2r/data/train-merge/fbank-pitch \
  /home/hhx502/w2016/sg-en-i2r/data/lang-int-01 \
  /home2/hhx502/kws2016/mling30-dnn/dnn-m30-layers6 \
  /home/hhx502/w2016/sg-en-i2r/exp-merge/tri4a/ali_train \
  /home2/hhx502/sg-en-i2r/exp-nnet-ml30-tl-merge291

 $0 --steps 1,2,3,4 --devdata kws2016/vllp-grapheme/data/dev/fbank_pitch --graphdir kws2016/flp-grapheme/exp/mono/tri4a/graph \
 --decodename "decode-dev" \
 kws2016/flp-grapheme/data/train/fbank_pitch  kws2016/flp-grapheme/data/lang \
 /home2/hhx502/kws2016/mling24x/dnn-m24-layers6 kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mling-cross/mling24x-nnet5a

 $0 --steps 1,2  --learn-rate 0.0001 --train-tool "nnet-train-lstm-streams" \
 --train-tool-opts "--num-stream=4 --targets-delay=5" \
 --scheduler-opts  "--momentum 0.9 --halving-factor 0.5" \
 kws2016/flp-grapheme/data/train/fbank_pitch  kws2016/flp-grapheme/exp/mono/phone-nnet5a/lang \
 kws2016/flp-grapheme/exp/mono/lstm-c800-r512/lstm-nnet kws2016/flp-grapheme/exp/mono/phone-nnet5a/mono0a/ali_train \
 kws2016/flp-grapheme/exp/mono/phone-nnet5a/lstm-nnet 

 $0 --steps 1,2  --learn-rate 0.0001 --train-tool "nnet-train-lstm-streams" \
 --train-tool-opts "--num-stream=32 --targets-delay=5" \
 --scheduler-opts  "--momentum 0.9 --halving-factor 0.5" \
 kws2016/flp-grapheme/data/train/fbank_pitch  kws2016/flp-grapheme/data/lang \
 kws2016/flp-grapheme/exp/mono/lstm-c800-r512/lstm-nnet kws2016/flp-grapheme/exp/mono/tri4a/ali_train \
 kws2016/flp-grapheme/exp/mono/tl-lstm-c800-r512 

END
}

if [ $# -ne 5 ] || [ -z $steps ]; then
  Usage && exit 1
fi

data=$1
lang=$2
srcnnet=$3
alidir=$4
dir=$5

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $dir ] || mkdir -p $dir
if [ ! -z $train_data_dir ]; then
  train=$train_data_dir/train
  valid=$train_data_dir/valid
  for x in $train $valid; do 
    [ -d $x ] || { echo "$0: train/valid folder expected"; exit 1; }
  done 
else
  train=$dir/train
  valid=$dir/valid
  if [ ! -z $step01 ]; then
    source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
    --random true \
    --data2 $train \
    $data  $valid || exit 1
  fi
fi
if $conv_ali; then
  [ -z $gmmdir ] && { echo "ERROR, you mean to do conv_ali, but no gmmdir specified"; exit 1; } 
  for x in $alidir/final.mdl $gmmdir/final.mdl $gmmdir/tree; do
    [ -f $x ] || { echo "ERROR, file $x expected"; exit 1; }
  done
  echo "conv_ali started @ `date`"
  aliname=$(echo $alidir|perl -pe 'use File::Basename; chomp; s/\/$//; $_=basename($_);')
  newali=$gmmdir/${aliname}-converted
  source/egs/swahili/convert-ali.sh $alidir  $gmmdir $newali || exit 1
  alidir=$newali
  echo "conv_ali done @ `date`"
fi
[ -z $gmmdir ] && gmmdir=$(dirname $alidir)
[ ! -f $gmmdir/final.mdl ] && \
{ echo "ERROR, final.mdl is not ready in $gmmdir $alidir"; exit 1; } 
[ ! -f $srcnnet/final.nnet ] && \
  { echo "ERROR, final.nnet is not ready in srcnnet $srcnnet "; exit 1; }
  num_tgt=$(hmm-info --print-args=false $gmmdir/final.mdl | grep pdfs | awk '{ print $NF }')
  soft_max_proto=$srcnnet/soft-max.proto
  front_nnet="nnet-copy --remove-last-layers=2 $srcnnet/final.nnet -|" 
  front_nnet=$srcnnet/final-remove-last-two-layer.nnet
  nnet-copy --remove-last-layers=2 $srcnnet/final.nnet $front_nnet
  hid_dim=$(nnet-info "$front_nnet" | \
                  grep output-dim | tail -1 |perl -pe 's/.*output-dim//g; m/(\d+)/g; $_=$1;') || \
  { echo "ERROR, hid_dim error"; exit 1; }

if [ ! -z $step02 ]; then
  utils/nnet/make_nnet_proto.py $hid_dim $num_tgt 0  $hid_dim >$soft_max_proto || exit 1 
  soft_max_init=$srcnnet/soft-max.init
  log=$srcnnet/sotf-max-initialize.log
  nnet-initialize $soft_max_proto $soft_max_init 2>$log || { cat $log; exit 1; }
  if $softmax_layer_tuning; then
    nnet_init=$soft_max_init
    feature_transform=$srcnnet/concat.feature_transform
    nnet-concat $srcnnet/final.feature_transform "$front_nnet"  $feature_transform 2>$dir/concat-init.log
  else
    nnet_init=$dir/nnet.init
    nnet-concat "$front_nnet" $soft_max_init $nnet_init 2> $dir/nnet-init.log
    feature_transform=$srcnnet/final.feature_transform
  fi
  steps/nnet/train.sh --learn-rate $learn_rate --feature-transform $feature_transform \
  --copy-feats-tmproot /local/hhx502 \
  --train-tool "$train_tool" \
  ${scheduler_opts:+ --scheduler-opts "$scheduler_opts"} \
  ${train_tool_opts:+ --train-tool-opts "$train_tool_opts"} \
  --hid-layers 0 --nnet-init $nnet_init $train $valid $lang $alidir \
  $alidir $dir || exit 1
  if $softmax_layer_tuning; then
    echo "LOG: regenerate final.feature_transform and final.nnet"
    (cd $dir; unlink final.feature_transform)
    cp -L $srcnnet/final.feature_transform  $dir/final.feature_transform
    nnet-concat "$front_nnet" $dir/final.nnet $dir/final-concat.nnet
    (cd $dir; unlink final.nnet; ln -s final-concat.nnet final.nnet )
  fi
fi

if [ ! -z $step03 ]; then
  echo "Decoding started @ `date`"
  effect_nj=$(wc -l < $devdata/spk2utt)
  [ $effect_nj -gt $nj ] && effect_nj=$nj
  [ -f $graphdir/HCLG.fst ] || \
  { echo "$0: HCLG.fst expected from $graphdir"; exit 1; }
  [ -z $devdata ] && { echo "$0: devdata not specified"; exit 1; }
  $decode_cmd ${append_ivector_dev:+--append-vector-to-feats "$append_ivector_dev"} --cmd "$cmd" --nj $effect_nj \
  --scoring-opts "$scoring_opts" \
  $decode_opts $graphdir $devdata $dir/$decodename || exit 1
fi

echo "Done @ `date` !"
