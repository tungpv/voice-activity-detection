#!/usr/bin/perl
use warnings;
use strict;
use open qw(:std :utf8);

print STDERR "$0: stdin expected\n";

while(<STDIN>) {
  chomp;
  my @A = split(/ /);
  for(my $i = 3; $i < @A; $i++) {
    my $w = $A[$i];
    print lc $w, "\n";
  }
}
print STDERR "$0: stdin ended\n";
