#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=slurm.pl
nj=40
steps=
lmwt=
ctmfile=
# end options

. parse_options.sh || exit 1
function Usage {
  cat <<END

Usage: $(basename $0) [options] <data> <lang> <lat_or_ali_dir>
[options]:
--cmd                                  # value, "$cmd"
--nj                                   # value, $nj
--steps                                # value, "$steps"
--lmwt                                 # value, $lmwt, used in decoding case
--ctmfile                              # value, "$ctmfile"

[steps]:
1: make utterance ctm from alignment or lattice
2: computing vad from utterance ctm, and making new data

END
}
if [ $# -ne 3 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
lat_or_ali_dir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  echo "getting ctm  started @ `date`"
  if [ ! -z $lmwt ]; then
    latdir=$lat_or_ali_dir
    steps/get_ctm.sh --cmd "$cmd" --use-segments false --min-lmwt $lmwt \
    --max-lmwt $lmwt  $data  $lang $latdir || exit 1
    ctmfile=$latdir/score_${lmwt}/$(basename $data).ctm
  else
    alidir=$lat_or_ali_dir 
    steps/get_train_ctm.sh --cmd "$cmd" --use-segments false $data $lang $alidir || exit 1
    ctmfile=$alidir/utt_ctm
  fi
  echo "ended @ `date`"
fi

if [ ! -z $step02 ]; then
  if [ -z "$ctmfile" ]; then
    echo "ERROR, ctmfile expected" && exit 1
  fi
  for x in $ctmfile $data/feats.scp; do
    [ -f $x ] || \
    { echo "ERROR, file $x expected"; exit 1; }
  done
  echo "making vad scp started @ `date`"
  feat=$(head -1 $data/feats.scp |perl -pe 'use File::Basename; chomp; m/(\S+)\s+(.*)/; $_ = dirname($2);')
  logdir=$(dirname $feat)/log
  $cmd $logdir/compute-vad-from-utt-ctm.log \
  source/code/compute-vad-from-utt-ctm  "grep -E -v -h '<silence>|<noise>|<v-noise>' $ctmfile |" \
  scp:$data/feats.scp ark,scp:$feat/vad.ark,$data/vad.scp || exit 1   
  echo "ended @ `date`"
fi

exit 0
