#!/usr/bin/perl 
use warnings;
use strict;
use open qw(:std :utf8);
use utf8;

@ARGV == 1 or die "$0: <source_lexicon expected> ", (scalar @ARGV), "\n";
my ($source_lex_file) = @ARGV;

my %vocab = ();
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  if(/(\S+)/) {
    $vocab{$1} = 0;
  }
}
$vocab{"<hes>"} = 0;
print STDERR "$0: stdin ended\n";

open(FILE, "$source_lex_file") or die "$0: ERROR, file $source_lex_file cannot open\n";
while(<FILE>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($w, $pron) = (lc $1, $2);
  if(exists $vocab{$w}) {
    $vocab{$w} ++;
    print "$w\t$pron\n";
  } 
}
close FILE;
for my $w (keys %vocab) {
  print STDERR "word $w is not seen\n" if($vocab{$w} == 0);
}

