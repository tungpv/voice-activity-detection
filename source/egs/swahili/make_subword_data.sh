#!/bin/bash

. path.sh
. cmd.sh

# begin options

# end options

. parse_options.sh || exit 1
function PrintOption {
 cat<<END
[options]:

END
}
if [ $# -ne 3 ]; then
  echo
  echo "Usage: $(basename $0) [options] <sdata> <word2subword_map> <data>"
  PrintOption
  echo && exit 1
fi

sdata=$1
word2subword_map=$2
data=$3

for x in $sdata/text; do
  [ -f $x ] || \
  { echo "ERROR, file $x expected"; exit 1; }
done

[ -d $data ] || mkdir -p $data
cp $sdata/* $data
rm $data/text
cat $sdata/text | \
source/test/openkws_transform_text.pl "gzip -cd $word2subword_map|" > $data/text
utils/fix_data_dir.sh $data

echo "Done !"






