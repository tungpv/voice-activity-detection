#!/usr/bin/perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\nExample: cat phone_trans.txt | $0 phone-to-silence-word-csl > new_phone_trans.txt\n\n";
}
my ($csl) = @ARGV;
my @A = split(/:/, $csl);
my %vocab = ();
for(my $i = 0; $i < @A; $i ++) {
  my $entry = $A[$i];
  my @B = split(/,/, $entry);
  die "## ERROR, $0, bad csl $csl\n" if scalar @B != 2;
  my $word = $B[1];
  my $phone = $B[0];
  my $single_phone = $phone . "_S";
  my $begin_phone = $phone . "_B";
  my $int_phone = $phone . "_I";
  my $end_phone = $phone . "_E";
  for my $x  ($phone, $single_phone, $begin_phone, $int_phone, $end_phone) {
    $vocab{$x} = $word;
  }
}

print STDERR "## LOG: $0, stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my $utt = $1;
  my @A  = split(/\s+/, $2);
  for(my $i = 0; $i < @A; $i ++) {
    my $phone = $A[$i];
    if (exists $vocab{$phone}) {
      $phone = $vocab{$phone};
    }
    $phone =~ s/_[SBIE]$//g;
    $utt .= " $phone";
  }
  print "$utt\n";
}
print STDERR "## LOG: $0, stdin ended\n";
