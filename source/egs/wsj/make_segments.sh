#!/bin/bash

. path.sh

# begin options
frame_length=0.025
frame_shift=0.01
# end options
echo
echo LOG: $0 $@
echo 

. parse_options.sh || exit 1

function Usage {
  cat<<END
 Usage: $0 [options] <data>
 [options]:
 --frame-length              # value, $frame_length
 --frame-shift               # value, $frame_shift
 [ examples]:

 $0 --frame-length 0.025 --frame-shift 0.01  phone/data/timit-qbe-std/keyword/fbank 

END
}

if [ $# -ne 1 ]; then
  Usage && exit 1
fi
data=$1
[ -f $data/feats.scp ] || \
{ echo "ERROR, $data is not ready, feats.scp expected !"; exit 1; }
echo "LOG: $0 started @ `date`"
feat-to-len scp:$data/feats.scp ark,t:- | \
perl -e '($frame_length, $frame_shift) = @ARGV; while(<STDIN>){ chomp; 
  ($utt, $len) = split(/[\s]/); $sec_len = ($len -1 ) * $frame_shift + $frame_length;
  $s = sprintf("%.2f", $sec_len); print "$utt $utt 0 $s\n";  }' $frame_length $frame_shift > $data/segments || exit 1

echo "LOG: $0 done @ `date`";
