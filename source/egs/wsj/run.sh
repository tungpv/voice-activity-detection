#!/bin/bash

. path.sh
. cmd.sh

echo
echo LOG: $0 $@
echo

# begin options
cmd=slurm.pl
nj=40
steps=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $(basename $0) [options] <srcdata> <tgtdir>
 [options]:
 --cmd                                     # value, "$cmd"
 --nj                                      # value, $nj
 --steps                                   # value, "$steps"

 [steps]:
 1: prepare data
 2: prepare mfcc feature
 3: run gmm-hmm training
 4: rerun step03
 5: make 40 dim fbank feature
 6: add segments to fbank  folder

 [example]:
 $0 --steps 1  /data/users/hhx502/wsj/prepare2/data  ./

END
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi
srcdata=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
function check_srcdata {
  for x in test_dev93 test_eval92 test_eval93 train_si284; do
    [ -f $srcdata/$x/wav.scp ] || { echo "ERROR, step01, $x is not ready !"; exit 1; }
  done
  echo "LOG: check_srcdata is done"
}
data=$tgtdir/data
[ -d $data ] || mkdir -p $data
if [ ! -z $step01 ]; then
  echo "LOG: step01, kaldi data preparation"
  check_srcdata || exit 1
  for x in test_dev93 test_eval92 test_eval93 train_si284; do
    echo "LOG: processing folder $x ..."
    sdata=$srcdata/$x
    tdata=$data/$x
    [ -d $tdata ] || mkdir -p $tdata
    cp $sdata/{utt2spk,spk2utt,spk2gender,text}  $tdata
    cat $sdata/wav.scp | sed s:/data5/wsj:/data/users/hhx502/wsj:g | \
    sed s:/opt/kaldi/tools/sph2pipe_v2.5/sph2pipe:/home2/hhx502/kaldi/tools/sph2pipe_v2.5/sph2pipe:g > $tdata/wav.scp
  done
  cp -r $srcdata/lang_test_tg  $data/lang_test_tg
  echo "LOG: step01, done @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "LOG: make mfcc started @ `date`"
  for x in test_eval92 train_si284; do
    sdata=data/$x
    data=$sdata/mfcc
    [ -d $data ] || mkdir -p $data
    cp $sdata/* $data/
    feat=$sdata/feat/mfcc
    echo "LOG: step02, mfcc for $data started @ `date`"
    steps/make_mfcc.sh --cmd "$cmd"  --nj $nj $data $feat/log $feat/data || exit 1
    steps/compute_cmvn_stats.sh $data $feat/log $feat/data || exit 1
    utils/fix_data_dir.sh $data 
    echo "LOG: step02, ended for $data @ `date`"
  done
  echo "LOG: ended @ `date`"
fi
mfcc_data=data/train_si284/mfcc
lang=data/lang_test_tg
gmmdir=exp
if [ ! -z $step03 ]; then
  echo "LOG: step03, run gmm-hmm started @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj 40 --steps 1,2,3,4,5,6,7 \
  --dataname train_si284 --state-num 4500 --pdf-num 45000 \
  --devdata data/test_eval92/mfcc --devname test_eval92 --graphname graph-tg --decodename decode-test_eval92 \
  $mfcc_data $lang  $gmmdir || exit 1
  echo "LOG: step03, Ended @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "LOG: step04, redo step03 for decoding started @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj 40 --steps 7 \
  --dataname train_si284 --state-num 4500 --pdf-num 45000 \
  --devdata data/test_eval92/mfcc --devname test_eval92 --graphname graph-tg --decodename decode-test_eval92 \
  $mfcc_data $lang  $gmmdir || exit 1
  echo "LOG: step04, Ended @ `date`"
fi
if [ ! -z $step05 ]; then
  echo "LOG: step05, fbank started @ `date`"
  for x in test_eval92 train_si284; do
    sdata=data/$x
    data=$sdata/fbank
    [ -d $data ] || mkdir -p $data
    cp $sdata/* $data/
    feat=$sdata/feat/fbank40
    echo "LOG: step05, fbank for $data started @ `date`"
    steps/make_fbank.sh --cmd "$cmd"  --nj $nj $data $feat/log $feat/data || exit 1
    steps/compute_cmvn_stats.sh $data $feat/log $feat/data || exit 1
    utils/fix_data_dir.sh $data 
    echo "LOG: step05, ended for $data @ `date`"
  done
  echo "LOG: step05, Ended @ `date`"
fi

if [ ! -z $step06 ]; then
  echo "LOG: step06, add segments"
  for x in test_eval92 train_si284; do
    echo "LOG processing data $x ..."
    data=data/$x/fbank
    source/egs/wsj/make_segments.sh $data || exit 1
  done
  echo "LOG: step06, Ended @ `date`"
fi
if [ ! -z $step07 ]; then
  echo "LOG: step07, train sbnf-extractor started @ `date`"
  (
   cd ..
   tgtdir=wsj/exp;  [ -d $tgtdir/log ] || mkdir -p $tgtdir/log; 
   nohup source/egs/swahili/run-hier-bnf.sh  --cmd "run.pl" --steps 1,2,3,4 --validating-rate 0.05 \
   wsj/data/train_si284/fbank wsj/data/lang_test_tg wsj/exp/tri4a/ali_train_si284 wsj/exp/sbnf-extractor > $tgtdir/log/sbnf-extractor.log 2>&1 & 
  )
  echo "LOG: step07, done @ `date`"
fi
if [ ! -z $step08 ]; then
  echo "LOG: step08, train fbank-ff-dnn started"
  (
    cd ..
    tgtdir=wsj/exp;  [ -d $tgtdir/log ] || mkdir -p $tgtdir/log; 
    nohup source/egs/swahili/run-fbank-nnet.sh  \
    --cmd "slurm.pl --exclude=node01,node02" \
    --steps 1,2,3,4,5 --validating-rate 0.05 \
    --devdata wsj/data/test_eval92/fbank --graphname graph-tg \
    --decodename decode-test_eval92 \
    wsj/data/train_si284/fbank  wsj/data/lang_test_tg wsj/exp/tri4a/ali_train_si284 $tgtdir  > $tgtdir/log/run-fbank-nnet.log 2>&1 &
  )
  echo "LOG: step08, run-fbank-nnet.sh submitted"
fi
if [ ! -z $step07 ]; then
  echo "## LOG: step07, make mfcc @ `date`"
  for x in test_dev93  test_eval93; do
    sdata=data/$x; data=$sdata/mfcc; feat=$sdata/feat/mfcc
    [ -d $data ] || mkdir -p $data
    cp $sdata/* $data/
    steps/make_mfcc.sh --cmd "$cmd"  --nj $nj $data $feat/log $feat/data || exit 1
    steps/compute_cmvn_stats.sh $data $feat/log $feat/data || exit 1
    utils/fix_data_dir.sh $data 
  done
  echo "## LOG: step07, done @ `date`"
fi
