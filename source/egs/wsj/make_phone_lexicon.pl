#!/usr/bin/perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  die "\n## Example: cat nonsilence_phones.txt | $0  silence_phone_to_word_csl > phone_lexicon.txt\n\n";
}

my ($csl) = @ARGV;
my @A = split(/:/, $csl);
for(my $i = 0; $i < @A; $i ++) {
  my $entry = $A[$i];
  my @B = split(/,/, $entry);
  die "## ERROR, $0, bad csl $csl\n" if scalar @B != 2;
  print "$B[1]\t$B[0]\n";
}

print STDERR "## LOG: $0, stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)/ or next;
  print "$1\t$1\n";
}
print STDERR "## LOG: $0, stdin ended\n";
