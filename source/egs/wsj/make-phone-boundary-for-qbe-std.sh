#!/bin/bash

echo
echo "$0 $@"
echo 

. path.sh
. cmd.sh

# begin options
steps=
cmd=slurm.pl
nj=20
# end options


function Usage {
 cat<<END
 Usage: $0 [options] <keyword-data> <test-data> <lang> <graphdir>  <source-dnn-dir>
 [options]:
 --steps                             # value, "$steps"
 [examples]:

 $0 --steps 1 phone/data/timit-qbe-std/keyword phone/data/timit-qbe-std/test \
 phone/data/lang  phone/exp/tri4a/graph  phone/exp/nnet5a/dnn   

END
}

. parse_options.sh || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 5 ]; then
  Usage && exit 1;
fi
keyword_data=$1
test_data=$2
lang=$3
graphdir=$4
dnndir=$5

if [ ! -z $step01 ]; then
  echo "## LOG: $0, step01, fbank preparation @ `date`"
  for sdata in $keyword_data $test_data; do
    data=$sdata/fbank; feat=$sdata/feat/fbank
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  \
    --make-fbank true --fbank-cmd "steps/make_fbank.sh --fbank-config conf/fbank.conf" \
    $sdata $data $feat || exit 1
  done 
  echo "## LOG: $0, step01, done @ `date`"
fi
if [ ! -z $step02 ]; then
  echo "## LOG: $0, step02, lattice @ `date`"
  for sdata in $keyword_data $test_data; do
    steps/nnet/decode.sh --cmd slurm.pl --nj $nj --acwt 0.125 \
    --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring true \
    $graphdir  $sdata/fbank $dnndir/decode-$(basename $(dirname $sdata))-$(basename $sdata) || exit 1
  done
  echo "## LOG: $0, step02, done with lattice generation @ `date`"
fi

if [ ! -z $step03 ]; then
  echo "## LOG: step03, make boundary for $keyword_data @ `data`"
  sdata=$keyword_data
  latdir=$dnndir/decode-$(basename $(dirname $sdata))-$(basename $sdata)
  source/egs/quesst/get_query_time_boundary.sh --steps 1,2,3,4 --cmd slurm.pl \
  --phone-sequence 6 --silence-int 2 \
  $sdata/fbank $lang $latdir || exit 1
  echo "## LOG: step03, done ('$sdata/fbank')"
fi
if [ ! -z $step04 ]; then
  echo "## LOG: step04, make boundary for $test_data @ `date`"
  sdata=$test_data
  latdir=$dnndir/decode-$(basename $(dirname $sdata))-$(basename $sdata)
  source/egs/quesst/get_query_time_boundary.sh --steps 1,2,3,4 --cmd slurm.pl \
  --phone-sequence 6 --silence-int 2 \
  $sdata/fbank $lang $latdir || exit 1
  echo "## LOG: step04, done ('$sdata/fbank')"
fi
