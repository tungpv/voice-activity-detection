#!/usr/bin/perl
use warnings;
use strict;

print STDERR "## LOG: $0, stdin set.txt expected\n";
while(<STDIN>) {
  chomp;
  s/_[BIES]//g;
  my @A = split(/\s+/);
  my $line = $A[0];
  my %vocab = ();
  for(my $i = 1; $i < @A; $i++) {
    my $x_phone = $A[$i];
    if(not exists $vocab{$x_phone}) {
      $line .= " $x_phone";
      $vocab{$x_phone} ++;
    }
  }
  print "$line\n";
}
print STDERR "## LOG: $0, stdin ended\n";
