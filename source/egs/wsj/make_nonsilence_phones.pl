#!/usr/bin/perl
use strict;
use warnings;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\n## Example: cat phone_trans.txt | $0 silence_phones.txt > nonsilence_phones.txt\n\n";
}
my ($silence_phones) = @ARGV;
open(F, "$silence_phones") or die "## ERROR: cannot open file $silence_phones\n";
my %vocab = ();
while(<F>) {
  chomp;
  m/(\S+)/ or next;
  $vocab{$1} ++;
} 
close F;
print STDERR "## LOG: $0, stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)$/ or next;
  my @A = split(/\s+/, $2);
  for(my $i = 0; $i < @A; $i ++) {
    my $phone = $A[$i];
    next if exists $vocab{$phone};
    print "$phone\n";
  }
}
print STDERR "## LOG: $0, stdin ended\n";
