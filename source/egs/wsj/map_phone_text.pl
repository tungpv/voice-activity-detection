#!/usr/bin/perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;
if($numArgs != 1) {
  die "\nExample: cat phone_trans.text | $0 phone_map.txt > new_phone_trans.txt\n\n";
}
my ($mapFile) = @ARGV;
open(F, "$mapFile") or die "## ERROR: file $mapFile cannot open\n";
my %vocab = ();
while(<F>) {
  chomp;
  my @A = split(/\s+/);
  my $target_phone = shift @A;
  for(my $i = 0; $i < @A; $i++) {
    my $source_phone = $A[$i];
    $vocab{$source_phone} = $target_phone;
  }
}
close F;

print STDERR "## LOG: stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my $utt = $1;
  my @A = split(/\s+/, $2);
  for(my $i = 0; $i < @A; $i++) {
    my $phone = $A[$i];
    if (exists $vocab{$phone}) {
      $phone = $vocab{$phone};
    }
    $utt .= " $phone";
  }
  print "$utt\n";
}
print STDERR "## LOG: stdin ended\n";
