#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo 

# begin options
steps=
cmd=slurm.pl
nj=20
# end options

. parse_options.sh  || exit 1

function Usage() {
 cat<<END
 Usage: $0 [options]  <source-data-dir> <source-wsj-dir> <tgtdir>
 [options]:
 --steps
 --cmd                            # value, "$cmd"
 --nj                             # value, $nj
 [steps]:
 03 make mfcc features for training and test data
 [examples]:
  
  $0  --steps 1 /data/users/hhx502/wsj/data_dan/local/data \
  /home/hhx502/w2016/wsj/data /home/blt503/w2016/wsj

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

source_data_dir=$1
source_wsj_data=$2
tgtdir=$3


if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
else
  echo "## LOG: WARNING, steps EMPTY !" && exit 1
fi
data=$tgtdir/data
train_si84=$data/train_si84
test_eval92=$data/test_eval92
if [ ! -z  $step01 ]; then
  echo "## LOG: $0, step01, prepare data"
  [ -d $train_si84 ] || mkdir -p $train_si84
  cat $source_data_dir/train_si84.txt | \
  awk '{print $1;}' > $train_si84/uttlist
  utils/subset_data_dir.sh --utt-list $train_si84/uttlist \
  $source_wsj_data/train_si284  $train_si84 || exit 1
  utils/fix_data_dir.sh $train_si84
  cp -r $source_wsj_data/test_eval92 $test_eval92 
  echo "## LOG: $0, step01, done with data preparation ($data)"
fi
if [ ! -z $step02 ]; then
  echo "## LOG: $0, step02, copy lang"
  cp -r $source_wsj_data/lang_test_tg $data/lang_test_tg
  echo "## LOG: $0, step02, done"
fi
if [ ! -z $step03 ]; then
  echo "## LOG: $0, step03, make mfcc feature"
  for x in train_si84 test_eval92;  do
    sdata=wsj/data/$x
    data=$sdata/mfcc; feat=$sdata/feat/mfcc; log=$sdata/feat/mffcc/log
    [ -d $data ] || mkdir -p $data
    cp $sdata/*  $data/
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj $data $log $feat   || exit 1;
    steps/compute_cmvn_stats.sh $data  || exit 1;
  done
fi

if [ ! -z $step04 ];then
  echo "## LOG: $0, step04, train monophone @ `date`"
  steps/train_mono.sh --boost-silence 1.25 --nj 10 --cmd   "$cmd" \
  wsj/data/train_si84/mfcc wsj/data/lang_test_tg  wsj/exp/mono0a  || exit 1
  echo "## LOG: $0, step04,firstly done with monophone training  (wsj/exp/mono0a) @ `date` "
fi
if [ ! -z $step05 ];then
  echo "## LOG: $0,step05,first force-alignment @ `date`"
  steps/align_si.sh --boost-silence 1.25 --nj 10 --cmd "$cmd" \
    wsj/data/train_si84/mfcc wsj/data/lang_test_tg wsj/exp/mono0a wsj/exp/mono0a_ali || exit 1;
    echo "## LOG: $0, step05,firstly done with make force-alignment   @ `date` "
fi

if [ ! -z $step06 ];then
  echo "## LOG: $0,step06,make mfcc feat static+delta+delta-delta @ `date`"
  steps/train_deltas.sh --boost-silence 1.25 --cmd "$cmd" 2000 30000 \
    wsj/data/train_si84/mfcc wsj/data/lang_test_tg wsj/exp/mono0a_ali wsj/exp/deltas_mfcc || exit 1;
  echo echo "## LOG: $0, step06,done with mfcc static+delta+delta-delta  @ `date` "
fi
if [ ! -z $step07 ];then
  echo "## LOG: $0,step07,second force-alignment after mfcc deltas @ `date`"
  steps/align_si.sh --nj 10 --cmd "$cmd" \
    wsj/data/train_si84/mfcc wsj/data/lang_test_tg wsj/exp/deltas_mfcc wsj/exp/deltas_mfcc_ali_si84 || exit 1;
  echo echo "## LOG: $0, step07, done with second force-alignment @ `date` "
fi
if [ ! -z $step08 ];then
  echo "## LOG: $0,step08,train lda @ `date`"
  steps/train_lda_mllt.sh --cmd "$cmd" \
     --splice-opts "--left-context=3 --right-context=3" 2000 30000 \
    wsj/data/train_si84/mfcc wsj/data/lang_test_tg wsj/exp/deltas_mfcc_ali_si84 wsj/exp/lda_mllt || exit 1;
    echo "## LOG: $0, step08,completed lda_mllt reduction   @ `date` "
fi
if [ ! -z $step09 ];then
  echo "## LOG: $0,step09,third force-alignment after training lda @ `date`"
  steps/align_si.sh  --nj 10 --cmd "$cmd" \
  --use-graphs true wsj/data/train_si84/mfcc \
  wsj/data/lang_test_tg  wsj/exp/lda_mllt  wsj/exp/lda_mllt_ali_si84  || exit 1; 
  echo "## LOG: $0,step09,done third force-alignment @ `date`"
fi
if [ ! -z $step10 ];then
  echo "## LOG: $0,step10, Speaker Adapted Training (SAT) ,trained on fMLLR-adapted features @ `date`"
  steps/train_sat.sh --cmd "$cmd" 2000 30000 \
    wsj/data/train_si84/mfcc wsj/data/lang_test_tg wsj/exp/lda_mllt_ali_si84  wsj/exp/fmllr_sat || exit 1;
 echo "## LOG: $0,step10,done with Speaker Adapted Training (SAT) ,trained on fMLLR-adapted features@ `date`"
fi
if [ ! -z $step11 ];then
  echo "## LOG: $0,step11, make fst-graph @ `date`"
  utils/mkgraph.sh wsj/data/lang_test_tg wsj/exp/fmllr_sat wsj/exp/fmllr_sat/graph_test_tg || exit 1;
  echo "## LOG: $0,step11, finished with making fst-graph  @ `date`"
fi
if [ ! -z $step12 ];then 
  echo "## LOG: $0,step12, decoding acording to fst-graph @ `date`"
  steps/decode_fmllr.sh --nj 8 --cmd "$decode_cmd" \
    wsj/exp/fmllr_sat/graph_test_tg wsj/data/test_eval92/mfcc  wsj/exp/fmllr_sat/decode_test_tg_eval92 || exit 1;
  echo "# #LOG: $0,step12, done with decoding the SAT system  @ `date`"
fi
if [ ! -z $step13 ];then
  echo "## LOG: $0,step13, starting forth force-alignment after fmllr sat @ `date`"
  steps/align_fmllr.sh --nj 10 --cmd "$cmd" \
    wsj/data/train_si84/mfcc wsj/data/lang_test_tg wsj/exp/fmllr_sat wsj/exp/fmllr_sat_ali_si84 || exit 1;
  echo "## LOG: $0,step13, done with forth force-alignment after fmllr sat @ `date`"
fi
confdir=/home/hhx502/w2016/wsj/conf
if [ ! -z $step14 ]; then
  echo "## LOG: $0, step14, extract fbank+pitch features @ `date`"
  for x in train_si84 test_eval92; do
    sdata=wsj/data/$x; data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch; 
    source/egs/swahili/make_feats.sh  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config $confdir/fbank.conf --pitch-config $confdir/pitch.conf" \
    $sdata $data $feat 
  done 
  echo "## LOG: $0, step14, done @ `date` "
fi
