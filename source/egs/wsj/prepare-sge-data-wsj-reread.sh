#!/bin/bash 

. path.sh
. cmd.sh 

echo 
echo "## LOG: $0 $@"
echo 

# begin options
steps=
# end options

. parse_options.sh || exit 1

function Usage {
cat<<EOF
 Usage: $0 [options] <source_wave> <wsj_train_si284>  <tgtdata>
 [options]:
 [steps]:
 [examples]:
 
 $0 --steps 1  /data/users/hhx502/english-corpus/wsj-sge/wave  \
 wsj/data/train_si284  /data/users/hhx502/english-corpus/wsj-sge/data

EOF
}
 
if [ $# -ne 3 ]; then
  Usage && exit 1
fi

source_wavdir=$1
orgin_wsj_train=$2
tgtdata=$3


if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
[ -d $tgtdata ]|| mkdir -p $tgtdata
if [ ! -z $step01 ]; then
  find $source_wavdir -name "*.wav"  > $tgtdata/wavlist.txt
  cat $tgtdata/wavlist.txt | perl -ane 'chomp; s/.*\///g; s/\.wav//g; print "$_\n";'  > $tgtdata/uttlist
fi

if [ ! -z $step02 ]; then
  utils/subset_data_dir.sh --utt-list $tgtdata/uttlist $orgin_wsj_train $tgtdata
  rm $tgtdata/wav.scp
  cat $tgtdata/wavlist.txt | \
  perl -ane 'chomp; $wavFile= $_; s/.*\///g; s/\.wav//g; print "$_ $wavFile\n";' > $tgtdata/wav.scp
  utils/fix_data_dir.sh $tgtdata
fi

if [ ! -z $step03 ]; then
  ./ted-libri-en/utils/data/get_utt2dur.sh $tgtdata
fi

if [ ! -z $step04 ]; then
  cat $tgtdata/utt2dur | \
  perl -ane 'chomp; @A = split(/\s+/); printf("%s %s 0 %.2f\n", $A[0], $A[0], $A[1]);' \
  > $tgtdata/segments
fi
