#!/bin/bash 

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
steps=
cmd=slurm.pl
nj=20
dev_data=data/test_eval92
ali_dev_dir=/home2/hhx502/seame/exp/nnet5a/dnn/ali-low-dev
devname=test_eval92
phone2word_map_csl="SIL,<silence>:SPN,<spn>:NSN,<nsn>:OOV,<unk>"
source_dict_dir=/home2/hhx502/seame/data/local/dict
# end options
function Usage {
 cat<<END
 
 Usage: $0 [options] <source_data_dir> <source_lang_dir> <source-gmm-dir> <source-dnn-dir> <tgtdir>
 [options]:
 --ali-dev-dir                                            # value, "$ali_dev_dir"
 [steps]:
 [examples]:
 
 $0 --steps 1  --devname $devname \
 data/train_si284 data/lang_test_tg exp/tri4a dummy phone 

END
}

. parse_options.sh || exit 1

if [ $# -ne 5 ]; then
  Usage && exit 1
fi

source_data_dir=$1
source_lang_dir=$2
source_gmm_dir=$3
source_dnn_dir=$4
tgtdir=$5

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
train_data=$tgtdir/data/train_si284
sdata=$source_data_dir
dataname=train_si284
train_data_csl="$sdata/mfcc,$dataname/mfcc:$sdata/fbank,$dataname/fbank"
train_data=$tgtdir/data/$dataname/mfcc
if [ ! -z $step01 ]; then
  echo "## LOG: $0, step01, prepare training data @ `date`"
  # [ -d $train_data ] || mkdir -p $train_data
  # ali-to-phones exp/tri4a/ali_train_si284/final.mdl \
  # "ark:gzip -cd exp/tri4a/ali_train_si284/ali.*.gz|" ark,t:- | \
  # utils/int2sym.pl -f 2- $source_lang_dir/phones.txt | \
  # source/egs/wsj/transcribe-phone-text.pl "$phone2word_map_csl" > $train_data/text
  ali_dir=$source_gmm_dir/ali_train_si284
  source/egs/ss-apsipa/make-phone-data.sh --nj $nj --cmd "$cmd" \
    --steps 1,2,3 --dataname train_si284 --ali-dir $ali_dir \
    --phone2word-map-csl "$phone2word_map_csl" \
    --data-csl "$train_data_csl" \
    data-dummy $source_lang_dir sdir-dummy $tgtdir/data || exit 1

  echo "## LOG: $0, step02, done ($train_data)"
fi
test_eval=$tgtdir/data/test_eval92/mfcc
if [ ! -z $step02 ]; then
  echo "## LOG: $0, step02, prepare test_eval92 data"
  sdata=$dev_data
  dataname=$devname
  ali_dir=$source_gmm_dir/ali_test_eval92
  dev_data_csl="$sdata/mfcc,$dataname/mfcc:$sdata/fbank,$devname/fbank"

  source/egs/ss-apsipa/make-phone-data.sh --nj $nj --cmd "$cmd" \
  --steps 1,2,3 --dataname $dataname --ali-dir $ali_dir \
  --phone2word-map-csl "$phone2word_map_csl" \
  --data-csl "$dev_data_csl" \
  data-dummy $source_lang_dir sdir-dummy $tgtdir/data || exit 1
  echo "## LOG: $0, step02, done"
fi
lang=$tgtdir/data/lang
dict=$tgtdir/data/local/dict
if [ ! -z $step03 ]; then
  echo "## LOG: $0, step03, prepare dict & lang"
  [ -d $dict ] || mkdir -p $dict
  echo "$phone2word_map_csl" | \
  perl -ane 'chomp; @A = split(/:/); for($i = 0; $i < @A; $i++) { $word_pron=$A[$i];
    @B = split(/,/, $word_pron); print "$B[0]\n";
  } '  > $dict/silence_phones.txt
  cat $train_data/text | \
  source/egs/wsj/make_nonsilence_phones.pl "echo -e \"<silence>\n<spn>\n<nsn>\n<unk>\"|" | \
  sort -u > $dict/nonsilence_phones.txt
  echo "SIL" > $dict/optional_silence.txt
  echo -n  > $dict/extra_questions.txt
  cat $dict/nonsilence_phones.txt | \
  source/egs/wsj/make_phone_lexicon.pl "$phone2word_map_csl" > $dict/lexicon.txt
  utils/validate_dict_dir.pl  $dict || exit 1
  utils/prepare_lang.sh --position-dependent-phones false  $dict "<unk>" $lang/tmp $lang || exit 1
  echo "## LOG: $0, step03, done('$dict', '$lang')"
fi
lmdir=$tgtdir/data/local/unigram
if [ ! -z $step04 ]; then
  echo "## LOG: $0, step04, make unigram"
  [ -d $lmdir ] || mkdir -p $lmdir
  cat $dict/lexicon.txt | \
  awk '{print $1; }' | gzip -c > $lmdir/vocab.gz
  cat $train_data/text | \
  cut -d' ' -f2- | sed 's#<silence>##g; s#<unk>##g' | \
  gzip -c > $lmdir/train-text.gz
  cat $test_eval/text | \
  cut -d' ' -f2- | sed 's#<silence>##g; s#<unk>##g' | \
  gzip -c > $lmdir/dev-text.gz
  source/egs/kws2016/georgian/train-srilm-v2.sh --lm-order-range "2 2" \
  --steps 1,2 --cutoff-csl "2,00,01" $lmdir
  ngram -lm $lmdir/lm.gz -order 1 -write-lm $lmdir/unigram.gz
  ~/w2016/local/arpa2G.sh $lmdir/unigram.gz  $lang  $lang
  echo "## LOG: $0, step04, done"
fi
train_data=$tgtdir/data/train_si284
dev_data=$tgtdir/data/test_eval92
expdir=$tgtdir/exp
if [ ! -z $step09 ]; then
  echo "## LOG: $0, step09, train gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd "$cmd" --nj $nj \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 5000 --pdf-num 75000 \
  --devdata $dev_data/mfcc \
  --decodename "decode-test_eval92" \
  $train_data/mfcc $lang $expdir || exit 1
  echo "## LOG: $0, step09, done ('$expdir') @ `date`"
fi
if [ ! -z $step10 ]; then
  echo "## LOG: $0, step10, train dnn @ `date`"
 source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 --delta-opts --delta-order=2 \
 --cmvn-opts "--norm-means=true --norm-vars=true"  \
 --use-partial-data-to-pretrain true --pretraining-rate 0.5 --nnet-pretrain-cmd \
 "/home/hhx502/w2016/steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048" \
 --nnet-id nnet5a --graphdir $expdir/tri4a/graph --devdata $dev_data/fbank \
 --decodename decode-test_eval92 $train_data/fbank $lang $expdir/tri4a/ali_train $expdir
  echo "## LOG: $0, step10, done @ `date`"
fi




























