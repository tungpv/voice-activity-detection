#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=run.pl
nj=20
steps=
prepare_data=false
source_segments=
prepare_opts="<sdata> <dir>"

# end options

echo
echo "$0 $@"
echo

. parse_options.sh

function PrintOptions {
  cmdName=$(echo $0| perl -pe 's/^.*\///g;')
  cat <<END

$cmdName [options]:
cmd					# value, "$cmd"
nj					# value, "$nj"
steps					# value, "$steps"
prepare_data				# value, $prepare_data
source_segments				# value, "$source_segments"
prepare_opts				# value, "$prepare_opts"

END
}

PrintOptions;

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=true
  done
fi
if $prepare_data; then
  echo "$0: prepare_data started @ `date`"
  optNames="sdata dir"
  . source/register_options.sh  "$optNames" "$prepare_opts" || \
  { echo "$0: prepare_data: ERROR, @register_options.sh"; exit 1; }
  [ -d $dir ] || mkdir -p $dir
  if [ ! -z $step01 ]; then
    [ ! -z "$source_segments" ] || \
    { echo "$0: prepare_data: ERROR, source_segments not specified"; exit 1; }
    cat $source_segments | sort -u > $dir/segments
    cp $sdata/{stm,wav.scp,reco2file_and_channel} $dir/
    cat $dir/segments | cut -d' ' -f1 | \
    perl -pe 'chomp; m/([^_]+_[^_]+)_*/; $lab=$1; $_="$_ $lab\n";' > $dir/utt2spk
    utils/utt2spk_to_spk2utt.pl < $dir/utt2spk > $dir/spk2utt
    utils/fix_data_dir.sh $dir
  fi
  if [ ! -z $step02 ]; then
    data=$dir/plp_pitch
    [ -d $data ] || mkdir -p $data
    cp $dir/* $data/
    feat=/local/hhx502/kws2015/$(basename $dir)/plp_pitch
    steps/make_plp_pitch.sh --nj $nj --plp-config conf/plp.conf --pitch-config conf/pitch.conf \
    $data $feat/log $feat/data || exit 1
    steps/compute_cmvn_stats.sh $data  $feat/log $feat/data || exit 1
    utils/fix_data_dir.sh $data
  fi
  if [ ! -z $step03 ]; then
    echo "$0: decoding started @ `date`"
    data=$dir/plp_pitch
    dataName=$(basename $dir)
    graph=grapheme_vllp/exp/gmm.plp_pitch/tri4a/graph.bd.0.05.201421
    decode_dir=grapheme_vllp/exp/gmm.plp_pitch/tri4a/decode_$dataName
    steps/decode_fmllr.sh --nj 20  $graph  $data $decode_dir || exit 1
    echo "$0: decoding ended @ `date`"
  fi
  echo "$0: prepare_data ended @ `date`"
fi
