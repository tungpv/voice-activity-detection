#!/bin/bash 
echo
echo "## LOG: $0 $@"
echo

. path.sh
. cmd.sh

# begin options
steps=
cmd=run.pl
nj=40
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF

 [examples]:

 $0 --steps 1  /home2/hhx502/sge2017/data/update-nov-28-16k \
 /home3/hhx502/w2017/mandarin/data \
 /home3/hhx502/w2017/em/update-dec-04-16

EOF
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

en_srcdata=$1
cn_srcdata=$2
tgtdir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
tgtdata=$tgtdir/data
tgtexpdir=$tgtdir/exp
en_srcdict=$en_srcdata/dict
cn_srcdict=$cn_srcdata/dict-merge
dict=$tgtdata/dict
if [ ! -z $step01 ]; then
  [ -d $dict ] || mkdir -p $dict
  grep -v '<' $en_srcdict/lexicon.txt | \
  source/egs/sge2017/update-nov-28-16k/map-phone.pl --phone-prefix="_eng" | \
  cat - $cn_srcdict/lexicon.txt | \
  source/egs/sge2017/update-nov-28-16k/merge-dict.pl --trivial-merge \
  $dict/lexicon.txt "|sort -u| egrep -v 'SIL|<sss>|<oov>|<vns>' > $dict/nonsilence_phones.txt"
  echo -e "<sss>\nSIL\n<oov>\n<vns>" > $dict/silence_phones.txt
  echo -e "SIL" > $dict/optional_silence.txt
  echo -n > $dict/extra_questions.txt
  utils/validate_dict_dir.pl $dict
  echo "## LOG (step01, $0): done & check '$dict'"
fi
lang=$tgtdata/lang
if [ ! -z $step02 ]; then
  [ -d $lang ] || mkdir -p $lang
  utils/prepare_lang.sh $dict "<unk>" $lang/tmp $lang
  echo "## LOG (step02, $0): done & check '$lang'"
fi
sdata=$tgtdata/train-em-mix
en_data=$en_srcdata/train-merge
cn_data=$cn_srcdata/train-merge
if [ ! -z $step03 ]; then
  utils/combine_data.sh $sdata/mfcc-pitch $en_data/mfcc-pitch $cn_data/mfcc-pitch
  utils/fix_data_dir.sh $sdata/mfcc-pitch
  utils/combine_data.sh $sdata/fbank-pitch $en_data/fbank-pitch $cn_data/fbank-pitch
  utils/fix_data_dir.sh $sdata/fbank-pitch
  echo "## LOG (step03, $0): done & check '$sdata'"
fi
factor=0.3
subdata=$tgtdata/train-subset$factor/mfcc-pitch
if [ ! -z $step04 ]; then
  source/egs/swahili/subset_data.sh --subset-time-ratio $factor --random true \
  $sdata/mfcc-pitch $subdata
  echo "## LOG (step04, $0): done & check '$subdata/mfcc-pitch'"
fi
lmdir=$tgtdata/lm
devNum=20000
if [ ! -z $step05 ]; then
  [ -d $lmdir ] || mkdir -p $lmdir
  cut -d' ' -f2- $sdata/mfcc-pitch/text | \
  utils/shuffle_list.pl | head -$devNum | gzip -c > $lmdir/dev-text.gz
  cut -d' ' -f2- $sdata/mfcc-pitch/text | \
  utils/shuffle_list.pl | tail -n +$[devNum+1] | \
  gzip -c > $lmdir/train-text.gz
  cut -f1 $dict/lexicon.txt | \
  gzip -c > $lmdir/vocab.gz
  echo "## LOG (step05, $0): done & check '$lmdir'"
fi

if [ ! -z $step06 ]; then
  source/egs/sge2017/local/build-lm/train-srilm-v2.sh --steps 1,2 --lm-order-range "3 3" \
  --cutoff-csl "3,011,012"  $lmdir || exit 1
  echo "## LOG (step06, $0): done & check '$lmdir'"
fi
lang_test=$tgtdata/lang_test
if [ ! -z $step07 ]; then
  source/egs/fisher-english/arpa2G.sh $lmdir/lm.gz $lang $lang_test
  echo "## LOG (step06, $0): done & check '$lang_test'"
fi
train_id=b
if [ ! -z $step08 ]; then
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1,2,3,4,5,6,7 \
  --done-with-lda-mllt true \
  --train-id $train_id --cmvn-opts "--norm-means=true"  --state-num 8000 --pdf-num 160000 \
  --devdata /home2/hhx502/seame/data/high-dev/mfcc-pitch \
  $subdata $lang_test  $tgtexpdir || exit 1
  echo "## LOG (step08, $0): done & check '$tgtexpdir'"
fi
alidir=$tgtexpdir/tri2b/ali_train-em-mix
expdir=$tgtexpdir
if [ ! -z $step09 ]; then
  steps/align_si.sh --cmd $cmd --nj $nj $sdata/mfcc-pitch $lang $expdir/tri2b \
  $alidir
  echo "## LOG (step09, $0): done & check '$aldir'"
fi
train_id=a
if [ ! -z $step10 ]; then
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1,2,3,4,5,6,7 \
  --alidir $alidir \
  --train-id $train_id --cmvn-opts "--norm-means=true"  --state-num 14000 --pdf-num 240000 \
  --devdata /home2/hhx502/seame/data/high-dev/mfcc-pitch \
  $sdata/mfcc-pitch $lang  $expdir || exit 1
  echo "## LOG (step10, $0): done & check '$expdir/tri3a'"
fi
graph=$expdir/tri3a/graph
if [ ! -z $step11 ]; then
  utils/mkgraph.sh $lang_test  $expdir/tri3a  $graph
  echo "## LOG (step11, $0): done & check '$expdir/tri3a/graph'"
fi
devdata=/home2/hhx502/seame/data/high-dev/mfcc-pitch
srcdir=$expdir/tri3a
if [ ! -z $step12 ]; then
  effect_nj=$(wc -l < $devdata/spk2utt)
  [ $effect_nj -gt $nj ] && effect_nj=$nj
  steps/decode_fmllr.sh --cmd "$cmd" --nj $effect_nj \
  --scoring-opts "--min-lmwt 8 --max-lmwt 15" \
  --acwt 0.1 \
  $graph $devdata $srcdir/decode-high-dev
  echo "## LOG ($0): done @ check '$srcdir/decode-high-dev'"
fi

if [ ! -z $step20 ]; then
  source/egs/swahili/subset_data.sh --exclude-uttlist \
  /home3/hhx502/w2017/em/update-dec-04-16/data/train-em-mix/flist-to-be-removed.txt \
  /home3/hhx502/w2017/em/update-dec-04-16/data/train-em-mix/fbank-pitch \
  /home3/hhx502/w2017/em/update-dec-04-16/data/train-em-mix/subset/fbank-pitch
  echo "## LOG (step20, $0): done & check '/home3/hhx502/w2017/em/update-dec-04-16/data/train-em-mix/subset/fbank-pitch'"
fi
