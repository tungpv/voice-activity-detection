#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);

print STDERR "\n$0 stdin expected\n";
my %vocab = ();
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  for(my $i = 0; $i < @A; $i ++) {
    next if ($A[$i] =~ /^$/);
    $vocab{$A[$i]} ++;
  }
}
foreach my $word (keys %vocab) {
  print "$word $vocab{$word}\n";
}
print STDERR "\n$0 stdin ended\n";
