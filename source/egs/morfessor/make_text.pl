#!/usr/bin/perl
use strict;
use warnings;
use open qw(:std :utf8);
use XML::Simple;
use Data::Dumper;

print STDERR "\nLOG: $0 ", join(" ", @ARGV), "\n";

if (scalar @ARGV != 2) {
  print STDERR "\nUsage: cat foo.txt | $0 <from> <map-file>\n";
  print STDERR "e.g.: cat foo.txt | $0 2 map > mapped.txt\n\n";
  exit 1
}
my ($from, $sMapFile) = @ARGV;
sub LoadMap {
  my ($sFile, $vocab) = @_;
  open(File, "$sFile") or 
  die "$0: LoadMap: ERROR, file $sFile cannot open\n";
  while(<File>) {
    chomp;
    my @A = split(/[\t\s]/);
    next if @A < 1;
    my $word = shift @A;
    $$vocab{$word} = join(" ", @A);   
  }
}
my %vocab = ();
LoadMap("$sMapFile", \%vocab);
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/[\s\t]/);
  next if @A < 1;
  my $s = "";
  for(my $i=0; $i< @A; $i++) {
    my $word = $A[$i];
    if ($i < $from - 1) {
      $s .= "$word ";
      next;
    }
    my @Sub = split(/[\_\-]/, $word);
    for(my $j = 0; $j < @Sub; ++ $j) {
      my $wordj = $Sub[$j];
      if(exists $vocab{$wordj}) {
        $s .= "$vocab{$wordj} ";
      } else {
        $s .= "$wordj ";
      }
    }
  }
  $s =~ s/\s+$//g;
  print $s, "\n";
}
print STDERR "$0: stdin ended\n";
