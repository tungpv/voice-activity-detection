#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
lexicon=
extra_text=

# end options
echo   1>&2
echo "LOG: $0 $@" 1>&2
echo  1>&2

. utils/parse_options.sh || exit 1
function Usage {
  cat <<END

 $(basename $0) [options] <train-text> <dir>
 [options]:
 --steps                                    # value, "$steps"
 --lexicon                                  # value, "$lexicon"
 --extra-text                               # value, "$extra_text"

 [steps]:
 1: normalize text, if text file is a kaldi format text
 2: make word count file fomr text data
 3: make word count file from lexicon, if any
 4: prepare training data
 5: train morph models

END
}
if [ $# -ne 2 ]; then
  Usage && exit 1
fi

text=$1
dir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

mtrn_tool=$(which morfessor-train)
if [ -z $mtrn_tool ]; then
  echo "ERROR, morfessor-train is not installed" && exit 1
fi

tmpdir=$dir/temp
logdir=$dir/log
[ -d $tmpdir ] || mkdir -p $tmpdir
[ -d $logdir ] || mkdir -p $logdir
if [ ! -z $step01 ]; then
  cat $text | awk '{$1 = ""; print;}' | gzip -c > $tmpdir/text.gz
  text=$tmpdir/text.gz
fi

train_data=$tmpdir/word-count.txt
if [ ! -z $step02 ]; then
  gzip -cd $text | \
  perl -e 'while(<>){chomp; @A = split(/\s+/); 
    for($i = 0; $i < @A; ++$i) {print "1 $A[$i]\n";} }' | \
  source/egs/morfessor/remove_dash.pl | \
  sort -k1rn | \
  perl -e 'while(<>){next if(m/[\<\>\[\]]/); print;}' > $train_data
fi
if [ ! -z $step03 ]; then
  if [ ! -z $lexicon ]; then
    cat $lexicon | awk '{printf("1 %s\n", $1);}' | \
    source/egs/morfessor/remove_dash.pl | \
    perl -e 'while(<>){next if(m/[\<\>\[\]]/); print;}' > $tmpdir/word-count-lex.txt
  fi
fi

if [ ! -z $step04 ]; then
  for x in $train_data $tmpdir/word-count-lex.txt;  do
    [ -f $x ] && cat $x
  done  | source/egs/morfessor/remove_dash.pl | \
  perl -e '$min = 20; $max = 40; $gap = $max - $min; $thresh = 15;
    while(<>){chomp; next if(! m/(\d+)\s+(\S+)/); $x = $min + int(rand($gap));
      if($1 < $thresh){ print "$x $2\n"; } else { print "$_\n";} 
    }' | sort -k1nr  > $tmpdir/word-count-merged.txt
  train_data=$tmpdir/word-count-merged.txt
fi

if [ ! -z $step05 ]; then
  echo "Train morph model started @ `date`"
  echo "training data is $train_data"
  morfessor-train -s $dir/morph.mdl $train_data \
  > $logdir/morfessor-train.log 2>&1 
  echo "Ended @ `date`"
fi



