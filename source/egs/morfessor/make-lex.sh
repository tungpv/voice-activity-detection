#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
textfile=
# end options
echo
echo $0 $@
echo

. parse_options.sh || exit 1

function Usage {
cat<<END

 $(basename $0) [options] <word-lexicon> <morfessor-model> <g2p-model> <dir>
 [options]:
 --steps                                    # value, "$steps"
 --textfile                                 # value, "$textfile" 

 [steps]:
 1: make normalized text, if text file is a kaldi format text
 2: make wordlist from textfile, if any
 3: make merged wordlist
 4: convert wordlist to subword list
 5: use g2p to label subword list
 6: add <hes> word from word lexicion
 [examples]:

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

wlex=$1
model=$2
g2p_model=$3
dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

g2p=/home/opt/tools/g2p/g2p.py
mtool=$(which morfessor-segment)
if [ ! -f $g2p ] || [ -z $mtool ]; then
  echo "ERROR, tools for g2p or segment are not ready" && exit 1
fi
tmpdir=$dir/temp
logdir=$dir/log

[ -d $tmpdir ] || mkdir -p $tmpdir
[ -d $mtool ] || mkdir -p $logdir

if [ ! -z $step01 ]; then
  cat $textfile | awk '{$1 = ""; print;}' | gzip -c > $tmpdir/text.gz
  textfile=$tmpdir/text.gz
fi

if [ ! -z $step02 ]; then
  gzip -cd $textfile |\
  perl -pe 's/[\-_]/ /g;' | \
  perl -e 'while(<>){chomp; @A = split(/\s+/); 
    for($i = 0; $i < @A; ++$i) { next if($A[$i] =~ m/[\<\>\[\]]/);  print "$A[$i]\n";} }' | \
  sort -u > $tmpdir/wordlist-text.txt
fi

if [ ! -z $step03 ]; then
  cat $wlex | \
  perl -e 'while(<>){next if(m/[\<\>\[\]]/); print;}'| awk '{print $1;}' | \
  perl -pe 's/[\-\_]/\n/g;' | \
  sort -u > $tmpdir/wordlist-lex.txt
  for x in wordlist-text.txt wordlist-lex.txt; do
    [ -f $tmpdir/$x ] && cat $tmpdir/$x;
  done | perl -e 'while(<STDIN>){chomp; next if(m/^$/); print "$_\n";}' > $tmpdir/wordlist.txt
fi
morphlist=$tmpdir/morphlist.txt
if [ ! -z $step04 ]; then
  cat $tmpdir/wordlist.txt | \
  morfessor-segment -l $model -e utf8  - | \
  awk '{for(i=1; i<=NF; ++i){print $i;}}' | \
  perl -e "while(<>){ next if(m/[\'\_]/); print; }" | sort -u > $morphlist
fi
if [ ! -z $step05 ]; then
  echo "g2p started @ `date`"
  $g2p --model=$g2p_model --apply=$morphlist \
  >$dir/lexicon_src01.txt 2> $logdir/g2p.log 
  echo "g2p ended @ `date`"
fi

if [ ! -z $step06 ]; then
 grep '\<hes\>'  $wlex | cat - $dir/lexicon_src01.txt \
 > $dir/lexicon_src.txt
fi
