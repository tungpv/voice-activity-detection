#!/bin/bash 

. path.sh
. cmd.sh

# begin options
steps=
from_field=2
data_name=text
source_data=
# end options

. parse_options.sh || exit 1
echo
echo "$0 $@"
echo
function Usage {
cat<<END 
 $(basename $0) [options] <morfessor-model> <text> <dir>
 [options]:
 --steps                               # value, "$steps"
 --from-field                          # value, "$from_field"
 --data-name                           # value, "$data_name"
 --source-data                         # value, "$source_data"
 [steps]:
 1: normalize text, if a kaldi format text is given
 2: wordlist from text data
 3: make word-to-subword map file
 4: make subword text data

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

model=$1
text=$2
dir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

mtool=$(which morfessor-segment)

tmpdir=$dir/temp
logdir=$dir/log
[ -d $tmpdir ] || mkdir -p $tmpdir
[ -d $logdir ] || mkdir -p $logdir
stext=$text
if [ ! -z $step01 ]; then
  cat $text | awk -v from_field=$from_field '{for(i=1;i <= from_field; i++){ $i = ""; } print;}' | gzip -c > $tmpdir/text.gz
  text=$tmpdir/text.gz
 fi

if [ ! -z $step02 ]; then
  gzip -cd $text | \
  perl -pe 's/\[[^\[]+\]//g; s/<[^<]+>//g;' | \
  perl -pe 's/[\-\_]/ /g;' | \
  perl -e 'while(<>){chomp; @A = split(/[\s+]/);
    for($i = 0; $i < @A; ++ $i) {next if($A[$i] =~ m/[\<\>\[\]]/ || $A[$i] =~ /^$/); print "$A[$i]\n"; }
  }' | sort -u | gzip -c > $tmpdir/wordlist.gz
fi

if [ ! -z $step03 ]; then
  gzip -cd  $tmpdir/wordlist.gz | \
  morfessor-segment -l $model -e utf8  -  | gzip -c > $tmpdir/sublist.gz
  word_lines=$(gzip -cd $tmpdir/wordlist.gz| wc -l)
  sub_lines=$(gzip -cd $tmpdir/sublist.gz | wc -l)
  if [ $word_lines -ne $sub_lines ]; then
     echo "ERROR, inconsistent lines, word_lines=$word_lines, sub_lines=$sub_lines"
     exit 1
  fi
  paste <(gzip -cd $tmpdir/wordlist.gz) <(gzip -cd $tmpdir/sublist.gz) | gzip -c > $tmpdir/word-to-subword.gz
fi
if [ ! -z $source_data ]; then
   cp $source_data/*  $dir
   rm $dir/text
fi
if [ ! -z $step04 ]; then
  cat $stext | \
  source/egs/morfessor/make_text.pl $from_field "gzip -cd $tmpdir/word-to-subword.gz|" > $dir/$data_name
fi


