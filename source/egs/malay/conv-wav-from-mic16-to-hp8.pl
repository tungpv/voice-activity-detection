#!/usr/bin/perl
use warnings;
use strict;

my $cmd = $0;
$cmd =~ s:.*\/::g;
print STDERR "$cmd: stdin expected ...\n";
while(<STDIN>) {
  chomp;
  if(! m/(\S+)\s+(\S+)/) {
    print STDERR "$cmd: WARNING, bad line $_ observed\n";
    next;
  }
  my ($lab, $wavfile) = ($1, $2);
  $lab =~ s/_mic/_hp/g;
  $wavfile =~ s/_mic/_hp/g;
  if( ! -f $wavfile ) {
    print STDERR "$cmd: WARNING, file $wavfile does not exist\n";
    next;
  }
  my $s = "$lab ";
  $s .= "/usr/bin/sox $wavfile -r 8000 -c 1 -b 16 -t wav - downsample |";
  print "$s\n";
}
print STDERR "$cmd: stdin ended ...\n";
