#!/usr/bin/perl 
use warnings;
use strict;

my $cmd = $0;
$cmd =~ s/.*\///g;
print STDERR "$cmd: stdin expected\n";
while (<STDIN>) {
  s/<UNK>/<unk>/g;
  s/\[oov\]/<unk>/g;
  s/\[speech_noise\]/<noise>/g;
  s/\[event\]/<noise>/g;
  s/<event>/<noise>/g;
  s/!SIL/<silence>/g;
  s/\[filler_ah\]/<hes>/g;
  s/<filler_ah>/<hes>/g;
  s/\[filler_er\]/<hes>/g;
  s/<filler_er>/<hes>/g;
  s/\[filler_ha\]/<hes>/g;
  s/<filler_ha>/<hes>/g;
  s/\[filler_oh\]/<hes>/g;
  s/<filler_oh>/<hes>/g;
  s/<filler_uhhuh>/<hes>/g;
  s/\[filler_uhhuh\]/<hes>/g;
  print;
}
print STDERR "$cmd: stdin ended \n";
