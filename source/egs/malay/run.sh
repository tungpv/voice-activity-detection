#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
# end options

. parse_options.sh || exit 1

function PrintOptions {
  cat<<END

$(basename $0) [steps]  <datadir>
--steps                    # value, "$steps"

[steps]:
1: update lexicon, redefine non-content words, to keep consistent with babel lexicon
2: make silence phone file 
3: make non-silence phone/optional_silence  files
4: modify language model
5: make G fst from arpa lm file
6: prepare training data
7: prepare test data

END
}

if [ $# -ne 1 ]; then
  echo
  PrintOptions && exit 1;
fi

data=$1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  dir=$data/local
  [ -d $dir ] || mkdir -p $dir
  echo "step01 started @ `date`"
  cat hai-scripts/data/local/dict/lexicon.txt  | \
  perl -pe 'if(/!SIL/){$_="<silence>\tSIL\n";}
    elsif(/<UNK>/){$_="<unk>\t<oov>\n";}
    elsif(/\[event\]/) { $_ = "<noise>\t<sss>\n";}
    elsif(/\[filler_ah/){ $_ = "<hes>\tfah\n";}
    elsif(/\[filler_er/){ $_ = "<hes>\tfer\n";}
    elsif(/\[filler_ha/){ $_ = "<hes>\tfha\n"; }
    elsif(/\[filler_oh/) { $_ = "<hes>\tfoh\n";}
    elsif(/\[filler_uhhuh/){ $_ = "<hes>\tfahh\n";}
    else {
      chomp;
      @A = split(/[ \t]/);
      $s = shift @A;
      $s .= "\t"; $s .= join(" ", @A);
      $_ = "$s\n";
    }
  ' > $dir/lexicon.txt
  echo "step01 ended @ `date`"
fi
if [ ! -z $step02 ]; then
  echo "step03 started @ `date`"
  cat $dir/lexicon.txt | \
  egrep '<noise>|<unk>|<silence>' | \
  awk '{print $2;}' > $dir/silence_phones.txt
  cat $dir/lexicon.txt | \
  egrep '<silence>' | \
  awk '{print $2;}'  > $dir/optional_silence.txt
  cat $dir/silence_phones.txt | \
  perl -pe 'chomp; $_ = "$_ ";' > $dir/extra_questions.txt
  echo >> $dir/extra_questions.txt
  echo "step03 ended @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "step02 started @ `date`"
  cat $dir/lexicon.txt | \
  egrep -v '<noise>|<unk>|<silence>' | \
  perl -pe 'chomp; 
    @A = split(/[ \t]/); shift @A;
    $_ = "";
    for($i = 0; $i < @A; ++ $i) {
      $word = $A[$i];
      $_ .= "$word\n";
    }
  ' |sort -u | grep -v '^$'> $dir/nonsilence_phones.txt
  cat $dir/nonsilence_phones.txt | \
  perl -pe 'chomp; $_ = "$_ ";' >> $dir/extra_questions.txt
  echo >> $dir/extra_questions.txt
  echo "step03 started @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "step04: substitution started @ `date`"
  gzip -cd hai-scripts/data/local/lm/srilm/Written0.15AllForumFiller0.05AllForum0.05Google0.05Trans.kn.gz  | \
  sed -e 's#UNK#unk#g; s#\[event\]#\<noise\>#g; s#!SIL#\<silence\>#g; 
   s#\[filler_ah\]#\<hes\>#g; s#\[filler_er\]#\<hes\>#g; s#\[filler_ha\]#\<hes\>#g;
   s#\[filler_oh\]#\<hes\>#g; s#\[filler_uhhuh\]#\<hes\>#g
  '  | gzip -c > $dir/lm.gz
  echo "step04: lm substitution ended @ `date`"
fi

if [ ! -z $step05 ]; then
  echo "step05: make grammar G started @ `date`"
  local/arpa2G.sh $data/srilm/lm.gz  $data/lang $data/lang
  echo "step05: making grammar G ended @ `date`"
fi

if [ ! -z $step06 ]; then
  echo "step06: prepare training data started @ `date`"
  dir=$data/train
  [ -d $dir ] || mkdir -p $dir
  sdir=hai-scripts/data_plp/train
  cp $sdir/{text,wav.scp,segments} $dir/
  cat $sdir/text | \
  source/egs/malay/sub_text.pl | \
  source/egs/malay/filter_text.pl 2> $dir/filtered.txt \
  > $dir/text
  cat $dir/segments | \
  awk '{print $1, " ", $2;}' > $dir/utt2spk
  utils/utt2spk_to_spk2utt.pl < $dir/utt2spk > $dir/spk2utt 
  utils/fix_data_dir.sh $dir
  echo "step06: training data preparation ended @ `date`"
fi
