#!/bin/bash

sdata=malay-cts2016/data/test
data=malay-cts2016/old-data/test-mic8k

[ -d $data ] || mkdir -p $data

cp $sdata/* $data

cat $sdata/wav.scp | \
perl -pe 'chomp; m/(\S+)\s+(.*)/; $s = $1;  
  $s .= " /usr/bin/sox $2 -r 8000 -c 1 -b 16 -t wav - downsample |\n"; $_=$s;' > $data/wav.scp
