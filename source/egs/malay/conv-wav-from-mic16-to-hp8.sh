#!/bin/bash

. path.sh
. cmd.sh

if [ $# -ne 2 ]; then
  echo "$0 <sdat> <data>"
  exit 1
fi

sdata=$1
data=$2

[ -d $data ] || mkdir -p $data
cp $sdata/* $data
rm $data/{cmvn.scp,feats.scp}
cat $sdata/wav.scp | \
source/egs/malay/conv-wav-from-mic16-to-hp8.pl > $data/wav.scp

cat $sdata/text | \
perl -pe 's/_mic/_hp/g;' >$data/text

cat $sdata/segments | \
perl -pe 's/_mic/_hp/g;' > $data/segments

cat $sdata/reco2file_and_channel | \
perl -pe 's/_mic/_hp/g;' > $data/reco2file_and_channel

cat $sdata/utt2spk | \
perl -pe 's/_mic/_hp/g;' > $data/utt2spk

utils/utt2spk_to_spk2utt.pl < $data/utt2spk > $data/spk2utt

if [ -f $sdata/stm ]; then
  cat $sdata/stm | \
  perl -pe 's/_mic/_hp/g;' > $data/stm
fi

utils/fix_data_dir.sh $data
