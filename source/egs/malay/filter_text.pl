#!/usr/bin/perl
use warnings;
use strict;

my $cmd = $0;
$cmd =~ s/.*\///g;
my $non_content_words = "<noise> <unk> <silence> <hes>";
my %vocab = ();
sub LoadVocab {
  my($vocab, $non_content_words) = @_;
  my @A = split(/[ \t]/, $non_content_words);
  for(my $i = 0; $i < @A; $i ++) {
    my $word = $A[$i];
    $$vocab{$word} ++;
  }
}
sub IsNonContentUtt {
  my ($vocab, $uA) = @_;
  for(my $i = 0; $i < @$uA; $i ++) {
    my $word = $$uA[$i];
    if(not exists $$vocab{$word}) {
      return 0;
    }
  }
  return 1;
}
LoadVocab(\%vocab, $non_content_words);
print STDERR "$cmd: stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/[ \t]/); 
  my $utt = shift @A;
  if(IsNonContentUtt(\%vocab, \@A) == 1) {
    print STDERR $utt, " ", join(" ", @A), "\n";
  } else {
    print $utt, " ", join(" ", @A), "\n";
  }
}
print STDERR "$cmd: stdin ended\n";
