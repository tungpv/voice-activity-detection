#!/usr/bin/perl
use warnings;
use strict;

my $numArg = scalar @ARGV;
if ($numArg != 1) {
  die "\n\nExample: cat ctm| $0 seg_name_map >new_ctm\n\n";
}
my $sFile = shift @ARGV;
open(F, "$sFile") or
die "$0: ERROR, file $sFile cannot open";
my %vocab = ();
while(<F>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  $vocab{$1} = $2;
}
close F;
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(" ");
  my $sMapped = $vocab{$A[0]};
  die "$0: ERROR, $A[0] is unidentified\n" if not defined $sMapped;
  $A[0] = $sMapped;
  $A[1] = "A";
  print join(" ", @A), "\n";
}
print STDERR "$0: stdin ended\n";
