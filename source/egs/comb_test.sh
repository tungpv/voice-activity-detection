#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=20
cmd=run.pl
steps=
nnet_comb_test=false
nnet_comb_opts="lang tunedata tunename gmmdir graphname xsdir devdata devname decodename acwt denlat_conf stgtdir"
scoring_opts="--min-lmwt 8 --max-lmwt 24"
lattice_opts="--beam 13.0 --lattice-beam 8.0 --max-mem 800000000"


# end options

echo
echo "$0 $@"
echo

. parse_options.sh

function PrintOptions {
  cmdName=$(echo $0| perl -pe 's/^.*\///g;')
  cat <<END

$cmdName [options]:
cmd					# value, "$cmd"
nj					# value, "$nj"
steps					# value, "$steps"
nnet_comb_test				# value, $nnet_comb_test
nnet_comb_opts				# value, "$nnet_comb_opts"
scoring_opts				# value, "$scoring_opts"
lattice_opts				# value, "$lattice_opts"


END
}

PrintOptions;

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=true
  done
fi
if $nnet_comb_test; then
  echo "$0: nnet_comb_test started @ `date`"
  optNames="lang tunedata tunename gmmdir graphname xsdir devdata devname decodename acwt denlat_conf stgtdir"
  . source/register_options.sh "$optNames" "$nnet_comb_opts" || exit 1
  if [ ! -z $step01 ]; then
    echo "$0: xnnet test started @ `date`"
    source/egs/run-bnf.sh  --cmd "$cmd" --nj $nj --decoding_eval true --decoding_opts \
    "$devdata  $lang $gmmdir $gmmdir/$graphname  $xsdir/$decodename steps/nnet/decode.sh" \
    --scoring_opts "$scoring_opts" --lattice_opts "$lattice_opts" || exit 1
    echo "x: nnet testing ende @ `date`"
  fi
 alidir=$xsdir/ali-${tunename}
  if [ ! -z $step02 ]; then
    echo "$0: making num alignment started @ `date`"
    source/egs/run-bnf.sh --cmd "$cmd" --nj $nj --make_ali true --make_ali_opts \
    "$tunedata $lang $xsdir  $alidir" \
    --ali_cmd "steps/nnet/align.sh --nj $nj" || exit 1
    echo "$0: making num alignment ended @ `date`"
  fi
  latdir=$xsdir/denlats-${tunename}
  if [ ! -z $step03 ]; then
    echo "$0: making denlats started @ `date`"
    source/egs/run-bnf.sh --cmd "$cmd" --nj $nj \
    --make_denlats true --make_denlats_opts \
    "$acwt $denlat_conf  $tunedata $lang $xsdir $latdir" || exit 1
    echo "$0: making denlats ended @ `date`"
  fi
  if [ ! -z $step04 ]; then
    echo "$0: sequential training started @ `date`"
    source/egs/run-bnf.sh --cmd "$cmd" --nj $nj \
     --train_nnet_mpe true --train_nnet_mpe_opts \
    "$acwt  $tunedata $lang $xsdir $alidir $latdir $stgtdir" || exit 1
    echo "$0: sequential training ended @ `date`"
  fi
  if [ ! -z $step05 ]; then
    echo "$0: sequential test started @ `date`"
    source/egs/run-bnf.sh --cmd "$cmd" --nj $nj --decoding_eval true --decoding_opts \
    "$devdata  $lang $gmmdir $gmmdir/$graphname  $stgtdir/$decodename steps/nnet/decode.sh" \
    --scoring_opts "$scoring_opts" --lattice_opts "$lattice_opts" || exit 1
    echo "$0: sequential test ended @ `date`"
  fi
  echo "$0: nnet_comb_test ended @ `date`"
fi
