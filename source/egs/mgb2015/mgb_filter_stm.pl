#!/usr/bin/perl
use warnings;
use strict;

if (@ARGV != 1) {
  die "\n\n Example: cat stm | filter_stm.pl ctm > new_stm\n\n";
}
my ($ctmFile) = @ARGV;
open(F, "$ctmFile") or die "$0: ERROR, file $ctmFile cannot open\n";
my %vocab = ();
while(<F>) {
  chomp;  
  my @A =split(/[\s\t]/);
  my $utt = shift @A;
  my ($start, $end) = ($A[1], $A[2]);
  $end += $start;
  my @aT = ($start, $end);
  if (not exists $vocab{$utt}) {
    my @TEMP = ();
    $vocab{$utt} = \@TEMP;
    my $aRef = $vocab{$utt};
    push (@$aRef, \@aT);
  } else {
    my $aRef = $vocab{$utt};
    push(@$aRef, \@aT);
  }  
}
close F;
sub Overlap {
  my ($aRef, $start, $end) = @_;
  my ($s1, $e1) = @$aRef;
  return 0 if ($s1 > $end - 0.03 || $e1 < $start - 0.03 );
  return 1;
}
sub Overlapping {
  my ($aTime, $start, $end) = @_;
  my $overlapped = 0;
  for(my $i = 0; $i < @$aTime; $i ++) {
    my $aSeg = $$aTime[$i];
    next if Overlap($aSeg, $start, $end) == 0;
    $overlapped = 1;
  }
  return 1 if $overlapped == 1;
  return 0;
}

print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/[\s]/);
  my ($utt, $start, $end) = ($A[0], $A[3], $A[4]);
  next if not exists $vocab{$utt};
  next if Overlapping($vocab{$utt}, $start, $end) == 0;
  print "$_\n";
}
print STDERR "$0: stdin ended\n";
