#!/usr/bin/perl

use warnings;
use strict;

# cat ctm | mgb_convert_ctm_to_stm.pl segments  reco2file_and_channel > stm
if (@ARGV != 3) {
  die "\n\nExample: cat ctm | mgb_convert_ctm_to_stm.pl segments utt2spk reco2file_and_channel > stm\n\n";
}
my ($sSegFile, $utt2spk, $sReco2File) = @ARGV;

open(File, "$sSegFile") or die "$0: ERROR, file $sSegFile cannot open\n";
my %seg_vocab = ();
my $line_num = 0;
while(<File>) {
  chomp;
  $line_num ++;
  my @A = split(/[\s\t]/);
  die "$0: ERROR, bad line $_ @ line $line_num\n" if @A != 4;
  my $utt = shift @A;
  $seg_vocab{$utt} = join (" ", @A);
}
close File;
open(File, "$sReco2File") or die "$0: ERROR, file $sReco2File cannot open\n";
$line_num = 0;
my %rec_vocab = ();
while(<File>) {
  chomp;
  $line_num ++;
  my @A = split(/[\s\t]/);
  die "$0: ERROR, bad line $_ @ line $line_num\n" if @A != 3;
  my $recoId = shift @A;
  $rec_vocab{$recoId} = join(" ", @A);
}
close File;
open(File, "$utt2spk") or die "$0: ERROR, file $utt2spk cannot open\n";
my %utt2spk_vocab = ();
while(<File>) {
  chomp;
  next if(! m/(\S+)\s+(\S+)/);
  $utt2spk_vocab{$1} = $2;
}
close File;
print STDERR "$0: stdin expected \n";
$line_num = 0;
while(<STDIN>) {
  chomp;
  $line_num ++;
  my @A = split(/[\s\t]/);
  die "$0: ERROR, bad line $_ @ line $line_num\n" if @A < 5;
  my $utt = shift @A;
  shift @A;
  my ($start, $dur, $word)  = ($A[0], $A[1], $A[2]);
  die "$0: ERROR, utt $utt does not exist in seg file\n" if not exists $seg_vocab{$utt};
  my $sSegInfo = $seg_vocab{$utt};
  my ($sRecoId, $startTime, $endTime) = split(/[\s]+/, $sSegInfo);
  my ($beginTime, $endTime1) = ($startTime + $start, $startTime + $start + $dur);
  die "$0: ERROR, utt $sRecoId does not exist in reco2file_and_channel" if not exists $rec_vocab{$sRecoId};
  my $s = $rec_vocab{$sRecoId};
  die "$0: ERROR, utt $utt does not exist in utt2spk file" 
  if not exists $utt2spk_vocab{$utt};
  my $spk = $utt2spk_vocab{$utt};
  $s .= " $spk $beginTime $endTime1 $word\n";
  print $s;
}
print STDERR "$0: stdin ended\n";
