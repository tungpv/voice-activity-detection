#!/usr/bin/perl
use strict;
use warnings;

if( @ARGV != 2) {
  die "\n\nExample cat utt.ctm | $0 segments utt2spk > ctm.has-spk\n\n";
}
my ($segFile, $utt2spkFile) = @ARGV;
my %seg_vocab = ();
my %utt2spk_vocab = ();
open(F, "$segFile") or die "$0: File $segFile cannot open\n";
my $lineNum = 0;
while(<F>) {
  chomp;
  $lineNum ++;
  my @A = split(" ");
  die "$0: ERROR, bad line $_, line $lineNum" if @A != 4;
  my $utt = shift @A;
  $seg_vocab{$utt} = \@A;  
}
close F;
open(F, "$utt2spkFile") or 
die "$0: File $utt2spkFile cannot open\n";
$lineNum = 0;
while(<F>) {
  chomp;
  $lineNum ++;
  my @A = split(" ");
  die "$0: ERROR, at line $lineNum, bad line $_"  if @A != 2;
  $utt2spk_vocab{$A[0]} = $A[1];
}
close F;

print STDERR "$0: stdin expected\n";
$lineNum = 0;
while(<STDIN>) {
  chomp;
  $lineNum ++;
  my @A = split(" ");
  my $utt = shift @A;
  die "$0: ERROR, at line $lineNum, $utt($_) is not in segments" if not exists $seg_vocab{$utt}; 
  my ($sFile, $start, $end) = @{$seg_vocab{$utt}};
  die "$0: ERROR, at line $lineNum, $utt($_) is not in utt2spk" if not exists $utt2spk_vocab{$utt};
  my $spk = $utt2spk_vocab{$utt};
  $A[1] += $start;
  print  "$sFile $spk ", join (" ", @A), "\n";
}
print STDERR "$0: stdin ended\n";
