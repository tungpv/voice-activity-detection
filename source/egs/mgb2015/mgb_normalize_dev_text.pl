#!/usr/bin/perl
use strict;
use warnings;

use open qw(:std :utf8);

print STDERR "$0: stdin expected\n";
sub NotNormalLine {
  my ($s) = @_;
  my @A = split(/[\s]/);
  return 1 if @A == 1;
  shift @A;
  my $j = 0;
  for(my $i = 0; $i < @A; $i++) {
    my $w = $A[$i];
    $j ++ if($w =~ /</);
  }
  return 1 if ($j == @A);
  return 0;
}
while(<STDIN>) {
  chomp;
  my @A =split(/[\s]/);
  next if @A == 1;
  my $sLabName = shift @A;
  my $s = join (" ", @A);
  $s =~ s/[\-]/ /g;
  $_ = "$sLabName $s";
  s/\[[^\[]+\]//g;
  s/\([^\(]+\)/<unk>/g;
  s/[\#\$%]/<v-noise>/g;
  next if NotNormalLine($_) == 1;
  print "$_\n";
}
print STDERR "$0: stdin ended\n";
