#!/bin/bash

# Language model generation for MGB Challenge 2015 (Peter Bell, University of Edinburgh)

set -e

if [ $# -ne 1 ]; then
  echo "Usage: $0 <lm-training-dir>"
  exit 1;
fi

dir=data/local/lm
mkdir -p data/local/lm

data=$1/mgb.normalized.lm

# Estimate LM on the full training text
$SRILM/ngram-count -order 3 -kndiscount -interpolate -sort -text $data -lm $dir/mgb2015.full.3gm.kn.arpa.gz

# Restrict LM to the top 150k words and prune it
$SRILM/ngram -lm $dir/mgb2015.full.3gm.kn.arpa.gz -vocab local/resources/mgb.150k.wlist -limit-vocab -order 3 \
    -write-lm $dir/mgb2015.150k.3gm.kn.arpa.gz

$IRSTLM/prune-lm --threshold=1e-7 $dir/mgb2015.150k.3gm.kn.arpa.gz /dev/stdout | gzip -c > $dir/mgb2015.150k.p07.3gm.kn.arpa.gz

