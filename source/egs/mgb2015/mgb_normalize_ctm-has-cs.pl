#!/usr/bin/perl 
use strict;
use warnings;

print STDERR "$0: stdin expected\n";
my $line_num = 0;
while(<STDIN>) {
  chomp;
  $line_num ++;
  my @A = split(" ");
  die "$0: ERROR, at line $line_num, 5 fields expected, but $_\n" if @A != 5;
  $A[0] =~ s/_spk-\d+.*//g;
  $A[2] = sprintf("%.2f", $A[2] - $A[1]);
  die "$0: ERROR, at line $line_num, bad duration should be over zero, but $A[2]" if $A[2] <= 0;
  splice(@A, 1, 0, 1);
  print join (" ", @A), "\n";
}

print STDERR "$0: stdin ended\n";
