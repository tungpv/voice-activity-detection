#!/bin/bash -u

set -e

. ./cmd.sh
. ./path.sh

##########################################################
#
#  Initial notes
#
##########################################################

# To this recipe you'll need
# 1) An installation of Kaldi
# 1) SRILM http://www.speech.sri.com/projects/srilm/
# 2) xmlstarlet http://xmlstar.sourceforge.net/

# This script assumes that you are already familiar with Kaldi recipes.


##########################################################
#
#  Actions required from users
#
##########################################################

# TO DO: You will need to place the lists of training and dev data
# (train.full and dev.full) in this working directory, link to the
# usual steps/ and utils/ directories, and create your copies path.sh
# and cmd.sh in this directory.

# TO DO: specify the directories containing the binaries for
# xmlstarlet, SRILM and IRSTLM

XMLSTARLET=/disk/data1/software/xmlstarlet-1.5.0
SRILM=/disk/data1/software/kaldi-trunk/tools/srilm/bin/i686-m64
IRSTLM=/disk/data1/software/kaldi-trunk/tools/irstlm/bin

# TO DO: you will need to choose the size of training set you want.
# Here we select according to an upper threshhold on Matching Error
# Rate from the lightly supervised alignment.  When using all the
# training shows, this will give you training data speech segments of
# approximate lengths listed below:

# MER	duration (hrs)
#
# 10 	  240
# 20      400
# 30 	  530
# 40 	  640
# 50 	  740
# all    1210

mer=10  

# TO DO: set the location of downloaded WAV files, XML, LM text and the Combilex Lexicon

# Location of downloaded WAV files
WAV_DIR=/disk/data1/mgb2015/wav

# Location of downloaded XML files
XML_DIR=/disk/data1/mgb2015/xml

# Location of downloaded LM text
LM_DIR=/disk/data1/mgb2015/lm

# Location of Combilex lexicon files
LEX_DIR=/disk/data1/mgb2015/lex

nj=30  # split training into how many jobs?

##########################################################
#
#  Recipe
#
##########################################################


#1) Data preparation

export XMLSTARLET SRILM IRSTLM

echo "Preparing training data"
local/mgb_data_prep.sh $WAV_DIR $XML_DIR $mer

echo "Training n-gram language model"
local/mgb_train_lm.sh $LM_DIR

echo "Preparing dictionary"
local/mgb_prepare_dict.sh $LEX_DIR

echo "Preparing lang dir"
utils/prepare_lang.sh data/local/dict "<unk>" data/local/lang data/lang

echo "Computing features"
mfccdir=data/mfcc_train_mer$mer
steps/make_mfcc.sh --nj $nj --cmd "$train_cmd" data/train_mer$mer exp/mer$mer/make_mfcc/train/log $mfccdir
steps/compute_cmvn_stats.sh data/train_mer$mer exp/mer$mer/make_mfcc/train/log $mfccdir

LM=mgb2015.150k.p07.3gm.kn
utils/format_lm.sh data/lang data/local/lm/$LM.arpa.gz data/local/dict/lexicon.txt data/lang_$LM

# 2) Building GMM systems
# This is based on the standard Kaldi GMM receipe

numutts=`cat data/train_mer$mer/feats.scp | wc -l`
utils/subset_data_dir.sh --shortest data/train_mer$mer $[numutts/3] data/train_mer${mer}_short
# take a random 10k from this
utils/subset_data_dir.sh data/train_mer${mer}_short 10000 data/train_mer${mer}_10k
# take first 30k utterances for the first two passes of triphone training
utils/subset_data_dir.sh --first data/train_mer$mer 30000 data/train_mer${mer}_30k

steps/train_mono.sh --nj 10 --cmd "$train_cmd" \
  data/train_mer${mer}_10k data/lang exp/mer$mer/mono 

steps/align_si.sh --nj $nj --cmd "$train_cmd" \
  data/train_mer${mer}_30k data/lang exp/mer$mer/mono exp/mer$mer/mono_ali 

steps/train_deltas.sh --cmd "$train_cmd" \
  3200 30000 data/train_mer${mer}_30k data/lang exp/mer$mer/mono_ali exp/mer$mer/tri1 

steps/align_si.sh --nj $nj --cmd "$train_cmd" \
  data/train_mer${mer}_30k data/lang exp/mer$mer/tri1 exp/mer$mer/tri1_ali 

steps/train_deltas.sh --cmd "$train_cmd" \
  3200 30000 data/train_mer${mer}_30k data/lang exp/mer$mer/tri1_ali exp/mer$mer/tri2

# now train on full data
steps/align_si.sh --nj $nj --cmd "$train_cmd" \
  data/train_mer$mer data/lang exp/mer$mer/tri2 exp/mer$mer/tri2_ali

# Train tri3, which is LDA+MLLT, on full
steps/train_lda_mllt.sh --cmd "$train_cmd" \
  5500 90000 data/train_mer$mer data/lang exp/mer$mer/tri2_ali exp/mer$mer/tri3 


# Train tri4, which is LDA+MLLT+SAT, on train_nodup data
steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
  data/train_mer$mer data/lang exp/mer$mer/tri3 exp/mer$mer/tri3_ali_nodup 

steps/train_sat.sh  --cmd "$train_cmd" \
  11500 200000 data/train_mer$mer data/lang exp/mer$mer/tri3_ali_nodup exp/mer$mer/tri4

# align the final models
steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
  data/train_mer$mer data/lang exp/mer$mer/tri4 exp/mer$mer/tri4_ali

# generate graph
utils/mkgraph.sh data/lang_$LM exp/mer$mer/tri4 exp/mer$mer/tri4/graph_$LM

