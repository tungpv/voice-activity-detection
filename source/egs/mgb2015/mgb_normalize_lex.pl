#!/usr/bin/perl
use warnings;
use strict;
use open qw(:std :utf8);

print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/[\s\t]/);
  my $w = shift @A;
  my $pron = join(" ", @A);
  print "$w\t$pron\n";
}
print STDERR "$0: stdin ended\n";
