#!/usr/bin/perl
use warnings;
use strict;

use open qw(:std :utf8);

print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/[\s\t]/);
  if (scalar @A != 4) {
    die "$0: ERROR, bad segment line: $_\n";
  }
  if($A[2] >= $A[3]) {
    print STDERR "$0: WARNING: remove bad lines $_\n";
    next;
  }
  print $_, "\n";
}
print STDERR "$0: stdin ended\n";
