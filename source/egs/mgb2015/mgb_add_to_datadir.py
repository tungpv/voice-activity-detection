#!/usr/bin/python

# This script appends utterances dumped out from XML to a Kaldi datadir

import sys, re, os
from optparse import OptionParser

parser = OptionParser()
parser.add_option('--make-utt2mer', action="store_true", dest='make_utt2mer', default=False, help='make utt2mer file')
parser.add_option('--basename', dest='basename', help='xml file basename')
parser.add_option('--tgtdir', dest='tgtdir', help='target data dir')
parser.add_option('--mer-thresh', dest='mer_thresh', help='MER threshold', default=None)

(options, args) = parser.parse_args()
if not options.basename or not options.tgtdir:
	parser.print_help()
	sys.exit(1) 
basename = options.basename
tgtdir = options.tgtdir
if options.make_utt2mer == True:
	# print 'the make_utt2mer is true'
	sFileName = tgtdir + '/utt2mer'
	# print sFileName
	# os.system('rm %s' %(sFileName))
	utt2mer_file = open(tgtdir + '/utt2mer', 'a')
	for line in sys.stdin:
		m = re.match(r'\w+speaker(\d+)\w+\s+(.*)', line)

		if m:
			spk = int(m.group(1))
			t = m.group(2).split()
			start = float(t[0])
			end = float(t[1])
			mer = float(t[2])
			words = ' '.join(t[3:])
			segId = '%s_spk-%04d_seg-%07d:%07d' % (basename, spk, start*100, end*100)
			spkId = '%s_spk-%04d' % (basename, spk)
			utt2mer_file.write('%s %.2f\n' %(segId, mer))
	utt2mer_file.close()
	sys.exit(0)

# basename=args[0]
# tgtdir = args[1]

# if len(args) > 3:
#     mer_thresh=float(args)
# else:
#     mer_thresh = None

# open the output files in append mode
segments_file = open(tgtdir + '/segments', 'a')
utt2spk_file = open(tgtdir + '/utt2spk', 'a')
text_file = open(tgtdir + '/text', 'a')
mer_thresh = options.mer_thresh
for line in sys.stdin:

    m = re.match(r'\w+sp(\d+)\w+\s+(.*)', line)

    if m:

        spk = int(m.group(1))

        t = m.group(2).split()
        start = float(t[0])
        end = float(t[1])
	if len(sys.argv) > 3:
		mer = float(t[2])
	else:
		mer = -10000
        words = ' '.join(t[3:])

        segId = '%s_spk-%04d_seg-%07d:%07d' % (basename, spk, start*100, end*100)
        spkId = '%s_spk-%04d' % (basename, spk)

        # only add segments where the Matching Error Rate is below the prescribed threshhold
        if mer_thresh == None or mer <= mer_thresh:
            print >> segments_file, '%s %s %.2f %.2f' % (segId, basename, start, end ) 
            print >> text_file, '%s %s' % (segId, words)
            print >> utt2spk_file, '%s %s' % (segId, spkId)

segments_file.close()
utt2spk_file.close()
text_file.close()
 
            
