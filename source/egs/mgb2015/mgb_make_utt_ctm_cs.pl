#!/usr/bin/perl
use strict;
use warnings;

print STDERR "$0: stdin expected\n";
my $lineNum = 0;
while(<STDIN>) {
  chomp;
  $lineNum ++;
  next if ! m/(\S+)\s+(.*)/;
  my ($lab, $utt) = ($1, $2);
  $lab =~ s/_lsdecode$//;
  die "$0: ERROR, bad line $_ @ $lineNum\n" if(! $lab =~ m/.*_sp\d+/);
  $lab =~ m/.*sp(\d+)/;
  my $spkId = $1;
  $spkId = sprintf("spk-%04d",$spkId);
  $lab =~ s/_sp\d+/_$spkId/;
  
  while(m/(\[[^\[]+\])/g) {
    my $sLine = $1;
    $sLine =~ s/[[\[\]]//g;
    my @A = split(" ", $sLine); 
    my $nField = scalar @A;
    die "$0: ERROR, $sLine @ $lab:$lineNum($nField) \n" if $nField != 4;
    my $sWordInfo = "$A[1] $A[2] $A[3] $A[0]";
    # my $sSeg = sprintf("seg-%07d:%07d", $A[1]*100, $A[2]*100);
    # my $curUttName = $lab.'_'.$sSeg;
    print "$lab $sWordInfo\n";
  }
}
print STDERR "$0: stdin ended\n";
