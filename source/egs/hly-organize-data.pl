#!/usr/bin/perl 
use strict;
use warnings;
use open qw(:std :utf8);

print STDERR "\n$0 ", join(" ", @ARGV), "\n";

print STDERR "$0: stdin expected\n";
my $lines = 0;
my %vocab = ();
while(<STDIN>) {
  chomp;
  if($lines++ == 0) {
    print STDERR "$_\n";
    next;
  } 
  my @A = split(/\t/);
  my $authors = shift @A;
  my $affis = $A[12];
  # print "$authors\t$affis\n";
  # $vocab{$authors} = join ("\t", @A) ;
  $vocab{$authors} = $affis;
}

print STDERR "$0: stdin ended\n";
sub SplitAuthors {
  my ($string, $strArray) = @_;
  $string =~ s/[\"]//g;
  my @A = split(/[,]/, $string);
  my $num = scalar @A;
  if ($num < 2) {
    my $pairStr = $A[0];
    push (@$strArray, \$pairStr);
  } elsif ($num == 2) {
    my $pairStr = "$A[0], $A[1]";
    push @$strArray, \$pairStr;
  }
  return if ($num <=2);
  for(my $i = 0; $i < $num; $i ++) {
    my $firstStr = $A[$i];
    for(my $j = $i+1; $j < $num; $j ++) {
      my $secStr = $A[$j];
      my $pairStr = "$firstStr, $secStr";
      push (@$strArray, \$pairStr);
    }
  }
}
sub SplitAuthorWithAffi {
  my ($authStr, $affStr, $strArray, $splitNum) = @_;
  $authStr =~ s/[\"]//g; 
  $affStr =~ s/[\"]//g;
  my @authArray = split(/[,]/, $authStr);
  my @affArray = split(/[;]/,$affStr);
  my $authNum = scalar @authArray;
  my $affNum = scalar @affArray;
  return if $authNum ==0 || $authNum != $affNum && $affNum != 1;
  $$splitNum ++;
  if ($affNum == 1 && $authNum > 1 ) {
    for (my $i = 1; $i< $authNum; $i++) {
      push (@affArray, $affArray[$i-1]);
    } 
  }
  $affNum = scalar @affArray;
  die "$0: ERROR, author_number($authNum) != affiliation_num ($affNum)\n" if $authNum != $affNum;
  if( $authNum < 2) {
    my $s1 = "\"$authArray[0]\"";
    $s1 .= "\t\"$affArray[0]\"";
    my $s="$s1\t$s1";
    push @$strArray, \$s;
    return;
  } 
  for(my $i = 0; $i < $authNum-1; $i ++) {
    $authArray[$i] =~ s/^ //g;
    my  $s = "\"$authArray[$i]\"";
    $affArray[$i] =~ s/^ //g;
    $s .= "\t\"$affArray[$i]\"";
    $authArray[$i+1] =~ s/^ //g;
    $s .= "\t\"$authArray[$i+1]\"";
    $affArray[$i+1] =~ s/^ //g;
    $s .= "\t\"$affArray[$i+1]\"";
    push @$strArray, \$s;
  } 
} 
my ($totNum, $splitNum) = (0, 0);
$totNum = keys (%vocab);
foreach my $authors (keys %vocab) {
  my @authorPairs = ();
  # SplitAuthors($authors, \@authorPairs);
  my $affStr = $vocab{$authors};
  SplitAuthorWithAffi($authors, $affStr, \@authorPairs, \$splitNum);
  for (my $i = 0; $i < scalar @authorPairs; $i++) {
    my $s = $authorPairs[$i];
    print "$$s\n"; 
  }
}
print STDERR "$0: totNum=$totNum, splitNum=$splitNum\n";
