#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
cmd=slurm.pl
nj=40
data_name=merge-ted-libri680hrs
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF

 $0 [options] <data> <lang> <source_gmm_dir> <tgtdir>
 [options]:
 --data-name                                       # value, "$data_name"
 [steps]:
 [examples]:

 $0 --steps 1 --cmd "$cmd" --nj $nj \
 --data-name $data_name \
 /home2/hhx502/ted-libri-en/data/train-merge-ted12-libri460/mfcc-pitch \
 /home2/hhx502/ted-libri-en/data/g2p-libri/lang \
 /home2/hhx502/ted-libri-en/exp/tri4b \
/home2/hhx502/ted-libri-en/exp

EOF
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi
source_data=$1
lang=$2
source_gmm_dir=$3
tgtdir=$4

set -e
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $tgtdir ] || mkdir -p $tgtdir
alidir=$source_gmm_dir/ali-${data_name}
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): align data @ `date`"
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj \
  $source_data $lang $source_gmm_dir $alidir || exit 1
  echo "## LOG (step01, $0): alignment done @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): train gmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1,2,3,4,5,6,7 \
  --alidir  $alidir \
  --train-id a --cmvn-opts "--norm-means=true" --state-num 9000 --pdf-num 180000 \
  --devdata /home2/hhx502/ted-libri-en/data/dev_tedlium_r1-3m/mfcc-pitch \
  $source_data $lang  $tgtdir || exit 1
 echo "## LOG (step02, $0): done with gmm training @ `date`"
fi
