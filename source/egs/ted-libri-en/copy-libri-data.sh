#!/bin/bash

. path.sh
. cmd.sh 

echo 
echo "## LOG: $0 $@"
echo

# begin options
steps=
# end options

. parse_options.sh || exit 1

function Usage {
 cat <<EOF
 
 Usage: $0 [options] <source_data> <tgtdata>
 [options]:
 [steps]:
 1: make segments
 2: copy data
 3: lowercase data

 [examples]:
 
 $0 --steps 1 /home2/hhx502/ted-libri-en/data/train-clean-100-libri /home2/hhx502/ted-libri-en/data/train-clean-100-libri-tbm

EOF
} 

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

source_data=$1
tgtdata=$2


if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
[ -d $tgtdata ] || mkdir -p $tgtdata
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): make segments"
  utt2dur=$source_data/utt2dur
  if [ ! -f $utt2dur ]; then
    utils/data/get_utt2dur.sh $source_data
  fi
  segments=$source_data/segments
  if [ ! -f $segments ]; then
    cat $utt2dur | \
    perl -ane 'chomp; @A = split(/\s+/); printf("%s %s %d %.2f\n", $A[0], $A[0], 0, $A[1]);' \
    > $segments
  fi
  echo "## LOG (step01, $0): making segments done"
fi
if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): copy data"
  cp $source_data/* $tgtdata/
  echo "## LOG (step02, $0): done"
fi
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): lowercase kaldi text "
  cat $source_data/text | \
  perl -ane 'chomp; use utf8; use open qw (:std :utf8); @A = split(/\s+/); $utt = shift @A;
    $s = lc join(" ", @A); print "$utt $s\n";' > $tgtdata/text
  echo "## LOG (step03, $0): lowercase done"
fi
