#!/bin/bash 

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh

# begin options
steps=1
cmd="slurm.pl --exclude=node01"
nj=40
release1_dict_dir=/home2/hhx502/ted-libri-en/data/local_tedlium_r1/dict_nosp
release2_dict=/data/users/hhx502/english-corpus/tedlium_release2/tedlium.152k.dic
# end options

function Usage {
 cat<<END
 Usage: $0 [options] <source_data>  <tgtdir>
 [options]:
 --cmd                                   # value, "$cmd"
 --nj                                    # value, "$nj"
 --release1-dict-dir                     # value, "$release1_dict"
 [examples]:

 $0 --steps 1  --release1-dict-dir $release1_dict_dir  \
 --release2-dict $release2_dict \
 /data/users/hhx502/english-corpus  /home2/hhx502/ted-libri-en 

END
}

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

source_data=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

tgtdata=$tgtdir/data
tgtlocal=$tgtdata/local
[ -d $tgtlocal ] || mkdir -p $tgtlocal

ted1=$source_data/tedlium_release1
ted2=$source_data/tedlium_release2

if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): prepare data @ `date`"
  source/egs/ted-libri-en/tedr2/prepare_data.sh  tedlium_s5_r2 $source_data $tgtdata
  echo "## LOg (step01, $0): prepare data done  @ `date`"
fi
id=ted_r2
tgtlocal=$tgtdata/local_${id}
tgtdict=$tgtlocal/dict
main_dict=$release1_dict_dir/lexicon.txt
sec_dict=$release2_dict
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): prepare dict"
  [ -d $tgtdict ] || mkdir -p $tgtdict
  cat $sec_dict | grep -v -w "<s>" | grep -v -w "</s>" | grep -v -w "<unk>" | \
  LANG= LC_ALL= sort | sed 's:([0-9])::g' | \
  source/egs/ted-libri-en/merge-dict.pl $main_dict | sort  > $tgtdict/lexicon.txt
  cp $release1_dict_dir/{silence_phones.txt,optional_silence.txt,nonsilence_phones.txt,extra_questions.txt} $tgtdict/
  echo "## LOG (step03, $0): done dict preparation ('$tgtdict')"
fi

if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): prepare lang started @ `date`"
  utils/prepare_lang.sh $tgtlocal/dict_nosp \
    "<unk>" $tgtdata/lang_nosp-$id $tgtdata/lang_nosp-$id
  echo "## LOG (step04, $0): done lang preparation @ `date`"
fi
if [ ! -z $step05 ]; then
  echo "## LOG (step05, $0): prepare lm @ `date`"
  tedlium_s5/local/prepare_lm.sh $source_data/tedlium_release1 $tgtdata/lang_nosp-$id
  echo "## LOG (step05, $0): lm preparation done @ `date`"
fi
if [ ! -z $step06 ]; then
  echo "## LOG (step05, $0): make features @ `date`"
  for x in train_tedlium_r1-3m test_tedlium_r1-3m dev_tedlium_r1-3m; do
    sdata=$tgtdata/$x; data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    utils/fix_data_dir.sh $sdata
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --mfcc-for-ivector true \
    --mfcc-cmd "tedlium_s5/steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc16k.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --fbank-pitch true \
    --fbank-pitch-cmd "tedlium_s5/steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
  done
  echo "## LOG (step05, $0): done @ `date`"
fi
exp=$tgtdir/exp-$id
train_mfcc=$tgtdata/train_tedlium_r1-3m/mfcc-pitch
dev_mfcc=$tgtdata/dev_tedlium_r1-3m/mfcc-pitch
lang=$tgtdata/lang_nosp-tedlium_r1-test
if [ ! -z $step07 ]; then
  echo "## LOG (step07, $0): train gmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd "$cmd" --nj $nj \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 5000 --pdf-num 100000 \
  --devdata $dev_mfcc \
  $train_mfcc $lang $exp || exit 1
  echo "## LOG (step07, $0): done ($exp/tri4a) @ `date`"
fi

train_fbank=$tgtdata/train_tedlium_r1-3m/fbank-pitch
dev_fbank=$tgtdata/dev_tedlium_r1-3m/fbank-pitch
if [ ! -z $step08 ]; then
  echo "## LOG (step08, $0): train dnn @ `date`"
  source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 --delta-opts "--delta-order=2" \
   --nnet-pretrain-cmd "/home/hhx502/w2016/steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048" \
  --use-partial-data-to-pretrain true --pretraining-rate 0.5   --nnet-id nnet5a  \
  --train-tool-opts "--minibatch-size=2048 --randomizer-size=32768 --randomizer-seed=777" \
  --graphdir /home2/hhx502/ted-libri-en/exp-tedlium_r1/tri4a/graph \
  --devdata $dev_fbank \
  $train_fbank $lang $exp/tri4a/ali_train \
  $exp || exit 1
  echo "## LOG (step08, $0): dnn training done @ `date`"
fi
