#!/bin/bash 

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh

# begin options
steps=1
cmd="slurm.pl --exclude=node01"
nj=40
release1_dict_dir=/home2/hhx502/ted-libri-en/data/local_tedlium_r1/dict_nosp
release2_dict=/data/users/hhx502/english-corpus/tedlium_release2/tedlium.152k.dic
# end options

function Usage {
 cat<<END
 Usage: $0 [options] <source_data>  <tgtdir>
 [options]:
 --cmd                                   # value, "$cmd"
 --nj                                    # value, "$nj"
 --release1-dict-dir                     # value, "$release1_dict"
 [examples]:

 $0 --steps 1  --release1-dict-dir $release1_dict_dir  \
 --release2-dict $release2_dict \
 /data/users/hhx502/english-corpus  /home2/hhx502/ted-libri-en 

END
}

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

source_data=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

tgtdata=$tgtdir/data
tgtlocal=$tgtdata/local
[ -d $tgtlocal ] || mkdir -p $tgtlocal

ted1=$source_data/tedlium_release1
ted2=$source_data/tedlium_release2

if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): prepare data @ `date`"
  source/egs/ted-libri-en/tedr2/prepare_data.sh  tedlium_s5_r2 $source_data $tgtdata
  echo "## LOg (step01, $0): prepare data done  @ `date`"
fi
id=ted_r2
tgtlocal=$tgtdata/local_${id}
tgtdict=$tgtlocal/dict
main_dict=$release1_dict_dir/lexicon.txt
sec_dict=$release2_dict
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): prepare dict"
  [ -d $tgtdict ] || mkdir -p $tgtdict
  cat $sec_dict | grep -v -w "<s>" | grep -v -w "</s>" | grep -v -w "<unk>" | \
  LANG= LC_ALL= sort | sed 's:([0-9])::g' | \
  source/egs/ted-libri-en/merge-dict.pl $main_dict | sort  > $tgtdict/lexicon.txt
  cp $release1_dict_dir/{silence_phones.txt,optional_silence.txt,nonsilence_phones.txt,extra_questions.txt} $tgtdict/
  echo "## LOG (step03, $0): done dict preparation ('$tgtdict')"
fi
tgtlang=$tgtdata/lang-$id
if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): prepare lang started @ `date`"
  utils/prepare_lang.sh $tgtdict \
    "<unk>" $tgtlang $tgtlang
  echo "## LOG (step04, $0): done lang ('$tgtlang') preparation @ `date`"
fi
id=libri
libri_source_data=/data/users/hhx502/english-corpus/librispeech
if [ ! -z $step05 ]; then
  echo "## LOG (step05, $0): prepare libri data @ `date`"
  for part in train-clean-100 train-clean-360; do
    source/egs/ted-libri-en/libri/data_prep.sh $libri_source_data/$part \
    $tgtdata/${part}-${id}
  done
  echo "## LOG (step05, $0): libri data preparation done  @ `date`"
fi

if [ ! -z $step06 ]; then
  echo "## LOG (step05, $0): make features @ `date`"
  for x in train_tedlium_r1-3m test_tedlium_r1-3m dev_tedlium_r1-3m; do
    sdata=$tgtdata/$x; data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    utils/fix_data_dir.sh $sdata
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --mfcc-for-ivector true \
    --mfcc-cmd "tedlium_s5/steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc16k.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --fbank-pitch true \
    --fbank-pitch-cmd "tedlium_s5/steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
  done
  echo "## LOG (step05, $0): done @ `date`"
fi
exp=$tgtdir/exp-$id
train_mfcc=$tgtdata/train_tedlium_r1-3m/mfcc-pitch
dev_mfcc=$tgtdata/dev_tedlium_r1-3m/mfcc-pitch
lang=$tgtdata/lang_nosp-tedlium_r1-test
if [ ! -z $step07 ]; then
  echo "## LOG (step07, $0): train gmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd "$cmd" --nj $nj \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 5000 --pdf-num 100000 \
  --devdata $dev_mfcc \
  $train_mfcc $lang $exp || exit 1
  echo "## LOG (step07, $0): done ($exp/tri4a) @ `date`"
fi

train_fbank=$tgtdata/train_tedlium_r1-3m/fbank-pitch
dev_fbank=$tgtdata/dev_tedlium_r1-3m/fbank-pitch
if [ ! -z $step08 ]; then
  echo "## LOG (step08, $0): train dnn @ `date`"
  source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 --delta-opts "--delta-order=2" \
   --nnet-pretrain-cmd "/home/hhx502/w2016/steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048" \
  --use-partial-data-to-pretrain true --pretraining-rate 0.5   --nnet-id nnet5a  \
  --train-tool-opts "--minibatch-size=2048 --randomizer-size=32768 --randomizer-seed=777" \
  --graphdir /home2/hhx502/ted-libri-en/exp-tedlium_r1/tri4a/graph \
  --devdata $dev_fbank \
  $train_fbank $lang $exp/tri4a/ali_train \
  $exp || exit 1
  echo "## LOG (step08, $0): dnn training done @ `date`"
fi

if [ ! -z $step09 ]; then
  echo "## LOG (step09, $0): normalize and copy libri data "
  source/egs/ted-libri-en/copy-libri-data.sh --steps 1,2,3 /home2/hhx502/ted-libri-en/data/train-clean-100-libri /home2/hhx502/ted-libri-en/data/train-clean-100-libri-tbm
  source/egs/ted-libri-en/copy-libri-data.sh --steps 1,2,3 /home2/hhx502/ted-libri-en/data/train-clean-360-libri /home2/hhx502/ted-libri-en/data/train-clean-360-libri-tbm
  echo "## LOG (step09, $0): done"
fi
data_merge=/home2/hhx502/ted-libri-en/data/train-merge-ted12-libri460
if [ ! -z $step10 ]; then
  echo "## LOG (step10, $0): merge tedr1, tedr2, libri100, libri360 to train system"
  utils/data/combine_data.sh $data_merge /home2/hhx502/ted-libri-en/data/train_tedlium_r1-3m\
  /home2/hhx502/ted-libri-en/data/train_ted_r2 /home2/hhx502/ted-libri-en/data/train-clean-100-libri-tbm \
  /home2/hhx502/ted-libri-en/data/train-clean-360-libri-tbm
  echo "## LOG (step10, $0): done "
fi

if [ ! -z $step11 ]; then
  echo "## LOG (step11, $0): normalize the text"
  echo -e "[BREATH]\t<breath>\n[NOISE]\t<noise>\n[COUGH]\t<cough>\n[SMACK]\t<smack>\n[UM]\t<um>\n[UH]\t<uh>" \
  > $data_merge/noncontent-word-map.txt

  cat $data_merge/text-history | \
  source/egs/ted-libri-en/rewrite-text.pl   $data_merge/noncontent-word-map.txt > $data_merge/text
  echo "## LOG (step11, $0): done with normalization"
fi

sdata=$data_merge
if [ ! -z $step12 ]; then
  echo "## LOG (step12, $0): make features @ `date`"
  data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
  source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --mfcc-for-ivector true \
  --mfcc-cmd "tedlium_s5/steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc16k.conf --pitch-config conf/pitch16k.conf" \
  $sdata $data $feat || exit 1
  echo "## LOG (step12, $0): making feature $data done @ `date`"
  data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch
  source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --fbank-pitch true \
  --fbank-pitch-cmd "tedlium_s5/steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
  $sdata $data $feat || exit 1
  echo "## LOG (step12, $0): done with feature extractiong @ `date`"
fi

if [ ! -z $step13 ]; then
  echo "## LOG (step13, $0): cure wav.scp"
  cat $sdata/wav-history.scp | \
  perl -ane  'chomp; @A = split(/\s+/); $num = scalar @A; 
    if($A[$num-2] =~ /release1/) {
       $A[$num-2] = lc $A[$num-2];
       if ( ! -e $A[$num-2]) {
         die "## ERROR#1: file $A[$num-2] does not exist\n";
       }
    } elsif ($A[$num-2] =~ /release2/) {
      $A[$num-2] =~ s#/home/hhx502/w2016/ted-libri-en/db/TEDLIUM_release2#/data/users/hhx502/english-corpus/tedlium_release2#g;
      $A[$num-2] = lc $A[$num-2];
      if (! -e $A[$num-2]) {
        die "## ERROR#2: file $A[$num-2] does not exist \n";
      }
    } elsif( ! -e $A[$num-2]) {
      die "## ERROR#3: file $A[$num-2] does not exist\n";
    }
    print join(" ", @A), "\n";' > $sdata/wav.scp
  echo "## LOG (step13, $0): done with curing ($sdata/wav.scp)"
fi
devdata=/home2/hhx502/ted-libri-en/data/dev_tedlium_r1-3m
lang=/home2/hhx502/ted-libri-en/data/g2p-libri/lang
lmdir=/home2/hhx502/ted-libri-en/data/g2p-libri/lm
if [ ! -z $step14 ]; then
  echo "## LOG (step14, $0): prepare lm @ `date`"
  [ -d $lmdir ] || mkdir -p $lmdir
  cat $devdata/text | \
  perl -ane 'chomp; @A= split(/\s+/);
    shift @A; print join(" ", @A), "\n";' | gzip -c > $lmdir/dev-text.gz
  cat $sdata/text | \
  perl -ane 'chomp; @A= split(/\s+/);
    shift @A; print join(" ", @A), "\n";' | gzip -c > $lmdir/train-text.gz
  cat $lang/words.txt | grep -v '<eps>' | egrep -v '\#' | \
  gzip -c > $lmdir/vocab.gz
  source/egs/kws2016/georgian/train-srilm-v2.sh --steps 1,2 \
  --lm-order-range "3 3" --cutoff-csl "3,011,012" $lmdir
  echo "## LOG (step14, $0): lm preparation done ('$lmdir')  @ `date`"
fi
if [ ! -z $step15 ]; then
  echo "## LOG (step15, $0): prepare grammar"
  source/egs/fisher-english/arpa2G.sh $lmdir/lm.gz $lang $lang
  echo "## LOG (step15, $0): done"
fi
