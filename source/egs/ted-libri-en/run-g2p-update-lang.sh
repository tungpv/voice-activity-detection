#!/bin/bash 

. path.sh
. cmd.sh 

echo 
echo "## LOG: $0 $@"
echo

# begin options
steps=
# end options

function Usage {
cat<<EOF

 Usage: $0 [options] <source_lexicon.txt> <target_kaldi_text> <tgtdir>
 [options]:
 [steps]:
 1: prepare data to train g2p model
 2: train g2p

 [examples]:
 
 $0 --steps 1 /home2/hhx502/ted-libri-en/data/local_ted_r2/dict/lexicon.txt \
 "/home2/hhx502/ted-libri-en/data/train-clean-100-libri/text /home2/hhx502/ted-libri-en/data/train-clean-360-libri/text" \
 /home2/hhx502/ted-libri-en/data/g2p-libri

EOF
}

set -e 
. parse_options.sh || exit 1

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

source_lexicon=$1
tgttext=$2
tgtdir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $tgtdir ] || mkdir -p $tgtdir

tgtdata=$tgtdir/data
[ -d $tgtdata ] || mkdir -p $tgtdata
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): prepare data to train g2p model"
  cat $tgttext | gzip -c > $tgtdata/text.gz
  cat $source_lexicon| egrep -v '<|\[' > $tgtdata/lex.txt
  echo "## LOG (step01, $0): data preparation ('$tgtdata') done "
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): train model @ `date`"
  [ -f $tgtdata/lex.txt ] || { echo "## ERROR (step02, $0): lex.txt expected from $tgtdata"; exit 1; }
  source/egs/g2p/run-g2p.sh --steps 2,3,4 $tgtdata/lex.txt $tgtdir
  echo "## LOG (step02, $0): done with training ('$tgtdir') @ `date`"
fi
oovdir=$tgtdir/oov
oov_wordlist=$oovdir/wordlist.txt
[ -d $oovdir ] || mkdir -p $oovdir
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): collect OOV word list"
  gzip -cd $tgtdir/data/text.gz | \
  source/egs/sg-en-i2r/print-oov-wlist.pl $tgtdata/lex.txt > $oov_wordlist
  echo "## LOG (step03, $0): done ('$oov_wordlist')"
fi
g2p_model=$tgtdir/model-3
oov_g2p_dict=$oovdir/wordlist-g2p-dict.txt
if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): apply g2p on oov word list"
  [ -f $g2p_model ] || { echo "## ERROR (step04, $0): $g2p_model expected"; exit 1; }
  g2p.py --model $g2p_model \
  --apply $oov_wordlist > $oov_g2p_dict 
  echo "## LOG (step04, $0): done ('$oov_g2p_dict')"
fi
tgtdict=$tgtdir/dict
if [ ! -z $step05 ]; then
  echo "## LOG (step05, $0): update the dict"
  [ -d $tgtdict ] || mkdir -p $tgtdict
  [ -f $oov_g2p_dict ] || \
  { echo "## ERROR (step05, $0): oov_g2p_dict ('$oov_g2p_dict') expected"; exit 1; }
  cat $oov_g2p_dict | \
  source/egs/ted-libri-en/merge-dict.pl $source_lexicon > $tgtdict/lexicon.txt
  cp $(dirname $source_lexicon)/{nonsilence_phones.txt,silence_phones.txt,optional_silence.txt,extra_questions.txt}  $tgtdict
  echo "## LOG (step05, $0): updated ('$tgtdict')"
fi
lang=$tgtdir/lang
if [ ! -z $step06 ]; then
  echo "## LOG (step06, $0): make lang ('$lang')"
  utils/lang/prepare_lang.sh $tgtdict "<unk>" $lang/tmp $lang 
  echo "## LOg (step06, $0): done ($lang)"
fi
