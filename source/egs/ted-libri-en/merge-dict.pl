#!/usr/bin/perl

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  my $usage = <<EOF;

  Example: cat sec-lex.txt | $0 main-lex.txt
  Notice: phone set of main-lex.txt would be super set of the phone set in the se-lex.txt.

EOF
    die $usage;
}
my ($mainDict) = @ARGV;

# begin sub
sub LoadDict {
  my ($dictFileName, $dict_vocab, $phone_vocab) = @_;
  open(F, "$dictFileName") or die "## ERROR ($0): cannot open dict file $dictFileName\n";
  while(<F>) {
    chomp;
    my @A = split(/\s+/);
    my $word = lc shift @A; my $pron = join(" ", @A);
    my $dictLine = sprintf("%s\t%s", $word, $pron);
    $$dict_vocab{$dictLine} ++;
    for(my $i = 0; $i < @A; $i ++) {
      my $phone = lc $A[$i];
      $$phone_vocab{$phone} ++;
    }
  }
  close F;
}
# end sub
my (%dict_vocab, %phone_vocab) = ((), ());
LoadDict($mainDict, \%dict_vocab, \%phone_vocab);
my $totalWordAdded = 0;
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  my ($word, $pron) = (lc shift @A, join(" ", @A));
  my $oov_phone = 0;
  for(my $i = 0; $i < scalar @A; $i ++) {
    my $phone = lc $A[$i];
    if(not exists $phone_vocab{$phone}) {
      print STDERR "## WARNING ($0): unidentified phone $phone ($_)\n";
      $oov_phone = 1;
      last;
    }
  } 
  if($oov_phone == 1) {
    next;
  }
  my $dictLine = sprintf("%s\t%s", $word, $pron);
  if(not exists $dict_vocab{$dictLine}) {
    $totalWordAdded ++;
    print STDERR "## LOG ($0): $dictLine added\n";
    $dict_vocab{$dictLine} ++;
  } 
}
print STDERR "## LOG ($0): stdin ended \n";
print STDERR "## LOG ($0): overall $totalWordAdded pronunciations are added\n";

foreach my $dictLine (keys%dict_vocab) {
  print "$dictLine\n";
}
