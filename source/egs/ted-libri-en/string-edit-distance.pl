#!/usr/bin/perl 
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);
use lib("/home/hhx502/perl5/lib/perl5");

use Text::Levenshtein qw(distance);

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\n Example: cat text | $0 <words.txt>  > map.txt\n\n";
}
my ($wordsFileName, $DistanceThresh) = @ARGV;

# begin sub
sub LoadDict {
  my ($dictFileName, $vocab) = @_;
  open(F, "$dictFileName") or die "## ERROR ($0): cannot open file $dictFileName\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    my $word = lc $1;
    $$vocab{$word} ++;
  }
  close F;
}
sub ComputeMinDistance {
  my ($vocab, $word, $substitute, $minDistance) = @_;
  foreach my $cand (%$vocab) {
    my $distance = distance($cand, $word);
    if($distance <= $$minDistance) {
      $$minDistance = $distance;
      $$substitute = $cand;
    }
  }
  return $minDistance;
}
# end sub
my %vocab = ();
LoadDict($wordsFileName, \%vocab);
print STDERR "## LOG ($0): stdin expected\n";
my %oovWordMap = ();
my %oov = ();
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  my $uttName = shift @A;
  for(my $i = 0; $i < @A; $i ++) {
    my $word = lc $A[$i];
    if(not exists $vocab{$word}) {
      if(not exists $oov{$word}) {
        $oov{$word} ++;
        my $substitute; my $minDistance = 100;
        ComputeMinDistance(\%vocab, $word, \$substitute, \$minDistance);
        print STDERR "## LOG ($0): map $word to $substitute\n";
        $oovWordMap{$word}{$substitute} = $minDistance;
      }
    }
  }
}
print STDERR "## LOG ($0): stdin ended \n";
foreach my $oovWord  (keys%oovWordMap) {
  foreach my $inWord (keys%{$oovWordMap{$oovWord}}) {
    print "$oovWord\t$inWord\t$oovWordMap{$oovWord}{$inWord}\n";
  }
}
