#!/usr/bin/perl -w
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\nExample: cat old-kaldi-text | $0 map.txt > new-kaldi-text\n\n";
}

my ($mapFile) = @ARGV;

# begin sub
sub LoadVocab {
  my ($fileName, $vocab) = @_;
  open(F, "$fileName") or die "## ERROR ($0): cannot open file $fileName\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/g or next;
    $$vocab{$1} = $2;
  }
  close F;
}
# end sub
my %vocab = ();
LoadVocab($mapFile, \%vocab);
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  my $utt = shift @A;
  for(my $i = 0; $i < scalar @A; $i++) {
    my $w = $A[$i];
    $w = $vocab{$w} if exists $vocab{$w};
    $utt .= " $w";
  }
  if(scalar @A >0) {
    print "$utt\n";
  }
}
print STDERR "## LOG ($0): stdin ended\n";
