#!/usr/bin/perl
use strict;
use warnings;
use open qw(:std :utf8);

print STDERR "$0: stdin expected\n";
my @A = ();
my $maxDegree = 0; my $minDegree = 100000000;
while(<STDIN>) {
  chomp;
  m/(\S+)/ or next;
  my $nDegree = $1;
  push @A, $nDegree;
  $maxDegree = $nDegree if $nDegree > $maxDegree;
  $minDegree = $nDegree if $nDegree < $minDegree;
}
print STDERR "$0: stdin ended\n";

my $nNode = scalar @A;
print STDERR "total nodes are $nNode\n";
my $numerator = 0;
for (my $i = 0; $i< $nNode; $i++) {
  my $nDegree = $A[$i];
  $numerator += $maxDegree - $nDegree;
}
my $fDivergence = $numerator / (($nNode -1)*($nNode -2));
my $sDivergence = sprintf("%.4f", $fDivergence);

print "Divergence = $sDivergence,minDegree=$minDegree, maxDegree=$maxDegree\n"
