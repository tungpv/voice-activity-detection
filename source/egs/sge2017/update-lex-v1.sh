#!/bin/bash 

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
kaldi_train_text_csl=
normal_train_text_csl=
kaldi_dev_text=
normal_dev_text=
dev_utterance_num=10000
# end options

function Usage {
cat<<EOF
 Usage: $0 --steps 1 <oov-word-list.txt> <g2p-model> <source-lexicon-dir> <target-lexicon-dir>
 [options]:
 [examples]:
 
 $0 --steps 1,2 --kaldi-train-text-csl sg-en-i2r/data/train/text:wsj/data/train_si284/text \
 --kaldi-dev-text /home2/hhx502/sge2017/data/dev-seame.28/16k/text \
 /home2/hhx502/sge2017/data/local/dict-sge-cmu-on-70h/oov.txt \
 /home2/hhx502/sge2017/data/local/g2p-data-30k-dict/model-3 \
 /home2/hhx502/sge2017/data/local/dict-sge-cmu-on-70h \
 /home2/hhx502/sge2017/data/local/ks-$( echo `date +%b-%d-%Y`| tr '[A-Z]' '[a-z]')

EOF
}

. parse_options.sh || exit 1

if [ $# -ne 4 ]; then
  Usage && exit 1;
fi

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

oovList=$1
g2pModel=$2
srcLex=$3
ksdir=$4

[ -d $ksdir ] || mkdir -p $ksdir
dict=$ksdir/dict
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): label OOV words with g2p model"
  tmpdir=$dict/g2p
  [ -d $tmpdir ] || mkdir -p $tmpdir
  cat $oovList | \
  perl -pe 'use utf8; use open qw(:std :utf8); if(! m/\p{Han}+/g){print "$_";}' \
  > $tmpdir/oov.txt
  g2p.py --model $g2pModel \
  --apply $tmpdir/oov.txt  > $tmpdir/g2p-dict.txt
  vocabSize=$(wc -l <$tmpdir/g2p-dict.txt)
  echo "## LOG ($0): number of OOVs is $vocabSize"
  cat $tmpdir/g2p-dict.txt $srcLex/lexicon.txt  | \
  perl -ane 'chomp; m/(\S+)\s+(.*)/g or next; $pron = join(" ", $2); print "$1\t$pron\n"; ' | \
  sort -u > $dict/lexicon.txt
  cp $srcLex/{extra_questions.txt,silence_phones.txt,nonsilence_phones.txt,optional_silence.txt} \
  $dict/

  utils/validate_dict_dir.pl $tgtdir
  echo "## LOG (step01, $0): done lexicon preparation"
fi 
lang=$ksdir/lang
if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): prepare lang"
  utils/prepare_lang.sh $dict "<unk>" $lang/tmp $lang
  echo "## LOG (step02, $0): lang preparation done"
fi
lmdir=$ksdir/lm
trap "echo '## LOG ($0): cleanup $lmdir ...'; rm $lmdir/overall.* 2>/dev/null" EXIT

function PrepareText {
  local myText=$1
  local myDir=$(dirname $myText)
  rm $myDir/{train-text.gz,dev-text.gz} 2>/dev/null
  gzip -cd $myText | utils/shuffle_list.pl | gzip -c >$myDir/overall.gz
  gzip -cd $myDir/overall.gz | \
  head -$dev_utterance_num | gzip -c > $myDir/dev-text.gz
  gzip -cd $myDir/overall.gz | \
  tail -n +$[dev_utterance_num+1] |  gzip -c > $myDir/train-text.gz || exit 1
  echo "## LOG (PrepareText, $0): done for text preparation"
}

if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): prepare text data for @ `date`"
  [ -z "$normal_train_text_csl" ] && \
  [ -z "$kaldi_train_text_csl" ] && \
  { echo "## LOG (step03, $0): no training text prepared for lm building"; exit 0; }
  [ -d $lmdir ] || mkdir $lmdir
  if [ ! -z "$normal_train_text_csl" ]; then
    for x in $(echo $normal_train_text_csl | tr '[:,]' ' '); do
      cat $x | perl -ane 'use utf8; use open qw(:std :utf8); print lc $_;' || exit 1
    done |  gzip -c > $lmdir/overall.1.gz

  fi
  if [ ! -z "$kaldi_train_text_csl" ]; then
    for x in $(echo $kaldi_train_text_csl | tr '[:,]' ' '); do
      cat $x | perl -ane 'use utf8; use open qw(:std :utf8); chomp; m/(\S+)\s+(.*)/g or next; print lc $2, "\n";' || exit 1
    done | gzip -c >> $lmdir/overall.1.gz
  fi
  if [ -z "$normal_dev_text" ] && [ -z "$kaldi_dev_text" ]; then
    PrepareText $lmdir/overall.1.gz 
    [ $? -eq 0 ] || { echo "## ERROR (step03, $0): PrepareText failed"; exit 1; }
    
  else
    cp $lmdir/overall.1.gz $lmdir/train-text.gz
    if [ ! -z $kaldi_dev_text ]; then
      cat $kaldi_dev_text | \
      perl -ane 'use utf8; use open qw(:std :utf8); 
       chomp; m/(\S+)\s+(.*)/g or next; print lc $2, "\n";' | \
      gzip -c > $lmdir/dev-text.gz
    fi
    if [ ! -z $normal_dev_text ]; then
      cat $normal_dev_text | \
      perl -ane 'use utf8; use open qw(:std :utf8);
        print lc $_;' >> $lmdir/dev-text.gz
    fi    
  fi
  if [ ! -e $lmdir/train-text.gz ] || \
     [ ! -e $lmdir/dev-text.gz ]; then
    echo "## ERROR ($0): failed to prepare train or dev text for lm building" && exit 1
  fi
  cat $dict/lexicon.txt | \
  perl -ane 'use utf8; use open qw(:std :utf8); m/(\S+)\s+(.*)/g or next;
    print lc $1, "\n";' | sort -u | gzip -c > $lmdir/vocab.gz
  echo "## LOG (step03, $0): done with text preparation ($lmdir) @ `date`"
fi

if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): build lm @ `date`"
  source/egs/kws2016/georgian/train-srilm-v2.sh --steps 1,2 \
  --lm-order-range "3 3" \
  --cutoff-csl "3,011,012" \
  $lmdir || exit 1
  echo "## LOG (step04, $0): done with lm ($lmdir) building @ `date`"
fi

if [ ! -z $step07 ]; then
  echo "## LOG (step07, $0): building grammar"
  [ -f $lmdir/lm.gz ] || \
  { echo "## ERROR (step07, $0): lm file is not ready !"; exit 1 ; }
  source/egs/fisher-english/arpa2G.sh $lmdir/lm.gz $lang $lang
  echo "## LOG (step07, $0): done ($lang)"
fi
