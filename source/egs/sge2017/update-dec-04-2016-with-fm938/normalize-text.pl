#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
use lib qw(source/egs/myperllib);
use LocalNumber;
use LocalDict;

my $one_by_one = 0;
my $word_transfer_dict = '';
GetOptions('one_by_one|one-by-one' => \$one_by_one,
           'word-transfer-dict|word_transfer_dict=s' => \$word_transfer_dict) or die;
# begin sub

sub RemoveDot {
  my ($s) = @_;
  $$s =~ s/(\S+)\.$/$1/g;
  $$s =~ s/(\S+)\.(\s+)/${1}${2}/g;
  $$s =~ s/^\.\s+//g;
  $$s =~ s/(\s+)\.$//g;
  $$s =~ s/(\s)+\.(\s+)/$1/g;
}
sub RemoveComma {
  my ($s) = @_;
  $$s =~ s/(\S+)\,$/$1/g;
  $$s =~ s/(\S+)\,(\s+)/${1}${2}/g;
}
sub RemoveQuestion {
  my ($s) = @_;
  $$s =~ s/[\?!]//g;
}
# end sub
if($word_transfer_dict ne '') {
  InitTransferDict($word_transfer_dict);
}
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my $text = lc $2;
  RemoveDot(\$text);
  RemoveComma(\$text);
  RemoveQuestion(\$text);
  ConvertTimeNumber(\$text);
  ConvertPercentNumber(\$text);
  ConvertOrdinalNumber(\$text);
  ConvertFractionalNumber(\$text);
  # ReadArabicNumberInEnglish(\$text, $one_by_one);
  ConvertDollarNumber(\$text);
  # print STDERR "text=$text\n";
  ConvertFourBitYear(\$text, 1600, 2100);
 
  ConvertTwoBitYear(\$text);
  DecomposeConcatenatedWord(\$text);
  ConvertNumericData(\$text);
  ConvertAlphaNumericString(\$text);
  if($word_transfer_dict ne '') {
    TransferWord(\$text);
  }
  ConvertDecimalNumber(\$text);
  ConvertURLAddress(\$text);
  print $1 . ' ' . $text . "\n";
}
print STDERR "## LOG ($0): stdin ended\n";
