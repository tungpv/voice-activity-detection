#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use lib qw(source/egs/myperllib);
use LocalDict;
use Getopt::Long;
my $label_transfer_dict = '';
my $dict_for_oov_checking = '';
GetOptions('label-transfer-dict=s' => \$label_transfer_dict,
           'dict-for-oov-checking=s' => \$dict_for_oov_checking) or die;
my $numArgs = scalar @ARGV;
if($numArgs != 2) {
  die "\nExample: cat transcript.flist | $0 --label-transfer-dict=label-transfer-dict.txt --dict-for-oov-checking=dict-for-oov-checking.txt wavlist.txt dir\n\n";
}
my ($wavScp, $tgtdir) = @ARGV;

# begin sub
sub LoadWavScp {
  my ($sFile, $vocab) = @_;
  open(F, "$sFile") or die "## ERROR (LoadWavScp, ", __LINE__, "): $sFile cannot open\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    my ($label, $rspecifier) = ($1, $2);
    $label =~ s/[\-_]mp3$//;
    $$vocab{$label} = $rspecifier;
  }
  close F;
}
sub RemoveBoundaryMark {
  my ($s) = @_;
  $$s =~ s/<unk>/TUNKNOWNWORDT/g;
  $$s =~ s/<[^<]+>/ <v-noise> /g;
  $$s =~ s/TUNKNOWNWORDT/ <unk> /g;
}
sub RemoveVerbalMark {
  my ($s) = @_;
  $$s =~ s/\([^\\(]+\)/ <v-noise> /g;
}
sub RemoveBracket {
  my ($s) = @_;
  $$s =~ s/[\[\]]//g;
}
sub RemoveBottomDash {
  my ($s) = @_;
  $$s =~ s/_/ /g;
}
sub DecomposeComplexWord {
  my ($s) = @_;
  $$s =~ s/<v-noise>/VERBALNOISE/g;
  $$s =~ s/\-/ /g;
  $$s =~ s/#/ /g;
  $$s =~ s/VERBALNOISE/<v-noise>/g;
}
sub DumpOovWord {
  my ($vocab, $sFile) = @_;
  open(F, "|sort -k1 >$sFile") or die "## ERROR(DumpOovWord, ", __LINE__, "): cannot open file $sFile\n";
  foreach my $label (keys%$vocab) {
    my $href = $$vocab{$label};
    print F $label, ' ', join(' ', (keys%$href)), "\n";
  }
  close F;
}
# end sub
my %vocab =();
LoadWavScp($wavScp, \%vocab);
if($label_transfer_dict ne '') {
  InitTransferDict($label_transfer_dict);
}
my %oovDict = ();
if ($dict_for_oov_checking ne '') {
  InitDictSimple($dict_for_oov_checking);
}

print STDERR "## LOG ($0): stdin expected\n";
my $unit = 100;
open(W, ">$tgtdir/wav.scp") or die;
open(T, ">$tgtdir/text") or die;
open(S, ">$tgtdir/segments") or die;
open(U, ">$tgtdir/utt2spk") or die;
while(<STDIN>) {
  chomp;
  my $sFile = $_;
  open(F, "$sFile") or die "## ERROR ($0): cannot open $sFile\n";
  my $label = lc $sFile; $label =~ s/.*\///g; $label =~ s/\.txt//g; $label =~ s/([\-_]mp3)?[\-_]completed//;
  if($label_transfer_dict ne '') {
    TransferWord(\$label);
  }
  if(not exists $vocab{$label} ) {
    print STDERR "## WARNINIG ($0): no file for label $label\n";
  }
  print W "$label $vocab{$label}\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(\S+)\s+(\S+)\s+(.*)/ or next;
    my ($start, $end, $spker, $text) = ($1, $2, $3, $4);
    my $timeStr = sprintf("%07d-%05d", $start*$unit, $end*$unit);
    my $updateSpeaker = sprintf("%s-%s", $spker, $label);
    my $uttName = sprintf("%s-%s", $updateSpeaker, $timeStr);
    $text =~ s/\s+$//g;
    if($text =~ /^(\([^\(]+\)\s+|<[^<]+>\s+)+(\<[^\<]+\>)$/ or
       $text =~ /^\([^\(]+\)$/ or $text =~ /^(<[^\<]+>|\[[^\[]+\])$/ or
       $text =~ /^\([^\(]+\)(\s+\([^\(]+\))?\s+\([^\(]+\)$/ or
       $text eq '') {
      print STDERR "## WARNING ($0): remove $text \n";
      next;
    }

    RemoveBoundaryMark(\$text);
    RemoveVerbalMark(\$text);
    RemoveBracket(\$text);
    RemoveBottomDash(\$text);
    DecomposeComplexWord(\$text);
    if($dict_for_oov_checking ne '') {
      my @A = ();
      GetOovWord(\$text, \@A);
      for(my $i = 0; $i < @A; $i ++) {
        my $word = $A[$i];
        $oovDict{$label}{$word} ++;
      }
    }
    print T "$uttName $text\n";
    print S "$uttName $label $start $end\n";
    print U "$uttName $updateSpeaker\n";
    # print STDERR "$uttName $updateSpeaker\n";
  }
  close F;
}

close W;
close T;
close S;
close U;

print STDERR "## LOG ($0): stdin ended\n";

if ($dict_for_oov_checking ne '') {
  my $oovFile = "$tgtdir/oov-text";
  DumpOovWord(\%oovDict, $oovFile);
}
