#!/bin/bash

. path.sh
. cmd.sh 

# begin options
steps=
# end options
function Usage {
 cat<<EOF
  
  [Examples]: $0 --steps 2 ../sge/name-entity/dict/singapore/mrt/sge-phone-mapping.txt \
  ../sge/name-entity/dict/singapore/mrt/MRT-phonetics-edit.txt  ../sge/name-entity/dict/singapore/mrt

EOF
}

. parse_options.sh || exit 1


steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

map_file=$1
human_lexicon=$2
tgtdir=$3

if [ ! -z $step01 ]; then
  source/egs/sge2017/convert-human-lexicon-to-asr-lexicon-with-phone-mapping-list.pl \
  $map_file $human_lexicon $tgtdir
fi
tgtdir=../sge/name-entity/dict/singapore/road
if [ ! -z $step02 ]; then
  cat $tgtdir/road-phonetics-utf8.txt | tail -n +2 | \
  cut -f1,4 > $tgtdir/road-name-lexicon-human.txt
  [ -f $tgtdir/road-name-lexicon-human-edit.txt ] ||\
  cp $tgtdir/road-name-lexicon-human.txt  $tgtdir/road-name-lexicon-human-edit.txt
fi

if [ ! -z $step03 ]; then
  
fi
