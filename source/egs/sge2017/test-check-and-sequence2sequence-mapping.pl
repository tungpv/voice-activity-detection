#!/usr/bin/perl -w
use strict;
use utf8;
use lib qw(source/egs/myperllib);
use LocalDict;

my ($mapFile) = @ARGV;
# begin sub
sub InsertPair {
  my ($key, $value, $vocab) = @_;
  if(not exists $$vocab{$key}) {
    my @A = ();
    $$vocab{$key} = [@A];
    my $refA = $$vocab{$key};
    push @$refA, $value;
    return;
  }
  my $refA = $$vocab{$key};
  push @$refA, $value;
}
sub LoadVocab {
  my ($mapFile, $vocab) = @_;
  open(F, "$mapFile") or die;
  while(<F>) {
    chomp;
    my @A =split(/\s+/);
    InsertPair($A[0], $A[1], $vocab);
  }
  close F;
}
# end sub

my %vocab = ();
LoadVocab($mapFile, \%vocab);

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  my @B = ();
  my $oovPhone = '';
  if(CheckAndSequence2SequenceMapping(\%vocab, \@A, \@B, \$oovPhone) == 0) {
    die "## ERROR ($0): oov phone $oovPhone seen\n";
  }
  for(my $i = 0; $i < @B; $i ++) {
    my $array = $B[$i];
    my $mySequence = join(" ", @$array);
    print "$mySequence\n";
  }
}
print STDERR "## LOG ($0): stdin ended\n";
