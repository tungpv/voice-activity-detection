#!/usr/bin/perl -w
use strict;
use utf8;
use open qw (:std :utf8);

my $numArgs = scalar @ARGV;

if($numArgs != 3) {
  die "\nUsage: cat text | $0 source_lexicon_dir cmudict-lexicon.txt tgtLexiconDir \n",
  "Example: cat /home2/hhx502/sge2017/data/train-merge70h/8k/text | $0 /home2/hhx502/sg-en-i2r/cmudict/data-30k/local/dict \"cat wsj/prepare2/dict/lexicon.txt |source/egs/sge2017/normalize-cmu-dict-phone-set.pl |\" /home2/hhx502/sge2017/data/local/dict-sge-cmu-on-70h\n";
}

my ($sLexDir, $cmuDict, $tgtLexDir) = @ARGV;

# begin sub
sub LoadVocab {
  my ($vocab, $dictFile) = @_;
  open(F, "$dictFile") or die "## ERROR (LoadVocab, $0): file $dictFile cannot open\n";
  my %uniqueVocab = ();
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/g or next;
    my $word = lc $1;
    my @A = split(/\s+/, $2);
    my $pron = "$word\t";
    for(my $i = 0; $i < scalar @A; $i ++) {
      my $phone = $A[$i];
      if($i==0) {
        $pron .= "$phone";
      } else {
        $pron .= " $phone";
      }
    }
    next if exists $uniqueVocab{$pron};
    $uniqueVocab{$pron} ++;
    my $pRef;
    if (not exists $$vocab{$word}) {
      $$vocab{$word} = [my @B];
      $pRef = $$vocab{$word};
    } else {
      $pRef = $$vocab{$word};
    }
    push @$pRef, \$pron;
  }
  close F;
}
sub LoadPhoneVocab{
  my ($vocab, $nonsilenceFile) = @_;
  open(F, "$nonsilenceFile") or die "## ERROR (LoadPhoneVocab, $0): file $nonsilenceFile cannot open\n";
  while(<F>) {
    chomp;
    m/(\S+)/g or next;
    $$vocab{$1} ++;
  }
  close F;
}
sub AddWordToVocab {
  my ($word, $cmuVocab, $sourceVocab) = @_;
  die "## ERROR (AddWordToVocab, $0): word $word is unknown\n" if not exists $$cmuVocab{$word};
  die "## ERROR (AddWordToVocab, $0): word $word already exists" if exists $$sourceVocab{$word};
  $$sourceVocab{$word} = $$cmuVocab{$word};
}
sub UpdatePhoneSet {
  my ($wordPronArray, $vocab, $phoneSetUpdated) = @_;
  for(my $i = 0; $i < scalar @$wordPronArray; $i ++) {
    my $s = $$wordPronArray[$i];
    my @A = split(/\s+/, $$s);
    for($i = 1; $i < @A; $i ++) {
      my $phone = $A[$i];
      next if exists $$vocab{$phone};
      $$phoneSetUpdated ++;
      $$vocab{$phone} ++;
    }
  }
}
sub PrintOutDict {
  my ($vocab, $outputFile) = @_;
  open(F, "$outputFile") or die "## ERROR (PrintOutDict, $0): cannot open file $outputFile to write\n";
  foreach my $word (keys %$vocab) {
    my $pronArray = $$vocab{$word};
    for(my $i = 0; $i < scalar @$pronArray; $i ++) {
      my $pron = $$pronArray[$i];
      print F "$$pron\n";
    }
  }
  close F;
}
sub PrintOutPhoneSet {
  my ($vocab, $outputFile) = @_;
  open(F, "$outputFile") or die "## ERROR (PrintOutPhoneSet, $0): cannot open file $outputFile to write\n";
  foreach my $phone (keys %$vocab) {
    print F "$phone\n";
  }
  close F;
}
# end sub

my %srcDictVocab = ();
LoadVocab(\%srcDictVocab, "$sLexDir/lexicon.txt");
my %srcPhoneVocab = ();
LoadPhoneVocab(\%srcPhoneVocab, "$sLexDir/nonsilence_phones.txt");
my %cmuDictVocab = ();
LoadVocab(\%cmuDictVocab, "$cmuDict");

print STDERR "## LOG ($0): stdin expected\n";

`[ -d $tgtLexDir ] || mkdir -p $tgtLexDir`;

open(OF, "|sort -u >$tgtLexDir/oov.txt") or die "## ERROR ($0): cannot open $tgtLexDir/oov.txt to write\n";
my $totalWordNum = 0;
my $totalOovNum = 0;
my $uniqueOovNum = 0;
my $recoveredOovNum = 0;
my $recoveredUniqueNum = 0;
my %uniqueOovVocab = ();
my %uniqueVocab = ();
my $phoneSetUpdated = 0;
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($lab, $utt) =  ($1, $2);
  my @A = split(/\s+/, $utt);  $totalWordNum += scalar @A;
  for(my $i = 0; $i < scalar @A; $i ++) {
    my $word = lc $A[$i];
    if(not exists $srcDictVocab{$word}) {
      $totalOovNum ++;
      if(exists $cmuDictVocab{$word}) {
        $recoveredOovNum ++;
        if (not exists $uniqueVocab{$word}) {
          AddWordToVocab($word, \%cmuDictVocab, \%srcDictVocab);
          UpdatePhoneSet($cmuDictVocab{$word}, \%srcPhoneVocab, \$phoneSetUpdated);
          $recoveredUniqueNum ++;
          $uniqueVocab{$word} ++;
        }
      } else {
        if(not exists $uniqueOovVocab{$word}) {
          $uniqueOovVocab{$word} ++;
          $uniqueOovNum ++;
          print OF "$word\n";
        }
      }
    }
  }
}
print STDERR "## LOG ($0): stdin ended\n";
if ($phoneSetUpdated > 0) {
  print STDERR "## LOG ($0): phone set updated\n";
}
close OF;
my $oovRate = 0;
if($totalWordNum > 0) {
  $oovRate = sprintf("%.2f", $totalOovNum / $totalWordNum);
}

print STDERR "## LOG ($0): total word: $totalWordNum, OOV ($oovRate): $totalOovNum, unique OOV: $uniqueOovNum, recovered OOV: $recoveredOovNum, recoverd unique OOV: $recoveredUniqueNum\n";

PrintOutPhoneSet(\%srcPhoneVocab, "|sort >$tgtLexDir/nonsilence_phones.txt");
PrintOutDict(\%srcDictVocab, "| sort -u > $tgtLexDir/lexicon.txt");
`cp $sLexDir/silence_phones.txt $tgtLexDir/`;
`cp $sLexDir/optional_silence.txt $tgtLexDir/`;
`cp $sLexDir/extra_questions.txt $tgtLexDir/`;

