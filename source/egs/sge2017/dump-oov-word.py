#!/usr/bin/env python

# -*- coding: utf-8 -*-
# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu

import argparse
import sys
import os
import re
import numpy as np
import logging

sys.path.insert(0, 'steps')
import libs.common as common_lib
logger = logging.getLogger('libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting convert-human-lexicon-to-asr-lexicon-with-phone-mapping-list')

def get_args():
    parser = argparse.ArgumentParser(description='Arguments parser')
    parser.add_argument('--debug', dest='debug', action='store_true')
    parser.add_argument('--normalize', dest='normalize', action='store_true')
    parser.add_argument('--dict', dest='dictfile', default='', help='dictionary file')
    parser.add_argument('--text', type=str, dest='textfile',  help='text to be in check for oov with the given dict')
    parser.add_argument('--tgtdir', type=str, dest='tgtdir', help='target dir to save output')
    return parser.parse_args()
def CheckLogger(args):
    if args.debug:
        logger.setLevel(logging.DEBUG)
        handler.setLevel(logging.DEBUG)
    logger.debug('logger now is at DEBUG level')
def InsertWordPron(dictLex, word, pron):
    if word in dictLex:
        dictPron = dictLex[word]
        if pron in dictPron:
            dictPron[pron] += 1
        else:
            dictPron[pron] = int(0)
    else:
        dictLex[word] = dict()
        dictPron = dictLex[word]
        dictPron[pron] = int(0)
        # logger.debug('dictLex={0}\n'.format(dictLex))
def DumpDict(dictLex, listLex):
    sortedDictLex = sorted(dictLex.items())
    for word, dictPron in sortedDictLex:
        sortedDictPron = sorted(dictPron.items())
        for pron, count in sortedDictPron:
            wordPron = "{0}\t{1}".format(word, pron)
            # logger.info('wordPron={0}'.format(wordPron))
            listLex.append(wordPron)
        
def LoadDict(args, dictLex):
    if not args.dictfile:
        logger.info('dict file not specified')
        return
    with open(args.dictfile, 'r') as inputFile:
        for line in inputFile:
            line = line.strip()
            if line:
                m = re.search(r'(^\S+)(.*)$', line)
                # logger.debug('word={0}, pron={1}\n'.format(m.group(1), m.group(2)))
                if m:
                    word = m.group(1).lower()
                    pron = m.group(2)
                    pron = ' '.join(pron.strip().split())
                    pron = pron.strip()
                    InsertWordPron(dictLex, word, pron)
        if args.debug:
            logger.info('dump dict')
            listLex = list()
            DumpDict(dictLex, listLex)
            for wordPron in listLex:
                print("{0}".format(wordPron))
def _NormalizeWord(word):
    word = re.sub(r'\.$', '', word)
    word = re.sub(r'[\"]', '', word)
    return word
def InsertOovDict(word, dictOov):
    if word in dictOov:
        dictOov[word] += 1
    else:
        dictOov[word] = int(1)

def ReadTextAndDumpOov(args, dictSupervisor, dictOov):
    if not args.textfile:
        return
    with open(args.textfile, 'r') as inputFile:
        for line in inputFile:
            line = line.strip().lower()
            if args.normalize:
                line =re.sub(r'[,]', ' ', line)
            for word in line.split():
                if args.normalize:
                   word = _NormalizeWord(word)
                if word not in dictSupervisor:
                  InsertOovDict(word, dictOov)
def main():
    args = get_args()
    CheckLogger(args)
    dictSupervisor = dict()
    LoadDict(args, dictSupervisor)
    dictOov = dict()
    ReadTextAndDumpOov(args, dictSupervisor, dictOov)
    import operator 
    sortedListOov = sorted(dictOov.items(), key=operator.itemgetter(1), reverse=True)
    for word in [word for word, _ in sortedListOov ]:
        print('{0}'.format(word))
if __name__ == "__main__":
    main()
