#!/bin/bash

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh

# begin options
steps=
data_name=train_merge
cmd=slurm.pl
nj=40
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF

 [examples]:
 
 $0 --steps 2 /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/train-merge/mfcc-pitch \
 /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/lang_test \
 /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/exp/tri3a \
 /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/merge-dict \
 /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/exp/debug-lexicon

EOF
}

if [ $# -ne 5 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
srcdir=$3
srcdict=$4
tgtdir=$5


if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
[ -d $tgtdir ] || mkdir -p $tgtdir
alidir=$srcdir/ali_train
if [ ! -z $step01 ]; then
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj $data $lang $srcdir \
  $alidir || exit 1
fi
phone_lang=$tgtdir/lang
if [ ! -z $step02 ]; then
  utils/make_phone_bigram_lang.sh $lang $alidir $phone_lang
fi
graphdir=$srcdir/graph_phone_bg
if [ ! -z $step03 ]; then
  utils/mkgraph.sh $phone_lang $srcdir $graphdir
  echo "## LOG (step03, $0): done & check '$graphdir'"
fi
decode_dir=$srcdir/decode-${data_name}-phone_bg
if [ ! -z $step04 ]; then
  steps/decode_si.sh --cmd "$cmd" --nj $nj --transform-dir $alidir \
    --acwt 0.25 --beam 25.0 --lattice-beam 5.0 --max-active 2500 \
    $graphdir $data $decode_dir
  echo "## LOG (step04, $0): done & check '$decode_dir'"
fi

if [ ! -z $step05 ]; then
  steps/get_train_ctm.sh --cmd "$cmd" \
  $data $lang $alidir || exit 1
  echo "## LOG (step05, $0): done & check '$alidir'"
fi

if [ ! -z $step06 ]; then
  steps/get_ctm.sh --min-lmwt 3 --max-lmwt 8  --cmd "$cmd" \
     $data $phone_lang $decode_dir || exit 1
  echo "## LOG (step06, $0): done & check '$decode_dir'"
fi

if [ ! -z $step07 ]; then
  cp $decode_dir/score_4/mfcc-pitch.ctm $tgtdir/phone.ctm
  gzip -cd $alidir/ctm.*.gz > $tgtdir/word.ctm

  grep -v '<eps>' $phone_lang/phones.txt | awk '{print $1, $1}' | \
    sed 's/_B$//' | sed 's/_I$//' | sed 's/_E$//' | sed 's/_S$//' >$tgtdir/phone_map.txt

  silphone=$(cat $phone_lang/phones/optional_silence.txt)
  cat $tgtdir/phone.ctm | utils/apply_map.pl -f 5 $tgtdir/phone_map.txt | grep -v "$silphone\$" > $tgtdir/phone_cleaned.ctm

  export LC_ALL=C
  
  cat $tgtdir/word.ctm | awk '{printf("%s-%s %09d START %s\n", $1, $2, 100*$3, $5); printf("%s-%s %09d END %s\n", $1, $2, 100*($3+$4), $5);}' | \
     sort >$tgtdir/word_processed.ctm

  cat $tgtdir/phone_cleaned.ctm | awk '{printf("%s-%s %09d PHONE %s\n", $1, $2, 100*($3+(0.5*$4)), $5);}' | \
     sort >$tgtdir/phone_processed.ctm

  # merge-sort both ctm's
  sort -m $tgtdir/word_processed.ctm $tgtdir/phone_processed.ctm > $tgtdir/combined.ctm
  echo "## LOG (step07, $0): done & check '$tgtdir/word.ctm'" 
fi
if [ ! -z $step08 ]; then
  awk '{print $3, $4}' $tgtdir/combined.ctm | \
  perl -e ' while (<>) { chop; @A = split(" ", $_); ($a,$b) = @A;
  if ($a eq "START") { $cur_word = $b; @phones = (); }
  if ($a eq "END") { print $cur_word, " ", join(" ", @phones), "\n"; }
  if ($a eq "PHONE") { push @phones, $b; }} ' | sort | uniq -c | sort -nr > $tgtdir/prons.txt
fi
if [ ! -z $step09 ]; then
  cp $srcdict/lexicon.txt $tgtdir/
  awk '{count[$2] += $1;} END {for (w in count){print w, count[w];}}' \
  <$tgtdir/prons.txt >$tgtdir/counts.txt
  cat $tgtdir/prons.txt | \
  perl -e '
  print ";; <count-of-this-pron> <rank-of-this-pron> <frequency-of-this-pron> CORRECT|INCORRECT <word> <pron>\n";
     open(D, "<$ARGV[0]") || die "opening dict file $ARGV[0]";
     # create a hash of all reference pronuncations, and for each word, record
     # a list of the prons, separated by " | ".
     while (<D>) { 
        @A = split(" ", $_); $is_pron{join(" ",@A)} = 1; 
        $w = shift @A; 
        if (!defined $prons{$w}) { $prons{$w} = join(" ", @A); }
        else { $prons{$w} = $prons{$w} . " | " . join(" ", @A); }
     }
     open(C, "<$ARGV[1]") || die "opening counts file $ARGV[1];";
     while (<C>) { @A = split(" ", $_); $word_count{$A[0]} = $A[1]; }
     while (<STDIN>) { @A = split(" ", $_);
       $count = shift @A; $word = $A[0]; $freq = sprintf("%0.2f", $count / $word_count{$word});
       $rank = ++$wcount{$word}; # 1 if top observed pron of word, 2 if second...
       $str = (defined $is_pron{join(" ", @A)} ? "CORRECT" : "INCORRECT");
       shift @A;
       print "$count $rank $freq $str $word \"" . join(" ", @A) . "\", ref = \"$prons{$word}\"\n";
     } ' $tgtdir/lexicon.txt $tgtdir/counts.txt  >$tgtdir/pron_info.txt
   grep -v '^;;' $tgtdir/pron_info.txt | \
     awk '{ word=$5; count=$1; if (tot[word] == 0) { first_line[word] = $0; }
            corr[word] += ($4 == "CORRECT" ? count : 0); tot[word] += count; }
          END {for (w in tot) { printf("%s\t%s\t%s\t\t%s\n", tot[w], w, (corr[w]/tot[w]), first_line[w]); }} ' \
     | sort -k1 -nr | cat <( echo ';; <total-count-of-word> <word> <correct-proportion>      <first-corresponding-line-in-pron_info.txt>') - \
      > $tgtdir/word_info.txt 
  echo "## LOG (step09, $0): done & check '$tgtdir/word_info.txt'"
fi
if [ ! -z $step10 ]; then
  echo "$0: some of the more interesting stuff in $dir/pron_info.txt follows."
  echo "# grep -w INCORRECT $tgtdir/pron_info.txt  | grep -w 1 | head -n 20"

  grep -w INCORRECT $tgtdir/pron_info.txt  | grep -w 1 | head -n 20

  echo "$0: here are some other interesting things.."
  echo "# grep -w INCORRECT $tgtdir/pron_info.txt  | grep -w 1 | awk '\$3 > 0.4 && \$1 > 10' | head -n 20"
  grep -w INCORRECT $tgtdir/pron_info.txt  | grep -w 1 | awk '$3 > 0.4 && $1 > 10' | head -n 20

  echo "$0: here are some high-frequency words whose reference pronunciations rarely show up."
  echo "# awk '\$3 < 0.1' $tgtdir/word_info.txt  | head -n 20"
  awk '$3 < 0.1 || $1 == ";;"' $tgtdir/word_info.txt  | head -n 20

fi
oov=`cat $lang/oov.int` || exit 1;
nj=`cat $alidir/num_jobs` || exit 1;
dict=$tgtdir/dict
if [ ! -z $step11 ]; then
  [ -d $dict ] || mkdir -p $dict   
    ( ( for n in `seq $nj`; do gunzip -c $alidir/ali.$n.gz; done ) | \
    linear-to-nbest ark:- "ark:utils/sym2int.pl --map-oov $oov -f 2- $lang/words.txt $data/text |" '' '' ark:- | \
    lattice-align-words $lang/phones/word_boundary.int $alidir/final.mdl ark:- ark:- | \
    lattice-to-phone-lattice --replace-words=false $alidir/final.mdl ark:- ark,t:- | \
    awk '{ if (NF == 4) { word_phones = sprintf("%s %s", $3, $4); count[word_phones]++; } } 
        END { for(key in count) { print count[key], key; } }' | \
          sed s:0,0,:: | awk '{print $2, $1, $3;}' | sed 's/_/ /g' | \
          utils/int2sym.pl -f 3- $lang/phones.txt  | \
          sed -E 's/_I( |$)/ /g' |  sed -E 's/_E( |$)/ /g' | sed -E 's/_B( |$)/ /g' | sed -E 's/_S( |$)/ /g' | \
          utils/int2sym.pl -f 1 $lang/words.txt > $dict/lexicon_counts.txt
  )
  echo "## LOG (step11, $0): done & check '$dict'"
fi
alidir=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938/exp/tri4a/ali_train
if [ ! -z $step12 ]; then
  steps/get_prons.sh $data $lang $alidir || exit 1
  echo "## LOG (step12, $0): done & check '$alidir'"
fi
update_dir=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data
dict=$update_dir/dict_from_ali_redo
lang=$update_dir/lang_from_ali_redo
if [ ! -z $step13 ]; then
  [ -d $dict ] || mkdir -p $dict
  cat $alidir/pron_counts_nowb.txt | grep -v '<' | \
  perl -ane 'chomp; m/(\S+)\s+(\S+)\s+(.*)/ or next; print "$2\t$3\n";'| \
  source/egs/sg-en-i2r/update-lex-lm/print-phone-set.pl | sort -u > $dict/nonsilence_phones.txt
  cat $srcdir/nonsilence_phones.txt | sort -u > $dict/source_nonsilence_phones.txt 
  if [ $(diff -q $dict $dict/source_nonsilence_phones.txt) -ne 0 ];  then
     echo "## ERROR (step13, $0): phone set is changed" && exit 1
  fi
  cat $alidir/pron_counts_nowb.txt | grep -v '<' | \
  perl -ane 'chomp; m/(\S+)\s+(\S+)\s+(.*)/ or next; print "$2\t$3\n";' | sort -u > $dict/lexicon-from-ali.txt
  cat $srcdict/lexicon.txt  | \
  source/egs/sge2017/update-lexicon-by-AM-24-dec-2016/merge-lexicon-by-word.pl $dict/lexicon-from-ali.txt | \
  sort -u | cat - <(grep '<' $srcdict/lexicon.txt) > $dict/lexicon.txt
  cp $srcdict/{nonsilence_phones.txt,silence_phones.txt,optional_silence.txt,extra_questions.txt} $dict/
  utils/validate_dict_dir.pl $dict
  utils/prepare_lang.sh $dict "<unk>" $lang/tmp  $lang
  echo "## (step13, $0): done & check '$dict' & '$lang'" 
fi
lmdir=$update_dir/lm_with_dict_from_ali
srclmdir=$update_dir/lm
if [ ! -z $step14 ]; then
  [ -d $lmdir ] || mkdir -p $lmdir
  cp $srclmdir/train-text.gz $lmdir/  || exit 1
  cp $srclmdir/dev-text.gz $lmdir/ || exit 1
  
  cat $dict/lexicon.txt | \
  source/egs/sge2017/local/build-lm/print-word-list.pl | gzip -c > $lmdir/vocab.gz
  
  source/egs/sge2017/local/build-lm/train-srilm-v2.sh --steps 1,2 --lm-order-range "3 3" \
  --cutoff-csl "3,011,012"  $lmdir || exit 1
  echo "## LOG (step14, $0): done & check '$lmdir'"
fi
graphdir=$update_dir/../exp/tri4a/graph-with-dict-from-ali
if [ ! -z $step15 ]; then
  source/egs/fisher-english/arpa2G.sh $lmdir/lm.gz $lang $lang
  utils/mkgraph.sh $lang $(dirname $graphdir) $graphdir
  echo "## LOG (step15, $0): done & check '$lang/G.fst'"
fi
dev_data=/home2/hhx502/sge2017/data/dev-seame.28/16k/mfcc-pitch
if [ ! -z $step16 ]; then
  steps/decode_fmllr.sh --cmd "$cmd -c 2" --nj 30 \
  --scoring-opts '--min-lmwt 8 --max-lmwt 25' \
  $graphdir $dev_data  $(dirname $graphdir)/decode-dev-seame.28-with-dict-ali || exit 1
fi
