#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

while(<STDIN>) {
  chomp;
  m:(^\S+)\s*(.*): or next;
  print("$1\t$1\n");
}
