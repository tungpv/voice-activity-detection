#!/usr/bin/perl -w 
use strict;
use utf8;
use open qw(:std :utf8);
my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  die "\nExample: cat text | $0  word-transfer-dict.txt new-text\n\n";
}
my ($dictFile, $textFile) = @ARGV;

open(D, "|sort -u > $dictFile") or die;
open(T, "|sort -u | gzip -c > $textFile") or die;

while(<STDIN>) {
  chomp;
  m:^$:g and  next;
  my @A = split(/\s+/);
  for(my $i = 0; $i < scalar @A; $i ++) {
    my $word = $A[$i];
    print D "$word\t$word\n";
  }
  my $utt = join(" ", @A);
  print T "$utt\n";
}
close D;
close T;
