#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
use lib qw(source/egs/myperllib);
use LocalNumber;
use LocalDict;

my $one_by_one = 0;
my $word_transfer_dict = '';
GetOptions('one_by_one|one-by-one' => \$one_by_one,
           'word-transfer-dict|word_transfer_dict=s' => \$word_transfer_dict) or die;
# begin sub
sub Lowercase {
  my ($s) = @_;
  $$s = lc $$s;
}
sub Cleaner {
  my ($s) = @_;
  $$s =~ s:[\)\(\”\“\.]: :g;
  $$s =~ s:[《…_》]: :g;
  $$s =~ s:[’]:\':g;
  $$s =~ s:[。，：￥%……&×（）——”：？．！［］‘’]: :g;
  $$s =~ s:[\"\;\,!\?]: :g;
  $$s =~ s:[•、]: :g;
}
# end sub

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  Lowercase(\$_);
  Cleaner(\$_);
  print "$_\n";
}
print STDERR "## LOG ($0): stdin ended\n";
