#!/usr/bin/perl -w 
use strict;
use utf8;
use open qw(:std :utf8);
my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  die "\nExample: cat lexicon.txt | $0 phones.txt > normalized-lexicon.txt\n\n";
}
my ($phoneSetFile) = @ARGV;
open(F, "$phoneSetFile") or die;
my %phoneVocab = ();
while(<F>) {
  chomp;
  m:(^\S+): or next;
  $phoneVocab{$1} ++;
}
close F;
sub HasOOVPhone {
  my ($array, $vocab) = @_;
  for(my $i = 0; $i < scalar @$array; $i ++) {
    if (not exists $$vocab{ uc $$array[$i]}) {
       print STDERR "oov phone $$array[$i]\n";
       next;
    }
  }
  return 0;
}
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m:(^\S+)\s+(.*):g  or next;
  my @A = split(/\s+/);
  my $word = shift @A;
  if (HasOOVPhone(\@A, \%phoneVocab)) {
    print STDERR "## WARNING ($0): word $_ has oov phone\n";
    next;
  }
  my $pron = join(" ", @A);
  $pron = uc $pron;
  $word = lc $word;
  print "$word\t$pron\n";
}
print STDERR "## LOG ($0): stdin ended\n";
