#!/usr/bin/perl -w

use utf8;
use strict;
use open qw(:std :utf8);
use lib qw(source/egs/myperllib);
use LocalDict;
my $numArgs = scalar @ARGV;

if ($numArgs != 3) {
  die "\nExample: $0 ipa-to-asr-phone-mapping-list.txt human-lexicon.txt  tgtdir\n\n";
}

my ($phoneMappingList, $humanLexicon, $tgtdir) = @ARGV;

# begin sub
sub InsertPair {
  my ($key, $value, $vocab) = @_;
  $key =~ s: ::g;
  $value =~ s: ::g;
  # print STDERR "## DEBUG (", __LINE__, "): key=$key, value=$value\n";
  if(not exists $$vocab{$key}) {
    my @A = ();
    $$vocab{$key} = [@A];
    my $refA = $$vocab{$key};
    push @$refA, $value;
    return;
  }
  my $refA = $$vocab{$key};
  push @$refA, $value;
}
sub LoadPhoneMapVocab {
  my ($mapFile, $vocab) = @_;
  open(F, "$mapFile") or die;
  while(<F>) {
    chomp;
    my @A = split(/\t/);
    die if scalar @A != 2;
    my $tgtPhone = shift @A;
    my @B = split(/\//, $A[0]);
    for(my $i = 0; $i < scalar @B; $i ++) {
      my $srcPhone = $B[$i];
      next if $srcPhone eq '';
      InsertPair($srcPhone, $tgtPhone, $vocab);
    }
  }
  close F;
}
sub CheckWordNum {
  my ($word, $pron, $array) = @_;
  my @A = split(/\s/, $word);
  my $wordNum = scalar @A;
  my @B = split(/\#/, $pron);
  my $pronNum = scalar @B;
  # print STDERR "word=$word, wordNum=$wordNum, pron=$pron, pronNum=$pronNum\n";
  if($wordNum == $pronNum) {
    for(my $i = 0; $i < @A; $i ++) {
      my $word = $A[$i];
      my $phones = $B[$i];
      $phones =~ s:[\.\+]: :g;
      my $pron = sprintf("%s\t%s", $word, $phones);
      push @$array, $pron;
    }
  }
  return 1 if $wordNum == $pronNum;
  return 0;
}
sub CheckHumanLexicon {
  my ($humanLexiconFile, $tgtdir, $array) = @_;
  open (F, "$humanLexiconFile") or die;
  open (REPORT, ">$tgtdir/error-labeling.txt") or die;
  my $previous_word = "";
  while(<F>) {
    chomp;
    my @A = split(/\t/);
    if($A[0] ne '') {
      $previous_word = $A[0];
    } else {
      die if $previous_word eq '';
      $A[0] = $previous_word;
    }
    my($word, $pron) = ($A[0], $A[1]);
    if(CheckWordNum($word, $pron, $array) == 0) {
      print REPORT $word, "\t", $pron, "\n";
      next;
    }
  }
  close F;
  close REPORT;
  print STDERR "## LOG (", $0, __LINE__,"): check $tgtdir/error-labeling.txt\n";
}
# end sub
`[ -d $tgtdir ] || mkdir -p $tgtdir`;
if ( ! -d  $tgtdir) {
  die "## ERROR ($0): failed to make dir '$tgtdir'\n";
}
my %phoneMapVocab = ();
LoadPhoneMapVocab($phoneMappingList, \%phoneMapVocab);
my @Array = ();
CheckHumanLexicon($humanLexicon, $tgtdir, \@Array);
open (ASRLEX, "|sort -u >$tgtdir/lexicon.txt") or die;
for(my $i = 0; $i < scalar @Array; $i ++) {
  my $dictEntry = $Array[$i];
  my @A = split(/\t/, $dictEntry);
  my $word = shift @A;
  my $phones = $A[0];
  @A = split(/\s+/, $phones);
  my @B = (); my $oovPhone;
  if(CheckAndSequence2SequenceMapping(\%phoneMapVocab, \@A, \@B, \$oovPhone) == 0) {
    print STDERR "## ERROR (", __LINE__, "): oov phone $oovPhone in $word\t$phones\n";
  }
  for(my $i = 0; $i < @B; $i ++) {
    my $array = $B[$i];
    my $pron = join(" ", @$array);
    print ASRLEX "$word\t$pron\n";
  }
}
close ASRLEX;
