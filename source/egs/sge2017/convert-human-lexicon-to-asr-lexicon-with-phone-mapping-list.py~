#!/usr/bin/env python 

# -*- coding: utf-8 -*-
# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu

import argparse
import sys
import os
import re
import numpy as np
import logging

sys.path.insert(0, 'steps')
import libs.common as common_lib
logger = logging.getLogger('libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting convert-human-lexicon-to-asr-lexicon-with-phone-mapping-list')

def get_args():
    parser = argparse.ArgumentParser(description='Arguments parser')
    parser.add_argument('--debug', dest='debug', action='store_true')
    parser.add_argument('--normalize', dest='normalize', action='store_true')
    parser.add_argument('--split-words', dest='split_words', action='store_true')
    parser.add_argument('phone_mapping_file', type=str, help='phone mapping file realizes human phone to machine phone mapping')
    parser.add_argument('human_lexicon', type=str, help='Human labeled lexicon')
    parser.add_argument('mapped_lexicon', type=str, help='new lexicon with mapped phone set')
    return parser.parse_args()
def CheckLogger(args):
    if args.debug:
        ''' does not work '''
        logger.setLevel(logging.DEBUG)
        handler.setLevel(logging.DEBUG)
    logger.debug('logger now is at DEBUG level')
def BuildPhoneDict(phoneDict, key, value):
    if key not in phoneDict:
        myList = list()
        phoneDict[key] = myList
        phoneDict[key].append(value)
    else:
        logger.debug('phone {0} has multiple mappings'.format(key))
        phoneDict[key].append(value)
        
def ReadPhoneMapFile(args, phoneDict):
    with open(args.phone_mapping_file, 'r') as inputFile:
        for line in inputFile:
            myList = line.split('\t')
            if len(myList) != 2:
                raise Exception('unexpected line {0} in file {1}'.format(line, args.phone_mapping_file))
            tgtPhone = myList[0].strip()
            phoneList = myList[1].split('/')
            for phone in phoneList:
                phone = phone.strip()
                if phone:
                    BuildPhoneDict(phoneDict, phone, tgtPhone)
def NormalizePronunciation(pron):
    pron = re.sub(r'[\.\+\#]', ' ', pron)
    pron = re.sub(r' +', ' ', pron)
    return pron
def AppendListDict(words, prons, listDict):
    wordList = [x for x in words.split() if x.strip()]
    pronList = [x for x in prons.split('#') if x.strip()]
    wordNum = len(wordList)
    pronNum = len(pronList)
    if wordNum != pronNum:
        print('illegal pronunciation transcription: {0}\t{1}\t'.format(words, prons))
        return
    for idx, word in enumerate(wordList):
        pron = pronList[idx]
        pron = NormalizePronunciation(pron)
        wordPron = "{0}\t{1}".format(word, pron)
        listDict.append(wordPron)
def ReadHumanLexicon(args, listArray):
    with open(args.human_lexicon, 'r') as inputFile:
        prevWord = ''
        for line in inputFile:
            myList = line.split('\t')
            if len(myList) != 2:
                raise Exception('unexpected line {0} in {1}'.format(line, args.human_lexicon))
            word = myList[0].strip()
            if word:
                prevWord = word
            else:
                word = prevWord
                if not prevWord:
                    raise Exception('empty word for line {0}'.format(line))
            # logger.debug('word={0}'.format(word))
            pron = myList[1].strip()
            if not pron:
                raise Exception('empty pronunciation line {0}'.format(line))
            if args.normalize:
                pron = NormalizePronunciation(pron)
            if args.split_words:
                AppendListDict(word, pron, listArray)
            else:
                wordPron = '{0}\t{1}'.format(word, pron)
                listArray.append(wordPron)
def _Push(myList, value):
    myList.append(value)
def _Pop(myList):
    if not myList:
        raise Exception('nothing to pop in list')
    outputList = myList[-1]
    del myList[-1]
    return outputList
def DoSequenceMap(srcList, mapDict, tgtList):
    i = 0
    pos = 0
    phoneNum = len(srcList)
    backTrace = list()
    outputPhoneList = list()
    while(True):
        if i < phoneNum:
            phone = srcList[i]
            if phone not in mapDict:
                raise Exception("phone {0} is  unidentified in mapDict".format(phone))
            mapPhoneList = mapDict[phone]
            mapPhone = mapPhoneList[0]
            _Push(backTrace, [mapPhoneList, 1])
            outputPhoneList.append(mapPhone)
            i += 1
        if i == phoneNum:
            tgtList.append(list(outputPhoneList))
            i -= 1
            del outputPhoneList[-1]
            [mapPhoneList, pos] = _Pop(backTrace)
            altNum = len(mapPhoneList)
            while(pos >= altNum and i >= 0):
                i -= 1
                if i < 0:
                    return
                del outputPhoneList[-1]
                [mapPhoneList, pos ] = _Pop(backTrace)
                altNum = len(mapPhoneList)
            mapPhone = mapPhoneList[pos]
            outputPhoneList.append(mapPhone)
            _Push(backTrace, [mapPhoneList, pos+1])
            i += 1

def DictPhoneMap(srcDictList, phoneDict, tgtDictList):
    for wordPron in srcDictList:
        m = re.search(r'(^\S+)\s+(.*)', wordPron)
        if not m:
            raise Exception('unexpected word pronunciation {0}'.format(wordPron))
        word = m.group(1)
        pron = m.group(2)
        phoneList = [x for x in pron.split() if x.strip()]
        mappedPhoneList = list()
        DoSequenceMap(phoneList, phoneDict, mappedPhoneList)
        for mappedPron in mappedPhoneList:
            pron = ' '.join(mappedPron)
            wordMappedPron = "{0}\t{1}".format(word, pron)
            tgtDictList.append(wordMappedPron)
def CheckOovPhone(wordListDict, phoneDict):
    oovInstance = 0
    for wordPron in wordListDict:
        wordPron = wordPron.strip()
        m = re.search(r'(\S+)\s+(.*)$', wordPron)
        pron = m.group(2)
        phoneList = [x for x in pron.split() if x.strip() ]
        for phone in phoneList:
            if phone not in phoneDict:
                print('oov phone {0} in {1}'.format(phone, wordPron))
                oovInstance += 1
    if oovInstance > 0:
        return False
    return True
def main():
    args = get_args()
    CheckLogger(args)
    phoneDict = dict()
    ReadPhoneMapFile(args, phoneDict)
    for phone in phoneDict:
        logger.debug('{0}\t{1}'.format(phone, phoneDict[phone]))
    listHumanLexicon = list()
    ReadHumanLexicon(args, listHumanLexicon)
    if not CheckOovPhone(listHumanLexicon, phoneDict):
        raise Exception('oov phone found in dict {0}'.format(args.human_lexicon))
    listMachineLexicon = list()
    DictPhoneMap(listHumanLexicon, phoneDict, listMachineLexicon)
    outputFile = open(args.mapped_lexicon, 'w')
    for wordPron in listMachineLexicon:
        outputFile.write('{0}\n'.format(wordPron))
    outputFile.close()
if __name__ == "__main__":
    main()



