#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;

my $remove_words = '';
my $from =1;

GetOptions("remove_words|remove-words=s" => \$remove_words,
           "from=i" => \$from) or die;
my %vocab = ();
if($remove_words ne '') {
  my @A = split(' ', $remove_words);
  for(my $i = 0; $i < @A; $i ++) {
    $vocab{$A[$i]} ++;
  }
}

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  my @B = split(" ");
  if(@B <=1) {
    print STDERR "## ($0): removing utterance '$_'\n";
  }
  my $nCount = 0; my $hasContent = 0;
  for(my $i = $from -1; $i < @B; $i ++) {
    my $word = $B[$i];
    if(exists $vocab{$word}) {
      $nCount ++;
    } else {
      $hasContent ++;
      last;
    }
  }
  if($hasContent > 0) {
    print "$_\n";
  } else {
    print STDERR "## LOG ($0): removing utterance '$_'\n";
  }
}
print STDERR "## LOG ($0): stdin ended\n";
