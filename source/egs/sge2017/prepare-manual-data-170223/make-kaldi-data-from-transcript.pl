#!/usr/bin/perl 

use warnings;
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  die "\nExample: cat transcript.txt | $0  tgtdir\n\n";
}
# begin sub
sub ModifySpeaker {
  my ($s, $utt) = @_;
  $$s = 'angela-unknown' if ($$s eq 'angela');
  $$s = 'speaker-938-unknown' if ($$s eq '_recording_');
  $$s = 'parliament-unknown' if ($$s eq 'a');
  $$s = 'bharati-talkback' if ($$s eq 'bharati' && $utt =~ /talkback/);
  $$s = 'bharati-dialogue' if ($$s eq 'bharati' && $utt =~ /dialogue/);
  $$s = 'daphne-culture' if ($$s eq 'daphne' && $utt =~ /culture/);
  $$s = 'david-king-938' if ($$s eq 'david-king');
  $$s = 'david-talkback' if ($$s eq 'david' && $utt =~ /talkback/);
  $$s = 'joseph-938-culture' if ($$s eq 'joseph' && $utt =~ /culture/);
  $$s = 'john-938-talkback' if ($$s eq 'john' && $utt =~ /talkback/);
  $$s = 'joe-tan-938-art'  if ($$s eq 'joe-tan' && $utt =~ /art/);
  $$s = 'kc-lee-938' if ($$s eq 'kc-lee' && $utt =~ /asol-eugene/);
  $$s = 'keith-de-souza' if($$s eq 'keith' && $utt =~ /talkback/);
  $$s = 'lee-bee-wah-parliament' if($$s eq 'lee-bee-wah' && $utt =~ /committee/);
  $$s = 'lee-shan-parliament' if($$s eq 'lee-shan' && $utt =~ /committee/);
  $$s = 'lee-938-talkback' if($$s eq 'lee' && $utt =~ /talkback/);
  $$s = 'ng-938-talkback' if($$s eq 'ng' && $utt =~ /talkback/);
  $$s = 'peterlee-938-kid' if($$s eq 'peter-lee' && $utt =~ /parenting/);
  $$s = 'nuboer-peter-938-culture' if($$s eq 'peter-nuboer' && $utt =~ /culture/);
  $$s = 'peng-pek-seng-soundcloud' if($$s eq 'peng-pek-seng' && $utt =~ /soundcloud/);
  $$s = 'quek-peter-938-father' if($$s eq 'peter-quek' && $utt =~ /parenting/);
  $$s = 'tony-koo-938-heritage' if($$s eq 'tony-koo' && $utt =~ /heritage/);
  $$s = 'tony-938-talkback' if($$s eq 'tony' && $utt =~ /talkback/);
  $$s = 'spk-unk-happiness-938' if($$s eq 'unknown-938-happiness' && $utt =~ /asol-building/);
}
# end sub
my ($tgtdir) = @ARGV;
open(UTT2SPK, "|sort -u > $tgtdir/utt2spk") or die;
open(TEXT, "|sort -u > $tgtdir/text") or die;
open (SEG, "|sort -u > $tgtdir/segments") or die;
print STDERR "## LOG ($0): stdin [transcript.txt] expected\n";
while(<STDIN>) {
  chomp;
  m:(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(.*):g or next;
  my ($utt, $start, $end, $spk, $text) = ($1, $2, $3, $4, $5);
  ModifySpeaker(\$spk, $utt);
  my $label = sprintf("%s-%s-%05d-%05d", $spk, $utt, $start*100, $end*100);
  $text = lc $text;
  print TEXT "$label $text\n";
  print UTT2SPK "$label $spk\n";
  print SEG "$label $utt $start $end\n";
}
print STDERR "## LOG ($0): stdin ended\n";

close UTT2SPK;
close TEXT;
close SEG;
