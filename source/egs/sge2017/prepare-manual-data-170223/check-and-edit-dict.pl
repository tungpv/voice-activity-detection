#!/usr/bin/perl 

use warnings;
use strict;
use utf8;
use open qw(:std :utf8);
# begin sub
sub WordTransform {
  my ($word) = @_;
  $$word =~ s:[\-_\~\.\#\?,]: :g;
}
# end sub
my $numArgs = scalar @ARGV;

if ($numArgs != 2) {
  die "\n[Example]: cat transfer-dict.txt | $0 new-transfer-dict.txt new-word-list-for-checking\n\n";
}

my ($transferDict, $wordList) = @ARGV;

print STDERR "## LOG ($0): stdin expected\n";
open(DICT, "|sort -u > $transferDict") or die;
open(WLIST, "|sort -u >$wordList") or die;
my %vocab = ();
while (<STDIN>) {
  chomp;
  m:(\S+)\s+(.*):g or next;
  my ($from, $to) = ($1, $2);
  WordTransform(\$to);
  print DICT "$from\t$to\n";
  my @A = split(" ", $to);
  for(my $i = 0; $i < @A; $i ++) {
    my $word = $A[$i];
    if(not exists $vocab{$word} && $word ne '') {
      print WLIST "$word\n";
      $vocab{$word} ++;
    }
  }
}

close DICT;
close WLIST;

print STDERR "## LOG ($0): stdin ended\n";
