#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;
if($numArgs != 1) {
  die "\n[Example]: cat wordlist| $0 dict > sub-dict\n\n";
}

my ($dictFile) = @ARGV;

# begin sub
sub LoadDict {
  my ($dictFile, $vocab) = @_;
  open(F, "$dictFile") or die;
  while(<F>) {
    m:(\S+)\s+(.*):g or next; 
    my ($word, $pron) = ($1, $2);
    my $entry = sprintf("%s\t%s", lc $word, $pron); 
    my $vptr;
    if(not exists $$vocab{$word}) {
      my %v = ();
      $vptr = $$vocab{$word} = \%v;
    } else {
      $vptr = $$vocab{$word};
    }
    $$vptr{$entry} ++;
  }
  close F;
}
# end sub
my %vocab = ();
LoadDict($dictFile, \%vocab);
print STDERR "## LOG ($0): stdin expected\n";
open (O, "|sort -u") or die;
while (<STDIN>) {
  chomp;
  my @A = split(" ");
  for(my $i = 0; $i < scalar @A; $i ++) {
    my $word = lc $A[$i];
    if(exists $vocab{$word}) {
      my $vPtr = $vocab{$word};
      foreach my $entry (keys%$vPtr) {
        print O "$entry\n";
      }
    }
  }
}
print STDERR "## LOG ($0): stdin ended\n";
close F;
