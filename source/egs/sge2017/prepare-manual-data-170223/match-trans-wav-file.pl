#!/usr/bin/perl
use warnings;
use strict;

use utf8;
use open qw(:std :utf8);
use lib qw(source/egs/myperllib);
use GridText;
my $numArgs = scalar @ARGV;

if($numArgs != 2) {
  die "\n[Example]: cat translist.txt | $0 wav.scp tgtdir\n\n";
}

my ($wavscp, $tgtdir) = @ARGV;

my %vocab = ();

# begin sub
sub LoadWavScpFile {
  my ($file, $vocab) = @_;
  open(F, "$file") or die "## ERROR (", __LINE__, "): canot open $file\n";
  while(<F>) {
    chomp;
    m:(\S+)\s+(.*): or next;
    $$vocab{$1} = $2;
  }
  close F;
}
# end sub
LoadWavScpFile($wavscp, \%vocab);
open(WAVSCP, "|sort -u >$tgtdir/wav.scp") or die;
open(TRANS, "|sort -k2,3 -n > $tgtdir/transcript.txt") or die;
my $sLogDoc;
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  my $sTextFile = $_;  my $label = $sTextFile;  $label =~ s:.*\/::g; $label =~ s:\.TextGrid::g; $label =~ s:-mp3$::g;
  if(not exists $vocab{$label}) {
    print STDERR "## WARNING (", __LINE__, "): no wave file for transcript $_\n";
  } else {
    print WAVSCP "$label $vocab{$label}\n";
    my %transVocab = ();
    if(ReadGridText($sTextFile, \%transVocab, \$sLogDoc) != 0) {
      die "## ERROR (", __LINE__, "): ReadGridText error for $sTextFile\n";
    }
    foreach my $key (keys%transVocab) {
      my $text = $transVocab{$key};
      print TRANS $label . " $text\n";
    }
  }
}

print STDERR "## LOG ($0): stdin ended\n";
close WAVSCP;
close TRANS;
