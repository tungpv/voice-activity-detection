#!/usr/bin/perl
use warnings;
use strict;

use utf8;
use open qw(:std :utf8);
use Getopt::Long;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\n[Example]: cat segments | $0 wav.scp > new-segments\n\n";
}

my ($wavscp) = @ARGV;

# begin sub
sub LoadWavScp {
  my ($wavScp, $vocab) = @_;
  open(F, "$wavScp") or die "## ERROR (", __LINE__, "): cannot open $wavScp\n";
  while(<F>) {
    chomp;
    m:^(\S+):g or next;
    $$vocab{$1} ++;
  }
  close F;
}
# end sub
my %vocab = ();
LoadWavScp($wavscp, \%vocab);
print STDERR "## LOG ($0): stdin expected\n";
my $totalNum = 0;
my $selectedNum = 0;
while(<STDIN>) {
  chomp;
  m:(\S+)\s+(\S+)\s+(\S+)\s+(\S+):g or next;
  $totalNum ++;
  my ($segName, $segFile, $start, $end) = ($1, $2, $3, $4);
  next if(not exists $vocab{$segFile});
  $selectedNum ++;
  print "$_\n";
}
print STDERR "## LOG ($0): stdin ended, selected $selectedNum out of $totalNum\n";
