#!/usr/bin/perl

use warnings;
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;

my $exclude_wave_list = '';
my $wave_list= '';

GetOptions ("exclude-wave-list|exclude_wave_list=s" => \$exclude_wave_list,
  "wave_list|wave-list=s"=> \$wave_list) or die;

# begin sub
sub LoadWaveList {
  my ($waveList, $vocab) = @_;
  open(F, "$waveList") or die;
  while(<F>) {
    chomp;
    m:^(\S+):g or next;
    $$vocab{$1} ++;
  }
  close F;
}
# end sub
my %excludeVocab = ();
my %vocab = ();
if($exclude_wave_list ne '') {
   LoadWaveList($exclude_wave_list, \%excludeVocab);
}
if($wave_list ne '') {
  LoadWaveList($wave_list, \%vocab);
}

print STDERR "## LOG ($0): stdin expected\n";

my $totalNum = 0;
my $selectedNum = 0;
while(<STDIN>) {
  chomp;
  $totalNum ++;
  m:^(\S+):g or next;
  my ($lab) = ($1);
  next if(exists $excludeVocab{$lab});
  next if($wave_list ne '' && not exists $vocab{$lab});
  $selectedNum ++;
  print $_, "\n";
}
print STDERR "## LOG ($0): selected $selectedNum out of $totalNum\n";
print STDERR "## LOG ($0): stdin ended\n";
