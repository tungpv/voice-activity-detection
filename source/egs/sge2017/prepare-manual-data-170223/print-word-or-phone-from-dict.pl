#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
my $print_phones;
GetOptions(
  "print_phones|print-phones" => \$print_phones
);
print STDERR "## LOG ($0): stdin expected\n";
open(F, "|sort -u") or die;
while(<STDIN>) {
  m:(\S+)\s+(.*):g;
  my ($word, $pron) = ($1, $2);
  if($print_phones) {
    my @A = split(" ", $pron);
    for(my $i = 0; $i < @A; $i++) {
      print F "$A[$i]\n";
    }
  } else {
    print F "$word\n";
  }
}
print STDERR "## LOG ($0): stdin ended\n";
close F;
