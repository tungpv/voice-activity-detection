#!/usr/bin/perl 
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);

print STDERR "## LOG ($0): stdin expected\n";
open (F, "|sort -u") or die;
while(<STDIN>) {
  chomp;
  m:(\S+)\s+(\S+):g or next;
  print F  "$1\t$1\n";
}
close F or die;
print STDERR  "## LOG ($0): stdin ended \n";
