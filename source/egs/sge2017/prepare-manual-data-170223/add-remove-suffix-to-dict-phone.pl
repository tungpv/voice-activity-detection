#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
my $add_suffix = '';
my $remove_suffix = '';

GetOptions (
  "add_suffix|add-suffix=s" => \$add_suffix,
  "remove_suffix|remove-suffix=s" =>\$remove_suffix) or die;
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m:(\S+)\s+(.*):g or next;
  my ($word, $pron) = ($1, $2);
  if($remove_suffix ne '') {
    $pron =~ s:$remove_suffix::g;
  }
  if($add_suffix ne '') {
    my @A = split(" ", $pron);  $pron = '';
    for(my $i = 0; $i < @A; $i ++) {
      my $phone = $A[$i] . $add_suffix;
      $pron .= "$phone ";
    }
    $pron =~ s: $::;
  }
  print "$word\t$pron\n";
}
print STDERR "## LOG ($0): stdin ended\n";
