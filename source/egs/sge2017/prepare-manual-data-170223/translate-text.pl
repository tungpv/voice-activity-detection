#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;

my $from=1;

GetOptions("from=i" => \$from) or die;

my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  die "\n[Example]: cat text | $0 [--from=1] dict.txt > translated-text\n\n";
}
my ($dictFile) = @ARGV;

# begin subs
sub LoadDict {
  my ($dictFile, $vocab) = @_;
  open(D, "$dictFile") or die;
  while(<D>) {
    chomp;
    m:(\S+)\s+(.*):g or next;
    my ($s, $t) = ($1, $2);
    $t = '' if(lc $t eq 'null' or $t eq '');
    $$vocab{$s} = $t;
  }
  close D;
}
# end subs
my %vocab = ();
LoadDict($dictFile, \%vocab);
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(" ");
  next if scalar @A == 0;
  my $utt= '';
  for(my $i = 0; $i< $from -1; $i ++) {
    $utt .= $A[$i] . ' ';
  }
  for(my $i = $from -1; $i < scalar @A; $i ++) {
    my $word = $A[$i];
    $word = $vocab{$word} if exists $vocab{$word};
    $utt .= $word . ' ';
  }
  $utt =~ s: $::g;
  print $utt . "\n";
}
print STDERR "## LOG ($0): stdin ended\n";
