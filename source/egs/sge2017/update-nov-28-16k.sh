#!/bin/bash 

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh 

# begin options
steps=
cmd=slurm.pl
nj=40
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF
 [examples]:

 $0 --steps 1 /home2/hhx502/sge2017/data/update-nov-28-16k  /home2/hhx502/sge2017/exp/update-nov-28-16k

EOF
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

datadir=$1
expdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

sdata=/home/hhx502/w2016/swbd/data/train/fbank_pitch
tgtdata=$datadir/swbd16k/data-1
if [ ! -z $step01 ]; then
  [ -d $tgtdata ] || mkdir -p $tgtdata
  cp $sdata/{segments,spk2utt,utt2spk} $tgtdata/
  cat $sdata/text | \
  source/egs/sge2017/update-nov-28-16k/rewrite-noise-word.pl > $tgtdata/text
  cat $sdata/wav.scp | \
  source/egs/sge2017/update-nov-28-16k/upsample-wav-and-copy.pl > $tgtdata/wav.scp
  sdata=$tgtdata
  tgtdata=$datadir/swbd16k/data
  source/egs/sge2017/update-nov-28-16k/copy-data.sh --prefix eng002-  $sdata $tgtdata
  utils/fix_data_dir.sh $tgtdata
  utils/validate_data_dir.sh --no-feats $tgtdata
  echo "## LOG (step01, $0): done & check '$tgtdata/wav.scp' & '$tgtdata/text'"
fi 
srcdict=/home/hhx502/w2016/swbd/data/local/dict_nosp
tgtdict=$datadir/swbd16k/dict
if [ ! -z $step02 ]; then
  [ -d $tgtdict ] || mkdir -p $tgtdict
  cat $srcdict/lexicon.txt | \
  grep -v '\[' | \
  egrep -v 'sil|<' | \
  source/egs/sge2017/update-nov-28-16k/normalize-swbd-dict.pl > $tgtdict/lexicon.txt
  cat $tgtdict/lexicon.txt | \
  source/egs/sge2017/update-nov-28-16k/print-swbd-phone-set.pl |\
  sort -u > $tgtdict/nonsilence_phones.txt
  echo "## LOG (step02, $0): done & check '$tgtdict/lexicon.txt'"
fi
sdata=/home/hhx502/w2016/wsj/data/train_si284
tgtdata=$datadir/wsj/data-1
tgtdict=$datadir/wsj/dict
if [ ! -z $step03 ]; then
  [ -d $tgtdata ] || mkdir -p $tgtdata
  [ -d $tgtdict ] || mkdir -p $tgtdict
  cp $sdata/{segments,utt2spk,spk2utt,wav.scp} $tgtdata/
  cat $sdata/text | \
  perl -pe 's/[\*]//g;' | \
  perl -ane 'chomp; m/(\S+)\s+(.*)/g or next; printf("%s %s\n", $1, lc $2);' > $tgtdata/text
  sdata=$tgtdata
  tgtdata=$datadir/wsj/data
  source/egs/sge2017/update-nov-28-16k/copy-data.sh --prefix eng003-  $sdata $tgtdata
  utils/validate_data_dir.sh --no-feats $tgtdata
  cat $tgtdata/text | \
  source/egs/mandarin/local/prepare-msra-863-mix/subset-lex-with-text.pl --from=1 --output-dir=$tgtdict  \
  /data/users/hhx502/w2017/mandarin/dict1/cmu-dict-normalized.txt
  echo "## LOG (step03, $0): done & check '$tgtdict' & '$tgtdata/text'"
fi
sdata=/home2/hhx502/sge2017/data/hub4
tgtdata=$datadir/hub4/data-1
tgtdict=$datadir/hub4/dict
if [ ! -z $step04 ]; then
  [ -d $tgtdata ] || mkdir -p $tgtdata
  [ -d $tgtdict ] || mkdir -p $tgtdict
  utils/combine_data.sh $tgtdata $sdata/data96 $sdata/data97
  # cp $sdata/data97/{segments,utt2spk,spk2utt,text,wav.scp} $tgtdata/
  # egrep -v 'janedoe|johndoe|a960521' $tgtdata/utt2spk   > $tgtdata/utt2spk-new
  # mv $tgtdata/utt2spk-new $tgtdata/utt2spk
  utils/fix_data_dir.sh $tgtdata
 
  cat $tgtdata/text | 
  source/egs/mandarin/local/prepare-msra-863-mix/subset-lex-with-text.pl --from=1 --output-dir=$tgtdict \
  "cat /data/users/hhx502/w2017/mandarin/dict1/cmu-dict-normalized.txt $datadir/swbd16k/dict/lexicon.txt |"
  sdata=$tgtdata
  tgtdata=$datadir/hub4/data
  source/egs/sge2017/update-nov-28-16k/copy-data.sh --prefix eng004-  $sdata $tgtdata
  utils/fix_data_dir.sh $tgtdata
  utils/validate_data_dir.sh --no-feats $tgtdata
  echo "## LOG (step04, $0): done & check '$tgtdata' & '$tgtdict'"
fi
sdata=/home2/hhx502/ted-libri-en/data/train-merge-ted12-libri460/fbank-pitch
tgtdata=$datadir/ted-libri/data-1
tgtdict=$datadir/ted-libri/dict

if [ ! -z $step05 ]; then
  [ -d $tgtdata ] || mkdir -p $tgtdata
  [ -d $tgtdict ] || mkdir -p $tgtdict
  cp $sdata/{segments,utt2spk,spk2utt,wav.scp} $tgtdata/
  cat $sdata/text | \
  perl -pe 's/<uh>/<v-noise>/g; s/<um>/<v-noise>/g; s/<breath>/<v-noise>/g; s/<smack>/<v-noise>/g;s/<cough>/<v-noise>/g;' \
  > $tgtdata/text
  cat /home2/hhx502/ted-libri-en/data/g2p-libri/dict/lexicon.txt | \
  grep -v '<' > $tgtdict/lexicon.txt
  sdata=$tgtdata
  tgtdata=$datadir/ted-libri/data
  source/egs/sge2017/update-nov-28-16k/copy-data.sh --prefix eng001-  $sdata $tgtdata
  echo "## LOG (step05, $0): done & check '$tgtdata' & '$tgtdict'"
fi
sdata=/home2/hhx502/sge2017/data/train-mix431/fbank-pitch
exclude_utt_list=/home2/hhx502/sge2017/data/train-wsj15h/16k/utt2spk
tgtdata=$datadir/sge-mix/data-1
srcdict=/home2/hhx502/sge2017/data/update-lm-nov-09-2016/dict
tgtdict=$datadir/sge-mix/dict
if [ ! -z $step06 ]; then
  cat $sdata/utt2spk | \
  source/egs/sge2017/update-nov-28-16k/subset-utt2spk.pl $exclude_utt_list > $tgtdata/uttlist
  utils/subset_data_dir.sh --utt-list $tgtdata/uttlist $sdata $tgtdata
  cat $tgtdata/utt2spk | perl -pe 's/ref\-/rev\-/g;' > $tgtdata/utt2spk-1
  mv $tgtdata/utt2spk-1 $tgtdata/utt2spk
  utils/utt2spk_to_spk2utt.pl < $tgtdata/utt2spk > $tgtdata/spk2utt
  rm $tgtdata/{cmvn.scp,feats.scp}
  # utils/fix_data_dir.sh $tgtdata
  # utils/validate_data_dir.sh --no-feats $tgtdata
    
  [ -d $tgtdict ] || mkdir -p $tgtdict
  cp $srcdict/lexicon.txt $tgtdict/
  sdata=$tgtdata
  tgtdata=$datadir/sge-mix/data
  source/egs/sge2017/update-nov-28-16k/copy-data.sh --prefix eng005-  $sdata $tgtdata
  utils/fix_data_dir.sh $tgtdata
  utils/validate_data_dir.sh --no-feats $tgtdata
  echo "## LOG (step06, $0): done & check '$tgtdata' & '$tgtdict'"
fi
sdata=/home2/hhx502/sge2017/data/train-wsj15h/16k
tgtdata=$datadir/sge-wsj15h/data
if [ ! -z $step07 ]; then
  source/egs/sge2017/update-nov-28-16k/copy-data.sh --prefix eng006-  $sdata $tgtdata
  utils/validate_data_dir.sh --no-feats $tgtdata
  echo "## LOG (step07, $0): done & check '$tgtdata'"
fi

tgtdata=$datadir/train-merge
if [ ! -z $step10 ]; then
  echo "## LOG (step10, $0): merge data"
  utils/combine_data.sh $tgtdata $datadir/{hub4,sge-wsj15h,sge-mix,swbd16k,ted-libri,wsj}/data
  utils/fix_data_dir.sh $tgtdata
  utils/validate_data_dir.sh --no-feats $tgtdata
  echo "## LOG (step10, $0): done & check '$tgtdata'"
fi
tgtdict=$datadir/dict
if [ ! -z $step11 ]; then
  [ -d $tgtdict ] || mkdir -p $tgtdict
  cat /home2/hhx502/sge2017/data/update-nov-28-16k/hub4/dict/lexicon.txt \
  /home2/hhx502/sge2017/data/update-nov-28-16k/swbd16k/dict/lexicon.txt  \
  /home2/hhx502/sge2017/data/update-nov-28-16k/ted-libri/dict/lexicon.txt \
  /home2/hhx502/sge2017/data/update-nov-28-16k/wsj/dict/lexicon.txt  \
  $datadir/sge-mix/dict/lexicon.txt | source/egs/sge2017/update-nov-28-16k/merge-dict.pl \
   --word-lower-case --phone-upper-case $tgtdict/lexicon-history.txt "|egrep -v '<|SIL'> $tgtdict/nonsilence_phones.txt"
  cat $tgtdict/lexicon-history.txt | \
  perl -pe 'if(/</ and ! /SIL/) {m/(\S+)\t(\S+)/g; $_ = "$1\t" . lc($2) . "\n"; } ' > $tgtdict/lexicon.txt
  cp /home2/hhx502/sge2017/data/update-lm-nov-09-2016/dict/{silence_phones.txt,optional_silence.txt,extra_questions.txt} \
  $tgtdict/
  utils/validate_dict_dir.pl $tgtdict 
  rm $tgtdict/lexicon-history.txt 2>/dev/null
  echo "## LOG (step11, $0): done & check '$tgtdict/lexicon.txt'"
fi
lang=$datadir/lang
if [ ! -z $step12 ]; then
  utils/prepare_lang.sh $tgtdict "<unk>" $lang/tmp $lang
  echo "## LOG (step12, $0): done & check '$lang' @ `date`"
fi

lmdir=$datadir/lm
if [ ! -z $step13 ]; then
  echo "## LOG (step13, $0): build lm @ `date`"
  [ -d $lmdir ] || mkdir -p $lmdir
  cat /home2/hhx502/sge2017/data/dev-seame.28/16k/text \
  /home2/hhx502/sge2017/data/fstd-demo-updated/16k/text | \
  source/egs/sge2017/local/build-lm/dump-kaldi-text.pl | gzip -c > $lmdir/dev-text.gz
  
  cat /home2/hhx502/sge2017/data/update-nov-28-16k/train-merge/text <(gzip -cd /home2/hhx502/sge2017/data/update-lm-nov-09-2016/lm-outdomain/train-text.gz) | \
  source/egs/sge2017/local/build-lm/dump-kaldi-text.pl | gzip -c > $lmdir/train-text.gz

  cat $tgtdict/lexicon.txt | \
  source/egs/sge2017/local/build-lm/print-word-list.pl | gzip -c > $lmdir/vocab.gz
  
  source/egs/sge2017/local/build-lm/train-srilm-v2.sh --steps 1,2 --lm-order-range "3 3" \
  --cutoff-csl "3,011,012"  $lmdir || exit 1
  echo "## LOG (step13, $0): done & check '$lmdir'"
fi
sdata=$datadir/train-merge
if [ ! -z $step14 ]; then
  echo "## LOG (step14, $0): make mfcc-pitch & fbank-pitch @ `date`"
  for sdata in $datadir/train-merge; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc16k.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    utils/fix_data_dir.sh $data
    echo "## LOG (step14, $0): mfcc-pitch done @ `date`"
    data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    echo "## LOG (step14, $0): fbank-pitch done @ `date`"
  done
fi
factor=0.3
if [ ! -z $step15 ]; then
  source/egs/swahili/subset_data.sh --subset-time-ratio $factor --random true \
  $sdata/mfcc-pitch $datadir/train-merge-subset$factor/mfcc-pitch
  echo "## LOG (step15, $0): done & check '$datadir/train-merge-subset$factor/mfcc-pitch'"
fi
train_id=b
if [ ! -z $step16 ]; then
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1,2,3,4,5,6,7 \
  --done-with-lda-mllt true \
  --train-id $train_id --cmvn-opts "--norm-means=true"  --state-num 8000 --pdf-num 160000 \
  --devdata /home2/hhx502/sge2017/data/dev-seame.28/16k/fbank-pitch \
  $datadir/train-merge-subset$factor/mfcc-pitch $lang  $expdir || exit 1
fi
train_id=a
if  [ ! -z $step17 ]; then
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1,2,3,4,5,6,7 \
  --alidir $expdir/tri2b/ali_train-merge \
  --train-id $train_id --cmvn-opts "--norm-means=true"  --state-num 12000 --pdf-num 240000 \
  --devdata /home2/hhx502/sge2017/data/dev-seame.28/16k/mfcc-pitch \
  $datadir/train-merge/mfcc-pitch $lang  $expdir || exit 1
fi

if [ ! -z $step18 ]; then
  source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 --delta-opts "--delta-order=2"  --nnet-pretrain-cmd "steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048" \
  --cmvn-opts "--norm-means=true --norm-vars=true" \
 --use-partial-data-to-pretrain true --pretraining-rate 0.2 \
 --nnet-id nnet4a --graphdir /home2/hhx502/sge2017/exp/update-nov-28-16k/tri3a/graph \
 --devdata /home2/hhx502/sge2017/data/dev-seame.28/16k/fbank-pitch \
  $datadir/train-merge/fbank-pitch $lang $expdir/tri3a/ali_train $expdir || exit 1
fi
