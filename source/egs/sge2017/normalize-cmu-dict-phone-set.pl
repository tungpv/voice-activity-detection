#!/usr/bin/perl -w
use strict;

print STDERR "## LOG (main, $0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s(.*)/ or next;
  my $word = lc $1; 
  my @pron = split(/\s+/, $2);
  for(my $i = 0; $i < scalar @pron; $i++) {
    $pron[$i] =~ s/(\S+)\d$/$1/g;
  }
  print "$word\t", join(" ", @pron), "\n";
}
print STDERR "## LOG (main, $0): stdin ended\n";
