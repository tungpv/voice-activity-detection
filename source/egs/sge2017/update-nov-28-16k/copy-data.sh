#!/bin/bash 

# begin options
prefix=
# end options

. utils/parse_options.sh || exit 1
function Usage {
 cat<<EOF

 $0 --prefix sge- /home2/hhx502/sge2017/data/train-wsj15h/16k /home2/hhx502/sge2017/data/update-nov-28-16k/sge-wsj15h/data

EOF
}
if [ $# -ne 2 ]; then
  Usage && exit 1
fi

sdata=$1
tgtdata=$2

[ -d $tgtdata ] || mkdir -p $tgtdata

cat $sdata/wav.scp | \
source/egs/sge2017/update-nov-28-16k/add-prefix-to-key.pl $prefix > $tgtdata/wav.scp
cat $sdata/utt2spk | \
source/egs/sge2017/update-nov-28-16k/add-prefix-to-key-and-value.pl $prefix   > $tgtdata/utt2spk
cat $sdata/segments | \
source/egs/sge2017/update-nov-28-16k/add-prefix-to-key-and-value.pl $prefix > $tgtdata/segments
cat $sdata/text | \
source/egs/sge2017/update-nov-28-16k/add-prefix-to-key.pl $prefix > $tgtdata/text

utils/utt2spk_to_spk2utt.pl < $tgtdata/utt2spk > $tgtdata/spk2utt
