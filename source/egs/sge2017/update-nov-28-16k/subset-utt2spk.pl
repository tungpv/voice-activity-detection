#!/usr/bin/perl -w
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\nExample: cat utt2spk1 | $0 utt2spk2 utt2spk1-2\n\n";
}
my ($utt2spk) = @ARGV;
# begin sub
sub LoadDict {
  my ($sFile, $vocab) = @_;
  open(F, "$sFile") or die "## ERROR (LoadDict): cannot open $sFile\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    $$vocab{$1} ++;
  }
  close F;
}
# end sub

my %vocab = ();
LoadDict($utt2spk, \%vocab);
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my $utt = $1;
  if(not exists $vocab{$utt}) {
    print "$utt\n";
  }
}
print STDERR "## LOG ($0): stdin ended\n";
