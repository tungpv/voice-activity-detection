#!/usr/bin/perl -w 
use strict;
my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  die "\nExample: cat utt2spk | $0 1- > new-utt2spk\n\n";
}
my $prefix = shift @ARGV;
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  s/(\S+)\s+(.*)/$prefix${1} $2/g;
  print "$_\n";
}
print STDERR "## LOG ($0): stdin ended\n";
