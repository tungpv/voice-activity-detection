#!/usr/bin/perl -w
use strict;

while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($word, $pron) = ($1, $2);
  $pron = uc $pron;
  printf("%s\t%s\n", $word, $pron);
}
