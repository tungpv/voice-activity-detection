#!/usr/bin/perl -w 

use strict;
use Getopt::Long;
my $word_lower_case = 0;
my $phone_upper_case = 0;
my $trivial_merge = 0;
my $main_dict = '';
GetOptions('word_lower_case|word-lower-case' => \$word_lower_case,
           'phone_upper_case|phone-upper-case' => \$phone_upper_case,
           'trivial_merge|trivial-merge' => \$trivial_merge,
           'main-dict' => \$main_dict);

my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  die "\ncat lexicon.txt | $0 --word-lower-case --phone-upper-case new-lexicon.txt phones.txt\n\n";
}
my ($sLexFile, $sPhones) = @ARGV;
open(LEX, "|sort >$sLexFile") or die "## ERROR ($0): cannot open $sLexFile\n";
open(PHN, "$sPhones") or die "## ERROR ($0): cannot open $sPhones\n";
print STDERR "## LOG ($0): stdin expected\n";
my %wordVocab = ();
my %phoneVocab = ();
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/g or next;
  my ($word, $phones) = ($1, $2);
  if(! $trivial_merge) {
    if($word_lower_case) {
      $word = lc $word;
    } else {
      $word = uc $word;
    }
  }
  if(! $trivial_merge) {
    if($phone_upper_case) {
      $phones = uc $phones;
    } else {
      $phones = lc $phones;
    }
  }
  my @A = split(/\s+/, $phones);
  $phones = join(" ", @A);
  my $pron =sprintf("%s\t%s", $word, $phones);
  if(not exists $wordVocab{$pron}) {
    print LEX "$pron\n";
    $wordVocab{$pron} ++;
  }
  for(my $i = 0; $i < @A; $i ++) {
    if(not exists $phoneVocab{$A[$i]}) {
      print PHN "$A[$i]\n";
      $phoneVocab{$A[$i]} ++;
    }
  }
}
print STDERR "## LOG ($0): stdin ended\n";
close PHN;
close LEX;
