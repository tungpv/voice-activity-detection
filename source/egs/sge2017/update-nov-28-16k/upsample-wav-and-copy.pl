#!/usr/bin/perl -w
use strict;

while(<STDIN>) {
  chomp;
  printf("%s %s \n", "$_", "/usr/bin/sox -r 8000 - -r 16000 -c 1 -b 16 -t wav - |");
}
