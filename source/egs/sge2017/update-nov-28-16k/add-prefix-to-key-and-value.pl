#!/usr/bin/perl -w 
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\nExample: cat segments | $0 1- > new-segments\n\n";
}
my $prefix = shift @ARGV;
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  s/(\S+)\s+(\S+)(.*)/$prefix$1 $prefix$2 $3\n/g;
  print;
}
print STDERR "## LOG ($0): stdin ended\n";
