#!/usr/bin/perl -w
use strict;
my %vocab = ();
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my @A = split(/\s+/, $2);
  for(my $i = 0; $i < scalar @A; $i ++) {
    if(not exists $vocab{$A[$i]}) {
      print "$A[$i]\n";
      $vocab{$A[$i]} ++;
    }
  }
}
