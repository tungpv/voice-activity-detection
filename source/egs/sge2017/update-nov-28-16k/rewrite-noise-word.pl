#!/usr/bin/perl -w
use strict;
while(<STDIN>) {
  chomp;
  s/\[laughter\]/<v-noise>/g;
  s/\[noise\]/<noise>/g;
  s/\[vocalized-noise\]/<v-noise>/g;
  print "$_\n";
}
