#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
my $one_to_many = 0;
my $map_file = "";
my $phone_prefix = "";
GetOptions("one-to-many|one_to_many" => \$one_to_many,
           "map_file|map-file=s" => \$map_file,
           "phone_prefix|phone-prefix=s" => \$phone_prefix) or
die "## ERROR ($0): GetOptions failed\n";



# begin sub
sub LoadMap {
  my ($sFile, $vocab) = @_;
  open(F, "$sFile") or die "## ERROR ($0): cannot open $sFile \n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    $$vocab{$1} = $2;
  }
  close F;
}
sub Map {
  my ($refA, $map) = @_;
  my @A = ();
  for(my $i = 0; $i < scalar @$refA; $i++) {
    my $phone = $$refA[$i];
    if(exists $$map{$phone}) {
      $phone = $$map{$phone};
    }
    push @A, $phone;
  }
  @$refA = @A;
}
sub OneToManyMap {
  my ($refA, $map) = @_;
  my @A = ();
  for(my $i = 0; $i < scalar @$refA; $i++) {
    my $phone = $$refA[$i];
    die "## ERROR (OneToManyMap, ", __LINE__, "): unknown phone $phone\n"
    if not exists $$map{$phone};
    my $phones = $$map{$phone};
    my @B = split(/\s+/, $phones);
    for(my $j = 0; $j < @B; $j ++) {
      push(@A, $B[$j]);
    }
  }
 @$refA = @A;
}
# end sub
my %map = ();
if($map_file ne "") {
  LoadMap($map_file, \%map);
}
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($word, $phones) = ($1, $2);
  my @A = split(/\s+/, $phones);
  if ($map_file ne "") {
    if(! $one_to_many) {
      Map(\@A, \%map);
    } else {
      OneToManyMap(\@A, \%map);
    }
  }
  if($phone_prefix ne "") {
    for(my $i = 0; $i < scalar @A; $i ++) {
      $A[$i] = sprintf("%s%s", $A[$i], $phone_prefix);
    }
  }
  $phones = join(" ", @A);
  print "$word\t$phones\n";
}
print STDERR "## LOG ($0): stdin ended\n";
