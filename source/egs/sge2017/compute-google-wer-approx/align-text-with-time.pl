#!/usr/bin/perl -w

use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;

if ($numArgs != 2) {
  die "\nExample: cat hyp.stm | $0 ref.stm dir\n\n";
}
my ($refStmFile, $tgtdir) = @ARGV;

# begin sub
sub InsertItem {
  my ($aref, $start, $end, $text) = @_;
  my @A = ();
  push @$aref, \@A;
  my $len = scalar @$aref;
  my $array = @$aref[$len-1];
  push @$array, $start;
  push @$array, $end;
  push @$array, $text;
}
sub GetStmLine {
  my ($array, $s) = @_;
  @$array = ();
  $s =~ m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(.*)/ or die "## ERROR (GetStmLine): bad line $s\n";
  push @$array, $1;
  push @$array, $2;
  push @$array, $3;
  push @$array, $4;
  push @$array, $5;
  push @$array, $6;
}
sub LoadStmFile {
  my ($href, $sFile) = @_;
  open(F, "$sFile") or die "## ERROR (LoadStmFile, ", __LINE__, "): cannot open $sFile\n";
  while(<F>) {
    chomp;
    my @array = ();
    GetStmLine(\@array, $_);
    my ($fileName, $channel, $speaker, $start, $end, $text) = @array;
    if(not defined $href->{file}) {
      $href->{file} = $fileName;
    }
    if(not defined $href->{content}) {
      my @A = ();
      $href->{content} = \@A;
    }
    InsertItem($href->{content}, $start, $end, $text);
  }
  close F;
}
sub AlignText {
  my ($tolerance, $array, $href, $hypText, $refText, $next_align_position) = @_;
  my $refArray = $href->{content};
 
  for(my $i = $$next_align_position; $i < scalar @$refArray; $i ++) {
    my $stmArray = $$refArray[$i];
    if(abs($$array[1] - $$stmArray[1]) <= $tolerance) {
       $$hypText .=  " $$array[2]";
       $$refText .= " $$stmArray[2]";
       $$next_align_position = $i + 1;
       return 1;
    }
    if($$array[1] > $$stmArray[1]) {
      $$refText .= " $$stmArray[2]";
      $$next_align_position = $i + 1;
    } else {
      $$hypText .= " $$array[2]";
      return 0;
    }
  }
  return 0;
}
sub DumpArray {
  my ($array) = @_;
  my $concat = "";
  for(my $i = 0; $i <scalar @$array; $i ++) {
    $concat .= "$$array[$i] ";
  }
  print STDERR "$concat\n";
}
sub DumpStm {
  my ($array) = @_;
  my $concat = "";
  for(my $i = 0; $i < scalar @$array; $i ++) {
    my $uttArray = $$array[$i];
    DumpArray($uttArray);
  }
}
# end sub
my %refStmVocab = ();
LoadStmFile(\%refStmVocab, $refStmFile);
my $href = \%refStmVocab;
# DumpStm($href->{content});
print STDERR "## LOG ($0): stdin expected\n";
open (REF, ">$tgtdir/ref.txt") or die "## ERROR: cannot open $tgtdir/ref.txt\n";
open (HYP, ">$tgtdir/hyp.txt") or die "## ERROR: cannot open $tgtdir/hyp.txt\n";
my $index = 0;
my $refText = "";
my $hypText = "";
my $tolerance = 0.2;
my $next_align_position = 0;
while(<STDIN>) {
  chomp;
  my @array = ();
  GetStmLine(\@array, $_);
  my @subArray =($array[3], $array[4], $array[5]);
  # DumpArray(\@array);
  die "## ERROR ($0): file $array[0] is different from $refStmVocab{file}\n" if $array[0] ne $refStmVocab{file};
  if(AlignText($tolerance, \@subArray, \%refStmVocab, \$hypText, \$refText, \$next_align_position) == 1) {
    $index ++;
    my $segName = sprintf("%s-%04d", $array[0], $index);
    print REF "$segName $refText\n";
    print HYP "$segName $hypText\n";
    $hypText = "";
    $refText = "";
    next;
  }
}
print STDERR "## LOG ($0): stdin ended\n";
