#!/bin/bash 

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
cmu_dict=wsj/prepare2/dict/lexicon.txt
devtext="/home2/hhx502/sge2017/data/dev-seame.28/8k/text /home2/hhx502/sge2017/data/fstd-demo-updated/8k/fbank-pitch/text"
traintext="/home2/hhx502/sge2017/data/train-merge70h/8k/text sg-en-i2r/data/train/text"
# end options


function Usage {
 cat <<EOF
 
 $0 [options] <train_text> <lexicon> <tgtdir>
 [options]:
 [examples]:

 $0 --steps 1 --cmu-dict $cmu_dict --traintext "$traintext" --devtext "$devtext" \
 /home2/hhx502/sge2017/data/local/web-nov-08/text/raw-text-2 \
 /home2/hhx502/sge2017/data/update-oct-27-2016/dict  /home2/hhx502/sge2017/data/update-lm-nov-09-2016

EOF
}

. parse_options.sh || exit 1

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

text=$1
srcdict=$2
tgtdir=$3

vocabdir=$tgtdir/vocab

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $vocabdir ] || mkdir -p $vocabdir
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): prepare vocabulary for g2p and lm building"
  if [ ! -z $cmu_dict ]; then
    cat $cmu_dict | \
    source/egs/sge2017/local/build-lm/normalize-cmudict-phone.pl | \
    source/egs/sge2017/local/build-lm/split-lexicon.pl $srcdict/nonsilence_phones.txt \
    $vocabdir/oov-lex.txt $vocabdir/inv-lex.txt  $vocabdir/oov-phones.txt
    cat $vocabdir/inv-lex.txt $srcdict/lexicon.txt | sort -u \
    > $vocabdir/lexicon.txt
   else
     cat $srcdict/lexicon.txt > $vocabdir/lexicon.txt
   fi
  echo "## LOG (step01, $0): done ('$vocabdir')"
fi
g2pdir=$tgtdir/g2p
if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): train g2p model @ `date`"
  source/egs/g2p/run-g2p.sh --steps 1,2,3,4  $vocabdir/lexicon.txt  $g2pdir
  echo "## LOG (step02, $0): g2p model training done @ `date`"
fi
oovdir=$tgtdir/out-of-vocab-words
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): get oov word list on target text"
  [ -d $oovdir ] || mkdir -p $oovdir
  cat $text | \
  source/egs/sge2017/local/build-lm/normalize-text.pl | \
  source/egs/sge2017/local/build-lm/print-oov.pl $vocabdir/lexicon.txt >$oovdir/word-count.txt
  cat $oovdir/word-count.txt  | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; m/(\S+)\s+(\d+)/; if(m/[^[:ascii:]]/){ } elsif(length($1)>3 && $2 > 10 && $2 !~ /@/ ) {print "$_\n";}'| grep -v '@' > $oovdir/word-count-thresh10.txt
  echo "## LOG (step03, $0): done ('$oovdir')"
fi

if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): select words from cmu dict"
  cat $text | \
  source/egs/sge2017/local/build-lm/normalize-text.pl | \
  source/egs/sge2017/local/build-lm/print-word-not-in-dict.pl $oovdir/word-count.txt  | \
  source/egs/sge2017/local/build-lm/print-word-not-in-dict.pl $srcdict/lexicon.txt | \
  source/egs/sge2017/local/build-lm/transcribe-word-with-prons.pl "cat $cmu_dict |source/egs/sge2017/local/build-lm/normalize-cmudict-phone.pl |"  > $oovdir/selected-lexicon.txt
  echo "## LOG (step04, $0): done ('$oovdir/selected-lexicon.txt')"  
fi
dict=$tgtdir/dict
[ -d $dict ] || mkdir -p $dict
if [ ! -z $step05 ]; then
  echo "## LOG (step05, $0): prepare lexicon"
  cat $srcdict/lexicon.txt $oovdir/selected-lexicon.txt | \
  sort -u > $dict/lexicon.txt
  cp $srcdict/{extra_questions.txt,nonsilence_phones.txt,silence_phones.txt,optional_silence.txt}  $dict/
  utils/validate_dict_dir.pl $dict
  echo "## LOG (step05, $0): done"
fi

lang=$tgtdir/lang
[ -d $lang ] || mkdir -p $lang
if [ ! -z $step06 ]; then
  echo "## LOG (step06, $0): prepare lang"
  utils/prepare_lang.sh $dict "<unk>" $lang/tmp $lang
  echo "## LOG (step06, $0): done "
fi

lmInDomain=$tgtdir/lm-indomain
[ -d $lmInDomain ] || mkdir -p $lmInDomain
if [ ! -z $step07 ]; then
  cat $devtext | \
  source/egs/sge2017/local/build-lm/dump-kaldi-text.pl | gzip -c > $lmInDomain/dev-text.gz
  cat $traintext | \
  source/egs/sge2017/local/build-lm/dump-kaldi-text.pl | gzip -c > $lmInDomain/train-text.gz
  cat $dict/lexicon.txt |\
  source/egs/sge2017/local/build-lm/print-word-list.pl | gzip -c > $lmInDomain/vocab.gz
  source/egs/sge2017/local/build-lm/train-srilm-v2.sh --steps 1,2 --lm-order-range "3 3" \
  --cutoff-csl "3,011,012"  $lmInDomain || exit 1
fi
lmOutDomain=$tgtdir/lm-outdomain
devNum=10000
[ -d $lmOutDomain ] || mkdir -p $lmOutDomain
if [ ! -z $step08 ]; then
  echo "## LOG (step08, $0): prepare data to train lm"
  cat $text | \
  source/egs/sge2017/local/build-lm/normalize-text.pl | \
  utils/shuffle_list.pl | head -$devNum | gzip -c > $lmOutDomain/dev-text.gz
  cat $text | \
  source/egs/sge2017/local/build-lm/normalize-text.pl | \
  utils/shuffle_list.pl | tail -n +$[devNum+1] | \
  gzip -c > $lmOutDomain/train-text.gz
  cat $dict/lexicon.txt |\
  source/egs/sge2017/local/build-lm/print-word-list.pl | gzip -c > $lmOutDomain/vocab.gz
  echo "## LOG (step08, $0): done ('$lmOutDomain')"
fi

if [ ! -z $step09 ]; then
  echo "## LOG (step09, $0): train lm"
  source/egs/sge2017/local/build-lm/train-srilm-v2.sh --steps 1,2 --lm-order-range "3 3" \
  --cutoff-csl "3,011,012"  $lmOutDomain || exit 1
  echo "## LOG (step09, $0): done ('$lmOutDomain')"
fi
intLm=$tgtdir/lm-int
if [ ! -z $step10 ]; then
  echo "## LOG (step10, $0): lm interpolation"
  source/egs/sge2017/local/build-lm/lm-interpolate.sh 3 $lmInDomain/lm.gz $lmOutDomain/lm.gz $lmInDomain/dev-text.gz \
  $intLm || exit 1
  echo "## LOG (step10, $0): interpolation done "
fi
lang_test=$tgtdir/lang_test
if [ ! -z $step11 ]; then
  echo "## LOG (step10, $0): build G @ `date`"
  [ -e $intLm/lm.gz ] || \
  { echo "## ERROR (step11, $0): $inLm/lm.gz is not ready !" && exit 1; }
  source/egs/sge2017/local/build-lm/arpa2G.sh $intLm/lm.gz $lang  $lang_test
  echo "## LOG (step10, $0): done ('$lang_test') @ `date`"
fi
