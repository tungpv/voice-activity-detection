#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
# end options

. parse_options.sh 

function Usage {
 cat <<EOF
 Usage: $0 <sdata>  <tgtdir>  
 [examples]:
 
 $0 --steps 1  /data/users/tungpham/Add_reverb/Haihua_Singapore_English/output_crawledText/raw_xml \
 /home2/hhx502/sge2017/data/local/web-nov-08

EOF
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi
sdata=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

textdir=$tgtdir/text
[ -d $textdir ] || mkdir -p $textdir
export PYTHONIOENCODING=UTF-8
if [ ! -z $step01 ]; then
 (
  LC_ALL=
 for f in $(ls $sdata/*.xml); do
   echo "## LOG ($0): process file $f ..."
   source/egs/sge2017/local/xml-parser.py -i $f -o -
 done  > $textdir/raw-text-1
 )
 echo "## LOG (step01, $0): done raw text generation ($textdir)"
fi

if [ ! -z $step02 ]; then
  cat $textdir/raw-text-1 | \
  source/egs/sge2017/local/normalize-text/word-count.pl  > $textdir/word-count-1.txt
  
fi

if [ ! -z $step03 ]; then
  cat $textdir/raw-text-1 | \
  source/egs/sge2017/local/normalize-text/normalize-text.pl | \
  source/egs/sge2017/local/normalize-text/word-count.pl  > $textdir/word-count-2.txt 
fi

if [ ! -z $step04 ]; then
 ( 
  LC_ALL=
  cat $textdir/raw-text-1 | \
  source/egs/sge2017/local/normalize-text/normalize-text.pl | \
  source/egs/sge2017/local/normalize-text/new-normalize.py > $textdir/raw-text-2
  )
  # source/egs/sge2017/local/normalize-text/word-count.pl  > $textdir/word-count-3.txt 
fi
