#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
# end options

. parse_options.sh || exit 1


if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi


if [ ! -z $step01 ]; then
  sdata1=/home2/hhx502/sge2017/data/cts16k/train291/fbank-pitch
  sdata2=/home2/hhx502/sge2017/data/train-mix140/16k/fbank-pitch

  data=/home2/hhx502/sge2017/data/train-mix431/fbank-pitch

  utils/combine_data.sh $data $sdata1 $sdata2
  utils/fix_data_dir.sh $data

  source/data-len.sh $data

fi

if [ ! -z $step02 ]; then
  sdata1=/home2/hhx502/sge2017/data/cts16k/train291/mfcc-pitch
  sdata2=/home2/hhx502/sge2017/data/train-mix140/16k/mfcc-pitch

  data=/home2/hhx502/sge2017/data/train-mix431/mfcc-pitch

  utils/combine_data.sh $data $sdata1 $sdata2
  utils/fix_data_dir.sh $data

  source/data-len.sh $data


fi

if [ ! -z $step03 ]; then
  sdata1=/home2/hhx502/sge2017/data/cts8k/train291/fbank-pitch
  sdata2=/home2/hhx502/sge2017/data/train-mix140/8k/fbank-pitch
  data=/home2/hhx502/sge2017/data/train-mix431/8k/fbank-pitch
  utils/combine_data.sh $data $sdata1 $sdata2
  utils/fix_data_dir.sh $data

  source/data-len.sh $data
fi

if [ ! -z $step04 ]; then
  sdata1=
  sdata2=
  data=

fi
