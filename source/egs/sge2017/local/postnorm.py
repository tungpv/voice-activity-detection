#!/cm/shared/apps/python3.5.2/bin/python3.5
# # /cm/shared/apps/python3.5.2/bin/python3.5
# -*- coding:utf-8 -*-
'''
In boilerpipe/extract/__init__.py
1. Change user-agent to: - "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17"
2. Change getHTML() function to "return self.data"
'''
from __future__ import print_function
# from guess_language import guessLanguage
from boilerpipe.extract import Extractor
import pdb, os, sys, re, lxml.html, json, urllib, argparse
import normalize, db, time
import xml.etree.ElementTree as ET
from xml_handler_2 import *
from num2words import num2words
from normalize import *
import codecs

#inputs
#num				- number of links per page to display  (default: 10)
#lang				- language (default: en)
#sortby				- sort link by relevance or date (default: relevance)
#sourceList.json	- source query pattern
#keyword.list		- list of keywords
#normalize			- true or false

##Org: /usr/local/bin/python3.5
if __name__=='__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-i","--inputWebpage", help="input file containg keywords (one keyword per line)", required=True)
	parser.add_argument("-o","--outputFile", help="output folder for saving search results", required=True)
	args = parser.parse_args()

	# out_writer		= open(args.outputFile,'w')
	tree	= ET.parse(args.inputWebpage)
	root	= tree.getroot()
	totalWord = 0
        for doc in root.iter('doc'):
                rawText = doc.text
		#print ("*************Raw text = " + rawText)
                text = rawText # cleanText(rawText, '')
                for line in text:
                        numWord = len(line.split())	
                        totalWord = totalWord + numWord	
                        print(text.encode('utf8').str())
		# out_writer.write(text.encode('utf8'))
			
	# print ("Finish! Number of word = " + str(totalWord))
	# out_writer.close()
