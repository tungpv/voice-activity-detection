#!/usr/bin/perl -w

use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;

my $trans_vocab   = '';
GetOptions ("trans-vocab|trans_vocab=s" =>\$trans_vocab);

print STDERR "## LOG ($0, ", __LINE__, "): trans_vocab=$trans_vocab (use --trans-vocab=xxx or --trans_vocab=xxx to load trans dict)\n";
# begin sub
sub LoadTransVocab {
  my ($sFile, $vocab) = @_;
  return if(length($sFile) == 0);
  open(F, "$sFile") or die "## ERROR ($0, ", __LINE__, "): cannot read file $sFile\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    $$vocab{$1} = $2;
  }
  close F;
}
sub Replace {
  my ($s) = @_;
  $$s =~ s/’/\'/g;
  $$s =~ s/dolloars/dollar/g;
  $$s =~ s/thousand,/thousand/g;
}
sub TransUtterance {
  my ($s, $vocab) = @_;
  my @A = split(/\s+/, $$s);
  my $utt = "";
  for(my $i = 0; $i < @A; $i ++) {
    my $word = $A[$i];
    if (exists $$vocab{$word}) {
      $word = $$vocab{$word};
    }
    $utt .= "$word ";
  }
  $utt =~ s/ $//g;
  $$s = $utt;
}
# end sub
my $doTrans = 0;
$doTrans = 1 if ($trans_vocab ne "");
my %transVocab = ();
LoadTransVocab($trans_vocab, \%transVocab);
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  Replace(\$_);
  if($doTrans == 1) {
    TransUtterance(\$_, \%transVocab);
  }
  print "$_\n";
}
print STDERR "## LOG ($0): stdin ended\n";
