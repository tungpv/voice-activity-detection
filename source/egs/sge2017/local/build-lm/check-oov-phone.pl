#!/usr/bin/perl

use warnings;
use strict;

use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;

if ($numArgs != 3) {
  die "\nExample: cat lexicon.txt | $0 phones.txt lexicon-has-oov-phone-word.txt oov-phones.txt\n\n";
}

my ($phonesFile, $tgtLexFile, $oovPhoneFile) = @ARGV;

# begin sub
sub InitVocab {
    my ($lexFile, $vocab) = @_;
}
# end sub 
