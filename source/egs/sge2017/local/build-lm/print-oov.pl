#!/usr/bin/perl -w
use strict;
use utf8;
use open qw (:std :utf8);

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\nUsage: cat text | $0 lexicon.txt > oov-word-count.txt\n\n";
}
my ($lexFile) = @ARGV;

# begin sub
sub InitWordVocab {
  my ($lexFile, $vocab) = @_;
  open (F, "$lexFile") or die "## ERROR (", __LINE__, "): cannot read $lexFile\n";
  while(<F>) {
    chomp;
    /(\S+)\s+(.*)/ or next;
    $$vocab{$1} ++;
  }
  close F;
}
# end sub

my %wordVocab = ();
InitWordVocab($lexFile, \%wordVocab);

print STDERR "## LOG (", __LINE__, "): stdin expected\n";
my %oovVocab = ();
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  for(my $i = 0; $i < @A; $i ++) {
    my $word = $A[$i];
    if(not exists $wordVocab{$word} && length($word) > 0) {
      $oovVocab{$word} ++;
    }
  }
}
print STDERR "## LOG (", __LINE__, "): stdin ended\n";

foreach my $word (sort {$oovVocab{$b} <=> $oovVocab{$a}} keys%oovVocab) {
  print "$word $oovVocab{$word}\n";
}
