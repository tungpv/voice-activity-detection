#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  die "\nExample: cat wordlist.txt | $0 lexicon.txt > transcribed-lexicon.txt\n\n";
}
my ($lexFile) = @ARGV;

# begin sub
sub InitLexVocab {
  my ($lexFile, $vocab) = @_;
  open(F, "$lexFile") or die "## ERROR (InitLexVocab, ", __LINE__, "): cannot read $lexFile\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    my ($word, $pron) = ($1, $2);
    $word = lc $word;
    my @A = split(/\s+/, $pron);
    $pron = join(" ", @A);
    my $line = sprintf("%s\t%s", $word, $pron);
    my $pronRef;
    if(exists $$vocab{$word}) {
      $pronRef = $$vocab{$word};
    } else {
      my %v = ();
      $$vocab{$word} = \%v;
      $pronRef = $$vocab{$word};
    }
    $$pronRef{$line} ++;
  }
  close F;
}
sub DumpWordPron {
  my ($vocab) = @_;
  foreach my $line (keys%$vocab) {
    print "$line\n";
  }
}
# end sub
my %wordVocab = ();
InitLexVocab($lexFile, \%wordVocab);

print STDERR "## LOG ($0): stdin expected\n";
while (<STDIN>) {
  chomp;
  m/(\S+)/ or next;
  my $word = $1;
  if(exists $wordVocab{$word}) {
    DumpWordPron($wordVocab{$word});
  } else {
    print STDERR "## LOG ($0): oov word: $word\n";
  }
}
print STDERR "## LOG ($0): stdin ended\n";
