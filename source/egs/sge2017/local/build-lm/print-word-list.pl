#!/usr/bin/perl -w 
use strict;
use utf8;
use open qw(:std :utf8);

print STDERR "## LOG ($0): stdin expected\n";
my %vocab = ();
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  if(not exists $vocab{$1}) {
    $vocab{$1} ++;
    print $1, "\n";
  }
}
print STDERR "## LOG ($0): stdin ended\n";
