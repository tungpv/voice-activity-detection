#!/usr/bin/perl -w 

use strict;
use utf8;
use open qw(:std :utf8);

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  my $word = lc shift @A;
  for(my $i = 0; $i < @A; $i ++) {
    my $phone = \$A[$i];
    $$phone =~ s/(\S+)(\d+)$/$1/g;
  }
  my $pron = join(" ", @A);
  print "$word\t$pron\n";
}
print STDERR "## LOG ($0): stdin ended\n";
