#!/bin/bash

. path.sh
. cmd.sh

echo 
echo "## LOG: $0 $@"
echo 
# begin options
steps=
vocab=
train_text=
dev_text=
lm_order_range="4 7"
cutoff_csl="4,0111,0112,0122,0123:5,01122,01113,01223:6,011222,011223:7,0111222,0111223,0112233"
# end options
function Usage {
 cat<<END

 Usage $0 [options]  <tgtdir>
 [options]:
 --steps                                        # value, "$steps"
 --vocab                                        # value, "$vocab"
 --train-text                                   # value, "$train_text"
 --dev-text                                     # value, "$dev_text"
 --lm-order-range                               # value, "$lm_order_range"
 --cutoff-csl                                   # value, "$cutoff_csl"
 [steps]:
 [examples]:

 $0 --steps 1 --kaldi-words-text true --train-kaldi-text true --dev-kaldi-text true \
 --cutoff-csl "$cutoff_csl" /home2/hhx502/kws2016/georgian/data/lang/words.txt \
 /home2/hhx502/kws2016/georgian/data/train/text \
 /home2/hhx502/kws2016/georgian/data/dev/text test/train-ngram-test

 $0  --steps 1,2 --vocab 

END
}

. parse_options.sh || exit 1

if [ $# -ne 1 ]; then
  Usage && exit 1
fi

tgtdir=$1

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
[ -z $vocab ] && vocab=$tgtdir/vocab.gz
[ -z $train_text ] && train_text=$tgtdir/train-text.gz
[ -z $dev_text ] && dev_text=$tgtdir/dev-text.gz

[ -d $tgtdir ] || mkdir -p $tgtdir
for x in $vocab $train_text $dev_text; do
  [ -f $x ] || { echo "## ERROR: file $x is not ready !" && exit 1;  }
done

if [ ! -z $step01 ]; then
  echo "## step01, train ngram lm @ `date`"
  for order in $(seq $lm_order_range); do
     for cutoff in $( perl -e '($cutoff_csl, $order) = @ARGV; @A = split(/:/, $cutoff_csl); 
     for($i=0; $i<@A; $i++) { $order_csl = $A[$i]; @B = split(/[,;]/, $order_csl); $cur_order = shift @B;
       if($cur_order == $order) { print join(" ", @B);  }
     } ' "$cutoff_csl" $order); do
       gt_train_opts="$(perl -e ' ($dir, $order, $cutoff_str) = @ARGV; @A = split(//, $cutoff_str); die "## ERROR: bad cutoff_str $cutoff_str\n" if($order != scalar @A); $lmfile = sprintf("%s/%dgram.gt%s.gz", $dir, $order, $cutoff_str);  $opts = ""; for($i=0; $i < @A; $i++){ $x=$i+1; $switch = sprintf("-gt%dmin %d", $x, $A[$i]); $opts .= "$switch ";   } print "-lm $lmfile $opts -order $order";  ' $tgtdir $order $cutoff)"
       [ $? -ne 0 ] && { echo "## ERROR: failed to make gt_train_opts" && exit 1; }
       kn_train_opts="$(perl -e ' ($dir, $order, $cutoff_str) = @ARGV; @A = split(//, $cutoff_str); die "## ERROR: bad cutoff_str $cutoff_str\n" if($order != scalar @A); $lmfile = sprintf("%s/%dgram.kn%s.gz", $dir, $order, $cutoff_str);  $opts = ""; for($i=0; $i < @A; $i++){ $x=$i+1; $switch = sprintf("-kndiscount%d -gt%dmin %d", $x, $x, $A[$i]); $opts .= "$switch ";   } print "-lm $lmfile $opts -order $order";  ' $tgtdir $order $cutoff)"
       [ $? -ne 0 ] && { echo "## ERROR: failed to make kn_train_opts" && exit 1; }
       echo "## LOG: train with opts='$gt_train_opts' started @ `date`"
       ngram-count $gt_train_opts -text $tgtdir/train-text.gz  -vocab $tgtdir/vocab.gz -unk -sort
       echo "## LOG: done with opts='$gt_train_opts' @ `date`"
       ngram-count $kn_train_opts -text $tgtdir/train-text.gz -vocab $tgtdir/vocab.gz -unk -sort
       echo "## LOG: done with opts='$kn_train_opts' @ `date`"
     done
  done
  echo "## step01, done @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "## step02, computing perplexity"
  ( for order in $(seq $lm_order_range); do
      for f in $tgtdir/${order}gram*; do
        ( echo $f; ngram -order $order -lm $f -unk -ppl $tgtdir/dev-text.gz ) | paste -s -d ' '
      done   
    done ) | sort  -r -n -k 13 | column -t | tee $tgtdir/perplexities.txt
  echo "## step02, done "
fi

if [ -e $tgtdir/perplexities.txt ]; then
  lmfilename=$(head -1 $tgtdir/perplexities.txt | cut -f1 -d' ')
  echo "lmfilename=$lmfilename"
  if [ ! -z $lmfilename ]; then 
    ( cd $tgtdir; [ -f lm.gz ] && unlink lm.gz;  ln -sf $(basename $lmfilename) lm.gz  ) 
  fi
fi

if [ ! -z $step03 ]; then
  ngram-count -order $order -wbdiscount -vocab $tgtdir/vocab.gz -unk -sort -text $tgtdir/train-text.gz -lm $tgtdir/lm-wb${order}.gz
fi
