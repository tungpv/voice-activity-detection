#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  m/(\S+)\s+(.*)/ or next;
  my $text = lc $2;
  print "$text\n";
}
print STDERR "## LOG ($0): stdin ended\n";
