#!/bin/bash 

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh 

# begin options

# end options

. parse_options.sh || exit 

function Usage {
 cat<<EOF
 
 Usage: $0 [options] <lm_order> <indomain-lmfile> <outdomain-lmfile> <devtext> <tgtdir>
 [examples]:
 
 $0 3 /home2/hhx502/sge2017/data/update-lm-nov-09-2016/lm-indomain/lm.gz \
 /home2/hhx502/sge2017/data/update-lm-nov-09-2016/lm-outdomain/lm.gz \
 /home2/hhx502/sge2017/data/update-lm-nov-09-2016/lm-indomain/dev-text.gz\
 /home2/hhx502/sge2017/data/update-lm-nov-09-2016/lm-int

EOF
}

if [ $# -ne 5 ]; then
  Usage && exit 1
fi
lm_order=$1
inLmFile=$2
outLmFile=$3
devtext=$4
tgtdir=$5

for x in $inLmFile $outLmFile $devtext; do
  [ -f $x ] || { echo "## ERROR ($0): $x does not exist !" && exit 1; }
done
[ -d $tgtdir ] || mkdir -p $tgtdir
tmpdir=$(mktemp -d -p $tgtdir)
  trap  "echo removing $tmpdir ...; rm -rf $tmpdir " EXIT
best_ppl=10000000
best_lm=
for ifactor in $(seq  0 0.1 0.6); do
  echo "interpolating, factor=$ifactor ..."
  tlm=$tmpdir/lm.int.${ifactor}.gz
  ngram -order $lm_order -lm  $outLmFile  -lambda $ifactor \
  -mix-lm $inLmFile  -write-lm $tlm
  ppl=$(ngram -order $lm_order -lm $tlm -unk -ppl $devtext | paste -s -d ' '| tee -a $tgtdir/perplexities | awk '{print $14;}')
  echo "## LOG ($0): Current PPL, $ppl"
  [ -z $ppl ] &&  { echo "## ERROR ($0): lm $tlm ppl is problematic "; exit 1; }
  if [ $(echo "$ppl < $best_ppl"| bc -l) -eq 1 ]; then
    best_lm=$tlm
    best_ppl=$ppl
  fi
done 
[ -z $best_lm ] && \
{ echo "ERROR, best_lm not found by interpolating"; exit 1; }
cp $best_lm $tgtdir/$(basename $best_lm)
( cd $tgtdir; [ -f lm.gz ] && unlink lm.gz; ln -sf $(basename $best_lm) lm.gz )

exit 0
