#!/usr/bin/perl

use warnings;
use strict;

use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;

if ($numArgs != 4) {
  die "\nExample: cat lexicon.txt | $0 phones.txt oov-lex.txt inv-lex.txt oov-phones.txt\n\n";
}

my ($phonesFile, $oovLexFile, $invLexFile, $oovPhoneFile) = @ARGV;

# begin sub
sub InitPhoneVocab {
  my ($lexFile, $vocab) = @_;
  open(F, "$lexFile") or
  die "## ERROR (InitPhoneVocab, ", __LINE__, "): can not open $lexFile\n";
  while (<F>) {
    chomp;
    m/(\S+)/ or next;
    $$vocab{$1} ++;
  }
  close F;
}
sub HasOovPhone {
  my ($vocab, $array, $oovPhoneVocab) = @_;
  my $oovNum = 0;
  for(my $i = 0; $i < @$array; $i ++) {
    my $phone = $$array[$i];
    if(not exists $$vocab{$phone}) {
      $oovNum ++;
      $$oovPhoneVocab{$phone} ++;
    }
  }
  return $oovNum;
}
# end sub
my %phoneVocab = ();
InitPhoneVocab($phonesFile, \%phoneVocab);
open(OOV, ">$oovLexFile") or die "## ERROR (", __LINE__, "): can not open $oovLexFile\n";
open(INV, ">$invLexFile") or die "## ERROR (", __LINE__, "): can not open $invLexFile\n";
open(PHN, "|sort >$oovPhoneFile") or die "## ERROR (", __LINE__, "): can not open $oovPhoneFile\n";
print STDERR "## LOG (", __LINE__, "): stdin expected\n";
my %oovPhoneVocab = ();
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($word, $pron) = ($1, $2);
  my @A = split(/\s+/, $pron);
  if(HasOovPhone(\%phoneVocab, \@A, \%oovPhoneVocab) == 0) {
    print INV "$_\n";
  } else {
    print OOV "$_\n";
  }
}
print STDERR "## LOG(", __LINE__, "): stdin ended\n";

foreach my $phone (keys%oovPhoneVocab) {
  print PHN "$phone\n";
}

close OOV;
close INV;
close PHN;
