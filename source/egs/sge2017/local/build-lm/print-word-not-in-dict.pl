#!/usr/bin/perl -w

use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  die "\nExample: cat text | $0 lexicon.txt > word-list.txt\n\n";
}
my ($dictFile) = @ARGV;
# begin options
sub InitWordVocab {
  my ($sFile, $vocab) = @_;
  open(F, "$sFile") or die "## ERROR (InitWordVocab, ", __LINE__, "): cannot read $sFile \n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    my $word = lc $1;
    $$vocab{$word} ++;
  }
  close F;
}
# end options
my %wordVocab = ();
InitWordVocab($dictFile, \%wordVocab);
print STDERR "## LOG ($0): stdin expected\n";
my %uniqueVocab = ();
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  for(my $i = 0; $i < @A; $i ++) {
    my $word = lc $A[$i];
    if (not (exists $wordVocab{$word} || exists $uniqueVocab{$word})) {
      print $word, "\n";
      $uniqueVocab{$word} ++;
    }
  }
}
print STDERR "## LOG ($0): stdin ended\n";
