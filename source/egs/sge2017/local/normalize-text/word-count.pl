#!/usr/bin/perl -w

use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;

my $from = 1;
GetOptions ('from=i' => \$from) || die;
$from --;
print STDERR "## LOG ($0): stdin expected\n";
my %vocab = ();
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  for(my $i = $from; $i < @A; $i ++) {
    my $w = $A[$i];
    if($w ne "") {
      $vocab{$w} ++;
    }
  }
}
foreach my $w (sort {$vocab{$b}<=>$vocab{$a}} keys%vocab) {
  print "$w $vocab{$w}\n";
}
print STDERR "## LOG ($0): stdin ended\n";
