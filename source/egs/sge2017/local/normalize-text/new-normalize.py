#!/cm/shared/apps/python3.5.2/bin/python3.5

from __future__ import print_function
from guess_language import *
from boilerpipe.extract import Extractor
import pdb, os, sys, re, lxml.html, json, urllib, argparse
import normalize, db, time
import xml.etree.ElementTree as ET
# from xml_handler_2 import *
from num2words import num2words
from normalize import *
import codecs
from optparse import OptionParser

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


if __name__ == '__main__':
    for text in sys.stdin:
        text = re.sub(' +', ' ', text)
        text = re.sub('(\d)\.(\d)', '\g<1> point \g<2>', text)
        text = re.sub('(\d),(\d)', '\g<1>\g<2>', text)
        text = re.sub('us\$(\d+)', '\g<1> u s dolloars', text)
        text = re.sub('s\$(\d+)', '\g<1> singapore dollars', text)
        text = re.sub('\$(\d+)', '\g<1> dollars', text) 
        text = re.sub('(\d+)\%', '\g<1> percent', text)
        text = re.sub('(\d+),(\d+)', '\g<1>\g<2>', text)
        text = re.sub('(\d+)(\D+)', '\g<1> \g<2>', text)
        text = re.sub('(\D+)(\d+)', '\g<1> \g<2>', text)
        text = re.sub('&', ' and ', text)
        text = re.sub('[\.,\-]+', ' ', text)
        text = re.sub('…', ' ', text)
        for word in text.split():
            isascii = lambda word: len(word) == len(word.encode())
            if(word.isdigit() and isascii):
                text = text.replace(word, num2words(int(word)), 1)
        text.rstrip()
        wordNum = len(text.split())
        if text and wordNum >= 2:
            text = re.sub('\-',' ', text)
            text = re.sub(' +', ' ', text)
            sys.stdout.write(text)
