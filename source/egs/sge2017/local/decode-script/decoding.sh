#!/bin/bash

#
. cmd.sh
. path.sh

# bash decoding.sh ../systems/SYS0\=LEX0+LM2+AM0/ 

# begin options
decodingdir=$lvcsrRootDir/decoding
echo $decodingdir
nj=4
decode_nj=1
num_threads=4
sysdir=$(readlink -f $1)
doScoring=0
stage=7	
use_gpu="no"

use_fbank_feature=false
fbank8k_config_opts="--window-type=hamming --use-energy=false --sample-frequency=8000 --low-freq=64 --high-freq=3800 --dither=1 --num-mel-bins=22 --htk-compat=true"
fbank16k_config_opts="--window-type=hamming --use-energy=false --sample-frequency=16000 --low-freq=64 --high-freq=8000 --dither=1 --num-mel-bins=40 --htk-compat=true"
graphdir=
fbank_nnet_dir=
data_sample_rate=8k

# end options

sysRootName=$(echo $(basename $sysdir)|cut -f1 -d"=")
. utils/parse_options.sh # accept options, very useful API.

if $use_fbank_feature; then
  for x in $fbank_nnet_dir/{final.nnet,final.mdl} $graphdir/HCLG.fst; do
  done
fi

for file in $(find $decodingdir -name "*.wav" -o -name "*.sph"); do
	echo $file
	fileRootName=$(basename $file .sph)
	fileRootName=$(basename $fileRootName .wav)
	fileRootName=$(basename $fileRootName .WAV)
	expedir=$sysdir/$fileRootName
	datadir=$sysdir/data/$fileRootName
	[ -d $datadir ] || mkdir -p $datadir
    
	if [ $stage -le 0 ]; then
		touch $datadir/stm
		if [ -e $lvcsrRootDir/decoding/$fileRootName.stm ]; then
			cp $lvcsrRootDir/decoding/$fileRootName.stm $datadir/stm
			doScoring=1
		fi
		
	fi

	if [ $stage -le 1 ]; then
		echo "running segment extraction"
		#Speaker Diarisation process wav or sphere based on ext

	java -Xmx2024m -jar $lvcsrRootDir/tools/lium_spkdiarization-8.4.1.jar  \
			--fInputMask=$file --sOutputMask=$datadir/$fileRootName.seg --doCEClustering  $fileRootName
	fi

	if [ $stage -le 2 ] ; then
		# Kaldi segments and spk2utt files
		awk '$1 !~ /^;;/ {print $1"-"$8"-"$3/100.0"-"($3+$4)/100.0" "$1" "$3/100.0" "($3+$4)/100.0}' \
		   	$datadir/$fileRootName.seg | sort -nk3 > $datadir/segments
		echo '------Printing segments contents------'
		awk '{split($1,a,"-"); print $1" "a[2]  }'  $datadir/segments > $datadir/utt2spk
		echo '------Printing utt2spk contents------'
		#cat $datadir/utt2spk
		echo here before utt2spk_to_spk2utt.pl	
		cat $datadir/utt2spk | utils/utt2spk_to_spk2utt.pl > $datadir/spk2utt
	
		(for tag in $(cut -f1 -d"-" $datadir/spk2utt | cut -f2 -d" "); do 
			if [ -e $lvcsrRootDir/decoding/$tag.sph ] ; then
				echo "$tag sph2pipe -f wav -p $lvcsrRootDir/decoding/$tag.sph |" 
			elif [ -e $lvcsrRootDir/decoding/$tag.wav ] ; then
				echo "$tag $lvcsrRootDir/decoding/$tag.wav"
			fi
		done) > $datadir/wav.scp
	

		echo here before validate_data_dir.sh
		cat $datadir/wav.scp | awk '{ print $1, $1, "1"; }' > $datadir/reco2file_and_channel
		touch $datadir/glm
		echo $datadir		
		#cat $datadir/utt2spk
		utils/validate_data_dir.sh --no-text --no-feats $datadir
		#cat $datadir/spk2utt
		utils/fix_data_dir.sh $datadir
	fi
	if [ $stage -le 3 ] && ! $use_fbank_feature; then
		mfccdir=$datadir/mfcc
		mkdir -p $mfccdir
		steps/make_mfcc.sh --nj $nj --cmd "$train_cmd" $datadir $datadir/log $mfccdir || exit 1
		steps/compute_cmvn_stats.sh $datadir $datadir/log $mfccdir || exit 1
	fi
        if $use_fbank_feature; then
          echo "## LOG ($0): make fbank+pitch started @ `date`"
          sdata=$datadir; data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch
          [ -d $data ] || mkdir -p $data
          cp $sdata/* $data
          if [ "$data_sample_rate" == "8k" ] || [ "$data_sample_rate" == "8K" ]; then
            opts="$fbank8k_config_opts"
            echo "--sample-frequency=8000" > $data/pitch.conf
          else
            [ "$data_sample_rate" != "16k" ] && [ "$data_sample_rate" != "16K" ] && \
            { echo "## ERROR ($0): unknown sampling rate $data_sample_rate"; exit 1; }
            opts="$fbank16k_config_opts"
            echo "--sample-frequency=16000" > $data/pitch.conf
          fi
          echo "opts" | perl -ane 'chomp; @A = split(/\s+/); 
            for($i = 0; $i < @A; $i++) {print "$A[$i]\n";}' > $data/fbank.conf
          steps/make_fbank_pitch --nj $nj --cmd "$train_cmd" \
            --fbank-config $data/fbank.conf --pitch-config  $data/pitch.conf \
            $data $feat/log $feat/data || exit 1
          steps/compute_cmvn_stats.sh $data $feat/log $feat/data || exit 1
          echo "## LOG ($0): done @ `date`"
        fi
	gmmdir=$sysdir/tri3
	transdir=$gmmdir/$fileRootName
	data_fmllr=$sysdir/data-fmllr-tri3/$fileRootName
	dnndir=$sysdir/dnn4_pretrain-dbn_dnn_smbr_i1lats

	if [ $stage -le 4 ] && ! $use_fbank_feature ; then
		if [ ! -f $transdir/trans.1 ]; then
			echo "run fmllr decode"
			steps/decode_fmllr.sh --nj $decode_nj --cmd "$decode_cmd"  --num-threads $num_threads --skip-scoring "true" \
			  	$gmmdir/graph $datadir $gmmdir/$fileRootName || exit 1
		fi
	fi

	if [ $stage -le 5 ] && ! $use_fbank_feature; then
		##############################################
   		# ****** fmllr features extraction ***********
		if [ ! -f $data_fmllr/feats.scp ]; then
			steps/nnet/make_fmllr_feats.sh --nj $decode_nj --cmd "$train_cmd"  \
				--transform-dir $gmmdir/$fileRootName $data_fmllr  \
				$datadir $gmmdir $data_fmllr/log $data_fmllr/data || exit 1
		fi

		#Deep Neural Network Acoustic Models are then applied on top of the fmllr features.
		acwt=0.1
		echo "========= smbr decoding ============"
		steps/nnet/decode.sh --nj $decode_nj --cmd "$decode_cmd" --config $lvcsrRootDir/scripts/conf/decode_dnn.config --acwt $acwt --use-gpu $use_gpu --stage 0 --skip-scoring "true" --num-threads $num_threads  --nnet $dnndir/4.nnet $gmmdir/graph $data_fmllr $dnndir/$fileRootName || exit 1;

	fi

	if [ $stage -le 6 ] && ! $use_fbank_feature; then
		echo $stage
		score_value=17
		scoring_opts="--min-lmwt $score_value --max-lmwt $score_value"
		latticedir=$gmmdir/$fileRootName
		if [ -e $dnndir/$fileRootName/lat.1.gz ]; then
			latticedir=$dnndir/$fileRootName
		fi
		steps/get_ctm.sh --cmd "$decode_cmd" $scoring_opts $datadir $gmmdir/graph $latticedir || exit 1;
		cp $latticedir/score_${score_value}/$fileRootName.ctm $decodingdir/$fileRootName.$sysRootName.ctm
		sort -nk3 $decodingdir/$fileRootName.$sysRootName.ctm -o $decodingdir/$fileRootName.$sysRootName.ctm
		if [[ doScoring==1 ]]; then
			echo doScoringhere
			#sclite -r $datadir/stm stm -h $decodingdir/$fileRootName.$sysRootName.ctm ctm -o sum pra
			#local/score.sh $scoring_opts --cmd "$cmd" $datadir $gmmdir/graph $latticedir || exit 1;
		fi

		perl $lvcsrRootDir/scripts/ctm2stm.pl $decodingdir/$fileRootName.$sysRootName.ctm  $datadir/segments $decodingdir/$fileRootName.$sysRootName.stm

	fi
        if $use_fbank_feature; then
          echo "## LOG ($0): decoding started @ `date`"
          data=$datadir/fbank-pitch
          latticedir=$fbank_nnet_dir/$fileRootName
          [ -e $data/feats.scp ] | \
          { echo "## ERROR ($0): feats.scp ('$data/feats.scp') is not ready !"; exit 1; }
          steps/nnet/decode.sh --nj $decode_nj --cmd "$decode_cmd" \
          --acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 \
          --skip-scoring true --num-threads $num_threads $graphdir \
          $data $latticedir || exit 1
          score_value=9
          echo "## LOG ($0): now generate ctm file @ `date`"
          scoring_opts="--min-lmwt 8 --max-lmwt 15"
          steps/get_ctm.sh --cmd "$decode_cmd" $scoring_opts $data $graphdir $latticedir || exit 1;
		cp $latticedir/score_${score_value}/$fileRootName.ctm $latticedir/$fileRootName.$sysRootName.ctm
		sort -nk3 $latticedir/$fileRootName.$sysRootName.ctm -o $decodingdir/$fileRootName.$sysRootName.ctm
          perl $lvcsrRootDir/scripts/ctm2stm.pl $decodingdir/$fileRootName.$sysRootName.ctm  $data/segments $decodingdir/$fileRootName.$sysRootName.stm
          echo "## LOG ($0): decoding done @ `date`"
        fi

cat $decodingdir/$fileRootName.$sysRootName.stm | awk 'var=1{$3="S1"; print $0}' > $decodingdir/tmp
mv $decodingdir/tmp $decodingdir/$fileRootName.$sysRootName.stm

#Insert ref/hyp words into stm file
# perl build_final_stm.pl $refSTM $.pra | sort -nk4 > $outputSTM
#perl $lvcsrRootDir/scripts/build_final_stm.pl $decodingdir/$fileRootName.stm $decodingdir/$fileRootName.$sysRootName.ctm.pra | sort -nk4 > $decodingdir/$fileRootName.$sysRootName.stm

#Prepare stm for manual transcribing format (ELAN and Text Grid)
perl $lvcsrRootDir/scripts/stm2TextGrid.1.2.pl $decodingdir/$fileRootName.$sysRootName.stm
perl $lvcsrRootDir/scripts/textgrid2csv.pl $decodingdir/$fileRootName.$sysRootName.TextGrid

done
echo "Success..."
