#!/bin/bash 

# begin options
steps=
# end options

. utils/parse_options.sh || exit 1

function Usage {
 cat<<EOF

  [Usage]: $0 <srcdata> <tgtdata>
  [Example]:

  $0 --steps 1 /home/hhx502/w2016/sg-en-i2r/data/train-merge    /home2/hhx502/sge2017/data/train-cts-rev291

EOF
}


if [ $# -ne 2 ]; then
  Usage && exit 1
fi

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi



srcdata=$1
tgtdata=$2


if [ ! -z $step01 ]; then
  sdata=$srcdata/mfcc-pitch
  data=$tgtdata/mfcc-pitch
  [ -d $data ] || mkdir -p $data
  cp $sdata/{wav.scp,segments,utt2spk,spk2utt,feats.scp,cmvn.scp} $data/ || \
  { echo "## ERROR (step01, $0): copy mfcc-pitch failed"; exit 1; }
  cat $sdata/text | source/egs/sg-en-i2r/lowercase-kaldi-text.pl > $data/text
  utils/fix_data_dir.sh $data
fi

if [ ! -z $step02 ]; then
  sdata=$srcdata/fbank-pitch
  data=$tgtdata/fbank-pitch
  [ -d $data ] || mkdir -p $data
  cp $sdata/{wav.scp,segments,utt2spk,spk2utt,feats.scp,cmvn.scp} $data/ || \
  { echo "## ERROR (step02, $0): copy fbank-pitch failed"; exit 1; }
  cat $sdata/text | source/egs/sg-en-i2r/lowercase-kaldi-text.pl > $data/text
  utils/fix_data_dir.sh $data
fi
sdata=/home2/hhx502/sge2017/data/cts8k/train291/fbank-pitch
data=/home2/hhx502/sge2017/data/cts16k/train291
if [ ! -z  $step03 ]; then
  [ -d $data ] || mkdir -p $data
  cp $sdata/{segments,utt2spk,spk2utt,text} $data
  cat $sdata/wav.scp | \
  source/egs/sge2017/local/upsample-cts-ref291-wav.pl > $data/wav.scp  
fi

sdata=/home2/hhx502/sge2017/data/cts8k/dev-spk20/mfcc-pitch
data=/home2/hhx502/sge2017/data/cts16k/dev-spk20
if [ ! -z $step04 ]; then
  [ -d $data ] || mkdir -p $data
  cp $sdata/{segments,utt2spk,spk2utt,text,stm,reco2file_and_channel} $data/
  cat $sdata/wav.scp | \
  source/egs/sge2017/local/upsample-cts-ref291-wav.pl > $data/wav.scp  
fi

if [ ! -z $step05 ]; then
 for sdata in /home2/hhx502/sge2017/data/cts16k/train291 \
     /home2/hhx502/sge2017/data/cts16k/dev-spk20; do
   data=$sdata/fbank-pitch
   feat=$sdata/feat/fbank-pitch
   source/egs/swahili/make_feats.sh --cmd "slurm.pl" --nj 20  --fbank-pitch true \
   --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
 done
fi

if [ ! -z $step06 ]; then
   for sdata in /home2/hhx502/sge2017/data/cts16k/train291 \
     /home2/hhx502/sge2017/data/cts16k/dev-spk20; do
   data=$sdata/mfcc-pitch
   feat=$sdata/feat/mfcc-pitch
   source/egs/swahili/make_feats.sh --cmd "slurm.pl" --nj 20  --mfcc-for-ivector true \
   --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc16k.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
 done
fi

if [ ! -z $step07 ]; then
   for sdata in   /home2/hhx502/sge2017/data/cts16k/dev-spk20; do
   data=$sdata/mfcc-pitch
   feat=$sdata/feat/mfcc-pitch
   source/egs/swahili/make_feats.sh --cmd "slurm.pl" --nj 20  --mfcc-for-ivector true \
   --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc16k.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
 done

fi
