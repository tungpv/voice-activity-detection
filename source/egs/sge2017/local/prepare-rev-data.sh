#!/bin/bash

. path.sh
. cmd.sh 

# begin options
steps=
cmd=slurm.pl
nj=40
# end options

. parse_options.sh || exit 

source_data=/data/users/tungpham/Add_reverb/Haihua_Singapore_English/out_dir
mirror_data=/home2/hhx502/sge2017/data/train-merge70h/16k

tgtdata=/home2/hhx502/sge2017/data/train-merge70h-rev/16k

function Usage {
 cat<<EOF
 Usage: $0 <source_data> <mirror_data> <tgtdata>
 [options]:
 [examples]:

 $0 --steps 3,4 --cmd "$cmd" --nj $nj $source_data $mirror_data $tgtdata

EOF
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi


tmpdir=$tgtdata/temp
[ -d $tmpdir ] || mkdir -p $tmpdir

if [ ! -z $step01 ]; then
  wavlist=$tmpdir/wavlist.txt 
  [ -f $wavlist ] || { 
    find $source_data -name "*.wav" > $wavlist;
    num_wav=$(wc -l < $wavlist); 
    echo "## LOG ($0): number of waves is $num_wav ($tmpdir)"; 
  }

  cat $mirror_data/wav.scp | \
  source/egs/sge2017/local/make_wav_scp.pl $wavlist > $tgtdata/wav.scp

# cp $mirror_data/{utt2spk,spk2utt,text,segments}   $tgtdata
  cat $mirror_data/text | \
  perl -ane 'chomp; m/(\S+)\s+(.*)/ or next; print "rev-$1 $2\n";' > $tgtdata/text
  cat $mirror_data/utt2spk | \
  perl -ane 'chomp; m/(\S+)\s+(.*)/ or next; print "rev-$1 ref-$2\n";' > $tgtdata/utt2spk
  utils/utt2spk_to_spk2utt.pl < $tgtdata/utt2spk > $tgtdata/spk2utt
  cat $mirror_data/segments | \
  perl -ane 'chomp; m/(\S+)\s+(\S+)\s+(.*)/ or next; print "rev-$1 rev-$2 $3\n";' > $tgtdata/segments
  utils/fix_data_dir.sh $tgtdata
fi
data16=/home2/hhx502/sge2017/data/train-mix140/16k
data8=/home2/hhx502/sge2017/data/train-mix140/8k
if [ ! -z $step02 ]; then
  utils/combine_data.sh $data16 $mirror_data $tgtdata
  utils/fix_data_dir.sh $tgtdata
  utils/combine_data.sh $data8 $mirror_data/../8k $tgtdata/../8k
  utils/fix_data_dir.sh $data8
fi

if [ ! -z $step03 ]; then
  echo "## LOG （step03, $0): mfcc-pitch, fbank-pitch extraction @ `date`"
  for sdata in /home2/hhx502/sge2017/data/dev-seame.28/16k /home2/hhx502/sge2017/data/train-mix140/16k; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    utils/fix_data_dir.sh $sdata
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc16k.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
  done
  echo "## LOG (step03, $0): done @ `date`"
fi

if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): fbank-pitch extraction @ `date`"
  for sdata in /home2/hhx502/sge2017/data/dev-seame.28/8k /home2/hhx502/sge2017/data/train-mix140/8k; do
    data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf --pitch-config conf/pitch.conf" \
    $sdata $data $feat || exit 1 
  done
  echo "## LOG (step04, $0): done @ `date`"
fi
