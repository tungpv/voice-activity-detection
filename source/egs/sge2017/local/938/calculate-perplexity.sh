#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
lm=/home2/hhx502/sge2017/data/update-lm-nov-09-2016/lm-int/lm.gz
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF

 [example]:

 $0 --steps 1 --lm $lm  /data/users/nhanh/938 /home2/hhx502/sge2017/data/data938

EOF

}

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ $# -ne 2 ]; then
  Usage && exit 1
fi
srcdata=$1
tgtdir=$2

localdir=$tgtdir/local
[ -d $localdir ] || mkdir -p $localdir
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): collect text and wav file list"
  find $srcdata -name "*.TextGrid" > $localdir/text-grid.flist
  find $srcdata -name "*.wav" | grep -v diarization > $localdir/wav.flist
  echo "## LOG (step01, $0): done ('$localdir')"
fi
perpdir=$tgtdir/perplexity
textdir=$perpdir/text
[ -d $textdir ] || mkdir -p $textdir
if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): make text for perplexity"
  source/egs/sge2017/local/938/extract-text-from-text-grid.pl  $localdir/text-grid.flist $textdir
  echo "## LOG (step02): done ('$perpdir')"
fi
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): calculate perplexity"
  for x in $(ls $textdir/*.txt); do
    fName=$(echo $x | perl -ane 'chomp; s/.*\///g; s/\.txt//g; print;')
    echo "## LOG (step03, $0): process file $x ..."
    pplFile=$textdir/${fName}.ppl
    ngram -order 3  -lm $lm  -unk -ppl $x  > $pplFile 2>&1 &
    echo "## LOG (step03, $0): done in $pplFile"
  done
  echo "## LOG (step03, $0): perplexity done"
fi

if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): collect statistical info"
  for x in $(ls $textdir/*.txt); do
    logFile=$(echo $x | perl -pe 'chomp; s/\.[^\.]+$/\.log/g;')
    [ -e $logFile ] || {
      echo "## WARNINIG (step04, $0): file $logFile does not exist";
    }
    
    pplFile=$(echo $x | perl -pe 'chomp; s/\.[^\.]+$/\.ppl/g;')
    [ -e $pplFile ] || {
      echo "## WARNING (step04, $0): file $pplFile does not exist";
    }
    cat $logFile $pplFile | \
    source/egs/sge2017/local/938/extract-ppl-stats.pl
  done  | sort -k2n,3n > $perpdir/perplexity-results.txt
  echo "## LOG (step04, $0): done ('$perpdir/perplexity-results.txt')"
fi

if [ ! -z $step05 ]; then
  cat $perpdir/perplexity-results.txt | \
  sort -k3n > $perpdir/perplexity-results-sort-by-range-c3.txt
  echo "('$perpdir/perplexity-results-sort-by-range-c3.txt')"
fi
