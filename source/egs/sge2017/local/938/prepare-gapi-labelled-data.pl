#!/usr/bin/perl -w 

use strict;
use utf8;
use lib qw(source/egs/myperllib);
use GridText;

print STDERR "## LOG: $0 ", join(" ", @ARGV), "\n";

my $numArgs = scalar @ARGV;

if ($numArgs != 5) {
  die "\n Example: $0 wavFile segFile textGridFile fileIndex tgtdir\n\n";
}
my ($wavFile, $segFile, $textGridFile, $fileIndex, $tgtdir) = @ARGV;
my $wavScp ="$tgtdir/wav.scp";

# begin sub
sub InitSegVocab {
  my ($segFile, $vocab) = @_;
  open(F, "$segFile") or die "## ERROR (InitSegVocab, ", __LINE__, "): cannot open $segFile\n";
  while(<F>) {
    chomp;
    next if(m:^;;:);
    my @A = split(/\s+/);
    if (scalar @A != 8) {
      die "## ERROR (InitSegVocab, ", __LINE__, "): bad line $_ in $segFile\n";
    }
    my ($start, $dur, $gender, $speaker) = ($A[2], $A[3], $A[4], $A[7]);
    die "## ERROR (InitSegVocab, ", __LINE__, "): bad speaker $speaker\n" if ($speaker !~ m/\S(\d+)/);
    my $speakerIndex = $1;
    $gender = lc $gender;
    my $speakerInfo = sprintf("s%03d%s", $speakerIndex, $gender);
    if(exists  $$vocab{$start}) {
      die "## ERROR (InitSegVocab, ", __LINE__, "): time $start duplicated\n";
    }
    $$vocab{$start} = [my @B];
    my $list = $$vocab{$start};
    @$list = ($dur, $speakerInfo);
  }
  close F;
}
# end sub

open (F, '>>', "$wavScp") or die "## ERROR (main, ", __LINE__, "): cannot open $wavScp \n";
my $label = $wavFile;
$label =~ s/.*\///g; $label =~ s/\.wav//g;  $label = lc $label; $label =~ s/[\-_]mp[34]$//;
print F "$label $wavFile\n";
close F;

my %segVocab = ();
InitSegVocab($segFile, \%segVocab);
open (F, "$textGridFile") or die "## ERROR (main, ", __LINE__, "): textGridFile $textGridFile cannot open\n";

open (SEG, '>>', "$tgtdir/segments") or die "## ERROR (main, ", __LINE__, "): cannot open $tgtdir/segments\n";
open (TEXT, '>>', "$tgtdir/text") or die "## ERROR (main, ", __LINE__, "): cannot open $tgtdir/text\n";
open (U2S, '>>', "$tgtdir/utt2spk") or die "## ERROR (main, ", __LINE__, "): cannot open $tgtdir/utt2spk\n";

my ($start, $end, $text);
while (<F>) {
  chomp;
  if(/xmin\s+=\s+(\S+)/) {
    $start = $1;
  } elsif(/xmax\s+=\s+(\S+)/) {
    $end = $1;
  } elsif (/text\s+=\s+\"(.*)\"/) {
    $text = $1;
    if ($start >= $end) {
      die "## ERROR (main, ", __LINE__, "): start ($start) >= end ($end), text = $text\n";
    }
    if($text ne "") {
      my $intStart = int($start * 100);
      my ($realStart, $realEnd, $spkInfo, $value);
      if(exists $segVocab{$intStart}) {
        $realStart = $start;
        $value = $segVocab{$intStart};
      }elsif (exists $segVocab{$intStart+1}) {
        $realStart = $start + 0.01;
        $value = $segVocab{$intStart+1}; 
      } elsif (exists $segVocab{$intStart-1}) {
        $realStart = $start - 0.01;
        $value = $segVocab{$intStart-1};
      } else {
        die "## ERROR (main, ", __LINE__, "): unexpected difference for timestamp $intStart\n";
      }
      $spkInfo = $$value[1];
      my $dur = sprintf("%.2f", $$value[0]*0.01);
      $realEnd = $dur + $realStart;
      if(abs($realEnd - $end) > 0.02) {
        die "## ERROR (main, ", __LINE__, "): unexpected timestamp $end versus $realStart realEnd $realEnd\n";
      }
      my $newSpkInfo = sprintf("%s%04d", $spkInfo, $fileIndex);
      my $uttName = sprintf("%s-%s-%06d-%05d", $newSpkInfo, $label, int($start*100), int($end*100 - $start*100));
      print TEXT "$uttName $text\n";
      print U2S "$uttName $newSpkInfo\n";
      print SEG "$uttName $label $start $end\n";
    }
  }
}
close F;

close SEG;
close TEXT;
close U2S;
