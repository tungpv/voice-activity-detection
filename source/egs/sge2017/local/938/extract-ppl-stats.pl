#!/usr/bin/perl -w

use strict;
use utf8;
use open qw (:std :utf8);

my ($minWordPerSec, $averageWordPerSec, $fileName, $ppl);

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  if(/minWordPerSec=(\S+),/) {
     $minWordPerSec = $1;
  }
  if (/averageWordPerSec=(\S+)/) {
    $averageWordPerSec = $1;
  }
  if (/file\s+(\S+)\:.*/) {
    $fileName = $1;
    $fileName =~ s/.*\///g;
    $fileName =~ s/\-diarize\.txt//g;
  }
  if(/ppl=\s+(\S+)/) {
    $ppl = $1;
    my $rangeOfWordPerSec = $averageWordPerSec - $minWordPerSec;
    print "$fileName $ppl $rangeOfWordPerSec $minWordPerSec $averageWordPerSec\n";
  }
}
print STDERR "## LOG ($0): stdin ended\n";
