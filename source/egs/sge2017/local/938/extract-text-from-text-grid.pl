#!/usr/bin/perl -w

use strict;
use utf8;
use Getopt::Long;
use lib qw(source/egs/myperllib);
use GridText;

print STDERR "## LOG: $0 ", join(" ", @ARGV), "\n";

my $numArgs = scalar @ARGV;

if($numArgs != 2) {
  die "\n Example: $0 text-grid.flist textdir\n\n";
}
my ($flist, $tgtdir) = @ARGV;

open(F, "$flist") or die "## ERROR (main, ", __LINE__, "): cannot read $flist\n";
` [ -d $tgtdir ] || mkdir -p $tgtdir`;
while(<F>) {
  chomp;
  my $gridFile = $_;
  my %vocab = ();
  my $sLogDoc;
  if(ReadGridText($gridFile, \%vocab, \$sLogDoc) != 0) {
    die "## ERROR (main, ", __LINE__, "): ReadGridText error for $gridFile\n";
  }
  my $textFile = $gridFile;
  $textFile =~ s/.*\///g;
  $textFile =~ s/\.[^\.]+$//g;
  my $logFile = sprintf("%s/%s.log", $tgtdir, $textFile);
  $textFile = sprintf("%s/%s.txt", $tgtdir, $textFile);
  open (T, "|sort -k1,2 -n >$textFile") or die "## ERROR (main, ", __LINE__, "): cannot write to $textFile\n";
  open (LOG, ">$logFile") or die "## ERROR (main, ", __LINE__, "): cannot write to $logFile\n";
  print LOG "$sLogDoc\n";
  close LOG;
  foreach my $key (keys%vocab) {
    my $text = $vocab{$key};
    GridTextNormalize(\$text);
    print T "$text\n";
  }
  close T;
}

close F;
