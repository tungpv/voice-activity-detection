#!/bin/bash

. path.sh
. cmd.sh 


# begin options
cmd=run.pl
start_j=1
nj=60
end_j=
acwt=0.0909
config=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF

 [example]:
 
 steps/nnet/make_denlats.sh --cmd "$cmd" --nj 60  --start-j 1 --end-j 20 --acwt 0.0909 \
 --config conf/decode_dnn.config /home2/hhx502/sge2017/data/train-mix431/fbank-pitch \
 /home2/hhx502/sge2017/data/update-oct-27-2016/lang /home2/hhx502/sge2017/exp/update-oct-27-2016/16k/nnet5a-tl-mix431 \
 /home2/hhx502/sge2017/exp/update-oct-27-2016/16k/nnet5a-tl-mix431/denlat-train

EOF

}

if [ $# -ne 5 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
srcdir=$4
dir=$5

[ -d $dir ] || mkdir -p $dir
[ -e $dir/lat.scp ] && rm $dir/lat.scp
[ $end_j -le $nj ] || \
{ echo "## ERROR ($0): end_j ($end_j) is not less than nj ($nj)"; exit 1; }

steps/nnet/make_denlats.sh --cmd "$cmd" --nj $nj --start-j $start_j --end-j $end_j --acwt $acwt --config conf/decode_dnn.config \
$data $lang $srcdir $dir  || exit 1
