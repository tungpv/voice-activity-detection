#!/usr/bin/perl -w
use strict;

print STDERR "## LOG ($0): stdin expected\n";
while (<STDIN>) {
  chomp;
  if(m/(\S+)\s+(\S+)\s+(\S+\.pcm)\s+(.*)/g) {
    print "$1 $2 -r 8000 $3 -r 16000 -c 1 -b 16 -t wav - |\n";
  } elsif (m/(\S+)\s+(\S+\.wav)/g) {
    print "$1 /usr/bin/sox -r 8000 $2 -r 16000 -c 1 -b 16 -t wav - |\n";
  } else {
    die "## ERROR ($0): unidentified line $_\n";
  }
}

print STDERR "## LOG ($0): stdin ended\n";
