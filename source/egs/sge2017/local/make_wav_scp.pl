#!/usr/bin/perl -w
use strict;

my %wavVocab = ();

my($wavList) = @ARGV;
open (WAV, "$wavList") or die "## ERROR ($0): cannot open wave list $wavList\n";
while(<WAV>) {
  chomp;
  if( ! m/(.*)\/([^\/]+)\.wav/g ) {
    print STDERR "## LOG ($0): unexpected file $_\n"; next;
  }
  if (exists $wavVocab{$2}) {
    die "## LOG ($0): duplicated file $_\n";
  }
  $wavVocab{$2} = $_;
}
close WAV;
# begin sub

# end sub
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  if(m/^(\S+)\s+(\S+)$/g) {
    my ($lab, $wavFile) = ($1, $2);
    if (not exists $wavVocab{$lab}) {
      print STDERR "## LOG ($0): unknown $lab\n"; next;
    }
    my $newWavFile = $wavVocab{$lab};
    # s#$wavFile#$newWavFile#g;
    print "rev-$lab $newWavFile\n";
  } elsif (/^(\S+)\s+(.*)\s+(\S+\.wav)\s+.*/g) {
    my ($lab, $wavFile) = ($1, $3);
    if (not exists $wavVocab{$lab}) {
      print STDERR "## LOG ($0): unknown $lab\n"; next;
    }
    my $newWavFile = $wavVocab{$lab};
    # s#$wavFile#$newWavFile#g;
    print "rev-$lab $newWavFile\n";
  } else {
    print STDERR "## LOG ($0): unexpected pattern $_\n";
  }

}
print STDERR "## LOG ($0): stdin ended\n";
