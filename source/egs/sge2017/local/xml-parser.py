#!/cm/shared/apps/python3.5.2/bin/python3.5

from __future__ import print_function
from guess_language import *
from boilerpipe.extract import Extractor
import pdb, os, sys, re, lxml.html, json, urllib, argparse
# import normalize, db, time
import xml.etree.ElementTree as ET
# from xml_handler_2 import *
from num2words import num2words
# from normalize import *
import codecs
from optparse import OptionParser

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


if __name__ == '__main__':
    if (sys.stdout.encoding is None):
        print >> sys.stderr, "please set python env PYTHONIOENCODING=UTF-8, example: export PYTHONIOENCODING=UTF-8, when write to stdout."
        exit(1)
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-xml',dest = 'input_xml', help = 'input xml file', required=True)
    parser.add_argument('-o', '--output-file',dest = 'output_file', help = 'output text file', required=True)
    args = parser.parse_args()
    if(args.input_xml == None or args.output_file == None):
        parser.print_help()
        sys.exit(1)
    
    tree = ET.parse(args.input_xml)
    root = tree.getroot()
    if args.output_file != '-':
        output_writer = open(args.output_file, 'w')
    else:
        output_writer = sys.stdout
    longer_word_dict = dict()
    for doc in root.iter('doc'):
        wordNum = 0
        paragraph = ''
        wordLen = 0
        for word in doc.text.split():
            if len(word) > 30: 
                # eprint ('## LOG (' + sys.argv[0] + '): longer word, ' + word)
                word_frequency = 0
                if word in longer_word_dict:
                    word_frequency = longer_word_dict[word]
                word_frequency += 1
                longer_word_dict[word] = word_frequency
                continue
            wordNum += 1
            paragraph += word
            wordLen += len(word)
            # print (str(len(word)) + ': ' + word)
            paragraph += ' '
        if wordNum >= 100:
             output_writer.write(paragraph.lower() + '\n')
    eprint('\n\nLonger word list:')
    for word in longer_word_dict:
        eprint (word + ':' + str(longer_word_dict[word]))
