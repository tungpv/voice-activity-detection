#!/bin/bash 

. path.sh
. cmd.shu

# begin options
cmd=run.pl
nj=60
steps=
start_nj=
end_nj=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF
  [examples]:

 steps/nnet/make_priors.sh --cmd slurm.pl --nj 60 --append-vector-to-feats append-vector-to-feats /home2/hhx502/sge2017/data/train-mix431/fbank-pitch /home2/hhx502/sge2017/exp/update-oct-27-2016/16k/mpe-nnet5a-tl-mix431

EOF

}

