#!/bin/bash

srcdata=/home2/hhx502/sge2017/data/train-merge70h-rev/16k
tgtdata=/home2/hhx502/sge2017/data/train-merge70h-rev/8k

cp $srcdata/{segments,text,utt2spk,spk2utt} $tgtdata
cat $srcdata/wav.scp | \
perl -ane 'chomp; m/(\S+)\s+(\S+)/g or next; print "$1 /usr/bin/sox $2 -r 8000 -c 1 -b 16 -t wav - downsample |\n";' > $tgtdata/wav.scp

utils/fix_data_dir.sh $tgtdata
