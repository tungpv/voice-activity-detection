#!/usr/bin/perl -w
use strict;
use utf8;
use open qw (:std :utf8);

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\n Example: cat sg-en-i2r/data/train-merge/mfcc-pitch/text | $0 /home2/hhx502/sg-en-i2r/data/train/text\n\n";
}

# begin sub
sub LoadTextVocab {
  my($vocab, $fileName) = @_;
  open(F, "$fileName") or die "## ERROR (LoadTextVocab, $0): file $fileName cannot open\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/g or next;
    $$vocab{$1} = $2;
    my $s = sprintf("rev-%s", $1);
    $$vocab{$s} = $2;
  }
  close F;
}
# end sub
my ($source_text_file) = @ARGV;

my %textVocab = ();
LoadTextVocab(\%textVocab, $source_text_file);

while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/g or next;
  if(not exists $textVocab{$1}) {
    print STDERR "## WARNING ($0): utterance $1 is unknown\n";
    next;
  }
  print "$1 $textVocab{$1}\n";
}


