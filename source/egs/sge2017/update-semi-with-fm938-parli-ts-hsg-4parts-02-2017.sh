#!/bin/bash

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh

# begin options
steps=
cmd=slurm.pl
cleanup=false
nj=40
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF
 
 [Examples]:
 
 $0 --steps 1 --cleanup false parliament:talkshow:historyofsg \
 sge003:sge004:sge005 \
 /data/users/nhanh/SingaporeanSpeechData/parliament \
 /data/users/nhanh/SingaporeanSpeechData/talkabout \
 /data/users/nhanh/SingaporeanSpeechData/history-of-singapore \
 /home2/hhx502/sge2017/update-semi-with-fm938-parli-ts-hsg-4parts-02-2017

EOF

}

if [ $# -ne 6 ]; then
  Usage && exit 1
fi

srcdataid_csl=$1
tgtdataid_csl=$2
srcparliament=$3
srctalkabout=$4
srchistoryofsg=$5
tgtdir=$6

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

function CheckFileStatus () {
  if [ $# -ne 1 ]; then
    return 1;
  fi
  local  file=$1;
  [ -e $file ] || return 1;
  return 0;
}

srcdataid_array=($(echo $srcdataid_csl|sed -e 's/:/ /g'))
tgtdataid_array=($(echo $tgtdataid_csl|sed -e 's/:/ /g'))
num_of_id=${#srcdataid_array[@]}

function CheckCslToArray () {
  declare -a array=("${!1}")
  local array_size=${#array[@]}  
  for i in $(seq 0 $[$array_size-1]); do
    echo "$i: ${array[$i]}"
  done
}

# CheckCslToArray srcdataid_array[@]
# CheckCslToArray tgtdataid_array[@]

tgtdata=$tgtdir/data
function Cleanup {
  echo "## LOG ($0): remove *-tmp folders under $tgtdata folder "
  rm -rf $tgtdata/*-tmp
}

trap "Cleanup" INT QUIT TERM EXIT

[ -d $tgtdata ] || mkdir -p $tgtdata
if [ ! -z $step01 ]; then
  srcdataIndex=0
  for srcdata in $srcparliament $srctalkabout $srchistoryofsg; do
    srcdataid=${srcdataid_array[$srcdataIndex]}
    tgtdataid=${tgtdataid_array[$srcdataIndex]}
    tmpdata=$tgtdata/${srcdataid}-${tgtdataid}-tmp
    [ -d $tmpdata ] || mkdir -p $tmpdata
    data=$tgtdata/${srcdataid}-${tgtdataid}
    [ -d $data ] || mkdir -p $data
    srcdataIndex=$[srcdataIndex+1]
    fileIndex=1
    echo "## LOG (step01, $0): srcdata=$srcdata"
    for x in $(find $srcdata -maxdepth 1 -mindepth 1 -type d|sort); do
      fileName=$(basename $x)
      wavFile=$(find $x -name "${fileName}.*"  | egrep  '\.wav')
      segFile=$(find $x -name "${fileName}.*"  | egrep '\.seg')
      textGridFile=$(find $x -name "${fileName}.*"  | egrep '\.TextGrid')
      CheckFileStatus $wavFile || { echo "## ERROR (step01, $0): waveFile $wavFile is not ready"; exit 1; }
      CheckFileStatus $segFile || { echo "## ERROR (step01, $0): segFile $segFile is not ready"; exit 1; }
      CheckFileStatus $textGridFile || { echo "#3 ERROR (step01, $0): textGridFile $textGridFile is not ready"; exit 1; }    
      source/egs/sge2017/local/938/prepare-gapi-labelled-data.pl $wavFile $segFile $textGridFile $fileIndex $tmpdata
      fileIndex=$[fileIndex+1]
    done
    utils/utt2spk_to_spk2utt.pl < $tmpdata/utt2spk > $tmpdata/spk2utt
    source/egs/sge2017/update-nov-28-16k/copy-data.sh --prefix ${tgtdataid}- \
    $tmpdata $data
  done
  echo "## LOG (step01, $0): done & check '$data'"
fi
combined_data=$tgtdata/combined456
if [ ! -z $step02 ]; then
  utils/combine_data.sh $combined_data \
  $tgtdata/parliament-sge003 $tgtdata/talkshow-sge004 \
  $tgtdata/historyofsg-sge005
  utils/validate_data_dir.sh --no-feats $combined_data 
  echo "## LOG (step02, $0): done & check '$combined_data'"
fi
localdict=$tgtdata/dict
bigdict=/home2/hhx502/sge2017/data/update-nov-28-16k/dict/lexicon.txt
word_transfer_dict=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/dict/word-transfer-dict.txt
if [ ! -z $step03 ]; then
  [ -d $localdict ] || mkdir -p $localdict
  [ -f $combined_data/history-text ] || cp $combined_data/text $combined_data/history-text
  cat $combined_data/history-text | \
  source/egs/sge2017/update-dec-04-2016-with-fm938/normalize-text.pl \
  --word-transfer-dict=$word_transfer_dict | \
  source/egs/mandarin/local/prepare-msra-863-mix/subset-lex-with-text.pl --from=1  --lex-output-dir=$localdict \
  --word-transfer-dict=$word_transfer_dict \
  --transferred-text=$combined_data/update-text \
  --segments=$combined_data/segments \
  $bigdict
  echo "## LOG (step03, $0): done & check '$combined_data' & '$localdict'"
fi
