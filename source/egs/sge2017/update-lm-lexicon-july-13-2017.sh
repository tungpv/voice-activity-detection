#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=

# end options

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 [Example]: $0 --steps 1   \
 ../sge/name-entity/dict/singapore \
 /data/users/tungpham/SingaporeEnglishText/singaporeEnglish_Jul2017 \
 /data/users/zin/data/dataforST/haihua_selected_data_no_dup.txt.gz  \
 /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017 

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 4 ]; then
  Example && exit 1
fi

interndictdir=$1
tungtextdatadir=$2
zintextdata=$3
tgtdir=$4

newdictdir=$tgtdir/interndictdir
[ -d $newdictdir ] || mkdir -p $newdictdir

if [ ! -z $step01 ]; then
  cat ../sge/name-entity/dict/singapore/mrt-food-hotel-road/asr-lexicon.txt \
  ../sge/name-entity/dict/singapore/restaurant/restaurant-asr-it2.txt  \
  ../sge/name-entity/dict/singapore/entity/asr-lexicon-it2.txt \
  ../sge/name-entity/dict/singapore/poi/poi-asr-lexicon-it2.txt | \
  source/egs/sge2017/update-lm-lexicon-july-13-2017/normalize-dict.pl ../acumen/data/local/dict/nonsilence_phones.txt \
  > $newdictdir/lexicon.txt
  echo "## LOG (step01, $0): done & check '$newdictdir/lexicon.txt'"
fi
dictdir=$tgtdir/dict
lang=$tgtdir/lang
[ -d $lang ] || mkdir -p $lang
[ -d $dictdir ] || mkdir -p $dictdir
if [ ! -z $step02 ]; then
  cat $newdictdir/lexicon.txt /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/tung/oov-asr-dict-it01.txt  | \
  source/egs/ted-libri-en/merge-dict.pl ../acumen/data/local/dict/lexicon.txt  > $dictdir/lexicon.txt
  cp ../acumen/data/local/dict/{extra_questions.txt,nonsilence_phones.txt,silence_phones.txt,optional_silence.txt} \
  $dictdir/
  utils/validate_dict_dir.pl $dictdir
  utils/prepare_lang.sh $dictdir "<unk>" $lang/tmp $lang 
  echo "## LOG (step02, $0): done & check '$dictdir/'"
fi
tungdata=$tgtdir/tung
[ -d $tungdata ] || mkdir -p $tungdata
if [ ! -z $step03 ]; then
  cat $tungtextdatadir/{output_channelnewsasiaParliament_Jul2017.txt,output_todayOnline.txt,output_alvinology_Jul2017.txt} | \
  source/egs/sge2017/update-lm-lexicon-july-13-2017/normalize-text.pl | \
  source/egs/mandarin/update-april-03-2017-with-pruned-lexicon/segment-chinese-text.py --do-word-segmentation | \
  sort -u | \
  source/egs/sge2017/update-lm-lexicon-july-13-2017/make-transfer-dict.pl  $tungdata/transfer-word-dict.txt  $tungdata/text.gz
fi

if [ ! -z $step04 ]; then
  gzip -cd $tungdata/text.gz | \
  source/egs/mipitalk/transfer-utterance-with-dict.pl $tungdata/transfer-word-dict.txt | \
  gzip -c > $tungdata/text-it01.gz
  gzip -cd  $tungdata/text-it01.gz | \
  utils/shuffle_list.pl | \
  head -10000 | gzip -c > $tungdata/dev10k.gz
  gzip -cd  $tungdata/text-it01.gz | \
  utils/shuffle_list.pl | \
  tail -n +10001 | gzip -c > $tungdata/train.gz
  echo "## LOG (step04, $0): done & check '$tungdata/dev10k.gz' & '$tungdata/train.gz'"
fi

zindata=$tgtdir/zin
[ -d $zindata ] || mkdir -p $zindata
if [ ! -z $step05 ]; then
  gzip -cd $zintextdata | \
  utils/shuffle_list.pl | \
  head -10000 | gzip -c > $zindata/dev10k.gz
  gzip -cd $zintextdata | \
  utils/shuffle_list.pl | \
  tail -n +10001 | gzip -c > $zindata/train.gz
fi

if [ ! -z $step06 ]; then
  source/egs/swahili/show-oovs.pl ../acumen/data/lang/words.txt  \
  'gzip -cd /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/tung/train.gz|' > $tungdata/oov-wordlist.txt
fi
if [ ! -z $step07 ]; then
  cut -f1 $tungdata/oov-wordlist.txt |\
  source/egs/sge2017/update-lm-lexicon-july-13-2017/paste.pl > $tungdata/word-transfer-dict-ver2.txt
fi
text1=../acumen/data/local/transcript-lm/train-text.gz
text2=../acumen/data/local/web-lm-wb/train-text.gz
merge_3part=$tgtdir/merge3
[ -d $merge_3part ] || mkdir -p $merge_3part
if [ ! -z $step11 ]; then
  gzip -cd $text1 $text2 $tungdata/train.gz $zindata/train.gz | \
  source/egs/mipitalk/transfer-utterance-with-dict-ignore-oov.pl  /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/tung/word-transfer-dict-ver2.txt \
  | sort -u | gzip -c > $merge_3part/train-text.gz
  gzip -cd  ../acumen/data/local/transcript-lm/dev-text.gz \
  /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/tung/dev10k.gz \
 /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/zin/dev10k.gz | 
 gzip -c > $merge_3part/dev-text.gz
 cut -f1 $dictdir/lexicon.txt | sort -u | gzip -c > $merge_3part/vocab.gz
 echo "## LOG (step11, $0): done & check '$merge_3part'"
fi
if [ ! -z $step09 ]; then
   echo "## LOG (step09, $0): add oov lexicon from tung text data"
  ./source/egs/sge2017/convert-human-lexicon-to-asr-lexicon-with-phone-mapping-list.py --debug --normalize \
  ../sge/name-entity/dict/singapore/mrt/sge-phone-mapping-it3.txt \
  /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/tung/oov-human-dict.txt  /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/tung/oov-asr-dict.txt
  cp /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/tung/oov-asr-dict.txt /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/tung/oov-asr-dict-it01.txt
fi

if [ ! -z $step12 ]; then
  echo "(step10, $0): train lm"
  source/egs/sge2017/local/build-lm/train-srilm-v2.sh --steps 1,2 --lm-order-range "3 3" \
  --cutoff-csl "3,011,012"  $merge_3part || exit 1
  echo "## LOG (step12, $0): done & check '$merge_3part'"
fi

if [ ! -z $step13 ]; then
  order=3
  tgtdir=$merge_3part
  cut -f1 $dictdir/lexicon.txt | sort -u | gzip -c > $tgtdir/vocab.gz
  ngram-count -order $order -wbdiscount -vocab $tgtdir/vocab.gz -unk -sort -text $tgtdir/train-text.gz -lm $tgtdir/lm-wb${order}.gz
fi

if [ ! -z $step14 ]; then
  tgtdir=$merge_3part
  for lambda in $(seq 0 0.1 0.6); do
    ngram -order 3  -lm $tgtdir/lm-wb3.gz -vocab $tgtdir/vocab.gz -lambda $lambda \
    -mix-lm $tgtdir/lm.gz -ppl $tgtdir/dev-text.gz
  done
fi
tgtdir=$merge_3part
intlm=$tgtdir/int0.3-lm.gz
if [ ! -z $step15 ]; then
  echo "## LOG (step15, $0): lm interpolating"
  ngram -order 3  -lm $tgtdir/lm-wb3.gz -vocab $tgtdir/vocab.gz -lambda 0.3 \
  -mix-lm $tgtdir/lm.gz -write-lm $intlm
  ngram -order 3 -lm $intlm -ppl $tgtdir/dev-text.gz
fi
if [ ! -z $step16 ]; then
  echo "## LOG (step16, $0): attempt to prune lm"
  for prune in 1.0e-8 1.0e-9; do
    lm=$tgtdir/pr${prune}-int0.3-lm.gz
    ngram -order 3 -lm $intlm -prune $prune -write-lm $lm
    ngram -order 3 -lm $intlm -ppl $tgtdir/dev-text.gz
  done
fi
lm=$tgtdir/pr1.0e-8-int0.3-lm.gz
if [ ! -z $step17 ]; then
  source/egs/fisher-english/arpa2G.sh $lm $lang $lang
fi
graph=../acumen/exp/dnn/graph-update-lm-lexicon-july-13-201
if [ ! -z $step18 ]; then
  utils/mkgraph.sh  $lang  $(dirname $graph) \
  $graph
fi
if [ ! -z $step19 ]; then
  cmd='slurm.pl --quiet --gres=gpu:1'
  devdata=../acumen/data/dev/fbank-pitch
  nnetdir=$(dirname $graph)
  steps/nnet/decode.sh --cmd "$cmd" --nj 4 \
  --use-gpu yes \
 $graph $devdata $nnetdir/decode-dev-938-update-lm-lexicon-july-13-201
fi
tgtdata=../acumen/data/train-merge-sup/fbank-pitch
if [ ! -z $step20 ]; then
  utils/combine_data.sh $tgtdata /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/train-merge/fbank-pitch-mix431-part \
  ../acumen/data/train/fbank-pitch
  echo "## LOG (step20, $0): done with data '$tgtdata'"
fi
srcdir=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938/exp/mpe-nnet5a-tl-nov28
lang=/home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/lang/
expdir=../acumen/update-aug-17-with938-50hrs/exp
if [ ! -z $step21 ]; then
  echo "## LOG (step21, $0): started @ `date`"
  cmd='slurm.pl --gres=gpu:1 --quiet --exclude=node02,node03,node06'
  nj=4
  steps/nnet/align.sh  --cmd "$cmd" --nj $nj \
  --use-gpu yes \
  $tgtdata $lang $srcdir $expdir/ali-train
  echo "## LOG (step21, $0): done with '$expdir/ali-train' @ `date`"
fi

if [ ! -z $step22 ]; then
  echo "## LOG (step22, $0): started @ `date`"
  cmd='slurm.pl --quiet'
  nj=120
  steps/nnet/make_denlats.sh --cmd "$cmd" --nj $nj \
  --use-gpu no \
  $tgtdata $lang $srcdir $expdir/lattice-train
  echo "## LOG (step22, $0): ended @ `date`"
fi
if [ ! -z $step23 ]; then
    cmd='slurm.pl --quiet --gres=gpu:1'
  steps/nnet/train_mpe.sh --cmd "$cmd" \
  $tgtdata  $lang $srcdir $expdir/ali-train $expdir/lattice-train \
  $expdir/mpe-nnet 
fi

if [ ! -z $step24 ]; then
  cmd='slurm.pl --quiet --gres=gpu:1'
  devdata=/home2/hhx502/sge2017/data/fstd-demo-updated/16k/fbank-pitch
  graph=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938/exp/tri4a/graph-with-dict-from-ali
  steps/nnet/decode.sh --cmd "$cmd" --nj 3 \
  --use-gpu yes \
  --score-script /home/hhx502/w2016/sge2017/local/score.sh \
  $graph $devdata $expdir/mpe-nnet/decode-fstd-demo-updated
fi

if [ ! -z $step25 ]; then
  cmd='slurm.pl --quiet --gres=gpu:1'
  devdata=/home2/hhx502/sge2017/data/fstd-demo-updated/16k/fbank-pitch
  graph=../acumen/exp/dnn/graph-update-lm-lexicon-july-13-201
  steps/nnet/decode.sh --cmd "$cmd" --nj 3 \
  --use-gpu yes \
  --score-script local/score_sclite.sh \
  $graph $devdata $expdir/mpe-nnet/decode-fstd-demo-updated_graph-update-lm-lexicon-july-13-201
fi
