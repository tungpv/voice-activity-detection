#!/bin/bash 

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
# end options

. parse_options.sh || exit 1

function Usage {
cat <<EOF

 Usage: $0 [options] <sge50_part_sdir> <seame_en_part_dir> <wsj_sge_part>  <tgtdir>
 [options]:
 [steps]:
 [examples]:
 
 $0 --steps 1 /home2/hhx502/sg-en-i2r-16k/data \
 /home2/hhx502/seame/data/english-train/16k/fbank-pitch \
 /data/users/hhx502/english-corpus/wsj-sge/data  /home2/hhx502/sge2017

EOF
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

sge50h_data=$1
seame15h_data=$2
wsj15h_data=$3
tgtdir=$4

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $tgtdir ] || mkdir -p $tgtdir
tgtdata=$tgtdir/data
seame_train16k=$tgtdata/train-seame/16k
seame_train8k=$tgtdata/train-seame/8k
[ -d $tgtdata ] || mkdir -p $tgtdata
spkNum=30
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): prepare seame data"
  seame_dev16k=$tgtdata/dev-seame.28/16k
  mkdir -p $seame_train16k $seame_dev16k
  sdata=$seame15h_data; data=$seame_dev16k
  cat $sdata/utt2spk | awk '{print $2;}' | uniq | utils/shuffle_list.pl | head -$spkNum > $data/spklist
  utils/subset_data_dir.sh --spk-list $data/spklist $sdata $data
  rm $data/{feats.scp,cmvn.scp,reco2file_and_channel}
  fix_data_dir.sh $data
  data=$seame_train16k
  cat $sdata/utt2spk | awk '{print $2;}' | uniq |utils/shuffle_list.pl | tail -n +$[$spkNum+1] > $data/spklist
  utils/subset_data_dir.sh --spk-list $data/spklist $sdata $data
  rm $data/{feats.scp,cmvn.scp,reco2file_and_channel}
  fix_data_dir.sh $data
  echo "## LOG (step01, $0): preparation done"
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): prepare seame 8k data"
  for sdata in $tgtdata/dev-seame.28/16k  $seame_train16k; do
    data=$sdata/../8k
    [ -d $data ] || mkdir -p $data
    cp $sdata/{wav.scp,text,segments,utt2spk,spk2utt} $data/
    cat $sdata/wav.scp | \
    perl -ane 'chomp; s#(\S+)\s+.*(\/data\/.*\.wav).*#\1 /usr/bin/sox \2 -r 8000 -c 1 -b 16 -t wav - downsample |#g; print "$_\n";' > $data/wav.scp
    echo "## LOG (step02, $0): $data is ready"
    head $data/wav.scp 
  done
  echo "## LOG (step02, $0): seame 8k data preparation done"
fi
sge15h_train8k=$tgtdata/train-sge15h/8k
sge15h_train16k=$tgtdata/train-sge15h/16k
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): prepare sge50h data"
  sdata=$sge50h_data/train; data=$sge15h_train16k
  [ -d $data ] || mkdir -p $data
  cp $sdata/{wav.scp,text,segments,utt2spk,spk2utt} $data/
  cat $sdata/text |\
  perl -ane 'use utf8; use open qw (:std :utf8); chomp; if(m/(\S+)\s+(.*)/) { print $1, " ", lc $2, "\n"; }' \
  > $data/text
  utils/fix_data_dir.sh $data
  echo "## LOG (step03, $0): data ('$data') preparation done"
  sdata=$sge50h_data/train-8k; data=$sge15h_train8k
  [ -d $data ] || mkdir -p $data
  cp $sdata/{wav.scp,text,segments,utt2spk,spk2utt} $data/
  cat $sdata/text |\
  perl -ane 'use utf8; use open qw (:std :utf8); chomp; if(m/(\S+)\s+(.*)/) { print $1, " ", lc $2, "\n"; }' \
  > $data/text
  utils/fix_data_dir.sh $data
  echo "## LOG (step03, $0): sge50h data preparation done"
fi
wsj15h_train8k=$tgtdata/train-wsj15h/8k
wsj15h_train16k=$tgtdata/train-wsj15h/16k
if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): prepare wsj data"
  sdata=$wsj15h_data; data=$wsj15h_train16k
  [ -d $data ] || mkdir -p $data
  cp $sdata/{wav.scp,text,segments,utt2spk,spk2utt} $data/
  cat $sdata/text |\
  perl -ane 'use utf8; use open qw (:std :utf8); chomp; if(m/(\S+)\s+(.*)/) { print $1, " ", lc $2, "\n"; }' \
  > $data/text
  utils/fix_data_dir.sh $data
  echo "## LOG (step04, $0): data ('$data') preparation done"
  sdata=$data; data=$wsj15h_train8k
  [ -d $data ] || mkdir -p $data
  cp $sdata/* $data/
  cat $sdata/wav.scp | \
  perl -ane 'chomp; if(m/(\S+)\s+(.*)/g){ printf("%s /usr/bin/sox %s -r 8000 -c 1 -b 16 -t wav - downsample |\n", $1, $2);  }' > $data/wav.scp
  utils/fix_data_dir.sh $data
  echo "## LOG (step04, $0): wsj data preparation done"
fi

merge70h_train16k=$tgtdata/train-merge70h/16k
merge70h_train8k=$tgtdata/train-merge70h/8k

if [ ! -z $step05 ]; then
  echo "## LOG (step05, $0): merge data"
  utils/data/combine_data.sh $merge70h_train16k \
  $seame_train16k $sge15h_train16k $wsj15h_train16k
  fix_data_dir.sh $merge70h_train16k
  source/data-len.sh $merge70h_train16k
  echo "## LOG (step05, $0): done with merging ('$merge70h_train16k')"
fi

if [ ! -z $step06 ]; then
  echo "## LOG (step05, $0): merged 8k data"
  utils/data/combine_data.sh $merge70h_train8k \
  $seame_train8k $sge15h_train8k $wsj15h_train8k
  echo "## LOG (step05, $0): done with mergining for data ('$merge70h_train8k')"
fi
