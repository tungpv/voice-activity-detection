#!/bin/bash 

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh

# begin options
steps=
cmd=slurm.pl
cleanup=false
nj=40
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF
 
 [Examples]:
 
 $0 --steps 1 --cleanup false /data/users/nhanh/SingaporeanSpeechData/938 /home2/hhx502/sge2017/update-dec-04-2016-with-fm938

EOF

}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

srcdata=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

tgtdata=$tgtdir/data/data938
function CheckFileStatus () {
  if [ $# -ne 1 ]; then
    return 1;
  fi
  local  file=$1;
  [ -e $file ] || return 1;
  return 0;
}
if [ ! -z $step01 ]; then
  [ -d $tgtdata ] || mkdir -p $tgtdata
   fileIndex=1
  for x in $(find $srcdata -maxdepth 2 -mindepth 2 -type d|sort); do
    fileName=$(basename $x)
    wavFile=$(find $x -name "${fileName}.*"  | egrep  '\.wav')
    segFile=$(find $x -name "${fileName}.*"  | egrep '\.seg')
    textGridFile=$(find $x -name "${fileName}.*"  | egrep '\.TextGrid')
    CheckFileStatus $wavFile || { echo "## ERROR (step01, $0): waveFile $wavFile is not ready"; exit 1; }
    CheckFileStatus $segFile || { echo "## ERROR (step01, $0): segFile $segFile is not ready"; exit 1; }
    CheckFileStatus $textGridFile || { echo "#3 ERROR (step01, $0): textGridFile $textGridFile is not ready"; exit 1; }    
    source/egs/sge2017/local/938/prepare-gapi-labelled-data.pl $wavFile $segFile $textGridFile $fileIndex $tgtdata
    fileIndex=$[fileIndex+1]
  done
  utils/utt2spk_to_spk2utt.pl < $tgtdata/utt2spk > $tgtdata/spk2utt

  echo "## LOG (step01, $0): done & check '$tgtdata'"
fi
bigdict=/home2/hhx502/sge2017/data/update-nov-28-16k/dict/lexicon.txt
tgtdict=$tgtdir/data/dict
if [ ! -z $step02 ]; then
  [ -d $tgtdict ] || mkdir -p $tgtdict
  [ -f $tgtdata/history-text ] || cp $tgtdata/text $tgtdata/history-text
  cat $tgtdata/history-text | \
  source/egs/sge2017/update-dec-04-2016-with-fm938/normalize-text.pl \
  --word-transfer-dict=$tgtdict/word-transfer-dict.txt | \
  source/egs/mandarin/local/prepare-msra-863-mix/subset-lex-with-text.pl --from=1  --lex-output-dir=$tgtdict \
  --word-transfer-dict=$tgtdict/word-transfer-dict.txt \
  --transferred-text=$tgtdata/update-text \
  --segments=$tgtdata/segments \
  $bigdict
  echo "## LOG (step02, $0): done & check '$tgtdict'"
fi
man_dir=/home/ngaho/work/iKanzi/transcription/manual
tgtdir=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/man-data-15h
tmpdir=$tgtdir/temp
merge_dict_dir=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938/merge-dict
function cleanup {
  echo "## LOG ($0): removing $tmpdir ..."
  rm -rf $tmpdir
  rm $merge_dict_dir/lexicon01.txt
}
if $cleanup; then
  trap "cleanup" INT QUIT TERM EXIT 
fi
if [ ! -z $step03 ]; then
  [ -d $tmpdir ] || mkdir -p $tmpdir
  find $man_dir -name "*.TextGrid" > $tmpdir/text-grid.flist
  source/egs/sge2017/local/938/extract-text-from-text-grid.pl $tmpdir/text-grid.flist $tmpdir
  find $tmpdir/ -name "*.txt" | \
  source/egs/sge2017/update-dec-04-2016-with-fm938/prepare-kaldi-data.pl \
  --label-transfer-dict=$tgtdir/label-transfer-dict.txt \
  --dict-for-oov-checking=$bigdict $tgtdata/wav.scp $tgtdir
  utils/utt2spk_to_spk2utt.pl < $tgtdir/utt2spk > $tgtdir/spk2utt
  utils/fix_data_dir.sh $tgtdir
  # [ -f $tgtdir/history-text ] || cp $tgtdir/text $tgtdir/history-text
  echo "## LOG (step03, $0): done & check '$tgtdir'"
fi
source_dict_dir=/home2/hhx502/sge2017/data/update-oct-27-2016/dict
if [ ! -z $step10 ]; then
  [ -d $merge_dict_dir ] || mkdir -p $merge_dict_dir
  cat $tgtdict/lexicon.txt | \
  egrep -v '^90' | \
  cat - $source_dict_dir/lexicon.txt | source/egs/sge2017/update-nov-28-16k/merge-dict.pl  \
  --word-lower-case --phone-upper-case $merge_dict_dir/lexicon01.txt  \
  "|egrep -v -i '<oov>|<sss>|<vns>|SIL' > $merge_dict_dir/nonsilence_phones.txt"
  cat $merge_dict_dir/lexicon01.txt | \
  sed 's/<OOV>/<oov>/;s/<SSS>/<sss>/; s/<VNS>/<vns>/;' > $merge_dict_dir/lexicon.txt
  grep '<' $merge_dict_dir/lexicon.txt
  cp $source_dict_dir/{silence_phones.txt,optional_silence.txt,extra_questions.txt} $merge_dict_dir/
  utils/validate_dict_dir.pl $source_dict_dir
  echo "## LOG(step10, $0): done & check '$merge_dict_dir'"
fi
tgtdir=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938
lang=$tgtdir/data/lang
if [ ! -z $step11 ]; then
  utils/prepare_lang.sh $merge_dict_dir "<unk>" $lang/tmp  $lang
  echo "## LOG (step11, $0): done & check '$lang'"
fi
lmdir=$tgtdir/data/lm
if [ ! -z $step12 ]; then
  [ -d $lmdir ] || mkdir -p $lmdir
  
  cat /home2/hhx502/sge2017/data/train-mix431/fbank-pitch/text \
  /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/data938/text | \
  source/egs/sge2017/local/build-lm/dump-kaldi-text.pl | gzip -c > $lmdir/train-text.gz

  cat /home2/hhx502/sge2017/data/dev-seame.28/16k/text \
  /home2/hhx502/sge2017/data/fstd-demo-updated/16k/text | \
  source/egs/sge2017/local/build-lm/dump-kaldi-text.pl | gzip -c > $lmdir/dev-text.gz

  cat $merge_dict_dir/lexicon.txt | \
  source/egs/sge2017/local/build-lm/print-word-list.pl | gzip -c > $lmdir/vocab.gz
  
  source/egs/sge2017/local/build-lm/train-srilm-v2.sh --steps 1,2 --lm-order-range "3 3" \
  --cutoff-csl "3,011,012"  $lmdir || exit 1
  echo "## LOG (step12, $0): done & check '$lmdir'"
fi
lang_test=$tgtdir/data/lang_test
if [ ! -z $step13 ]; then
  source/egs/fisher-english/arpa2G.sh $lmdir/lm.gz $lang $lang_test
  echo "LOG (step13, $0): done & check '$lang_test'"
fi

if [ ! -z $step14 ]; then
  for sdata in $tgtdir/data/data938; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc16k.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    utils/fix_data_dir.sh $data
    echo "## LOG (step14, $0): mfcc-pitch done @ `date`"
    data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    echo "## LOG (step14, $0): fbank-pitch done & check '$data' @ `date`"
  done
fi
train_merge=$tgtdir/data/train-merge
data_mix431=$tgtdir/data/train-mix431-copy
if [ ! -z $step15 ]; then
 source/egs/sge2017/update-nov-28-16k/copy-data.sh --prefix sge001- /home2/hhx502/sge2017/data/train-mix431/fbank-pitch $data_mix431
 cat $data_mix431/utt2spk | perl -pe 's/ref\-/rev\-/g;' > $data_mix431/utt2spk-1
 mv $data_mix431/utt2spk-1 $data_mix431/utt2spk
 utils/utt2spk_to_spk2utt.pl < $data_mix431/utt2spk > $data_mix431/spk2utt
 utils/fix_data_dir.sh $data_mix431
 rm $data_mix431/{cmvn.scp,feats.cp} 
fi

if [ ! -z $step18 ]; then
  data938=$tgtdir/data/data938
  data938_copy=$tgtdir/data/data938-copy
  source/egs/sge2017/update-nov-28-16k/copy-data.sh --prefix sge002-  $data938/mfcc-pitch $data938_copy/mfcc-pitch
  source/egs/sge2017/update-nov-28-16k/copy-data.sh --prefix sge002-  $data938/fbank-pitch $data938_copy/fbank-pitch
 
  utils/combine_data.sh $train_merge $data_mix431  $data938_copy/mfcc-pitch
  echo "## LOG (step18, $0): done & check '$train_merge'"
fi
if [ ! -z $step19 ]; then
  for sdata in $train_merge; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc16k.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    utils/fix_data_dir.sh $data
    echo "## LOG (step19, $0): mfcc-pitch done @ `date`"
    data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    echo "## LOG (step19, $0): fbank-pitch done & check '$data' @ `date`"
  done

fi
train_id=a
expdir=$tgtdir/exp
if [ ! -z $step20 ]; then
  echo "## LOG (step20): gmm started @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1,2,3,4,5,6,7 \
  --train-id $train_id --cmvn-opts "--norm-means=true"  --state-num 12000 --pdf-num 240000 \
  --devdata /home2/hhx502/sge2017/data/dev-seame.28/16k/fbank-pitch \
  $train_merge/mfcc-pitch $lang_test  $expdir || exit 1
  echo "## LOG (step20): gmm ended @ `date`"
fi
