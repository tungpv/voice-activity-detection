#!/usr/bin/perl -w
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 2) {
    die "\nExample: $0  sge-lex.txt en-lex.txt\n\n";
}

my($sgeLexFile, $enLexFile) = @ARGV;

# begin sub
sub LoadDict {
  my ($dictFile, $vocab, $phoneVocab) = @_;
  open(F, "$dictFile") or die "## ERROR (LoadDict, $0): cannot read file $dictFile\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    my $word = lc $1; 
    my $pron = $2;  my @A = split(/\s+/, $pron);
    for(my $i = 0; $i < scalar @A; $i ++) {
      $$phoneVocab{$A[$i]} = 0;
    }
    if(not exists $$vocab{$word}) {
      $$vocab{$word} = [my @array];
      my $ref = $$vocab{$word};
      push @$ref, \@A;
    } else {
      my $ref = $$vocab{$word};
      push @$ref, \@A;
    }
  }
  close F;
}

# end sub

my %sgeVocab = ();
my %sgePhoneVocab = ();
LoadDict($sgeLexFile, \%sgeVocab, \%sgePhoneVocab);
my %sgePhone2EnPhoneMap = ();
my $totalOverlappedWord = 0;
open(F, "$enLexFile") or die "## ERROR (main, $0): cannot read file $enLexFile\n";
while(<F>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my $word = lc $1;
  my $pron = $2;
  my @A = split(/\s+/, $pron);
  if(exists $sgeVocab{$word}) {
    $totalOverlappedWord ++;
    my $refA2 = $sgeVocab{$word};
    for(my $i = 0; $i < scalar @$refA2; $i ++) {
      my $refA = $$refA2[$i];   
      my $sgePron = join(" ", @$refA);
      my $enPron = join(" ", @A);
      if(scalar @A == scalar @$refA) {
        for(my $j = 0; $j < scalar @A; $j++) {
          my $sgePhone = $$refA[$j];
          my $enPhone = $A[$j];
          $sgePhone2EnPhoneMap{$sgePhone}{$enPhone} ++;
        }
      } else {
        print STDERR "word's pronunciations ('$word\t$enPron') ('$word\t$sgePron') have different phone number\n";
      }
    }
  }
}
close F;
print STDERR "## LOG (main, $0): total overlapped words are $totalOverlappedWord\n";
my %twoBestMapVocab = ();

foreach my $sgePhone (keys%sgePhone2EnPhoneMap) {
  my %tmpVocab = ();
  foreach my $enPhone (keys%{$sgePhone2EnPhoneMap{$sgePhone}}) {
    $sgePhoneVocab{$sgePhone} ++;
    $tmpVocab{$enPhone} = $sgePhone2EnPhoneMap{$sgePhone}{$enPhone};
  }
  my $maxOut = 3; my $i = 0;
  foreach my $enPhone (sort {$tmpVocab{$b}<=>$tmpVocab{$a}} keys%tmpVocab) {
    if($i++ < $maxOut) {
      print "$sgePhone\t$enPhone\t$tmpVocab{$enPhone}\n";
    }
  }
}

print STDERR "## SGE phones that have not been mapped\n";
foreach my $sgePhone (keys%sgePhoneVocab) {
  if($sgePhoneVocab{$sgePhone} == 0) {
    print STDERR "$sgePhone\n";
  }
}
