#!/bin/bash

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=

# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF
 
 [Example]: $0 --steps 1 /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/lang_test/words.txt \
 /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/lm_with_dict_from_ali/lm.gz \
  /data/users/zin/Data_sel/temp/exp/exp4/selected_data.gz \
 /data/users/zin/Data_sel/input/judge-sg.txt \
 /data/users/zin/update-lm-but-fixt-lexicon

 [prompts]: please refer to script: 'source/egs/sge2017/local/build-lm.sh' 

EOF
}

if [ $# -ne 5 ]; then
  Usage && exit 1
fi


steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi


words=$1
indomain_lm=$2
selected_daeta=$3
dev_data=$4
tgtdir=$5

tmpdir=$tgtdir/temp
[ -d $tmpdir ] || mkdir -p $tmpdir
if [ ! -z $step01 ]; then
  echo "## prepare lexicon"
  
fi
