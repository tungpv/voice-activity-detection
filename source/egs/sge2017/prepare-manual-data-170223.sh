#!/bin/bash 

. path.sh 
. cmd.sh 

# begin options
steps=
cmd='slurm.pl --quiet'
nj=40
# end options

function Usage {
 cat<<EOF
 
 [examples]: $0 --steps 1 /home/ngaho/work/iKanzi/transcription/20170223_manual/train \
 /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/data938/wav.scp \
 /home2/hhx502/sge2017/data/manual-data-170223  

EOF
}

. parse_options.sh || exit 1 

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi


if [ $# -ne 3 ]; then
  Usage && exit 1
fi

mdata=$1
wavscp=$2
tgtdir=$3

[ -d $tgtdir ] || mkdir -p $tgtdir

overall_speech_data=$tgtdir/overall
[ -d $overall_speech_data ] || mkdir -p $overall_speech_data
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): collect all recently added data from Anh"
  find  /data/users/nhanh/speech/  -name "*.wav" | grep resampled | sort -u > $overall_speech_data/wavlist.txt
  cat $overall_speech_data/wavlist.txt | \
  perl -ane 'chomp; $label = $_; $label =~ s:.*\/::g;  $label =~ s:\.wav::g; $label =~ s:\-mp[34]$::g;  print "$label $_\n";'  > $overall_speech_data/wav.scp
fi
transcript=$tgtdir/transcript
[ -d $transcript ] || mkdir -p $transcript
if [ ! -z $step02 ] && false; then
  cp $mdata/*.TextGrid  $transcript/
fi
if [ ! -z $step03 ]; then
  find $transcript/ -name "*.TextGrid" > $tgtdir/translist.txt
  cat $tgtdir/translist.txt | \
  source/egs/sge2017/prepare-manual-data-170223/match-trans-wav-file.pl $overall_speech_data/wav.scp $tgtdir
  echo "## LOG (step03, $0): check '$tgtdir'"
fi

[ -d $wavdir ] || mkdir -p $wavdir
if [ ! -z $step04 ]; then
  for x in $(cut -d' ' -f2 $tgtdir/wav.scp); do
    [ -f $x ] || { echo "## WARNING (step04, $0): file $x does not exist"; exit 1;  }
    wavfile=$(basename $x);
    tgtfile=$transcript/$wavfile
    [ -f $tgtfile ] || cp $x $transcript/$wavfile
  done
fi

if [ ! -z $step06 ]; then
  cat $tgtdir/transcript.txt | \
  source/egs/sge2017/prepare-manual-data-170223/make-kaldi-data-from-transcript.pl $tgtdir
  utils/utt2spk_to_spk2utt.pl < $tgtdir/utt2spk > $tgtdir/spk2utt
  utils/fix_data_dir.sh $tgtdir
  cut -d' ' -f2-  $tgtdir/text | source/egs/sge2017/local/normalize-text/word-count.pl > $tgtdir/word-list-of-text.txt
  # utils/validate_data_dir.sh --no-feats $tgtdir
  echo "## LOG (step06, $0): check '$tgtdir' && '$tgtdir/word-list-of-text.txt'"
fi

if [ ! -z $step07 ]; then
  cat $tgtdir/word-list-of-text.txt | \
  source/egs/sge2017/prepare-manual-data-170223/make-word-transfer-dict.pl  > $tgtdir/word-transfor-dict.txt
  echo "## LOG (step07, $0): done with '$tgtdir/word-transfor-dict.txt'"
fi
srcdict=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938/merge-dict/lexicon.txt
if [ ! -z $step08 ]; then
  cat $tgtdir/word-transfor-dict-edit.txt | \
  source/egs/sge2017/prepare-manual-data-170223/check-and-edit-dict.pl \
  $tgtdir/word-transfer-dict-final.txt $tgtdir/word-list-final.txt
  cat $tgtdir/word-list-final.txt| source/egs/seame/show-oov.pl $srcdict  1  > $tgtdir/oov-word-transfer-dict-final.txt
  echo "## LOG (step08, $0): done with '$tgtdir/word-transfer-dict-final.txt' & '$tgtdir/word-list-final.txt' @ `date`"
fi
bigdict=/home3/hhx502/w2017/mandarin/data/update-with-nnet3/dict-sweet-pie/lexicon.txt
tgtdict=$tgtdir/dict
[ -d $tgtdict ] || mkdir -p $tgtdict
if [ ! -z $step09 ]; then
  cat $tgtdir/oov-word-transfer-dict-final-edit.txt | \
  source/egs/sge2017/prepare-manual-data-170223/print-word-or-phone-from-dict.pl --print-phones | \
  source/egs/sge2017/prepare-manual-data-170223/grep-word-entry-from-dict.pl $bigdict | \
  grep -v '_man' |  \
  source/egs/sge2017/prepare-manual-data-170223/add-remove-suffix-to-dict-phone.pl --remove-suffix '_eng' | \
  source/egs/ted-libri-en/merge-dict.pl $srcdict > $tgtdict/lexicon.txt
fi

if [ ! -z $step10 ]; then
  [ -f $tgtdir/history-text ] || mv $tgtdir/text $tgtdir/history-text
  cat $tgtdir/history-text | \
  source/egs/sge2017/prepare-manual-data-170223/translate-text.pl --from=2 \
  $tgtdir/word-transfer-dict-final.txt  | \
  source/egs/sge2017/prepare-manual-data-170223/translate-text.pl --from=2 \
  $tgtdir/oov-word-transfer-dict-final-edit.txt | \
  source/egs/sge2017/prepare-manual-data-170223/remove-empty-text.pl --from=2 \
  --remove-words="<unk> <v-noise> <noise>"  > $tgtdir/text
  utils/fix_data_dir.sh $tgtdir
fi

tgtdata=$tgtdir/overall-unlabeled-data
function CheckFileStatus () {
  if [ $# -ne 1 ]; then
    return 1;
  fi
  local  file=$1;
  [ -e $file ] || return 1;
  return 0;
}
[ -d $tgtdata ] || mkdir -p $tgtdata
if [ ! -z $step11 ]; then
  srcdata=/data/users/nhanh/speech
  depth=3
  for x in $(find $srcdata  -maxdepth $depth -mindepth $depth -type d|sort); do
    fileName=$(basename $x)
    if [[ "$fileName" =  "diarization" ]]; then
      newDir=$(dirname $x)
      x=$newDir
      fileName=$(basename $x)
    fi
    if [[ "$fileName" = "raw" || "$fileName" = "resampled" || "$fileName" = "transcript" ]]; then 
       continue;
    fi
    wavFile=$(find $x -name "${fileName}.*"  | egrep  '\.wav')
    segFile=$(find $x -name "${fileName}.*"  | egrep '\.seg')
    textGridFile=$(find $x -name "${fileName}.*"  | egrep '\.TextGrid')
    CheckFileStatus $wavFile || { echo "## WARNING (step11, $0): waveFile $wavFile is not ready ('$x')"; continue; }
    CheckFileStatus $segFile || { echo "## WARNING (step11, $0): segFile $segFile is not ready ('$x')"; continue; }
    CheckFileStatus $textGridFile || { echo "## WARNING (step11, $0): textGridFile $textGridFile is not ready ('$x')"; continue; }    
    source/egs/sge2017/local/938/prepare-gapi-labelled-data.pl $wavFile $segFile $textGridFile $fileIndex $tgtdata
    fileIndex=$[fileIndex+1]
  done
  utils/utt2spk_to_spk2utt.pl < $tgtdata/utt2spk > $tgtdata/spk2utt  
fi
unlabeldir=$tgtdir/subset-unlabeled-data
[ -d $unlabeldir ] || mkdir -p $unlabeldir
if [ ! -z $step12 ]; then
  cat $tgtdir/overall-unlabeled-data/wav.scp | \
  source/egs/sge2017/prepare-manual-data-170223/subset-wavscp.pl --exclude-wave-list="cat $tgtdir/wav.scp| cut -d' ' -f1|" \
  >$unlabeldir/wav.scp 
  cat $tgtdir/overall-unlabeled-data/segments | \
  source/egs/sge2017/prepare-manual-data-170223/subset-segments-by-wavscp.pl $unlabeldir/wav.scp | \
  cut -d' ' -f1 > $unlabeldir/utt-list
  utils/data/subset_data_dir.sh --utt-list $unlabeldir/utt-list $tgtdir/overall-unlabeled-data $unlabeldir
  echo "## done & check '$unlabeldir/utt-list'"
fi
# now, we are organize google asr labelled text data
if [ ! -z $step13 ]; then
  cut -d' ' -f2-  $unlabeldir/text | source/egs/sge2017/local/normalize-text/word-count.pl  | \
  source/egs/sge2017/prepare-manual-data-170223/make-word-transfer-dict.pl  > $unlabeldir/word-transfor-dict.txt
  echo "## LOG(step13, $0): done & check '$unlabeldir'"
fi
