#!/bin/bash 

. path.sh
. cmd.sh 

# begin options
steps=
# end options

function Usage {
 cat<<EOF

 [Examples]:

 $0 --steps 1  /home/ngaho/work/scripts/data/groundtruth/wow-club-jue-wong-mp3-resampled.stm \
 /home/ngaho/work/scripts/data/google/wow-club-jue-wong-mp3-resampled.stm  /home2/hhx502/sge2017/exp/score-google

EOF
}

. parse_options.sh || exit 1

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

ref_stm=$1
google_stm=$2
tgtdir=$3


if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z $step01 ]; then
  cp $google_stm $tgtdir/hyp.stm
  cp $ref_stm $tgtdir/ref.stm
  cat $tgtdir/hyp.stm | \
  source/egs/sge2017/compute-google-wer-approx/align-text-with-time.pl $tgtdir/ref.stm $tgtdir
  compute-wer --text --mode=present ark:$tgtdir/ref.txt ark:$tgtdir/hyp.txt
fi
