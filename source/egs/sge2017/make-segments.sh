#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options

# end options

. parse_options.sh || exit 1

function Usage {
cat<<EOF
 Usage: $0 <datadir>
 [options]:
 [exmaples]:

 $0  /home2/hhx502/sge2017/data/fs938-two-speaker/fbank-pitch

EOF

}

if [ $# -ne 1 ]; then
  Usage && exit 1
fi

data=$1

if [  -e $data/segments ]; then
  echo "## LOG ($0): segments already there !"; exit 0
fi

for x1 in wav.scp; do
  x=$data/$x1
  [ -e $x ] || { echo "## ERROR: $x expected from $data"; exit 1; }
done

echo "## LOG ($0): segments generation started @ `date`"

wav-to-duration scp:$data/wav.scp ark,t:-| \
perl -ane 'chomp; @A = split(/\s+/); $len_in_sec = sprintf("%.2f", $A[1]);  print "$A[0] $A[0] 0 $len_in_sec\n";' > $data/segments

echo "## LOG ($0): segments generation done @ `date` !" && exit 0
  
