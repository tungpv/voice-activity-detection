#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
cmd=slurm.pl
nj=10
compute_fbank_feats=compute-fbank-feats
fbank_config=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF
 [examples]:

 $0 --steps 1 --compute-fbank-feats $compute_fbank_feats\
  --fbank-config conf/empty.conf \
  /home2/hhx502/sge2017/data/dev-seame.28/16k \
 /home2/hhx502/sge2017/data/dev-seame.28/16k/fbank-test01

EOF
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

data=$1
featdata=$2

feat=$featdata/data
log=$featdata/log

steps/make_fbank.sh --cmd "$cmd" --nj $nj \
--compute-fbank-feats $compute_fbank_feats \
${fbank_config:+--fbank-config $fbank_config} \
$data $log $feat

