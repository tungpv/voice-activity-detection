#!/usr/bin/perl

use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 3) {
  die "\n## Example: $0 <trans-list> <wave-list> <tgtdir>\n\n";
}

my ($transListFile, $wavListFile, $tgtdir) = @ARGV;

# begin sub
sub InitTransVocab {
  my ($filename, $flistVocab, $transVocab) = @_;
  open (F, "$filename") or die "## ERROR: $0, file $filename cannot open\n";
  while(<F>) {
    chomp;
    my $speaker = $_;
    $speaker =~ s/.*\///g; $speaker =~ s/\.txt//;
    unless (open(TRANS, "$_")) {
      print STDERR "## WARNING: InitTransVocab@$0: cannot read $_ \n";
      next;
    } 
    $$flistVocab{$speaker} = $_;
    my $index;
    while(<TRANS>) {
      chomp;
     if( m/([\d]+)\s+(.*)/) {
       $index = $1;
     } else {
       $$transVocab{$speaker}{$index} = $_;
     }
    }
    close TRANS;
  }
  close F;
}
# end sub
my %flistVocab = (); my %transVocab = (); 
InitTransVocab($transListFile, \%flistVocab, \%transVocab);
open(WAVSCP, ">$tgtdir/wav.scp") or die "## ERROR: $0, cannot open $tgtdir/wav.scp to write\n";
open(TEXT, ">$tgtdir/text.1") or die "## ERROR: $0: cannot open $tgtdir/text to write\n";
open(UTT2SPK, ">$tgtdir/utt2spk") or die "## ERROR: @$0: cannot open $tgtdir/utt2spk to write\n";
open(WAV, "$wavListFile") or die "## ERROR: $0, file $wavListFile cannot open\n";
while(<WAV>) {
  chomp;
  my @A = split(/\//);  my $N = scalar @A;
  my ($speaker, $session, $index) = ($A[$N-3], $A[$N-2], $A[$N-1]);
  $index =~ s/\.wav//;
  my $label = sprintf("%s-%s-%s", $speaker, $session, $index);
  if(not exists $flistVocab{$speaker}) {
    print STDERR "## WARNING: $0: no trans for speaker $speaker\n";
    next;
  }
  print WAVSCP "$label $_\n";
  if(not exists $transVocab{$speaker}{$index}) {
    print STDERR "## WARNING: $0: no trans for index $index\n";
    next;
  }
  print UTT2SPK "$label $speaker\n";
  print TEXT "$label $transVocab{$speaker}{$index}\n";
}

close WAV;
close WAVSCP;
close TEXT;
close UTT2SPK;
