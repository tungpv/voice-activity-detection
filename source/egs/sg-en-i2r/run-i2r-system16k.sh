#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=
i2r_source=
# end options

function Usage {
 cat<<END

 Usage: $0 [options] <source-dir> <test_data>  <tgtdir>
 [options]:
 [steps]:
 [examples]:

 $0 --steps 1 /data/users/hhx502/sg-en-source-sept-2016/FromI2R \
 /home2/hhx502/seame/data/english-train/16k/fbank-pitch \
 /home2/hhx502/seame/exp/i2r-system16k-eval 

END
}
. parse_options.sh || exit 1

if [ $# -ne 3 ]; then
  Usage && exit 1;
fi

source_dir=$1
test_data=$2
tgtdir=$3

[ -d $tgtdir ] || mkdir -p $tgtdir

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
feat_cmd=$source_dir/frontend/compute-i2r-feats
config=$source_dir/frontend/frontend.conf
dev_data=$tgtdir/data/dev-sg-en-seame15h
[ -d $dev_data ] || mkdir -p $dev_data
if [ ! -z $step01 ]; then
  echo "## LOG-step01@$0: make feature for test data @ `date`"
  cp $test_data/* $dev_data/
  rm $dev_data/{feats.scp,cmvn.scp} 2>/dev/null
  extract-segments scp:$dev_data/wav.scp $dev_data/segments ark:- | \
  $feat_cmd --frontend=$config --wav-input=true ark:- ark,scp:$dev_data/feats.ark,$dev_data/feats.scp
  echo "## LOG-step01@$0: done @ `date`"
fi

if [ ! -z $step02 ]; then
  cat $dev_data/lowercase-text | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; @A = split(/\s+/); $utt = shift @A; for($i = 0; $i < @A; $i ++) { $utt .= " ". uc($A[$i]) ; } print "$utt\n"; ' > $dev_data/text
  utils/fix_data_dir.sh $dev_data
fi
decode_dir=$tgtdir/decode-dev-sg-en-seame15h
graph=$source_dir/graph
srcdir=$source_dir/am
if [ ! -z $step03 ]; then
  echo "## LOG-step03@$0: decode @ `date`"
  steps/nnet/decode.sh --cmd "slurm.pl --nodelist=node06" --nj 10 --srcdir $srcdir \
   --scoring-opts "--min-lmwt 8 --max-lmwt 15  --acwt 0.1" --beam 10 --lattice-beam 8 --max-mem 500000000 \
  --skip-scoring false $graph $dev_data $decode_dir || exit 1 
  echo "## LOG-step03@$0: done @ `date`"
fi
