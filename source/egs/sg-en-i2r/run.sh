#!/bin/bash

echo
echo "### LOG: $0 $@"
echo

. path.sh
. cmd.sh 

# begin options
steps=
phone_map_str="#,NULL;.,NULL;3:,x3:;4,x4;@,AT;{,/br"
vendor_lex=/home/hhx502/w2016/sg-en-i2r/data/overall/lexicon-vendor.txt
mandarin_lex=/home2/hhx502/ss-apsipa2016/mandarin/data/local/dict/lexicon.1
mandarin_lang_id=man
transform_dict=/data/users/hhx502/w2016/sg-en-i2r/trans-dict.txt
langid=sge
silence_words="<noise>,<sss>:<unk>,<oov>:<v-noise>,<vns>"
lmtype=3gram-mincount
# end options

function Usage {
 cat<<END
 Usage: $0 [options] <resource-dir> <tgtdir>
 [options]
 --steps                            # value, "$steps"
 --phone-map_str                    # value, "$phone_map_str"
 --vendor-lex                       # value, "$vendor_lex"
 --mandarin-lex                     # value, "$mandarin_lex"
 --mandarin-lex                     # value, "$mandarin_lex"
 --mandarin-lang-id                 # value, "$mandarin_lang_id"
 --transform-dict                   # value, "$transform_dict"
 --langid                           # value, "$langid"
 --silence-words                    # value, "$silence_words"
 --lmtype                           # value, "$lmtype"

 [examples]:

 $0 --steps 7 --vendor-lex $vendor_lex \
 --phone-map-str "$phone_map_str" \
 --langid $langid \
 --mandarin-lex $mandarin_lex \
 --mandarin-lang-id $mandarin_lang_id \
 --transform-dict $transform_dict \
 --silence-words "$silence_words" \
 --lmtype $lmtype \
 /data/users/hhx502/w2016/sg-en-i2r  /home2/hhx502/sg-en-i2r

END
}

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  Usage && exit 1;
fi
source_data_dir=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
slocal_dir=$source_data_dir/data/local
tmpdir=$slocal_dir/temp
[ -d $tmpdir ] || mkdir -p $tmpdir

if [ ! -z $step01 ]; then
  echo "## LOG: step01, prepare speaker info @ `date`"
  cat $source_data_dir/SpeakerInfo-CategoryI*-utf8-linux.txt | grep -v Gender | \
  perl -e 'while(<>){ chomp; @A = split(/[\s\t]+/); if(@A == 0 ){ next; }
  elsif(@A < 5){die "bad line: $_\n";}
  elsif(@A == 5) { if($A[0] =~ /^\d/){$A[0] = "Speaker".$A[0];} push(@A, "noavail"); $_ = join(" ", @A); print "$_\n";  }
  elsif(@A == 6 ) {if($A[0] =~ /^\d/){$A[0] = "Speaker".$A[0];} $_ = join(" ", @A); print "$_\n";  }
  else{ if($A[0] =~ /^\d/){$A[0] = "Speaker".$A[0];} $s = substr($A[5], 0, 1); for($i=6; $i < @A; $i++){$s .=substr($A[$i], 0 ,1);} splice(@A, 6); $A[5] = $s;  $_ = join(" ", @A);  print "$_\n";  }
  }' | \
  perl -e 'while(<>){chomp; @A= split(/[\s\t]+/); if(@A != 6) {die "ERROR, badline: $_"; } @B=(); $B[0] = $A[0]; $B[0] =~ s:Speaker:spkr:; 
   $B[1] = $A[1] . $A[2]; $B[1] = lc ($B[1]); $B[2] = substr($A[3], 0, 2); $B[2] = lc($B[2]);
   $B[3] = substr($A[4], 0, 3); $B[3] = lc($B[3]);
   $B[4] = substr($A[5], 0, 3); $B[4] = lc($B[4]);
   print $_. " ". join("-", @B) . "\n";
   }' >  $tmpdir/speaker-info.txt

  echo "## LOG: step01, ($tmpdir/speaker-info.txt) done with speaker info preparation @ `date`"
fi
data_overall=$slocal_dir/overall
[ -d $data_overall ] || mkdir -p $data_overall
if [ ! -z $step02 ]; then
  echo "## LOG: step02, prepare wav.scp @ `date`"
  cat $source_data_dir/wavlist.txt | \
  perl -e 'use File::Basename; ($infilename) = @ARGV; open(F, "<$infilename") or die "ERROR, file $infilename cannot open\n"; 
    while (<F>){ chomp; @A = split(/[\s\t]+/); die "ERROR, bad line $_\n" if (@A != 7); $vocab{$A[0]} = $A[6];  } close F; 
    while (<STDIN>){chomp; $fname = basename($_); $fname =~ s/\.pcm//;  if(/CategoryII/){$cate="c02";}elsif(/CategoryI/){$cate="c01";} else {die "ERROR, no Category name in file path\n";} 
      @A = split("-", $fname);  if (@A == 1){$index = 1;} elsif(@A==2) {$index = $A[1];} else {die "ERROR, bad filename $fname\n";}
      if (not exists $vocab{$A[0]}) {
        print STDERR "WARNING, unidentified speaker $A[0]\n"; next;
      } 
      $spker = $vocab{$A[0]}; $labname = $cate . "-" . $spker . "-" . sprintf("%02d", $index);
      print $labname, " /usr/bin/sox $_ -r 8000 -c 1 -b 16 -t wav - |\n";
    }
  '  $tmpdir/speaker-info.txt   > $data_overall/wav.scp

  echo "## LOG: step02, done with wav.scp ($data_overall/wav.scp) @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "## LOG: make initial transcript @ `date`"
  cat $source_data_dir/transcript.list | \
  perl -e '($wavfile) = @ARGV; open(F, "<$wavfile") or die "ERROR, file $wavfile cannot open\n"; 
    while(<F>) { chomp; if(/(\S+)\s+(\S+)\s+.*(Speaker.*)\.pcm\s+(.*)/) { $labname = $1; $fname = $3; $vocab{$fname} = $labname;  } else {die "ERROR, bad line $_\n";} } close F;
    while(<STDIN>) {chomp; if(/.*\/(Speaker.*)\.TextGrid/) { $spker = $1; if (not exists $vocab{$spker}) { print STDERR "WARNING: no pcm file for $_\n"; next; } $labname = $vocab{$spker}; open(F, "<$_") or die "ERROR, file $_ cannot open\n";
   if(not exists $voc{$labname}) {$voc{$labname}++;}else {die "ERROR, duplicated label $labname, for $spker\n";}
   while(<F>){ chomp; if(/xmin\s+=\s+(\S+)/) {$start = $1;  next; } if(/xmax\s+=\s+(\S+)/){$end = $1; next; } 
     if(/text\s+=\s+(.*)$/) { $text = $1; $text =~ s:[\"]::g;  print $labname, " ", $start, " ", $end, " $text\n"; next; }
   } close F;
  } else {die "ERROR, bad line $_\n";}   }
  '  $data_overall/wav.scp | dos2unix > $data_overall/initial-transcript.txt
  
  echo "## LOG: done with initial transcript generation ($data_overall/initial-transcript.txt) @ `date`"
fi
dict=$slocal_dir/dict
[ -d $dict ] || mkdir -p $dict
if [ ! -z $step04 ]; then
  echo "## LOG: analyze vendor lexicon @ `date`"
  cat $vendor_lex | \
  perl -e 'while(<>) {chomp; @A = split(/[\s\t]/); shift @A; for($i=0; $i<@A; $i++){$phone = $A[$i]; print "$phone\t$phone\n"; } }' | \
   sort -u  | \
  perl -e '($map_str, $langid) = @ARGV; @A = split(/[;]/, $map_str); for($i=0; $i<@A; $i++){ $pair=$A[$i]; @B = split(/[,]/, $pair); if($B[1] eq "NULL"){ $vocab{$B[0]} = $B[1];} else { $vocab{$B[0]} = $B[1] . "_$langid";  }  } 
  while(<STDIN>) { chomp; @A = split(/\s+/); if(exists $vocab{$A[0]}) { print "$A[0]\t$vocab{$A[0]}\n" ; }
    else{print "$A[0]\t$A[1]_$langid\n";}    }
 '  "$phone_map_str" $langid  > $dict/phone_map.txt
 cat $vendor_lex | \
 perl -e '($mapfile) = @ARGV; open(F, "<$mapfile") or die "ERROR, file $mapfile cannot open\n";
  while(<F>) { chomp; @A = split(/[\s\t]/); $vocab{$A[0]} = $A[1];  } close F;
  while(<STDIN>) { chomp; @A = split(/[\s\t]/); $w = shift @A; $w = uc $w;
    $w =~ s/\([^\(]+\)//g;
    $s = "";
    for($i = 0; $i < @A; $i++) { $p = $A[$i];  $map_p = $vocab{$p}; if($map_p eq "NULL") {$map_p = ""; } 
      if($i == 0) {$s = $map_p; }else { $s .= " $map_p";  }  }  print "$w\t$s\n";   }' $dict/phone_map.txt  | \
    sort -u  > $dict/lexicon-$langid.txt
   cat $mandarin_lex | \
   perl -e ' use utf8; use open qw(:std :utf8); ($langid) = @ARGV;
     while(<STDIN>) { chomp; @A = split(/\s+/); $w = shift @A;  $pron = $A[0] . "_$langid "; 
       for($i = 1; $i < @A; $i++){$p = $A[$i]; $pron .= $A[$i] . "_$langid ";  }  print "$w\t$pron\n";  }
   ' $mandarin_lang_id  > $dict/lexicon-${mandarin_lang_id}.txt
   cat $dict/{lexicon-${langid}.txt,lexicon-${mandarin_lang_id}.txt} > $dict/lexicon-${langid}-${mandarin_lang_id}.txt
  echo "## LOG: done($dict/phone_map.txt) @ `date`"
fi
if [ ! -z $step05 ]; then
  echo "## LOG: step05, filter initial transcript @ `date`"
  cat $data_overall/initial-transcript.txt | \
  grep -v '\<P\>' | \
  perl -e 'use utf8; use open qw(:std :utf8); while(<STDIN>){ @A = split(/\s+/); if(@A <= 3){next;} 
   $start = sprintf("%.2f", $A[1]); $end = sprintf("%.2f", $A[2]);
   $utt = $A[0] . " " . $start . " " . $end;  
   for($i = 3; $i < @A; $i ++) { $w = $A[$i];
     if($w =~ m/\p{Han}+/) {
       $s = $w;
       $start = 0;  $tot_len = length($s);
       while($s =~ m/(\p{Han}+)/g) {
         $ms = $1;  $len = length($ms);  $end = pos($s); $new_start = $end - $len;
         if($new_start > $start) {
           $sub_s = substr($s, $start, $new_start - $start);
           if($sub_s ne "[" && $sub_s ne "]" && $sub_s ne "<" && $sub_s ne ">"  ) {
             $utt .= " " . $sub_s;
           } 
         }
         $start = $end;
         @B = split("", $ms); $utt .= " " . join(" ", @B);
       }
       if($start > 0 && $start < $tot_len) {
         $sub_s = substr($s, $start, $tot_len - $start);
         if($sub_s ne "[" && $sub_s ne "]" &&  $sub_s ne "<" && $sub_s ne ">") {
           $utt .= " " .  $sub_s;
         }
       }
     } else {
       if($w ne "[" && $w ne "]" && $w ne ">" && $w ne "<") { $utt .= " $w";  }
     }
   }
   print "$utt\n";
  }' | \
   perl -pe '
    s#\[\]#<unk>#g;
    s#\*\*#<unk>#g;
    s#<SPK/>#<v-noise>#g;
    s#<@/># #g;
    s#<NON/>#<noise>#g;
    s#<STA/>#<noise>#g;
    s#<NPS/>#<v-noise>#g;
    s#\( \(\) \)#<unk>#g;
    s#\(spk\)#<v-noise>#g;
    s#\(sound\s+[\s\)]#<noise>#g;
    s#(sound)##g;
    s#\(s[oi]und##g;
    s#\(hes\)#<v-noise>#g;
    s#[\[\]]##g;
  ' | \
   perl -e 'while(<>){chomp; @A = split(/[\s\t]+/); if(@A<=3){print STDERR "WARNING, empty utterance: $_\n"; next;}
     $has_output = 0; for($i =3; $i < @A; $i++) { if($A[$i]=~ /\</){next;}else{ $has_output=1; last;}}
     if($has_output ==0) {print STDERR "WARNING, un-normal utterance: $_\n"; next; }
     print "$_\n";
   }' > $data_overall/transcript.1.txt 2> $data_overall/filtered.1.txt
  echo "## LOG: step05, done ($data_overall/transcript.1.txt) @ `date`"
fi
if [ ! -z $step06 ]; then
  echo "## LOG: print word list to check @ `date`"
  cat $data_overall/transcript.1.txt | \
  perl -e 'use open qw(:std :utf8); 
     while(<>){chomp; @A = split(/[\s\t]/); for($i=3; $i< @A; ++$i) { print "$A[$i]\t$A[$i]\n";}  }'| \
   sort -u > $data_overall/wordlist.1.txt
  echo "## LOG: done($data_overall/wordlist.1.txt)"
fi
if [ ! -z $step07 ]; then
  echo "## LOG: transform transcript @ `date`"
  cat $data_overall/transcript.1.txt | \
  perl -e ' ($dictfile) = @ARGV; open(F, "<$dictfile") or die "ERROR, file $dictfile cannot open\n";
    while(<F>) {chomp; @A = split(/[\s\t]+/); $word=shift @A;  $prons = ""; if(@A>=1){ $prons = join(" ", @A);} $vocab{$word} = $prons; } close F;
    while(<STDIN>) {chomp; @A = split(/[\s\t]+/); $utt = $A[0] . " " . $A[1] . " " . $A[2]; 
    for($i = 3; $i < @A; $i++){  $word = $A[$i];
      if(exists $vocab{$A[$i]}) {$word = $vocab{$A[$i]}; }  if($word !~ /[<]/){ $word = uc $word;  } 
      $utt .= " $word";
    }  print "$utt\n";
    } ' $transform_dict  > $data_overall/transcript.2.txt 
  cat $data_overall/transcript.2.txt | \
  perl -e 'use open qw(:std :utf8); 
     while(<>){chomp; @A = split(/[\s\t]/); for($i=3; $i< @A; ++$i) { print "$A[$i]\t$A[$i]\n";}  }'| \
   sort -u > $data_overall/wordlist.2.txt
  cat $data_overall/transcript.2.txt | \
  perl -e '($lex) = @ARGV; open(F, "<$lex") or die "ERROR, file $lex cannot open\n";
    while(<F>) { @A  = split(/[\s\t]+/); $w = shift @A; $vocab{$w} ++;  } close F;
    while(<STDIN>) {chomp; @A = split(/[\s\t]+/); for($i=3; $i<@A; $i++){ $w = uc $A[$i]; if(not exists $vocab{$w}) { $oov_vocab{$w}++; } }  } 
    foreach $key (sort {$oov_vocab{$b}<=>$oov_vocab{$a} } keys %oov_vocab) {
      if ($key !~ /[\x00-\x7f]/) {
        print STDERR "$key\t $oov_vocab{$key}\n";
      } else {
        print "$key\t$oov_vocab{$key}\n";
      }
    }
  '  $dict/lexicon-${langid}-${mandarin_lang_id}.txt  >$dict/oov.2.txt 2>&1 &
  echo "## LOG: done ($data_overall/transcript.2.txt, $data_overall/wordlist.2.txt, $dict/oov.2.txt) "
fi 
tgt_dict=$tgtdir/data/local/dict
lang=$tgtdir/data/lang
if [ ! -z $step08 ]; then
  echo "## LOG: prepare dict/lang @ `date`"
  [ -d $tgt_dict ] || mkdir -p $tgt_dict
  cat $dict/lexicon-${langid}-${mandarin_lang_id}.txt | \
  perl -ane 'chomp; @A = split(/\s+/); shift @A; 
    for($i = 0; $i < @A; $i++){ $phone = $A[$i]; print "$phone\n"; }' | sort -u > $tgt_dict/nonsilence_phones.txt
  echo "SIL" > $tgt_dict/optional_silence.txt
  echo "SIL" > $tgt_dict/silence_phones.txt
  perl -e '($s) = @ARGV; @A = split(/[:]/,$s); for($i = 0; $i < @A; $i++){ $pair = $A[$i]; 
    @B = split(/[,]/, $pair); print "$B[1]\n";
  }'  "$silence_words"  >> $tgt_dict/silence_phones.txt
  cat $tgt_dict/silence_phones.txt | perl -e 'while(<STDIN>){chomp; print "$_ "; } print "\n"; '  > $tgt_dict/extra_questions.txt
  cat $tgt_dict/nonsilence_phones.txt | \
  grep "_$mandarin_lang_id"  | perl -e 'while(<STDIN>){chomp; print "$_ "; } print "\n"; ' \
  >> $tgt_dict/extra_questions.txt
  cat $tgt_dict/nonsilence_phones.txt | \
  grep "_$langid" | perl -e 'while(<STDIN>){chomp; print "$_ "; } print "\n"; ' \
  >> $tgt_dict/extra_questions.txt
  perl -e '($s) = @ARGV; @A = split(/[:]/, $s); for($i = 0; $i < @A; $i++){ $pair = $A[$i]; 
    @B = split(/[,]/, $pair); print "$B[0]\t$B[1]\n";
  }'  "$silence_words"  | cat - <(cat $dict/lexicon-${langid}-${mandarin_lang_id}.txt|sort -u ) > $tgt_dict/lexicon.txt
  utils/validate_dict_dir.pl $tgt_dict
  utils/prepare_lang.sh $tgt_dict "<unk>" $lang/tmp $lang
  echo "## LOG: step08, dict($tgt_dict), lang($lang) prepared @ `date`"
fi
kaldi_data=$tgtdir/data/overall-train
if [ ! -z $step09 ]; then
  echo "## LOG: step09, prepare overall kaldi data @ `date`"
  [ -d $kaldi_data ] || mkdir -p $kaldi_data
  cp $data_overall/wav.scp $kaldi_data/
  cat $data_overall/transcript.2.txt | \
  perl -e  ' use utf8; use open qw(:std :utf8);
    ($segfile, $utt2spk, $textfile) = @ARGV; 
    open(SEG, ">", "$segfile") or die "ERROR, segfile $segfile cannot open\n"; 
    open(U2S, ">", "$utt2spk") or die "ERROR, utt2spk $utt2spk cannot open\n";
    open(TXT, ">", "$textfile") or die "ERROR, txtfile $textfile cannot open\n ";
    while(<STDIN>) { chomp; @A = split(/[\s\t]/); $seg = shift @A; $start = shift @A; $end = shift @A; 
      $stime = $start, $etime = $end;
      @B = split(/[\-]/, $seg);  $spk = sprintf("%s-%s-%s", $B[0], $B[1], $B[2]);
      $start = sprintf("%07d", $start * 100); $end = sprintf("%07d", $end * 100);
      $segId = sprintf("%s-%s-%s", $seg, $start, $end);
      print SEG "$segId $seg $stime $etime\n";
      print U2S "$segId $spk\n";
      print TXT "$segId ", join(" ", @A), "\n";
    } close SEG; close U2S; close TXT;
 '  $kaldi_data/segments $kaldi_data/utt2spk $kaldi_data/text
    utils/utt2spk_to_spk2utt.pl < $kaldi_data/utt2spk > $kaldi_data/spk2utt
    utils/fix_data_dir.sh $kaldi_data

  echo "## LOG: step09, done @ `date`"
fi
devdata=$tgtdir/data/dev
traindata=$tgtdir/data/train
if [ ! -z $step10 ]; then
  echo "## LOG: step10, make train/dev data @ `date`"
  [ -d $traindata ] || mkdir -p $traindata
  [ -d $devdata ] || mkdir -p $devdata
  cat $kaldi_data/text | \
  perl -e 'use utf8; use open qw(:std :utf8); while(<STDIN>) {
    if(/\p{Han}+/g){ print "$_";}
  }' | utils/shuffle_list.pl | \
  perl -e '($segments, $hour) = @ARGV; 
    open(S, "$segments") or die "## ERROR: segments $segments cannot open\n"; 
    while(<S>){chomp; @A = split(/\s+/); $vocab{$A[0]} = $A[3] - $A[2];  } close S;
    $limit = $hour * 3600; $selected = 0;
    while(<STDIN>) {chomp; m/(\S+)\s+(.*)/; if(not exists $vocab{$1}){ die "## ERROR: $1 ($_) not in $segments\n"; } 
      $selected += $vocab{$1}; if($selected > $limit) {last;} print "$_\n";
    }
  ' $kaldi_data/segments  1.6 | sort -u  > $devdata/text.1
  utils/subset_data_dir.sh --utt-list $devdata/text.1 $kaldi_data  $devdata
  mv $devdata/utt2spk $devdata/old-utt2spk
  cat $devdata/old-utt2spk | perl -ane 'chomp; @A=split(/\s+/); print "$A[0] $A[0]\n"; ' > $devdata/utt2spk
  utils/utt2spk_to_spk2utt.pl < $devdata/utt2spk > $devdata/spk2utt
  utils/fix_data_dir.sh $devdata
  cat $kaldi_data/segments | \
  perl -e '($segments) = @ARGV; open(S, "$segments") or die "## ERROR: segments ($segments) cannot open\n";
    while(<S>) {chomp; m/(\S+)\s+(.*)/; $vocab{$1}++;} close S;
    while(<STDIN>){chomp; m/(\S+)\s+(.*)/; if(not exists $vocab{$1}){print "$_\n";}  }
  ' $devdata/segments  > $traindata/uttlist
  utils/subset_data_dir.sh --utt-list $traindata/uttlist $kaldi_data  $traindata
  utils/fix_data_dir.sh $traindata
  echo "## LOG: step10, dev data ($devdata/text) done @ `date`"
fi
if [ ! -z $step11 ]; then
  echo "## LOG: step11, make arpa lm and G @ `date`"
  mv $traindata/text  $traindata/text.1
  cat $traindata/text.1 | perl -ane 'use utf8; use open qw(:std :utf8);  s:<[^<]+>::g; print;' | \
  awk '{if(NF>1){print;}}' > $traindata/text
  source/egs/fisher-english/train-kaldi-lm.sh --lmtype $lmtype  $tgt_dict/lexicon.txt \
  $traindata $devdata  $tgtdir/data/local/lm
  arpa_lm_dir=$tgtdir//data/local/lm/$lmtype
  [ -f $arpa_lm_dir/lm_unpruned.gz ] || \
  { echo "## ERROR:  lm_unpruned.gz expected"; exit 1; }
  source/egs/fisher-english/arpa2G.sh $arpa_lm_dir/lm_unpruned.gz $lang $lang
  mv $traindata/text.1  $traindata/text
  echo "## LOG: step11, done @ `date`"
fi
if [ ! -z $step12 ]; then
  echo "## LOG: step12, mfcc_pitch & fbank_pitch @ `date`"
  for x in train dev; do
    sdata=$tgtdir/data/$x;  data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    utils/fix_data_dir.sh $sdata
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc.conf --pitch-config conf/pitch.conf" \
    $sdata $data $feat || exit 1
    data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf --pitch-config conf/pitch.conf" \
    $sdata $data $feat || exit 1
  done

  echo "## LOG: step12, mfcc_pitch & fbank_pitch @ `date`"
fi
if [ ! -z $step13 ]; then
  echo "## LOG: step13, gmm-hmm training @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd run.pl --nj 14 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 7000 --pdf-num 210000 \
  --devdata $devdata/mfcc-pitch \
  $traindata/mfcc-pitch $lang $tgtdir/exp/mono || exit 1
  echo "## LOG: step13, done @ `date`"
fi

