#!/bin/bash

echo
echo "## LOG: $0 $@"
echo

. path.sh
. cmd.sh

# begin options
cmd=run.pl
nj=4
# end options 

function Usage {
 cat<<END
 Usage: $0 [options] <srcdata> <data> <feat>
 [options]:
 --cmd                             # value, "$cmd"
 --nj                              # value, $nj
 [examples]:

 $0  /home2/hhx502/sg-en-i2r-16k/data/train /home2/hhx502/sg-en-i2r-16k/data/train/i2r-mfcc \
 /home2/hhx502/sg-en-i2r-16k/data/train/feat/i2r-mfcc 

END
}


. parse_options.sh || exit 1

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

srcdata=$1
data=$2
feat=$3

source_dir=/data/users/hhx502/sg-en-source-sept-2016/FromI2R

feat_cmd=$source_dir/frontend/compute-i2r-feats
config=$source_dir/frontend/frontend.conf
name=$(basename $data)
[ -d $data ] || mkdir -p $data
cp $srcdata/* $data/
[ -d $feat/data ] || mkdir -p $feat/data
featdir=$(cd  $feat/data; pwd)
logdir=$feat/log
[ -d $logdir ] || mkdir -p $logdir
split_segments=""
for n in $(seq $nj); do
  split_segments="$split_segments $logdir/segments.$n"
done
utils/split_scp.pl $data/segments $split_segments || exit 1
rm $data/{feats.scp,cmvn.scp} 2>/dev/null
for x in wav.scp segments; do
   [ -e $data/$x ] || { echo "## ERROR ($0): file $x expected fomr $data"; exit 1; }
done

echo "## LOG ($0): feature extraction started @ `date`"
$cmd JOB=1:$nj $logdir/make_${name}.JOB.log \
extract-segments scp,p:$data/wav.scp $logdir/segments.JOB ark:- \| \
$feat_cmd --frontend=$config --wav-input=true ark:- ark,scp:$featdir/$name.JOB.ark,$featdir/$name.JOB.scp || exit 1

# concatenate the .scp file together
for n in $(seq $nj); do
  cat $featdir/$name.$n.scp || exit 1;
done > $data/feats.scp

rm $featdir/$name.*.scp $logdir/segments.* 2>/dev/null

nf=`cat $data/feats.scp | wc -l` 
nu=`cat $data/utt2spk | wc -l` 
if [ $nf -ne $nu ]; then
  echo "It seems not all of the feature files were successfully processed ($nf != $nu);"
  echo "consider using utils/fix_data_dir.sh $data"
fi
echo "## LOG ($0): feature extraction done @ `date`"

