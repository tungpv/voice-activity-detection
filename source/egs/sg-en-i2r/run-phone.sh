#!/bin/bash

. path.sh
. cmd.sh

echo 
echo "$0 $@"
echo

# begin options
cmd="slurm.pl --exclude=node01,node02,node05"
nj=40
steps=
word_dict_dir=
phone2word_map_csl="SIL,<silence>:<oov>,<unk>:ns,<noise>"
train_data_csl=
trainname=train
dev_data=
dev_data_csl=
devname=dev

# end options

. parse_options.sh

function Usage {
 cat<<END
 $0 [options] <train-data> <lang> <alidir> <tgtdir>
 [options]:
 --cmd                                    # value, "$cmd"
 --nj                                     # value, "$nj"
 --steps                                  # value, "$steps"
 --word-dict-dir                          # value, "$word_dict_dir"
 --phone2word-map-csl                     # value, "$phone2word_map_csl"
 --train-data-csl                         # value, "$train_data_csl"
 --trainname                              # value, "$trainname"
 --dev-data                               # value, "$dev_data"
 --dev-data-csl                           # value, "$dev_data_csl"
 --devname                                # value, "$devname"

 [steps]:
 step04: train phone-gmm-hmm model

 [examples]:

 $0 --steps 1,2 --cmd "$cmd" --nj 40 \
 --phone2word-map-csl "SIL,<silence>:<vns>,<v-noise>:<oov>,<unk>:<sss>,<noise>" \
 --trainname train  --devname dev \
 --dev-data /home2/hhx502/sg-en-i2r/data/dev/mfcc-pitch \
 --train-data-csl "/home2/hhx502/sg-en-i2r/data/train/mfcc-pitch,train/mfcc-pitch:/home2/hhx502/sg-en-i2r/data/train/fbank-pitch,train/fbank-pitch" \
 --dev-data-csl "/home2/hhx502/sg-en-i2r/data/dev/mfcc-pitch,dev/mfcc-pitch:/home2/hhx502/sg-en-i2r/data/dev/fbank-pitch,dev/fbank-pitch" \
--word-dict-dir /home2/hhx502/sg-en-i2r/data/local/dict \
/home2/hhx502/sg-en-i2r/data/train/mfcc-pitch \
/home2/hhx502/sg-en-i2r/data/lang /home2/hhx502/sg-en-i2r/exp/mono/tri4a/ali_train \
/home2/hhx502/sg-en-i2r/phone

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi
train_data=$1
word_lang=$2
ali_dir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  echo "## LOG: $0, step01, prepare training data @ `date`" 
  if [ ! -z "$train_data_csl" ]; then
    source/egs/ss-apsipa/make-phone-data.sh --nj $nj --cmd "$cmd" \
    --steps 1,2,3 --dataname $trainname  --ali-dir $ali_dir \
    --phone2word-map-csl "$phone2word_map_csl" \
    --data-csl "$train_data_csl" \
    data-dummy $word_lang sdir-dummy $tgtdir/data || exit 1
  fi
  echo "## LOG: dev_data_csl=$dev_data_csl, dev_data=$dev_data"
  if [ ! -z "$dev_data_csl" ] && [ ! -z $dev_data ]; then
    source/egs/ss-apsipa/make-phone-data.sh --nj $nj --cmd "$cmd" \
    --steps 1,2,3 --dataname dev  \
    --phone2word-map-csl "$phone2word_map_csl" \
    --data-csl "$dev_data_csl" \
    $dev_data $word_lang $ali_dir $tgtdir/data || exit 1
  fi
  echo "## LOG: $0, step01, data preparation done '$tgtdir/data' @ `date`"
fi
phone_dict_dir=$tgtdir/data/local/dict
phone_lang=$tgtdir/data/lang
if [ ! -z $step02 ]; then
  echo "## $0, step02, prepare phone lang @ `date`"
  [ ! -z $word_dict_dir ] || { echo "## ERROR: word_dict_dir not specified"; exit 1; }
  [ -d $phone_dict_dir ] || mkdir -p $phone_dict_dir
  textfile=$(find $tgtdir/data/$trainname -name "text" | egrep -v '\.backup'| head -1)
  [ -z $textfile ] &&{ echo "## ERROR: textfile text not found"; exit 1;  }
  cat $textfile | \
  perl -e '($phoneFile) = @ARGV; open(F, "<$phoneFile") or die "## ERROR: file $phoneFile cannot open\n";
    while(<F>) { chomp; m/(\S+)/ or next; $vocab{$1} ++; } close F;
    while(<STDIN>) { m/(\S+)\s+(.*)/ or next; @A = split(/\s+/, $2);
      for($i= 0; $i< @A; $i++) { $s = $A[$i]; if(exists $vocab{$s}) { print $s, "\n";   }  }
    } ' $word_dict_dir/nonsilence_phones.txt |sort -u > $phone_dict_dir/nonsilence_phones.txt || exit 1
  cp $word_dict_dir/silence_phones.txt $phone_dict_dir/silence_phones.txt || exit 1
  cp $word_dict_dir/optional_silence.txt $phone_dict_dir/optional_silence.txt
  cat $phone_dict_dir/silence_phones.txt |awk '{printf("%s ", $0);}END{printf("\n");}' > $phone_dict_dir/extra_questions.txt 
  cat $phone_dict_dir/nonsilence_phones.txt | awk '{printf("%s ", $0);}END{printf("\n");}' >> $phone_dict_dir/extra_questions.txt
  cat $phone_dict_dir/nonsilence_phones.txt | perl -ane 'chomp; print "$_\t$_\n";' > $phone_dict_dir/dict.1
  (
    A=($(echo "$phone2word_map_csl" |  tr ':' ' ')) 
    num=${#A[@]}
    for x in $(seq 0 $[num-1]); do
      B=($(echo ${A[$x]} | tr ',' ' ')) 
      echo -e "${B[1]}\t${B[0]}"
    done
  ) | cat - $phone_dict_dir/dict.1 > $phone_dict_dir/lexicon.txt
  utils/validate_dict_dir.pl  $phone_dict_dir || exit 1
  utils/prepare_lang.sh --position-dependent-phones false  $phone_dict_dir "<unk>" $tgtdir/data/lang/tmp $phone_lang || exit 1
  source/egs/quesst/make_word_boundary_int.sh $phone_lang/phones
  echo "## $0, step02, phone lang preparation done ('$phone_lang/phones') @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "## LOG: $0, step03, prepare phone loop @ `date`"
  source/egs/quesst/make-phone-loop.sh $phone_dict_dir/lexicon.txt \
  $tgtdir/data/local/phone-loop 
  source/egs/fisher-english/arpa2G.sh $tgtdir/data/local/phone-loop/lm.gz \
  $tgtdir/data/lang $tgtdir/data/lang
  echo "## LOG: $0, step03, done '$lang/G.fst'"
fi
featname=$(basename $train_data)
train_data=$tgtdir/data/$trainname/$featname
[ -f $train_data/feats.scp ] ||
dev_data=$tgtdir/data/$devname/$featname
if [ ! -z $step04 ]; then
  echo "## LOG: step04, train gmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd "$cmd" --nj $nj \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 5000 --pdf-num 75000 \
  --devdata $dev_data\
  $train_data $phone_lang $tgtdir/exp || exit 1
  echo "## LOG: step04, done, checking '$tgtdir/exp/tri4a' @ `date`"
fi
lmdir=$tgtdir/data/local/lm
lang_test=$tgtdir/data/lang_test
if [ ! -z $step05 ]; then
  echo "## LOG: step05, build 4-gram lm & G.fst @ `date`"
  lmtype=4gram-mincount
  textfile=$(find $tgtdir/data/$trainname -name "text" | egrep -v '\.backup'| head -1)
  [ -z $textfile ] && { echo "## ERROR: textfile expected fomr $tgtdir/data/$trainname"; exit 1; }
  train_data_dir=$(dirname $textfile)
  textfile=$(find $tgtdir/data/$devname -name "text" | egrep -v '\.backup'| head -1)
  [ -z $textfile ] && { echo "## ERROR: textfile expected fomr $tgtdir/data/$devname"; exit 1; }
  dev_data_dir=$(dirname $textfile)
  cp $train_data_dir/text  $train_data_dir/text-backup
  cat $train_data_dir/text-backup | perl -pe 's/<silence>//g;' > $train_data_dir/text 
  source/egs/fisher-english/train-kaldi-lm.sh --lmtype 4gram-mincount \
  $phone_dict_dir/lexicon.txt $train_data_dir $dev_data_dir $lmdir 
  lmfile=$lmdir/$lmtype/lm_unpruned.gz
  [ -f $lmfile ] || \
  { echo "## ERROR: $lmfile is not built"; exit 1; }
  cp $train_data_dir/text-backup $train_data_dir/text || exit 
  cp -r $phone_lang $lang_test
  source/egs/fisher-english/arpa2G.sh $lmfile \
  $lang_test $lang_test 
  echo "## LOG: step05, done @ `date`"
fi
graph_test=$tgtdir/exp/tri4a/graph_4glm_test
decode_dir=$tgtdir/exp/tri4a/decode-${devname}_4glm_test
if [ ! -z $step06 ]; then
  echo "## LOG: step06, building lm @ `date`"
  utils/mkgraph.sh $lang_test $tgtdir/exp/tri4a $graph_test 
  echo "## LOG: $0, step06, done '$graph_test' @ `date`"
fi
if [ ! -z $step07 ]; then
  echo "## LOG: $0, step07, decode @ `date`"
  steps/decode_fmllr.sh --cmd "$cmd" --nj 40 \
  --scoring-opts "--min-lmwt 8 --max-lmwt 25" --acwt 0.1 --skip-scoring false \
  $graph_test $tgtdir/data/$devname/$featname $decode_dir || exit 1 
  echo "## LOG: $0, step07, decode done @ `date`"
fi
if [ ! -z $step08 ]; then
  echo "## LOG: $0, step08, @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2,3,4 \
  --devdata $tgtdir/data/$devname/fbank-pitch --graphdir $graph_test \
  --decodename decode-dev-4glm $tgtdir/data/train/fbank-pitch \
  $lang_test /home2/hhx502/sg-en-i2r/exp/mono/nnet5a/dnn \
  $tgtdir/exp/tri4a/ali_train $tgtdir/exp/nnet5a-tl || exit 1
  echo "## LOG: $0, step08 done @ `date`"
fi
