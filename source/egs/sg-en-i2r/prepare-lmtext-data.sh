#!/bin/bash

. path.sh || exit 1
. cmd.sh

# begin options
steps=
trans_dict=
# end options 
# Usage:
# ./parepare-data.sh --steps <step-number>
# e.g.: ./prepare-data.sh --steps 1, or ./prepare-data.sh --steps 1,2,3,4,5,6 (no recommended)

. parse_options.sh || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
function Usage {
  cat <<END

 Usage:
 $(basename $0) [options] <lmtext> <lang> <dir>
 [options]:
 --trans-dict                                  # value, "$trans_dict"
 [steps]:
 1: make trans-dict.txt
 2: make trans-dict edited
 3: transform the text for the first time

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

lmtext=$1
lang=$2
dir=$3
[ -d $dir ] || mkdir -p $dir
trans_dict_01=$dir/trans-dict.txt
if [ ! -z $step01 ]; then
  echo "step01: make transform dict from text file $lmtext"
  cat $lmtext |\
  perl -e 'use open qw(:std :utf8); 
    while(<STDIN>) { chomp; @A = split(/[\s]+/); 
      for($i=0; $i<@A; ++$i) { print "$A[$i]\t$A[$i]\n";}  }' | \
      sort -u > $trans_dict_01
  echo "step01: check trans-dict $dir/$trans_dict_01 !"
fi
trans_dict_edit=$dir/trans-dict-edit.txt
if [ ! -z $step02 ]; then
  echo "step02: edit trans_dict $trans_dict_01"
  cat $trans_dict_01 | \
  perl -e 'use open qw(:std :utf8);
    while(<STDIN>) {@A = split(/[\t]/); if(m/\p{Han}+/){ print "$A[0]\t<unk>\n"; }else{print;} }' | \
  perl -e 'use open qw(:std :utf8); 
    while(<STDIN>){@A = split(/[\t]/); if(length($A[0]) > 30){print "$A[0]\t<unk>\n";} else {print; } }' > $trans_dict_edit
  echo "step02: done with $trans_dict_edit"
fi
trans_text=$dir/text
if [ ! -z $step03 ]; then
  echo "step03: transform the text using $trans_dict_edit"
  cat $lmtext | \
  perl -e 'use open qw(:std :utf8); ($dict) = @ARGV; open(F, "<", "$dict") or die "ERROR, cannot open file $dict\n";
    while(<F>) {chomp; @A = split(/[\t]/); die "ERROR, bad line $_\n" if @A != 2; if($A[1] eq "<unk>") { $vocab{$A[0]} = ""; } else { $vocab{$A[0]} = $A[1];  }  }  close F;
    while(<STDIN>) {chomp; @A = split(/[\s]+/); $utt = ""; for($i=0; $i<@A; ++$i) { $w = $A[$i];  if(exists $vocab{$w}) { $utt .= "$vocab{$w} ";}else{ print STDERR "WARNING, oov word $w\n"; $utt .= "$w ";  }   } print "$utt\n";  }
  ' $trans_dict_edit | \
  perl -pe 'use open qw(:std :utf8); $linenum=0; while (<STDIN>) { chomp; $s = uc $_; $num = sprintf("%07d", ++$linenum); print "$num $s\n";  }' > $trans_text
  echo "step03: transformed text $trans_text generated !"
fi

