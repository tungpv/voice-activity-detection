#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <sge-lexicon.txt> <en-lexicon.txt> <tgtdir> 
 [options]:
 --steps                               # value, "$steps"
 [steps]:

 [examples]:

 $0 --steps 1 /home2/hhx502/sg-en-i2r/data/local/dict/lexicon.txt \
 /home2/hhx502/ldc-cts2016/fisher-english/data/local/dict/lexicon.txt \
 /home2/hhx502/sg-en-i2r/data/local/sge-to-en-phone-map 

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

sge_lex=$1
en_lex=$2
tgtdir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $tgtdir ] || mkdir -p $tgtdir
dict=$tgtdir/dict
if [ ! -z $step01 ]; then
  echo "## step01, filter sge lexicon"
  [ -d $dict ] || mkdir -p $dict
  cat $sge_lex | \
  perl -e 'use utf8; use open qw(:std :utf8);
    while(<STDIN>) {next if(/</||/\p{Han}+/); print;  } ' > $dict/sge-lex.txt
  cat $en_lex | \
  perl -e 'use utf8; use open qw(:std :utf8);
    while(<STDIN>) { chomp; next if (/</); @A = split(/\s+/); $w = shift @A;  $pron = "";
      for($i = 0; $i < @A; $i++) { if($i==0){$pron = $A[$i] ."_eng";}else{$pron .= " $A[$i]_eng";} }
      print "$w\t$pron\n";
    }' > $dict/eng-lex.txt
  cat $dict/sge-lex.txt | \
  perl -e ' ($eng_lex_file, $phone_map_file) = @ARGV;  open(F, "$eng_lex_file") or die "## ERROR: eng_lex_file $eng_lex_file cannot open\n";
    while(<STDIN>) {chomp; @A = split(/\s/); $w = shift @A;  }
   
  '  $dict/eng-lex.txt $dict/phone-map.txt
  echo "## step02, done sge($dict/sge-lex.txt) lexicon"
fi
