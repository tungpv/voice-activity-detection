#!/bin/bash

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh

set -e
# begin options
steps=
cmd=slurm.pl
nj=40
dataname=sg_en_8kcts_16kmob_merge
src_nnet_dir=/home2/hhx502/sg-en-i2r/exp/mono/nnet5a/dnn
# end options

function Usage {
 cat<<END

 $0 [options] <lang> <data> <source_dir> <tgtdir> 
 [options]:
 --nj                                         # value, "$cmd"
 --dataname                                   # value, "$dataname"
 --src-nnet-dir                               # value, "$src_nnet_dir"

 [examples]:

 $0 --steps 1 --cmd "$cmd" --nj $nj \
 --dataname $dataname \
 /home2/hhx502/sg-en-i2r/data/update-knowledge-source-01-10-2016/lang \
 /home2/hhx502/sg-en-i2r/data/update-knowledge-source-01-10-2016/train \
 /home2/hhx502/sg-en-i2r/exp/mono/tri4a \
 /home2/hhx502/sg-en-i2r/exp/update-am-01-10-2016 

END
}

. parse_options.sh || exit 1

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

lang=$1
data=$2
source_dir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
alidir=$tgtdir/ali-${dataname}
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): align data @ `date`"
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj \
  $data/mfcc-pitch $lang $source_dir $alidir || exit 1
  echo "## LOG (step01, $0): done with data alignment @ `date`"
fi
if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): update gmm to new tri4a @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "slurm.pl --exclude=node01" --nj $nj \
  --steps 1,2,3,4,5,6,7 \
  --cmvn-opts "--norm-means=true"  --state-num 7500 --pdf-num 10000 \
  --devdata /home2/hhx502/sg-en-i2r/data/dev/mfcc-pitch \
  --alidir $alidir \
  $data/mfcc-pitch $lang  $tgtdir || exit 1
  echo "## LOG (step02, $0): done"
fi
alidir_from_nnet=$src_nnet_dir/ali-${dataname}
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): make alignment from nnet ($src_nnet_dir) @ `date`"
  steps/nnet/align.sh --cmd "slurm.pl --exclude=node01" --nj $nj \
  $data/fbank-pitch $lang $src_nnet_dir $alidir_from_nnet || exit 1
  echo "## LOG (step03, $0): done ('$alidir_from_nnet')"
fi
tgt_nnet_dir=$tgtdir/nnet5a-tl-with-gmm-retrained
if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): train dnn by transfer learning @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --cmd "slurm.pl" --nj $nj \
  --steps 1,2,3,4 \
  --devdata /home2/hhx502/sg-en-i2r/data/dev/mfcc-pitch \
  --graphdir $tgtdir/tri4a/graph \
  --decodename "decode-dev" \
  $data/fbank-pitch $lang $src_nnet_dir $tgtdir/tri4a/ali_train \
  $tgt_nnet_dir || exit 1
  echo "## LOG (step04, $0): done ('$tgt_nnet_dir') @ `date`"
fi
tgt_nnet_dir=$tgtdir/nnet5a-tl-with-sg-en8k-gmm
ali=$alidir_from_nnet
if [ ! -z $step05 ]; then
  echo "## LOG (step05, $0): transfer learning for ('$tgt_nnet_dir') @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --cmd "slurm.pl" --nj $nj \
  --steps 1,2,3,4 \
  --devdata /home2/hhx502/sg-en-i2r/data/dev/mfcc-pitch \
  --graphdir $tgtdir/tri4a/graph \
  --decodename "decode-dev" \
  $data/fbank-pitch $lang $src_nnet_dir $ali \
  $tgt_nnet_dir || exit 1
  echo "## LOG (step05, $0): done ('$tgt_nnet_dir') @ `date`"
fi
