#!/bin/bash 

echo
echo "## LOG: $0 $@"
echo

. path.sh
. cmd.sh

# begin options
steps=
cmd=slurm.pl
nj=40
phone_map=source/egs/sg-en-i2r/sge-phone-to-en-phone-map.txt
lex_id=30k
train_source=sg-en-i2r/data/train
dev_source=sg-en-i2r/data/dev-20spk
lmtype=3gram-mincount
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF
 
 Usage: $0 [options] <tgtdir>
 [options]:
 [steps]:
 [example]:

 $0  --steps 1 --phone-map $phone_map --lex-id $lex_id  \
 --train-source $train_source --dev-source $dev_source \
 /home2/hhx502/sg-en-i2r/cmudict 

EOF
}

if [ $# -ne 1 ]; then
  Usage && exit 1
fi

tgtdir=$1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
tgtdata=$tgtdir/data-${lex_id}
tgtexp=$tgtdir/exp-${lex_id}
srclex=/home/hhx502/w2016/sg-en-i2r/data/local
tgtdict=$tgtdata/local/dict
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): prepare dict"
  [ -d $tgtdict ] || mkdir -p $tgtdict
  cat $srclex/lexicon.txt | grep -v '<' | \
  source/egs/sg-en-i2r/convert-sge-lexicon.pl $phone_map | \
  source/egs/sg-en-i2r/lowercase-word-of-dict.pl > $tgtdict/lexicon_src.txt
  grep '<' $srclex/lexicon.txt | \
  cat - $tgtdict/lexicon_src.txt > $tgtdict/lexicon.txt
  cat $tgtdict/lexicon_src.txt | \
  perl -ane 'chomp; @A = split(/\s+/); shift @A; for($i = 0; $i< @A; $i++){print "$A[$i]\n";}' | \
  sort -u > $tgtdict/nonsilence_phones.txt
  grep '<' $srclex/lexicon.txt | \
  perl -ane 'chomp; @A = split(/\s+/); shift @A; for($i = 0; $i< @A; $i++){print "$A[$i]\n";}' | \
  sort -u > $tgtdict/silence_phones.txt
  echo "SIL" > $tgtdict/optional_silence.txt
  echo -n > $tgtdict/extra_questions.txt
  utils/validate_dict_dir.pl $tgtdict
  echo "## LOG (step01, $0): dict ($tgtdict) preparation done"
fi
lang=$tgtdata/lang
if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): prepare lang"
  utils/prepare_lang.sh $tgtdict "<unk>" $lang/tmp $lang
  echo "## LOG (step02, $0): lang preparation done"
fi
train_mfcc=$tgtdata/train/mfcc-pitch
dev_mfcc=$tgtdata/dev/mfcc-pitch
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): copy train data"
  [ -d $train_mfcc ] || mkdir -p $train_mfcc
  cp $train_source/mfcc-pitch/*   $train_mfcc
  cat $train_source/mfcc-pitch/text | \
  source/egs/sg-en-i2r/lowercase-kaldi-text.pl > $train_mfcc/text
  [ -d $dev_mfcc ] || mkdir -p $dev_mfcc
  cp $dev_source/mfcc-pitch/* $dev_mfcc
  cat $dev_source/mfcc-pitch/text | \
  source/egs/sg-en-i2r/lowercase-kaldi-text.pl > $dev_mfcc/text
  echo "## LOG (step03, $0): done"
fi
lmdir=$tgtdata/local/lm
if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): prepare lm"
  [ -d $lmdir ] || mkdir -p $lmdir
  traindata=$train_mfcc
  devdata=$dev_mfcc
  [ -f $traindata/text-history ] || mv $traindata/text $traindata/text-history
  cat $traindata/text-history | perl -ane 'use utf8; use open qw(:std :utf8);  s:<[^<]+>::g; print;' | \
  awk '{if(NF>1){print;}}' > $traindata/text
  source/egs/fisher-english/train-kaldi-lm.sh --lmtype $lmtype  $tgtdict/lexicon.txt \
  $traindata $devdata  $lmdir
  lmFile=$lmdir/$lmtype/lm_unpruned.gz
  [ -f $lmFile ] || \
  { echo "## ERROR (step04, $0): lm_unpruned.gz expected";  cp $traindata/text-history $traindata/text;  exit 1; }
   source/egs/fisher-english/arpa2G.sh $lmFile $lang $lang
  cp $traindata/text-history  $traindata/text
  echo "## LOG (step04, $0): done ('$lmdir', '$lang')"
fi

if [ ! -z $step05 ]; then
  echo "## LOG (step05, $0): train gmm-hmn @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd slurm.pl --nj 17 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 7000 --pdf-num 210000 \
  --devdata $dev_mfcc \
  $train_mfcc $lang $tgtexp || exit 1
  echo "## LOG (step05, $0): gmm-hmm ('$tgtexp') done @ `date`"
fi
