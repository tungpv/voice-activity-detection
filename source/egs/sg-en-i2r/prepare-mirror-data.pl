#!/usr/bin/perl

use warnings;
use strict;
use File::Path qw(make_path remove_tree);
use File::Basename;
use Getopt::Long;

my $cmdName = $0;
$cmdName =~ s/.*\///g;
our $prefix = "rev";
our $wave_path_field_index = 2; 
my $cmdFixDataDir = "utils/fix_data_dir.sh";

GetOptions('prefix=s' => \$prefix,
           'wave-path-field-index=i'=> \$wave_path_field_index,
           'cmd-fix-data-dir=s' => \$cmdFixDataDir);
# print STDERR "prefix=$prefix\n";

my $usage = << "END";

 $cmdName [options] <original-dir> <mirror-dir> <new-dir>
 [options]:
 --prefix                                 # value, "$prefix";
 --wave-path-field-index                  # value, $wave_path_field_index
 --cmd-fix-data-dir                       # value, "$cmdFixDataDir"

END

if (@ARGV != 3) {
  print $usage; exit 1;
}

my ($srcdir, $mirrordir, $newdir) = @ARGV;

sub CheckKaldiFolder {
  my ($srcdir) = @_; 
  foreach my $filename  ("wav.scp", "text", "segments", "utt2spk") {
    my $x = "$srcdir/$filename";
    if (! -e $x) {
      die "ERROR, file $x expected\n";
    }
  }
}
sub MakeDir {
  my ($newdir) = @_;
  make_path("$newdir");
  if(! -e $newdir) {
    die "ERROR, failed to make folder $newdir";
  }
}
sub CollectWavFile {
  my ($srcdir, $newdir, $wavList) = @_;
  if(! -e $$wavList) {
    `find $srcdir -name "*.wav" | sort -u > $$wavList`;
  }
  open(F, "<", $$wavList) or die "ERROR, cannot open file $$wavList\n";
  my $lineNumber = 0;
  while(<F>) {
    $lineNumber ++;
  }
  close F;
  if($lineNumber == 0) {
    `rm $$wavList`;
    die "ERROR, no wav files in folder $srcdir\n";
  }
  print STDERR "LOG: $lineNumber wav files collected from folder $srcdir\n";
}
sub Basename {
  my ($path, $basename) = @_;
  my $s = $path;
  $s =~ s/.*\///g;
  $s =~ s/\.[^\.]+$//g;
  $$basename = $s;
}
sub MakeWavScp {
  my ($srcdir, $wavlist, $newdir) = @_;
  open(F, "<", "$srcdir/wav.scp") or die "ERROR, failed to open wav.scp from folder $srcdir\n";
  my %vocab = ();
  while(<F>) {
    chomp;
    my @A = split(/\s+/);
    die "ERROR, the field ($wave_path_field_index) of the wave path overflowed !\n"
    if $wave_path_field_index > @A or $wave_path_field_index <= 0;
    my $s = $A[$wave_path_field_index - 1];
    Basename($s, \$s);
    my $label = $A[0];
    $vocab{$s} = $label;
  }
  close F; 
  open(F, "<", "$wavlist") or die "ERROR, cannot open wavlist $wavlist\n";
  open(OF, ">", "$newdir/wav.scp") or die "ERROR, cannot open wav.scp for write in folder $newdir\n";
  while(<F>) {
    chomp;
    my $wavpath = $_;
    my $s = $wavpath;
    Basename($s, \$s);
    die "ERROR, unidentified file $wavpath\n" if not exists $vocab{$s};
    my $label = $vocab{$s};
    $label = $prefix . "-" . $label;
    print OF $label, " ", $wavpath, "\n";
  }
  close F;
  close OF;
  print STDERR "LOG: MakeWaveScp: $newdir/wav.scp created !\n";
}
sub RewriteSegment {
  my ($srcdir, $newdir) = @_;
  open(IF, "<", "$srcdir/segments") or die "ERROR, failed to open file $srcdir/segments\n";
  open(OF, ">", "$newdir/segments") or die "ERROR, failed to open file $newdir/segments to write\n";
  while(<IF>) {
    chomp;
    my @A = split(/\s+/);
    die "ERROR, bad line $_\n in $srcdir/segments\n" if @A != 4;
    my ($segId, $segFile, $start, $end) = @A;
    my $newSegId = $prefix . "-" . $segId;
    my $newSegFile = $prefix . "-" . $segFile;
    print OF "$newSegId $newSegFile $start $end\n";
  }
  close IF;
  close OF;
  print STDERR "LOG: RewriteSegment: $newdir/segments created !\n";
}
sub RewriteText {
  my ($srcdir, $newdir) = @_;
  open(IF, "<", "$srcdir/text") or die "ERROR, failed to open file $srcdir/text\n";
  open(OF, ">", "$newdir/text") or die "ERROR, failed to open file $newdir/text to write\n";
  while(<IF>) {
    chomp;
    m/(\S+)\s+(.*)/;
    my ($lab, $text) = ($1, $2);
    $lab = $prefix . "-" . $lab;
    print OF "$lab $text\n";
  }
  close IF;
  close OF;
  print STDERR "LOG: RewriteText: $newdir/text created !\n";
}
sub RewriteUtt2spk {
  my ($srcdir, $newdir) = @_;
  open(IF, "<", "$srcdir/utt2spk") or die "ERROR, failed to open file $srcdir/utt2spk\n";
  open(OF, ">", "$newdir/utt2spk") or die "ERROR, failed to open file $newdir/utt2spk\n";
  while(<IF>) {
    chomp;
    my ($utt, $spk) = split(/[\s]+/);
    $utt = $prefix . "-" . $utt;
    $spk = $prefix . "-" . $spk;
    print OF "$utt $spk\n";
  }
  close IF;
  close OF;
  print STDERR "LOG: RewriteUtt2spk: $newdir/utt2spk created ! \n";
}
sub Utt2spkToSpk2utt {
  my ($dir) = @_;
  open(IF, "<", "$dir/utt2spk") or die "ERROR, failed to open file $dir/utt2spk\n";
  open(OF, ">", "$dir/spk2utt") or die "ERROR, failed to open file $dir/spk2utt\n";
  my @spkList = ();
  my %vocab = ();
  my %seen = ();
  while(<IF>) {
    chomp;
    my @A = split(/[\s]+/);
    die "ERROR, bad line $_ in $dir/utt2spk\n" if @A != 2;
    if( not exists $seen{$A[1]}) {
      $seen{$A[1]} = 1;
      push @spkList, $A[1];
    }
    push @{$vocab{$A[1]}}, "$A[0]";
  }
  close IF;
  foreach my $spk (@spkList) {
    print OF "$spk ", join(" ", @{$vocab{$spk}}), "\n";
  }
  close OF;
  print STDERR "LOG: Utt2spkToSpk2utt: $dir/spk2utt created ! \n";
}
CheckKaldiFolder $srcdir;
MakeDir $newdir;

my $wavList = "$newdir/wavlist.txt";
CollectWavFile($mirrordir, $newdir, \$wavList);

MakeWavScp($srcdir, $wavList, $newdir);
RewriteSegment($srcdir, $newdir);
RewriteText($srcdir, $newdir);
RewriteUtt2spk($srcdir, $newdir);
Utt2spkToSpk2utt($newdir);

if ( ! -e $cmdFixDataDir ) {
  print STDERR "WARNING, cannot fix data, $cmdFixDataDir expected\n";
} else {
   print STDERR "LOG: fix folder $newdir\n";
   open (F, "|-" , "$cmdFixDataDir $newdir") or die "ERROR, cannot run pipe";
   close F;
}

print STDERR "Done !\n";
