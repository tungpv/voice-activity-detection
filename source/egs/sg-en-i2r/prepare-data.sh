#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
# end options 
# Usage:
# ./parepare-data.sh --steps <step-number>
# e.g.: ./prepare-data.sh --steps 1, or ./prepare-data.sh --steps 1,2,3,4,5,6 (no recommended)

. parse_options.sh || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

# find /data/users/tungpham/Singapore_English_I2R/delivery_20151030/ -type f | grep Wave > /data/users/hhx502/w2016/sg-en-i2r/wav-zip.list
data_root=/data/users/hhx502/w2016/sg-en-i2r
if [ ! -z $step01 ]; then
  for x in $(cat $data_root/wav-zip.list); do
    echo "$0: processing file $x ..."
    sub_dir=$(echo $x| perl -pe 'chmod; s:.*delivery_20151030\/::g; s:\.zip::;')
    tgt_dir=$data_root/$sub_dir
    [ -d $tgt_dir ] || mkdir -p $tgt_dir
    unzip -d $tgt_dir $x
  done
fi
if [ ! -z $step02 ]; then
  echo "prepare speaker info"
  cat /data/users/hhx502/w2016/sg-en-i2r/SpeakerInfo-CategoryI*-utf8-linux.txt | grep -v Gender | \
  perl -e 'while(<>){ chomp; @A = split(/[\s\t]+/); if(@A == 0 ){ next; }
  elsif(@A < 5){die "bad line: $_\n";}
  elsif(@A == 5) { if($A[0] =~ /^\d/){$A[0] = "Speaker".$A[0];} push(@A, "noavail"); $_ = join(" ", @A); print "$_\n";  }
  elsif(@A == 6 ) {if($A[0] =~ /^\d/){$A[0] = "Speaker".$A[0];} $_ = join(" ", @A); print "$_\n";  }
  else{ if($A[0] =~ /^\d/){$A[0] = "Speaker".$A[0];} $s = substr($A[5], 0, 1); for($i=6; $i < @A; $i++){$s .=substr($A[$i], 0 ,1);} splice(@A, 6); $A[5] = $s;  $_ = join(" ", @A);  print "$_\n";  }
  }' | \
  perl -e 'while(<>){chomp; @A= split(/[\s\t]+/); if(@A != 6) {die "ERROR, badline: $_"; } @B=(); $B[0] = $A[0]; $B[0] =~ s:Speaker:spkr:; 
   $B[1] = $A[1] . $A[2]; $B[1] = lc ($B[1]); $B[2] = substr($A[3], 0, 2); $B[2] = lc($B[2]);
   $B[3] = substr($A[4], 0, 3); $B[3] = lc($B[3]);
   $B[4] = substr($A[5], 0, 3); $B[4] = lc($B[4]);
   print $_. " ". join("-", @B) . "\n";
   }' >  $data_root/speaker-info.txt
  echo "done with preparing speaker info"
fi
# find /data/users/hhx502/w2016/sg-en-i2r/  -name "*.pcm" > /data/users/hhx502/w2016/sg-en-i2r/wavlist.txt
data_overall=sg-en-i2r/data/overall
if [ ! -z $step03 ]; then
  echo "prepare wav.scp"
  cat $data_root/wavlist.txt | \
  perl -e 'use File::Basename; ($infilename) = @ARGV; open(F, "<$infilename") or die "ERROR, file $infilename cannot open\n"; 
    while (<F>){ chomp; @A = split(/[\s\t]+/); die "ERROR, bad line $_\n" if (@A != 7); $vocab{$A[0]} = $A[6];  } close F; 
    while (<STDIN>){chomp; $fname = basename($_); $fname =~ s/\.pcm//;  if(/CategoryII/){$cate="c02";}elsif(/CategoryI/){$cate="c01";} else {die "ERROR, no Category name in file path\n";} 
      @A = split("-", $fname);  if (@A == 1){$index = 1;} elsif(@A==2) {$index = $A[1];} else {die "ERROR, bad filename $fname\n";}
      if (not exists $vocab{$A[0]}) {
        print STDERR "WARNING, unidentified speaker $A[0]\n"; next;
      } 
      $spker = $vocab{$A[0]}; $labname = $cate . "-" . $spker . "-" . sprintf("%02d", $index);
      print $labname, " /usr/bin/sox $_ -r 8000 -c 1 -b 16 -t wav - |\n";
    }
  '  $data_root/speaker-info.txt   > $data_overall/wav.scp
  echo "done with  preparing $data_overall/wav.scp"
fi
if [ ! -z $step04 ]; then
  echo "unzip the transcript"
  for x in $(cat $data_root/transcript-zip.list); do
    echo "$0: processing file $x ..."
    sub_dir=$(echo $x| perl -pe 'chomp; s:.*delivery_20151030\/::g; s:\.zip::;')
    xsub_dir=$(dirname $sub_dir)
    tgt_dir=$data_root/$xsub_dir
    [ -d $tgt_dir ] || mkdir -p $tgt_dir
    unzip -d $tgt_dir $x
  done
  echo "done with unzipping the transcript"
fi
# find /data/users/hhx502/w2016/sg-en-i2r/CategoryI/Script/ /data/users/hhx502/w2016/sg-en-i2r/CategoryII/Script/ -name "*.TextGrid" > /data/users/hhx502/w2016/sg-en-i2r/transcript.list
if [ ! -z $step05 ]; then
  echo "make the initial transcript"
  cat /data/users/hhx502/w2016/sg-en-i2r/transcript.list | \
  perl -e 'use utf8; use open qw(:std :utf8); ($wavfile) = @ARGV; open(F, "<$wavfile") or die "ERROR, file $wavfile cannot open\n"; 
    while(<F>) { chomp; if(/(\S+)\s+(\S+)\s+.*(Speaker.*)\.pcm\s+(.*)/) { $labname = $1; $fname = $3; $vocab{$fname} = $labname;  } else {die "ERROR, bad line $_\n";} } close F;
    while(<STDIN>) {chomp; if(/.*\/(Speaker.*)\.TextGrid/) { $spker = $1; if (not exists $vocab{$spker}) { print STDERR "WARNING: no pcm file for $_\n"; next; } $labname = $vocab{$spker}; open(F, "<$_") or die "ERROR, file $_ cannot open\n";
   if(not exists $voc{$labname}) {$voc{$labname}++;}else {die "ERROR, duplicated label $labname, for $spker\n";}
   while(<F>){ chomp; if(/xmin\s+=\s+(\S+)/) {$start = $1;  next; } if(/xmax\s+=\s+(\S+)/){$end = $1; next; } 
     if(/text\s+=\s+(.*)$/) { $text = $1; $text =~ s:[\"]::g;  print $labname, " ", $start, " ", $end, " $text\n"; next; }
   } close F;
  } else {die "ERROR, bad line $_\n";}   }
  '  sg-en-i2r/data/overall/wav.scp > /data/users/hhx502/w2016/sg-en-i2r/transcript.txt
  echo "done with the initial transcript: /data/users/hhx502/w2016/sg-en-i2r/transcript.txt"
fi
# dos2unix < /data/users/hhx502/w2016/sg-en-i2r/transcript.txt > /data/users/hhx502/w2016/sg-en-i2r/transcript-linux.txt
if [ ! -z $step06 ]; then
  echo "filter the initial transcript"
  cat /data/users/hhx502/w2016/sg-en-i2r/transcript-linux-man.txt | \
  grep -v '\<P\>' | \
  perl -pe 's#\[[^\[]+\]#<unk>#g;
    s#\[\]#<unk>#g;
    s#\*\*#<unk>#g;
    s#<SPK/>#<v-noise>#g;
    s#<@/># #g;
    s#<NON/>#<noise>#g;
    s#<STA/>#<noise>#g;
    s#<NPS/>#<v-noise>#g;
    s#\( \(\) \)#<unk>#g;
    s#\(spk\)#<v-noise>#g;
    s#\(sound\s+[\s\)]#<noise>#g;
    s#(sound)##g;
    s#\(s[oi]und##g;
    s#\(hes\)#<v-noise>#g;
  ' | \
   perl -e 'while(<>){chomp; @A = split(/[\s\t]+/); if(@A<=3){print STDERR "WARNING, empty utterance: $_\n"; next;}
     $has_output = 0; for($i =3; $i < @A; $i++) { if($A[$i]=~ /\</){next;}else{ $has_output=1; last;}}
     if($has_output ==0) {print STDERR "WARNING, un-normal utterance: $_\n"; next; }
     print "$_\n";
   }' > /data/users/hhx502/w2016/sg-en-i2r/transcript-linux-man-filtered.txt
   echo "done with the filtered transcript /data/users/hhx502/w2016/sg-en-i2r/transcript-linux-man-filtered.txt "
fi
if [ ! -z $step07 ]; then
  echo "make dict from /data/users/hhx502/w2016/sg-en-i2r/transcript-linux-man-filtered.txt"
  cat /data/users/hhx502/w2016/sg-en-i2r/transcript-linux-man-filtered.txt | \
  perl -e 'use open qw(:std :utf8); 
     while(<>){chomp; @A = split(/[\s\t]/); for($i=3; $i< @A; ++$i) { print "$A[$i]\t$A[$i]\n";}  }'| \
   sort -u > /data/users/hhx502/w2016/sg-en-i2r/wordlist-transcript.txt  
  echo "Generating file /data/users/hhx502/w2016/sg-en-i2r/wordlist-transcript.txt  done"
fi

# cp  /data/users/hhx502/w2016/sg-en-i2r/wordlist-transcript.txt  /data/users/hhx502/w2016/sg-en-i2r/wordlist-transcript-man.txt
man_dict=/data/users/hhx502/w2016/sg-en-i2r/wordlist-transcript-man-02.txt
tgt_file=/data/users/hhx502/w2016/sg-en-i2r/transcript-linux-man-filtered-dict.txt
cn_dict=/data/users/hhx502/w2016/sg-en-i2r/cn-dec-lex.txt

if [ ! -z $step08 ]; then
  echo "transform the transcript using manual dict "
  tgt_file=/data/users/hhx502/w2016/sg-en-i2r/transcript-linux-man-filtered-dict-02.txt
  cat /data/users/hhx502/w2016/sg-en-i2r/transcript-linux-man-filtered.txt | \
    perl -e ' ($dictfile) = @ARGV; open(F, "<$dictfile") or die "ERROR, file $dictfile cannot open\n";
    while(<F>) {chomp; @A = split(/[\s\t]+/); $word=shift @A;  $prons = ""; if(@A>=1){ $prons = join(" ", @A);} $vocab{$word} = $prons; } close F;
    while(<STDIN>) {chomp; @A = split(/[\s\t]+/); $utt = ""; for($i = 0; $i < @A; $i++){ if($i < 3){ $word = $A[$i];
      if ($i != 0) {$word = sprintf("%.2f",$word);}   $utt .= "$word ";  }
       else { $word = $A[$i]; if(exists $vocab{$A[$i]}) {$word = $vocab{$A[$i]}; }   if($i==3) {$utt .= "$word"; }else {$utt .= " $word";} } }  print "$utt\n";
    } ' $man_dict | \
  perl -e 'while(<>){chomp; @A = split(/[\s\t]/); ($lab, $start, $end) = ($A[0], $A[1], $A[2]);
   $utt = "$lab $start $end"; for($i = 3; $i < @A; $i ++) { $w = $A[$i]; if($w !~ /[\<]/){ $w = uc $w; } $utt .= " $w";   }  print "$utt\n";
  }' | \
  perl -e 'use open qw(:std :utf8); ($dict) = @ARGV; open(F, "<:encoding(UTF-8)", "$dict") or die "ERROR, dict $dict cannot open\n"; 
    while(<F>) { chomp; @A = split(/[\s\t]/); $w = shift @A;  $p = join(" ", @A); $vocab{$w} = $p; } close F; 
    while(<STDIN>) {chomp; @A = split(/[\s\t]/); $utt= ""; for($i = 0; $i < @A; ++$i) { 
      $w =$A[$i];  if(exists $vocab{$w}) {$w = $vocab{$w};} if ($i == 0) {$utt = $w;} else{ $utt .= " $w"; }  } 
      print "$utt\n";
   } ' $cn_dict > $tgt_file
  echo "done: $tgt_file"
fi
if [ ! -z $step09 ]; then
  cat $tgt_file | \
  perl -e 'use open qw(:std :utf8); 
     while(<>){chomp; @A = split(/[\s\t]/); for($i=3; $i< @A; ++$i) { print "$A[$i]\t$A[$i]\n";}  }'| \
   sort -u >  ${man_dict}-it01 
   echo "done with unique word list: ${man_dict}-it01"
fi

lex=sg-en-i2r/data/overall/lexicon-user.txt
en_oov=/data/users/hhx502/w2016/sg-en-i2r/en-oov-02.txt
cn_oov=/data/users/hhx502/w2016/sg-en-i2r/cn-oov-02.txt

if [ ! -z $step10 ]; then
  echo "show OOV statistics related to the CMU dictionary from WSJ"
  cat $tgt_file | \
  perl -e '($lex) = @ARGV; open(F, "<$lex") or die "ERROR, file $lex cannot open\n";
    while(<F>) { @A  = split(/[\s\t]+/); $w = shift @A; $vocab{$w} ++;  } close F;
    while(<STDIN>) {chomp; @A = split(/[\s\t]+/); for($i=3; $i<@A; $i++){ $w = $A[$i]; if(not exists $vocab{$w}) { $oov_vocab{$w}++; } }  } 
    foreach $key (sort {$oov_vocab{$b}<=>$oov_vocab{$a} } keys %oov_vocab) {
      if ($key !~ /[\x00-\x7f]/) {
        print STDERR "$key\t $oov_vocab{$key}\n";
      } else {
        print "$key\t$oov_vocab{$key}\n";
      }
    }
  ' $lex  > $en_oov 2> $cn_oov
  echo "done with EN_OOV: $en_oov, and CN_OOV: $cn_oov !"
fi
src_lex=sg-en-i2r/data/overall/lexicon-vendor.txt
phone_map=sg-en-i2r/data/overall/phone_map.txt
man_phone_map=sg-en-i2r/data/overall/phone_map-manual.txt
user_lex=sg-en-i2r/data/overall/lexicon-user.txt
if [ ! -z $step11 ]; then
  echo "LOG: analyze lexicon from vendor"
  cat $src_lex | \
  perl -e 'while(<>) {chomp; @A = split(/[\s\t]/); shift @A; for($i=0; $i<@A; $i++){$phone = $A[$i]; print "$phone\t$phone\n"; } }' | \
   sort -u     > sg-en-i2r/data/overall/phone_map.txt
  echo "LOG: done with lexicon analysis"
fi
if [ ! -z $step12 ]; then
  echo "LOG: generate user lexicon @ step 12"
  cat $src_lex | \
  perl -e '($mapfile) = @ARGV; open(F, "<$mapfile") or die "ERROR, file $mapfile cannot open\n";
  while(<F>) { chomp; @A = split(/[\s\t]/); $vocab{$A[0]} = $A[1];  } close F;
  while(<STDIN>) { chomp; @A = split(/[\s\t]/); $w = shift @A; $w = uc $w;
    $w =~ s/\([^\(]+\)//g;
    $s = "";
    for($i = 0; $i < @A; $i++) { $p = $A[$i];  $map_p = $vocab{$p}; if($map_p eq "NULL") {$map_p = ""; } 
      if($i == 0) {$s = $map_p; }else { $s .= " $map_p";  }  }  print "$w\t$s\n";   }' $man_phone_map  | sort -u  > $user_lex
  echo "LOG: done @ step 12"
fi
localdir=sg-en-i2r/data/local
langdir=sg-en-i2r/data/lang
if [ ! -z $step13 ]; then
  echo "LOG: prepare lexicon & lang"
  local/prepare_lexicon.pl  $user_lex $localdir || exit 1
   utils/prepare_lang.sh --share-silence-phones true \
    $localdir "<unk>"  $localdir/tmp.lang $langdir
  echo "done with $localdir & $langdir preparation!"
fi
seg_data=/data/users/hhx502/w2016/sg-en-i2r/transcript-linux-man-filtered-dict-02.txt
datadir=sg-en-i2r/data/overall
if [ ! -z $step14 ]; then
  echo "LOG: prepare overall kaldi data"
  [ -d $datadir ] || mkdir -p $datadir
  cat $seg_data | \
  perl -e  '($segfile, $utt2spk, $textfile) = @ARGV; 
    open(SEG, ">:encoding(utf8)", "$segfile") or die "ERROR, segfile $segfile cannot open\n"; 
    open(U2S, ">:encoding(utf8)", "$utt2spk") or die "ERROR, utt2spk $utt2spk cannot open\n";
    open(TXT, ">:encoding(utf8)", "$textfile") or die "ERROR, txtfile $textfile cannot open\n ";
    while(<STDIN>) { chomp; @A = split(/[\s\t]/); $seg = shift @A; $start = shift @A; $end = shift @A; 
      $stime = $start, $etime = $end;
      @B = split(/[\-]/, $seg);  $spk = sprintf("%s-%s-%s", $B[0], $B[1], $B[2]);
      $start = sprintf("%07d", $start * 100); $end = sprintf("%07d", $end * 100);
      $segId = sprintf("%s-%s-%s", $seg, $start, $end);
      print SEG "$segId $seg $stime $etime\n";
      print U2S "$segId $spk\n";
      print TXT "$segId ", join(" ", @A), "\n";
    } close SEG; close U2S; close TXT;
 '  $datadir/segments $datadir/utt2spk $datadir/text
    utils/utt2spk_to_spk2utt.pl < $datadir/utt2spk > $datadir/spk2utt
    utils/fix_data_dir.sh $datadir
  echo "Done for $datadir/segments, $datadir/utt2spk, $datadir/text !"
fi
old_wav_scp=sg-en-i2r/data/overall/old-wav.scp
new_wav_scp=sg-en-i2r/data/overall/wav.scp
new_wav_dir=$(cd sg-en-i2r/data/audio; pwd)
if [ ! -z $step15 ]; then
  echo "LOG: prepare new wav.scp"
  [ -f $new_wav_scp ] && rm $new_wav_scp
  for x in $(cat $old_wav_scp | awk '{printf("%s=%s\n", $1, $3);}'); do
    new_lab=$(echo $x | perl -pe 's/=.*//g;')
    new_wav_name=${new_lab}.pcm
    src_wav_dir=$(echo $x | perl -pe 's/.*=//g;')
    (cd $new_wav_dir;  [ -f $new_wav_name ] || ln -s $src_wav_dir $new_wav_name)
    new_wav=$new_wav_dir/$new_wav_name
    echo  "$new_lab /usr/bin/sox $new_wav -r 8000 -c 1 -b 16 -t wav - |" 
  done   > $new_wav_scp
 echo "Done"
fi
devspk_num=20
dev=sg-en-i2r/data/dev-${devspk_num}spk
train=sg-en-i2r/data/train
if [ ! -z $step16 ]; then
  echo "LOG: prepare dev data by selected 20 speakers"
  data=sg-en-i2r/data/overall
  cut -d" " -f2 $data/utt2spk |\
  sort -u | \
  utils/shuffle_list.pl --srand 777  | \
  perl -e '($num, $dev, $train) = @ARGV;
    open(DEV, ">", "$dev") or die "ERROR, dev $dev cannot open\n";
    open(TRN, ">", "$train") or die "ERROR, train $train cannot open\n";
    $cur_num = 0;
    while(<STDIN>) { chomp;
      if($cur_num < $num) {
        print DEV "$_\n";
      } else {
        print TRN "$_\n";
      }
      $cur_num ++;
    } close DEV; close TRN;
  ' \
  $devspk_num  $data/spklist-dev$devspk_num $data/spklist-train  || exit 1
  echo "LOG: check spklist: $data/spklist-dev$devspk_num, $data/spklist-train" 
  subset_data_dir.sh --spk-list $data/spklist-dev$devspk_num $data $dev  || exit 1
  
  cat $dev/text | \
  awk '{if(NF==1){}else{print;}}' > $dev/text-1
  mv $dev/text-1 $dev/text
  local/prepare_stm.pl $dev 
  fix_data_dir.sh $dev
  subset_data_dir.sh --spk-list $data/spklist-train $data $train  || exit 1
  cat $train/text | \
  awk '{if(NF==1){}else{print;}}' > $train/text-1
  mv $train/text-1 $train/text
  fix_data_dir.sh $train
  echo "Done !"
fi
if [ ! -z $step17 ]; then
  echo "LOG: build srilm started"
  local/train_lms_srilm.sh --dev-text $dev/text  sg-en-i2r/data sg-en-i2r/srilm
  echo "Done !"
fi
if [ ! -z $step18 ]; then
  echo "LOG: convert arpa lm to fst"
  local/arpa2G.sh  sg-en-i2r/srilm/lm.gz sg-en-i2r/data/lang sg-en-i2r/data/lang
  echo "Done"
fi
if [ ! -z $step19 ]; then
  echo "LOG: make mfcc+pitch features @ `date`"
  for sdata in sg-en-i2r/data/train sg-en-i2r/data/dev-20spk; do
    echo "LOG: proceeding with data $sdata started @ `date`"
    data=$sdata/mfcc-pitch
    [ -d $data ] || mkdir -p $data
    cp $sdata/* $data; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
    feat=$sdata/feat-data/mfcc-pitch
    steps/make_mfcc_pitch.sh --cmd slurm.pl --nj 40 --mfcc-config conf/mfcc.conf --pitch-config conf/pitch.conf \
    $data $feat/log $feat/data || exit 1
    steps/compute_cmvn_stats.sh $data $feat/log $feat/data 
    utils/fix_data_dir.sh $data 
    echo "Done with data $sdata @ `date`"
  done 
  echo "Done @ `date`"
fi
echo "done overall !" && exit 0

if [ ! -z $step04 ]; then
  for x in $(cat $data_root/wavlist.txt); do
    x_name=$(basename $x)
    x1=$(basename $(dirname $x) )
    x2=$(basename `dirname $(dirname $x)`)
    dir_x2=$(dirname `dirname $x`)
    tgt_path=$dir_x2/$x_name
    if [ $x1 == $x2 ]; then
      # echo dir_x2=$dir_x2, x1=$x1, x2=$x2, tgt_path=$tgt_path; break;
      echo mv $x $tgt_path
      mv $x $tgt_path
    fi
  done
  for x in $(cat $data_root/wavlist.txt); do
    x1=$(basename $(dirname $x) )
    x2=$(basename `dirname $(dirname $x)`)
    if [ $x1 == $x2 ]; then
      rmdir_x1=$(dirname $x)
      echo "remove $rmdir_x1, if it exists"
      [ -d $rmdir_x1 ] && rmdir $rmdir_x1
    fi
  done
fi
# c01-spk20101-i2r /data/users/hhx502/w2016/sg-en-i2r/CategoryI/Wave/Speaker201/Speaker201-1.pcm
data_overall=sg-en-i2r/data/overall
if [ ! -z $step05 ]; then
  for x in $(cat $data_root/wavlist.txt); do
    label_name=$(echo $x | perl -pe 'if(/CategoryII/){$c=c02;}elsif(/CategoryI/){$c=c01;}else{die;} if(/Speaker(\d+)/){$spkid=$1;}else {die;} if(/\-(\d+)\.pcm/){$idx = $1;}else{$idx=1;} $_ = sprintf("%s-spk%s%02d-i2r", $c, $spkid, $idx); ')
    echo $x, label_name=$label_name 1>&2 ; 
    echo "$label_name  /usr/bin/sox $x -r 8000 -c 1 -b 16 -t wav - |"
  done > $data_overall/wav.scp ||exit 1
fi

# iconv -f utf-16 -t utf-8 /data/users/hhx502/w2016/sg-en-i2r/SpeakerInfo-CategoryI.txt  > /data/users/hhx502/w2016/sg-en-i2r/SpeakerInfo-CategoryI-utf8.txt
