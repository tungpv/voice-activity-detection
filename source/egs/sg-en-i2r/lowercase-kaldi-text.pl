#!/usr/bin/perl -w
use strict;
use utf8;
use open qw (:std :utf8);

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my $utt = $1;
  my $words = lc($2);
  print "$utt $words\n";
}
print STDERR "## LOG ($0): stdin ended\n";
