#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
# end options 
# Usage:
# ./parepare-data.sh --steps <step-number>
# e.g.: ./prepare-data.sh --steps 1, or ./prepare-data.sh --steps 1,2,3,4,5,6 (no recommended)

. parse_options.sh || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
srcdir=/data/users/tungpham/FSTD_demo/data/mfcc_channel2
tgtdir=sg-en-i2r/data/fstd-demo
wavdir=/home/hhx502/w2016/sg-en-i2r/data/audio/fstd
wav_scp=$tgtdir/wav.scp
if [ ! -z $step01 ]; then
  echo "## step01: prepare file $wav_scp"
  echo > $wav_scp
  for x in $(ls $wavdir/*.wav); do
    echo $x | \
    perl -pe 'chomp; $labname = $_; $labname =~ s/.*\///g; $labname =~ s/\.wav//g;
      $_ = "$labname /usr/bin/sox $_ -r 8000 -c 1 -b 16 -t wav - |\n";
    '  >> $wav_scp
  done  
  echo "## step01: done with $wav_scp preparation!"
fi

segments=$tgtdir/segments
utt2spk=$tgtdir/utt2spk
map=$tgtdir/old-lab-to-new-lab-map.txt
if [ ! -z $step02 ]; then
  echo "## step02: prepare segments $segments"
  cat $srcdir/segments | \
  perl -e '($old_utt2spk, $new_lab, $segFile, $utt2spk, $map) = @ARGV; open(F, "<", "$old_utt2spk") or die "ERROR, file $old_utt2spk cannot open\n";
    while(<F>) { chomp; m/(\S+)\s+(\S+)/; ($utt, $spk) = ($1, $2); $vocab{$utt} = $spk;  }  close F;
    $spkIndex = 0;
    open(SEG, ">", "$segFile") or die "ERROR, file $segFile cannot open\n";
    open(UTT2SPK, ">", "$utt2spk") or die "ERROR, file $utt2spk canot open\n";
    open(MAP, ">", "$map") or die "ERROR, file $map cannot open\n";
    while(<STDIN>) { chomp; m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/; ($segId, $segName, $start, $end) = ($1, $2, $3, $4); 
      $new_start = sprintf("%.2f", $start); 
      if(not exists $vocab{$segId}) { die "utt $segId does not exist\n"; } $spk = $vocab{$segId};
      if(not exists $spk_vocab{$spk}) { $new_spk = sprintf("%s%02d", "spk", ++$spkIndex); $spk_vocab{$spk} = $new_spk; } else { $new_spk = $spk_vocab{$spk}; }
      $new_segId = sprintf("%s-%s-%06d", $new_spk, $new_lab, $new_start*100);
      print SEG "$new_segId $new_lab $start $end\n";
      print UTT2SPK "$new_segId $new_spk\n";
      print MAP "$segId $new_segId\n";
    }  close SEG; close UTT2SPK; close MAP;
  ' $srcdir/utt2spk "unk-interview-b-20151217" $segments $utt2spk  $map 
  utils/utt2spk_to_spk2utt.pl < $utt2spk > $tgtdir/spk2utt
  echo "## step02: done with segments $segments"
fi
stext=$srcdir/text
text=$tgtdir/text
if [ ! -z $step03 ]; then
  echo "## step03: prepare text $text"
  cat $stext | \
  perl -e ' ($map, $text) = @ARGV;  open(MAP, "<", "$map") or die "ERROR, file $map canot open\n";
    while(<MAP>) {chomp; m/(\S+)\s+(\S+)/; ($oldLab, $newLab) = ($1, $2); $vocab{$oldLab} = $newLab; } close MAP;
    open(F, ">", "$text") or die "ERROR, file $text cannot open\n";
    while(<STDIN>) {chomp; m/(\S+)\s+(.*)/; ($lab, $utt) = ($1, $2);  if(not exists $vocab{$lab}){die "ERROR, $lab does not exist !\n"; }
      @A = split(/[\s]+/, $utt); $s = ""; for($i = 0; $i < @A; ++$i) { $w = $A[$i]; $w =~ s/[\.]//g;  if($w !~ /[<\[]/){ $w = uc $w; } else { $w =~ s/[\[]/</g; $w =~ s/[\]]/>/g;   }    if($i == 0){$s = "$w";} else {$s .= " $w";}   } 
      print F "$vocab{$lab} $s\n";
    } close F;
  ' $map $text
  echo "## step03: done with text $text preparation !"
fi
