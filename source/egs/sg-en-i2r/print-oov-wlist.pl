#!/usr/bin/perl

use strict;
use warnings;
use utf8;
use open qw(:std :utf8);

my $numArgs = @ARGV;

if ($numArgs != 1) {
  die "\nExample: cat text | $0 <words.txt> >oov-wlist.txt\n\n";
}

my ($wordfName) = @ARGV;

# begin sub
sub InitVocab {
  my ($wordfName, $vocab) = @_;
  open(F, "$wordfName") or die "## ERROR: InitVocab($0) cannot open file $wordfName\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    $$vocab{lc $1} ++;
  }
  close F;
}
# end sub
my %vocab = ();
InitVocab($wordfName, \%vocab);

open (OUT, "|sort -u") or die "## ERROR: $0 cannot open stdout\n";
print STDERR "## LOG: $0 expects stdin\n";
my %outputVocab = ();
my $totalWords = 0;
my $oovWords = 0;
my $totalUtts = 0;
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  shift @A;  $totalWords += scalar @A;
  $totalUtts ++;
  for(my $i = 0; $i < @A; $i ++) {
    my $w = lc $A[$i];
    if (not exists $vocab{$w}) {
      $oovWords ++;
      if(not exists $outputVocab{$w}) {
        $outputVocab{$w} ++;
        print OUT "$w\n";
      }
    }
 }
}
print STDERR "## LOG: $0 ends with stdin\n";
my $oovRate = 0;
if ($totalWords != 0) {
  $oovRate = sprintf("%.4f", $oovWords/$totalWords);
}
print STDERR "## LOG ($0): total utterances ($totalUtts), total oov words ($oovWords), total words ($totalWords), oovRate ($oovRate) \n";
close OUT;
