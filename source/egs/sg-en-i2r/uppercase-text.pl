#!/usr/bin/perl

use warnings;
use strict;

use utf8;
use open qw(:std :utf8);

print STDERR "## LOG: $0 expects stdin\n";

while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  my $utt = shift @A;
  for(my $i = 0; $i < @A; $i ++) {
    my $w = $A[$i];
    if($w =~ /\p{Han}+/ || $w =~ /</) {
      $utt .= " $w";
    } else {
      $utt .= " " . uc $w; 
    }
  }
  print "$utt\n";
}

print STDERR "## LOG: $0 ends with stdin\n";
