#!/usr/bin/perl -w 
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\n Example: cat old-lexicon.txt | $0 phone-map.txt > new-lexicon.txt\n\n";
}

my ($phoneMapFile) = @ARGV;

# begin sub
sub InitVocab {
  my ($dictFile, $vocab) = @_;
  open(F, "$dictFile") or die "## ERROR (InitVocab, $0): cannot read file $dictFile \n";
  while(<F>) {
    chomp;
    my @A = split(/\s+/);
    my $aPTR;
    if(not exists $$vocab{$A[0]}) {
      $$vocab{$A[0]} = [my @B ];
      $aPTR = $$vocab{$A[0]};
    } else {
      $aPTR = $$vocab{$A[0]};
    }
    push (@$aPTR, $A[1]);
  }
  close F;
}
sub Push {
  my ($array, $item, $pos) = @_;
  my @tmpArray = ($item, $pos);
  push @$array, \@tmpArray;
}
sub Pop {
  my ($array, $item, $pos) = @_;
  my $tmpArray = pop @$array;
  $$item = $$tmpArray[0]; $$pos = $$tmpArray[1];
}
sub MapWordPron {
  my ($word, $pron, $vocab, $mapArray) = @_;
  my @A = split(/\s+/, $pron);
  my $i = 0;
  my $phoneNum = scalar @A;
  my @backtrace = ();
  my @output = ();
  my $pos = 0;
  while(1) {
    if (not exists $$vocab{$A[$i]}) {
      print STDERR "## WARNING (MapWordPron, $0): unidentified phone $A[$i]\n";
      return 0;
    }
    my $ptrArray = $$vocab{$A[$i]};
    push @output, $$ptrArray[$pos]; $pos ++;
    Push(\@backtrace, $ptrArray, $pos);
    # print STDERR "## Debug (MapWordPron, $0): ", $A[$i], " ", scalar @$ptrArray, ", i=$i", "\n";
    my $tmpA = $backtrace[0];
    my $array = $$tmpA[0];
    # print STDERR "## Debug (): array_size=", scalar @$array, "\n";
    $i ++;
    if($i == $phoneNum) {
      my @tmpA = @output;
      push @$mapArray, \@tmpA;
      pop @output; $i --;
      my $ptrA;
      Pop(\@backtrace, \$ptrA, \$pos);
      my $dim = scalar @$ptrA;
      while($pos >= $dim && scalar @backtrace > 0) {
	pop @output; $i --;
        Pop(\@backtrace, \$ptrA, \$pos); 
        # print STDERR "i == $i, pos=$pos, ptrArray_size=", scalar @$ptrA, ", backtrace_size=", scalar @backtrace, "\n";
        $dim = scalar @$ptrA;
      }
      last if ($pos >= $dim);
    } else {
      $pos = 0;
    }
  }
  return 1;
}
# end sub

my %mapVocab = ();
InitVocab($phoneMapFile, \%mapVocab);
my %outputVocab = ();
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my $word = $1; my $pron = $2;
  my @mapArray = ();
  if (MapWordPron($word, $pron, \%mapVocab, \@mapArray) == 0 ) {
    print STDERR "'$_' cannot mapped due to oov phone\n";
    @mapArray = ();
    next;
  }
  for(my $i = 0; $i < scalar @mapArray; $i ++) {
    my $ptrArray = $mapArray[$i];
    my $pron = join(" ", @$ptrArray);
    my $dictLine = sprintf("%s\t%s", $word, $pron);
    if(not exists $outputVocab{$dictLine}) {
      print "$dictLine\n";
      $outputVocab{$dictLine} ++;
    }
  }
}
print STDERR "## LOG ($0): stdin ended\n";
