#!/bin/bash

echo
echo "$0 $@"
echo

. path.sh
. cmd.sh

# begin options
steps=1
sg_en_8k_lex=/home2/hhx502/sg-en-i2r/data/lang/words.txt
g2p_model=/home2/hhx502/seame/data/local/g2p-model-2/model-3
# end options

function Usage {
cat<<END

 Usage: $0 [options] <source-data> <tgtdir>
 [examples]:
 --sg-en-8k-lex                           # value, $sg_en_8k_lex
 --g2p-model                              # value, $g2p_model

 $0 --steps 1 --sg-en-8k-lex $sg_en_8k_lex \
 --g2p-model $g2p_model \
 /data/users/hhx502/sg-en-source-sept-2016/data /home2/hhx502/sg-en-i2r-16k

END
}

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  Usage && exit 1;
fi
source_data=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
localdir=$tgtdir/data/local
local_train=$localdir/train
if [ ! -z $step01 ]; then
  echo "## LOG-step01@$0: prepare wave list @ `date`"
  for SRC in `find $source_data -depth`; do
    DST=`dirname "${SRC}"`/`basename "${SRC}" | tr '[A-Z]' '[a-z]'`
    if [ "${SRC}" != "${DST}" ]
    then
        [ ! -e "${DST}" ] && mv -T "${SRC}" "${DST}" || echo "${SRC} was not renamed"
    fi
  done 
  echo "## LOG-step01@$0: done ($local_train) "
fi
[ -d $local_train ] || mkdir -p $local_train
if [ ! -z $step02 ]; then
  echo "## LOG-step02@$0: prepare wave text lists @ `date`"
  find $source_data/wave -name "*.wav" > $local_train/wav.list
  find $source_data/script -name "*.txt" > $local_train/trans.list
  echo "## LOG-step02@$0: done ($local_train) @ `date` "
fi

if [ ! -z $step03 ]; then
  source/egs/sg-en-i2r/i2r-data16k-make-wavscp-text.pl \
  $local_train/trans.list $local_train/wav.list $local_train || exit 1 
fi

if [ ! -z $step04 ]; then
  echo "## LOG: step04: organized the text"
  cat $local_train/text.1 | \
  perl -ane 'chomp; @A = split(/\s+/); shift @A; for($i = 0; $i < @A; $i++){ $w = lc $A[$i]; print "$w\t$w\n"; }' | \
  sort -u >  $local_train/word-to-word-map.txt 
  cat $local_train/text.1 | \
  perl -e '($map) = @ARGV; open(M, "$map") or die "## ERROR: file $map cannot open\n"; while(<M>) {chomp; m/(\S+)\s+(\S+)/; $vocab{$1} = $2;} close M;
    while(<STDIN>) {chomp; @A = split(/\s+/); $utt= shift @A; for($i = 0; $i < @A; $i++){ $w = lc $A[$i]; die "## ERROR: bad word $w\n" if not exists $vocab{$w};
     $w = $vocab{$w}; if ($w =~ /</){$utt .= " $w";} else { $utt .= " ". uc $w;}} print "$utt\n"; }' $local_train/word-to-word-map-EDIT.txt > $local_train/text
  echo "## LOG: step04: done ($local_train/text)"
fi 

if [ ! -z $step05 ]; then
  echo "## LOG: step05, collect oov word and make prons for them using g2p"
  cat $local_train/text | \
  source/egs/sg-en-i2r/print-oov-wlist.pl  $sg_en_8k_lex > $local_train/oov-word-v-sg_en_8k_lex.txt
  echo "## LOG: step05, done ('$local_train')"
fi

g2p_temp=$local_train/g2p
if [ ! -z $step06 ]; then
  [ -d $g2p_temp ] || mkdir -p $g2p_temp
  cat $local_train/oov-word-v-sg_en_8k_lex.txt | perl -ane 'print lc $_;' > $g2p_temp/wlist.txt
  g2p.py --model $g2p_model \
  --apply $g2p_temp/wlist.txt > $g2p_temp/wlist-g2p-dict.txt 
  cat $g2p_temp/wlist-g2p-dict.txt | \
  perl -ane 'chomp; @A = split(/\s+/); $w = shift @A; $w = uc $w;  $pron = join(" ", @A); print "$w\t$pron\n";'  > $g2p_temp/uppercase-wlist-g2p-dict.txt 
 echo "## LOG ($0): done ('$g2p_temp/uppercase-wlist-g2p-dict.txt') "
fi
train_data=$tgtdir/data/train
if [ ! -z $step07 ]; then
  echo "## LOG (step07, $0): make segment file & train data"
  wav-to-duration scp:$local_train/wav.scp ark,t:-| \
  perl -ane 'chomp; @A = split(/\s+/); print "$A[0] $A[0] 0 $A[1]\n";' > $local_train/segments
  utils/utt2spk_to_spk2utt.pl < $local_train/utt2spk > $local_train/spk2utt
  [ -d $train_data ] || mkdir -p $train_data
  cp $local_train/{wav.scp,segments,text,utt2spk,spk2utt} $train_data/ 
  echo "## LOG (step07, $0): done ('$local_train')"
fi
mfcc_pitch_8k=$tgtdir/data/train-8k/mfcc-pitch
fbank_pitch_8k=$tgtdir/data/train-8k/fbank-pitch
if [ ! -z $step08 ]; then
  for x in $train_data; do
    data=$mfcc_pitch_8k;
    sdata=$(dirname $data)
    [ -d $sdata ] || mkdir -p $sdata
    cp $x/* $sdata/
    feat=$sdata/feat/mfcc-pitch
    cat $x/wav.scp | perl -ane 'chomp; @A = split(/\s+/);  printf("%s %s %s %s|\n",$A[0], "/usr/bin/sox", $A[1], "-r 8000 -c 1 -b 16 -t wav - downsample" ); ' > $sdata/wav.scp
    fix_data_dir.sh $sdata
    source/egs/swahili/make_feats.sh --cmd slurm.pl --nj 20  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc.conf --pitch-config conf/pitch.conf" \
    $sdata $data $feat || exit 1

    echo "## LOG ($0, step08): check data '$data'"
    data=$fbank_pitch_8k
    feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd slurm.pl --nj 20  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf --pitch-config conf/pitch.conf" \
    $sdata $data $feat || exit 1
    echo "## LOG ($0, step08): check data '$data'"
  done
fi
