#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <sge-lexicon.txt> <en-lexicon.txt> <tgtdir> 
 [options]:
 --steps                               # value, "$steps"
 [steps]:

 [examples]:

 $0 --steps 1 /home2/hhx502/sg-en-i2r/data/local/dict/lexicon.txt \
 /home2/hhx502/ldc-cts2016/fisher-english/data/local/dict/lexicon.txt \
 /home2/hhx502/sg-en-i2r/data/local/sge-to-en-phone-map 

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

sge_lex=$1
en_lex=$2
tgtdir=$3

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi


