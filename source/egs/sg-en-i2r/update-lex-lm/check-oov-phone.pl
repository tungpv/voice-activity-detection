#!/usr/bin/perl

use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\n## Example: cat new-phones.txt | $0  old-phones.txt\n\n";
}

my ($oldPhones) = @ARGV;

open(F, "$oldPhones") or die "## ERROR ($0): failed to open $oldPhones to read\n";
my %vocab = ();
while(<F>) {
  chomp;
  m/(\S+)/ or next;
  $vocab{$1} ++;
}
close F || die "## ERROR ($0): failed to close file handle\n";

print STDERR "## LOG: $0 expects stdin\n";
while(<STDIN>) {
  chomp;
  m/(\S+)/ or next;
  if (not exists $vocab{$1}) {
    print STDERR "## ERROR ($0): oov phone $1 observed\n";
    exit 1;
  }
}
print STDERR "## LOG: $0 ends with stdin\n";


