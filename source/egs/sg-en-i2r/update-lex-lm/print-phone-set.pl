#!/usr/bin/perl

use warnings;
use strict;

print STDERR "## LOG: $0 expects stdin\n";
my %vocab = ();
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  die "## ERROR ($0): bad line $_ in dict\n" if @A < 2;
  my $word = shift @A;
  for(my $i = 0; $i < @A; $i ++) {
    my $phone = $A[$i];
    if(not exists $vocab{$phone}) {
      print "$phone\n";
      $vocab{$phone} ++;
    }
  } 
}
print STDERR "## LOG: $0 ends with stdin\n";
