#!/usr/bin/perl

use warnings;
use strict;
use utf8;
use open qw(:std :utf8);
print STDERR "## LOG: $0 expects stdin\n";
my %vocab = ();
my $wordNum = 0;
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  die "## ERROR ($0): bad line $_\n" if(scalar @A < 2);
  my $word = shift @A; 
  my $pron = join(" ", @A);
  my $dictLine = sprintf("%s\t%s", $word, $pron);
  if (not exists $vocab{$dictLine}) {
    $wordNum ++;
    $vocab{$dictLine} ++;
    print "$dictLine\n";
  }
}
print STDERR "## LOG ($0): total $wordNum pronunciations are generated\n";
print STDERR "## LOG: $0 ends with stdin\n";
