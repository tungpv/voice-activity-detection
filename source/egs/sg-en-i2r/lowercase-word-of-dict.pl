#!/usr/bin/perl -w
use strict;

use open qw(:std :utf8);
my $lineIndex = 0;
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  $lineIndex ++;
  die "## ERROR ($0): bad line $_ @(line: $lineIndex)\n" if scalar @A < 2;
  my $word = lc(shift @A);
  my $pron = join(" ", @A);
  print "$word\t$pron\n";
}
print STDERR "## LOG ($0): stdin ended\n";
