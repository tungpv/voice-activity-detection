#!/bin/bash

. path.sh
. cmds.sh

echo
echo "## LOG (user): $0 $@"
echo

set -e
# begin options
steps=
unknown_word="<unk>"
dev_data_line_num=1000
# end options

function Usage {
cat<<END

 Usage: $0 [options] <source-dict-dir> <tgt-data-dir> <tgtdirname> <dict-to-be-merged> <text1> [ ... textN ] 
 [options]:
 --unknown-word                                 # value, "$unknown_word"
 --dev-data-line-num                            # value, "$dev_data_line_num"
 [steps]:

 [examples]:
 $0 --steps 1 --unknown-word "$unknown_word" \
 --dev-data-line-num $dev_data_line_num  \
 /home2/hhx502/sg-en-i2r/data/local/dict /home2/hhx502/sg-en-i2r/data update-knowledge-source-$(date +%d-%m-%Y) \
 /home2/hhx502/sg-en-i2r-16k/data/local/train/g2p/uppercase-wlist-g2p-dict.txt  /home2/hhx502/sg-en-i2r/data/train/text \
 /home2/hhx502/sg-en-i2r-16k/data/train/text 

END
}

. parse_options.sh || exit 1

if [ $# -le 5 ]; then
  Usage && exit 1
fi

source_dict_dir=$1; shift
tgtdata=$1; shift
tgtdirname=$1; shift
dict_to_be_merged=$1; shift
textArray=("$@")

tgtdir=$tgtdata/$tgtdirname

[ -d $tgtdir ] || mkdir -p $tgtdir

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
dict=$tgtdir/dict
[ -d $dict/temp ] || mkdir -p $dict/temp
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): prepare target dict"

  cat $dict_to_be_merged | \
  source/egs/sg-en-i2r/update-lex-lm/print-phone-set.pl > $dict/temp/phones.txt || \
  { echo "## ERROR (step01, $0): print-phone-set failed"; exit 1; }
  [ -e $source_dict_dir/nonsilence_phones.txt ] && \
  [ -e $source_dict_dir/silence_phones.txt ] || \
  { echo "## ERROR (step01, $0): nosilence_phones.txt or silence_phones.txt expected from $source_dict_dir"; exit 1; }
  echo "## LOG (step01, $0): done with target dict preparation ($dict)"
  cat $dict/temp/phones.txt | \
  source/egs/sg-en-i2r/update-lex-lm/check-oov-phone.pl "cat $source_dict_dir/nonsilence_phones.txt $source_dict_dir/silence_phones.txt|" ||\
  { echo "## ERROR (step01, $0): check-oov-phone failed !"; exit 1; }
  cat $dict_to_be_merged $source_dict_dir/lexicon.txt | \
  source/egs/sg-en-i2r/update-lex-lm/merge-dict.pl | \
  perl -ane 'chomp; use utf8; use open qw(:std :utf8); if(/\p{Han}+/){ next; }else{print "$_\n";}' > $dict/lexicon.txt || \
  { echo "## ERROR (step01, $0): merge-dict failed"; exit 1; }
  echo -e "<silence>\tSIL" | cat - $dict/lexicon.txt > $dict/lexicon.1.txt
  mv $dict/lexicon.1.txt $dict/lexicon.txt
  cp $source_dict_dir/{nonsilence_phones.txt,silence_phones.txt,optional_silence.txt,extra_questions.txt} $dict  || exit 1

  echo "## LOG (step01, $0): target dict preparation done ($dict)"
fi
lang=$tgtdir/lang
if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): make lang"
  utils/validate_dict_dir.pl $dict
  utils/prepare_lang.sh $dict "$unknown_word" $lang/tmp $lang
  echo "## LOG (step02, $0): target lang ($lang)"
fi
lmdir=$tgtdir/lm
 [ -d $lmdir ] || mkdir -p $lmdir
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): prepare LM data"
  for text in ${textArray[@]}; do
    cat "$text"
  done  | utils/shuffle_list.pl | gzip -c > $lmdir/overall.gz
  gzip -cd $lmdir/overall.gz | head -$dev_data_line_num | \
  perl -pe 'chomp; m/(\S+)\s+(.*)/; $_="$2\n";' | \
  gzip -c > $lmdir/dev-text.gz
  gzip -cd $lmdir/overall.gz | tail -n +$[dev_data_line_num+1] |\
  perl -pe 'chomp; m/(\S+)\s+(.*)/; $_= "$2\n";' | \
  gzip -c > $lmdir/train-text.gz
  cat $lang/words.txt | \
  egrep -v '<eps>|#' | awk '{print $1;}' | gzip -c > $lmdir/vocab.gz
  source/egs/kws2016/georgian/train-srilm-v2.sh --steps 1,2 \
  --lm-order-range "3 3" \
  --cutoff-csl "3,011,012" \
  $lmdir || exit 1
  echo "## LOG (step03, $0): done with LM data preparation ($lmdir)"
fi
if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): build grammar"
  source/egs/fisher-english/arpa2G.sh $lmdir/lm.gz $lang $lang
  echo "## LOG (step04, $0): done with grammar building ('$lang')"
fi

