#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=20
cmd=run.pl
steps=
prepare_data=false
prepare_data_opts="data lang graph sdir aliname decodename"
ali_cmd=steps/nnet/align.sh
decode_cmd=steps/nnet/decode.sh
prepare_data2=false

# end options

echo
echo "$0 $@"
echo

. parse_options.sh

function PrintOptions {
  cmdName=$(echo $0| perl -pe 's/^.*\///g;')
  cat <<END

$cmdName [options]:
cmd					# value, "$cmd"
nj					# value, "$nj"
steps					# value, "$steps"
prepare_data				# value, $prepare_data
prepare_data_opts			# value, "$prepare_data_opts"
ali_cmd					# value, "$ali_cmd"
decode_cmd				# value, "$decode_cmd"
prepare_data2				# value, $prepare_data2

END
}

PrintOptions;

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=true
  done
fi

if $prepare_data; then
  echo "$0: prepare_data started @ `date`"
  optNames="data lang graph sdir aliname decodename"
  . source/register_options.sh "$optNames" "$prepare_data_opts" || exit 1
  if [ ! -z $step01 ]; then
    [ -f $data/segments ] || { echo "$0: segments file expected"; exit 1; }
    if [ ! -f $data/reco2file_and_channel ]; then
      cat $data/segments | awk '{print $2;}' | sort -u |\
      awk '{printf("%s %s 1\n", $1, $1);}' > $data/reco2file_and_channel
    fi
    alidir=$sdir/$aliname
    if [ ! -f $alidir/ali.1.gz ]; then
      echo "$0: making alignment on data $data started @ `date`"
      $ali_cmd --nj $nj --cmd "$cmd" \
      $data $lang $sdir $alidir || exit 1
      echo "$0: making alignment ended @ `date`"
    fi
    steps/get_train_ctm.sh $data $lang $alidir || exit 1
  fi
  if [ ! -z $step02 ]; then
    $decode_cmd --nj $nj --cmd "$cmd" \
    --skip-scoring true \
    $graph $data   $sdir/$decodename  || exit 1
  fi
  if [ ! -z $step03 ]; then
    echo "$0: prepare_data: making sample data @ `date`"
    
    echo "$0: prepare_data: making sample data @ `date`"
  fi
  echo "$0: prepare_data ended @ `date`"
fi
if $prepare_data2 ; then

fi
