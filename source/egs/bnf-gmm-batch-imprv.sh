#!/bin/bash


pattern=
steps=

. path.sh
. cmd.sh
echo
echo "LOG: $0 $@" 
echo

. parse_options.sh

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ -z $pattern ]; then
  echo "$0: pattern $pattern is empty"; exit 1
fi
bnfe=./G.FLP/exp/$pattern/hierbn2
bnfe_pattern=$(basename `dirname $bnfe`)
lang=G.FLP/data/lang
featdir=strain-${bnfe_pattern}-node03
data=$(dirname $bnfe)/strain
if [ ! -z $step01 ]; then
  source/egs/run-bnf.sh --make_bnf2 true --make_bnf2_opts "G.FLP/data/strain/fbank_pitch $bnfe  $data  /local/hhx502/kws2015/G.FLP/feat/$featdir" > log/bnf-FLP-${featdir}-node03.log
fi

featdir=dev-${bnfe_pattern}-node04
bnf_dev=$(dirname $bnfe)/dev
if [ ! -z $step02 ]; then
  source/egs/run-bnf.sh --make_bnf2 true --make_bnf2_opts "G.FLP/data/dev/fbank_pitch $bnfe  $bnf_dev  /local/hhx502/kws2015/G.FLP/feat/$featdir" > log/bnf-FLP-${featdir}-node03.log
fi

sdir=G.FLP/exp/gmm-bnf-${bnfe_pattern}
if [ ! -z $step03 ]; then
  source/egs/run-bnf.sh --run_gmm2 true --run_gmm2_import_alidir ./G.FLP/exp/gmm.plp_pitch-strain/tri4a/ali_strain  --run_gmm2_opts "a $data  strain $lang 6000 75000  $sdir" > log/gmm-FLP-bnf-${bnfe_pattern}.log 
fi

if [ ! -z $step04 ]; then
  source/egs/run-bnf.sh --decoding_eval true --decoding_opts "$bnf_dev $lang $sdir/tri4a  $sdir/tri4a/graph $sdir/tri4a/decode.dev steps/decode_fmllr.sh" --lattice_opts ' ' > log/decode-FLP-dev-${bnfe_pattern}.log 
fi
