#!/bin/bash

. path.sh
. cmd.sh

echo
echo LOG: $0 $@
echo

# begin options
nj=40
cmd=slurm.pl
steps=
# end options

. parse_options.sh


if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

function Usage {
  cat<<END
 Usage: $0 [options] <unlabelled-data>
 [options]:
 --nj                         # value, $nj
 --cmd                        # value, "$cmd"
 --steps                      # value, "$steps"
 [steps]:
 1:
 [example]:


END
}

if [ $# -ne 1 ]; then
  Usage && exit 1
fi 
