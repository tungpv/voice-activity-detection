#!/bin/bash

. path.sh
. cmd.sh

# begin options
train_cmd=run.pl
cmd=slurm.pl
nj=40
steps=
graphdir=
devdata=
devname=dev
# end options

echo
echo LOG: $0 $@
echo

. parse_options.sh || exit 1

function Usage {
  cat<<END

 $(basename $0) [options] <data> <lang> <srcnnet> <alidir> <tgtdir>
 [options]:
 --train-cmd                              # value, "$train_cmd"
 --cmd                                    # value, "$cmd"
 --nj                                     # value, $nj
 --steps                                  # value, "$steps"
 --graphdir                               # value, "$graphdir"
 --devdata                                # value, "$devdata"
 --devname                                # value, "$devname"
 [steps]:
 1:  do multilingual based cross-lingual transfer learning (mctl) by overall-tuning
 2:  do smbr sequential training
 3:  do mctl by sofxmax tuning
 4:  do sequential training
 5:  redo step 2 for decoding
 [example]:
 source/egs/is16/run-mctl.sh --steps 1,2,3,4 --graphdir kws2016/vllp-grapheme/exp/mono/tri4a/graph \
  --devdata kws2016/vllp-grapheme/data/dev/fbank_pitch kws2016/vllp-grapheme/data/train/fbank_pitch \
  kws2016/vllp-grapheme/data/lang ../w2015/monoling/viet107/llp/exp/mling.flp6/nnet.fbank \
  kws2016/vllp-grapheme/exp/mono/nnet6a-mpe/ali-semi-conf0.9 kws2016/vllp-grapheme/exp/mctl-is16

END
}

if [ $# -ne 5 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
srcnnet=$3
alidir=$4
tgtdir=$5

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
overall_xe_dir=$tgtdir/overall-tune
if [ ! -z $step01 ]; then
  echo "LOG: step01, transfer learning with overall tuning started @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2,3,4  \
  ${graphdir:+--graphdir $graphdir}  --softmax-layer-tuning false \
  ${devdata:+--devdata $devdata} --decodename decode-$devname $data $lang $srcnnet $alidir $overall_xe_dir
  echo "LOG: step01, done @  `date`"
fi
function step02_check {
  [ -f $overall_xe_dir/final.nnet ] || \
  { echo "ERROR, step02, final.nnet expected"; exit 1; }
  echo "LOG: step02_check is done";
}
smbr_overall=$tgtdir/smbr-overall-tune
if [ ! -z $step02 ]; then
  echo "LOG: step02, smbr training started @ `date`"
  step02_check || exit 1
  source/egs/swahili/run-sequential-nnet.sh --steps 1,2,3,4,5 --train-cmd "run.pl" --cmd "$cmd" \
  ${devdata:+--devdata $devdata} --graphdir $graphdir --decodename decode-$devname \
  $data  $lang $overall_xe_dir  $smbr_overall || exit 1
  echo "LOG: step02, done @ `date`"
fi
if [ ! -z $step05 ]; then
  echo "LOG: step05, redo step02, for decoding started @ `date`"
  source/egs/swahili/run-sequential-nnet.sh --steps 4 --train-cmd "run.pl" --cmd "$cmd" --nj $nj \
  ${devdata:+--devdata $devdata} --graphdir $graphdir --decodename decode-$devname \
  $data  $lang $overall_xe_dir  $smbr_overall || exit 1
  echo "LOG: step05, redo step02, done @ `date`"
fi
softmax_xe_dir=$tgtdir/softmax-tune
if [ ! -z $step03 ]; then
  echo "LOG: step03, softmax tuning started @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2,3,4  \
  ${graphdir:+--graphdir $graphdir}  --softmax-layer-tuning true \
  ${devdata:+--devdata $devdata} --decodename decode-$devname $data $lang $srcnnet $alidir $softmax_xe_dir
  echo "LOG: step03,  done @ `date`"
fi
function step04_check {
 [ -f $softmax_xe_dir/final.nnet ] || \
 { echo "ERROR, step04, final.nnet expected"; exit 1; }
 echo "LOG: step04_check is done";
}
smbr_softmax=$tgtdir/smbr-softmax
if [ ! -z $step04 ]; then
  echo "LOG: step04, smbr training started @ `date`"
  step04_check || exit 1
  source/egs/swahili/run-sequential-nnet.sh --steps 1,2,3,4,5 --train-cmd "run.pl" --cmd "$cmd" \
  ${devdata:+--devdata $devdata} --graphdir $graphdir --decodename decode-$devname \
  $data  $lang $softmax_xe_dir  $smbr_softmax || exit 1
  echo "LOG: step04, done @ `date`"
fi

