#!/bin/bash

. path.sh
. cmd.sh

echo
echo LOG: $0 $@
echo

# begin options
nj=40
cmd="slurm.pl --exclude=node01,node02"
steps=
conf=0
labelled_data=

seed_srcdir=

state_num=2500
pdf_num=25000
devdata=
devname=dev
decodename=decode-dev


# end options

. parse_options.sh || exit 1


if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

function Usage {
 cat<<END
 Usage: $0 [options] <unlabelled_data> <lang> <utt_ctm> <tgtdir>
 [options]:
 --nj                                 # value, $nj
 --cmd                                # value, "$cmd"
 --steps                              # value, "$steps"
 --conf                               # value, $conf
 --labelled-data                      # value, "$labelled_data"
 
 --seed-srcdir                       # value, "$seed_srcdir"
 
 --state-num                          # value, $state_num
 --pdf-num                            # value, $pdf_num
 --devdata                            # value, "$devdata"
 --devname                            # value, "$devname"
 --decodename                         # value, "$decodename"

 [steps]:

 1:  make SS data with confidence $conf
 2:  make fbank feature for SS data
 3:  make alignment if seed_srcdir provided
 4:  make plp feature for ss data
 5:  retrain gmm-hmm with ss data, and test it if devdata provided
 6:  ali conversion
 7:  transfer learning based sst with xe
 8:  smbr training with labelled_data

 9:  redo step5 for decoding, if necessary
 10: redo step9 for decoding, if necessary
 [example]:

 source/egs/is16/run-is16.sh --steps 1,2,3,4,5,6,7,8 --conf 0.9 --labelled-data \
 kws2016/vllp-grapheme/data/train/fbank_pitch --seed-srcdir kws2016/vllp-grapheme/exp/mono/nnet6a-mpe \
 --state-num 2500 --pdf-num 25000 \
 --devdata kws2016/vllp-grapheme/data/dev \
 kws2016/vllp-grapheme/data/unlabelled kws2016/vllp-grapheme/data/lang kws2016/vllp-grapheme/exp/mono/nnet6a-mpe/decode_unlabelled/ctm_11/fbank_pitch.utt.ctm \
 kws2016/vllp-grapheme/exp/semi-is16-conf0.9

END
}
unlabelled_data=$1
lang=$2
utt_ctm=$3
tgtdir=$4
if [ $# -ne 4 ]; then
  Usage && exit 1
fi
semidir=$tgtdir/semi-data
asrdir=$tgtdir/asr-data
function check_asrdata {
  [ -f $asrdir/segments ] || \
  { echo "ERROR, step01, segments expected from $asrdir"; exit 1; }
  toth=$(cat $asrdir/segments | awk '{x+=$4-$3;}END{print x/3600;}')
  if (( $(bc <<< "$toth <1.0") )); then
    echo "ERROR, step01, too little data selected"; exit 1
  fi
  echo "LOG: step01, checking is done !"
}
if [ ! -z $step01 ]; then
  echo "LOG: step01, make SS data"
  source/egs/swahili/make_unsupervised_data.sh --steps 1,2 \
  --conf-thresh $conf --labelled-data $labelled_data --semi-data $semidir $unlabelled_data $utt_ctm   $asrdir
  check_asrdata || exit 1
  echo "LOG: selected machine transcribed data length:"
  source/data-len.sh $asrdir
  echo "LOG: ss data $semidir length: "
  source/data-len.sh $semidir
  echo "LOG: step01, done for $semidir"
fi
fbank_data=$semidir/fbank-pitch
if [ ! -z $step02 ]; then
  echo "LOG: step02, make fbank+pitch feature for SS data"
  source/egs/swahili/make_feats.sh  --fbank-pitch true $semidir $fbank_data $semidir/feat/fbank-pitch || exit 1
  echo "LOG: step02, done !"
fi
alidir=$seed_srcdir/ali-semi-conf$conf
if [ ! -z $step03 ]; then
  echo "LOG: step03, make alignment started @ `date`"
  [ -z $seed_srcdir ] && { echo "seed_srcdir expected"; exit 1; }
  effect_nj=$(wc -l < $fbank_data/spk2utt)
  [ $effect_nj -gt $nj ] && effect_nj=$nj
  steps/nnet/align.sh --cmd "$cmd" --nj $effect_nj \
  $fbank_data $lang  $seed_srcdir $alidir || exit 1   
  echo "LOG: step03, done @ `date`"
fi
plp_data=$semidir/plp-pitch
if [ ! -z $step04 ]; then
  echo "LOG: step04, make plp+pitch started"
  source/egs/swahili/make_feats.sh  --plp-pitch true $semidir $plp_data $semidir/feat/plp-pitch || exit 1
  echo "LOG: step04, done !"
fi
function gmm_training_check {
  [ ! -f $alidir/ali.1.gz ] && { echo "ERROR, alidir $alidir is not ready !"; exit 1; }
  [ ! -f $plp_data/feats.scp ] && { echo "ERROR, feats.scp expected from plp_data folder !"; exit 1; }
  echo "LOG: gmm_training_check is done !"
}

if [ ! -z $step05 ]; then
  echo "LOG: step05, train gmm-hmm started @ `date`"
  gmm_training_check || exit 1
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj \
  --steps 1,2,3,4,5,6,7  \
  --state-num $state_num --pdf-num $pdf_num \
  --alidir $alidir \
  ${devdata:+--devdata $devdata/plp_pitch} --devname $devname --decodename decode-$devname \
  $plp_data $lang $tgtdir
  echo "LOG: step05, gmm-hmm training done @ `date`"
fi
gmmdir=$tgtdir/tri4a
graphdir=$gmmdir/graph
if [ ! -z $step09 ]; then
  echo "LOG: step09, redo run-gmm-v2.sh for decoding @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj \
  --steps 7  \
  --state-num $state_num --pdf-num $pdf_num \
  --alidir $alidir \
  ${devdata:+--devdata $devdata/plp_pitch} --devname $devname --decodename decode-$devname \
  $plp_data $lang $tgtdir

  echo "LOG: step09, done @ `date`"
fi

conv_alidir=$gmmdir/ali-conv-semi-conf$conf
if [ ! -z $step06 ]; then
  echo "LOG: step06, ali conversion"
  [ -f $gmmdir/final.mdl ] || { echo "ERROR, $gmmdir/final.mdl do not exist"; exit 1; } 
  source/egs/swahili/convert-ali.sh $alidir $gmmdir $conv_alidir || exit 1
  echo "LOG: step06, done for $conv_alidir !"
fi

semi_nndir=$tgtdir/nnet5a
function transfer_learning_check {
  [ -f $seed_srcdir/final.nnet ] || { echo "ERROR, seed_srcdir is not ready !"; exit 1; }
  [ -f $fbank_data/feats.scp ] || { echo "ERROR, fbank_data (semi) is not ready !"; exit 1; }
  [ -f $conv_alidir/ali.1.gz ] || { echo "ERROR, conv_alidir $conv_alidir isn't ready !"; exit 1; }
  echo "LOG: transfer_learning_check is done !"
}
if [ ! -z $step07 ]; then
  echo "LOG: step07, transfer learning based ss (xe) started @ `date`"
  transfer_learning_check || exit 1
  source/egs/swahili/run-cross-train-nnet.sh --steps 1,2,3,4 --cmd "$cmd"  --graphdir $graphdir \
  ${devdata:+--devdata $devdata/fbank_pitch} --devname $devname --decodename decode-$devname \
  $fbank_data $lang $seed_srcdir  $conv_alidir  $semi_nndir || exit 1
  echo "LOG: step07, done for $semi_nndir @ `date`"
fi
smbr_nndir=$tgtdir/nnet6a-mpe
function smbr_training_check {
  [ -f $labelled_data/feats.scp ] || { echo "ERROR, labelled_data $labelled_data is not ready !"; exit 1; }
  [ -f $semi_nndir/final.nnet ] || { echo "ERROR, semi_nndir $semi_nndir is not ready !"; exit 1; }
  echo "LOG: smbr_training_check is done !"
}
if [ ! -z $step08 ]; then
  echo "LOG: step08, sMBR training started @ `date`"
  smbr_training_check || exit 1;
  source/egs/swahili/run-sequential-nnet.sh --steps 1,2,3,4,5 --train-cmd "run.pl" --cmd "$cmd" \
  ${devdata:+--devdata $devdata/fbank_pitch} --graphdir $graphdir --decodename decode-$devname \
  $labelled_data  $lang $semi_nndir  $smbr_nndir || exit 1
  echo "LOG: step08, done for $smbr_nndir training @ `date`"
fi
if [ ! -z $step10 ]; then
  echo "LOG: step10, run sMBR for decoding started @ `date`"
  smbr_training_check || exit 1;
  source/egs/swahili/run-sequential-nnet.sh --steps 4 --train-cmd "run.pl" --cmd "$cmd" \
  ${devdata:+--devdata $devdata/fbank_pitch} --graphdir $graphdir --decodename decode-$devname \
  $labelled_data  $lang $semi_nndir  $smbr_nndir || exit 1
  echo "LOG: step10, done @ `date`"
fi
