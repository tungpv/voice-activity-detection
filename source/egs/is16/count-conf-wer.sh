#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=slurm.pl
nj=40
steps=
# end options
echo
echo $0 $@
echo 

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $(basename $0) [options] <data> <tgtdir>
 [options]:
 [steps]:
 1: do regular checking work
 2: check vllp-mono-semi
 3: check vllp-ml-semi
 4: check llp-mono-semi
 5: check llp-ml-semi
END
}

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  echo "LOG: step01, check the reference ctm started @ `date`"
  cat  kws2016/flp-grapheme/exp/mono/nnet6a-mpe/ali_train/utt_ctm  | awk '{x+=$4;}END{print x/3600;}'
  cat  kws2016/flp-grapheme/exp/mono/nnet6a-mpe/ali_train/utt_ctm  | awk '{print $1;}' |sort -u | wc -l
  cat kws2016/flp-grapheme/data/train/segments | wc -l
  echo "LOG: step01, done @ `date`"
fi


if [ ! -z $step02 ]; then
  echo "LOG: step02, check vllp-mono-semi @ `date`"
  for conf in 0 0.5 0.7 0.9 1.0; do
    source/code/compute-wer-with-time-boundary --overlap-thresh=0.85 \
    kws2016/flp-grapheme/exp/mono/nnet6a-mpe/ali_train/utt_ctm  "cat kws2016/vllp-grapheme/exp/mono/nnet6a-mpe/decode_unlabelled/ctm_11/fbank_pitch.utt.ctm | awk -v conf=$conf '{if(\$NF >=conf){print;}}'|"
  done
  # 0.3369 0.4252 0.5149 0.6270 0.7525
  echo "LOG: step02, done @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "LOG: step03, check vllp-ml-semi @ `date`"
  for conf in 0 0.5 0.7 0.9 1.0; do
    source/code/compute-wer-with-time-boundary --overlap-thresh=0.85 \
    kws2016/flp-grapheme/exp/mono/nnet6a-mpe/ali_train/utt_ctm  "cat kws2016/vllp-grapheme/exp/mctl-is16/smbr-overall-tune/decode-unlabelled/ctm_12/fbank_pitch.utt.ctm | awk -v conf=$conf '{if(\$NF >=conf){print;}}'|"
  done
  # 0.4290 0.4943 0.5671 0.6568 0.7551
  echo "LOG: step03, done "
fi
if [ ! -z $step04 ]; then
  echo "LOG: step04, check llp-mono-semi @ `date`"
  hyp_ctm=kws2016/llp-grapheme/exp/mono/nnet6c-mpe/decode_unlabelled/ctm_11/fbank_pitch.utt.ctm
  for conf in 0 0.5 0.7 0.9 1.0; do
    source/code/compute-wer-with-time-boundary --overlap-thresh=0.85 \
    kws2016/flp-grapheme/exp/mono/nnet6a-mpe/ali_train/utt_ctm  "cat $hyp_ctm | awk -v conf=$conf '{if(\$NF >=conf){print;}}'|"
  done
  # 0.4190 0.4933 0.5720 0.6680 0.7624
  echo "LOG: step04, done"
fi

if [ ! -z $step05 ]; then
  echo "LOG: step05, check ll-ml-semi @ `date`"
  hyp_ctm=kws2016/llp-grapheme/exp/mctl-is16/smbr-overall-tune/decode-unlabelled/ctm_13/fbank_pitch.utt.ctm
  for conf in 0 0.5 0.7 0.9 1.0; do
    source/code/compute-wer-with-time-boundary --overlap-thresh=0.85 \
    kws2016/flp-grapheme/exp/mono/nnet6a-mpe/ali_train/utt_ctm  "cat $hyp_ctm | awk -v conf=$conf '{if(\$NF >=conf){print;}}'|"
  done
  # 0.4742 0.5339 0.6024 0.6868 0.7722
fi
