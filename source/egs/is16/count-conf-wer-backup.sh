#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=slurm.pl
nj=40
steps=
# end options
echo
echo $0 $@
echo 

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $(basename $0) [options] <data> <tgtdir>
 [options]:

END 
}


# if [ ! -z $step01 ]; then
#  echo "LOG: step01, check the reference ctm started @ `date`"
#  cat  kws2016/flp-grapheme/exp/mono/nnet6a-mpe/ali_train/utt_ctm  | awk '{x+=$4;}END{print x/3600;}'
#  cat  kws2016/flp-grapheme/exp/mono/nnet6a-mpe/ali_train/utt_ctm  | awk '{print $1;}' |sort -u | wc -l
#  cat kws2016/flp-grapheme/data/train/segments | wc -l
#  echo "LOG: step01, done @ `date`"
# fi


# if [ ! -z $step02 ]; then
#  echo "LOG: step02, check vllp-mono-semi `date`"
#  for conf in 0 0.5 0.7 0.9 1.0; do
#    source/code/compute-wer-with-time-boundary --overlap-thresh=0.85 \
#    kws2016/flp-grapheme/exp/mono/nnet6a-mpe/ali_train/utt_ctm  "cat kws2016/vllp-grapheme/exp/mono/nnet6a-mpe/decode_unlabelled/ctm_11/fbank_pitch.utt.ctm | awk -v conf=$conf '{if(\$NF >=conf){print;}}'|"
#  done
#  echo "LOG: step02, done @ `date`"
# fi
