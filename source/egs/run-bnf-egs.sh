# !/bin/bash


ilrn=0.04; flrn=0.004; nohup source/egs/run-bnf.sh --run_pnorm true --pnorm_opts "$ilrn $flrn  3000 300 4000000 1 7 400 llp2/data/train/bnf.mono.hierbn llp2/data/lang llp2/exp/bnf.mono.gmm/tri3a/ali_train.llp2 llp2/exp/pnorm$ilrn.$flrn" > nohupout/pnorm.$ilrn.$flrn.log 2>&1 &

ilrn=0.001; flrn=0.0001; nohup source/egs/run-bnf.sh --run_pnorm true --pnorm_opts "$ilrn $flrn  3000 300 4000000 1 7 400 llp2/data/train/bnf.mono.hierbn llp2/data/lang llp2/exp/bnf.mono.gmm/tri3a/ali_train.llp2 llp2/exp/pnorm$ilrn.$flrn" --decoding_eval true --decoding_opts "llp2/data/dev/bnf.mono llp2/data/lang llp2/exp/bnf.mono.gmm/tri3a llp2/exp/bnf.mono.gmm/tri3a/graph llp2/exp/pnorm$ilrn.$flrn/decode_dev10h steps/nnet2/decode.sh"
