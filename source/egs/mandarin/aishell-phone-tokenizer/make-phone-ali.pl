#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
my $uttList = '';
my $time = 1;
my $featLengths = '';
my $phoneMap = '';
my $silPhones = 'SIL';
my $minBegin = 0;
my $minEnd = 0;
my $minPercentageSil = 0;
GetOptions('utt-list|utt_list=s' => \$uttList,
           'time=i' => \$time,
           'feat-lengths|feat_lengths=s' => \$featLengths,
           'sil-phones|sil_phones=s' => \$silPhones,
           'min-begin|min_begin=i' => \$minBegin,
           'min-end|min_end=i' => \$minEnd,
           'min-percentage-sil|min_percentage_sil=f' => \$minPercentageSil,
           'phone-map|phone_map=s' => \$phoneMap) or die;

# begin sub
sub LoadLabelDict {
  my ($uttList, $dict) = @_;
  return if $uttList eq '';
  open(F, "$uttList") or die;
  while(<F>) {
    chomp;
    m:^(\S+)\s+(.*): or next;
    $$dict{$1} ++;
  }
  close F;
}
sub LoadFeatLenDict {
  my ($featLengths, $dict) = @_;
  return if $featLengths eq '';
  open(F, "$featLengths") or die;
  while(<F>) {
    chomp;
    m:^(\S+)\s+(\S+):g or next;
    $$dict{$1} = $2;
  }
  close F;
}
sub RemovePhonePositionMark {
  my ($s) = @_;
  $$s =~ s:_[SBIE]$::g;
}
sub Symbol2IntId {
  my ($phone) = @_;
  my @listSilPhones = split(/\s+/, $silPhones);
  RemovePhonePositionMark(\$phone);
  if($phone ~~ @listSilPhones) {
  	return 1;
  }
  return 0;
}

sub checkValid {
	my ($trans_ptr) = @_;
	my @trans = @$trans_ptr;
	my $numPhone = scalar @trans;
	my $countSilBegin = 0;
	my $countSilEnd = 0;
	for(my $i = 0; $i < $numPhone;$i++) {
		if($trans[$i] == 1) { 
			$countSilBegin++;
		}
		else {
			last;
		}	
	}
	for(my $i = $numPhone - 1; $i >= 0;$i--) {
		if($trans[$i] == 1) { 
			$countSilEnd++;
		}
		else {
			last;
		}	
	}
	return ($countSilBegin, $countSilEnd)
}
sub  MakePhoneAli {
  my ($phoneArray, $featLen, $time, $phoneIdArray, $dict) = @_;
  my $phoneId = 0;
  my $countSil = 0;
  for(my $i = 0; $i < scalar @$phoneArray; $i ++) {
    my $phone = $$phoneArray[$i];
    $phoneId= Symbol2IntId($phone);
    if ($phoneId == 1) {
    	$countSil++;
    }
    push @$phoneIdArray, $phoneId;
    if($time > 1 and $i > 0) {
      push @$phoneIdArray, $phoneId;
    }
  }
  my $actualFrame = scalar @$phoneIdArray;
  if($actualFrame < $featLen) {
    print STDERR "WARNING: There are ", $featLen - $actualFrame, " frame gaps\n";
  } elsif($actualFrame > $featLen) {
    die;
  }
  my $i = $actualFrame;
  while($i < $featLen) {
    push @$phoneIdArray, $phoneId;
    $i ++;
  }
  return $countSil;
}
# end sub
my %labelDict = ();
my %featLenDict = ();
my %phoneMapDict = ();

LoadLabelDict($uttList, \%labelDict);
LoadFeatLenDict($featLengths, \%featLenDict);
print STDERR "## LOG ($0): stdin expected\n";
my $countAllSent = 0;
my $countValidSent = 0;
while(<STDIN>) {
  chomp;
  m:^(\S+)\s+(.*)$:g or next;
  my ($label, $phones) = ($1, $2);
  if($uttList ne '') {
    next if not exists $labelDict{$label};
  }
  my @A = ();
  my @P = split(/\s+/, $phones);
  my $len = scalar @P;
  if($featLengths ne '') {
    if( not exists $featLenDict{$label}) {
      print STDERR "## WARNING ($0): no feats for $label\n";
      next;
    }
    $len = $featLenDict{$label};
  }
  my $numSil = MakePhoneAli(\@P, $len, $time, \@A, \%phoneMapDict);
  my ($numSilBegin, $numSilEnd) = checkValid(\@A);
  die if scalar @A != $len;
  my $perc = $numSil * 100.0/$len;
  $countAllSent++;
  if ($numSilBegin >= $minBegin && $numSilEnd >= $minEnd && $perc >= $minPercentageSil) {
  	print $label, " ", $numSilBegin, ",", $numSil, ",", $len, ",", $numSilEnd, " ", join(" ", @A), "\n";
  	$countValidSent++;
  }
  else {
  	print STDERR "## LOG ($0): Sentence $label has $numSilBegin , $numSil ($perc percent), $numSilEnd number of silence at begin, all sentence and at the end. Ignore it \n";
  }
}
print STDERR "## LOG ($0): Total number of sentence is $countAllSent . Number of valid sentence is $countValidSent \n";
print STDERR "## LOG ($0): stdin ended\n";
if($phoneMap ne '') {
  open(F, "|sort -k2n > $phoneMap") or die;
  foreach my $phone (keys %phoneMapDict) {
    print F "$phone $phoneMapDict{$phone}\n";
  }
  close F;
}
