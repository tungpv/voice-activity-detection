#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;
my $uttList = '';
my $time = 1;
my $featLengths = '';
my $phoneMap = '';
GetOptions('utt-list|utt_list=s' => \$uttList,
           'time=i' => \$time,
           'feat-lengths|feat_lengths=s' => \$featLengths,
           'phone-map|phone_map=s' => \$phoneMap) or die;

# begin sub
sub LoadLabelDict {
  my ($uttList, $dict) = @_;
  return if $uttList eq '';
  open(F, "$uttList") or die;
  while(<F>) {
    chomp;
    m:^(\S+)\s+(.*): or next;
    $$dict{$1} ++;
  }
  close F;
}
sub LoadFeatLenDict {
  my ($featLengths, $dict) = @_;
  return if $featLengths eq '';
  open(F, "$featLengths") or die;
  while(<F>) {
    chomp;
    m:^(\S+)\s+(\S+):g or next;
    $$dict{$1} = $2;
  }
  close F;
}
sub RemovePhonePositionMark {
  my ($s) = @_;
  $$s =~ s:_[SBIE]$::g;
}
sub Symbol2IntId {
  my ($phone, $dict) = @_;
  RemovePhonePositionMark(\$phone);
  if (exists $$dict{$phone}) {
    return $$dict{$phone};
  }
  my $nSize = keys%$dict;
  print STDERR "(", __LINE__, "), nSize=$nSize\n";
  $$dict{$phone} = $nSize;
  return $nSize;
}
sub  MakePhoneAli {
  my ($phoneArray, $featLen, $time, $phoneIdArray, $dict) = @_;
  my $phoneId = 0;
  for(my $i = 0; $i < scalar @$phoneArray; $i ++) {
    my $phone = $$phoneArray[$i];
    $phoneId= Symbol2IntId($phone, $dict);
    push @$phoneIdArray, $phoneId;
    if($time > 1 and $i > 0) {
      push @$phoneIdArray, $phoneId;
    }
  }
  my $actualFrame = scalar @$phoneIdArray;
  if($actualFrame < $featLen) {
    print STDERR "WARNING: There are ", $featLen - $actualFrame, " frame gaps\n";
  } elsif($actualFrame > $featLen) {
    die;
  }
  my $i = $actualFrame;
  while($i < $featLen) {
    push @$phoneIdArray, $phoneId;
    $i ++;
  }
}
# end sub
my %labelDict = ();
my %featLenDict = ();
my %phoneMapDict = ();

LoadLabelDict($uttList, \%labelDict);
LoadFeatLenDict($featLengths, \%featLenDict);
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m:^(\S+)\s+(.*)$:g or next;
  my ($label, $phones) = ($1, $2);
  if($uttList ne '') {
    next if not exists $labelDict{$label};
  }
  my @A = ();
  my @P = split(/\s+/, $phones);
  my $len = scalar @P;
  if($featLengths ne '') {
    if( not exists $featLenDict{$label}) {
      print STDERR "## WARNING ($0): no feats for $label\n";
      next;
    }
    $len = $featLenDict{$label};
  }
  MakePhoneAli(\@P, $len, $time, \@A, \%phoneMapDict);
  die if scalar @A != $len;
  print $label, " ", join(" ", @A), "\n";
}
print STDERR "## LOG ($0): stdin ended\n";
if($phoneMap ne '') {
  open(F, "|sort -k2n > $phoneMap") or die;
  foreach my $phone (keys %phoneMapDict) {
    print F "$phone $phoneMapDict{$phone}\n";
  }
  close F;
}
