#!/bin/bash 
. path.sh
. cmd.sh
cmd='slurm.pl --quiet --gres=gpu:1'

audio_dir=/home2/tungpham/vad_crosstalkRemover/hhx_vad/mandarin/audio_profChng
label_dir=/home2/tungpham/vad_crosstalkRemover/mydata/mml-12-sept-2017/profChngAndrewArmy/new_vad
data=tmp
if [ 0 -eq 1 ]; then
    mkdir -p $data
    source/egs/mandarin/prepare_test_data.sh $audio_dir $data
fi

if [ 0 -eq 1 ]; then
  feat-to-len scp:$data/feats.scp ark,t:feats_test.lengths
fi

if [ 0 -eq 1 ]; then
    python local/gen_label_vad.py $label_dir feats_test.lengths | ali-to-post ark,t:- ark:post_test.ark
fi

tgbnf=dnn_output
mkdir -p $tgbnf/log
if [ 0 -eq 1 ]; then
  steps/nnet/make_bn_feats.sh  --cmd "slurm.pl --quiet --gres=gpu:1" --remove-last-components 0 \
  --use-gpu yes $data/ppg $data dnn $tgbnf/log $tgbnf || exit 1  
fi

if [ 0 -eq 0 ]; then
	files=$tgbnf/*.ark
	for f in $files
	do
		python convertToTxt.py $f $tgbnf

	done	

fi

