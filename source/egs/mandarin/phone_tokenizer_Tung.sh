#!/bin/bash 
. path.sh
. cmd.sh
cmd='slurm.pl --quiet --gres=gpu:1'

dir=/home3/hhx502/w2017/seame-nnet3-feb-06-2017

alidir=$dir/exp-baseline/tri4a/ali_train
lang=$dir/data/lang

if [ 0 -eq 1 ]; then
  ali-to-phones --frame-shift=0.03 --per-frame=true $alidir/final.mdl  "ark:gzip -cd $alidir/ali.*.gz|" \
  "ark,t:|utils/int2sym.pl -f 2- $lang/phones.txt" > all_ali.txt
fi

data=$dir/data/train/mfcc-pitch
if [ 0 -eq 1 ]; then
  feat-to-len scp:$data/feats.scp ark,t:feats_train.lengths
fi

minBegin=5
minEnd=5
minPercent=10
if [ 0 -eq 1 ]; then
cat all_ali.txt | source/egs/mandarin/aishell-phone-tokenizer/make-phone-ali.pl --utt-list=$data/text --min-begin=$minBegin \
  --min-end=$minEnd --min-percentage-sil=$minPercent --feat-lengths=feats_train.lengths --sil-phones=SIL > all_ali_id_minBegin${minBegin}_minEnd${minEnd}_minPercentage${minPercent}.txt
fi

if [ 0 -eq 1 ]; then
cut -d' ' -f1,3- all_ali_id_minBegin${minBegin}_minEnd${minEnd}_minPercentage${minPercent}.txt | \
 ali-to-post ark,t:- ark:post.ark
fi


if [ 0 -eq 1 ]; then
  steps/make_fbank_pitch.sh --cmd "$cmd" --nj 30 \
    --fbank-config conf/fbank16k40-cv.conf --pitch-config conf/pitch16k-cv.conf \
    $data $log  $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
fi


valid=validate_dnn
train=train_dnn
if [ 0 -eq 1 ]; then
  source/egs/swahili/subset_data.sh --subset_time_ratio 0.1 \
  --random true \
  --data2 $train \
  $dir/data/train/fbank-pitch  $valid
fi

srcdir=dnn
train_tool_opts="--minibatch-size=256 --randomizer-size=32768 --randomizer-seed=777"

if [ 0 -eq 0 ]; then
  echo "## LOG (step05): training nnet started @ `date`"
  $cmd $srcdir/dnn.log \
  steps/nnet/train.sh --learn-rate 0.008 \
  --train-tool-opts "$train_tool_opts" \
  --skip-lang-check true \
  --delta-opts "--delta-order=2" \
  --scheduler-opts "--max-iters 30" \
  --feat-type traps \
  --hid-layers 4 --hid-dim 1024 \
  --splice 5 \
  --num-tgt 2 \
  --labels ark:post.ark \
  $train $valid dummy dummy dummy $srcdir 
fi
