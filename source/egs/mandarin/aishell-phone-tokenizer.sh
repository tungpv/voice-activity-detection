#!/bin/bash 


. path.sh
. cmd.sh

echo 
echo "LOG: $0 $@"
echo

# begin sub
cmd='slurm.pl --quiet'
nj=120
steps=
numleaves=7000
hidden_dim=1024
train_stage=-10
get_egs_stage=-10
frames_per_eg=150
minibatch_size=128
num_epochs=4
proportional_shrink=0
chainname=chain1024
num_jobs_initial=3
num_jobs_final=16
remove_egs=false
max_param_change=2.0
initial_effective_lrate=0.001
final_effective_lrate=0.0001
xent_regularize=0.1
remove_egs=false
get_egs_stage=-10
common_egs_dir=
# end sub

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 $0 --steps 1 --cmd "$cmd" --nj $nj \
  update-lexicon-transcript-oct13/data/man-aishell017 \
  ./update-lexicon-transcript-oct13/exp/tri4a/ali_train/pron_perutt_nowb.txt \
  update-lexicon-transcript-oct13/aishell-phone-tokenizer  

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 3 ]; then
  Example && exit 1
fi

srcdata=$1
srcdict=$2
updatedir=$3
[ -d $updatedir ] || mkdir -p $updatedir
if [ ! -z $step01 ]; then
  for sdata in $srcdata; do
    data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch/data; log=$sdata/feat/fbank-pitch/log
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_fbank_pitch.sh --cmd "$cmd" --nj $nj \
    --fbank-config conf/fbank16k40-cv.conf --pitch-config conf/pitch16k-cv.conf \
    $data $log  $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
  done
fi

if [ ! -z $step02 ]; then
  data=$srcdata/fbank-pitch
  feat-to-len scp:$data/feats.scp ark,t:$data/feats.lengths
fi
alidir=update-lexicon-transcript-oct13/exp/tri4a/ali_train
lang=update-lexicon-transcript-oct13/data/lang-train
data=$srcdata/fbank-pitch
if [ ! -z $step03 ]; then
  ali-to-phones --frame-shift=0.01 --per-frame=true $alidir/final.mdl  "ark:gzip -cd $alidir/ali.*.gz|" \
  "ark,t:|utils/int2sym.pl -f 2- $lang/phones.txt" | \
  source/egs/mandarin/aishell-phone-tokenizer/make-phone-ali.pl --utt-list=$data/text \
  --time=2 --feat-lengths=$data/feats.lengths --phone-map=$data/phones.txt | \
  ali-to-post ark:- ark:$data/post
fi
nnet_data=$updatedir/data
valid=$nnet_data/validate
train=$nnet_data/train
if [ ! -z $step04 ]; then
  source/egs/swahili/subset_data.sh --subset_time_ratio 0.1 \
  --random true \
  --data2 $train \
  $data  $valid
fi
srcdir=$updatedir/dnn
train_tool_opts="--minibatch-size=256 --randomizer-size=32768 --randomizer-seed=777"
if [ ! -z $step05 ]; then
  echo "## LOG (step05): training nnet started @ `date`"
  $cmd $srcdir/dnn.log \
  steps/nnet/train.sh --learn-rate 0.008 \
  --train-tool-opts "$train_tool_opts" \
  --skip-lang-check true \
  --delta-opts "--delta-order=2" \
  --scheduler-opts "--max-iters 30" \
  --feat-type traps \
  --hid-layers 6 --hid-dim 2048 \
  --splice 5 \
  --num-tgt $(wc -l < $data/phones.txt) \
  --labels ark:$data/post \
  $train $valid dummy dummy dummy $srcdir 
fi
tgtdata=./update-lexicon-transcript-oct13/aishell-phone-tokenizer/data/tgt-speaker
[ -d $tgtdata ] || mkdir -p $tgtdata
if [ ! -z $step06 ]; then
  if [ ! -f $tgtdata/wavlist.txt ]; then
    find /home/xht211/data/ManBC2010/wav -name "*.wav" > $tgtdata/wavlist.txt
  fi
  cat $tgtdata/wavlist.txt | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; $wav = $_; $wav =~ s:.*\/::g; $wav =~ s:\.wav::g; print "spk", "$wav $_\n"; ' > $tgtdata/wav.scp
fi
if [ ! -z $step07 ]; then
  cat $tgtdata/wav.scp | \
  awk '{print $1;}'  | \
  perl -ane 'use utf8; use open qw(:std :utf8);chomp; print "$_ spk0\n";' > $tgtdata/utt2spk
  utils/utt2spk_to_spk2utt.pl < $tgtdata/utt2spk > $tgtdata/spk2utt
  utils/fix_data_dir.sh $tgtdata
fi
if [ ! -z $step08 ]; then
  for sdata in $tgtdata; do
    data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch/data; log=$sdata/feat/fbank-pitch/log
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_fbank_pitch.sh --cmd "$cmd" --nj $nj \
    --fbank-config conf/fbank16k40-cv.conf --pitch-config conf/pitch16k-cv.conf \
    $data $log  $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
  done
fi
if [ ! -z $step09 ]; then
  steps/nnet/make_bn_feats.sh  --cmd "slurm.pl --quiet --gres=gpu:1" --remove-last-components 0 \
  --use-gpu yes $tgtdata/ppg $tgtdata/fbank-pitch $srcdir $tgtdata/ppg/log $tgtdata/ppg/data || exit 1  
fi
