#!/bin/bash

. path.sh
. cmd.sh


# begin options
prepare_lang=false
subsetlm_test=false
run_subword=false
subword_train_model=false
subword_train_model_opts="<word_count(.gz file)> <use_word_count(trune/false)> <target_dir>"
subword_make_map=false
subword_make_map_opts="<count_word_list> <model> <word_to_subword_map>"
subword_make_vocab=false
subword_vocab_opts="<svocab> <model> <dir>"
subword_make_text=false
subword_make_text_opts="<stext> <map> <new_text.gz>"
train_srilms=false
ngram_count_opts="-kndiscount1 -gt1min 0 -kndiscount2 -gt2min 2 -kndiscount3 -gt3min 3 -order 3 -unk -sort"
train_srilm_opts="<vocab> <text.gz> <lm.gz>"
interpolating_srilms=false
interpolating_opts="<ood_lmfile> <ind_lmfile> <dev_text> <dir> <intId>"
make_glang=false
make_glang_opts="<vocab> <slocal> <dir> <langId>"
make_grammar=false
make_grammar_opts="<lmfile> <slang> <lang>"
run_swah_subword=false
swah_subword_opts="<stext> <svocab> <model> <lmdir> <dir>"
run_subword_ilm=false
run_subword_tamil=false
make_subword_data=false
make_subword_data_opts="<sdata> <word2subword_map> <data>"
lexlm_viet=false
decoding_cmd="steps/nnet/decode.sh"
lexlm_tami=false
lexlm_swah=false
lexlm_swah_opts="<tgtdir> <sdir> <vocab> <lmname>"
swah_vocab_select=false
swah_vocab_opts="<tgtdir> <devdata> <oov_rates> <increase_stride>"
lex_fname=
wdlist_fname=
lm_fname=
langdir=
steps=
train_text=
dev_text=
kws_run=false
kws_index_done=true
kws_output_id=
kwsdatadir=
kws_run_opts="<kws_data> <kws_lang> <kws_source_dir> <kws_sdir> <kws_latdir> <kws_lmwt> <kwlistId> [kws_morfessor_model]"
make_index=false
make_index_opts="<lmwt> <lat_index_num> <prune_beam> <data> <lang> <sdir> <latdir> <pruned_latdir>"
tgtdir=
# end options

echo
echo "$0 $@"
echo

. parse_options.sh

function PrintOptions {
  cat<<END

$0 [options]:
prepare_lang				# value, $prepare_lang
subsetlm_test				# value, $subsetlm_test
run_subword				# value, $run_subword
subword_train_model			# value, $subword_train_model          			       # updated, use it, step01
subword_train_model_opts		# value, "$subword_train_model_opts"
subword_make_map			# value, $subword_make_map             			       # step02
subword_make_map_opts			# value, "$subword_make_map_opts"
subword_make_vocab			# value, $subword_make_vocab	      			       # step03
subword_vocab_opts			# value, "$subword_vocab_opts"
subword_make_text			# value, $subword_make_text					# step04
subword_make_text_opts			# value, "$subword_make_text_opts"					
train_srilms				# value, $train_srilms
ngram_count_opts			# value, "$ngram_count_opts"
train_srilm_opts			# value, "$train_srilm_opts"					# step05
interpolating_srilms			# value, $interpolating_srilms          			# step06
interpolating_opts			# value, $interpolating_opts					 
make_glang				# value, $make_glang						# step07
make_glang_opts				# value, "$make_glang_opts"					
make_grammar				# value, $make_grammar						# step08
make_grammar_opts			# value, "$make_grammar_opts"
run_swah_subword			# value, $run_swah_subword
swah_subword_opts			# value, "$swah_subword_opts"
run_subword_ilm				# value, $run_subword_ilm
run_subword_tamil			# value, $run_subword_tamil
make_subword_data			# value, $make_subword_data
make_subword_data_opts			# value, "$make_subword_data_opts"
lexlm_viet				# value, $lexlm_viet
decoding_cmd				# value, "$decoding_cmd"
lexlm_tami				# value, $lexlm_tami
lexlm_swah				# value, $lexlm_swah
lexlm_swah_opts				# value, "$lexlm_swah_opts"
swah_vocab_select			# value, $swah_vocab_select
swah_vocab_opts				# value, "$swah_vocab_opts"
lex_fname				# value, "$lex_fname"
wdlist_fname				# value, "$wdlist_fname"
lm_fname				# value, "$lm_fname"
langdir					# value, "$langdir"
steps					# value, "$steps"
train_text				# value, "$train_text"
dev_text				# value, "$dev_text"
kws_run					# value, $kws_run
kws_index_done				# value, $kws_index_done
kws_output_id				# value, "$kws_output_id"
kwsdatadir				# value, "$kwsdatadir"
kws_run_opts				# value, "$kws_run_opts"
make_index				# value, $make_index
make_index_opts				# value, "$make_index_opts"

tgtdir					# value, "$tgtdir"

END
}

PrintOptions

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if $prepare_lang; then
  echo "$0: prepare_lang started @ `date`"
  [ ! -z $langdir ] || \
  { echo "$0: prepare_lang: ERROR, langdir is not specified"; exit 1; }
  source/test/lex-lm-update.sh  \
  ${step01:+--make-grapheme-lex true } ${lex_fname:+--lexfname $lex_fname} ${wdlist_fname:+--wdlist_fname $wdlist_fname} \
  ${step02:+--make-grammar true } ${lm_fname:+--lm-fname $lm_fname} \
  $langdir
  echo "$0: prepare_lang ended @ `date`"
fi

if $subsetlm_test; then
  echo "$0: subsetlm_test started @ `date`"
  [ ! -z $tgtdir ] || \
  { echo "$0: subsetlm_test: ERROR, tgtdir $tgtdir expected"; exit 1; }
  [ ! -z $train_text ] || \
  { echo "$0: subsetlm_test: ERROR, train_text $train_text expected"; exit 1; }
  [ ! -z $dev_text ] || \
  { echo "$0: subsetlm_test: ERROR, dev_text $dev_text expected"; exit 1; }
  [ -d $tgtdir ] || mkdir -p $tgtdir
  source_lm=flp2/data/srilm/lm.gz
  cut -d' ' -f2- $train_text > $tgtdir/train.txt
  cut -d' ' -f2- $dev_text > $tgtdir/dev.txt
  source/test/lex-lm-update.sh --tgtdir $tgtdir \
  ${step01:+--subsetlm-extract-subvocab true  --subsetlm-subvocab 1000 } \
  ${step02:+--subsetlm-extract-sublm true --subsetlm-source-lm $source_lm } \
  ${step03:+--subsetlm-train true} 
  echo "$0: subsetlm_test ended @ `date`"
fi

if $lexlm_viet; then
  echo "$0: lexlm_viet started @ `date`"
  [ -z $tgtdir ] && \
  { echo "$0: lexlm_viet: ERROR, tgtdir not specified"; exit 1; }
  [ -d $tgtdir ] || mkdir -p $tgtdir
  cat grapheme_vllp/data/dev/text | cut -d' ' -f2- > $tgtdir/dev.txt
  ngram_count_source_opts="-vocab $tgtdir/vocab -text $tgtdir/text.gz -lm $tgtdir/bd.3gram.kn013.gz"
  glangdir=xl_grapheme_vllp/data
  ngramPruneCmd="ngram -order 3 -prune 1e-7"
  vocab_filter_cmd="source/test/openkws_longer_words.pl 2 12"
  interpolating_opts="$tgtdir/bd.3gram.kn013.gz grapheme_vllp/data/srilm/lm.gz $tgtdir/dev.txt $tgtdir"
  make_glang_opts="$tgtdir/vocab grapheme_vllp/data/local xl1_grapheme_vllp"
  sdir=grapheme_vllp/exp/cross.fbank.nnet
  decoding_opts="grapheme_vllp/data/dev/fbank_pitch xl1_grapheme_vllp/lang"
  decoding_opts="$decoding_opts $sdir $sdir/graph.xl1 $sdir/decode.dev10.xl1"
  pruning_opts="-order 3 -prune 1e-5"
  pruning_source="$tgtdir/final.int.lm.gz $tgtdir/pruned.final.int.lm.gz"
  make_grammar_opts="$tgtdir/final.int.lm.gz xl1_grapheme_vllp/lang xl1_grapheme_vllp/lang"
  make_vocab_cmd="source/test/openkws_make_vocab.pl 0.01 0.0001 1000"
  source/test/lex-lm-update.sh --tgtdir $tgtdir \
  --lexlm_slang grapheme_vllp/data/lang \
  --lexlm_slocal grapheme_vllp/data/local \
  ${step01:+--lexlm_prepare_vidata2012 true } \
  ${step02:+--lexlm_prepare_vidata2013 true} \
  ${step03:+--lexlm_prepare_viweb true } \
  ${step04:+--lexlm_wordcount_vi true } \
  ${step05:+--lexlm_make_vocab true --vocab_filter_cmd "$vocab_filter_cmd" --make_vocab_cmd "$make_vocab_cmd" } \
  ${step06:+--lexlm_train_srilms true --ngram-count-opts "$ngram_count_opts" --ngram_count_source_opts "$ngram_count_source_opts"} \
  ${step07:+--lexlm_interpolating true --interpolating_opts "$interpolating_opts" } \
  ${step08:+--lexlm_pruning true --pruning_source "$pruning_source" --pruning_opts "$pruning_opts"} \
  ${step09:+--make_glang true --make_glang_opts "$make_glang_opts"} \
  ${step10:+--make_grammar true --make_grammar_opts "$make_grammar_opts"} \
  ${step11:+--decoding-eval true  --decoding_cmd "$decoding_cmd" --decoding_opts "$decoding_opts"}
  echo "$0: lexlm_viet ended @ `date`"
fi
if $lexlm_swah; then
  echo "$0: lexlm_swah started @ `date`"
  optNames="tgtdir sdir vocab lmname"
  . source/register_options.sh "$optNames" "$lexlm_swah_opts" || \
  { echo "$0: lexlm_swah: ERROR, @register_options.sh"; exit 1; }
  [ -d $tgtdir ] || mkdir -p $tgtdir
  vocab_filter_cmd="source/test/openkws_longer_words.pl 1 25"
  make_vocab_cmd="source/test/openkws_make_vocab.pl 0.01 0.0001 1000"
  ngram_count_source_opts="-vocab $vocab -text $tgtdir/text.gz -lm $lmname.kn023.gz"
  lmId=$(basename $lmname)
  interpolating_opts="$lmname.kn023.gz grapheme_vllp/data/srilm/lm.gz $tgtdir/dev.txt $tgtdir $lmId"
  make_glang_opts="$vocab grapheme_vllp/data/local grapheme_vllp/data $lmId"
  make_grammar_opts="$tgtdir/final.int.${lmId}.lm.gz grapheme_vllp/data/lang.$lmId grapheme_vllp/data/lang.$lmId"
  decoding_opts="grapheme_vllp/data/tuning/plp_pitch grapheme_vllp/data/lang.$lmId"
  decoding_opts="$decoding_opts $sdir $sdir/graph.$lmId $sdir/decode.tuning.$lmId"

  source/test/lex-lm-update.sh --tgtdir $tgtdir \
  --lexlm_slang grapheme_vllp/data/lang \
  --lexlm_slocal grapheme_vllp/data/local \
  ${step01:+--lexlm_prepare_swahweb true } \
  ${step02:+--lexlm_wordcount_swah true} \
  ${step03:+--lexlm_train_srilms true --ngram-count-opts "$ngram_count_opts" --ngram_count_source_opts "$ngram_count_source_opts" } \
  ${step04:+--lexlm_interpolating true --interpolating_opts "$interpolating_opts"  } \
  ${step05:+--make_glang true --make_glang_opts "$make_glang_opts"} \
  ${step06:+--make_grammar true --make_grammar_opts "$make_grammar_opts" } \
  ${step07:+--decoding-eval true  --decoding_cmd "$decoding_cmd" --decoding_opts "$decoding_opts" }
  echo "$0: lexlm_swah ended @ `date`"
fi
if $swah_vocab_select; then
  echo "$0: swah_vocab_select started @ `date`"
  optNames="tgtdir dev oovs stride"
  . source/register_options.sh "$optNames" "$swah_vocab_opts" || \
  { echo "$0: swah_vocab_select: ERROR, @register_options.sh"; exit 1; }
  [ -d $tgtdir ] || mkdir -p $tgtdir
  [ -f $tgtdir/in_domain_vocab ] || \
  { cat grapheme_vllp/data/lang/words.txt | egrep -v '<eps>|#' |awk '{print $1;}' > $tgtdir/in_domain_vocab; }
  [ -f $tgtdir/dev.txt ] || \
  { cat $dev/text |cut -d" " -f2-  > $tgtdir/dev.txt;  }
  vocab_filter_cmd="source/test/openkws_longer_words.pl 1 25 2"
  for x in $(echo $oovs| perl -pe 's/[:]/ /g;'); do
    oov=$(echo $x| perl -pe '$s=sprintf("%.4f",$_/100); $_=$s,"\n";')
    make_vocab_cmd="source/test/openkws_make_vocab.pl $oov 0.0001 $stride"
    source/test/lex-lm-update.sh --tgtdir $tgtdir \
    --lexlm_vocab_size 500000 \
    --lexlm_slang grapheme_vllp/data/lang \
    --lexlm_slocal grapheme_vllp/data/local \
    --lexlm_make_vocab true --vocab_filter_cmd "$vocab_filter_cmd" --make_vocab_cmd "$make_vocab_cmd"
  done
  echo "$0: swah_vocab_select ended @ `date`"
fi
if $lexlm_tami; then
  echo "$0: lexlm_tami started @ `date`"
  [ -z $tgtdir ] && \
  { echo "$0: lexlm_tami: ERROR, tgtdir not specified"; exit 1; }
  [ -d $tgtdir ] || mkdir -p $tgtdir
  cat grapheme_vllp/data/dev/text | cut -d' ' -f2- > $tgtdir/dev.txt
  ngram_count_source_opts="-vocab $tgtdir/vocab -text $tgtdir/text.gz -lm $tgtdir/bd.3gram.kn023.gz"
  glangdir=xl_grapheme_vllp/data
  sdir=grapheme_vllp/exp/cross.fbank.nnet
  decoding_opts="grapheme_vllp/data/dev/fbank_pitch xl1_grapheme_vllp/lang"
  decoding_opts="$decoding_opts $sdir $sdir/graph.xl1 $sdir/decode.dev10.xl1"
  make_vocab_cmd="source/test/openkws_make_vocab.pl 0.01 0.0001 1000"
  vocab_filter_cmd="source/test/openkws_longer_words.pl 1 25"
  interpolating_opts="$tgtdir/bd.3gram.kn023.gz grapheme_vllp/data/srilm/lm.gz $tgtdir/dev.txt $tgtdir"
  make_glang_opts="$tgtdir/vocab grapheme_vllp/data/local xl1_grapheme_vllp"
  make_grammar_opts="$tgtdir/final.int.lm.gz xl1_grapheme_vllp/lang xl1_grapheme_vllp/lang"
  source/test/lex-lm-update.sh --tgtdir $tgtdir \
  --lexlm_slang grapheme_vllp/data/lang \
  --lexlm_slocal grapheme_vllp/data/local \
  ${step03:+--lexlm_prepare_tamiweb true } \
  ${step04:+--lexlm_wordcount_tamil true } \
  ${step05:+--lexlm_make_vocab true --vocab_filter_cmd "$vocab_filter_cmd" --make_vocab_cmd "$make_vocab_cmd" } \
  ${step06:+--lexlm_train_srilms true --ngram-count-opts "$ngram_count_opts" --ngram_count_source_opts "$ngram_count_source_opts" } \
  ${step07:+--lexlm_interpolating true --interpolating_opts "$interpolating_opts"  } \
  ${step08:+--lexlm_pruning true --pruning_opts "$pruning_opts" } \
  ${step09:+--make_glang true --make_glang_opts "$make_glang_opts"} \
  ${step10:+--make_grammar true --make_grammar_opts "$make_grammar_opts"  } \
  ${step11:+--decoding-eval true  --decoding_cmd "$decoding_cmd" --decoding_opts "$decoding_opts" }
  echo "$0: lexlm_tami ended @ `date`"
fi
if $run_swah_subword; then
  echo "$0: run_swah_subword started @ `date`"
  optNames="stext svocab model lmdir dir"
  . source/register_options.sh "$optNames" "$swah_subword_opts" || \
  { echo "$0: run_swah_subword: ERROR, @register_options.sh"; exit 1; }
  
  [ -d $dir ] || mkdir -p $dir
  ngram_count_source_opts="-vocab $lmdir/vocab -text $lmdir/text.gz -lm $lmdir/lm3.23.gz"
  subword_vocab_opts="$svocab $model $lmdir" 
  source/test/lex-lm-update.sh --tgtdir $dir  \
  ${step01:+--subword_make_vocab true  --subword_vocab_opts "$subword_vocab_opts"} \
  ${step02:+--subword_make_map true} \
  ${step03:+--subword_make_text true --subword_text_opts "$stext $dir/word2subword_map.gz $lmdir/text.gz"} \
  ${step04:+--subword_make_text true --subword_text_opts "$stext $dir/word2subword_map.gz $lmdir/dev.gz"} \
  ${step05:+--lexlm_train_srilms true --ngram_count_opts "$ngram_count_opts" --ngram_count_source_opts "$ngram_count_source_opts"} \
  ${step06:+--lexlm_interpolating true --interpolating_opts "$interpolating_opts" } \
  ${step07:+--make-glang true --make_glang_opts "$make_glang_opts"} \
  ${step08:+--make_grammar true --make_grammar_opts "$make_grammar_opts"} 
  echo "$0: run_swah_subword ended @ `date`"
fi
# step 1
if $subword_train_model; then
  echo "$0: subword_train_model started @ `date`"
  optNames="word_count use_word_count dir"
  . source/register_options.sh "$optNames" "$subword_train_model_opts" || \
  { echo "$0: subword_train_model: ERROR, @register_options.sh"; exit 1; }
  [ -d $dir ] || mkdir -p $dir
  if $use_word_count; then
    [ -f $dir/traindata_list ] || \
    gzip -cd $word_count | awk '{s=sprintf("%d\t%s",$2,$1); print s;}' > $dir/traindata_list
  else
    [ -f $dir/traindata_list ] || \
    gzip -cd $word_count | awk '{s=sprintf("%d\t%s",1,$1); print s;}' > $dir/traindata_list
  fi
  source/test/lex-lm-update.sh --tgtdir $dir  --subword_train true 
  echo "$0: subword_train_model ended @ `date`"
fi
# step 2
if $subword_make_map; then
  echo "$0: subword_make_map started @ `date`"
  optNames="cwlist model map"
  . source/register_options.sh "$optNames" "$subword_make_map_opts" || \
  { echo "$0: subword_make_map: ERROR, @register_options.sh"; exit 1; }
  dir=$(dirname $map)
  [ -d $dir ] || mkdir -p $dir
  source/test/lex-lm-update.sh  --tgtdir $dir \
  --subword_make_map true --subword_make_map_opts "$subword_make_map_opts" || exit 1
  echo "$0: subword_make_map ended @ `date`"
fi
# step 3
if $subword_make_vocab; then
  echo "$0: subword_make_vocab started @ `date`"
  optNames="svocab model dir"
  . source/register_options.sh "$optNames" "$subword_vocab_opts" || \
  { echo "$0: subword_make_vocab: ERROR, @register_options.sh"; exit 1;}
  source/test/lex-lm-update.sh --tgtdir $dir  \
  --subword_make_vocab true  --subword_vocab_opts "$subword_vocab_opts"
  echo "$0: subword_make_vocab ended @ `date`"
fi
# step 4
if $subword_make_text; then
  echo "$0: subword_make_text started @ `date`"
  optNames="stext map new_text"
  . source/register_options.sh "$optNames" "$subword_make_text_opts" || \
  { echo "$0: subword_make_text: ERROR, @register_options.sh"; exit 1; }
  dir=$(dirname $new_text)
  source/test/lex-lm-update.sh --tgtdir $dir  \
  --subword_make_text true --subword_text_opts "$subword_make_text_opts" || exit 1
  echo "$0: subword_make_text ended @ `date`"
fi
# step 5
if $train_srilms; then
  echo "$0: train_srilms started @ `date`"
  optNames="vocab text lmFile"
  . source/register_options.sh "$optNames" "$train_srilm_opts" || \
  { echo "$0: train_srilms: ERROR, @register_options.sh"; exit 1; }
  dir=$(dirname $lmFile) 
  [ -d $dir ] || mkdir -p $dir
  ngram_count_source_opts="-vocab $vocab -text $text -lm $lmFile"
  source/test/lex-lm-update.sh --tgtdir $dir  \
  --lexlm_train_srilms true --ngram_count_opts "$ngram_count_opts" --ngram_count_source_opts "$ngram_count_source_opts"
  echo "$0: train_srilms ended @ `date`"
fi
# step 6
if $interpolating_srilms; then
  echo "$0: interpolating_srilms started @ `date`"
  optNames="olm ilm devdata lmdir intId"
  . source/register_options.sh "$optNames" "$interpolating_opts" || \
  { echo "$0: interpolating_srilms: ERROR, @register_options.sh"; exit 1; }
  dir=$(dirname $lmdir)
  [ -d $dir ] || mkdir -p $dir
  source/test/lex-lm-update.sh --tgtdir $dir  \
  --lexlm_interpolating true --interpolating_opts "$interpolating_opts" || exit 1
  echo "$0: interpolating_srilms ended @ `date`"
fi
# step 7
if $make_glang; then
  echo "$0: make_glang started @ `date`"
  optNames="x1 x2 dir x3"
  . source/register_options.sh "$optNames" "$make_glang_opts" || \
  { echo "$0: make_glang: ERROR, @ register_options.sh"; exit 1; }
  [ -d $dir ] || mkdir -p $dir
  source/test/lex-lm-update.sh --tgtdir $dir \
  --make-glang true --make_glang_opts "$make_glang_opts"  || exit 1
  echo "$0: make_glang ended @ `date`"
fi
# step 8
if $make_grammar; then
  echo "$0: make_grammar started @ `date`"
  optNames="x1 x2 lang"
  . source/register_options.sh "$optNames" "$make_grammar_opts" ||\
  { echo "$0: make_grammar: ERROR, @register_options.sh"; exit 1; }
  dir=$lang
  [ -d $dir ] || mkdir -p $dir
  source/test/lex-lm-update.sh --tgtdir $dir \
  --make_grammar true --make_grammar_opts "$make_grammar_opts" || exit 1
  echo "$0: make_grammar ended @ `date`"
fi
if $make_subword_data; then
  echo "$0: make_subword_data started @ `date`"
  optNames="sdata map data"
  . source/register_options.sh "$optNames" "$make_subword_data_opts" || \
  { echo "$0: make_subword_data: ERROR, @register_options.sh for $make_subword_data_opts"; exit 1; }
  [ -f $sdata/text ] || \
  { echo "$0: make_subword_data: ERROR, text file is not ready for folder $data"; exit 1; }
  [ -d $data ] || mkdir -p $data
  cp $sdata/*  $data; rm $data/text
  cat $sdata/text | \
  source/test/openkws_transform_text.pl "gzip -cd $map|" > $data/text
  echo "$0: make_subword_data ended @ `date`"
fi
if $run_subword; then
  echo "$0: run_subword started @ `date`"
  [ ! -f $tgtdir/in-vocab-count.gz ] && \
  { echo "$0: run_subword: ERROR, in-vocab-count.gz does not exist in folder $tgtdir"; exit 1; }
  dir=$tgtdir/subword; [ -d $dir ] || mkdir -p $dir
  ngram_count_opts="-kndiscount1 -gt1min 0 -kndiscount2 -gt2min 2 -kndiscount3 -gt3min 3 -order 3 -unk -sort"
  lmdir=$dir/olm
  ngram_count_source_opts="-vocab $lmdir/vocab -text $lmdir/text.gz -lm $lmdir/lm.023.gz"
  subword_vocab_opts="$tgtdir/vocab $dir/morfessor_model.bin $dir/olm" 
  [ -f $dir/traindata_list ] || \
  gzip -cd $tgtdir/in-vocab-count.gz | awk '{s=sprintf("%d\t%s",$2,$1); print s;}' > $dir/traindata_list
  source/test/lex-lm-update.sh --tgtdir $dir  \
  ${step01:+--subword_train true } \
  ${step02:+--subword_make_vocab true  --subword_vocab_opts "$subword_vocab_opts"} \
  ${step03:+--subword_make_map true} \
  ${step04:+--subword_make_text true --subword_text_opts "$tgtdir/text.gz $dir/word2subword_map.gz $dir/olm/text.gz"} \
  ${step05:+--subword_make_text true --subword_text_opts "$tgtdir/dev.txt $dir/word2subword_map.gz $dir/olm/dev.gz"} \
  ${step06:+--lexlm_train_srilms true --ngram_count_opts "$ngram_count_opts" --ngram_count_source_opts "$ngram_count_source_opts"}
  echo "$0: run_subword ended @ `date`"
fi
if $run_subword_ilm; then
  echo "$0: run_subword_ilm started @ `date`"
  dir=$tgtdir/subword; [ -d $dir ] || mkdir -p $dir
  ngram_count_opts="-gt1min 0 -gt2min 2 -gt3min 3  -order 3 -unk -sort"
  lmdir=$dir/ilm
  ngram_count_source_opts="-vocab $lmdir/vocab -text $lmdir/text.gz -lm $lmdir/lm3.023.gz"
  subword_vocab_opts="grapheme_vllp/data/srilm/vocab $dir/morfessor_model.bin $dir/ilm" 
  itext=grapheme_vllp/data/srilm/train.txt
  interpolating_opts="$dir/olm/lm3.23.gz $lmdir/lm3.023.gz $lmdir/dev.gz $dir"
  pruning_opts="-order 3 -prune 1e-8"
  pruning_source="$dir/final.int.lm.gz $dir/pruned.final.int.lm.gz"
  make_glang_opts="$dir/olm/vocab grapheme_vllp/data/local subxl_grapheme_vllp"
  make_grammar_opts="$dir/final.int.lm.gz subxl_grapheme_vllp/lang subxl_grapheme_vllp/lang"

  sdir=grapheme_vllp/exp/cross.fbank.nnet
  decoding_opts="grapheme_vllp/data/dev/fbank_pitch subxl_grapheme_vllp/lang $sdir $sdir/graph.subxl $sdir/decode.subxl.dev10h"
  source/test/lex-lm-update.sh --tgtdir $dir  \
  ${step01:+--subword_make_vocab true  --subword_vocab_opts "$subword_vocab_opts"} \
  ${step02:+--subword_make_text true --subword_text_opts "$itext $dir/word2subword_map.gz $lmdir/text.gz"} \
  ${step03:+--subword_make_text true --subword_text_opts "$tgtdir/dev.txt $dir/word2subword_map.gz $dir/ilm/dev.gz"} \
  ${step04:+--lexlm_train_srilms true --ngram_count_opts "$ngram_count_opts" --ngram_count_source_opts "$ngram_count_source_opts"} \
  ${step05:+--lexlm_interpolating true --interpolating_opts "$interpolating_opts" } \
  ${step06:+--lexlm_pruning true --pruning_opts "$pruning_opts" --pruning_source "$pruning_source"} \
  ${step07:+--make-glang true --make_glang_opts "$make_glang_opts"} \
  ${step08:+--make_grammar true --make_grammar_opts "$make_grammar_opts"} \
  ${step09:+--decoding_eval true --decoding_cmd "steps/nnet/decode.sh" --decoding_opts "$decoding_opts"}
  echo "$0: run_subword_ilm ended @ `date`"
fi
if $run_subword_tamil; then
  echo "$0: run_subword_tamil started @ `date`"
  [ ! -z $tgtdir ] && [ -f $tgtdir/in-vocab-count.gz ] || \
  { echo "$0: run_subword: ERROR, tgtdir or in-vocab-count.gz is not ready"; exit 1; }
  dir=$tgtdir/subword; [ -d $dir ] || mkdir -p $dir
  gzip -cd $tgtdir/in-vocab-count.gz | awk '{s=sprintf("%d\t%s",$2,$1); print s;}' > $dir/traindata_list
  ngram_count_opts="-kndiscount1 -gt1min 0 -kndiscount2 -gt2min 1 -kndiscount3 -gt3min 3 -order 3 -unk -sort"
  lmdir=$dir/olm
  ngram_count_source_opts="-vocab $lmdir/vocab -text $lmdir/text.gz -lm $lmdir/lm3.13.gz"
  subword_vocab_opts="$tgtdir/vocab $dir/morfessor_model.bin $dir/olm" 
  source/test/lex-lm-update.sh --tgtdir $dir  \
  ${step01:+--subword_train true } \
  ${step02:+--subword_make_vocab true  --subword_vocab_opts "$subword_vocab_opts"} \
  ${step03:+--subword_make_map true} \
  ${step04:+--subword_make_text true --subword_text_opts "$tgtdir/text.gz $dir/word2subword_map.gz $dir/olm/text.gz"} \
  ${step05:+--subword_make_text true --subword_text_opts "$tgtdir/dev.txt $dir/word2subword_map.gz $dir/olm/dev.gz"} \
  ${step06:+--lexlm_train_srilms true --ngram_count_opts "$ngram_count_opts" --ngram_count_source_opts "$ngram_count_source_opts"}

  echo "$0: run_subword_tamil ended @ `date`"
fi

if $kws_run; then
  echo "$0: kws_run started @ `date`"
  KWSEval=/opt/tools/NIST/F4DE-3.2.0/KWSEval/tools/KWSEval/KWSEval.pl
  numArgs=$( echo "$kws_run_opts" |awk 'END{print NF;}')
  if [ $numArgs -eq 7 ]; then
    optNames="data lang kws_source sdir latdir lmwt kwlistId "
  else
    optNames="data lang kws_source sdir latdir lmwt kwlistId kws_morfessor_model"
  fi
  . source/register_options.sh "$optNames" "$kws_run_opts" || \
  { echo "$0: kws_run: ERROR, @register_options.sh"; exit 1; }
  [ ! -z $data ] && [ -f $data/segments ] || { echo "$0: kws_run: ERROR, data($data) or data/segments is not ready"; exit 1; }
  [ ! -z $lang ] && [ -f $lang/phones/word_boundary.int ] || \
  { echo "$0: kws_run: ERROR, lang($lang) or word_boundary.int is not ready"; exit 1; }
  [ ! -z $kws_source ] && [ -f $kws_source/ecf.xml ] || \
  { echo "$0: kws_run: ERROR, kws_source($kws_source) or file exf.xml is not ready"; exit 1;}
  nkwid=$(egrep 'kwid|termid' < $kws_source/kwlist.xml | wc -l)
  [ ! -z $nkwid ] && [ $nkwid -gt 0 ] || { echo "$0: kws_run: ERROR, kwid($nkwid) not found"; exit 1; }
  [ ! -z $sdir ] && [ -f $sdir/final.mdl ] || \
  { echo "$0: kws_run: ERROR, sdir($sdir) or final.mdl is not ready"; exit 1; }
  [ ! -z $latdir ] && [ -f $latdir/lat.1.gz ] || \
  { echo "$0: kws_run: ERROR, latdir($latdir) or lat.1.gz not ready"; exit 1; }
  [ ! -z $lmwt ] && [ $lmwt -gt 0 ] || \
  { echo "$0: kws_run: ERROR, lmwt($lmwt) is illegal"; exit 1; }
  [ -z $kwsdatadir ] && kwsdatadir=$(dirname $data)/kws.eval.$kwlistId.$nkwid
  [ -d $kwsdatadir ] || mkdir -p $kwsdatadir
  kwxml=$kws_source/kwlist.xml
  source/test/openkws_dump_kwlist.pl $kwxml  > $kwsdatadir/keywords.txt
  if [ ! -z $kws_morfessor_model ]; then
    echo "$0: kws_run: subword keyword searching started @ `date`"
    cut -f2- $kwsdatadir/keywords.txt | \
    source/test/rm_quote.pl | \
    awk '{for(i=1;i<=NF; i++){print $i;}}' | sort -u | gzip -c > $kwsdatadir/wordlist.gz
    gzip -cd $kwsdatadir/wordlist.gz | \
    morfessor-segment -l $kws_morfessor_model -e utf8  -| \
    perl -pe 's/[_\-]//g;'| gzip -c >  $kwsdatadir/sub_wordlist.gz  
    nWord=$(gzip -cd $kwsdatadir/wordlist.gz | wc -l)
    nSubword=$(gzip -cd $kwsdatadir/sub_wordlist.gz|wc -l)
    [ $nWord -eq $nSubword ] || \
    { echo "$0: kws_run: ERROR, words($nWord) and subwords($nSubword) are inequal"; exit 1; }
    paste <(gzip -cd $kwsdatadir/wordlist.gz) <(gzip -cd $kwsdatadir/sub_wordlist.gz) | \
    gzip -c > $kwsdatadir/word2subword_map.gz
    paste <(cut -f1 $kwsdatadir/keywords.txt) \
    <(cut -f2- $kwsdatadir/keywords.txt| source/test/rm_quote.pl | source/test/openkws_transform_text.pl "gzip -cd $kwsdatadir/word2subword_map.gz|") \
    > $kwsdatadir/sub_keywords.txt
    mv $kwsdatadir/keywords.txt $kwsdatadir/word_keywords.txt
    mv $kwsdatadir/sub_keywords.txt $kwsdatadir/keywords.txt
  fi
  head $kws_source/ecf.xml  | \
  perl -e '$str=<>; if($str =~ m/.*duration="(\S+)"\s+/) {
  print sprintf("%.2f\n", $1/2); }' > $kwsdatadir/duration
  
  acwt=$(echo $lmwt | perl -e '$lmwt=<>; $sLMWT=sprintf("%.4f",1/$lmwt); print $sLMWT;')
  kws_latscale_opts="--acoustic-scale=$acwt --lm-scale=1.0"
  indexdir=$latdir/index.${kwlistId}.$lmwt
  kwsoutputdir=$indexdir/output${kws_output_id:+.$kws_output_id}
  kws_index_opts="$data $lang $sdir $latdir $indexdir"
  kws_query_opts="$data $kwsdatadir/keywords.txt $lang $kwsdatadir"
  search_index_opts="$indexdir $kwsdatadir $kwsoutputdir $kwsdatadir/keywords.fsts"
  write_kwslist_opts="--Ntrue-scale=1.0 --flen=0.01 --normalize=true --duptime=0.6 --remove-dup=true --digits=5"
  write_kwslist_opts="$kwsdatadir $kwsoutputdir $write_kwslist_opts"
  kws_score_cmd="$KWSEval -e $kws_source/ecf.xml -r $kws_source/rttm -t $kwxml"
  $kws_index_done && step01=
  source/test/lex-lm-update.sh  \
  ${step01:+--kws_make_index true  --kws_index_opts "$kws_index_opts" --kws_latscale_opts "$kws_latscale_opts" } \
  ${step02:+--kws_make_query true --kws_query_opts "$kws_query_opts"} \
  ${step03:+--kws_search_index true --search_index_opts "$search_index_opts"}\
  ${step04:+--kws_write_kwslist true --write_kwslist_opts "$write_kwslist_opts"} \
  ${step05:+--kws_score true --kws_score_cmd "$kws_score_cmd" --kws_score_opts "$kwsoutputdir $kwsoutputdir/score"}
  echo "$0: kws_run ended @ `date`"
fi
if $make_index; then
  echo "$0: make_index started @ `date`"
  optNames="lmwt lat_index_num beam data lang sdir latdir dir"
  . source/register_options.sh "$optNames" "$make_index_opts" || \
  { echo "$0: make_index: register_options.sh: ERROR"; exit 1; }
  acwt=$(echo $lmwt | perl -e '$lmwt=<>; $sLMWT=sprintf("%.4f",1/$lmwt); print $sLMWT;')
  kws_latscale_opts="--acoustic-scale=$acwt --lm-scale=1.0"
  if [ ! -z $step01 ]; then
    echo "$0: make_index: step01: pruning lattice started @ `date`"
    [ -f $latdir/lat.$lat_index_num.gz ] || \
    { echo "$0: make_index: step01: ERROR, lat.$lat_index_num.gz expected"; exit 1; }
    [ -d $dir ] || mkdir -p $dir
    lattice-prune --acoustic-scale=$acwt --beam=$beam \
    "ark:gzip -cd $latdir/lat.$lat_index_num.gz|" "ark:|gzip -c > $dir/lat.$lat_index_num.gz" || \
    { echo "$0: make_index: step01: ERROR, lattice_prune failed "; exit 1; }
    echo "$0: make_index: step01: ended @ `date`"
  fi
  if [ ! -z $step02 ]; then
    echo "$0: make_index: making index started @ `date`"
    utter_id=$data/utter_id
    cat $data/segments | awk '{print $1;}' | \
    perl -e '$index = 1; while(<>) {chomp; print "$_ $index\n"; $index ++; }' > $utter_id
    cat $data/segments | awk '{printf ("%s %s\n", $1, $2);}'  > $data/utter_map
    wbound=$lang/phones/word_boundary.int
    lattice-align-words $wbound  $sdir/final.mdl "ark:gzip -cdf $dir/lat.$lat_index_num.gz|" ark:- | \
    lattice-scale $kws_latscale_opts  ark:- ark:- | \
    lattice-to-kws-index ark:$utter_id ark:- ark:- | \
    kws-index-union ark:- "ark:|gzip -c > $dir/index.$lat_index_num.gz"
    echo "$0: make_index: making index ended @ `date`"
  fi
  echo "$0: make_index ended @ `date`"
fi
