#!/bin/bash

. path.sh
. cmd.sh 

echo 
echo "## LOG: $0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <source-datadir> <tgtdir>
 [options]:
 [steps]:
 1: format data
 2: make link locally
 3: collect sph and trans flists
 4: prepare reco2channel data
 5: prepare kaldi data
 6: prepare wav.scp
 7: remove speech-noise utterance
 8: make spk2gender
 9: fix speaker id
 10: fix data dir 
 11: download dict
 12,13: prepare lexicon
 14: prepare lang
 15: prepare train and dev data 
 16: prepare data to train lm 
 17: train lm (with kaldi lm toolkits)
 18: build FST grammar in test_lang
 19: make mfcc features
 20: train gmm-hmm models

 [example]:
 $0 --steps 1 /data/users/hhx502/ldc-cts2016  fisher-english

END
}

if [ $# -ne 2 ] || [ -z $steps ]; then
  Usage && exit 1
fi

srcdatadir=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ ! -z $step01 ]; then
  echo "## LOG: step01, make datalink @ `date`"
  for x in fishereng-part1-ldc2004s13 fishereng-part2-ldc2005s13; do
    xdir=$srcdatadir/$x
    for y in $(find $xdir -maxdepth 2 -mindepth 2 -name "fe_03_p*"); do
      ydir=$xdir/$(basename $y)
      sdir=$(dirname $y)
      echo "## LOG: processing folder $ydir, sdir=$sdir"
      [ -d $ydir ] || mkdir $ydir
      for z in $(ls $sdir/); do
        tgtname=$z
        srcname=$sdir/$z
        {
          cd $ydir; 
          [ -f $z ] || ln -s $srcname $tgtname
          rm_tgtname=$(basename $sdir)
          [ "$rm_tgtname" == "$tgtname" ] && unlink $tgtname
        }
      done
    done
  done
  echo "## LOG: step01, done @ `date`"
fi
newdata=$tgtdir/data
newtgtdir=$tgtdir/data/local/data
[ -d $newtgtdir ] || mkdir -p $newtgtdir
linkdir=$newtgtdir/links
[ -d $linkdir ] || mkdir $linkdir
if [ ! -z $step02 ]; then
  echo "## LOG: step02, check data format @ `date`"
  for subdir in fe_03_p1_sph1  fe_03_p1_sph3  fe_03_p1_sph5  fe_03_p1_sph7 \
  fe_03_p2_sph1  fe_03_p2_sph3  fe_03_p2_sph5  fe_03_p2_sph7 fe_03_p1_sph2 \
  fe_03_p1_sph4  fe_03_p1_sph6  fe_03_p1_tran  fe_03_p2_sph2  fe_03_p2_sph4 \
  fe_03_p2_sph6  fe_03_p2_tran; do
  found_subdir=false
  for xdir in /data/users/hhx502/ldc-cts2016/fishereng-part1-ldc2004s13 \
             /data/users/hhx502/ldc-cts2016/fishereng-part2-ldc2005s13 \
             /data/users/hhx502/ldc-cts2016/LDC2004T19 \
             /data/users/hhx502/ldc-cts2016/LDC2005T19; do
    if [ -d $xdir/$subdir ]; then
      found_subdir=true
      [ -e $linkdir/$subdir ] || ln -s $xdir/$subdir $linkdir/$subdir
    else
      new_style_subdir=$(echo $subdir | sed s/fe_03_p2_sph/fisher_eng_tr_sp_d/)
      if [ -d $dir/$new_style_subdir ]; then
        found_subdir=true
        echo "## ERROR, step02, unexpected"
        # ln -s $dir/$new_style_subdir $newtgtdir/links/$subdir
      fi
    fi
  done
  if ! $found_subdir; then
    echo "$0: could not find the subdirectory $subdir in any of $*"
    exit 1;
  fi
done

  echo "## LOG: step02, done @ `date`"
fi

tmpdir=$newtgtdir
links=$linkdir

. ./path.sh # Needed for KALDI_ROOT

sph2pipe=$KALDI_ROOT/tools/sph2pipe_v2.5/sph2pipe

if [ ! -x $sph2pipe ]; then
   echo "Could not find (or execute) the sph2pipe program at $sph2pipe";
   exit 1;
fi
if [ ! -z $step03 ]; then
  echo "## LOG: step03, collet file list @ `date`"
  find $links/fe_03_p1_tran/data $links/fe_03_p2_tran/data -name '*.txt'  > $tmpdir/transcripts.flist

  for dir in fe_03_p{1,2}_sph{1,2,3,4,5,6,7}; do
    echo "## LOG: step03, $links/$dir" 1>&2
    find  -L $links/$dir/ -name '*.sph'
  done > $tmpdir/sph.flist

  n=`cat $tmpdir/transcripts.flist | wc -l`
  if [ $n -ne 11699 ]; then
    echo "Expected to find 11699 transcript files in the Fisher data, found $n"
    exit 1;
  fi
  n=`cat $tmpdir/sph.flist | wc -l`
  if [ $n -ne 11699 ]; then
    echo "Expected to find 11699 .sph files in the Fisher data, found $n"
    exit 1;
  fi
  echo "## LOG: done@ `date`"
fi

trainalldir=$newdata/train_all
[ -d $trainalldir ] || mkdir -p $trainalldir
if [ ! -z $step04 ]; then
  echo "## LOG: step04, prepare kaldi data @ `date`"

## fe_03_00004.sph
## Transcpribed at the LDC
#
#7.38 8.78 A: an- so the topic is 

  echo -n > $tmpdir/text.1 || exit 1;

  perl -e ' 
   use File::Basename;
   ($newdata, $tmpdir)=@ARGV;
   open(F, "<$tmpdir/transcripts.flist") || die "Opening list of transcripts";
   open(R, "|sort >$newdata/train_all/reco2file_and_channel") || die "Opening reco2file_and_channel";
   open(T, ">$tmpdir/text.1") || die "Opening text output";
   while (<F>) {
     $file = $_;
     m:([^/]+)\.txt: || die "Bad filename $_";
     $call_id = $1;
     print R "$call_id-A $call_id A\n";
     print R "$call_id-B $call_id B\n"; 
     open(I, "<$file") || die "Opening file $_";

     $line1 = <I>;
     $line1 =~ m/# (.+)\.sph/ || die "Bad first line $line1 in file $file";
     $call_id eq $1 || die "Mismatch call-id $call_id vs $1\n";
     while (<I>) {
       if (m/([0-9.]+)\s+([0-9.]+) ([AB]):\s*(\S.+\S|\S)\s*$/) {
         $start = sprintf("%06d", $1 * 100.0);
         $end = sprintf("%06d", $2 * 100.0);
         length($end) > 6 && die "Time too long $end in file $file";
         $side = $3; 
         $words = $4;
         $utt_id = "${call_id}-$side-$start-$end";
         print T "$utt_id $words\n" || die "Error writing to text file";
       }
     }
   }
   close(R); close(T) ' $newdata $tmpdir || exit 1;
  echo "## LOG: step04, done @ `date`"
fi
if [ ! -z $step05 ]; then
  echo "## LOG: step05, prepare kaldi data @ `date`"
   sort $tmpdir/text.1 | \
    perl -pe 's/\(\([^\(]+\)\)/\[speech-noise\]/g;' | \
    awk '{if (NF > 1){ print; }}' | \
    sed 's:\[laugh\]:[laughter]:g' | \
    sed 's:\[sigh\]:[noise]:g' | \
    sed 's:\[cough\]:[noise]:g' | \
    sed 's:\[sigh\]:[noise]:g' | \
    sed 's:\[mn\]:[noise]:g' | \
    sed 's:\[breath\]:[noise]:g' | \
    sed 's:\[lipsmack\]:[noise]:g' > $tmpdir/text.2
  cp $tmpdir/text.2 $trainalldir/text
  # create segments file and utt2spk file...
  ! cat $trainalldir/text | perl -ane 'm:([^-]+)-([AB])-(\S+): || die "Bad line $_;"; print "$1-$2-$3 $1-$2\n"; ' \
   > $trainalldir/utt2spk  \
     && echo "Error producing utt2spk file" && exit 1;

  cat $trainalldir/text | perl -ane 'm:((\S+-[AB])-(\d+)-(\d+))\s: || die; $utt = $1; $reco = $2; $s = sprintf("%.2f", 0.01*$3);
                 $e = sprintf("%.2f", 0.01*$4); print "$utt $reco $s $e\n"; ' > $trainalldir/segments

  utils/utt2spk_to_spk2utt.pl <$trainalldir/utt2spk > $trainalldir/spk2utt

  echo "## LOG: step05, done @ `date`"
fi

if [ ! -z $step06 ]; then
  echo "## LOG: step06, prepare wav.scp @ `date`"
  for f in `cat $tmpdir/sph.flist`; do
    # convert to absolute path
    readlink -e $f
  done > $tmpdir/sph_abs.flist
  cat $tmpdir/sph_abs.flist | perl -ane 'm:/([^/]+)\.sph$: || die "bad line $_; ";  print "$1 $_"; ' > $tmpdir/sph.scp
  cat $tmpdir/sph.scp | awk -v sph2pipe=$sph2pipe '{printf("%s-A %s -f wav -p -c 1 %s |\n", $1, sph2pipe, $2); 
    printf("%s-B %s -f wav -p -c 2 %s |\n", $1, sph2pipe, $2);}' | \
    sort -k1,1 -u  > $trainalldir/wav.scp || exit 1;

  echo "## LOG: step06, ended @ `date`"
fi
if [ ! -z $step07 ]; then
  echo "## LOG: step07, remove speech noise @ `date`"
  mv $trainalldir/text $trainalldir/text-has-speech-noise
  cat $trainalldir/text-has-speech-noise | \
  perl -e 'while(<>){chomp; if(/\[speech-noise\]/){ @A =split(/\s+/); if(@A<=3){next;}else{print "$_\n";} }else {print "$_\n";} }'\
  | \
  perl -ane 's/\[laughter\]/\<noise\>/g; s/\[noise\]/\<noise\>/g; s/\[speech\-noise\]/\<unk\>/g; print;' > $trainalldir/text
  echo "## LOG: step07, done @ `date`"
fi

if [ ! -z $step08 ]; then
  echo "## LOG: step08, make spk2gender @ `date`"
  cat $links/fe_03_p1_sph{1,2,3,4,5,6,7}/filetable.txt \
    $links/fe_03_p2_sph{1,2,3,4,5,6,7}/docs/filetable2.txt | \
  perl -ane 'm:^\S+ (\S+)\.sph ([fm])([fm]): || die "bad line $_;"; print "$1-A $2\n", "$1-B $3\n"; ' | \
   sort | uniq | utils/filter_scp.pl $trainalldir/spk2utt > $trainalldir/spk2gender

  if false && [ ! -s $trainalldir/spk2gender ]; then
    echo "It looks like our first try at getting the spk2gender info did not work."
    echo "(possibly older distribution?)  Trying something else."
    cat $links/fe_03_p1_tran/doc/fe_03_p1_filelist.tbl  $links/fe_03_p2_tran/doc/fe_03_p2_filelist.tbl  | \
       perl -ane 'm:fe_03_p[12]_sph\d\t(\d+)\t([mf])([mf]): || die "Bad line $_";
                print "fe_03_$1-A $2\n", "fe_03_$1-B $3\n"; ' | \
         sort | uniq | utils/filter_scp.pl data/train_all/spk2utt > data/train_all/spk2gender
   fi
  echo "## LOG: step08, ended @ `date`"
fi

if [ ! -z $step09 ]; then
  echo "## LOG: step09, fix speaker gender @ `date`"
  cat $links/fe_03_p{1,2}_tran/doc/*calldata.tbl > $tmpdir/combined-calldata.tbl
  local/fisher_fix_speakerid.pl $tmpdir/combined-calldata.tbl $trainalldir
  utils/utt2spk_to_spk2utt.pl $trainalldir/utt2spk.new > $trainalldir/spk2utt.new
  # patch files
  for f in spk2utt utt2spk text segments spk2gender; do
    cp $trainalldir/$f $trainalldir/$f.old || exit 1;
    cp $trainalldir/$f.new $trainalldir/$f || exit 1;
  done
  echo "## LOG: step09, done @ `date`"
fi
if [ ! -z $step10 ]; then
  echo "## LOG: step10, fix data @ `date`"
  utils/fix_data_dir.sh $trainalldir
  echo "## LOG: step10, done @ `date`"
fi
dictdir=$newdata/local/dict
[ -d $dictdir ] || mkdir -p $dictdir
if [ ! -z $step11 ]; then
  echo "## LOG: step11, prepare dict @ `date`"
  svn co  https://svn.code.sf.net/p/cmusphinx/code/trunk/cmudict  $dictdir/cmudict
  echo "## LOG: step11, ended @ `date`"
fi
if [ ! -z $step12 ]; then
  echo "## LOG: step12, prepare dict @ `date`"
  for w in SIL  "<sss>" "<oov>"; do echo "$w"; done > $dictdir/silence_phones.txt
  echo SIL > $dictdir/optional_silence.txt
  # For this setup we're discarding stress.
  cat $dictdir/cmudict/cmudict.0.7a.symbols | sed s/[0-9]//g | \
  tr '[A-Z]' '[a-z]' | perl -ane 's:\r::; print;' | sort | uniq > $dictdir/nonsilence_phones.txt
  # An extra question will be added by including the silence phones in one class.
  cat $dictdir/silence_phones.txt| awk '{printf("%s ", $1);} END{printf "\n";}' > $dictdir/extra_questions.txt || exit 1;
  grep -v ';;;' $dictdir/cmudict/cmudict.0.7a |  tr '[A-Z]' '[a-z]' | \
  perl -ane 'if(!m:^;;;:){ s:(\S+)\(\d+\) :$1 :; s:  : :; print; }' | \
  perl -ane '@A = split(" ", $_); for ($n = 1; $n<@A;$n++) { $A[$n] =~ s/[0-9]//g; } print join(" ", @A) . "\n";' | \
  sort | uniq > $dictdir/lexicon1_raw_nosil.txt || exit 1;
  # Add prons for laughter, noise, oov
  echo -e "<noise>\t<sss>\n<unk>\t<oov>"  | cat - $dictdir/lexicon1_raw_nosil.txt  > $dictdir/lexicon2_raw.txt || exit 1;
  # This is just for diagnostics:
  cat $trainalldir/text  | \
  awk '{for (n=2;n<=NF;n++){ count[$n]++; } } END { for(n in count) { print count[n], n; }}' | \
  sort -nr > $dictdir/word_counts
  cat $dictdir/word_counts | awk '{print $2}' > $dictdir/word_list
  utils/filter_scp.pl $dictdir/word_list $dictdir/lexicon2_raw.txt | \
  perl -ane 'chomp; @A = split(/\s+/); $w = shift @A; print "$w\t", join(" ", @A), "\n";' > $dictdir/lexicon3_expand.txt
  cat $dictdir/word_counts | awk '{print $2}' > $dictdir/word_list
  echo "## LOG: step12, done @ `date`"

fi

if [ ! -z $step13 ]; then
  echo "## LOG: step13, prepare lexicon @ `date`"
  cat $dictdir/lexicon2_raw.txt | \
  perl -e 'while(<STDIN>) { @A=split(/\s+/);; $w = shift @A; $pron{$w} = join(" ", @A); }
     ($w) = @ARGV;  open(W, "<$w") || die "Error opening word-counts from $w";
     while(<W>) { # reading in words we saw in training data..
       ($c, $w) = split;
       if (!defined $pron{$w}) { 
         @A = split("_", $w);
         if (@A > 1) {
           $this_pron = "";
           $pron_ok = 1;
           foreach $a (@A) { 
             if (defined($pron{$a})) { $this_pron = $this_pron . "$pron{$a} "; }
             else { $pron_ok = 0; print STDERR "Not handling word $w, count is $c\n"; last; } 
           }
           if ($pron_ok) { $new_pron{$w} = $this_pron;   }
         }
       }
     }
     foreach $w (keys %new_pron) { print "$w\t$new_pron{$w}\n"; }' \
   $dictdir/word_counts >> $dictdir/lexicon3_expand.txt || exit 1;
  cat $dictdir/lexicon3_expand.txt  \
   <( echo -e "mm\tm" ) > $dictdir/lexicon4_extra.txt
  cp $dictdir/lexicon4_extra.txt $dictdir/lexicon.txt
  rm $dictdir/lexiconp.txt 2>/dev/null; # can confuse later script if this exists.
  
  awk '{print $1}' $dictdir/lexicon.txt | \
  perl -e '($word_counts)=@ARGV;
   open(W, "<$word_counts")||die "opening word-counts $word_counts";
   while(<STDIN>) { chop; $seen{$_}=1; }
   while(<W>) {
     ($c,$w) = split;
     if (!defined $seen{$w}) { print; }
   } ' $dictdir/word_counts > $dictdir/oov_counts.txt
  echo "*Highest-count OOVs are:"
  head -n 20 $dictdir/oov_counts.txt
  utils/validate_dict_dir.pl $dictdir
  echo "## LOG: step13, done @ `date`"
fi

if [ ! -z $step14 ]; then
  echo "## LOG: step14, prepare lang @ `date`"
  utils/prepare_lang.sh $dictdir "<unk>" $newdata/local/lang $newdata/lang
  echo "## LOG: step14, done @ `date`"
fi
traindir=$newdata/train
devdir=$newdata/dev
devspk=50
if [ ! -z $step15 ]; then
  echo "## LOG: step15, prepare train, dev data @ `date`"
  mkdir -p $traindir $devdir
  awk '{print $1;}' $trainalldir/spk2utt | head -$devspk > $devdir/spklist
  awk '{print $1;}' $trainalldir/spk2utt | \
  perl -e '($spklist) = @ARGV; open(F, "<$spklist") or die "## ERROR, cannot open spklist\n";
    while(<F>){chomp; $vocab{$_}++;}; close F;
    while(<STDIN>) {chomp; if(not exists $vocab{$_}){print "$_\n";} }
  ' $devdir/spklist  > $traindir/spklist
  utils/subset_data_dir.sh --spk-list $devdir/spklist $trainalldir $devdir
  utils/subset_data_dir.sh --spk-list $traindir/spklist $trainalldir $traindir
  echo "## LOG: step15, done @ `date`"
fi

lmdir=$newdata/local/lm
[ -d $lmdir ] || mkdir -p $lmdir
if [ ! -z $step16 ]; then
  echo "## LOG: step16, prepare data to train lm @ `date`"
  lexicon=$newdata/local/dict/lexicon.txt
  heldout_sent=$(wc -l < $devdir/text)
  [ $heldout_sent -gt 0 ] || { echo "ERROR: step16, heldout_sent error"; exit 1; }
  echo $heldout_sent > $lmdir/heldout_sent
  cat $devdir/text $traindir/text > $lmdir/kaldi_text
  cleantext=$lmdir/text.no-oov
  cat $lmdir/kaldi_text | awk -v lex=$lexicon 'BEGIN{while((getline<lex) >0){ seen[$1]=1; } } 
  {for(n=1; n<=NF;n++) {  if (seen[$n]) { printf("%s ", $n); } else {printf("<unk> ");} } printf("\n");}' \
  > $cleantext || exit 1;
  
  cat $cleantext | awk '{for(n=2;n<=NF;n++) print $n; }' | sort | uniq -c | \
   sort -nr > $lmdir/word.counts || exit 1;
  
  # Get counts from acoustic training transcripts, and add  one-count
  # for each word in the lexicon (but not silence, we don't want it
  # in the LM-- we'll add it optionally later).
  cat $cleantext | awk '{for(n=2;n<=NF;n++) print $n; }' | \
  cat - <(grep -w -v '!SIL' $lexicon | awk '{print $1}') | \
  sort | uniq -c | sort -nr > $lmdir/unigram.counts || exit 1;
  
  # note: we probably won't really make use of <unk> as there aren't any OOVs
  cat $lmdir/unigram.counts  | awk '{print $2}' | get_word_map.pl "<s>" "</s>" "<unk>" > $lmdir/word_map \
  || exit 1;
 
  # note: ignore 1st field of train.txt, it's the utterance-id.
  cat $cleantext | awk -v wmap=$lmdir/word_map 'BEGIN{while((getline<wmap)>0)map[$1]=$2;}
  { for(n=2;n<=NF;n++) { printf map[$n]; if(n<NF){ printf " "; } else { print ""; }}}' | gzip -c >$lmdir/train.gz \
   || exit 1;
  rm $lmdir/kaldi_text $cleantext 2>/dev/null 
  echo "## LOG: step16, ended @ `date`"
fi

if [ ! -z $step17 ]; then
  echo "## LOG: step17, train lm,  `hostname`@`date`"
  source/egs/fisher-english/train_lm.sh --arpa --lmtype 3gram-mincount  $lmdir
  echo "## LOG: step17, done,  `hostname`@`date`"
fi
lang_test=$newdata/lang_test
lang=$newdata/lang
if [ ! -z $step18 ]; then
  echo "## LOG: step18, `hostname`@`date`"
  arpa_lm=$lmdir/3gram-mincount/lm_unpruned.gz
  [ ! -f $arpa_lm ] && echo No such file $arpa_lm && exit 1;
  mkdir -p $lang_test
  cp -r $lang/* $lang_test

  gunzip -c "$arpa_lm" | \
  arpa2fst --disambig-symbol=#0 \
           --read-symbol-table=$lang_test/words.txt - $lang_test/G.fst

  echo  "Checking how stochastic G is (the first of these numbers should be small):"
  fstisstochastic $lang_test/G.fst

  ## Check lexicon.
  ## just have a look and make sure it seems sane.
  echo "First few lines of lexicon FST:"
  fstprint   --isymbols=$lang/phones.txt --osymbols=$lang/words.txt $lang/L.fst  | head

  echo Performing further checks

  # Checking that G.fst is determinizable.
  fstdeterminize $lang_test/G.fst /dev/null || echo Error determinizing G.

  # Checking that L_disambig.fst is determinizable.
  fstdeterminize $lang_test/L_disambig.fst /dev/null || echo Error determinizing L.

# Checking that disambiguated lexicon times G is determinizable
# Note: we do this with fstdeterminizestar not fstdeterminize, as
# fstdeterminize was taking forever (presumbaly relates to a bug
# in this version of OpenFst that makes determinization slow for
# some case).
fsttablecompose $lang_test/L_disambig.fst $lang_test/G.fst | \
   fstdeterminizestar >/dev/null || echo Error

# Checking that LG is stochastic:
fsttablecompose $lang/L_disambig.fst $lang_test/G.fst | \
   fstisstochastic || echo "[log:] LG is not stochastic"

  echo "## LOG: step18, `hostname`@`date`"
fi
expdir=/local/hhx502/ldc-cts2016/fisher-english
expdata=$expdir/data
expexp=$expdir/exp
if [ ! -z $step19 ]; then
  echo "## LOG: step19, make mfcc features, `hostname`@ `date`"
  for x in train dev; do
    sdata=$newdata/$x
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc.sh --mfcc-config conf/mfcc.conf" \
    $sdata $sdata/mfcc $sdata/feat/mfcc || exit 1
  done 
  echo "## LOG: step19, done `hostname`@ `date`"
fi

if [ ! -z $step20 ]; then
  echo "## LOG: step20, train gmm-hmm, `hostname`@`date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd run.pl --nj 20 \
  --train-id b \
  --cmvn-opts "--norm-means=true" --state-num 5000 --pdf-num 75000 \
  --devdata $newdata/dev/mfcc \
  $newdata/train-sub/mfcc $newdata/lang_test $expexp || exit 1
  echo "## LOG: step20, done `hostname`@ `date`"
fi
alidir=$expexp/tri4b/ali-full-train
if [ ! -z $step21 ]; then
  echo "## LOG: step21, align full data, `hostname`@`date`"
  steps/align_fmllr.sh --cmd run.pl --nj 20 $traindir/mfcc $lang $expexp/tri4b $alidir || exit 1
  echo "## LOG: step21, done, `hostname`@`date`"
fi 
if [ ! -z $step22 ]; then
  echo "## LOG: step22, train gmm-hmm with full-train data, `hostname`@`date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7 --cmd run.pl --nj 20 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 10000 --pdf-num 300000 \
  --alidir $alidir \
  --devdata $newdata/dev/mfcc \
  $newdata/train/mfcc $newdata/lang_test $expexp || exit 1
  echo "## LOG: step22, done, `hostname`@`date`"
fi
