#!/bin/bash

echo
echo "$0 $@"
echo 

. path.sh || exit 1

# begin options
lmtype="3gram-mincount"
# end options

. parse_options.sh || exit 1
function Usage {
 cat<<END
 $0 [options] <vocab> <train_data> <dev_data> <tgtdir>
 [options]:
 --lmtype                            # value, "$lmtype"

 [examples]:
 $0 --lmtype 4gram-mincount  ~/w2016/kws2016/flp-grapheme-phone/data/local/dict/lexicon.txt \
 ~/w2016/kws2016/flp-grapheme-phone/data/train/fbank_pitch \
 ~/w2016/kws2016/flp-grapheme-phone/data/dev/fbank_pitch \
 ~/w2016/kws2016/flp-grapheme-phone/data/local/lm
  
END
}
if [ $# -ne 4 ]; then 
  Usage && exit 1
fi

vocab=$1
train_data=$2
dev_data=$3
tgtdir=$4

for xdir in $train_data $dev_data; do
  [ -e $xdir/text ] || { echo "## ERROR: text expected from $xdir"; exit 1;  }
done
[ -d $tgtdir ] || mkdir -p $tgtdir

heldout_sent=$(wc -l < $dev_data/text)
  [ $heldout_sent -gt 0 ] || { echo "ERROR: step16, heldout_sent error"; exit 1; }

echo $heldout_sent > $tgtdir/heldout_sent

cat $dev_data/text $train_data/text > $tgtdir/kaldi_text
cleantext=$tgtdir/text.no-oov
cat $tgtdir/kaldi_text | \
awk -v lex=$vocab 'BEGIN{while((getline<lex) >0){ seen[$1]=1; } } 
  {for(n=1; n<=NF;n++) {  if (seen[$n]) { printf("%s ", $n); } else {printf("<unk> ");} } printf("\n");}' \
  > $cleantext || exit 1;

cat $cleantext | awk '{for(n=2;n<=NF;n++) print $n; }' | sort | uniq -c | \
   sort -nr > $tgtdir/word.counts || exit 1;

# Get counts from acoustic training transcripts, and add  one-count
# for each word in the lexicon (but not silence, we don't want it
# in the LM-- we'll add it optionally later).
cat $cleantext | awk '{for(n=2;n<=NF;n++) print $n; }' | \
cat - <(grep -w -v '!SIL' $vocab | awk '{print $1}') | \
sort | uniq -c | sort -nr > $tgtdir/unigram.counts || exit 1;

# note: we probably won't really make use of <unk> as there aren't any OOVs
cat $tgtdir/unigram.counts  | \
awk '{print $2}' | \
get_word_map.pl "<s>" "</s>" "<unk>" > $tgtdir/word_map || exit 1;

cat $cleantext | \
awk -v wmap=$tgtdir/word_map 'BEGIN{while((getline<wmap)>0)map[$1]=$2;}
{ for(n=2;n<=NF;n++) { printf map[$n]; if(n<NF){ printf " "; } else { print ""; }}}' |\
gzip -c >$tgtdir/train.gz || exit 1;
rm $tgtdir/kaldi_text $cleantext 2>/dev/null 

source/egs/fisher-english/train_lm.sh --arpa --lmtype $lmtype  $tgtdir

