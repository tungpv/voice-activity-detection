#!/bin/bash

. path.sh
. cmd.sh 

# begin options
steps=
cmd=slurm.pl
nj=4
bnf_extractor=
# end options
echo 
echo LOG: $0 $@
echo 
. parse_options.sh || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

function Usage {
  cat<<END
 Usage: $(basename $0) [options] <source-wav-dir> <tgtdir>
 [options]:
 --steps                    # value, "$steps"
 --cmd                      # value, "$cmd"
 --nj                       # value, $nj
 --bnf-extractor            # value, "$bnf_extractor"
 [steps]:
 1: downsample to 8k 
 2: make kaldi data
 3: make fbank-pitch feature
 4: extract bnf 
 5: dump binary feature into text feature
 6: make 16k data
 7: make fbank for 16k data
 8: make bnf with wsj bnf extractor without pitch
 9: make wsj train_si284 fbank-pitch data
 10: make fbank-pitch for 16k data
 11: train wsj bnf-extractor with pitch
 12: extract sbnf from fbank+pitch

 [example]:
 $0  --steps 1,2,3,4,5 --bnf-extractor swbd/exp/sbnf-extractor/h2  /home/xht211/data/cmu_database/wav-16k txh-vc-01

END
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi
srcwavdir=$1
tgtdir=$2

data=$tgtdir/data/vc01
[ -d $data ] || mkdir -p $data
if [ ! -z $step01 ]; then
  echo "LOG: step01, make wavlist"
  find $srcwavdir -name "*.wav"  | \
  egrep  'bdl|clb|rms|slt' | \
  perl -pe 'chomp; $lab = $_; $lab =~ s/.*\///g;  $lab =~ s/\.wav//g;  $_ = "$lab /usr/bin/sox $_ -r 8000 -c 1 -b 16 -t wav - downsample |\n"; '  > $data/wav.scp
  echo "LOG: step01, done !"
fi
if [ ! -z $step02 ]; then
  echo "LOG: step02, make utt2spk"
  cat $data/wav.scp | \
  awk '{print $1;}' | \
  perl -pe 'chomp; $spk = $_; $spk =~ s/cmu_us_arctic_//g; $spk =~ s/_[^_]+$//g; $_ ="$_ $spk\n";' > $data/utt2spk
  utils/utt2spk_to_spk2utt.pl < $data/utt2spk > $data/spk2utt
  utils/fix_data_dir.sh $data
  echo "LOG: step02, done !"
fi
sdata=$data
data=$sdata/fbank-pitch
if [ ! -z $step03 ]; then
  echo "LOG: step03, make fbank-pitch started @ `date`"
  source/egs/swahili/make_feats.sh --nj 4 --fbank-pitch true  \
  --compute-fbank-feats "compute-fbank-feats --frame-shift=5" \
  --compute-kaldi-pitch-feats "compute-kaldi-pitch-feats --frame-shift=5" \
  $sdata $data $data/feat/fbank-pitch || exit 1
  echo "LOG: step03, done @ `date`"
fi
sbnf_data=$sdata/bnf
if [ ! -z $step04 ]; then
  echo "LOG: step04, make BNF started @ `date`"
  if [ -z $bnf_extractor ] || [ ! -f $bnf_extractor/final.nnet ]; then
    echo "ERROR, step03, bnf_extractor not ready !"; exit 1 
  fi
  source/egs/swahili/make_feats.sh --nj 4 --make-bnf true --bnf-extractor-dir $bnf_extractor \
  $data $sbnf_data $sdata/feat/sbnf
  echo "LOG: step04, ended @ `date`"
fi

if [ ! -z $step05 ]; then
  echo "LOG: step05, dump bnf into text format"
  copy-feats scp:$sbnf_data/feats.scp  "ark,t:|gzip >$sbnf_data/feats-text.gz"
  echo "LOG: step05, ended @ `date`"
fi
data16k=$tgtdir/data/vc01-16k
if [ ! -z $step06 ]; then
  echo "LOG: step06, make 16k wav.scp"
  [ -d $data16k ] || mkdir -p $data16k
  cat $data/wav.scp |  awk '{print $1, " ", $3;}' > $data16k/wav.scp
  cp $data/{spk2utt,utt2spk} $data16k/ 
  echo "LOG: step06, ended @ `date`"
fi
fbank_data16k=$data16k/fbank
if [ ! -z $step07 ]; then
  echo "LOG: step07, make fbank started @ `date`"
  [ -d $fbank_data16k ] || mkdir -p $fbank_data16k
  cp $data16k/* $fbank_data16k/
  steps/make_fbank.sh --cmd "$cmd" --nj 40 --fbank-config wsj/conf/fbank.conf  \
  --compute-fbank-feats "compute-fbank-feats --frame-shift=5" \
  $fbank_data16k $data16k/feat/fbank/log $data16k/feat/fbank/data || exit 1
  steps/compute_cmvn_stats.sh $fbank_data16k $data16k/feat/fbank/log $data16k/feat/fbank/data || exit 1
  utils/fix_data_dir.sh $fbank_data16k
  echo "LOG: step07, done @ `date`"
fi
function check_sbnf_extractor {
  if [ -z $bnf_extractor ] || [ ! -f $bnf_extractor/final.nnet ]; then
    echo "ERROR, step03, bnf_extractor not ready !"; exit 1 
  fi
  echo "LOG: bnf_extractor checking done"
}
if [ ! -z $step08 ]; then
  echo "LOG: step08, make sbnf for 16 data started @ `date`"
  check_sbnf_extractor || exit 1
  sbnf_data=$data16k/bnf
  source/egs/swahili/make_feats.sh --nj 4 --make-bnf true --bnf-extractor-dir $bnf_extractor \
  $fbank_data16k $sbnf_data $data16k/feat/sbnf
  copy-feats scp:$sbnf_data/feats.scp  "ark,t:|gzip >$sbnf_data/feats-text.gz"
  echo "LOG: step08, done @ `date`"
fi

if [ ! -z $step09 ]; then
  echo "LOG: step09, make fbank-pitch for wsj train_si284  started @ `date`"
  sdata=wsj/data/train_si284
  data=$sdata/fbank-pitch
  source/egs/swahili/make_feats.sh --nj 40 --fbank-pitch true  \
  --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config wsj/conf/fbank.conf --pitch-config wsj/conf/pitch.conf" \
  --compute-fbank-feats "compute-fbank-feats --frame-shift=10" \
  --compute-kaldi-pitch-feats "compute-kaldi-pitch-feats --frame-shift=10" \
  $sdata $data $data/feat/fbank-pitch || exit 1
  echo "LOG: step09, done @ `date`"
fi
fbank_pitch16k=$data16k/fbank-pitch
if [ ! -z $step10 ]; then
  echo "LOG: step10, make fbank-pitch for vc01-16k data started @ `date`"
  sdata=$data16k
  data=$fbank_pitch16k
  source/egs/swahili/make_feats.sh --nj 40 --fbank-pitch true  \
  --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config wsj/conf/fbank.conf --pitch-config wsj/conf/pitch.conf" \
  --compute-fbank-feats "compute-fbank-feats --frame-shift=5" \
  --compute-kaldi-pitch-feats "compute-kaldi-pitch-feats --frame-shift=5" \
  $sdata $data $data/feat/fbank-pitch || exit 1
  echo "LOG: step10, done @ `date`"
fi
if [ ! -z $step11 ]; then
  echo "LOG: step11, train sbnf-extractor started @ `date`"
  tgtdir=wsj/exp;  [ -d $tgtdir/log ] || mkdir -p $tgtdir/log; 
  nohup source/egs/swahili/run-hier-bnf.sh  --cmd "run.pl" --steps 1,2,3,4 --validating-rate 0.05 \
  wsj/data/train_si284/fbank-pitch wsj/data/lang_test_tg wsj/exp/tri4a/ali_train_si284 wsj/exp/sbnf-extractor-pitch > $tgtdir/log/sbnf-extractor-pitch.log 2>&1 & 
  echo "LOG: step11, done @ `date`"
fi
if [ ! -z $step12 ]; then
  echo "LOG: step12, make sbnf for 16 data (fbank+pitch) started @ `date`"
  check_sbnf_extractor || exit 1
  sbnf_data=$data16k/bnf-frm-fbank-pitch
  source/egs/swahili/make_feats.sh --nj 4 --make-bnf true --bnf-extractor-dir $bnf_extractor \
  $fbank_pitch16k $sbnf_data $data16k/feat/sbnf-frm-fbank-pitch
  copy-feats scp:$sbnf_data/feats.scp  "ark,t:|gzip >$sbnf_data/feats-text.gz"
  echo "LOG: step12, done @ `date`"
fi
