#!/usr/bin/perl -w
use strict;
use utf8;
use open qw (:std :utf8);

print STDERR "## LOG ($0): stdin expected\n";
my %vocab = ();
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  for(my $i = 0; $i < @A; $i ++) {
    my $word = $A[$i];
    $vocab{$word} ++;
  }
}
print STDERR "## LOG ($0): stdin ended\n";

foreach my $word (sort { $vocab{$b}<=>$vocab{$a}} keys%vocab) {
  printf("%-20s\t%-7d\n", $word, $vocab{$word});
}
