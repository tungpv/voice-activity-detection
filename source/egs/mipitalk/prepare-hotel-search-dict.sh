#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
# end options

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 $0 --steps 1 ../sge/name-entity/dict/singapore/hotel/sg-hotel-utterance-cn-sgemented.txt  ../sge/name-entity/dict/singapore/hotel/test 

EOF
}

if [ $# -ne 2 ]; then
  Example && exit 1
fi

text=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir
steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  cat $text | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; 
    @A = split(/\s+/); 
    for($i = 0; $i < @A; $i ++){ $word = $A[$i]; print "$word\n";  }'  | \
  sort -u > $tgtdir/hotel-kw-dict.txt
  [ -f $tgtdir/hotel-kw-dict-edit.txt ] || cp $tgtdir/hotel-kw-dict.txt $tgtdir/hotel-kw-dict-edit.txt 
  echo "## LOG (step01, $0): done with '$tgtdir/hotel-kw-dict.txt'"
fi
if [ ! -z $step02 ]; then
  source/egs/mipitalk/label-utterance.py --word-list-of-text $tgtdir/word-list.txt  --text ../sge/name-entity/dict/singapore/hotel/transfer-hotel-utterance.txt
  [ -e $tgtdir/word-label-dict.txt ] || cp $tgtdir/word-list.txt $tgtdir/word-label-dict.txt
  cat $tgtdir/word-label-dict.txt | \
  perl -e 'use utf8; use open qw(:std :utf8); while(<STDIN>) { chomp; @A=split(/\s+/); if (@A < 2){ next; }  print "$A[1]\n"; }' | \
  sort -u > $tgtdir/label.txt
  echo "## LOG (step02, $0): done & check '$tgtdir'"
fi

if [ ! -z $step03 ]; then
  source/egs/mipitalk/label-utterance.py --word-dict $tgtdir/word-label-dict.txt \
  --unit-dict $tgtdir/../unit-dict.txt \
  --unit-to-number-label-dict $tgtdir/../unit-label-dict.txt \
  --approx-label-dict $tgtdir/../approx-unit-dict.txt \
  --text $tgtdir/../transfer-hotel-utterance.txt \
  --label-text $tgtdir/labeled-utterance.txt
  echo "## LOG (step03, $0): done & check '$tgtdir'"
fi
