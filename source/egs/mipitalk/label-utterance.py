#!/usr/bin/env python 

# -*- coding: utf-8 -*-
# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu

import argparse
import sys
import os
import re
import numpy as np
import logging

sys.path.insert(0, 'steps')
import libs.common as common_lib
sys.path.insert(0,'source/egs')

import python_libs.mipidict as mipidict_lib
import python_libs.mipinumber as mipinumber_lib
import python_libs.mipiconverttext as mipitext_lib

logger = logging.getLogger('libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting check-and-edit-utterance-and-kw-dict')

def get_args():
    parser = argparse.ArgumentParser(description='Arguments parser')
    parser.add_argument('--debug', dest='debug', action='store_true')
    parser.add_argument('--word-dict', dest='word_dict', type=str,  help='word dict to transfer or label utterance')
    parser.add_argument('--text', dest='text',  type=str, help='formatted text for checking with kw_dict_file')
    parser.add_argument('--label-text', type=str, dest='label_text', help='labelled text from dict')
    parser.add_argument('--transfer-text', type=str, dest='transfer_text', help='transfer text with transfer dict')
    parser.add_argument('--word-list-of-text', type=str, dest='word_list_of_text', help='dump word list of the text')
    parser.add_argument('--unit-dict', type=str, dest='unit_dict', help='unit dict to be used to label number')
    parser.add_argument('--unit-to-number-label-dict', type=str, dest='unit_to_number_label_dict', help='unit label to number label mapping')
    parser.add_argument('--approx-label-dict', type=str, dest='approx_label_dict', help='dict indicating numbers that have particular labels should be approximated')
    return parser.parse_args()
def CheckLogger(args):
    if args.debug:
        ''' does not work '''
        logger.setLevel(logging.DEBUG)
        handler.setLevel(logging.DEBUG)
    logger.debug('logger now is at DEBUG level')

def LoadWordDict(args):
    wordDict = mipidict_lib.MipiDictVege(True)
    if args.word_dict:
        wordDict.LoadDict(args.word_dict)
    return wordDict

def LoadUnitDict(args):
    unitDict = mipidict_lib.MipiDictVege(True)
    if args.unit_dict:
        unitDict.LoadDict(args.unit_dict)
    return unitDict

def LoadUnitToNumberLabelDict(args):
    unit2numDict = mipidict_lib.MipiDictVege(True)
    if args.unit_to_number_label_dict:
        unit2numDict.LoadDict(args.unit_to_number_label_dict)
    return unit2numDict
 
def LoadApproxDict(args):
    approxDict = mipidict_lib.MipiDictVege(True)
    if args.approx_label_dict:
        approxDict.LoadDict(args.approx_label_dict)
    return approxDict

def main():
    args = get_args()
    CheckLogger(args)
    wordDict = LoadWordDict(args)
    unitDict = LoadUnitDict(args)
    unit2numDict = LoadUnitToNumberLabelDict(args)
    approxDict = LoadApproxDict(args)
    mipiNumber = mipinumber_lib.MipiNumber(unitDict, unit2numDict, approxDict)
    if args.text:
        mipiText = mipitext_lib.MipiConvertText(wordDict, mipiNumber, True, False)
        if args.word_list_of_text:
            mipiText.DumpWordList(args.text, args.word_list_of_text)
        if args.transfer_text:
            mipiText.TransferText(args.text, args.transfer_text)
        if args.label_text:
            mipiText.LabelText(args.text, args.label_text)
                        

if __name__ == "__main__":
    main()
