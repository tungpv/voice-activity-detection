#!/bin/bash

. path.sh
. cmd.sh


# begin sub
cmd='slurm.pl --quiet'
nj=120
steps=
# end sub

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 [Example]: $0 --steps 1   \
   /home2/wjc505/mipitalk_data/audios \
  /home2/wjc505/mipitalk_data/mobile-dat-sept07

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 2 ]; then
  Example && exit 1
fi

srcdata=$1
tgtdata=$2
[ -d $tgtdata ] || mkdir -p $tgtdata
if [ ! -z $step01 ]; then
  echo "## LOG (step01): normalize data"
  cat $srcdata/text | \
  source/egs/mipitalk/normalize-mobile-data/normalize-text.pl $tgtdata
  echo "## LOG (step01): done & check '$tgtdata'"
fi
# make a big hybrid lexicon
dictdir=update-sept-19-on-lexicon/data/local/dict
[ -d $dictdir ] || mkdir -p $dictdir
if [ ! -z $step02 ]; then
  grep -v '_sge' ../seame-sept-18-2017/data/local/dict/lexicon.txt | \
  cat - <(cat ../acumen/data/local/dict/lexicon.txt| source/egs/seame-sept-18-2017/add-or-remove-lang-identifier-to-phone.pl --add-identifier='_eng') | \
  source/egs/ted-libri-en/merge-dict.pl  ./update-april-03-2017-with-pruned-lexicon/data/dict-sweet-pie/lexicon.txt  > $dictdir/lexicon.txt
  cp ./update-april-03-2017-with-pruned-lexicon/data/dict-sweet-pie/{nonsilence_phones.txt,silence_phones.txt,optional_silence.txt,extra_questions.txt} $dictdir
  utils/validate_dict_dir.pl $dictdir
  echo "## LOG (step02): done with '$dictdir'"
fi
dictdir=update-sept-20-on-lexicon/data/local/dict
[ -d $dictdir ] || mkdir -p $dictdir
if [ ! -z $step03 ]; then
  cat ../sge/name-entity/dict/singapore/entity/asr-lexicon-it2.txt \
   ../sge/name-entity/dict/singapore/poi/poi-asr-lexicon-it2.txt \
   ../sge/name-entity/dict/singapore/mrt-food-hotel-road/asr-lexicon.txt | \
  source/egs/seame-sept-18-2017/add-or-remove-lang-identifier-to-phone.pl --lower-word --upper-phone --add-identifier='_eng' | \
  source/egs/ted-libri-en/merge-dict.pl update-sept-19-on-lexicon/data/local/dict/lexicon.txt  > $dictdir/lexicon.txt
  cp update-sept-19-on-lexicon/data/local/dict/{nonsilence_phones.txt,silence_phones.txt,optional_silence.txt,extra_questions.txt} $dictdir
  utils/validate_dict_dir.pl $dictdir
fi
tgtdir=/home2/wjc505/mipitalk_data/mobile-data-sept-20
if [ ! -z $step04 ]; then
  [ -d $tgtdir ] || mkdir -p $tgtdir
  PYTHONIOENCODING=utf-8  source/egs/acumen/crack-oov-word.py --normalize-mipitalk-mobile-data \
  --word-lexicon-file ./update-sept-20-on-lexicon/data/local/dict/lexicon.txt \
  --text-file  /home2/wjc505/mipitalk_data/mobile-dat-sept07/text --tgtdir $tgtdir 
  echo "## LOG (step04): check '$tgtdir'"
fi
if [ ! -z $step05 ]; then
  cat /home2/wjc505/mipitalk_data/mobile-data-sept-20/oov-word-count.txt | \
  source/egs/sge2017/prepare-manual-data-170223/make-word-transfer-dict.pl  > $tgtdir/word-transfer-dict-it2.txt
fi
if [ ! -z $step06 ]; then
  PYTHONIOENCODING=utf-8  source/egs/acumen/crack-oov-word.py --transfer-text \
  --word-lexicon-file ./update-sept-20-on-lexicon/data/local/dict/lexicon.txt \
  --transfer-dict-file $tgtdir/word-transfer-dict.txt \
  --text-file  $tgtdir/text --tgtdir $tgtdir 
  echo "## (step06): done & check '$tgtdir'"
fi
if [ ! -z $step07 ]; then
  cat $tgtdir/oov-word-count-to-be-labeled.txt | grep -vP '^[\d]+' > $tgtdir/oov-word-count-to-be-human-labeled.txt
  head -120 $tgtdir/oov-word-count-to-be-human-labeled.txt | cut -d' ' -f1 > $tgtdir/oov-word-count-to-be-human-labeled-120.txt
  cat $tgtdir/oov-word-count-to-be-labeled.txt | grep -P '^[\d]+' | cut -d' ' -f1 > $tgtdir/oov-number-wort-list.txt
  echo "## LOG (step07): done & check '$tgtdir/oov-word-count-to-be-human-labeled-120.txt' & '$tgtdir/oov-number-wort-list.txt'"
fi
if [ ! -z $step07 ]; then
  PYTHONIOENCODING=utf-8  source/egs/acumen/crack-oov-word.py --label-pronunciation-for-number-word \
  --word-lexicon-file ./update-sept-20-on-lexicon/data/local/dict/lexicon.txt \
  --text-file  $tgtdir/oov-number-wort-list.txt --tgtdir $tgtdir 
fi

if [ ! -z $step08 ]; then
  PYTHONIOENCODING=utf-8  source/egs/acumen/crack-oov-word.py --transfer-text \
 --word-lexicon-file ./update-sept-20-on-lexicon/data/local/dict/lexicon.txt \
 --text-file /home2/wjc505/mipitalk_data/mobile-data-sept-20/text-transfered  \
 --tgtdir /home2/wjc505/mipitalk_data/mobile-data-sept-29 
fi
dictdir=update-sept-20-on-lexicon/data/local/dict-align
[ -d $dictdir ] || mkdir -p $dictdir
if [ ! -z $step09 ]; then
  cp ./update-sept-20-on-lexicon/data/local/dict-modified/* $dictdir/
  cat /home2/wjc505/mipitalk_data/mobile-data-sept-29/arabic-lexicon.txt | \
  perl -ane 'use utf8; use open qw(:std :utf8); s:\[[^\[]+\]::g; print;' | \
  cat - ./update-sept-20-on-lexicon/data/local/dict-modified/lexicon.txt | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; @A = split(/\s+/); $word = shift @A;
    $s = join(" ", @A); print "$word\t$s\n";' | sort -u > $dictdir/lexicon.txt
  utils/validate_dict_dir.pl $dictdir
  echo "## LOG (step09): done with '$dictdir'"
fi
lang=update-sept-20-on-lexicon/data/lang-align
if [ ! -z $step10 ]; then
  utils/prepare_lang.sh $dictdir "<unk>" $lang/tmp $lang
fi
srcdata=update-sept-20-on-lexicon/data/mipimobile-data/01
[ -d $srcdata ] || mkdir -p $srcdata
if [ ! -z $step20 ]; then
  cp /home2/wjc505/mipitalk_data/audios/*  $srcdata/
  cat /home2/wjc505/mipitalk_data/audios/segments | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; @A=split(/\s+/); print "$A[1] $A[1] $A[2] $A[3]\n"; ' > $srcdata/segments
  cp /home2/wjc505/mipitalk_data/mobile-data-sept-29/text $srcdata/
  utils/fix_data_dir.sh $srcdata
  for sdata in $srcdata; do
    data=$sdata/mfcc-hires feat=$sdata/feat/mfcc-hires/data log=$sdata/feat/mfcc-hires/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    echo "## LOG (step20): done with feat '$data'"
  done
fi
ivector_extractor=./update-april-03-2017-with-pruned-lexicon/exp/tdnn/ivector-extractor
ivectors=./update-april-03-2017-with-pruned-lexicon/exp/tdnn/ivectors-mipimobile-data-01
if [ ! -z $step21 ]; then
  echo "## LOG (step21): ivector extraction started @ `date`"
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" \
   --nj 40 $srcdata/mfcc-hires $ivector_extractor $ivectors || exit 1
  echo "## LOG (step21): ivector extraction done @ `date`"  
fi
srcdir=./update-april-03-2017-with-pruned-lexicon/exp/tdnn/chain
alidir=$srcdir/ali_mipimobile-data-01
if [ ! -z $step22 ]; then
  echo "## LOG (step22): do alignment @ `date`"
  steps/nnet3/align.sh --cmd "$cmd" --nj 20 \
  --online-ivector-dir $ivectors\
  $srcdata/mfcc-hires $lang  $srcdir $alidir
  echo "## LOG (step22): done @ `date`"
  echo 
fi


# get phone alignment and word pronunciation
dir=$alidir/get_prons
[ -d $dir ] || mkdir -p $dir
if [ ! -z $step30 ]; then
  echo "## LOG (step30): get_prons started @ `date`"
  nj=$(cat $alidir/num_jobs)
  sdata=$srcdata/mfcc-hires/split${nj}utt
  mdl=$alidir/final.mdl
  oov=$(cat $lang/oov.int)
  $cmd JOB=1:$nj $dir/log/nbest_to_prons.JOB.log \
  linear-to-nbest "ark:gunzip -c $alidir/ali.JOB.gz|" \
  "ark:sym2int.pl --map-oov $oov -f 2- $lang/words.txt <$sdata/JOB/text |" \
   '' '' ark:- \| lattice-align-words $lang/phones/word_boundary.int $mdl ark:- ark:- \| \
  nbest-to-prons $mdl ark:- "|gzip -c >$dir/prons.JOB.gz" || exit 1;
  echo "## LOG (step30): done with get_prons in '$dir' @ `date`"
fi
if [ ! -z $step31 ]; then
  nj=$(cat $alidir/num_jobs)
  for x in $(seq 1 $nj); do
    gzip -cd $dir/prons.$x.gz
  done  | \
  awk '{$2=""; $3=""; print;}' | \
  utils/int2sym.pl -f 2 $lang/words.txt | \
  grep -v '<eps>' | \
  utils/int2sym.pl -f 3- $lang/phones.txt  > $dir/word-pron.txt
   echo "## LOG (step31): done with '$dir/word-pron.txt'"
fi
if [ ! -z $step32 ]; then
  cat $dir/word-pron.txt | \
  source/egs/mipitalk/transfer-text.pl "cat /home2/wjc505/mipitalk_data/mobile-data-sept-29/arabic-lexicon.txt ./update-april-03-2017-with-pruned-lexicon/exp/tdnn/chain/ali_mipimobile-data-01/get_prons/oov-dict.txt|"  \
  $srcdata/mfcc-hires/text $dir
  echo "## LOG (step32): done with '$dir'"
fi

if [ ! -z $step40 ]; then
   source/egs/sge2017/convert-human-lexicon-to-asr-lexicon-with-phone-mapping-list.py --debug --normalize\
  --oov-phone-map-file ../sge/name-entity/dict/singapore/mipimobile-25h-oct-12/oov-phone-map.txt \
  ../sge/name-entity/dict/singapore/mrt/sge-phone-mapping-it3.txt  ../sge/name-entity/dict/singapore/mipimobile-25h-oct-12/human-lexicon.txt  ../sge/name-entity/dict/singapore/mipimobile-25h-oct-12/asr-lexicon-it01.txt
fi
if [ ! -z $step41 ]; then
  source/egs/sge2017/convert-human-lexicon-to-asr-lexicon-with-phone-mapping-list.py --debug --normalize\
  --oov-phone-map-file  ../sge/name-entity/dict/singapore/mipimobile-25h-oct-12/oov-phone-map-ifah.txt \
  ../sge/name-entity/dict/singapore/mrt/sge-phone-mapping-it3.txt ../sge/name-entity/dict/singapore/mipimobile-25h-oct-12/to-be-transcribed-by-human-part01.csv ../sge/name-entity/dict/singapore/mipimobile-25h-oct-12/asr-lexicon-ifah-it01.txt 
 echo "## LOG (step41): done with '../sge/name-entity/dict/singapore/mipimobile-25h-oct-12/asr-lexicon-ifah-it01.txt'"
fi
if [ ! -z $step42 ]; then
  source/egs/sge2017/convert-human-lexicon-to-asr-lexicon-with-phone-mapping-list.py --debug --normalize \
  --oov-phone-map-file ../sge/name-entity/dict/singapore/mipimobile-25h-oct-12/oov-phone-map-van.txt \
../sge/name-entity/dict/singapore/mrt/sge-phone-mapping-it3.txt  ../sge/name-entity/dict/singapore/mipimobile-25h-oct-12/to-be-transcribed-by-human-part02.txt  ../sge/name-entity/dict/singapore/mipimobile-25h-oct-12/asr-lexicon-van-part02-it01.txt
fi
