#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

# begin sub
sub RemovePunctuation {
  my ($s) = @_;
  $$s =~ s:[\:,;?!]: :g;
  $$s =~ s:，|。: :g;
  my @A = split(/\s+/, $$s);
  for (my $i = 0; $i < scalar @A; $i ++) {
    my $w = \$A[$i];
    $$w =~ s/(\S+)\.$/$1/g;
  }
  $$s = join(" ", @A);
}
sub CountWord {
  my ($utterance, $vocab) = @_;
  my @A = split(/\s+/, $utterance);
  for(my $i = 0; $i < scalar @A; $i ++) {
    my $w = $A[$i];
    $$vocab{$w} ++;
  }
}
sub IsChineseContained {
  my ($word) = @_;
  return 1 if($word =~ m:\p{Han}+:g);
  return 0;
}
# end sub
my $numArgs = scalar @ARGV;
if($numArgs != 1) {
  die "\n\nExample: cat text |$0 <tgtdir>\n\n";
}
my ($tgtdir) = @ARGV;
`[ -d $tgtdir ] || mkdir -p $tgtdir`;
open(T, ">$tgtdir/text") or die;
open(CV, " > $tgtdir/chinese-word-map.txt") or die;
open(EV, "> $tgtdir/english-word-map.txt") or die;
my %vocab = ();
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m:^(\S+)\s+(.*)$:g or next;
  my ($label, $utterance) = ($1, $2);
  $utterance = lc $utterance;
  RemovePunctuation(\$utterance);
  CountWord($utterance, \%vocab);
  print T "$label $utterance\n";
}
close T;
print STDERR "## LOG ($0): stdin ended\n";
for my $w (keys %vocab){
  my $line = sprintf("%-40s\t%-40s", $w, $w);
  if (IsChineseContained($w)) {
    print CV "$line\n";
  } else {
    print EV "$line\n";
  }
}
close CV;
close EV;
