#!/usr/bin/env python 

# -*- coding: utf-8 -*-
# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu

import argparse
import sys
import os
import re
import numpy as np
import logging
from bs4 import BeautifulSoup

sys.path.insert(0, 'steps')
import libs.common as common_lib
sys.path.insert(0,'source/egs')

import python_libs.mipidict as mipidict_lib
import python_libs.mipinumber as mipinumber_lib
import python_libs.mipiconverttext as mipitext_lib

logger = logging.getLogger('libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting dump-hotel-xml')

def get_args():
    parser = argparse.ArgumentParser(description='Arguments_parser')
    parser.add_argument('xml_file', type=str, help='xml file to be dumped')
    parser.add_argument('tgtdir', type=str, help='target dir to dump file')
    return parser.parse_args()
def InitDict(city, hotel):
    universalDict = dict()
    universalDict[city] = dict()
    cityDict = universalDict[city]
    cityDict[hotel] = dict()
    return universalDict
def GetHotelDict(universalDict, city, hotel):
    if city not in universalDict:
        raise Exception('city {0} is not registered'.format(city))
    cityDict = universalDict[city]
    if hotel not in cityDict:
        raise Exception('hotel {0} is not registered'.format(hotel))
    return cityDict[hotel]
def DumpHotelDict(hotelDict, args):
    idDict = hotelDict['id']
    infoOverall = str()
    infoStar = str()
    infoRate = str()
    infoName = str()
    infoPrice = str()
    infoAddress = str()
    for hotelId in idDict:
        hotelInfo = idDict[hotelId]
        infoOverall += 'Hotel: {0}\nStar: {1}\nRate: {2}\nReference Price (RMB): {3}\nAddress: {4}\n'.format(hotelInfo['name'], hotelInfo['star'], hotelInfo['rate'], hotelInfo['price'], hotelInfo['address'])
        infoName += '{0}\t{1}\n'.format(hotelId, hotelInfo['name'])
        infoStar += '{0}\t{1}\n'.format(hotelId, hotelInfo['star'])
        infoRate += '{0}\t{1}\n'.format(hotelId, hotelInfo['rate'])
        infoPrice += '{0}\t{1}\n'.format(hotelId, hotelInfo['price'])
        infoAddress += '{0}\t{1}\n'.format(hotelId, hotelInfo['address'])
    ostream = open('{0}/hotel-overall.txt'.format(args.tgtdir), 'w')
    ostream.write('{0}'.format(infoOverall))
    ostream.close()
    ostream = open('{0}/hotel-name.txt'.format(args.tgtdir), 'w')
    ostream.write('{0}'.format(infoName))
    ostream.close()
    ostream = open('{0}/hotel-star.txt'.format(args.tgtdir), 'w')
    ostream.write('{0}'.format(infoStar))
    ostream.close()
    ostream = open('{0}/hotel-rate.txt'.format(args.tgtdir), 'w')
    ostream.write('{0}'.format(infoRate))
    ostream.close()
    ostream = open('{0}/hotel-price.txt'.format(args.tgtdir), 'w')
    ostream.write('{0}'.format(infoPrice))
    ostream.close()
    ostream = open('{0}/hotel-address.txt'.format(args.tgtdir), 'w')
    ostream.write('{0}'.format(infoAddress))
    ostream.close()
    
def InsertHotel(hotelDict, hotelId, hotelName, hotelStar, hotelRate, hotelPrice, hotelAddress):
    sId = 'id'
    if sId not in hotelDict:
        hotelDict['id'] = dict()
    idDict = hotelDict['id']
    if hotelId in idDict:
        raise Exception('duplicated hotel id {0}'.format(hotelId))
    idDict[hotelId] = dict()
    infoDict = idDict[hotelId]
    infoDict['name'] = hotelName
    infoDict['star'] = hotelStar
    infoDict['rate'] = hotelRate
    infoDict['price'] = hotelPrice
    infoDict['address'] = hotelAddress
    
def Dump(args):
    universalDict = InitDict('singapore', 'hotel')
    hotelDict = GetHotelDict(universalDict, 'singapore', 'hotel')
    with open(args.xml_file, 'r') as istream:
        soup = BeautifulSoup(istream, 'html.parser')
        for hotel in soup.find_all('hotel'):
            hotelId = hotel.find('id').get_text()
            hotelName = hotel.find('name').get_text()
            # print(hotelName)
            hotelStar = hotel.find('star').get_text()
            hotelRate = hotel.find('rate').get_text()
            hotelPrice = hotel.find('price').get_text()
            hotelAddress = hotel.find('address').get_text()
            InsertHotel(hotelDict, hotelId, hotelName, hotelStar, hotelRate, hotelPrice, hotelAddress)
    DumpHotelDict(hotelDict, args)
def main():
    args = get_args()
    Dump(args)
if __name__ == "__main__":
    main()
