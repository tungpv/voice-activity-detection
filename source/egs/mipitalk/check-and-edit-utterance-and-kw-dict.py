#!/usr/bin/env python 

# -*- coding: utf-8 -*-
# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu

import argparse
import sys
import os
import re
import numpy as np
import logging

sys.path.insert(0, 'steps')
import libs.common as common_lib
sys.path.insert(0,'source/egs')
import python_libs.mipidict as mipidict_lib

logger = logging.getLogger('libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting check-and-edit-utterance-and-kw-dict')

def get_args():
    parser = argparse.ArgumentParser(description='Arguments parser')
    parser.add_argument('--debug', dest='debug', action='store_true')
    parser.add_argument('--normalize', dest='normalize', action='store_true')
    parser.add_argument('--kw-dict-file', dest='kw_dict_file', type=str,  help='predefined keyword dict to be checked and edited')
    parser.add_argument('--segmented-text', dest='text',  type=str, help='formatted text for checking with kw_dict_file')
    parser.add_argument('--label-text', type=str, dest='label_text', help='labelled text with dict')
    parser.add_argument('--transfer-text', type=str, dest='transfer_text', help='transfer text with transfer dict')
    parser.add_argument('--dump-word-list', type=str, dest='dump_word_list', help='word list file')
    parser.add_argument('--dump-text-to-label', type=str, dest='text_to_label', help='dump text file to label')
    parser.add_argument('--currency-unit-list', type=str, dest='currency_unit_list', help='currency unit file')
    return parser.parse_args()
def CheckLogger(args):
    if args.debug:
        ''' does not work '''
        logger.setLevel(logging.DEBUG)
        handler.setLevel(logging.DEBUG)
    logger.debug('logger now is at DEBUG level')

def main():
    args = get_args()
    CheckLogger(args)
    mipiDict = mipidict_lib.MipiDictVege(True)
    if args.kw_dict_file:
        mipiDict.LoadDict(args.kw_dict_file)
    if args.debug:
        mipiDict.DumpPhoneList()
    currencyUnitDict = dict()
    if args.currency_unit_list:
        mipiDict.LoadWordList(args.currency_unit_list, currencyUnitDict)
    wordCount = dict()
    labelText = str()
    transferText = str()
    textToLabel = str()
    if args.text:
        with open(args.text, 'r') as istream:
            for words in istream:
                if args.label_text:
                   labelText += mipiDict.LabelText(words)
                   labelText += '\n'
                if args.dump_word_list:
                    mipiDict.CountWord(words, wordCount)
                if args.transfer_text:
                    transferWords = mipiDict.TransferText(words, None, currencyUnitDict)
                    transferText += transferWords
                    transferText += '\n'
                    if args.text_to_label:
                        if textToLabel:
                            textToLabel += '\n'
                        textToLabel += mipiDict.DumpTextToLabel(transferWords)
                        
        if args.label_text:
            ostream = open(args.label_text, 'w')
            ostream.write('{0}'.format(labelText))
            ostream.close()
        if args.dump_word_list:
            sortedList = mipiDict.DumpCountDict(wordCount)
            ostream = open(args.dump_word_list, 'w')
            for word in sortedList:
                ostream.write('{0}\t{1}\n'.format(word, word))
            ostream.close()

        if args.transfer_text:
            ostream = open(args.transfer_text, 'w')
            ostream.write('{0}'.format(transferText))
            ostream.close()
        if args.text_to_label:
            ostream = open(args.text_to_label, 'w')
            ostream.write('{0}'.format(textToLabel))
            ostream.close()
if __name__ == "__main__":
    main()
