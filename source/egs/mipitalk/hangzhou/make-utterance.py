#!/usr/bin/env python 
# -*- coding: utf-8 -*-

# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu
from __future__ import print_function
import argparse
import sys
import os
import re
import numpy as np
import logging
from bs4 import BeautifulSoup
import random
# sys.path.insert(0, 'steps')
# import libs.common as common_lib
sys.path.insert(0,'source/egs')

import python_libs.mipidict as mipidict_lib
import python_libs.mipinumber as mipinumber_lib
import python_libs.mipiconverttext as mipitext_lib

import python_libs.mipihotel as mipihotel_lib
import python_libs.mipiutterance as mipiutt_lib

logger = logging.getLogger('python_libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting load-query-dict')
class UtteranceNormalizer(object):
    def __init__(self):
        pass

def get_args():
    parser = argparse.ArgumentParser(description='Arguments_parser')
    parser.add_argument('--xml-hotel', dest='xml_hotel', type=str, help='hotel table as xml format')
    parser.add_argument('--hotel-utterance', dest='hotel_utterance',  type=str, help='make utterance for hotel')
    parser.add_argument('--xml-restaurant', dest='xml_restaurant', type=str, help='restaurant table as xml format')
    parser.add_argument('--restaurant-utterance', dest='restaurant_utterance', type=str, help='make utterance for restaurant')

    return parser.parse_args()
def Normalize1(utterance):
    utterance = utterance.replace('（', ', ')
    utterance = utterance.replace('）', ',')
    utterance = utterance.replace('。', '')
    utterance = utterance.replace('，', ', ')
    utterance = re.sub('[\(\)\.·\t@]', ', ', utterance)
    utterance = re.sub(',\s*,', ',', utterance)
    utterance = re.sub(' ,', ',', utterance)
    return utterance
def Normalize2(utterance):
    utterance = re.sub(',\s*,', ',', utterance)
    return utterance
def GetRandomNumber(lower, upper):
    return random.randint(lower, upper)
def MakeRatePhrase(rate):
    rateExpressList = ['大众评分', '旅客评分', '旅客评价', '酒店得分']
    rIndex = GetRandomNumber(0, len(rateExpressList) -1)
    utterance = '{0} {1} 分'.format(rateExpressList[rIndex], rate)
    return utterance
def MakeRatePhrase4Rest(rate):
    rateExpressList = ['大众评分', '顾客评分', '顾客好评', '餐馆得分']
    rIndex = GetRandomNumber(0, len(rateExpressList) -1)
    utterance = '{0} {1} 分'.format(rateExpressList[rIndex], rate)
    return utterance
def MakePhonePhrase4Rest(phone):
    phoneList = ['通讯联系', '电话', '联系电话']
    rIndex = GetRandomNumber(0, len(phoneList) -1)
    utterance = '{0}: {1}'.format(phoneList[rIndex], phone)
    return utterance

def MakePricePhrase(price):
    pricePrefixList = ['人均参考价格', '每晚参考价格', '每晚均价', '一晚约', '每晚约', '一天约']
    rIndex = GetRandomNumber(0, len(pricePrefixList) -1)
    utterance = '{0} {1} 元'.format(pricePrefixList[rIndex], price)
    return utterance
def MakePricePhrase4Rest(price):
    pricePrefixList = ['参考价', '均价', '大概价位', '人均价', '人均大概消费']
    rIndex = GetRandomNumber(0, len(pricePrefixList) -1)
    price = re.sub(ur'^¥', '', price)
    utterance = '{0} {1} 元'.format(pricePrefixList[rIndex], price)
    return utterance

def MakeAddressPhrase(address):
    addressPrefixList = ['地址在','地址', '位于', '位置在', '位置']
    rIndex = GetRandomNumber(0, len(addressPrefixList) -1)
    utterance = '{0} {1}'.format(addressPrefixList[rIndex], address)
    return utterance
def MakeCuisinePhrase(cuisine):
    cuisineList = ['菜系', '菜类']
    rIndex = GetRandomNumber(0, len(cuisineList) -1)
    utterance = '{0}: {1}'.format(cuisineList[rIndex], cuisine)
    return utterance

def MakeUtteranceForHotel(args):
    random.seed(777)
    utterances = str()
    if not args.xml_hotel:
        return
    with open(args.xml_hotel, 'r') as istream:
        soup = BeautifulSoup(istream, 'html.parser')
        for hotel in soup.find_all('hotel'):
            utterance = ''
            hotelId = hotel.find('id').get_text().strip()
            hotelName = hotel.find('name').get_text().strip()
            nameList = hotelName.split('\t')
            if len(nameList) == 2 and nameList[0] == nameList[1]:
                logger.info('Chinese and English name are duplicated for {0}'.format(hotelName))
                hotelName = nameList[0]
            utterance = Normalize1(hotelName) + ', '
            hotelRate = hotel.find('rate').get_text().strip()
            utterance += MakeRatePhrase(hotelRate) + ', '
            hotelPrice = hotel.find('price').get_text().strip()
            utterance += MakePricePhrase(hotelPrice) + ', '
            hotelAddress = hotel.find('address').get_text().strip()
            hotelAddress = Normalize1(hotelAddress)
            utterance += MakeAddressPhrase(hotelAddress) 
            utterances += Normalize2(utterance) + '\n'
    if not args.hotel_utterance:
        print('{0}'.format(utterances))
        return
    ostream = open(args.hotel_utterance, 'w')
    ostream.write('{0}'.format(utterances))
    ostream.close()
def NormalizePhone(phone):
    phone = re.sub(r'[\s;]', ', ', phone)
    phone = re.sub(r'[\-#]', '杠', phone)
    return phone
def MakeUtteranceForRestaurant(args):
    random.seed(777)
    utterances = str()
    if not args.xml_restaurant:
        return
    with open(args.xml_restaurant, 'r') as istream:
        soup = BeautifulSoup(istream, 'html.parser')
        for rest in soup.find_all('restaurant'):
            utterance = str()
            restId = rest.find('id').get_text().strip()
            restName = rest.find('name').get_text().strip()
            nameList = restName.split('\t')
            if len(nameList) == 2 and nameList[0] == nameList[1]:
                logger.info('Chinese and English name are duplicated for {0}'.format(restName))
                restName = nameList[0]
            utterance = Normalize1(restName) + ', '
            # logger.info('utterance={0}'.format(utterance))
            restRate = rest.find('rate').get_text().strip()
            utterance += MakeRatePhrase4Rest(restRate) + ', '
            restPrice = rest.find('price').get_text().strip()
            utterance += MakePricePhrase4Rest(restPrice) + ', '
            restPhone = rest.find('phone').get_text().strip()
            restPhone = NormalizePhone(restPhone)
            utterance += MakePhonePhrase4Rest(restPhone) + ', '
            restAddress = rest.find('address').get_text().strip()
            restAddress = Normalize1(restAddress)
            utterance += MakeAddressPhrase(restAddress) + ', '
            cuisine = rest.find('cuisine').get_text().strip()
            utterance += MakeCuisinePhrase(cuisine)
            utterances += Normalize2(utterance) + '\n'
    if not args.restaurant_utterance:
        print('{0}'.format(utterances))
        return
    ostream = open(args.restaurant_utterance, 'w')
    ostream.write('{0}'.format(utterances))
    ostream.close()

def main():
    args = get_args()
    MakeUtteranceForHotel(args)
    MakeUtteranceForRestaurant(args)
if __name__ == "__main__":
    main()
