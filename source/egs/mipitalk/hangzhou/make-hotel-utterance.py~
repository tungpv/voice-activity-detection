#!/usr/bin/env python 
# -*- coding: utf-8 -*-

# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu
from __future__ import print_function
import argparse
import sys
import os
import re
import numpy as np
import logging
from bs4 import BeautifulSoup
import random
# sys.path.insert(0, 'steps')
# import libs.common as common_lib
sys.path.insert(0,'source/egs')

import python_libs.mipidict as mipidict_lib
import python_libs.mipinumber as mipinumber_lib
import python_libs.mipiconverttext as mipitext_lib

import python_libs.mipihotel as mipihotel_lib
import python_libs.mipiutterance as mipiutt_lib

logger = logging.getLogger('python_libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting load-query-dict')
class UtteranceNormalizer(object):
    def __init__(self):
        pass

def get_args():
    parser = argparse.ArgumentParser(description='Arguments_parser')
    parser.add_argument('xml_hotel', type=str, help='hotel table as xml format')
    parser.add_argument('hotel_utterance', type=str, help='make utterance for hotel')
    return parser.parse_args()
def Normalize1(utterance):
    utterance = utterance.replace('（', ', ')
    utterance = utterance.replace('）', ',')
    utterance = utterance.replace('。', '')
    utterance = utterance.replace('，', ', ')
    utterance = re.sub('[\(\)\.·\t@]', ', ', utterance)
    utterance = re.sub(',\s*,', ',', utterance)
    utterance = re.sub(' ,', ',', utterance)
    return utterance
def Normalize2(utterance):
    utterance = re.sub(',\s*,', ',', utterance)
    return utterance
def GetRandomNumber(lower, upper):
    return random.randint(lower, upper)
def MakeRatePhrase(rate):
    rateExpressList = ['大众评分', '旅客评分', '旅客评价', '酒店得分']
    rIndex = GetRandomNumber(0, len(rateExpressList) -1)
    utterance = '{0} {1} 分'.format(rateExpressList[rIndex], rate)
    return utterance
def MakePricePhrase(price):
    pricePrefixList = ['人均参考价格', '每晚参考价格', '每晚均价', '一晚约', '每晚约', '一天约']
    rIndex = GetRandomNumber(0, len(pricePrefixList) -1)
    utterance = '{0} {1} 元'.format(pricePrefixList[rIndex], price)
    return utterance
def MakeAddressPhrase(address):
    addressPrefixList = ['地址在','地址', '位于', '位置在', '位置']
    rIndex = GetRandomNumber(0, len(addressPrefixList) -1)
    utterance = '{0} {1}'.format(addressPrefixList[rIndex], address)
    return utterance
def MakeUtteranceForHotel(args):
    random.seed(777)
    utterances = str()
    with open(args.xml_hotel, 'r') as istream:
        soup = BeautifulSoup(istream, 'html.parser')
        for hotel in soup.find_all('hotel'):
            utterance = ''
            hotelId = hotel.find('id').get_text().strip()
            hotelName = hotel.find('name').get_text().strip()
            nameList = hotelName.split('\t')
            if len(nameList) == 2 and nameList[0] == nameList[1]:
                logger.info('Chinese and English name are duplicated for {0}'.format(hotelName))
                hotelName = nameList[0]
            utterance = Normalize1(hotelName) + ', '
            hotelRate = hotel.find('rate').get_text().strip()
            utterance += MakeRatePhrase(hotelRate) + ', '
            hotelPrice = hotel.find('price').get_text().strip()
            utterance += MakePricePhrase(hotelPrice) + ', '
            hotelAddress = hotel.find('address').get_text().strip()
            hotelAddress = Normalize1(hotelAddress)
            utterance += MakeAddressPhrase(hotelAddress) 
            utterances += Normalize2(utterance) + '\n'
    ostream = open(args.hotel_utterance, 'w')
    ostream.write('{0}'.format(utterances))
    ostream.close()
                
def main():
    args = get_args()
    MakeUtteranceForHotel(args)

if __name__ == "__main__":
    main()
