#!/usr/bin/perl -w 
use strict;
use utf8;
use open qw(:std :utf8);

while(<STDIN>) {
  chomp;
  while(m:([^\p{Han}\s]+):g) {
    my $w = $1;
    if ($w !~ m:^\d+$:g) {
      print "$w\t$w\n";
    }
  }
}
