#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\nExample: cat text | $0 transfer-match-dict.txt > transfered-text.txt\n\n";
}

my ($dictFile) = @ARGV;

# begin sub
sub LoadDictAllowNakedWord {
  my ($inFile, $vocab) = @_;
  open(F, "$inFile") or die "## ERROR: cannot open file $inFile\n";
  while(<F>) {
    chomp;
    m:(^\S+)\s*(.*)$:g or next;
    my $word = $1;
    my $phoneStr = $2;
    $$vocab{$1} = $2;
  }
  close(F);
}
# end sub

my %vocab = ();
LoadDictAllowNakedWord($dictFile, \%vocab);

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  my $utterance = $_;
  while(m:([^\p{Han}\s]+):g) {
    my $w = $1;
    if ($w !~ m:^\d+$:g) {
      if (exists $vocab{$w}) {
        $utterance =~ s:\Q$1\E:$vocab{$w}:;
      }
    }
  }
  print "$utterance\n";
}
print STDERR "## LOG ($0): stdin ended\n";
