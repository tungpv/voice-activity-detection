#!/bin/bash

. path.sh 
. cmd.sh 

# begin options
steps=
# end options
. parse_options.sh || exit 1

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
xml_hotel=../sge/name-entity/dict/hangzhou/hotel/hangzhou-hotel.xml
tgtdir=$(dirname $xml_hotel)
if [ ! -z $step01 ]; then
  source/egs/mipitalk/hangzhou/make-hotel-utterance.py $xml_hotel  $tgtdir/hangzhou-hotel-utterance.txt
  cat $tgtdir/hangzhou-hotel-utterance.txt | \
  source/egs/mipitalk/hangzhou/make-word-transfer-dict.pl  | sort -u > $tgtdir/word-transfer-dict-01.txt
  echo "## LOG (step01, $0): done & check '$tgtdir/hangzhou-hotel-utterance.txt' & '$tgtdir/word-transfer-dict.txt'"
fi
if [ ! -z $step02 ]; then
  cat $tgtdir/hangzhou-hotel-utterance.txt | \
  source/egs/mipitalk/hangzhou/transfer-utterance-with-match-dict.pl  $tgtdir/word-transfer-dict-01.txt \
  > $tgtdir/hangzhou-hotel-utterance-it01.txt
  echo "## LOG (step02, $0): done & check '$tgtdir/hangzhou-hotel-utterance-it01.txt'"
fi
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): done & check ''"
fi
