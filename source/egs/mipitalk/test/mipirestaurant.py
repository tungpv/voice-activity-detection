#!/usr/bin/env python 
# -*- coding: utf-8 -*-

# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu
from __future__ import print_function
import argparse
import sys
import os
import re
import numpy as np
import logging

# sys.path.insert(0, 'steps')
# import libs.common as common_lib
sys.path.insert(0,'source/egs')

import python_libs.mipidict as mipidict_lib
import python_libs.mipinumber as mipinumber_lib
import python_libs.mipiconverttext as mipitext_lib

import python_libs.mipihotel as mipihotel_lib
import python_libs.mipirestaurant as mipirest_lib
import python_libs.mipiutterance as mipiutt_lib

logger = logging.getLogger('python_libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting load-query-dict')

def get_args():
    parser = argparse.ArgumentParser(description='Arguments_parser')
    parser.add_argument('xml_restaurant', type=str, help='restaurant table as xml format')
    parser.add_argument('--dump-name-table', dest='dump_name_table', type=str, help='file for the dump name table')
    parser.add_argument('--dump-address-table', dest='dump_address_table', type=str, help='file for the dump address table')
    parser.add_argument('--dump-cuisine-table', dest='dump_cuisine_table', type=str, help='file for the dump cuisine table')

    parser.add_argument('--load-name-table', dest='load_name_table', type=str, help='file for name table')
    parser.add_argument('--load-address-table', dest='load_address_table', type=str, help='file for address table')
    parser.add_argument('--load-cuisine-table', dest='load_cuisine_table', type=str, help='file for cuisine table')
    parser.add_argument('--load-clue-keyword-table', dest='load_clue_keyword_table', type=str, help='clue keyword to check if restaurant query happens')
    parser.add_argument('--load-user-table', dest='load_user_table', type=str, help='user defined table')
    return parser.parse_args()

def DumpNameTable(args, mipiRestaurant):
    if args.dump_name_table:
        ostream = open(args.dump_name_table, 'w')
        tableStr = mipiRestaurant.DumpTable('name', True)
        ostream.write('{0}'.format(tableStr.lower()))
        ostream.close()
def DumpAddressTable(args, mipiRestaurant):
    if args.dump_address_table:
        ostream = open(args.dump_address_table, 'w')
        tableStr = mipiRestaurant.DumpTable('address', True)
        ostream.write('{0}'.format(tableStr.lower()))
        ostream.close()
def DumpCuisineTable(args, mipiRestaurant):
    if args.dump_cuisine_table:
        ostream = open(args.dump_cuisine_table, 'w')
        tableStr = mipiRestaurant.DumpTable('cuisine', True)
        ostream.write('{0}'.format(tableStr.lower()))
        ostream.close()
def TestLoadTables(args, mipiRestaurant):
    """ test loading table modules """
    if args.load_name_table:
        mipiRestaurant.LoadCheckAndSearchTable('name', args.load_name_table)
        # tableStr = mipiRestaurant.DumpTable('name')
        # print('{0}'.format(tableStr))
    if args.load_address_table:
        mipiRestaurant.LoadCheckAndSearchTable('address', args.load_address_table)
    if args.load_cuisine_table:
        mipiRestaurant.LoadCheckAndSearchTable('cuisine', args.load_cuisine_table)
    if args.load_clue_keyword_table:
        mipiRestaurant.LoadClueKeywordDict(args.load_clue_keyword_table)
    if args.load_user_table:
        mipiRestaurant.LoadCheckAndSearchTable('user', args.load_user_table)
def Test(args):
    mipiRestaurant = mipirest_lib.MipiRestaurant()
    mipiRestaurant.LoadRestaurant(args.xml_restaurant)
    DumpNameTable(args, mipiRestaurant)
    DumpAddressTable(args, mipiRestaurant)
    DumpCuisineTable(args, mipiRestaurant)
    mipiUtterance = mipiutt_lib.MipiUtterance()
    TestLoadTables(args, mipiRestaurant)
    # print('{0}'.format(mipiRestaurant.DumpTable('name', True)))
    # print('{0}'.format(mipiRestaurant.DumpTable('name', False)))
    uttList  = [  u'帮我 找 餐馆' ]
    mipiUtterance.NormalizeAndCopy(uttList)
    if mipiRestaurant.IsQueryRestaurant(mipiUtterance):
        print('\nIt seems that you ask me to look for a restaurant, for your query ({0})\n'.format(re.sub(ur'\s+', '',uttList[0])))
    # print('{0}'.format(mipiRestaurant.DumpCheckTable('user')))
    print ('user table: {0}'.format(mipiRestaurant.DumpTable('user', False)))
    print('{0}'.format(mipiRestaurant.DumpResult()))
    
    uttList = [u'I want to look for a food court nearby']
    mipiUtterance.NormalizeAndCopy(uttList)
    mipiRestaurant.ResetSearchResult()
    if mipiRestaurant.IsQueryRestaurant(mipiUtterance):
        print('\nIt seems that you ask me to look for a restaurant, for your query ({0})\n'.format(re.sub(ur'\s+', ' ',uttList[0])))
    print('{0}'.format(mipiRestaurant.DumpResult()))

    uttList = [u'Is there any starbucks']
    mipiUtterance.NormalizeAndCopy(uttList)
    mipiRestaurant.ResetSearchResult()
    if mipiRestaurant.IsQueryRestaurant(mipiUtterance):
        print('\nIt seems that you ask me to look for a restaurant, for your query ({0})\n'.format(re.sub(ur'\s+', ' ',uttList[0])))
    print('{0}'.format(mipiRestaurant.DumpResult()))

    uttList  = [  u'附近 有 啤酒屋 吗' ]
    mipiUtterance.NormalizeAndCopy(uttList)
    mipiRestaurant.ResetSearchResult()
    if mipiRestaurant.IsQueryRestaurant(mipiUtterance):
        print('\nIt seems that you ask me to look for a restaurant, for your query ({0})\n'.format(re.sub(ur'\s+', '',uttList[0])))
    print('{0}'.format(mipiRestaurant.DumpResult()))
   
    uttList  = [  u'附近 有 海南鸡饭 吗' ]
    mipiUtterance.NormalizeAndCopy(uttList)
    mipiRestaurant.ResetSearchResult()
    if mipiRestaurant.IsQueryRestaurant(mipiUtterance):
        print('\nIt seems that you ask me to look for a restaurant, for your query ({0})\n'.format(re.sub(ur'\s+', '',uttList[0])))
    print('{0}'.format(mipiRestaurant.DumpResult()))

    uttList = [u'附近 有 海南鸡饭 吗 ? 价格在 三十 左右']
    mipiUtterance.NormalizeAndCopy(uttList)
    mipiRestaurant.ResetSearchResult()
    # print('{0}'.format(mipiRestaurant.DumpTable('price')))
    if mipiRestaurant.IsQueryRestaurant(mipiUtterance):
        print('\nIt seems that you ask me to look for a restaurant, for your query ({0})\n'.format(re.sub(ur'\s+', '',uttList[0])))
    print('{0}'.format(mipiRestaurant.DumpResult()))

    uttList = [u'附近 有 海南鸡饭 吗 ? 价格在 三十 左右, 评分 在 3.7 分 左右']
    mipiUtterance.NormalizeAndCopy(uttList)
    mipiRestaurant.ResetSearchResult()
    # print('{0}'.format(mipiRestaurant.DumpTable('price')))
    if mipiRestaurant.IsQueryRestaurant(mipiUtterance):
        print('\nIt seems that you ask me to look for a restaurant, for your query ({0})\n'.format(re.sub(ur'\s+', '',uttList[0])))
    print('{0}'.format(mipiRestaurant.DumpResult()))

    uttList = [u'附近 有 海南鸡饭 吗 ? 价格在 三十 左右, 评分 在 4.5 分 左右']
    mipiUtterance.NormalizeAndCopy(uttList)
    mipiRestaurant.ResetSearchResult()
    # print('{0}'.format(mipiRestaurant.DumpTable('price')))
    if mipiRestaurant.IsQueryRestaurant(mipiUtterance):
        print('\nIt seems that you ask me to look for a restaurant, for your query ({0})\n'.format(re.sub(ur'\s+', '',uttList[0])))
    print('{0}'.format(mipiRestaurant.DumpResult()))
def main():
    args = get_args()
    Test(args)

    
if __name__ == "__main__":
    main()
