#!/bin/bash

. path.sh
# begin options
steps=
# end options
. parse_options.sh || exit 1

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

xml_hotel=../sge/name-entity/dict/singapore/hotel/sg-hotel-detail.xml
table_name=../sge/name-entity/dict/singapore/hotel/table-name.txt
table_address=../sge/name-entity/dict/singapore/hotel/table-address.txt

if [ ! -z $step01 ]; then
  source/egs/mipitalk/test/mipihotel.py $xml_hotel \
  --dump-name-table $table_name \
  --dump-address-table $table_address
  echo "## (step01, $0): done & check '$table_name' & '$table_address'"
fi
tgtdir=../sge/name-entity/dict/singapore/hotel
if [ ! -z $step02 ]; then
  cat $tgtdir/table-name-edit.txt | \
  source/egs/mipitalk/make-word-transfer-dict.pl | \
  sort -u  > $tgtdir/transfer-name-dict-edit.txt
  [ -f $tgtdir/transfer-name-dict-edit-it01.txt ] || \
  cp $tgtdir/transfer-name-dict-edit.txt $tgtdir/transfer-name-dict-edit-it01.txt
  echo "## LOG (step02, $0): done & check '$tgtdir/transfer-name-dict-edit-it01.txt'"
fi
if [ ! -z $step03 ]; then
  cat $tgtdir/table-name-edit.txt |\
  source/egs/mipitalk/transfer-utterance-with-dict.pl $tgtdir/transfer-name-dict-edit-it01.txt \
  > $tgtdir/table-name-edit-it01.txt
  echo "## LOG (step03, $0): done & check '$tgtdir/table-name-edit-it01.txt'"
fi

