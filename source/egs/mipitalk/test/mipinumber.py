#!/usr/bin/env python 
# -*- coding: utf-8 -*-

# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu

import argparse
import sys
import os
import re
import numpy as np
import logging

sys.path.insert(0, 'steps')
import libs.common as common_lib
sys.path.insert(0,'source/egs')

import python_libs.mipidict as mipidict_lib
import python_libs.mipinumber as mipinumber_lib
import python_libs.mipiconverttext as mipitext_lib

logger = logging.getLogger('libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting check-and-edit-utterance-and-kw-dict')

def get_args():
    parser = argparse.ArgumentParser(description='Arguments parser')
    return parser.parse_args()

def main():
    args = get_args()
    mipiNumber = mipinumber_lib.MipiNumber()
    utterance = u'一千 一万 零二 号'
    for word in utterance.split():
        newWord = mipiNumber.NumberConvertible(word)
        print('{0}\t{1}'.format(word, newWord))
    newUtterance = mipiNumber.ChineseToArabicNumberTransfer(utterance)
    print('{0}'.format(newUtterance))

if __name__ == "__main__":
    main()
