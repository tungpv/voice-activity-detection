#!/usr/bin/env python 
# -*- coding: utf-8 -*-

# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu
from __future__ import print_function
import argparse
import sys
import os
import re
import numpy as np
import logging

# sys.path.insert(0, 'steps')
# import libs.common as common_lib
sys.path.insert(0,'source/egs')

import python_libs.mipidict as mipidict_lib
import python_libs.mipinumber as mipinumber_lib
import python_libs.mipiconverttext as mipitext_lib

import python_libs.mipihotel as mipihotel_lib
import python_libs.mipiutterance as mipiutt_lib

logger = logging.getLogger('python_libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting load-query-dict')

def get_args():
    parser = argparse.ArgumentParser(description='Arguments_parser')
    parser.add_argument('xml_hotel', type=str, help='hotel table as xml format')
    parser.add_argument('--dump-name-table', dest='dump_name_table', type=str, help='file for the dump name table')
    parser.add_argument('--dump-address-table', dest='dump_address_table', type=str, help='dump address table')
    parser.add_argument('--load-name-table', dest='load_name_table', type=str, help='file for name table')
    parser.add_argument('--load-address-table', dest='load_address_table', type=str, help='load address table')
    return parser.parse_args()

def DumpNameTable(args, mipiHotel):
    if args.dump_name_table:
        ostream = open(args.dump_name_table, 'w')
        tableStr = mipiHotel.DumpTable('name', True)
        ostream.write('{0}'.format(tableStr.lower()))
        ostream.close()

def DumpAddressTable(args, mipiHotel):
    if args.dump_address_table:
        ostream = open(args.dump_address_table, 'w')
        tableStr = mipiHotel.DumpTable('address', True)
        ostream.write('{0}'.format(tableStr.lower()))
        ostream.close()

def TestDumpTable(args, mipiHotel):
    DumpNameTable(args, mipiHotel)
    DumpAddressTable(args, mipiHotel)

def TestLoadTables(args, mipiHotel):
    if args.load_name_table:
        mipiHotel.LoadCheckAndSearchTable('name', args.load_name_table)
    if args.load_address_table:
        mipiHotel.LoadCheckAndSearchTable('address', args.load_address_table)
def Test(args):
    mipiHotel = mipihotel_lib.MipiHotel()
    mipiHotel.LoadHotel(args.xml_hotel)
    TestDumpTable(args, mipiHotel)
    TestLoadTables(args, mipiHotel)
    mipiUtterance = mipiutt_lib.MipiUtterance()
    
def main():
    args = get_args()
    Test(args)
if __name__ == "__main__":
    main()
