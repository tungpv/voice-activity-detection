#!/usr/bin/env python 
# -*- coding: utf-8 -*-

# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu
from __future__ import print_function
import argparse
import sys
import os
import re
import numpy as np
import logging

sys.path.insert(0, 'steps')
import libs.common as common_lib
sys.path.insert(0,'source/egs')

import python_libs.mipidict as mipidict_lib
import python_libs.mipinumber as mipinumber_lib
import python_libs.mipiconverttext as mipitext_lib

import python_libs.mipihotel as mipihotel_lib

logger = logging.getLogger('libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting load-query-dict')

def get_args():
    parser = argparse.ArgumentParser(description='Arguments_parser')
    parser.add_argument('table_hotel', type=str, help='table file for the hotel overall information')
    parser.add_argument('table_name', type=str, help='table file for name keyword')
    parser.add_argument('table_address', type=str, help='table file for address keyword')
    parser.add_argument('--test', dest='test', action='store_true')
    return parser.parse_args()

def Test2(args):
    mipiHotel = mipihotel_lib.MipiHotel()
    mipiHotel.LoadTable(args.table_hotel)
    # mipiHotel.DumpTable('id')
    mipiHotel.LoadQueryKeywordTable('name', args.table_name)
    mipiHotel.LoadQueryKeywordTable('address', args.table_address)
    # mipiHotel.DumpTable('name')
    # mipiHotel.DumpQuery()
    uttList1 = [  u'帮我　找　hotel' ]
    if mipiHotel.IsQueryHotel(uttList1):
        print('\nYou are searching a hotel, for your query ({0})'.format(uttList1[0]))
        mipiHotel.DumpResultDict()
    uttList2 = [ u'圣淘沙 周边 酒店' ]
    if mipiHotel.IsQueryHotel(uttList2):
        print('\nYou are searching a hotel, for your query ({0})'.format(re.sub(ur'\s+', '', uttList2[0])))
        mipiHotel.DumpResultDict()
    uttList2 = [u'请 查找 新加坡 所有 五星级 酒店']
    mipiHotel.ResetSearch()
    if mipiHotel.IsQueryHotel(uttList2):
        print('\nYou are searching a hotel, for your query ({0})'.format(re.sub(ur'\s+', '', uttList2[0])))
        mipiHotel.DumpResultDict()
    uttList2 = [u'请 查找 酒店 评 分 在 4.8 分 左右']
    mipiHotel.ResetSearch()
    if mipiHotel.IsQueryHotel(uttList2):
        print('\nYou are searching a hotel, for your query ({0})'.format(re.sub(ur'\s+', '', uttList2[0])))
        mipiHotel.DumpResultDict()
    uttList2 = [u'请 帮我 找 一家 价格 在 500 左右 的 酒店']
    mipiHotel.ResetSearch()
    if mipiHotel.IsQueryHotel(uttList2):
        print('\nYou are searching a hotel, for your query ({0})'.format(re.sub(ur'\s+', '', uttList2[0])))
        mipiHotel.DumpResultDict()
    uttList2 = [u'请 帮我 找 一家 酒店 价格 在 五百 到 六百 之间']
    mipiHotel.ResetSearch()
    if mipiHotel.IsQueryHotel(uttList2):
        print('\nYou are searching a hotel, for your query ({0})'.format(re.sub(ur'\s+', '', uttList2[0])))
        mipiHotel.DumpResultDict()
def main():
    args = get_args()
    Test2(args)
if __name__ == "__main__":
    main()
