#!/bin/bash 

. path.sh
# begin options
steps=
# end options
. parse_options.sh || exit 1

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

xml_restaurant=../sge/name-entity/dict/singapore/restaurant/sg-restaurant-detail.xml
table_name=../sge/name-entity/dict/singapore/restaurant/table-name.txt
table_address=../sge/name-entity/dict/singapore/restaurant/table-address.txt
table_cuisine=../sge/name-entity/dict/singapore/restaurant/table-cuisine.txt
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0):"
  source/egs/mipitalk/test/mipirestaurant.py $xml_restaurant  \
  --dump-name-table $table_name \
  --dump-addresss-table $table_address \
  --dump-cuisine-table $table_cuisine
  echo "done & check '$table_name' & '$table_address' & '$table_cuisine'"
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): "
  cat ../sge/name-entity/dict/singapore/restaurant/table-address-edit.txt | \
  source/egs/mipitalk/make-word-transfer-dict.pl | \
  sort -u  > ../sge/name-entity/dict/singapore/restaurant/transfer-address-dict.txt
  echo "done & check '../sge/name-entity/dict/singapore/restaurant/transfer-address-dict.txt'"
fi

if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0)"
  cat ../sge/name-entity/dict/singapore/restaurant/table-address-edit.txt | \
  source/egs/mipitalk/transfer-utterance-with-dict.pl ../sge/name-entity/dict/singapore/restaurant/transfer-address-dict-edit.txt \
  > ../sge/name-entity/dict/singapore/restaurant/table-address-edit-it01.txt
fi

if [ ! -z $step04 ]; then
  cat ../sge/name-entity/dict/singapore/restaurant/table-name-edit.txt | \
  source/egs/mipitalk/make-word-transfer-dict.pl | \
  sort -u > ../sge/name-entity/dict/singapore/restaurant/transfer-name-dict-edit.txt
  echo "done & check '../sge/name-entity/dict/singapore/restaurant/transfer-name-dict-edit.txt'"
fi
if [ ! -z $step05 ]; then
  cat ../sge/name-entity/dict/singapore/restaurant/table-name-edit.txt | \
  source/egs/mipitalk/transfer-utterance-with-dict.pl ../sge/name-entity/dict/singapore/restaurant/transfer-name-dict-edit.txt \
 > ../sge/name-entity/dict/singapore/restaurant/table-name-edit-it01.txt
  echo "done & check '../sge/name-entity/dict/singapore/restaurant/table-name-edit-it01.txt'"
fi
if [ ! -z $step06 ]; then
  cat ../sge/name-entity/dict/singapore/restaurant/table-cuisine.txt | \
  source/egs/mipitalk/make-word-transfer-dict.pl | \
  sort -u > ../sge/name-entity/dict/singapore/restaurant/transfer-cuisine-edit.txt
  echo "done & check '../sge/name-entity/dict/singapore/restaurant/transfer-cuisine-edit.txt'"
fi
if [ ! -z $step07 ]; then
  cat ../sge/name-entity/dict/singapore/restaurant/table-cuisine.txt | \
  source/egs/mipitalk/transfer-utterance-with-dict.pl ../sge/name-entity/dict/singapore/restaurant/transfer-cuisine-edit.txt \
  > ../sge/name-entity/dict/singapore/restaurant/table-cuisine-transfer-it01.txt
fi
load_name_table=../sge/name-entity/dict/singapore/restaurant/table-name-edit-it01.txt
load_address_table=../sge/name-entity/dict/singapore/restaurant/table-address-edit-it01.txt
load_cuisine_table=../sge/name-entity/dict/singapore/restaurant/table-cuisine-transfer-it01.txt
load_user_table=../sge/name-entity/dict/singapore/restaurant/table-user.txt
if [ ! -z $step08 ]; then
  echo "## LOG (step01, $0):"
  source/egs/mipitalk/test/mipirestaurant.py $xml_restaurant  \
  --load-name-table $load_name_table \
  --load-address-table $load_address_table \
  --load-cuisine-table $load_cuisine_table
  echo "done & check '$table_name' & '$table_address' & '$table_cuisine'"
fi
if [ ! -z $step09 ]; then
  cat $load_name_table \
   $load_address_table \
   $load_cuisine_table | \
   source/egs/sge2017/local/normalize-text/word-count.pl --from=2 > ../sge/name-entity/dict/singapore/restaurant/word-count-from-tables.txt
  echo ""
fi
load_clue_keyword_table=../sge/name-entity/dict/singapore/restaurant/word-count-from-tables.txt
if [ ! -z $step10 ]; then
  echo "## LOG (step10, $0): test loading clue keyword table as well as normal table"
  source/egs/mipitalk/test/mipirestaurant.py $xml_restaurant  \
  --load-name-table $load_name_table \
  --load-address-table $load_address_table \
  --load-cuisine-table $load_cuisine_table \
  --load-clue-keyword-table $load_clue_keyword_table \
  --load-user-table $load_user_table
fi
