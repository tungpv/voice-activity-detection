#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;
if ($numArgs != 3) {
  die "\nExample: cat text | $0 arabic-lexicon.txt word-pron.txt tgtdir\n\n";
}
my ($arabic_lexicon_file, $text_file, $tgtdir) = @ARGV;

# begin sub
sub LoadArabicLexicon {
  my ($file_name, $vocab) = @_;
  open(F, "$file_name") or die;
  while(<F>) {
    chomp;
    m:^(\S+)\s+\[(.*)\]\s+(.*)$:g or next;
    my ($arabic, $word, $pron) = ($1, $2, $3);
    $pron =~ s:^\s+|\s+$::g;
    $pron =~ s:\s+: :g;
    my $key = sprintf("%s\t%s", $arabic, $pron);
    if(exists $$vocab{$key}) {
      print STDERR "## WARNING ($0): duplicated $key\n";
    }
    $$vocab{$key} = $word;
  }
  close F;
}
sub LoadText {
  my ($file_name, $vocab) = @_;
  open (F, "$file_name") or die;
  while(<F>) {
    chomp;
    m:^(\S+)\s+(.*)\s*$:g or next;
    $$vocab{$1} = $2;
  }
  close F;
}
sub TransferText {
  my ($vocab, $array, $text, $oovDict) = @_;
  my @A = split(/\s+/, $$text);
  if (scalar @A != scalar @$array) {
    die "## ERROR (TransferText): size mismatched $$text\n";
  }
  my $words = '';
  for(my $i = 0; $i < @$array; $i ++) {
    my $wordPron = $$array[$i];
    if ($wordPron =~ /^(\d+)/) {
      if (not exists $$vocab{$wordPron}) {
        print STDERR "## WARNING (TransferText): oov arabic word $wordPron\n";
	$$oovDict{$wordPron} ++;
        $words .= $1 . ' ';
        next;
      }
      $words .= $$vocab{$wordPron} . ' ';
      next;
    }
    $words .= $A[$i] . ' ';
  }
  $words =~ s:\s+$::g;
  $$text = $words;
}
# end sub
my %transferDict = ();
LoadArabicLexicon($arabic_lexicon_file, \%transferDict);
my %textDict = ();
LoadText($text_file, \%textDict);
my %oovDict = ();
open(OUTPUT, ">$tgtdir/text") or die;
open(OOV, ">$tgtdir/oov-dict.txt") or die;
print STDERR "stdin expected ...\n";
my $prevLabel = '';
my @A = ();
while(<STDIN>) {
  chomp;
  m:^(\S+)\s+(\S+)\s+(.*)$:g or next;
  my ($curLabel, $word, $pron) = ($1, $2, $3);
  $pron =~ s:_S|_B|_I|_E::g;
  $pron =~ s:^\s+|\s+$::g;
  $pron =~ s:\s+: :g;
  my $wordPron = sprintf("%s\t%s", $word, $pron);
  if($prevLabel ne '' && $curLabel ne $prevLabel) {
    die if not exists $textDict{$prevLabel};
    my $text = $textDict{$prevLabel};
    TransferText(\%transferDict, \@A, \$text, \%oovDict);
    print OUTPUT "$prevLabel $text\n";
    @A = ();
  }
  $prevLabel = $curLabel;
  push @A, $wordPron;
}
if ($prevLabel ne '') {
  die if not exists $textDict{$prevLabel};
  my $text = $textDict{$prevLabel};
  TransferText(\%transferDict, \@A, \$text, \%oovDict);
  print OUTPUT "$prevLabel $text\n";
}
print STDERR "stdin ended ...\n";
foreach my $x (keys%oovDict) {
  print OOV "$x\n";
}
close OOV;
close OUTPUT;
