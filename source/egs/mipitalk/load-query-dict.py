#!/usr/bin/env python 

# -*- coding: utf-8 -*-
# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu

import argparse
import sys
import os
import re
import numpy as np
import logging

sys.path.insert(0, 'steps')
import libs.common as common_lib
sys.path.insert(0,'source/egs')

import python_libs.mipidict as mipidict_lib
import python_libs.mipinumber as mipinumber_lib
import python_libs.mipiconverttext as mipitext_lib
import python_libs.mipihotel as mipihotel_lib

logger = logging.getLogger('libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting load-query-dict')

def get_args():
    parser = argparse.ArgumentParser(description='Arguments_parser')
    parser.add_argument('table_hotel', type=str, help='table file for the hotel overall information')
    parser.add_argument('table_name', type=str, help='table file for name keyword')
    parser.add_argument('table_address', type=str, help='table file for address keyword')
    parser.add_argument('--test', dest='test', action='store_true')
    return parser.parse_args()
def Test(args):
     worldDict = mipidict_lib.MipiWorldDict()
     worldDict.GetCityDict('singapore')
     worldDict.LoadHotelTable(args.table_hotel)
     worldDict.DumpTable('hotel', 'id')
     worldDict.DumpTable('hotel', 'price')
     # worldDict.DumpTable('hotel', 'star')
     # worldDict.DumpTable('hotel', 'rate')
     worldDict.LoadNameKeywordTable('name', args.table_name)
     worldDict.DumpTable('hotel', 'name')
     worldDict.LoadNameKeywordTable('address', args.table_address)
     worldDict.DumpTable('hotel', 'address')
def Test2(args):
    mipiHotel = mipihotel_lib.MipiHotel()
    mipiHotel.LoadTable(args.table_hotel)
    mipiHotel.DumpTable('id')
    mipiHotel.LoadQueryKeywordTable('name', args.table_name)
    mipiHotel.LoadQueryKeywordTable('address', args.table_address)
    # mipiHotel.DumpTable('name')
    mipiHotel.DumpQuery()
def main():
    args = get_args()
    if not args.test:
        Test(args)
    else:
        Test2(args)
if __name__ == "__main__":
    main()
