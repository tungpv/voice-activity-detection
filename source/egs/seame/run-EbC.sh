#!/bin/bash
. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
steps=
cmd=slurm.pl
nj=40
lmwt_for_lat=10
align_overlap_rate=0.8
cn_syl_dict_dir=/home2/hhx502/seame/cn-syllable/data/local/dict
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <english-word-data> <source-dict> <source-lang>  <source-dnn-dir> <mandarin-syllable-lang>  <mandarin-syllable-graph> <mandarin-syllable-dnn-dir> <tgtdir>
 [options]:
 --cn-syl-dict-dir                                        # value, "$cn_syl_dict_dir"
 [steps]:
 step01, prepare alignment for the target english training data
 step05, making en-word-to-cn-syl-map file from ctm files
 [examples]:
 
 $0 --steps 1 --lmwt-for-lat $lmwt_for_lat  \
  --align-overlap-rate $align_overlap_rate \
  --cn-syl-dict-dir $cn_syl_dict_dir \
 /home2/hhx502/seame/data/english-train/16k \
 /home2/hhx502/seame/data/local/dict \
 /home2/hhx502/seame/data/lang /home2/hhx502/seame/exp/nnet5a/dnn \
 /home2/hhx502/seame/cn-syllable/data/lang  /home2/hhx502/seame/cn-syllable/exp/tri4a/graph \
 /home2/hhx502/seame/cn-syllable/exp/nnet5a-tl /home2/hhx502/seame/ebc-merge01

END
}

if [ $# -ne 8 ]; then
  Usage && exit 1
fi
en_data=$1
source_dict_dir=$2
source_lang=$3
source_dnn_dir=$4
cn_lang=$5
cn_graph=$6
cn_dnn_dir=$7
tgtdir=$8


[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
en_word_alidir=$source_dnn_dir/ali-en-train
if [ ! -z $step01 ]; then
  echo "## LOG: $0, step01, ali en word @ `date`"
  steps/nnet/align.sh --cmd "$cmd"  --nj $nj  $en_data/fbank-pitch \
  $source_lang  $source_dnn_dir $en_word_alidir || exit 1
  echo "## LOG: $0, step01, done ('$en_word_alidir') @ `date`"
fi
en_cn_latdir=$cn_dnn_dir/decode-en-train
if [ ! -z $step02 ]; then
  echo "## LOG: $0, step02, decode en data @ `date`"
  steps/nnet/decode.sh --cmd slurm.pl --nj $nj --acwt 0.1 --beam 10 --lattice-beam 8 \
  --max-mem 500000000 --skip-scoring true $cn_graph $en_data/fbank-pitch \
  $en_cn_latdir || exit 1
  echo "## LOG: $0, step02, done ('$en_cn_latdir') @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "## LOG: $0, step03, make en word ctm file"
  ~/w2016/steps/get_train_ctm.sh --cmd run.pl $en_data/fbank-pitch \
  $source_lang $en_word_alidir || exit 1
  echo "## LOG: $0, step03, done"
fi
if [ ! -z $step04 ]; then
  echo "## LOG: $0, step04, make cn decoded ctm file"
  source/egs/swahili/reco2file_and_channel.sh $en_data/fbank-pitch
  source/egs/seame/get_ctm.sh --cmd slurm.pl --min-lmwt $lmwt_for_lat \
  --max-lmwt $lmwt_for_lat   $en_data/fbank-pitch \
  $cn_lang $en_cn_latdir || exit 1
  echo "## LOG: $0, step04, done"
fi
en_word_to_cn_syl_ali_dir=$tgtdir/en_word_to_cn_syl_ali
if [ ! -z $step05 ]; then
  echo "## LOG: $0, step05, do en-word to cn-syllable alignment using ctm files"
  [ -d $en_word_to_cn_syl_ali_dir ] || mkdir $en_word_to_cn_syl_ali_dir
  for f in $en_word_alidir/utt_ctm $en_cn_latdir/score_${lmwt_for_lat}/fbank-pitch.ctm; do
    [ -e $f ] || \
    { echo "## ERROR: $0, file $f expected"; exit 1; }
  done
  cat $en_cn_latdir/score_${lmwt_for_lat}/fbank-pitch.ctm |\
  source/egs/seame/word-to-pron.pl $en_word_alidir/utt_ctm $align_overlap_rate | \
  source/egs/seame/prune-word-to-pron-output-with-frequency.pl "grep -v _man $source_dict_dir/lexicon.txt|" \
  "grep -v _sge $cn_syl_dict_dir/lexicon.txt |" 10 1 0.5 > $en_word_to_cn_syl_ali_dir/en-word-to-cn-syl-map.txt
   cat $en_word_to_cn_syl_ali_dir/en-word-to-cn-syl-map.txt | \
   source/egs/seame/make-en-word-to-cn-IF-lex.pl $cn_syl_dict_dir/lexicon.txt \
   > $en_word_to_cn_syl_ali_dir/en-word-to-IF-lex.txt || exit 1
  [ $? -eq 0 ] || { echo "## ERROR: $0, step05, making en-word-to-cn-syl-map.txt file error"; exit 1; }
  echo "## LOG: $0, step05, done ($en_word_to_cn_syl_ali_dir)"
fi

