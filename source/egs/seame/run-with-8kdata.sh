#!/bin/bash

. path.sh
. cmd.sh 

echo
echo LOG: $0 $@ 
echo

# begin options
steps=
cmd=slurm.pl
nj=20
# end options

function Usage {
 cat <<END
 $0 [options] <data> <tgtdir>
 [options]:
 [steps]:
 [examples]:

 $0 --steps 1 /home2/hhx502/seame/data /home2/hhx502/seame/exp

END
}
. parse_options.sh || exit 1

if [  $# -ne 2 ]; then
  Usage && exit 1
fi

data=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
train_data_16k=$data/train
train_data_8k=$data/train-8k
if [ ! -z $step01 ]; then
  echo "## LOG: $0, step01, make 8k training data"
  for sdata in $data/train $data/low-dev $data/high-dev; do 
    tgtdata=$data/8k-$(basename $sdata)
    [ -d $tgtdata ] || mkdir -p $tgtdata
    cp $sdata/{text,segments,utt2spk,spk2utt,stm} $tgtdata/
    cat $sdata/wav.scp | \
    perl -ane 'chomp; if(/sox/){ s/\-r 16000/\-r 8000/; } else { m/(\S+)\s+(\S+)/g;
    $_ = "$1 /usr/bin/sox $2 -r 8000 -c 1 -b 16 -t wav - downsample |";
    } print "$_\n";' > $tgtdata/wav.scp
  done
  echo "## LOG: $0, step02, done ($data/8k-{train,low-dev,high-dev})"
fi
source_data=$data
if [ ! -z $step02 ]; then
  echo "## LOG: $0, step02, make feature for 8k data @ `date`"
  for sdata in $data/8k-train $data/8k-low-dev $data/8k-high-dev; do
    data=$sdata/fbank-pitch feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 20  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf --pitch-config conf/pitch.conf" \
    $sdata $data $feat || exit 1
    echo "## LOG: $0, done ($data) @ `date`"
  done
  echo "## LOG: $0, step03, done"
fi
dev_data=$source_data/8k-low-dev/fbank-pitch
train_data=$source_data/8k-train/fbank-pitch
lang_dir=$source_data/lang
expdir=$tgtdir
if [ ! -z $step03 ]; then
  echo "## LOG: $0, step03, run nnet @ `date`"
  source/egs/kws2016/run-nnet.sh --steps 3,4 \
  --delta-opts "--delta-order=2" --cmvn-opts "--norm-means=true" \
  --use-partial-data-to-pretrain true \
  --pretraining-rate 0.5 \
  --nnet-pretrain-cmd "/home/hhx502/w2016/steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048"  \
  --nnet-id 8k-nnet5a --graphdir $expdir/tri4a/graph --devdata $dev_data \
  --decodename decode-low-dev \
  $train_data $lang_dir $expdir/tri4a/ali_train  $expdir || exit 1
  echo "## LOG: $0, step03, done nnet @ `date`"
fi 
english_train_data=$source_data/english-train
if [ ! -z $step04 ]; then
  echo "## LOG: $0, step04, select english data"
  [ -d $english_train_data ] || mkdir -p $english_train_data
  cat $source_data/train/text | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; @A = split(/\s+/); $total_en = 0; $total_cn = 0;
    for($i = 1; $i < @A; $i ++){ $w = $A[$i]; if($w =~ /\p{Han}+/){ $total_cn ++; } else { $total_en ++; } }
    $rate = $total_en/($total_cn + $total_en); if ($rate > 0.9) { print $_, "\n"; }' > $english_train_data/text
  awk '{print $1;}'  $english_train_data/text > $english_train_data/uttlist
  mfcc_pitch_16k=$english_train_data/16k/mfcc-pitch
  utils/subset_data_dir.sh --utt-list $english_train_data/uttlist $source_data/train/mfcc-pitch $mfcc_pitch_16k || exit 1
  utils/fix_data_dir.sh $mfcc_pitch_16k
  fbank_pitch_8k=$english_train_data/8k/fbank-pitch
  utils/subset_data_dir.sh --utt-list $english_train_data/uttlist $source_data/8k-train/fbank-pitch $fbank_pitch_8k || exit 1
  utils/fix_data_dir.sh $fbank_pitch_8k
  echo "## LOG: $0, step04, done ($english_train_data)"
fi
cn_train_data=$source_data/cn-train
if [ ! -z $step05 ]; then
  echo "## LOG: $0, step05, select cn data"
  [ -d $cn_train_data ] || mkdir -p $cn_train_data
  selected_hours=$(cat $english_train_data/16k/mfcc-pitch/segments | awk '{x+=$4-$3;}END{print x/3600;}')
  cat $source_data/train/text | \
  perl -e 'use utf8; use open qw(:std :utf8); ($selected_hours, $segments) = @ARGV; open(F, "$segments") or die "## ERROR: cannot open $segments\n";
    while(<F>) {chomp; @A = split(/\s+/); $vocab{$A[0]} = $A[3]- $A[2];} close F;
    $total_sec = 0;
    while(<STDIN>){chomp; @A = split(/\s+/); $total_en = 0; $total_cn = 0;
      for($i = 1; $i < @A; $i ++){$w = $A[$i]; if($w =~ /\p{Han}+/){$total_cn ++;} else{$total_en++;} }
      $cn_rate = $total_cn/($total_en + $total_cn);
      if($cn_rate > 0.9 && $total_sec/3600 < $selected_hours) {
        die "## ERROR: unidentified id $A[0]\n" if not exists $vocab{$A[0]};
        $sec = $vocab{$A[0]};
        $total_sec += $sec;
        print "$_\n";
      }
   }
  ' $selected_hours  $source_data/train/segments > $cn_train_data/text
  cat $cn_train_data/text | awk '{print $1;}' > $cn_train_data/uttlist
  mfcc_pitch_16k=$cn_train_data/16k/mfcc-pitch
  utils/subset_data_dir.sh --utt-list $cn_train_data/uttlist $source_data/train/mfcc-pitch $mfcc_pitch_16k || exit 1
  utils/fix_data_dir.sh $mfcc_pitch_16k
  fbank_pitch_8k=$cn_train_data/8k/fbank-pitch
  utils/subset_data_dir.sh --utt-list $cn_train_data/uttlist $source_data/8k-train/fbank-pitch $fbank_pitch_8k || exit 1
  utils/fix_data_dir.sh $fbank_pitch_8k
  echo "## LOG: $0, step05, done ($cn_train_data)"
fi
en_extra_data=$source_data/8k-en-extra-train/fbank-pitch
source_extra_en_data=/home2/hhx502/sg-en-i2r/data/train/fbank-pitch
if [ ! -z $step06 ]; then
  echo "## LOG: $0, step06, make en-extra-train-8k data"
  [ -d $en_extra_data ] || mkdir -p $en_extra_data
  cat $source_extra_en_data/text | utils/shuffle_list.pl | \
  source/egs/seame/subset_data.pl 50 $source_extra_en_data/segments $en_extra_data/uttlist || exit 1
  utils/subset_data_dir.sh --utt-list $en_extra_data/uttlist $source_extra_en_data \
  $en_extra_data || exit 1
  cat $en_extra_data/text | \
  perl -ane 'chomp; m/(\S+)\s+(.*)$/g; print $1, " ", lc $2, "\n";' > $en_extra_data/text-1
  mv $en_extra_data/text-1 $en_extra_data/text
  utils/fix_data_dir.sh $en_extra_data 
  echo "## LOG: $0, step06, done ($en_extra_data)"
fi
cn_extra_data=$source_data/8k-cn-extra-train-8k/fbank-pitch
source_extra_cn_data=/home2/hhx502/ldc-cts2016/hkust-mandarin-cts/data-mix/train/fbank-pitch
if [ ! -z $step07 ]; then
  echo "## LOG: $0, step07, make cn-extra-train-8k data"
  [ -d $cn_extra_data ] || mkdir -p $cn_extra_data
  cat $source_extra_cn_data/text | utils/shuffle_list.pl | \
  source/egs/seame/subset_data.pl 50 $source_extra_cn_data/segments $cn_extra_data/uttlist || exit 1
  utils/subset_data_dir.sh --utt-list $cn_extra_data/uttlist $source_extra_cn_data \
  $cn_extra_data || exit 1
  utils/fix_data_dir.sh $cn_extra_data 
  echo "## LOG: $0, step07, done ($cn_extra_data)"
fi
en_merge_data=$source_data/8k-merge/en-merge/fbank-pitch
sdata1=$source_data/8k-train/fbank-pitch
if [ ! -z $step08 ]; then
  echo "## LOG: $0, step08, merge english data"
  sdata2=$en_extra_data
  utils/combine_data.sh $en_merge_data $sdata1 $sdata2 
  utils/fix_data_dir.sh $en_merge_data
  sdata2=$cn_extra_data
  cn_merge_data=$source_data/8k-merge/cn-merge/fbank-pitch
  utils/combine_data.sh $cn_merge_data $sdata1 $sdata2
  utils/fix_data_dir.sh $cn_merge_data

  cn_en_merge_data=$source_data/8k-merge/cn-en-merge/fbank-pitch
  utils/combine_data.sh $cn_en_merge_data $cn_merge_data $en_merge_data
  utils/fix_data_dir.sh $cn_en_merge_data
  echo "## LOG: $0, step08, done ($en_merge_data)"
fi
if [ ! -z $step09 ]; then
  echo "## LOG: $0, step09, ali data @ `date`"
  dnn_dir=/home2/hhx502/seame/exp/8k-nnet5a/dnn
  for x in en-merge cn-merge cn-en-merge; do
    data=$source_data/8k-merge/$x/fbank-pitch
    steps/nnet/align.sh --cmd run.pl --nj 1 --use-gpu yes \
    $data $source_data/lang  $dnn_dir \
    $dnn_dir/ali-$x || exit 1
  done
  echo "## LOG: $0, step09, done @ `date`"
fi
if [ ! -z $step10 ]; then
  echo "## LOG: $0, step10, make mfcc-pitch for 8k data @ `date`"
  for sdata in $source_data/8k-train $source_data/8k-low-dev $source_data/8k-high-dev; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 40  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc.conf --pitch-config conf/pitch.conf" \
    $sdata $data $feat || exit 1
    utils/fix_data_dir.sh $data
    echo "## LOG: $0, done ($data) @ `date`"
  done
  echo "## LOG: $0, step10, done"
fi
if [ ! -z $step11 ]; then
  echo "## LOG: $0 ,step11, mfcc-pitch for 8k data @ `date`"
  for sdata in $source_data/8k-merge/cn-merge/fbank-pitch $source_data/8k-merge/en-merge/fbank-pitch \
  $source_data/8k-merge/cn-en-merge/fbank-pitch; do
    data=$(dirname $sdata)/mfcc-pitch;  feat=$data/feat/mfcc-pitch
    source/egs/swahili/make_feats.sh --cmd run.pl --nj 40  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc.conf --pitch-config conf/pitch.conf" \
    $sdata $data $feat || exit 1
    utils/fix_data_dir.sh $data
  done
  echo "## LOG: $0, step11, done"
fi
train_data=$source_data/8k-merge/cn-en-merge
dev_data=$source_data/8k-low-dev
lang_dir=$source_data/lang-sil
expdir=/home2/hhx502/seame/cn-en-8k-exp
nj=40
if [ ! -z $step12 ]; then
  echo "## step12: gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd "$cmd" --nj $nj \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 6000 --pdf-num 90000 \
  --devdata $dev_data/mfcc-pitch \
  $train_data/mfcc-pitch $lang_dir $expdir || exit 1
  echo "## step12: gmm-hmm done `date`"
fi
if [ ! -z $step13 ]; then
  echo "## step13: dnn-hybrid @ `date`"
  source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 \
  --delta-opts "--delta-order=2" --cmvn-opts "--norm-means=true" \
  --use-partial-data-to-pretrain true \
  --pretraining-rate 0.5 \
  --nnet-pretrain-cmd "/home/hhx502/w2016/steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048"  \
  --nnet-id nnet5a --graphdir $expdir/tri4a/graph --devdata $dev_data/fbank-pitch \
  $train_data/fbank-pitch $lang_dir $expdir/tri4a/ali_train  $expdir || exit 1

  echo "## step13: dnn-hybrid done @ `date`"
fi
