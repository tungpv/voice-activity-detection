#!/usr/bin/perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  print STDERR "\n Example: cat ctm | $0  0.03 silence-word\n\n"; 
  exit 1;
}
my ($min_dur, $silence_word) = @ARGV;
print STDERR "## LOG: $0 stdin expected\n";
my $prev_utt_id = ""; 
my $utt = "";
my %uniq_vocab = ();
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  if($prev_utt_id ne $A[0]) {
    if(exists $uniq_vocab{$A[0]}) {
      die "## ERROR: $0, duplicated utterance $A[0]\n";
    }
    $uniq_vocab{$A[0]} ++;
    $prev_utt_id = $A[0];
    if ($utt ne "") {
      print "$utt\n";
    }
    $utt = $A[0];
  } else {
    my $word_dur = $A[3];
    my $word = $A[4];
    if($word eq "<eps>") {
      $word = $silence_word;
      if($word_dur > $min_dur) {
        $utt .= " $silence_word";
      } else {
        print STDERR "## WARNING: $0, skipping $_\n";
      }
    } else {
      $utt .= " $word";
    }
  } 
}
if ($utt ne "") {
  print "$utt\n";
}
print STDERR "## LOG: $0 stdin ended\n";
