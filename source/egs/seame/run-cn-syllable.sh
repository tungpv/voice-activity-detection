#!/bin/bash

. path.sh
. cmd.sh 

echo
echo $0 $@
echo

# begin options
cmd=slurm.pl
nj=40
steps=
cn_syl_dict=/data/users/hhx502/ldc-cts2016/hkust-mandarin-tele-trans-ldc2005t32/data/local/dict/if.syl.dct-utf8
source_dict_dir=/home2/hhx502/seame/data/local/dict
silence_word_csl="<sss>,<noise>:<oov>,<unk>:<vns>,<v-noise>:SIL,<silence>"
dev_data=/home2/hhx502/seame/data/low-dev
# end options

function Usage {
 cat <<END

 Usage: $0 [options] <source_data_dir> <tgtdir>
 [options]:
 --steps                        # value, "$steps"
 --cmd                          # value, "$cmd"
 --nj                           # value, $nj
 [examples]:

 $0 --steps 1 --cmd "$cmd" --nj $nj --silence-word-csl "$silence_word_csl"   --cn-syl-dict $cn_syl_dict \
 --source-dict-dir $source_dict_dir \
 --dev-data $dev_data \
 /home2/hhx502/seame/data/cn-train/16k/fbank-pitch \
 /home2/hhx502/seame/data/lang /home2/hhx502/seame/exp/nnet5a/dnn \
  /home2/hhx502/seame/cn-syllable

END
}

. parse_options.sh || exit 1

if [ $# -ne 4 ]; then
  Usage && exit 1
fi
fbank_data=$1
source_lang_dir=$2
source_mdl_dir=$3
tgtdir=$4

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
data=$tgtdir/data
ali_dir=$source_mdl_dir/ali-cn-train
if [ ! -z $step01 ]; then
  echo "## LOG: $0, step01, ali data ($fbank_data) @ `date`"
  steps/nnet/align.sh --cmd "$cmd" --nj $nj \
  $fbank_data $source_lang_dir $source_mdl_dir  $ali_dir || exit 1
  echo "## LOG: $0, step01, done ($ali_dir) @ `date`"
fi
if [ ! -z $step02 ]; then
  echo "## LOG: $0, step02, ali to pron for ($fbank_data) @ `date`"
  steps/get_prons.sh --cmd "$cmd"  $fbank_data $source_lang_dir $ali_dir || exit 1
  echo "## LOG: $0, step02, done ($ali_dir) @ `date`"
fi
fbsyl_train_data=$data/train/fbank-pitch
mfcsyl_train_data=$data/train/mfcc-pitch
dict_dir=$data/local/dict
if [ ! -z $step03 ]; then
  echo "## LOG: $0, step03, prepare syllable data ($fbsyl_train_data)"
  [ -d $fbsyl_train_data ] || mkdir -p $fbsyl_train_data
  [ -d $dict_dir ] || mkdir -p $dict_dir
  cat $ali_dir/pron_perutt_nowb.txt | \
  source/egs/seame/get_syllable_text_from_pron_perutt_nowb.pl $cn_syl_dict $dict_dir  $(dirname $fbsyl_train_data)/text
  cp $fbank_data/* $fbsyl_train_data/
  cp $(dirname $fbsyl_train_data)/text $fbsyl_train_data/
  utils/fix_data_dir.sh $fbsyl_train_data
  source_mfcc=/home2/hhx502/seame/data/cn-train/16k/mfcc-pitch
  [ -d $mfcsyl_train_data ] || mkdir $mfcsyl_train_data 
  echo "## LOG: $0, step03, prepare mfc data"
  cp $source_mfcc/* $mfcsyl_train_data/
  cp $fbsyl_train_data/text $mfcsyl_train_data/
  utils/fix_data_dir.sh $mfcsyl_train_data 
  echo "## LOG: $0, step03, done ('$fbsyl_train_data', '$dict_dir')"
fi
lang=$data/lang
if [ ! -z $step04 ]; then
  echo "## LOG: $0, step04, prepare dict &lang "
  echo "$silence_word_csl" | \
  perl -ane 'chomp; @A = split(/:/); for($i = 0; $i < @A; $i++) { @B = split(",", $A[$i]); die "## ERROR: illegal silence word,  ", join(" ", @B), "\n" if @B != 2;
    print "$B[1]\t$B[0]\n"; }' | cat - $dict_dir/lexicon_src.txt  > $dict_dir/lexicon.txt
  cat $dict_dir/lexicon_src.txt | \
  perl -ane 'chomp; @A = split(/\s+/); shift @A; for($i = 0; $i < @A; $i ++){ print "$A[$i]\n"; }' | sort -u > $dict_dir/nonsilence_phones.txt
  echo "$silence_word_csl" | \
  perl -ane 'chomp; @A = split(/:/); for($i = 0; $i < @A; $i ++){ @B = split(",", $A[$i]);  die "## ERROR: illegal silence word,  ", join(" ", @B), "\n" if @B != 2;
  print "$B[0]\n"; }' > $dict_dir/silence_phones.txt
  echo "SIL" > $dict_dir/optional_silence.txt
  echo -n > $dict_dir/extra_questions.txt
  utils/validate_dict_dir.pl $dict_dir 
  utils/prepare_lang.sh $dict_dir "<unk>" $lang/tmp $lang
  echo "## LOG: $0, step04, done with dict & lang ('$dict_dir', '$lang')"
fi
lmdir=$data/local/lm-bigram
if [ ! -z $step05 ]; then
  echo "## LOG: $0, step05, make unigram and fst grammar"
  total_line=$(wc -l < $mfcsyl_train_data/text)
  validate_line=$(perl -e '($total, $percent) = @ARGV; print int($total*$percent);' $total_line 0.05)
  echo "## LOG: $0, step05, total_line=$total_line, validate_line=$validate_line"
  [ -d $lmdir ] || mkdir -p $lmdir 
  cat $dict_dir/lexicon.txt | \
  awk '{print $1; }' | gzip -c > $lmdir/vocab.gz
  cat $mfcsyl_train_data/text | \
  utils/shuffle_list.pl | \
  source/egs/seame/make_train_dev_text.pl $validate_line  $lmdir
  source/egs/kws2016/georgian/train-srilm-v2.sh --lm-order-range "2 2" \
  --steps 1,2 --cutoff-csl "2,00,01" $lmdir
  ngram -lm $lmdir/lm.gz -order 1 -write-lm $lmdir/unigram.gz
  ~/w2016/local/arpa2G.sh $lmdir/unigram.gz  $lang  $lang
  echo "## LOG: $0, step05, done"
fi
ali_dev_dir=$source_mdl_dir/ali-low-dev
if [ ! -z $step06 ]; then
  echo "## LOG: $0, step06, ali data ($fbsyl_dev_data)"
  max_nj=$(wc -l < $dev_data/fbank-pitch/spk2utt)
  [ $nj -gt $max_nj ] && nj=$max_nj
  steps/nnet/align.sh --cmd "$cmd" --nj $nj \
  $dev_data/fbank-pitch $source_lang_dir $source_mdl_dir  $ali_dev_dir || exit 1
  echo "## LOG: $0, step06, done ($ali_dev_dir)"
fi
if [ ! -z $step07 ]; then
  echo "## LOG: $0, step07, ali to pron"
  steps/get_prons.sh --cmd "$cmd"  $dev_data/fbank-pitch $source_lang_dir $ali_dev_dir || exit 1
  echo "## LOG: $0, step07, done"
fi
fbsyl_dev_data=$data/low-dev/fbank-pitch
mfcsyl_dev_data=$data/low-dev/mfcc-pitch
if [ ! -z $step08 ]; then
  echo "## LOG: $0, step08, prepare dev data"
  [ -d $fbsyl_dev_data ] || mkdir -p $fbsyl_dev_data
  [ -d $mfcsyl_dev_data ] || mkdir -p $mfcsyl_dev_data
  cat $ali_dev_dir/pron_perutt_nowb.txt | \
  source/egs/seame/get_syllable_text_from_pron_perutt_nowb.pl $cn_syl_dict $dict_dir  $(dirname $fbsyl_dev_data)/text
  cp $dev_data/fbank-pitch/* $fbsyl_dev_data/
  cp $data/low-dev/text $fbsyl_dev_data/
  utils/fix_data_dir.sh $fbsyl_dev_data
  cp $dev_data/mfcc-pitch/* $mfcsyl_dev_data/
  cp $data/low-dev/text $mfcsyl_dev_data/
  utils/fix_data_dir.sh $mfcsyl_dev_data
  echo "## LOG: $0, step08, done "
fi 
expdir=$tgtdir/exp
train_data=$data/train
dev_data=$data/low-dev
if [ ! -z $step09 ]; then
  echo "## LOG: $0, step09, train gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd "$cmd" --nj $nj \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 2000 --pdf-num 30000 \
  --devdata $dev_data/mfcc-pitch \
  $train_data/mfcc-pitch $lang $expdir || exit 1
  echo "## LOG: $0, step09, done ('$expdir') @ `date`"
fi
if [ ! -z $step10 ]; then
  echo "## LOG: $0, step10, train dnn @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2,3 \
  --devdata $dev_data/fbank-pitch \
  --decodename decode-low-dev \
  --graphdir $expdir/tri4a/graph \
  $train_data/fbank-pitch $lang $source_mdl_dir \
  $expdir/tri4a/ali_train  $expdir/nnet5a-tl  || exit 1
  echo "## LOG: $0, step10, done @ `date`"
fi
