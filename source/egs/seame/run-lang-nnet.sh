#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo 

# begin options
steps=
cmd="slurm.pl --exclude=node02,node05"
nj=40
source_nnet=/home2/hhx502/seame/exp/nnet5a/dnn
learn_rate=0.008
validating_rate=0.1
copy_feats_tmproot=/local/hhx502

# end options

function Usage {
cat<<END
 Usage: $0 [options] <data> <dictdir> <langd> <alidir> <tgtdir>
 [options]:
 [steps]:
 --source-nnet                             # value, $source_nnet
 --learn-rate                              # value, $learn_rate
 --validating-rate                         # value, $validating_rate
 --copy-feats-tmproot                      # value, "$copy_feats_tmproot"

 [examples]:
 
 $0 --steps 1 --source-nnet $source_nnet \
 /home2/hhx502/seame/data/train/fbank-pitch /home2/hhx502/seame/data/local/dict  /home2/hhx502/seame/data/lang \
 /home2/hhx502/seame/exp/tri4a/ali_train /home2/hhx502/seame/exp/lang-dnn-feat-extractor 
 
END
}

. parse_options.sh || exit 1

if [ $# -ne 5 ]; then
  Usage && exit 1
fi

data=$1
dictdir=$2
lang=$3
alidir=$4
tgtdir=$5

[ -d $tgtdir ] || mkdir -p $tgtdir

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  echo "## $0: step01, make phone to language class map file"
  cat $dictdir/nonsilence_phones.txt | \
  perl -ane 'chomp; if(/_sge/){ print "$_ 1\n"; }
    elsif(/_man/) {print "$_ 2\n"; } 
    else { die "## ERROR: unknown phone $_\n"; }' > $dictdir/phone2class || exit 1
  cat $dictdir/silence_phones.txt | \
  perl -ane 'chomp; print "$_ 0\n";' >> $dictdir/phone2class || exit 1
  cat $lang/phones.txt | \
  egrep -v '#|<eps>' | \
  perl -pe 's/_[BISE]//g;' | \
  perl -e '($phone2class) = @ARGV; open(F, "$phone2class") or die "## ERROR: $phone2class cannot open\n";
    while(<F>) {chomp; m/(\S+)\s+(\S+)/ or next; $vocab{$1} = $2; } close F;
    while(<STDIN>) { chomp; m/(\S+)\s+(\S+)/ or next;  if(not exists $vocab{$1}){ die "## unknown phone $1\n";} else{
      print "$2 $vocab{$1}\n"; } }' $dictdir/phone2class > $lang/phone2class || exit 1
  echo "## $0: step01, done('$dictdir/phone2class', '$lang/phone2class')" 
fi
label_dir=$tgtdir/labels
if [ ! -z $step02 ]; then
  echo "## $0: step02, ali to label-class"
  [ -d $label_dir ] || mkdir -p $label_dir
  ali-to-phones $alidir/final.mdl  "ark:gzip -cd $alidir/ali.*.gz|" "ark,t:|source/egs/seame/phone2class.pl $lang/phone2class| gzip -c >  $label_dir/label.gz"  || exit 0
  echo "## $0: step02, done '$label_dir/label.gz'"
fi
train=$tgtdir/train
valid=$tgtdir/valid
if [ ! -z $step03 ]; then
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $train \
  $data  $valid || exit 1
fi
nnetdir=$tgtdir/dnn
if [ ! -z $step04 ]; then
  echo "## $0: step04, init nnet @ `date`"
  num_tgt=$(cat $lang/phone2class| awk '{print $2;}'| sort -u | wc -l)
  front_nnet="nnet-copy --remove-last-layers=2 $source_nnet/final.nnet -|" 
  hid_dim=$(nnet-info "$front_nnet" | \
                  grep output-dim | tail -1 |perl -pe 's/.*output-dim//g; m/(\d+)/g; $_=$1;') || \
  { echo "ERROR, hid_dim error"; exit 1; }
  [ -d $nnetdir ] || mkdir -p $nnetdir
  soft_max_proto=$nnetdir/soft-mat.proto
  soft_max_init=$nnetdir/soft-max.init
  utils/nnet/make_nnet_proto.py $hid_dim $num_tgt 0  $hid_dim >$soft_max_proto || exit 1 
  log=$nnetdir/sotf-max-initialize.log
  nnet-initialize $soft_max_proto $soft_max_init 2>$log || { cat $log; exit 1; }
  
  nnet_init=$nnetdir/nnet.init
  nnet-concat "$front_nnet" $soft_max_init $nnet_init 2> $nnetdir/nnet-init.log
  [ $? -ne 0 ] && { echo "## ERROR: $0, nnet-concat error"; exit 1;  }
  feature_transform=$source_nnet/final.feature_transform
  
  steps/nnet/train.sh --learn-rate 0.008 --feature-transform $feature_transform \
  --copy-feats-tmproot $copy_feats_tmproot \
  --labels "ark:gzip -cd $label_dir/label.gz|" \
  --train-tool nnet-train-frmshuff \
  ${scheduler_opts:+ --scheduler-opts "$scheduler_opts"} \
  ${train_tool_opts:+ --train-tool-opts "$train_tool_opts"} \
  --hid-layers 0 --nnet-init $nnet_init $train $valid dummy  dummy \
  dummy $nnetdir || exit 1

  echo "## $0: step04, done @ `date`"
fi
