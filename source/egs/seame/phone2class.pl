#!/usr/bin/perl
use strict;
use warnings;

my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  print STDERR "\nUsage: $0 <phon2class map>\n";
  exit 1;
}
my ($phone2class) = @ARGV;

open (F, "$phone2class") or die "## ERROR: $0, $phone2class cannot open\n";
my %vocab = ();
while(<F>) {
  chomp;
  m/(\d+)\s+(\d+)/ or next;
  $vocab{$1} = $2;
}
close F;
print STDERR "## $0: stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  my $utt = $A[0];
  for(my $i = 1; $i < @A; $i ++) {
    my $phoneId = $A[$i];
    if (not exists $vocab{$phoneId}) {
      die "## ERROR: $0, unknown phoneId $phoneId\n";
    }
    my $s = sprintf("[ %d 1 ]", $vocab{$phoneId});
    $utt .= " $s";
  }
  print "$utt\n";
}
print STDERR "## $0: stdin ended\n";
