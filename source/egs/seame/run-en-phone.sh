#!/bin/bash 

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
steps=
cmd=slurm.pl
nj=20
dev_data=/home2/hhx502/seame/data/low-dev
ali_dev_dir=/home2/hhx502/seame/exp/nnet5a/dnn/ali-low-dev
devname=low-dev
phone2word_map_csl="<sss>,<noise>:<oov>,<unk>:<vns>,<v-noise>:SIL,<silence>"
source_dict_dir=/home2/hhx502/seame/data/local/dict
# end options
function Usage {
 cat<<END
 
 Usage: $0 [options] <source_data_dir> <source_lang_dir> <source-gmm-dir> <source-dnn-dir> <tgtdir>
 [options]:
 --ali-dev-dir                                            # value, "$ali_dev_dir"
 [steps]:
 [examples]:
 
 $0 --steps 1 --ali-dev-dir $ali_dev_dir --devname $devname \
 /home2/hhx502/seame/data/english-train/16k /home2/hhx502/seame/data/lang \
 /home2/hhx502/seame/exp/tri4a \
 /home2/hhx502/seame/exp/nnet5a/dnn /home2/hhx502/seame/en-phone 

END
}

. parse_options.sh || exit 1

if [ $# -ne 5 ]; then
  Usage && exit 1
fi

source_data_dir=$1
source_lang_dir=$2
source_gmm_dir=$3
source_dnn_dir=$4
tgtdir=$5

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

localdir=$tgtdir/local
datadir=$tgtdir/data
expdir=$tgtdir/exp

for x in $localdir $datadir $expdir; do
  [ -d $x ] || mkdir -p $x
done
ali_train_dir=$source_dnn_dir/ali_train
if [ ! -z $step01 ]; then
  echo "## align data @ `date`"
  max_nj=$(wc -l < $source_data_dir/fbank-pitch/spk2utt)
  [ ! -z $max_nj ] && [ $nj -gt $max_nj ] && nj=$max_nj 
  steps/nnet/align.sh --cmd "$cmd" --nj $nj \
  $source_data_dir/fbank-pitch $source_lang_dir $source_dnn_dir $ali_train_dir || exit 1
  echo "## done ('$ali_train_dir')"
fi
sdata=$source_data_dir
train_data_csl="$sdata/mfcc-pitch,train/mfcc-pitch:$sdata/fbank-pitch,train/fbank-pitch"
if [ ! -z $step02 ]; then
  echo "## make phone training data"
  source/egs/ss-apsipa/make-phone-data.sh --nj $nj --cmd "$cmd" \
    --steps 1,2,3 --dataname train --ali-dir $ali_train_dir \
    --phone2word-map-csl "$phone2word_map_csl" \
    --data-csl "$train_data_csl" \
    data-dummy $source_lang_dir sdir-dummy $datadir || exit 1

  echo "## done with phone training data preparation"
fi
sdata=$dev_data
dev_data_csl="$sdata/mfcc-pitch,low-dev/mfcc-pitch:$sdata/fbank-pitch,low-dev/fbank-pitch"
if [ ! -z $step03 ]; then
  echo "## LOG: $0, step03, make phone dev data"
  source/egs/ss-apsipa/make-phone-data.sh --nj $nj --cmd "$cmd" \
    --steps 1,2,3 --dataname $devname --ali-dir $ali_dev_dir \
    --phone2word-map-csl "$phone2word_map_csl" \
    --data-csl "$dev_data_csl" \
    data-dummy $source_lang_dir sdir-dummy $datadir || exit 1
  echo "## LOG: $0, step03, done ('$datadir/$devname')"
fi
phone_dict_dir=$localdir/dict
phone_lang=$datadir/lang
if [ ! -z $step04 ]; then
  echo "## LOG: $0, step04, make phone lang"
  phone_text=$datadir/train/fbank-pitch/text
  source/egs/seame/prepare_phone_lang.sh "$phone2word_map_csl" $phone_text $source_dict_dir $localdir/dict $phone_lang || exit 1
  echo "## LOG: $0, step04, done ('$phone_lang')"
fi
lmdir=$localdir/lm
if [ ! -z $step05 ]; then
  echo "## LOG: $0, step05, make phone bigram lm"
  [ -d $lmdir ] || mkdir -p $lmdir 
  cat $phone_dict_dir/lexicon.txt | \
  awk '{print $1; }' | gzip -c > $lmdir/vocab.gz
  cat $datadir/train/fbank-pitch/text | \
  cut -d' ' -f2- | sed 's#<silence>##g; s#<unk>##g' | \
  gzip -c > $lmdir/train-text.gz
  cat $datadir/low-dev/fbank-pitch/text | \
  cut -d' ' -f2- | sed 's#<silence>##g; s#<unk>##g' | \
  gzip -c > $lmdir/dev-text.gz
  source/egs/kws2016/georgian/train-srilm-v2.sh --lm-order-range "2 2" \
  --steps 1,2 --cutoff-csl "2,00,01" $lmdir
  ~/w2016/local/arpa2G.sh $lmdir/lm.gz  $phone_lang  $phone_lang
  echo "## LOG: $0, step05, done ('$lmdir')"
fi
train_data=$datadir/train
dev_datat=$datadir/low-dev
lang=$phone_lang
if [ ! -z $step09 ]; then
  echo "## LOG: $0, step09, train gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd "$cmd" --nj $nj \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 2000 --pdf-num 30000 \
  --devdata $dev_data/mfcc-pitch \
  $train_data/mfcc-pitch $lang $expdir || exit 1
  echo "## LOG: $0, step09, done ('$expdir') @ `date`"
fi
if [ ! -z $step10 ]; then
  echo "## LOG: $0, step10, train dnn @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2,3 \
  --devdata $dev_data/fbank-pitch \
  --decodename decode-low-dev \
  --graphdir $expdir/tri4a/graph \
  $train_data/fbank-pitch $lang $source_dnn_dir \
  $expdir/tri4a/ali_train  $expdir/nnet5a-tl  || exit 1
  echo "## LOG: $0, step10, done @ `date`"
fi
