#!/bin/bash

echo
echo $0 $@
echo 

. path.sh
. cmd.sh

# begin options
steps=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <source-data-dir>
 [options]:
 [steps]:
 [examples]:
 $0 --steps 1  /home2/hhx502/seame/data

END
}

if [ $# -ne 1 ]; then
  Usage && exit 1
fi

source_data=$1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

source_lexicon=/home2/hhx502/seame/data/local/dict/lexicon.txt
dict=/home2/hhx502/seame/data/local/dict-sil
lang_dir=/home2/hhx502/seame/data/lang-sil
if [ ! -z $step01 ]; then
  echo "## LOG: $0, step01, prepare dict"
  [ -d $dict ] || mkdir -p $dict
  echo -e "<silence>\tSIL" | cat - $source_lexicon > $dict/lexicon.txt
  cp $(dirname $source_lexicon)/{extra_questions.txt,nonsilence_phones.txt,optional_silence.txt,silence_phones.txt} \
  $dict/
  utils/validate_dict_dir.pl $dict		  
  utils/prepare_lang.sh $dict "<unk>" $lang_dir/tmp $lang_dir
  echo  "## LOG: $0, step01, done ($dict)"
fi
mfcc_train_data=/home2/hhx502/seame/data/train/mfcc-pitch
ali_dir=/home2/hhx502/seame/exp/tri4a/ali_train
new_mfcc_train_data=/home2/hhx502/seame/data/train-sil/mfcc-pitch
fbank_train_data=/home2/hhx502/seame/data/train/fbank-pitch
new_fbank_train_data=/home2/hhx502/seame/data/train-sil/fbank-pitch
if [ ! -z $step02 ]; then
  echo "## LOG: $0, step02, make ctm file"
  [ -d $new_mfcc_train_data ] || mkdir -p $new_mfcc_train_data
  steps/get_train_ctm.sh --print-silence true --only-utterance-ctm true $mfcc_train_data \
  /home2/hhx502/seame/data/lang $ali_dir || exit 1
  tmpdir=$(dirname $new_mfcc_train_data)
  gzip -cd $ali_dir/ctm.*.gz | \
  source/egs/seame/ctm-to-text.pl 0.03 "<silence>" > $tmpdir/text 2>/dev/null
  cat  $tmpdir/text | \
  awk '{print $1;}' > $tmpdir/uttlist
  utils/subset_data_dir.sh --utt-list $tmpdir/uttlist \
  $mfcc_train_data  $new_mfcc_train_data
  cp $tmpdir/text $new_mfcc_train_data/
  utils/fix_data_dir.sh $new_mfcc_train_data
  utils/subset_data_dir.sh --utt-list $tmpdir/uttlist \
  $fbank_train_data  $new_fbank_train_data
  cp $tmpdir/text $new_fbank_train_data/
  utils/fix_data_dir.sh $new_fbank_train_data
  echo "## LOG: $0, step02, done ($new_mfcc_train_data)"
fi
lmdir=/home2/hhx502/seame/data/local/lm-sil
lmtype=3gram-mincount
arpa_lm_dir=$lmdir/$lmtype
train_data=$new_mfcc_train_data
dev_data=/home2/hhx502/seame/data/low-dev/mfcc-pitch
if [ ! -z $step03 ]; then
  echo "## LOG: $0, step03, prepare lm"
  cp $train_data/text $train_data/text-backup
  cat $train_data/text-backup | perl -pe 's/<silence>//g;' > $train_data/text
  source/egs/fisher-english/train-kaldi-lm.sh --lmtype $lmtype  $dict/lexicon.txt \
  $train_data $dev_data  $lmdir
  cp $train_data/text-backup $train_data/text
  echo "## LOG: $0, step03, done "
fi
