#!/bin/bash

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
cmd=slurm.pl
nj=40
# end options

function Usage {
 cat<<EOF
 
 Usage: $0 [options] <datadir> <tgtdir>
 [options]:
 [examples]:
 


EOF
}

. parse_options.sh || exit 1


if [ $# -ne 2 ]; then
  Usage && exit 1
fi


