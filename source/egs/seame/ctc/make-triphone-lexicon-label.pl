#!/usr/bin/perl

use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 5) {
  die "\nExample: ali-to-phones final.mdl \"ark:gzip -cd ali.1.gz|\" ark,t:- |utils/int2sym.pl -f 2- phones.txt | $0  10 5000  srcdictdir  tgtdictdir labeldir \n\n";
}

my ($minPhoneOccurrence, $totalPhysicalNum, $srcdictdir, $tgtdictdir, $labeldir) = @ARGV;

## begin sub
sub Init {
  my ($dictDir, $optionalSilencePhone, $silencePhoneVocab, $phoneMapVocab, $contextPhoneVocab, $lexFile) = @_;
  foreach my $x ("optional_silence.txt", "nonsilence_phones.txt", "silence_phones.txt", "lexicon.txt") {
    my $sourceFile = "$dictDir/$x";
    if( ! -e $sourceFile) {
      die "## ERROR (Init, $0): file $sourceFile expected\n";
    }
  }
 # step01: get optional silence phone string
  open(F, "$dictDir/optional_silence.txt") or die "## ERROR (Init, $0): failed to read optional_silence.txt\n";
  while(<F>){chomp; m/(\S+)$/ or next; $$optionalSilencePhone = $1; } close F;
 # step02: make position to non-position phone mapping
 open(F, "cat $dictDir/nonsilence_phones.txt $dictDir/silence_phones.txt |") or die "## ERROR (Init, $0): failed to read nonsilence_phones.txt and silence_phones.txt\n"; 
  while(<F>) {
    chomp;
    m/(\S+)/ or next;
    my $rawPhone = $1;
    $$phoneMapVocab{$rawPhone} = $rawPhone;
    my $singleton = sprintf("%s_S", $rawPhone);
    $$phoneMapVocab{$singleton} = $rawPhone;
    my $initial = sprintf("%s_B", $rawPhone);
    $$phoneMapVocab{$initial} = $rawPhone;
    my $intermediate = sprintf("%s_I", $rawPhone);
    $$phoneMapVocab{$intermediate} = $rawPhone;
    my $ending = sprintf("%s_E", $rawPhone);
    $$phoneMapVocab{$ending} = $rawPhone;
  }
  close F;
  # step03: load silence phones
  open(F, "$dictDir/silence_phones.txt") or die "## ERROR (Init, $0): failed to read silence_phones.txt\n";
  while(<F>) {
    chomp;
    m/(\S+)/ or next;
    $$silencePhoneVocab{$1} ++; 
  }
  close F;
  # step04: make context phone vocab
  open(F, "cat $dictDir/silence_phones.txt $dictDir/nonsilence_phones.txt|") or die "## ERROR (Init, $0): failed to read optional_silence.txt and nonsilence_phones.txt\n";
  while(<F>) {
    chomp;
    m/(\S+)/ or next;
    $$contextPhoneVocab{$1} ++;
  }
  close F;
  # step05, get lexFile, not necessary
  $$lexFile = "$dictDir/lexicon.txt";
}
sub IsSilencePhone {
  my ($vocab, $phone) = @_;
  return 1 if exists $$vocab{$phone};
  return 0;
}
sub GetRawPhone {
  my ($vocab, $phone, $rawPhone) = @_;
  die "## ERROR (GetRawPhone, $0): un-registered phone $phone" if not exists $$vocab{$phone};
  $$rawPhone = $$vocab{$phone};
}
sub MakeTriphone {
  my ($leftPhone, $noLeft, $rightPhone, $noRight, $curPhone, $silencePhoneVocab, $curTriphone, $leftBiphone, $rightBiphone) = @_;
  $$leftBiphone = "";
  $$rightBiphone = "";
  $$curTriphone = "";
  if(IsSilencePhone($silencePhoneVocab, $curPhone) == 1) {
    $$curTriphone = $curPhone;
    return;
  }
  if($noLeft ==1 && $noRight == 1) {
    $$curTriphone = $curPhone;
    return;
  }
  if ($noLeft == 1) {
    $$curTriphone = sprintf("%s+%s", $curPhone, $rightPhone);
    $$rightBiphone = $$curTriphone;
    return;
  }
  if($noRight == 1) {
    $$curTriphone = sprintf("%s-%s", $leftPhone, $curPhone);
    $$leftBiphone = $$curTriphone;
    return;
  }
  $$leftBiphone = sprintf("%s-%s", $leftPhone, $curPhone);
  $$rightBiphone = sprintf("%s+%s", $curPhone, $rightPhone);
  $$curTriphone = sprintf("%s-%s+%s", $leftPhone, $curPhone, $rightPhone);
}
sub GetTriphoneWithMaxOccurrence {
  my ($vocab, $maxTriphone, $maxOccurrence) = @_;
  $$maxOccurrence = 0;
  foreach my $triphone (keys%$vocab) {
    my $curOcc = $$vocab{$triphone};
    if ($curOcc >= $$maxOccurrence) {
      $$maxOccurrence = $curOcc;
      $$maxTriphone = $triphone;
    }
  }
}
sub IsLeftBiphone {
  my ($phone, $vocab) = @_;
  my @A = ();
  @A = split(/[\-]/, $phone);
  return 0 if(scalar @A != 2);
  die "## ERROR (IsLeftBiphone, $0): unknown phone $A[0]\n" if not exists $$vocab{$A[0]};
  $phone = $A[1];
  return 0 if not exists $$vocab{$phone};
  return 1;
}
sub IsRightBiphone {
  my ($phone, $vocab) = @_;
  my @A = ();
  @A = split(/[\+]/, $phone);
  return 0 if(scalar @A != 2);
  die "## ERROR (IsRightBiphone, $0): unknown phone $A[1]\n" if not exists $$vocab{$A[1]};
  $phone = $A[0];
  return 0 if not exists $$vocab{$phone};
  return 1;
}
sub IsNormalTriphone {
  my ($triphone, $contextPhoneVocab, $leftBiphone, $rightBiphone) = @_;
  my @A = split(/\-/, $triphone);
  my $leftMonophone = $A[0];
  $$rightBiphone = $A[1];
  @A = split(/\+/, $triphone);
  $$leftBiphone = $A[0];
  my $rightMonophone = $A[1];
  return 1 if exists $$contextPhoneVocab{$leftMonophone} && IsRightBiphone($$rightBiphone, $contextPhoneVocab) == 1 &&
  exists $$contextPhoneVocab{$rightMonophone} && IsLeftBiphone($$leftBiphone, $contextPhoneVocab) == 1;
  return 0;
}
sub GetMonophone {
  my ($phone, $vocab, $monophone) = @_;
  $phone =~ s#^.*\-##g;
  $phone =~ s#\+.*$##g;
  die "## ERROR (GetMonophone, $0): monophone $phone not seen\n"
  if not exists $$vocab{$phone};
  $$monophone = $phone;
}
sub SearchOtherLeftBiphone {
  my ($leftBiphone, $occurrence, $contextPhoneVocab, $physicalVocab, $phone2PhysicalVocab) = @_;
  if (IsLeftBiphone($leftBiphone, $contextPhoneVocab) != 1) {
    die "## ERROR (SearchOtherLeftBiphone, $0): $leftBiphone is not a left biphone\n";
  }
  my $monophone;
  GetMonophone($leftBiphone, $contextPhoneVocab, \$monophone);
  foreach my $phone (keys%$contextPhoneVocab) {
    my $newLeftBiphone = sprintf("%s-%s", $phone, $monophone);
    next if not exists $$phone2PhysicalVocab{$newLeftBiphone};
    my $physicalPhone = $$phone2PhysicalVocab{$newLeftBiphone};
    die "## ERROR (SearchOtherLeftBiphone, $0): phone $leftBiphone already got mapped\n" if exists $$phone2PhysicalVocab{$leftBiphone};
    # print STDERR "## LOG (SearchOtherLeftBiphone, $0): triphone=$triphone, physicalPhone=$physicalPhone\n";
    $$phone2PhysicalVocab{$leftBiphone} = $physicalPhone;
    $$physicalVocab{$physicalPhone} += $occurrence;
    return 1;
  }
  return 0;
}
sub SearchOtherRightBiphone {
  my ($rightBiphone, $occurrence, $contextPhoneVocab, $physicalVocab, $phone2PhysicalVocab) = @_;
  if(IsRightBiphone($rightBiphone, $contextPhoneVocab) != 1) {
    die "## ERROR (SearchOtherRightBiphone, $0): $rightBiphone is not a right biphone\n";
  }
  my $monophone;
  GetMonophone($rightBiphone, $contextPhoneVocab, \$monophone);
  foreach my $phone (keys%$contextPhoneVocab) {
    my $newRightBiphone = sprintf("%s+%s", $monophone, $phone);
    my $physicalPhone = $$phone2PhysicalVocab{$newRightBiphone};
    next if not defined $physicalPhone;
    die "## ERROR (SearchOtherRightBiphone, $0): phone $rightBiphone already got mapped\n" if exists $$phone2PhysicalVocab{$rightBiphone};
    # print STDERR "## LOG (SearchOtherRightBiphone, $0): phone triphone=$triphone, physicalPhone=$physicalPhone\n";
    $$phone2PhysicalVocab{$rightBiphone} = $physicalPhone;
    $$physicalVocab{$physicalPhone} += $occurrence;
    return 1;
  }
  return 0;
}
sub SearchOtherLeftTriphone {
  my ($rightBiphone, $triphone, $occurrence, $contextPhoneVocab, $physicalVocab, $phone2PhysicalVocab) = @_;
  if(IsRightBiphone($rightBiphone, $contextPhoneVocab) != 1) {
    die "## ERROR (SearchOtherLeftTriphone, $0): $rightBiphone is not a right-biphone\n";
  }
  foreach my $phone (keys%$contextPhoneVocab) {
    my $curTriphone = sprintf("%s-%s", $phone, $rightBiphone);
    my $physicalPhone = $$phone2PhysicalVocab{$curTriphone};
    if (defined $physicalPhone) {
      # print STDERR "## LOG (SearchOtherLeftTriphone, $0): triphone=$triphone, physicalPhone=$physicalPhone\n";
      $$phone2PhysicalVocab{$triphone} = $physicalPhone;
      $$physicalVocab{$physicalPhone} += $occurrence;
      return 1;
    }
  }
  return 0;
}
sub SearchOtherRightTriphone {
  my ($leftBiphone, $triphone, $occurrence, $contextPhoneVocab, $physicalVocab, $phone2PhysicalVocab) = @_;
  if (IsLeftBiphone($leftBiphone, $contextPhoneVocab) != 1) {
    die "## ERROR (SearchOtherRightTriphone, $0): $leftBiphone is not a leftf-biphone \n";
  }
  foreach my $phone (keys%$contextPhoneVocab) {
    my $curTriphone = sprintf("%s+%s", $leftBiphone, $phone);
    my $physicalPhone = $$phone2PhysicalVocab{$curTriphone};
    if (defined $physicalPhone) {
      # print STDERR "## LOG (SearchOtherRightTriphone, $0): triphone=$triphone, physicalPhone=$physicalPhone\n";
      $$phone2PhysicalVocab{$triphone} = $physicalPhone;
      $$physicalVocab{$physicalPhone} += $occurrence;
      return 1;
    }
  }
  return 0;
}
sub ClusterAllToOne {
  my ($physicalPhone, $vocab, $contextPhoneVocab, $physicalPhoneVocab, $phone2PhysicalVocab, $selectedNum) = @_;

  foreach my $triphone (keys%$vocab) {
    my $leftBiphone; my $rightBiphone;
    my $occurrence = $$vocab{$triphone};
    if (IsLeftBiphone($triphone, $contextPhoneVocab) == 1) {
      if (exists $$phone2PhysicalVocab{$triphone}) {
        $$physicalPhoneVocab{$physicalPhone} += $occurrence;
        next;
        # die "## ERROR ($0): left-biphone: $triphone, mapped: $$phone2PhysicalVocab{$triphone}, intended: $physicalPhone\n";
      }
    } elsif (IsRightBiphone($triphone, $contextPhoneVocab) == 1) {
      $$physicalPhoneVocab{$physicalPhone} += $occurrence;
      next if (exists $$phone2PhysicalVocab{$triphone});
    } elsif (IsNormalTriphone($triphone, $contextPhoneVocab, \$leftBiphone, \$rightBiphone) == 1) {
      next if (exists $$phone2PhysicalVocab{$triphone});
      $$phone2PhysicalVocab{$leftBiphone} = $physicalPhone;
      next if (exists $$phone2PhysicalVocab{$triphone});
      $$phone2PhysicalVocab{$rightBiphone} = $physicalPhone;
    } else {
      die "## ERROR (ClusterAllToOne, $0): unexpected triphone $triphone\n";
    }
    die "## ERROR (ClusterAllToOne, $0): triphone $triphone already mapped\n" if exists $$phone2PhysicalVocab{$triphone};
    $$phone2PhysicalVocab{$triphone} = $physicalPhone;
    $$physicalPhoneVocab{$physicalPhone} += $occurrence;
  }
  $$selectedNum ++;
}
sub HasMoreClusters {
  my ($array, $threshold) = @_;
  my $j = 0;
  my $i = scalar @$array - 1;
  my $totalOcc = 0;
  while($totalOcc < $threshold && $j < $i) {
    $totalOcc += $$array[$j];
    $j ++;
  }
  $j --;
  $totalOcc = 0;
  while($totalOcc < $threshold && 0 <= $i) {
    $totalOcc += $$array[$i];
    $i --;
  }
  $i ++;
  return 1 if $i > $j;
  return 0;
}
sub  TryClusterAllToOne {
  my ($vocab, $contextPhoneVocab, $physicalPhoneVocab, $phone2PhysicalVocab, $threshold, $selectedNum, $physicalPhone, $totalOccurrence) = @_;
  my @A = ();
  my $maxOccurrence = 0;
  $$totalOccurrence = 0;
  foreach my $triphone (keys%$vocab) {
    my $occurrence = $$vocab{$triphone};
    $$totalOccurrence += $occurrence;
    push @A, $occurrence;
    if($occurrence >= $maxOccurrence) {
      $$physicalPhone = $triphone;
      $maxOccurrence = $occurrence;
    }
  }
  if(0) {
    if(not defined $$physicalPhone) {
      my $triphoneNum = scalar (keys%$vocab);
      die "## ERROR (", __LINE__, "): physical phone not defined ", $triphoneNum, "\n";
    }
  }
  return 0 if (scalar @A > 1 && HasMoreClusters(\@A, $threshold) == 1);
  if (exists $$physicalPhoneVocab{$$physicalPhone} or exists $$phone2PhysicalVocab{$$physicalPhone}) {
    die "## ERROR (TryClusterAllToOne, $0): Triphone $$physicalPhone already existed\n";
  }
  ClusterAllToOne($$physicalPhone, $vocab, $contextPhoneVocab, $physicalPhoneVocab, $phone2PhysicalVocab, $selectedNum);
  my $clusteredOcc = $$physicalPhoneVocab{$$physicalPhone};
  if($clusteredOcc != $$totalOccurrence) {
    die "## ERROR (TryClusterAllToOne, $0): total occurrence: $$totalOccurrence, clustered occurrence: $clusteredOcc\n";
  }
  return 1;
}
sub   ClusterPartialToMany {
  my ($vocab, $contextPhoneVocab, $physicalPhoneVocab, $phone2PhysicalVocab, $threshold, $selectedNum) = @_;
  my $totalOcc = 0; my %clusterVocab = (); my $repTriphone;
  my $maxOcc = 0;
  foreach my $triphone (keys%$vocab) {
    if($totalOcc < $threshold) {
      my $occ = $$vocab{$triphone};
      $clusterVocab{$triphone} = $occ;
      if($occ >= $maxOcc) {
        $maxOcc = $occ;
        $repTriphone = $triphone;
      }
      $totalOcc += $occ;
    } else {
      ClusterAllToOne($repTriphone, \%clusterVocab, $contextPhoneVocab, $physicalPhoneVocab, $phone2PhysicalVocab);
      $$selectedNum ++;
      $maxOcc = 0;
      $totalOcc = 0;
    }
  }
  # processing the remaining, if any
  if($maxOcc > 0) {
    ClusterAllToOne($repTriphone, \%clusterVocab, $contextPhoneVocab, $physicalPhoneVocab, $phone2PhysicalVocab);
  }
}
sub DumpContextPhoneVocab {
  my ($vocab) = @_;
  foreach my $phone (keys%$vocab) {
    print STDERR "$phone\n";
  }
}
sub CheckMapped {
  my ($vocab, $phone2PhysicalVocab, $physicalVocab, $total, $mapped) = @_;
  $$total = 0; $$mapped = 0;
  foreach my $phone (keys%$vocab) {
    if(exists $$phone2PhysicalVocab{$phone}) {
      my $physical = $$phone2PhysicalVocab{$phone};
      my $occurrence = $$physicalVocab{$physical};
      warn "## LOG (CheckMapped, ",  __LINE__ ,"): $phone\t$$phone2PhysicalVocab{$phone}\t$occurrence\n";
      $$mapped ++;
    }
    $$total ++;
  }
}
sub DumpPhone2PhoneMapFile {
  my ($vocab, $fname) = @_;
  open(F, "$fname") or die "## ERROR (DumpPhone2PhoneMapeFile, $0): cannot write file $fname";
  foreach my $phone (keys%$vocab) {
    print F "$phone $$vocab{$phone}\n";
  }
}
sub   CheckPhysicalVocab { 
  my ($vocab, $ctxVocab, $phone) = @_;
  my ($leftBiphone, $rightBiphone);
  if(IsLeftBiphone($phone, $ctxVocab) == 1) {
    if(exists $$vocab{$phone}) {
      die "## (CheckPhysicalVocab): $phone is mapped\n";
    }
  } elsif(IsRightBiphone($phone, $ctxVocab) == 1) {
    if(exists $$vocab{$phone}) {
      die "## (CheckPhysicalVocab): $phone is mapped\n";
    }
  } elsif(IsNormalTriphone($phone, $ctxVocab, \$leftBiphone, \$rightBiphone) == 1) {
    if(exists $$vocab{$leftBiphone}) {
      die "## (CheckPhysicalVocab): $leftBiphone is mapped\n";
    } elsif(exists $$vocab{$rightBiphone}) {
      die "## (CheckPhysicalVocab): $rightBiphone is mapped\n";
    }
  } else {
    die "## (CheckPhysicalVocab): unknown phone $phone\n";
  }
}
## end sub

my %silencePhoneVocab = ();
my %contextPhoneVocab = ();
my %phoneMapVocab = ();
my $optionalSilencePhone;
my $lexFile;
Init($srcdictdir, \$optionalSilencePhone, \%silencePhoneVocab, \%phoneMapVocab, \%contextPhoneVocab, \$lexFile);

`[ -d $labeldir ] || mkdir -p $labeldir`;
open(LAB, "|gzip -c > $labeldir/raw-label.gz") or
die "## ERROR (main, $0): cannot open $labeldir/raw-label.gz to write\n";
my %leftBiphoneVocab = ();
my %rightBiphoneVocab = ();
my %isolatedPhoneVocab = ();
print STDERR "## LOG (main, $0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($uttName, $s) = ($1, $2); my $utt = $uttName;
  my $i = 0;
  my @A = split(/\s+/, $s);
  my @T = ();
  MakeCtxTriphone(\@A, \%phoneMapVocab, \%silencePhoneVocab,\@T);
  while($i < scalar @A) {
    my $curPhone;
    GetRawPhone(\%phoneMapVocab, $A[$i], \$curPhone);
    my $leftPhone;
    my $rightPhone;
    my $noLeft = 0;
    my $noRight = 0;
    my $leftBiphone;
    my $rightBiphone;
    if($i > 0) {
      GetRawPhone(\%phoneMapVocab, $A[$i-1], \$leftPhone);
      $noLeft = 1 if(IsSilencePhone(\%silencePhoneVocab, $leftPhone) == 1);
    } else {
      $noLeft = 1;
    }
    if($i + 1 < scalar @A) {
      GetRawPhone(\%phoneMapVocab, $A[$i+1], \$rightPhone);
      # print STDERR "## LOG ($0): rightPhone=$rightPhone\n";
      $noRight = 1 if (IsSilencePhone(\%silencePhoneVocab, $rightPhone) == 1);
    } else {
      $noRight = 1;
    }
    my $curTriphone;
    MakeTriphone($leftPhone, $noLeft, $rightPhone, $noRight, $curPhone, \%silencePhoneVocab, \$curTriphone, \$leftBiphone, \$rightBiphone);
    if($leftBiphone ne "") {
      $leftBiphoneVocab{$leftBiphone}{$curTriphone} ++;
    }
    if($rightBiphone ne "") {
      $rightBiphoneVocab{$rightBiphone}{$curTriphone} ++;
    }
    if($leftBiphone eq ""  and $rightBiphone eq "") {
      $isolatedPhoneVocab{$curTriphone} ++;
    }
    $utt .= " $curTriphone";
    $i ++;
  }
  print LAB "$utt\n";
}
close LAB;
print STDERR "## LOG (main, $0): stdin ended\n";

open(D, "$lexFile") or die "## ERROR (main, $0): lexFile $lexFile cannot open\n";
open(O, ">$tgtdictdir/raw-triphone-lexicon.txt") or die "## ERROR (main, $0): cannot open raw-biphone-lexicon.txt to write\n";

while(<D>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($word, $phones) = ($1, $2);
  my @A = split(/\s+/, $phones);
  if(scalar @A == 1) {
    die "## ERROR ($0): unknown phone $phones\n" if not exists $contextPhoneVocab{$phones};
    print O "$word\t$phones\n";
    $isolatedPhoneVocab{$phones} ++;
  } else {
    my $wordPron = $word;
    for(my $i = 0; $i < scalar @A; $i ++) {
      my $curTriphone;
      my $leftBiphone;
      my $rightBiphone;
      die "## ERROR ($0): unknown phone $A[$i]\n" if not exists $contextPhoneVocab{$A[$i]};
      if($i == 0) {
        die "## ERROR ($0): silence phone $A[$i+1]\n" if exists $silencePhoneVocab{$A[$i+1]};
        MakeTriphone("", 1, $A[$i+1], 0, $A[$i], \%silencePhoneVocab, \$curTriphone, \$leftBiphone, \$rightBiphone);
        $wordPron .= "\t$curTriphone";
        $rightBiphoneVocab{$rightBiphone}{$curTriphone} = 0 if(not exists $rightBiphoneVocab{$rightBiphone}{$curTriphone});
      } elsif($i == scalar @A -1) {
        die "## ERROR ($0): silence phone $A[$i-1]\n" if exists $silencePhoneVocab{$A[$i-1]};
        MakeTriphone($A[$i-1], 0, "", 1, $A[$i], \%silencePhoneVocab, \$curTriphone, \$leftBiphone, \$rightBiphone);
        $wordPron .= " $curTriphone";
        $leftBiphoneVocab{$leftBiphone}{$curTriphone} = 0  if (not exists $leftBiphoneVocab{$leftBiphone}{$curTriphone});
      } else {
        die "## ERROR ($0): silence phone $A[$i-1] or $A[$i+1] ?\n" if (exists $silencePhoneVocab{$A[$i-1]} or exists $silencePhoneVocab{$A[$i+1]});
        MakeTriphone($A[$i-1], 0, $A[$i+1], 0, $A[$i], \%silencePhoneVocab, \$curTriphone, \$leftBiphone, \$rightBiphone);
        $wordPron .= " $curTriphone";
        if(0 && $curTriphone eq "b_sge-d_sge+U_sge") {
	  print STDERR "## debug ($0): curTriphone = $curTriphone, leftBiphone = $leftBiphone, rightBiphone = $rightBiphone !\n";
        }
        $rightBiphoneVocab{$rightBiphone}{$curTriphone} = 0 if(not exists $rightBiphoneVocab{$rightBiphone}{$curTriphone});
        $leftBiphoneVocab{$leftBiphone}{$curTriphone} = 0  if (not exists $leftBiphoneVocab{$leftBiphone}{$curTriphone});
      }
    }
    print O "$wordPron\n";
  }
}
close D;
close O;
print STDERR "## LOG ($0): now we start to cluster context dependent triphone\n";
my %leftToBeMergedVocab = ();
my %rightToBeMergedVocab = ();
my %physicalVocab = ();
my %phone2PhysicalVocab = ();
my $selectedNum = 0;
my $threshold = $minPhoneOccurrence;
my $iterNum  = 0;
while(1) {
  $iterNum ++;
  print STDERR "## LOG ($0): Iter ($iterNum) ...\n";
  foreach my $leftBiphone (keys%leftBiphoneVocab) {
    my ($maxTriphone, $maxOccurrence) = ("", 0);
    GetTriphoneWithMaxOccurrence($leftBiphoneVocab{$leftBiphone}, \$maxTriphone, \$maxOccurrence);
    # print STDERR "## LOG ($0): left-biphone: $leftBiphone, most frequent triphone: $maxTriphone ($maxOccurrence)\n";
    if($maxOccurrence >= $threshold) {
      if (not exists $phone2PhysicalVocab{$leftBiphone}) {
        $phone2PhysicalVocab{$leftBiphone} =  $maxTriphone;
      }
      if(not exists $physicalVocab{$maxTriphone}) {
        $physicalVocab{$maxTriphone} = $maxOccurrence;
        $selectedNum ++;
      }
    }
    foreach my $triphone (keys %{$leftBiphoneVocab{$leftBiphone}}) {
      my $occurrence = $leftBiphoneVocab{$leftBiphone}{$triphone};
      if($occurrence >= $threshold) {
        if (not exists $physicalVocab{$triphone}) {
          $physicalVocab{$triphone} = $occurrence;
          $selectedNum ++;
        }
        $phone2PhysicalVocab{$triphone} = $triphone;
      } else {
        if ($maxOccurrence >= $threshold) {
          $phone2PhysicalVocab{$triphone} = $maxTriphone;
          $physicalVocab{$maxTriphone} += $occurrence;
        } else {
           # print STDERR "## LOG ($0): rare triphone $triphone from left side\n";
           $leftToBeMergedVocab{$leftBiphone}{$triphone} = $occurrence;
        }
      }
    }
  }
  print STDERR "## LOG ($0): leftBiphone finished, phyical triphone number: $selectedNum\n";
  foreach my $rightBiphone (keys%rightBiphoneVocab) {

    my ($maxTriphone, $maxOccurrence) = ("", 0);
    GetTriphoneWithMaxOccurrence($rightBiphoneVocab{$rightBiphone}, \$maxTriphone, \$maxOccurrence);
    if($maxOccurrence >= $threshold) {
      if(not exists $physicalVocab{$maxTriphone}) {
       # print STDERR "## LOG ($0): most frequent triphone: $maxTriphone ($maxOccurrence)\n";
        $physicalVocab{$maxTriphone} = $maxOccurrence; $selectedNum ++;
      }
      if(not exists $phone2PhysicalVocab{$rightBiphone}) {
        $phone2PhysicalVocab{$rightBiphone} = $maxTriphone;
      }
    }
    foreach my $triphone (keys%{$rightBiphoneVocab{$rightBiphone}}) {
      my $occurrence = $rightBiphoneVocab{$rightBiphone}{$triphone};
      if($occurrence >= $threshold) {
        if(not exists $physicalVocab{$triphone}) {
          $physicalVocab{$triphone} = $occurrence;
          $selectedNum ++;
        }
        $phone2PhysicalVocab{$triphone} = $triphone;
      } else {
        if($maxOccurrence >= $threshold) {
          $phone2PhysicalVocab{$triphone} = $maxTriphone;
          $physicalVocab{$maxTriphone} += $occurrence;
        } else {
          # print STDERR "## LOG ($0): rare triphone $triphone from right side\n";
          $rightToBeMergedVocab{$rightBiphone}{$triphone} = $occurrence;
        }
      }
    }
  }
  if($selectedNum <= $totalPhysicalNum) {
    last; # our threshold is appropriate, stop selecting
  } else {  # otherwise, threshold is too small, we will raise it and redo
    my $historySelectedNum = $selectedNum;
    my $historyThreshold = $threshold;
    $selectedNum = 0;
    %leftToBeMergedVocab = ();
    %rightToBeMergedVocab = ();
    %physicalVocab = ();
    %phone2PhysicalVocab = ();
    $threshold ++;  # increase threshold
    print STDERR "## LOG ($0): previous status are (senone, threshold) are ($historySelectedNum, $historyThreshold, ",
                 " now threshold is $threshold\n";
  }
}
if(0) {
  print STDERR "## debug ($0): checking left-to-be-merged\n";
  CheckPhysicalVocab(\%leftToBeMergedVocab, \%contextPhoneVocab, "b_sge-d_sge");
  print STDERR "## debug ($0): checking right-to-be-merged\n";
  CheckPhysicalVocab(\%rightToBeMergedVocab, \%contextPhoneVocab, "d_sge+U_sge");
}
print STDERR "## LOG ($0): physical phone number: $selectedNum\n";
my %monoToBeMergedVocab = ();
foreach my $leftBiphone (keys%leftToBeMergedVocab) {
  foreach my $triphone (keys%{$leftToBeMergedVocab{$leftBiphone}}) {
     my ($curLeftBiphone, $curRightBiphone);
     my $occurrence = $leftToBeMergedVocab{$leftBiphone}{$triphone};
     # print STDERR "## LOG ($0): triphone: $triphone\n";
    if(IsLeftBiphone($triphone, \%contextPhoneVocab) == 1) {
      next if (SearchOtherLeftBiphone($leftBiphone, $occurrence, \%contextPhoneVocab, \%physicalVocab, \%phone2PhysicalVocab) == 1);
    } elsif (IsNormalTriphone($triphone, \%contextPhoneVocab, \$curLeftBiphone, \$curRightBiphone) == 1) {
      # print STDERR "## LOG ($0): triphone: $triphone, left-biphone: $curLeftBiphone, right-biphone: $curRightBiphone\n";
      next if (SearchOtherLeftTriphone($curRightBiphone, $triphone, $occurrence, \%contextPhoneVocab, \%physicalVocab, \%phone2PhysicalVocab) == 1);
      next if (SearchOtherRightTriphone($curLeftBiphone, $triphone, $occurrence, \%contextPhoneVocab, \%physicalVocab, \%phone2PhysicalVocab) == 1);
    } else {
      die "## ERROR ($0): incorrectly classified  triphone $triphone ($leftBiphone)\n";
    }
    if(0) {
      if($triphone eq "b_sge-d_sge+U_sge") {
        die;
      }
    }
    my $monophone;
    GetMonophone($triphone, \%contextPhoneVocab, \$monophone);
    if (exists $monoToBeMergedVocab{$monophone}{$triphone}) {
      die "## ERROR ($0): see an unexpected duplicated case $triphone ($monophone)\n";
    }
    $monoToBeMergedVocab{$monophone}{$triphone} = $leftToBeMergedVocab{$leftBiphone}{$triphone};
  }
}
my $rightToBeMergedNum = keys%rightToBeMergedVocab;
print STDERR "## LOG ($0): rightToBeMergedNum: $rightToBeMergedNum\n";
foreach my $rightBiphone (keys%rightToBeMergedVocab) {
  foreach my $triphone (keys%{$rightToBeMergedVocab{$rightBiphone}}) {
    my ($curLeftBiphone, $curRightBiphone);
    my $occurrence = $rightToBeMergedVocab{$rightBiphone}{$triphone};
    # print STDERR "## LOG ($0): right-biphone: $rightBiphone, triphone: $triphone\n";
    if(IsRightBiphone($triphone, \%contextPhoneVocab) == 1) {
      next if (SearchOtherRightBiphone($rightBiphone, $occurrence, \%contextPhoneVocab, \%physicalVocab, \%phone2PhysicalVocab) == 1);
    } elsif (IsNormalTriphone($triphone, \%contextPhoneVocab, \$curLeftBiphone, \$curRightBiphone) == 1) {
      next if (SearchOtherLeftTriphone ($curRightBiphone, $triphone, $occurrence, \%contextPhoneVocab, \%physicalVocab, \%phone2PhysicalVocab) == 1);
      next if (SearchOtherRightTriphone ($curLeftBiphone, $triphone, $occurrence, \%contextPhoneVocab, \%physicalVocab, \%phone2PhysicalVocab) == 1);
    } else {
      die "## ERROR ($0): incorrectly classified triphone $triphone\n";
    }
    my $monophone;
    GetMonophone($triphone, \%contextPhoneVocab, \$monophone);
    if(not exists $monoToBeMergedVocab{$monophone}{$triphone}) {
      $monoToBeMergedVocab{$monophone}{$triphone} = $rightToBeMergedVocab{$rightBiphone}{$triphone};
    } else {
      if($monoToBeMergedVocab{$monophone}{$triphone} != $rightToBeMergedVocab{$rightBiphone}{$triphone}) {
        die "## ERROR ($0): inconsistent occurrence number, $monoToBeMergedVocab{$monophone}{$triphone}", 
            " versus $rightToBeMergedVocab{$rightBiphone}{$triphone}\n";
      }
    }
  }
}
my %tmpVocab = ();
foreach my $monophone (keys%monoToBeMergedVocab) {
  foreach my $phone (keys%{$monoToBeMergedVocab{$monophone}}) {
    if(exists $phone2PhysicalVocab{$phone}) {
      print STDERR "## LOG (main, ", __LINE__, "): $phone already mapped\n";
      # delete $monoToBeMergedVocab{$monophone}{$phone};
      next;
    }
    $tmpVocab{$monophone}{$phone} = $monoToBeMergedVocab{$monophone}{$phone};
  }
}
# now do 'heterogenious' clustering, meaning the same core phone but differenct context might be clustered
foreach my $monophone (keys%tmpVocab) {
  my $total = 0; my $mapped = 0;
  CheckMapped($tmpVocab{$monophone}, \%phone2PhysicalVocab, \%physicalVocab, \$total, \$mapped);
  die "## ERROR ($0, ", __LINE__ ,"): for $monophone, total = $total, mapped = $mapped\n" if ($mapped != 0);
  my $physicalPhone; my $totalOcc;
  my $triphoneNum = scalar (keys%{$tmpVocab{$monophone}});
  # print STDERR "## LOG (", __LINE__, "): monophone $monophone,", "triphone $triphoneNum\n";
  if (TryClusterAllToOne($tmpVocab{$monophone}, \%contextPhoneVocab, \%physicalVocab, \%phone2PhysicalVocab, $threshold, \$selectedNum, \$physicalPhone, \$totalOcc) == 1) {
    # print STDERR "## LOG ($0): monophone: $monophone, clustered ctx phone: $physicalPhone, total occurrence: $totalOcc\n"; 
    next;
  }
  ClusterPartialToMany($tmpVocab{$monophone}, \%contextPhoneVocab, \%physicalVocab, \%phone2PhysicalVocab, \$selectedNum);
}
print STDERR "## LOG ($0): ctx triphone number after heterogenious clustering: $selectedNum\n";
# for isolated monophone
for my $monophone (keys%isolatedPhoneVocab) {
  if(exists $silencePhoneVocab{$monophone}) {
    if(exists $physicalVocab{$monophone}) {
      die "## ERROR ($0): silence phone $monophone already existed\n";
    }
    if(exists $phone2PhysicalVocab{$monophone}) {
      die "## ERROR ($0): silence phone $monophone already mapped\n";
    }
    $physicalVocab{$monophone} = $isolatedPhoneVocab{$monophone};
    $phone2PhysicalVocab{$monophone} = $monophone; 
    $selectedNum ++;
    next;
  }
  my $maxOcc = 0;
  my $repTriphone;
  for my $ctxPhone (keys%contextPhoneVocab) {
    next if(exists $silencePhoneVocab{$ctxPhone});
    my $leftBiphone = sprintf("%s-%s", $ctxPhone, $monophone);
    if (exists $phone2PhysicalVocab{$leftBiphone}) {
      my $physicalTriphone = $phone2PhysicalVocab{$leftBiphone};
      my $occurrence = $physicalVocab{$physicalTriphone};
      if($occurrence > $maxOcc) {
        $maxOcc = $occurrence;
        $repTriphone = $physicalTriphone;
      }
    }
    my $rightBiphone = sprintf("%s+%s", $monophone, $ctxPhone);
    if (exists $phone2PhysicalVocab{$rightBiphone}) {
      my $physicalTriphone = $phone2PhysicalVocab{$rightBiphone};
      my $occurrence = $physicalVocab{$physicalTriphone};
      if ($occurrence > $maxOcc) {
        $maxOcc = $occurrence;
        $repTriphone = $physicalTriphone;
      }
    }
  }
  if($maxOcc > 0) {
    $phone2PhysicalVocab{$monophone} = $repTriphone;
    $physicalVocab{$repTriphone} += $isolatedPhoneVocab{$monophone};
  } else {
    if (exists $physicalVocab{$monophone}) {
      die "## ERROR ($0): monophone $monophone already existed\n";
    }
    $physicalVocab{$monophone} = $isolatedPhoneVocab{$monophone};
    $phone2PhysicalVocab{$monophone} = $monophone;
    $selectedNum ++;
  }
}
print STDERR "## LOG ($0): final ctx triphones: $selectedNum\n";
my $phone2PhysicalFile = "|sort -u >$tgtdictdir/phone-map.txt";
DumpPhone2PhoneMapFile(\%phone2PhysicalVocab, "$phone2PhysicalFile");
