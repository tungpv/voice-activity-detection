#!/usr/bin/perl

use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 3) {
  die "\nExample: ali-to-phones final.mdl \"ark:gzip -cd ali.1.gz|\" ark,t:- |utils/int2sym.pl -f 2- phones.txt | $0 dict/lexicon.txt dictdir labeldir \n\n";
}

my ($lexFile, $dictdir, $labeldir) = @ARGV;
open(LAB, "|gzip -c > $labeldir/label.1.gz") or 
die "## ERROR: $0, cannot open $labeldir/label.1.gz to write\n";
my %unit_vocab = ();
print STDERR "## LOG: $0, stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($utt, $s) = ($1, $2); my $uttName = $utt; 
  my $i = 0;
  my @A = split(/\s+/, $s);
  while($i < scalar @A) {
    my $cur_phone = $A[$i];
    my $left_phone = "";
    my $right_phone = "";
    if($i > 0) {
      $left_phone = $A[$i-1];
      if($i + 1 < scalar @A) {
        $right_phone = $A[$i+1];
      } 
    } else {
      $right_phone = $A[$i+1];
    }
    if($cur_phone =~ /_S$/ || $cur_phone !~ /_[BIE]$/) {
      $cur_phone =~ s/_S$//;
      $utt .= " $cur_phone";
      $unit_vocab{$cur_phone} ++;
    } elsif($cur_phone =~ /_B$/) {
      if($right_phone eq "") {
        die "## ERROR: $0, isolated phone $cur_phone ($uttName), no right phone\n";
      }
      my $biphone = "$cur_phone+" . "$right_phone";
      $biphone =~ s/_[BIE]//g;
      $utt .= " $biphone";
      $unit_vocab{$biphone} ++;
    } elsif($cur_phone =~ /_[IE]$/) {
      if($left_phone eq "") {
        die "## ERROR: $0, isolated phone $cur_phone, no left phone\n";
      }
      my $biphone = "$left_phone-" . "$cur_phone";
      $biphone =~ s/_[BIE]//g;
      $utt .= " $biphone";
      $unit_vocab{$biphone} ++;
    } else {
      die "## ERROR: $0, unexpected phone $cur_phone\n";
    }
    $i ++;
  }
  print LAB "$utt\n";
}
close LAB;
print STDERR "## LOG: $0, stdin ended\n";

open(D, "$lexFile") or die "## $0, ERROR: lexFile $lexFile cannot open\n";
open(O, ">$dictdir/raw-biphone-lexicon.txt") or 
die "## $0, ERROR: cannot open raw-biphone-lexicon.txt to write\n";
while(<D>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($word, $phones) = ($1, $2);
  my @A = split(/\s+/, $phones);
  if(scalar @A == 1) {
    print O "$word\t$A[0]\n";
    next if (exists $unit_vocab{$A[0]}); 
    $unit_vocab{$A[0]} ++;
  } else {
    for(my $i = 0; $i < scalar @A; $i ++) {
      my $biphone = $A[$i];
      if($i == 0) {
        $biphone .= "+$A[$i+1]";
        $word .= "\t$biphone";
      } else {
        $biphone = "$A[$i-1]-$biphone";
        $word .= " $biphone";
      }
      next if exists $unit_vocab{$biphone};
      $unit_vocab{$biphone} ++;
    }
    print O "$word\n";
  }
}
close D;
close O;

open(F, "|sort -rnk2> $dictdir/biphone-count.txt") or
die "## ERROR: $0, cannot open $dictdir/biphone-count.txt to write\n";
foreach my $biphone (keys %unit_vocab) {
  print F "$biphone\t$unit_vocab{$biphone}\n";
}
close F;
