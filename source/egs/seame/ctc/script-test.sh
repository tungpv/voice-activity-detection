# make raw-biphone and lexicon
/home2/hhx502/kaldi/src/bin/ali-to-phones /home2/hhx502/seame/exp/tri4a/ali_train/final.mdl  "ark:gzip -cd /home2/hhx502/seame/exp/tri4a/ali_train/ali.*.gz |" ark,t:- | utils/int2sym.pl -f 2- /home2/hhx502/seame/data/lang/phones.txt  | source/egs/seame/ctc/make-biphone-lexicon-label.pl /home2/hhx502/seame/data/local/dict/lexicon.txt  /home2/hhx502/seame/data/local/ctc-data/dict /home2/hhx502/seame/data/local/ctc-data/labels

# make tie-biphone
cat /home2/hhx502/seame/data/local/ctc-data/dict/biphone-count.txt |source/egs/seame/ctc/tie-biphone.pl 10 "<oov>" | sort -u  > /home2/hhx502/seame/data/local/ctc-data/dict/tie-biphone.txt

# make biphone lexicon
cat /home2/hhx502/seame/data/local/ctc-data/dict/raw-biphone-lexicon.txt | source/egs/seame/ctc/make-biphone-dict.pl /home2/hhx502/seame/data/local/ctc-data/dict/tie-biphone.txt  > /home2/hhx502/seame/data/local/ctc-data/dict/lexicon.txt

# make tied-biphone text
gzip -cd /home2/hhx502/seame/data/local/ctc-data/labels/label.1.gz  | source/egs/seame/ctc/map-biphone-transcript.pl /home2/hhx502/seame/data/local/ctc-data/dict/tie-biphone.txt  > /home2/hhx502/seame/data/local/ctc-data/labels/text
