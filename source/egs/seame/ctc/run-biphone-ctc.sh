#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
steps=
cmd=slurm.pl
nj=40
num_sequence=14
frame_num_limit=1000000
learn_rate=0.00004
dev_data=/home2/hhx502/seame/data/low-dev/fbank-pitch
decodename=decode-low-dev
alidir=/home2/hhx502/seame/exp/nnet5a/dnn/ali-train-seame
word_lang=/home2/hhx502/seame/data/lang

# end options

function Usage {
 cat<<END
 Usage: $0 [options] <datadir> <tgtdir>
 [options]
 --steps                              # value, "$steps"
 --cmd                                # value, "$cmd"
 --nj                                 # value, $nj
 --num-sequence                       # value, $num_sequence
 --frame-num-limit                    # value, $frame_num_limit
 --learn-rate                         # value, $learn_rate
 [steps]:
 1: prepare biphone lexicon and transcript
 6: decode dev_data, if any
 [examples]:
 
 $0 --steps 1 --learn-rate $learn_rate --dev-data $dev_data \
 --decodename $decodename --num-sequence $num_sequence --frame-num-limit $frame_num_limit \
 /home2/hhx502/seame/data  /home2/hhx502/seame/biphone-ctc-exp

END
}

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  Usage && exit 1
fi
data=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

data_source_dir=$data/local/ctc-data
bindir=/home2/hhx502/kaldi/bin-may-17-2016
if [ ! -z $step01 ]; then
  echo "## $0, LOG: prepare biphone lexicon and transcript @ `hostname` & `date`"
  [ -d $data_source_dir ] || mkdir -p $data_source_dir
  $bindir/bin/ali-to-phones $alidir/final.mdl  "ark:gzip -cd $alidir/ali.*.gz |" ark,t:- | \
  utils/int2sym.pl -f 2- $data/lang-sil/phones.txt  | source/egs/seame/ctc/make-biphone-lexicon-label.pl $data/local/dict-sil/lexicon.txt  $data/local/ctc-data/dict $data/local/ctc-data/labels || exit 1
  
  echo "## $0, LOG: biphone-count.txt, raw-biphone-lexicon.txt, & label.1.gz successfully generated"
  cat $data_source_dir/dict/biphone-count.txt |source/egs/seame/ctc/tie-biphone.pl 10 "<oov>" | sort -u  > $data_source_dir/dict/tie-biphone.txt  || exit 1
  echo "## $0, LOG: tie-biphone.txt successfully generated"
  cat $data_source_dir/dict/raw-biphone-lexicon.txt | source/egs/seame/ctc/make-biphone-dict.pl $data_source_dir/dict/tie-biphone.txt  > $data_source_dir/dict/lexicon.txt || exit 1
  echo "## $0, LOG: biphone lexicon.txt ($data_source_dir/dict) successfully generated"
  gzip -cd $data_source_dir/labels/label.1.gz  | source/egs/seame/ctc/map-biphone-transcript.pl  $data_source_dir/dict/tie-biphone.txt  > $data_source_dir/labels/text
  echo "## $0, LOG: done ('$data/local/ctc-data/dict', '$data/local/ctc-data/labels')"
fi


srcdict=$data_source_dir/dict/lexicon.txt
langdir=$data/ctc-biphone-lang
if [ ! -z $step02 ]; then
  echo "## LOG, step02, $0: prepare ctc lang @ `date`"
  source/egs/seame/ctc/prepare_phn_dict.sh $srcdict $(dirname $srcdict)
  utils/ctc_compile_dict_token.sh $(dirname $srcdict) \
  $data/local/lang-biphone-tmp $langdir || exit 1;
  echo "## LOG, step02, $0: done @ `date` "
fi

lm=$data/local/lm-sil/3gram-mincount/lm_unpruned.gz
lexicon=$srcdict/lexicon.txt
tmpdir=$data/local/lm/tmpdir
if [ ! -z $step03 ]; then
  echo "## LOG, step03, $0:  compile decoding graph"
  [ -d $tmpdir/out_lm ] || mkdir -p $tmpdir
  gzip -cd "$lm" | \
  /home2/hhx502/kaldi/bin-may-17-2016/lmbin/arpa2fst --disambig-symbol=#0 \
  --read-symbol-table=$langdir/words.txt - $langdir/G.fst
  fsttablecompose ${langdir}/L.fst $langdir/G.fst | fstdeterminizestar --use-log=true | \
  fstminimizeencoded | fstarcsort --sort_type=ilabel > $langdir/LG.fst || exit 1;
  fsttablecompose ${langdir}/T.fst $langdir/LG.fst > $langdir/TLG.fst || exit 1;
  echo "## LOG, step03, $0: done ('$langdir')"
fi

overall_data=$data/train-sil/fbank-pitch
train=$tgtdir/data/train
valid=$tgtdir/data/valid
if [ ! -z $step04 ]; then
  echo "## LOG, step04, $0: training data preparation for blstm-ctc training"
  source/egs/swahili/subset_data.sh --subset_time_ratio 0.1 \
  --random true \
  --data2 $train \
  $overall_data  $valid || exit 1
  echo "## LOG, step04, $0: done"
fi

if [ ! -z $step05 ]; then
  echo "## LOG, step05, $0: label preparation started @ `date`"
  cat $data_source_dir/labels/text | \
  ~/w2016/utils/sym2int.pl -f 2- $langdir/units.txt | gzip  > $train/labels.gz
  abs_train=$(cd $train; pwd)
  (rm $valid/labels.gz 2>/dev/null; unlink $valid/labels.gz 2>/dev/null;
    cd $valid; ln -s $abs_train/labels.gz
  )
  echo "## LOG, step05, $0: done ('$train', '$valid')  @ `date`"
fi

input_feat_dim=$[$(feat-to-dim scp:$overall_data/feats.scp -)*3] || exit 1
echo "## LOG: $0, input_feat_dim=$input_feat_dim"
lstm_layer_num=4
lstm_cell_dim=320
target_num=$[$(cat $langdir/units.txt|wc -l)+1]
echo "## LOG: $0, target_num=$target_num"
if [ ! -z $step06 ]; then
  echo "## LOG, step06, $0: prepare dnn topology"
  utils/model_topo.py --input-feat-dim $input_feat_dim --lstm-layer-num $lstm_layer_num \
    --lstm-cell-dim $lstm_cell_dim --target-num $target_num \
    --fgate-bias-init 1.0 > $tgtdir/nnet.proto || exit 1;
  echo "## LOG, step06, $0: done"
fi

if [ ! -z $step07 ]; then
  echo "## LOG, step07, $0: blstm-ctc training started @ `date`"
  steps/train_ctc_parallel.sh --add-deltas true --num-sequence $num_sequence \
    --frame-num-limit $frame_num_limit \
    --learn-rate $learn_rate --report-step 1000 --halving-after-epoch 12 \
    $train $valid $tgtdir || exit 1;
  echo "## LOG, step07, $0: done ($tgtdir) `date`"
fi
