#!/usr/bin/perl

use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 2) {
  die "\n\nExample: cat biphone-count.txt | $0 10 <oov-phone> > tie-biphone.txt\n\n";
}
# begin sub
sub InsertBiphone {
  my ($vocab, $key, $biphone, $count) = @_;
  if(exists $$vocab{$key}) {
    my $refList = $$vocab{$key};
    my @A = ($biphone, $count);
    push @$refList, \@A;
  } else {
    $$vocab{$key} = [];
    my $refList = $$vocab{$key};
    my @A = ($biphone, $count);
    push @$refList, \@A;
  }
}
#
sub CheckBiphoneSet {
  my ($vocab, $key, $retPair) = @_;
  if(not exists $$vocab{$key}) {
    die "## ERROR: key $key does not exist \n";
  }
  my $list = $$vocab{$key}; my $count = 0;
  $$retPair = $$list[0];
  for(my $i = 0; $i < scalar @$list; $i ++) {
    my $pair = $$list[$i];
    if($$pair[1] > $$$retPair[1]) {
      $$retPair = $pair;
    }
    $count += $$pair[1];
  }
  return $count;
}
#
sub UniqueInsertItem4Vocab {
  my ($vocab, $key, $value) = @_;
  if(exists $$vocab{$key}) {
    print STDERR "## WARNING, $0: $key already exists\n";
    return;
  }
  $$vocab{$key} = $value;
}
# end sub
my ($count_thresh, $oov_phone) = @ARGV;

my %leftVocab = ();
my %rightVocab = ();
my %tiedBiphone = ();
print STDERR "## LOG: stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(\d+)/ or next;
  my ($biphone, $count) = ($1, $2);
  my @A = (); 
  my $left = 0;  my $right = 0;
  if($biphone =~ /\-/) {
    @A = split(/\-/, $biphone); $left = 1;
  } elsif($biphone =~ /\+/) {
    @A = split(/\+/, $biphone);  $right = 1;
  } else {
    $tiedBiphone{$biphone} = $biphone;
    next;
  }
  if(scalar @A != 2) {
    die "## ERROR: illegal biphone $biphone\n";
  }
  if($left == 1) {
    InsertBiphone(\%leftVocab, $A[1], $biphone, $count);
  }
  if($right == 1) {
    InsertBiphone(\%rightVocab, $A[0], $biphone, $count);
  }
}
print STDERR "## LOG: stdin ended\n";
foreach my $key (keys%leftVocab) {
  my $retPair;
  my $count = CheckBiphoneSet(\%leftVocab, $key, \$retPair);
  # print STDERR "## LOG: retPair = ($$retPair[0], $$retPair[1])\n";
  # print STDERR "## LOG: total count number for $key in left case, $count\n";
  my $list = $leftVocab{$key};
  if($count < $count_thresh) {
    print STDERR "## LOG, $0: phone $key has lower frequency ($count) in its left biphone format\n";
    my $rightRetPair;
    if (exists $rightVocab{$key}) {
      CheckBiphoneSet(\%rightVocab, $key, \$rightRetPair);
    }
    if(exists $rightVocab{$key} &&  $$rightRetPair[1] >= $count_thresh) {
      print STDERR "## LOG, $0: using right biphone $$rightRetPair[0] \n";
      for(my $i = 0; $i < scalar @$list; $i ++) {
        my $pair = $$list[$i];
       UniqueInsertItem4Vocab(\%tiedBiphone, $$pair[0], $$rightRetPair[0]);
      }
    } else {
      for(my $i = 0; $i < scalar @$list; $i ++) {
        my $pair = $$list[$i];
        if($$pair[1] <= 1) {
          print STDERR "## $0, WARNING: $$pair[0] is mapped to $oov_phone\n";
          UniqueInsertItem4Vocab(\%tiedBiphone, $$pair[0], $oov_phone);
        } else {
          UniqueInsertItem4Vocab(\%tiedBiphone, $$pair[0], $key);
        }
      }
    }
  } else {
    if($$retPair[1] >= $count_thresh) {
      for(my $i = 0; $i < scalar @$list; $i ++) {
        my $pair = $$list[$i];
        if($$pair[1] >= $count_thresh) {
          UniqueInsertItem4Vocab(\%tiedBiphone, $$pair[0], $$pair[0]);
        } else {
          UniqueInsertItem4Vocab(\%tiedBiphone, $$pair[0], $$retPair[0]);
        }
      }     
    } else {
      for(my $i = 0; $i < scalar @$list; $i ++) {
        my $pair = $$list[$i];
        UniqueInsertItem4Vocab(\%tiedBiphone, $$pair[0], $key);
      }
    }
  } 
}
foreach my $key (keys%rightVocab) {
  my $retPair;
  my $count = CheckBiphoneSet(\%rightVocab, $key, \$retPair);
  # print STDERR "## LOG: total count number for $key in right case, $count\n";
  my $list = $rightVocab{$key};
  if($count < $count_thresh) {
    print STDERR "## LOG: phone $key has lower frequency ($count) in its right biphone format\n";
    my $leftRetPair;
    if(exists $leftVocab{$key}) {
      CheckBiphoneSet(\%leftVocab, $key, \$leftRetPair);
    }
    if(exists $leftVocab{$key} && $$leftRetPair[1] >= $count_thresh) {
      print STDERR "## LOG, $0: using left biphone $$leftRetPair[0]\n";
      for(my $i = 0; $i < scalar @$list; $i++) {
        my $pair = $$list[$i];
        UniqueInsertItem4Vocab(\%tiedBiphone, $$pair[0], $$leftRetPair[0]);
      }
    } else {
      for(my $i = 0; $i < scalar @$list; $i++) {
        my $pair = $$list[$i];
        if($$pair[1] <= 1) {
          print STDERR "## $0, WARNING: $$pair[0] is mapped to $oov_phone";
          UniqueInsertItem4Vocab(\%tiedBiphone,$$pair[0], $oov_phone);
        } else {
          UniqueInsertItem4Vocab(\%tiedBiphone,$$pair[0], $key);
        }
      }
    }
  } else {
    if($$retPair[1] >= $count_thresh) {
      for(my $i = 0; $i < scalar @$list; $i++) {
        my $pair = $$list[$i];
        if($$pair[1] >= $count_thresh) {
          UniqueInsertItem4Vocab(\%tiedBiphone, $$pair[0], $$pair[0]);
        } else {
          UniqueInsertItem4Vocab(\%tiedBiphone, $$pair[0], $$retPair[0]);
        }
      }
    } else {
      for(my $i = 0; $i < scalar @$list; $i ++) {
        my $pair = $$list[$i];
        UniqueInsertItem4Vocab(\%tiedBiphone, $$pair[0], $key);
      }
    }
  }
}
# output results
foreach my $key (keys%tiedBiphone) {
  print "$key\t$tiedBiphone{$key}\n";
}
