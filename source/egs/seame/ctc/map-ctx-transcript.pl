#!/usr/bin/perl

use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\nExample: cat raw-biphone-transcript.txt | $0 tied-biphone.txt\n\n";
}
my ($tied_biphone) = @ARGV;

# begin sub
sub LoadVocab {
  my ($vocab, $tied_biphone_fname) = @_;
  open(F, "$tied_biphone_fname") or die "## $0, ERROR: tied_biphone file $tied_biphone_fname cannot open\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(\S+)/ or next;
    die "## $0, ERROR: dupilcated biphone $1\n" if exists $$vocab{$1};
    $$vocab{$1} = $2;
  }
  close F;
}
# end sub
my %vocab = ();
LoadVocab (\%vocab, $tied_biphone);
print STDERR "## $0, LOG: stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($labId, $words) = ($1, $2);
  my @A = split(/\s+/, $words);
  my $utt = $labId;
  for(my $i = 0; $i < scalar @A; $i ++) {
    my $biphone = $A[$i];
    die "## $0, ERROR: unidentified biphone $biphone \n" if not exists $vocab{$biphone};
    $biphone = $vocab{$biphone};
    $utt .= " $biphone";
  }
  print "$utt\n";
}
print STDERR "## $0, LOG: stdin ended\n";
