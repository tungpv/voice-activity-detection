#!/bin/bash 

. path.sh
. cmd.sh 

if [ $# -ne 2 ]; then
  echo 
  echo "Example: $0  /home2/hhx502/seame/data/local/dict/lexicon.txt /home2/hhx502/seame/data/local/ctc-phn-dict"
  echo  && exit 1
fi

srcdict=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir
cp $srcdict $tgtdir/lexicon.txt
cat $srcdict | \
perl -ane 'chomp; @A = split(/\s+/); shift @A; for($i = 0; $i < @A; $i++)
{ $phn = $A[$i]; print "$phn\n"; }' | sort -u | \
perl -e '$i = 1; while(<>){ chomp; print "$_ $i\n"; $i ++;}' > $tgtdir/units.txt || exit 1

utils/sym2int.pl -f 2- $tgtdir/units.txt < $srcdict > $tgtdir/lexicon_numbers.txt || exit 1

echo "Phoneme-based dictionary preparation succeeded"
