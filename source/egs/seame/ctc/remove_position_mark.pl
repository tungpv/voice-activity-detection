#!/usr/bin/perl

print STDERR "## LOG: $0, stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/g;  
  ($uttid, $s) = ($1, $2);
  $s =~ s/_[SBIE]//g;
  print "$uttid $s\n";
}
print STDERR "## LOG: $0, stdin ended\n";
