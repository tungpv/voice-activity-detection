#!/bin/bash

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
cmd=slurm.pl
silence_word="<silence>,<noise>,<unk>,<v-noise>"
word_text=/home2/hhx502/seame/data/train/text
clustered_triphone_num=5000
phone_occurrence_threshold=110
nj=40
num_sequence=14
frame_num_limit=1000000
learn_rate=0.00004
dev_data=/home2/hhx502/seame/data/low-dev/fbank-pitch
decodename=decode-low-dev
# end options

function Usage {
 cat<<EOF
 
 Usage: $0 [options] <datadir> <lang> <alidir> <tgtdir>
 [options]:
 [examples]:
 
 $0 --steps 1 --silence-word "$silence_word" \
 --word-text $word_text \
 --dev-data $dev_data --decodename $decodename \
 --phone-occurrence-threshold 110 --clustered-triphone-num 5000 \
 --num-sequence $num_sequence --frame-num-limit $frame_num_limit \
 /home2/hhx502/seame/data /home2/hhx502/seame/data/ctc-triphone-lang \
 /home2/hhx502/seame/exp/nnet5a/dnn/ali-train-seame \
 /home2/hhx502/seame/ctc-triphone-exp    

EOF
}

. parse_options.sh || exit 1


if [ $# -ne 4 ]; then
  Usage && exit 1
fi

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

data=$1
lang=$2
alidir=$3
tgtdir=$4

data_source_dir=$data/local/ctc-ctx-triphone
bindir=/home2/hhx502/kaldi/bin-oct-13-2016

dict=$data_source_dir/dict
labeldir=$data_source_dir/labels
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): prepare triphone lexicon and transcript @ `date`"
  [ -d $dict ] || mkdir -p $dict
  [ -d labeldir ] || mkdir -p $labeldir  
  $bindir/bin/ali-to-phones $alidir/final.mdl  "ark:gzip -cd $alidir/ali.*.gz |" ark,t:- | \
  utils/int2sym.pl -f 2- $data/lang-sil/phones.txt  | \
  source/egs/seame/ctc/make-triphone-lexicon-label-2.pl \
  --silence-word="$silence_word"  --word-text=$word_text \
  $phone_occurrence_threshold \
  $clustered_triphone_num $data/local/dict-sil \
  $dict $labeldir || exit 1
  
  echo "## LOG (step01, $0): done with $data_source_dir  @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): relabel lexicon and transcript"
  cat $dict/raw-triphone-lexicon.txt | \
  source/egs/seame/ctc/make-ctx-dict.pl \
  $dict/phone-map.txt  > $dict/lexicon.txt
  gzip -cd $labeldir/raw-label.gz | \
  source/egs/seame/ctc/map-ctx-transcript.pl $dict/phone-map.txt > $labeldir/text
  echo "## LOG (step02, $0): lexicon and transcript relabelling done"
fi 

if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): prepare ctc lang @ `date`"
  source/egs/seame/ctc/prepare_phn_dict.sh $dict/lexicon.txt $dict
  utils/ctc_compile_dict_token.sh $dict \
  $lang/tmp $lang || exit 1;
  echo "## LOG (step03, $0): lang preparation done"
fi

lm=$data/local/lm-sil/3gram-mincount/lm_unpruned.gz
tmpdir=$data/local/lm/tmpdir
if [ ! -z $step04 ]; then
  echo "## LOG (step04, $0): make TLG @ `date`"
  gzip -cd "$lm" | \
  /home2/hhx502/kaldi/bin-oct-13-2016/lmbin/arpa2fst --disambig-symbol=#0 \
  --read-symbol-table=$lang/words.txt - $lang/G.fst

  fsttablecompose ${lang}/L.fst $lang/G.fst | fstdeterminizestar --use-log=true | \
  fstminimizeencoded | fstarcsort --sort_type=ilabel > $lang/LG.fst || exit 1;
  fsttablecompose ${lang}/T.fst $lang/LG.fst > $lang/TLG.fst || exit 1;
  echo "## LOG (step04, $0): done ('$lang/TLG.fst')"
fi

overall_data=$data/train-sil/fbank-pitch
train=$tgtdir/data/train
valid=$tgtdir/data/valid
if [ ! -z $step05 ]; then
  echo "## LOG (step05, $0): training data preparation for blstm-ctc training"
  source/egs/swahili/subset_data.sh --subset_time_ratio 0.1 \
  --random true \
  --data2 $train \
  $overall_data  $valid || exit 1
  echo "## LOG (step05, $0): done"
fi

if [ ! -z $step06 ]; then
  echo "## LOG (step06, $0): label preparation started @ `date`"
  cat $labeldir/text | \
  ~/w2016/utils/sym2int.pl -f 2- $lang/units.txt | gzip  > $train/labels.gz
  abs_train=$(cd $train; pwd)
  (rm $valid/labels.gz 2>/dev/null; unlink $valid/labels.gz 2>/dev/null;
    cd $valid; ln -s $abs_train/labels.gz
  )
  echo "## LOG (step06, $0): done ('$train', '$valid')  @ `date`"
fi

input_feat_dim=$[$(feat-to-dim scp:$overall_data/feats.scp -)*3] || exit 1
echo "## LOG: $0, input_feat_dim=$input_feat_dim"
lstm_layer_num=4
lstm_cell_dim=320
target_num=$[$(cat $lang/units.txt|wc -l)+1]
echo "## LOG: $0, target_num=$target_num"
if [ ! -z $step07 ]; then
  echo "## LOG (step06, $0): prepare dnn topology"
  utils/model_topo.py --input-feat-dim $input_feat_dim --lstm-layer-num $lstm_layer_num \
    --lstm-cell-dim $lstm_cell_dim --target-num $target_num \
    --fgate-bias-init 1.0 > $tgtdir/nnet.proto || exit 1;
  echo "## LOG (step06, $0): done ('$tgtdir/nnet.proto')"
fi

if [ ! -z $step08 ]; then
  echo "## LOG (step08, $0): blstm-ctc training started @ `date`"
  steps/train_ctc_parallel.sh --add-deltas true --num-sequence $num_sequence \
    --frame-num-limit $frame_num_limit \
    --learn-rate $learn_rate --report-step 1000 --halving-after-epoch 12 \
    $train $valid $tgtdir || exit 1;
  echo "## LOG (step07, $0): done ($tgtdir) `date`"
fi



