#!/usr/bin/perl 
use utf8;
use open qw(:std :utf8);
use strict;
use warnings;

my $numArgs = scalar @ARGV;

if ($numArgs != 2) {
  die "\n\nExample: cat text | $0 utt2spk  spk2rate\n\n";
}

my ($utt2spk, $spk2rate) = @ARGV;

my %vocab_utt2spk = ();
my %spk_rate = ();
open (SPK2RATE, "|sort -k2nr > $spk2rate") or die "## ERROR: $0, $spk2rate cannot open to write\n";
open(F, "$utt2spk") or die "## ERROR: $0, $utt2spk cannot open\n";
while(<F>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  $vocab_utt2spk{$1} =$2;
}
close F;
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my $utt = $1;
  die "## ERROR: $0, unknown utterance $utt\n" if not exists $vocab_utt2spk{$utt};
  my $spk = $vocab_utt2spk{$utt};
  my @A = split(/\s+/, $2);
  my $total = scalar @A;
  my $num_man = 0; my $num_en = 0;
  for(my $i = 0; $i < @A; $i ++) {
    my $w = $A[$i];
    if($w =~ m/\p{Han}+/) {
      $num_man ++;
    } else {
      $num_en ++;
    }
  }
  my $man_rate = $num_man / $total;
  if (0) {
    print STDERR "$_ ($man_rate)\n";
  }
  if (exists $spk_rate{$spk}) {
    my $aRef = $spk_rate{$spk};
    $$aRef[0] += $man_rate;
    $$aRef[1] ++;
  } else {
    $spk_rate{$spk} = [my @B ];
    my $aRef = $spk_rate{$spk};
    push @$aRef, $man_rate;
    push @$aRef, 1;
  }
}

foreach my $spk (keys%spk_rate) {
  my $aRef = $spk_rate{$spk};
  my $rate = sprintf("%.2f", $$aRef[0]/ $$aRef[1]);
  print SPK2RATE "$spk $rate\n";
}
close SPK2RATE
