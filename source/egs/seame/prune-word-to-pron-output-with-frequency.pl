#!/usr/bin/perl
use warnings;
use strict;

use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;

if ($numArgs != 5) {
  die "\nExample: cat word-to-pron-output.txt | $0 en-word-lex.txt cn-syllable-lex.txt 10 (frequency threshold) 1 (top_order) 0.5 (occupancy_rate) \n\n";
}
my ($en_word_lex, $cn_syl_lex, $frequency, $top_order, $occupancy_rate) = @ARGV;

# begin sub
sub LoadDict {
  my($vocab, $dict_file) = @_;
  open(F, "$dict_file") or die "## ERROR: dict $dict_file cannot open\n";
  while(<F>) {
    chomp;
    my @A = split(/\s+/);
    $$vocab{$A[0]} ++;
  }
  close F;
}
#
sub IsSyllableSequence {
  my ($cn_syl_vocab, $pron) = @_;
  my @A = split(/\s+/, $pron);
  for (my $i = 0; $i < @A; $i ++) {
    return 0 if (not exists $$cn_syl_vocab{$A[$i]});
  }
  return 1;
}
#
sub IsOK {
  my ($cn_syl_vocab, $A, $frequency, $top_order, $occupancy_rate, $dominant_pron) = @_;
  my %vocab = (); 
  my $total_occ = 0;
  for(my $i = 0; $i < @$A; $i ++) {
    my $line = $$A[$i];
    die "## ERROR: $0, IsOK, bad line: $line\n" if ($line !~ m/(.*)\s+(\d+)$/g);
    $vocab{$1} = $2;  
    $total_occ += $2;
  }
  my ($i, $maxi) = (0, $top_order); 
  my $isOk = 0;
  foreach my $pron (sort {$vocab{$b} <=>$vocab{$a} } (keys %vocab)) {
    # print STDERR "$pron $vocab{$pron}\n";
    if($i < $maxi && IsSyllableSequence($cn_syl_vocab, $pron) == 1) {
      if($vocab{$pron} / $total_occ >= $occupancy_rate && $vocab{$pron} > $frequency) {
        $$dominant_pron  = $pron;
        $isOk = 1; last;
      }
    } elsif ($i >= $maxi) {
      last;
    }
    $i ++;
  }
  return $isOk;
}
# end sub
my %en_word_vocab = ();
LoadDict(\%en_word_vocab, $en_word_lex);
my %cn_syl_vocab = ();
LoadDict(\%cn_syl_vocab, $cn_syl_lex);

print STDERR "## LOG: $0, stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\t/);
  my $en_word = shift @A;
  next if($en_word =~ m/\p{Han}+/);
  my $dominant_pron = "";
  next if (IsOK(\%cn_syl_vocab, \@A, $frequency, $top_order, $occupancy_rate, \$dominant_pron) == 0);
  print "$en_word\t$dominant_pron\n";
}
print STDERR "## LOG: $0, stdin ended\n";
