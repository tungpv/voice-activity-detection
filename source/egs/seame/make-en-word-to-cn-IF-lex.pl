#!/usr/bin/perl

use strict;
use warnings;

my $numArgs = @ARGV; 

if ($numArgs != 1) {
  die "\nExample: cat en-word-to-cn-syl.txt | $0 cn-syl-to-IF-lex.txt\n\n";
}

my($lexFile) = @ARGV;

open(L, "$lexFile") or die "## ERROR: $0, file $lexFile cannot open\n";
my %vocab = ();
while(<L>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $vocab{$1} = $2;
}
close L;
print STDERR "## LOG: $0, stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  my $word = shift @A;  my $pron = "";
  for(my $i = 0; $i < @A; $i++) {
    my $syl = $A[$i];
    die "## ERROR: unidentified syllable $syl\n" if not exists $vocab{$syl};
    $pron .= "$vocab{$syl} ";
  }
  $pron =~ s/ $//;
  print "$word\t$pron\n";
}
print STDERR "## LOG: $0, stdin ended\n";
