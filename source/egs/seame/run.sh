#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
steps=
source_lexicon=/home2/hhx502/sg-en-i2r/data/local/dict/lexicon.txt
# end options

function Usage {
 cat<<END
 Usage: $0 [options] <source_data_dir> <tgtdir>
 [options]
 --steps                          # value, "$steps"
 --source-lexicon                 # value, "$source_lexicon"
 [examples]:

 $0 --steps 1  --source-lexicon $source_lexicon  /data/users/hhx502/seame  /home2/hhx502/seame

END
}

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  Usage && exit 1; 
fi
source_data_dir=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

localdir=$tgtdir/data/local
[ -d $localdir ] || mkdir -p $localdir
if [ ! -z $step01 ]; then
  echo "## step01, lowercase file name @ `date`"
  for SRC in `find $source_data_dir -depth`
do
    DST=`dirname "${SRC}"`/`basename "${SRC}" | tr '[A-Z]' '[a-z]'`
    if [ "${SRC}" != "${DST}" ]
    then
        [ ! -e "${DST}" ] && mv -T "${SRC}" "${DST}" || echo "${SRC} was not renamed"
    fi
done 
  echo "## step01, done @`date`"
fi
overall_data=$localdir/overall-data
if [ ! -z $step02 ]; then
  echo "## step02, collect wave and transcript file lists @ `date`"
  [ -d $overall_data ] || mkdir -p $overall_data
  find  $source_data_dir/phasei/original  $source_data_dir/phaseii/transcribing  -name "*.wav" | \
  perl -e 'while(<STDIN>){chomp; m/([^\/]+)\.wav/; if(not exists $vocab{$1}){ print "$_\n"; $vocab{$1} ++;}  }' | sort -u > $overall_data/wavlist.txt
  find $source_data_dir/phaseii/transcripts/txt  -name "*.txt" | sort -u > $overall_data/translist.txt
  cat $overall_data/wavlist.txt | \
  perl -e 'while(<STDIN>){chomp; m/([^\/]+)\.wav/;print $1 . " /usr/bin/sox $_ -r 8000 -c 1 -b 16 -t wav - downsample |\n";  }' > $overall_data/wav.scp
  for f in $(cat $overall_data/translist.txt); do
    cat $f | perl -ane 'chomp; m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(.*)/g or next; $fileid = lc $1; $start = sprintf("%.2f", $2/1000); $end = sprintf("%.2f", $3/1000);  print $fileid . " $start $end $5\n"; '
  done  > $overall_data/transcript.txt
  echo "## step02, done ($overall_data/wavlist.txt, $overall_data/translist.txt, $overall_data/transcript.txt ) @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "## LOG: step03, transcript preparation @ `date`"
  cat $overall_data/transcript.txt | \
  perl -e 'use utf8; use open qw(:std :utf8); 
    while(<STDIN>){ @A = split(/\s+/); if(@A <= 3){next;} 
    $start = $A[1]; $end = $A[2];  $utt = $A[0] . " " . $start . " " . $end;  
   for($i = 3; $i < @A; $i ++) { $w = $A[$i];
     if($w =~ m/\p{Han}+/) {
       $s = $w;
       $start = 0;  $tot_len = length($s);
       while($s =~ m/(\p{Han}+)/g) {
         $ms = $1;  $len = length($ms);  $end = pos($s); $new_start = $end - $len;
         if($new_start > $start) {
           $sub_s = substr($s, $start, $new_start - $start);
           if($sub_s ne "[" && $sub_s ne "]" && $sub_s ne "<" && $sub_s ne ">"  ) {
             $utt .= " " . $sub_s;
           } 
         }
         $start = $end;
         @B = split("", $ms); $utt .= " " . join(" ", @B);
       }
       if($start > 0 && $start < $tot_len) {
         $sub_s = substr($s, $start, $tot_len - $start);
         if($sub_s ne "[" && $sub_s ne "]" &&  $sub_s ne "<" && $sub_s ne ">") {
           $utt .= " " .  $sub_s;
         }
       }
     } else {
       if($w ne "[" && $w ne "]" && $w ne ">" && $w ne "<") { $utt .= " $w";  }
     }
   }
   print "$utt\n";
  }'   | \
  grep -v "<unk" | \
  grep -v "<unl" | \
  source/egs/seame/clean-text.pl | \
  source/egs/seame/transform-text.pl 3 ' ' $overall_data/transform-dict-edited.txt  > $overall_data/transcript.1.txt 

  cat $overall_data/transcript.1.txt | perl -ane 'chomp; @A = split(/\s+/);
    for($i=3; $i < @A; $i++){$w = $A[$i];  if(not exists $vocab{$w}){ print "$w\t$w\n"; $vocab{$w}++; } }
  '| sort -u > $overall_data/transform-dict.txt
  echo "## LOG: step02, done ($overall_data/transform-dict.txt) @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## step04: get oov status with a given lexicon ($source_lexicon)"
  cat $overall_data/transcript.1.txt | \
  perl -e ' use utf8; use open qw(:std :utf8);
   ($from, $lexFile) = @ARGV; open(L, "$lexFile") or die "## ERROR: file $lexFile cannot open\n";
   while(<L>) {chomp; m/(\S+)\s+(.*)/ or die "## bad lexicon line $_\n"; $vocab{$1} ++; } close L;
   while(<STDIN>) { chomp; @A = split(/\s+/);
     for($i=$from-1; $i < @A; $i++) {
       $word = $A[$i];
       if(not exists $vocab{$word}) {
         if(not exists $oov{$word}) { $oov{$word} ++; print "$word\t$word\n"; }
       }
     }
   }
  ' 4 $source_lexicon  | sort -u > $overall_data/oov-dict.txt
  echo "## step04: done ($overall_data/oov-dict.txt)"
fi
g2pdir=$tgtdir/data/local/g2p-model
if [ ! -z $step05 ]; then
  echo "## step05, train g2p model"
  [ -d $g2pdir ] || mkdir -p $g2pdir
  cat $source_lexicon | \
  perl -e 'use utf8; use open qw(:std :utf8);
    while(<STDIN>) { chomp; next if(/[<]/ || /\p{Han}+/g); print "$_\n";  }' > $g2pdir/lex.txt 
  source/egs/g2p/run-g2p.sh --steps 1,2,3,4 $g2pdir/lex.txt $g2pdir
  echo "## step05, g2p model training done @ `date`"
fi
