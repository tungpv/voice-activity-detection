#!/usr/bin/perl

use utf8;
use open qw(:std :utf8);

$numArgs = scalar @ARGV;

if ($numArgs != 2) {
  print STDERR "\nUsage: cat text | $0 <lexicon.txt> <from>\n";
  exit 1;
}
($lex, $from) = @ARGV;

open(L, "$lex") or die "## ERROR: lexicon $lex cannot open\n";
while(<L>) {
  chomp;
  m/(\S+)\s+(.*)/ or die "## ERROR: bad line $_ in $lex\n";
  $word = lc $1;
  $vocab{$word} ++;
} 
close L;

print STDERR "stdin expected\n";
while(<STDIN>) {
  chomp;
  @A = split(/\s+/);
  for($i=$from -1; $i < @A; $i++) {
    $word = lc $A[$i];
    if(not exists $vocab{$word}) {
      if(not exists $oov{$word}) {
        $oov{$word} ++;
        print "$word\t$word\n";
      }
    }
  }
}
print STDERR "stdin ended\n";
