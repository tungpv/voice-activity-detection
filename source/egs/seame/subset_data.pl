#!/usr/bin/perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 3) {
  print STDERR "\nExample: cat text | $0 1(hours) segments uttlist\n\n";
  exit 1;
}
my ($required_hours, $segments, $uttlist) = @ARGV;
open(F, "$segments") or die "## ERROR: $0, $segments cannot open\n";
my %vocab = ();
while(<F>) {
  chomp;
  my @A = split(/\s+/);
  $vocab{$A[0]} = $A[3] - $A[2];
}
close F;
open (U, "|sort -u >$uttlist") or die "## ERROR: $0, uttlist $uttlist cannot open\n";
print STDERR "$0, stdin expected\n";
my $selected_sec = 0;
my $selected_utt = "";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  die "## ERROR: unknown utterance id $1\n" if not exists $vocab{$1};
  last if $selected_sec/3600 >= $required_hours;
  $selected_sec += $vocab{$1};
  $selected_utt .= "$1\n";
}
print U $selected_utt;
print STDERR "$0, stdin ended\n";
close U;
