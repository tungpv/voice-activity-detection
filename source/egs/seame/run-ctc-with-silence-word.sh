#!/bin/bash 

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
steps=
cmd=slurm.pl
nj=40
num_sequence=15
frame_num_limit=1000000
learn_rate=0.00004
dev_data=/home2/hhx502/seame/data/low-dev/fbank-pitch
decodename=decode-low-dev
alidir=/home2/hhx502/seame/exp/nnet5a/dnn/ali-train-seame
word_lang=/home2/hhx502/seame/data/lang

# end options

function Usage {
 cat<<END
 Usage: $0 [options] <datadir> <tgtdir>
 [options]
 --steps                              # value, "$steps"
 --cmd                                # value, "$cmd"
 --nj                                 # value, $nj
 --num-sequence                       # value, $num_sequence
 --frame-num-limit                    # value, $frame_num_limit
 --learn-rate                         # value, $learn_rate
 [steps]:
 6: decode dev_data, if any
 [examples]:
 
 $0 --steps 5,6 --learn-rate $learn_rate --dev-data $dev_data \
 --decodename $decodename --num-sequence $num_sequence --frame-num-limit $frame_num_limit \
 /home2/hhx502/seame/data  /home2/hhx502/seame/ctc-exp-ali

END
}

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  Usage && exit 1
fi
data=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
srcdict=$data/local/dict-sil/lexicon.txt
tgtdictdir=$data/local/ctc-phn-dict-sil
langdir=$data/lang_ctc_phn-sil
if [ ! -z $step01 ]; then
  source/egs/seame/ctc/prepare_phn_dict.sh $srcdict  \
  $tgtdictdir || exit 1
  utils/ctc_compile_dict_token.sh $tgtdictdir \
  $data/local/lang_phn_tmp $langdir || exit 1;
  echo "## LOG: $0, step01, done ($tgtdictdir)"
fi
lm=$data/local/lm-sil/3gram-mincount/lm_unpruned.gz
lexicon=$tgtdictdir/lexicon.txt
tmpdir=$data/local/lm/tmpdir
if [ ! -z $step02 ]; then
  echo "## LOG: $0, step02, compile decoding graph"
  [ -d $tmpdir/out_lm ] || mkdir -p $tmpdir
  gzip -cd "$lm" | \
  /home2/hhx502/kaldi/bin-may-17-2016/lmbin/arpa2fst --disambig-symbol=#0 \
  --read-symbol-table=$langdir/words.txt - $langdir/G.fst
  fsttablecompose ${langdir}/L.fst $langdir/G.fst | fstdeterminizestar --use-log=true | \
  fstminimizeencoded | fstarcsort --sort_type=ilabel > $langdir/LG.fst || exit 1;
  fsttablecompose ${langdir}/T.fst $langdir/LG.fst > $langdir/TLG.fst || exit 1;

  echo "## LOG: $0, step02, done"
fi

overall_data=$data/train-sil/fbank-pitch
train=$tgtdir/data/train
valid=$tgtdir/data/valid
if [ ! -z $step03 ]; then
  echo "## LOG: $0, step03, data preparation @ `date`"
  source/egs/swahili/subset_data.sh --subset_time_ratio 0.1 \
    --random true \
    --data2 $train \
    $overall_data  $valid || exit 1
  echo "## LOG: $0, step03, done @ `date`"
fi
if [ ! -z $alidir ] && [ ! -z $word_lang ]; then
  echo "## LOG: $0,  use ali to make phone transcriptions @ `date`"
  step04=
  for x in $alidir/ali.1.gz $alidir/final.mdl $word_lang/phones.txt; do
    [ -f $x ] || { echo "## ERROR: file $x expected"; exit 1; }
  done
  num_jobs=$(cat $alidir/num_jobs)
  $cmd JOB=1:$num_jobs $alidir/log/ali-to-phone.JOB.log \
  /home2/hhx502/kaldi/bin-may-17-2016/bin/ali-to-phones $alidir/final.mdl \
 "ark:gzip -cd $alidir/ali.JOB.gz|" ark,t:- \| \
  ~/w2016/utils/int2sym.pl -f 2- $word_lang/phones.txt \| \
  source/egs/seame/ctc/remove_position_mark.pl \| \
  ~/w2016/utils/sym2int.pl -f 2- $langdir/units.txt \| \
  gzip -c ">" $alidir/label.JOB.gz || exit 1
  zcat $alidir/label.*.gz | gzip -c > $train/labels.gz
  abs_train=$(cd $train; pwd)
  (rm $valid/labels.gz 2>/dev/null unlink $valid/labels.gz 2>/dev/null; 
   cd $valid; ln -s $abs_train/labels.gz)
  echo "## LOG: $0, done @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## LOG: $0, step04, format data @ `date`"
  utils/prep_ctc_trans.py $langdir/lexicon_numbers.txt \
  $train/text "<unk>" | gzip -c - > $train/labels.gz
  utils/prep_ctc_trans.py $langdir/lexicon_numbers.txt \
  $valid/text "<unk>" | gzip -c - > $valid/labels.gz
  echo "## LOG: $0, step04, done @ `date`"
fi

input_feat_dim=$[$(feat-to-dim scp:$overall_data/feats.scp -)*3] || exit 1
echo "## LOG: $0, input_feat_dim=$input_feat_dim"
lstm_layer_num=4
lstm_cell_dim=320
target_num=$[$(cat $langdir/units.txt|wc -l)+1]
echo "## LOG: $0, target_num=$target_num"

# Output the network topology
utils/model_topo.py --input-feat-dim $input_feat_dim --lstm-layer-num $lstm_layer_num \
    --lstm-cell-dim $lstm_cell_dim --target-num $target_num \
    --fgate-bias-init 1.0 > $tgtdir/nnet.proto || exit 1;

if [ ! -z $step05 ]; then
  echo "## LOG: $0, training @ `date`"
  steps/train_ctc_parallel.sh --add-deltas true --num-sequence $num_sequence \
    --frame-num-limit $frame_num_limit \
    --learn-rate $learn_rate --report-step 1000 --halving-after-epoch 12 \
    $train $valid $tgtdir || exit 1;
  echo "## LOG: $0, done @ `date`" 
fi
if [ ! -z $step06 ]; then
  max_nj=$(wc -l < $dev_data/spk2utt)
  [ $nj -gt $max_nj ] && nj=$max_nj
  steps/decode_ctc_lat.sh --cmd "$cmd" --nj $nj --beam 17.0 \
  --lattice_beam 8.0 --max-active 5000 --acwt 0.6 --score-with-conf true \
  $langdir $dev_data $tgtdir/$decodename || exit 1;
fi
