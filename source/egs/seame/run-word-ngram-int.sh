#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
order=3
third_party_data=/home2/zpz505/seame/formatted/hkust_train 
third_party_data_name=hkust
dev_man=/home2/zpz505/seame/formatted-seame/dev-man
dev_sge=/home2/zpz505/seame/formatted-seame/dev-sge
# end options

. parse_options.sh || exit 1

function Example {
 cat<<EOF
 $0 --steps 1  \
  --third-party-data $third_party_data \
  --third-party-data-name $third_party_data_name \
  --dev-man $dev_man \
   --dev-sge $dev_sge \
   /home2/zpz505/seame/formatted-seame/trainvocab.txt \
     /home2/zpz505/seame/formatted-seame/train \
   /home3/hhx502/w2017/seame-nnet3-feb-06-2017/apsip-word-lm
EOF

}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ $# -ne 3 ]; then
  Example && exit 1
fi

vocab=$1
seamedata=$2
tgtdir=$3

[ -d $tgtdir ] || mkdir -p $tgtdir

inlm=$tgtdir/lm.source.${order}gram.gz
if [ ! -z $step01 ]; then
  ngram-count -order $order -vocab $vocab \
  -text $seamedata -lm $inlm -kndiscount
fi
outlm=$tgtdir/lm.${third_party_data_name}.${order}gram.gz
if [ ! -z $step02 ]; then
  ngram-count -order $order -vocab $vocab \
  -text $third_party_data -lm $outlm -kndiscount
fi

if [ ! -z $step03 ]; then
  echo
  for lambda in $(seq 0.5 0.1 1.0); do
    ppl=$( ngram -order $order -lm $inlm \
      -mix-lm $outlm -lambda $lambda \
      -ppl $dev_man | perl -ane 'chomp; s:file\s+\S+::g; print;')
    echo "$lambda, $(basename $dev_man), $ppl"
  done
  echo
  for lambda in $(seq 0.5 0.1 1.0); do
    ppl=$(ngram -order $order -lm $inlm \
      -mix-lm $outlm -lambda $lambda \
      -ppl $dev_sge | perl -ane 'chomp; s:file\s+\S+::g; print;')
    echo "$lambda, $(basename $dev_sge), $ppl"
  done
fi
