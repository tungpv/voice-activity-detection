#!/usr/bin/perl
use warnings;
use strict;

use utf8;
use open qw(:std :utf8);
my $numArgs = scalar @ARGV;
if ($numArgs != 3) {
  print STDERR "\n cat pron_perutt_nowb | $0 cn_syllable_dict.txt dict_dir text\n\n";
  exit 1;
}
my ($dictfile, $dict_dir, $text) = @ARGV;
sub LoadSyllableDict {
  my($dictfile, $vocab) = @_;
  open(F, "$dictfile") or die "## ERROR: $0, cannot open $dictfile\n";
  while(<F>) {
    chomp;
    my @A = split(/\s+/);
    my $syl = $A[0]; my $pron = "";
    for(my $i = 1; $i < @A; $i++) {
      my $man_phone = sprintf("%s_%s", $A[$i], "man");
      $pron .= "$man_phone ";
    }
    $pron =~ s/ $//g;
    my $aRef = "";
    if(exists $$vocab{$pron}) {
      $aRef = $$vocab{$pron};
    } else {
      $$vocab{$pron} = [my @A];
      $aRef = $$vocab{$pron};
    }
    push(@$aRef, $syl);
  }
  close F;
}
my %vocab = ();
LoadSyllableDict($dictfile, \%vocab);
open(DICT, "|grep -v '<' | sort -u > $dict_dir/lexicon_src.txt") or die "## ERROR: $0, cannot open lexicon_src.txt\n";
open(T, ">$text") or die "## ERROR: $0, text cannot open\n";
my %out_vocab = ();
my %nonsilence_phones = ();
print STDERR "## LOG: $0, stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\t/);
  my $utt = shift @A;
  for(my $i = 0; $i < @A; $i++) {
    my $token = $A[$i];
    next if($token =~ /<s>/ || $token =~ /<\/s>/ || $token =~ /<eps>/);
    my @B = split(/\s+/, $token);
    my $word = shift @B;
    my $pron = "";
    for(my $j = 0; $j < @B; $j ++) {
      $pron .="$B[$j] ";
    }
    $pron =~ s/ $//g;
    if($word =~ /\p{Han}+/) {
      if(not exists $vocab{$pron}) {
        die "## ERROR: $0, unidentified word $word\t", join(" ", @B), "\n";
      }
      my $refC = $vocab{$pron};
      $word = $$refC[0];  ## only get the first word
      $utt .= " $word";
    } else {
      $utt .= " $word";
    }
    my $s = sprintf("%s\t%s", $word, $pron);
    print DICT "$s\n";
  }
  print T "$utt\n";
}
close DICT;
close T;
print STDERR "## LOG: $0, stdin ended\n";
