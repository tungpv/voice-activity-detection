#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo


if [ $# -ne 5 ]; then
  echo
  echo "$0 phone2word_map_csl  phone_text word-dict-dir tgt-dict-dir tgt-phone-lang"
  echo
  exit 1
fi

phone2word_map_csl=$1
phone_text=$2
word_dict_dir=$3
phone_dict_dir=$4
phone_lang=$5

for x in $phone_dict_dir $phone_lang; do
  [ -d $x ] || mkdir -p $x
done 

textfile=$phone_text
cat $textfile | \
  perl -e '($phoneFile) = @ARGV; open(F, "<$phoneFile") or die "## ERROR: file $phoneFile cannot open\n";
    while(<F>) { chomp; m/(\S+)/ or next; $vocab{$1} ++; } close F;
    while(<STDIN>) { m/(\S+)\s+(.*)/ or next; @A = split(/\s+/, $2);
      for($i= 0; $i< @A; $i++) { $s = $A[$i]; if(exists $vocab{$s}) { print $s, "\n";   }  }
    } ' $word_dict_dir/nonsilence_phones.txt |sort -u > $phone_dict_dir/nonsilence_phones.txt || exit 1
cp $word_dict_dir/silence_phones.txt $phone_dict_dir/silence_phones.txt || exit 1
cp $word_dict_dir/optional_silence.txt $phone_dict_dir/optional_silence.txt
cat $phone_dict_dir/silence_phones.txt |awk '{printf("%s ", $0);}END{printf("\n");}' > $phone_dict_dir/extra_questions.txt 
cat $phone_dict_dir/nonsilence_phones.txt | awk '{printf("%s ", $0);}END{printf("\n");}' >> $phone_dict_dir/extra_questions.txt
cat $phone_dict_dir/nonsilence_phones.txt | perl -ane 'chomp; print "$_\t$_\n";' > $phone_dict_dir/dict.1
  (
    A=($(echo "$phone2word_map_csl" |  tr ':' ' ')) 
    num=${#A[@]}
    for x in $(seq 0 $[num-1]); do
      B=($(echo ${A[$x]} | tr ',' ' ')) 
      echo -e "${B[1]}\t${B[0]}"
    done
  ) | cat - $phone_dict_dir/dict.1 > $phone_dict_dir/lexicon.txt
utils/validate_dict_dir.pl  $phone_dict_dir || exit 1
utils/prepare_lang.sh --position-dependent-phones false  $phone_dict_dir "<unk>" $phone_lang/tmp $phone_lang || exit 1

source/egs/quesst/make_word_boundary_int.sh $phone_lang/phones

echo "$0: done"
