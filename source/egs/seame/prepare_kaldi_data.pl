#!/usr/bin/perl

use utf8;
use open qw(:std :utf8);
use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  print STDERR "example: cat transcript.txt | $0 <tgtdir>";
  exit 1;
}

my($tgtdir) = @ARGV;

open(FS, ">$tgtdir/segments") or die "## ERROR: cannot open file $tgtdir/segments\n";
open(FT, ">$tgtdir/text") or die "## ERROR: cannot open file $tgtdir/text\n";
open(FU, ">$tgtdir/utt2spk") or die "## ERROR: cannot open file $tgtdir/utt2spk\n";
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(.*)$/ or next;
  my ($segId, $start, $end, $text) = ($1, $2, $3, $4);
  if($start + 0.1 >= $end) {
    print STDERR "## WARNING: skip $_\n"; 
    next;
  }
  my $spkId;  
  if($segId =~ /^\d/) {
    $segId =~ m/\d+(\w{5})/ or die "## ERROR: illegal segId: $segId\n";
    $spkId = $1;
  } else {
    $segId =~m/(^\w{5})/ or die "## ERROR: illegal segId: $segId\n";
    $spkId = $1;
  }
  $spkId =~ m/[mf]$/ or die "## ERROR: spkid $spkId\n";
  my $uttId = sprintf("%s-%s-%05d-%05d", $spkId, $segId, $start*100, $end*100);
  print FU "$uttId $spkId\n";
  print FT "$uttId $text\n";
  print FS "$uttId $segId $start $end\n";
}
close FS;
close FT;
close FU;
system("utils/utt2spk_to_spk2utt.pl < $tgtdir/utt2spk > $tgtdir/spk2utt");
system("utils/fix_data_dir.sh $tgtdir");
system(" utils/validate_data_dir.sh --no-feats $tgtdir");
print STDERR "$0: stdin ended\n";


