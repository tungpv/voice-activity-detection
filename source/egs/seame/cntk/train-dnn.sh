#!/bin/bash 

. path.sh
. cmd.sh 

echo 
echo "$0 $@"
echo

# begin options
steps=
cmd="slurm.pl --exclude=node01"
cntk_tool=/home2/hhx502/cntk-oct-04-2016/build/release/bin/cntk
dev_data=/home2/hhx502/seame/data/low-dev/fbank-pitch
nj=30
# end options

. parse_options.sh

function Usage {
 cat<<END
 Usage: $0 [options] <source_data> <tgtdir>
 [options]:
 [steps]:
 1: align training data
 [examples]:

 $0 --steps 1 --dev-data $dev_data \
  /home2/hhx502/seame/data /home2/hhx502/seame/exp-cntk-eng15hr

END
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

source_data=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
expdir=$tgtdir
overall_fbank=$source_data/english-train/16k/fbank-pitch
lang=$source_data/lang-sil
srcdir=/home2/hhx502/seame/exp/nnet5a/dnn
alidir=$tgtdir/ali-english-train
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): ali training data @ `date`" 
  steps/nnet/align.sh --cmd "$cmd" --nj $nj \
  $overall_fbank $lang $srcdir $alidir || exit 1
  echo "## LOG (step01, $0): ali done ($alidir) @ `date`"
fi
train_data=$tgtdir/train
valid_data=$tgtdir/valid
if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): subset data @ `date`"
  source/egs/swahili/subset_data.sh --subset-time-ratio 0.1 \
  --random true \
  --data2 $train_data \
  $overall_fbank $valid_data  || exit 1
  
  echo "## LOG (step02, $0): subset data done @ `date`"
fi
left_context=10
right_context=10
raw_feat_dim=$(feat-to-dim scp:$overall_fbank/feats.scp -)
echo "## LOG ($0): raw_feat_dim=$raw_feat_dim"
window_size=$[left_context+right_context+1]
echo "## LOG ($0): window_size=$window_size"
featDim=$[raw_feat_dim*window_size]
echo "## LOG ($0): featDim=$featDim"

feats_tr="ark:copy-feats scp:$train_data/feats.scp ark:- | splice-feats --left-context=$left_context --right-context=$right_context ark:- ark:-|"
feats_cv="ark:copy-feats scp:$valid_data/feats.scp ark:- | splice-feats --left-context=$left_context --right-context=$right_context ark:- ark:-|"
labels_tr="ark:ali-to-pdf $alidir/final.mdl 'ark:gzip -cd $alidir/ali.*.gz |' ark:- | ali-to-post ark:- ark:- |"
labelDim=$(nnet-info $srcdir/final.nnet|egrep 'Softmax' | perl -ane  'm/.*input-dim\s+(\d+),.*/; print $1;') 
echo "## LOG ($0): labelDim=$labelDim"
if [ ! -z $step03 ]; then
  echo "## LOG (step03, $0): prepare data @ `date`"
  (feat-to-len "$feats_tr" ark,t:- > $expdir/cntk_train.counts) || exit 1;
  echo "$feats_tr" > $expdir/cntk_train.feats
  echo "$labels_tr" > $expdir/cntk_train.labels

  (feat-to-len "$feats_cv" ark,t:- > $expdir/cntk_valid.counts) || exit 1;
  echo "$feats_cv" > $expdir/cntk_valid.feats
  echo "$labels_tr" > $expdir/cntk_valid.labels
  for (( c=0; c<labelDim; c++)) ; do
    echo $c
  done >$expdir/cntk_label.mapping
  echo "## LOG (step03, $0): data preparation done @ `date`"
fi
if [ ! -z $step04 ]; then
  cntk_config=/home2/hhx502/Kaldi_CNTK_AMI/cntk_config
  cat $cntk_config/CNTK2.config | grep -v Truncated > $tgtdir/CNTK2.config
  cp $cntk_config/default_macros.ndl $tgtdir/
  cp $cntk_config/dnn_6layer.ndl $tgtdir/
fi
if [ ! -z $step05 ]; then
  echo "## LOG (step05, $0): copy cntk configure files"
  ndlfile=$tgtdir/dnn_6layer.ndl

tee $tgtdir/Base.config <<EOF
ExpDir=$tgtdir
logFile=train_cntk.log
modelName=cntk.dnn
labelDim=${labelDim}
featDim=${featDim}
labelMapping=${expdir}/cntk_label.mapping
featureTransform=NO_FEATURE_TRANSFORM
inputCounts=${expdir}/cntk_train.counts
inputFeats=${expdir}/cntk_train.feats
inputLabels=${expdir}/cntk_train.labels
cvInputCounts=${expdir}/cntk_valid.counts
cvInputFeats=${expdir}/cntk_valid.feats
cvInputLabels=${expdir}/cntk_valid.labels
EOF
  $cntk_tool configFile=${expdir}/Base.config configFile=${expdir}/CNTK2.config DeviceNumber=0 action=TrainDNN ndlfile=$ndlfile
  echo "## LOG (step05, $0): done ($expdir)"
fi
config_write=$expdir/CNTK2_write.config
cntk_model=$expdir/cntk.dnn.20
action=write
graphdir=/home2/hhx502/seame/exp/tri4a/graph
if [ ! -z $step06 ]; then
  echo "## LOG (step06, $0): prepare data for decoding"
  cp /home2/hhx502/Kaldi_CNTK_AMI/cntk_config/CNTK2_write.config $expdir/
  cp $alidir/final.mdl $expdir/
  cntk_write_cmd="$cntk_tool configFile=$config_write DeviceNumber=auto modelName=$cntk_model labelDim=$labelDim featDim=$featDim action=$action Expdir=$expdir"
  echo "$cntk_write_cmd" > $expdir/cntk_write_cmd
  labels_tr_pdf="ark:ali-to-pdf $alidir/final.mdl 'ark:gzip -cd $alidir/ali.*.gz |' ark:- |"
  analyze-counts --verbose=1 --binary=false "$labels_tr_pdf" $tgtdir/ali_train_pdf.counts
  echo "## LOG (step06, $0): preparation done"
fi

decode_dir=$tgtdir/decode_dev
if [ ! -z $step07 ]; then
  echo "## LOG (step07, $0): decoding @ `date`"
  cntk_write_cmd="$cntk_tool configFile=$config_write DeviceNumber=-1 modelName=$cntk_model labelDim=$labelDim featDim=$featDim action=$action Expdir=$expdir"
  source/egs/seame/cntk/decode_cntk2.sh --cmd "$cmd" \
  --nj $nj \
  --acwt 0.0833 \
  $graphdir $dev_data $decode_dir "$cntk_write_cmd" || exit 1
  echo "## LOG (step07, $0): decoding done ($decode_dir) @ `date`"
fi
