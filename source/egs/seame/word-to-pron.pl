#!/usr/bin/perl

use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 2 ) {
  die "\nExample: cat syllable-ctm | $0 word-ctm  0.8\n\n";
}
# begin sub
sub Insert {
  my ($vocab, $A) = @_;
  my $uttId = shift @$A;
  shift @$A;
  die "## ERROR: $0, Insert, bad line: ", join(" ", @$A), "\n" if @$A != 3; 
  my $refA;
  if(not exists $$vocab{$uttId}) {
    $$vocab{$uttId} = [my @B];
    $refA = $$vocab{$uttId};
  } else {
    $refA = $$vocab{$uttId};
  }
  push @$refA, $A;
}
#
sub InsertPronOfWord {
  my ($vocab, $word, $pronA) = @_;
  my $pron = join(" ", @$pronA);
  $$vocab{$word}{$pron} ++;
}
#
sub AlignWord {
  my ($vocab, $wordLine, $A, $starti, $overlap_rate) = @_;
  my $word = $$wordLine[2];
  my $word_start = $$wordLine[0];
  my $word_end = $$wordLine[0] + $$wordLine[1]; 
  my $word_dur = $$wordLine[1];
  my @pronA = ();
  for(my $i = $$starti; $i < @$A; $i ++) {
    my $eleA = $$A[$i];
    my $phone = $$eleA[2];
    my $phone_start = $$eleA[0];
    my $phone_end = $$eleA[0] + $$eleA[1];
    my $phone_dur = $$eleA[1];
    next if($phone_end <= $word_start);
    # print STDERR "$phone, $phone_start, $phone_end\n";
    if($word_start < $phone_end && $phone_start < $word_end) {
      my $overlap_start = $phone_start;
      if($phone_start < $word_start) {
        $overlap_start = $word_start;
      }
      my $overlap_end = $phone_end;
      if($phone_end > $word_end) {
        $overlap_end = $word_end;
      }
      my $overlap_dur = $overlap_end - $overlap_start;
      die "## ERROR: $0, overlap ($overlap_dur) is less than zero\n" if $overlap_dur <= 0;
      if($overlap_dur/$phone_dur >= $overlap_rate) {
        push @pronA, $phone;
        $$starti = $i + 1;
      } else {
        $$starti = $i;
      }
    } else {
      last if($phone_start >= $word_end); 
    }
  }
  push (@pronA, "<eps>") if (scalar @pronA == 0);
  InsertPronOfWord($vocab, $word, \@pronA);
}
#
sub Align {
  my ($vocab, $ali_vocab, $uttId, $A, $overlap_rate) = @_;
  if (not exists $$vocab{$uttId}) {
    print STDERR "## WARNING, $0, no utterance $uttId\n";
    return;
  }
  my $wordA = $$vocab{$uttId};
  my $j = 0;
  for(my $i = 0; $i < scalar @$wordA; $i ++) {
    AlignWord($ali_vocab, $$wordA[$i], $A, \$j, $overlap_rate);
  }
}
# end sub
my ($word_ctm, $overlap_rate) = @ARGV;
my %word_ctm_vocab = ();
my %ali_vocab = ();
open(F, "$word_ctm") or die "## ERROR: $0, file $word_ctm cannot open\n";
while(<F>) {
  chomp;
  my @A = split(/\s+/);
  die "## ERROR: $0, bad line $_\n" if @A != 5;
  Insert(\%word_ctm_vocab, \@A);
}
print STDERR "## LOG: $0, stdin expected\n";
my $prevUttId = "";
my @Ali = ();
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  my $uttId = shift @A; 
  shift @A;
  die "## ERROR: $0, bad line: ", join(" ", @A), "\n" if (@A != 3);
  if($uttId ne $prevUttId && $prevUttId ne "" ) {
    Align(\%word_ctm_vocab, \%ali_vocab, $prevUttId, \@Ali, $overlap_rate);
    @Ali = ();
    $prevUttId = "";
  }
  push @Ali, \@A;
  $prevUttId = $uttId;
}
if ($prevUttId ne "") {
  Align(\%word_ctm_vocab, \%ali_vocab, $prevUttId, \@Ali, $overlap_rate);
}
print STDERR "## LOG: $0, stdin ended \n";

foreach my $word (keys %ali_vocab) {
  print "$word\t";
  foreach my $pron (keys %{$ali_vocab{$word}}) {
    print "$pron $ali_vocab{$word}{$pron}\t";
  }
  print "\n";
}
