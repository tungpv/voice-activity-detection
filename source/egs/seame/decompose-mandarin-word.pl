#!/usr/bin/perl -w
use utf8;
use open qw(:std :utf8);
use lib qw(source/egs/myperllib);
use LocalMandarinWord;
use Getopt::Long;

print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  chomp;
  my $text = $_;
  DecomposeMandarinWord(\$text);
  print "$text\n";
}
print STDERR "## LOG ($0): stdin ended\n";
