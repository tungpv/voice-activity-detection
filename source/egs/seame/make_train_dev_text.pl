#!/usr/bin/perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  print STDERR "\n Example: cat text | utils/shuffle_list.pl | $0 1000 lmdir\n\n";
  exit 1;
}

my ($numLine, $lmdir) = @ARGV;
open(TRAIN, "|gzip -c > $lmdir/train-text.gz") or die "## ERROR: $0, cannot open train-text.gz to write\n";
open(DEV, "|gzip -c > $lmdir/dev-text.gz") or die "## ERROR: $0, cannot open dev-text.gz to write\n";
my $countLine = 0;
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  if($countLine < $numLine) {
    print DEV "$2\n";
  } else {
    print TRAIN "$2\n";
  }
  $countLine ++;
}
close TRAIN;
close DEV;
