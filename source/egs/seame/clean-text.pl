#!/usr/bin/perl
use utf8;
use open qw(:std :utf8);
while(<STDIN>) {
     chomp;
     s#\(ppl\)#verbalnoise#g;
     s#\(ppb\)#verbalnoise#g;
     # s#ppl#verbalnoise#g;
     s#\(ppc\)#verbalnoise#g;
     # s#ppc#verbalnoise#g;
     s#\(ppo\)#verbalnoise#g;
     s#<un[^<]+>#tmpunknownword#g;
     # s#ppo#verbalnoise#g;
     @A = split(/\s+/); $utt = $A[0]; $start = $A[1]; $end = $A[2];
     shift @A; shift @A; shift @A; $words = join(" ", @A);
     $words =~ s/[\']/singlequote/g;
     $words =~ s/[[:punct:]]/ /g;
     @A = split(/\s+/, $words);  $u = ""; $total_nonword = 0;
     for($i = 0; $i < @A; $i ++) { $w = $A[$i];
       $uw = lc $w;
       if( $uw ne "ppb" && $uw ne "ppl" && $uw ne "ppc" && $uw ne "ppo") {
         $u .= " $w";
       }
       if($w eq "verbalnoise" || $w eq "tmpunknownword" ) { $total_nonword ++; } 
     }
     $num_word = scalar @A; 
     if($total_nonword > 0.5*$num_word ) {
       ## print STDERR "$_\n"; 
       next;
     }
     $u =~ s/\s+/ /g;  $u =~ s/^ //; $u =~ s/ $//;
     $_ = "$utt $start $end ". lc $u . "\n";
     s#verbalnoise# <v-noise> #g;
     s#singlequote#'#g;
     s#tmpunknownword#<unk>#g;
     print;
   }

