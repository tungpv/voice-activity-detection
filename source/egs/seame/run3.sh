#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
cmd="slurm.pl --exclude=node02"
nj=40
steps=
source_lexicon=/home2/hhx502/sg-en-i2r/data/local/dict/lexicon.txt
# end options

function Usage {
 cat<<END
 Usage: $0 [options] <source_data_dir> <tgtdir>
 [options]
 --steps                          # value, "$steps"
 --source-lexicon                 # value, "$source_lexicon"
 [examples]:

 $0 --steps 1  --source-lexicon $source_lexicon  /data/users/hhx502/seame  /home2/hhx502/seame

END
}

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  Usage && exit 1; 
fi

source_data_dir=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

overall_data=$tgtdir/data/overall-data

if [ ! -z $step01 ]; then
  cat $overall_data/text | \
  source/egs/seame/get-mandarin-english-rate.pl $overall_data/utt2spk  $overall_data/spk2rate
fi

train_data=$tgtdir/data/train
low_dev=$tgtdir/data/low-dev
high_dev=$tgtdir/data/high-dev
if [ ! -z $step02 ]; then
  utils/subset_data_dir.sh --spk-list $overall_data/lower-spklist $overall_data $low_dev
  utils/fix_data_dir.sh $low_dev
  utils/subset_data_dir.sh --spk-list $overall_data/higher-spklist $overall_data $high_dev
  utils/fix_data_dir.sh $high_dev
  [ -d $train_data ] || mkdir -p $train_data
  awk '{print $1;}' $overall_data/spk2utt | \
  perl -e '($uttlist) = @ARGV; open(F, "$uttlist") or die "## ERROR: $uttlist cannot open\n";
    while(<F>) {chomp; m/(\S+)/ or next; $vocab{$1} ++; } close F;
    while(<STDIN>){chomp; m/(\S+)/ or next; if (not exists $vocab{$1}){print "$1\n";}}
  '  "cat $overall_data/{lower-spklist,higher-spklist} |"  > $train_data/uttlist
  utils/subset_data_dir.sh --spk-list $train_data/uttlist $overall_data $train_data
  utils/fix_data_dir.sh $train_data
fi
lang_dir=$tgtdir/data/lang
lmdir=$tgtdir/data/local/lm-kaldi
lmtype=3gram-mincount
if [ ! -z $step03 ]; then
  source/egs/fisher-english/train-kaldi-lm.sh --lmtype $lmtype  $tgtdir/data/local/dict/lexicon.txt \
  $train_data $low_dev  $lmdir
  arpa_lm_dir=$lmdir/$lmtype
  [ -f $arpa_lm_dir/lm_unpruned.gz ] || \
  { echo "## ERROR:  lm_unpruned.gz expected"; exit 1; }
  source/egs/fisher-english/arpa2G.sh $arpa_lm_dir/lm_unpruned.gz $lang_dir $lang_dir
fi

if [ ! -z $step04 ]; then
  for sdata in $train_data $low_dev $high_dev; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd"  --nj $nj  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc16k.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    echo "## LOG: done ($feat)"
    data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    echo "## LOG: done ($feat)"
  done
fi
expdir=$tgtdir/exp
if [ ! -z $step05 ]; then
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd "$cmd" --nj $nj \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 6000 --pdf-num 90000 \
  --decodename decode-low-dev \
  --devdata $low_dev/mfcc-pitch \
  $train_data/mfcc-pitch $lang_dir $expdir || exit 1
fi
if [ ! -z $step06 ]; then
  source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 \
  --delta-opts "--delta-order=2" --cmvn-opts "--norm-means=true" \
  --use-partial-data-to-pretrain true \
  --pretraining-rate 0.5 \
  --nnet-pretrain-cmd "/home/hhx502/w2016/steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048"  \
  --nnet-id nnet5a --graphdir $expdir/tri4a/graph --devdata $low_dev/fbank-pitch \
  --decodename decode-low-dev \
  $train_data/fbank-pitch $lang_dir $expdir/tri4a/ali_train  $expdir || exit 1
fi
