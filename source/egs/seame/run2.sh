#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
cmd="slurm.pl --exclude=node02"
nj=40
steps=
source_lexicon=/home2/hhx502/sg-en-i2r/data/local/dict/lexicon.txt
# end options

function Usage {
 cat<<END
 Usage: $0 [options] <source_data_dir> <tgtdir>
 [options]
 --steps                          # value, "$steps"
 --source-lexicon                 # value, "$source_lexicon"
 [examples]:

 $0 --steps 1  --source-lexicon $source_lexicon  /data/users/hhx502/seame  /home2/hhx502/seame

END
}

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  Usage && exit 1; 
fi
source_data_dir=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

localdir=$tgtdir/data/local
[ -d $localdir ] || mkdir -p $localdir
if [ ! -z $step01 ]; then
  echo "## step01, lowercase file name @ `date`"
  for SRC in `find $source_data_dir -depth`
do
    DST=`dirname "${SRC}"`/`basename "${SRC}" | tr '[A-Z]' '[a-z]'`
    if [ "${SRC}" != "${DST}" ]
    then
        [ ! -e "${DST}" ] && mv -T "${SRC}" "${DST}" || echo "${SRC} was not renamed"
    fi
done 
  echo "## step01, done @`date`"
fi
overall_data=$localdir/overall-data-2
if [ ! -z $step02 ]; then
  echo "## step02, collect wave and transcript file lists @ `date`"
  [ -d $overall_data ] || mkdir -p $overall_data
  find  $source_data_dir/phasei/original  $source_data_dir/phaseii/transcribing  -name "*.wav" | \
  perl -e 'while(<STDIN>){chomp; m/([^\/]+)\.wav/; if(not exists $vocab{$1}){ print "$_\n"; $vocab{$1} ++;}  }' | sort -u > $overall_data/wavlist.txt
  find /data/users/hhx502/seame/ldc-transcription  -name "*.txt" | sort -u > $overall_data/translist.txt
  cat $overall_data/wavlist.txt | \
  perl -e 'while(<STDIN>){chomp; m/([^\/]+)\.wav/;print $1 . " $_\n";  }' > $overall_data/wav.scp
  for f in $(cat $overall_data/translist.txt); do
    cat $f | perl -ane 'chomp; m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(.*)/g or next; $fileid = lc $1; $start = sprintf("%.2f", $2/1000); $end = sprintf("%.2f", $3/1000);  print $fileid . " $start $end $5\n"; '
  done  > $overall_data/transcript.txt
  echo "## step02, done ($overall_data/wavlist.txt, $overall_data/translist.txt, $overall_data/transcript.txt ) @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "## LOG: step03, transcript preparation @ `date`"
  cat $overall_data/transcript.txt | \
  perl -e 'use utf8; use open qw(:std :utf8); 
    while(<STDIN>){ @A = split(/\s+/); if(@A <= 3){next;} 
    $start = $A[1]; $end = $A[2];  $utt = $A[0] . " " . $start . " " . $end;  
   for($i = 3; $i < @A; $i ++) { $w = $A[$i];
     if($w =~ m/\p{Han}+/) {
       $s = $w;
       $start = 0;  $tot_len = length($s);
       while($s =~ m/(\p{Han}+)/g) {
         $ms = $1;  $len = length($ms);  $end = pos($s); $new_start = $end - $len;
         if($new_start > $start) {
           $sub_s = substr($s, $start, $new_start - $start);
           if($sub_s ne "[" && $sub_s ne "]" && $sub_s ne "<" && $sub_s ne ">"  ) {
             $utt .= " " . $sub_s;
           } 
         }
         $start = $end;
         @B = split("", $ms); $utt .= " " . join(" ", @B);
       }
       if($start > 0 && $start < $tot_len) {
         $sub_s = substr($s, $start, $tot_len - $start);
         if($sub_s ne "[" && $sub_s ne "]" &&  $sub_s ne "<" && $sub_s ne ">") {
           $utt .= " " .  $sub_s;
         }
       }
     } else {
       if($w ne "[" && $w ne "]" && $w ne ">" && $w ne "<") { $utt .= " $w";  }
     }
   }
   print "$utt\n";
  }'   | \
  grep -v "<unk" | \
  grep -v "<unl" | \
  source/egs/seame/clean-text.pl | \
  source/egs/seame/transform-text.pl 3 ' ' $overall_data/transform-dict-edited.txt  > $overall_data/transcript.1.txt 

  cat $overall_data/transcript.1.txt | perl -ane 'chomp; @A = split(/\s+/);
    for($i=3; $i < @A; $i++){$w = $A[$i];  if(not exists $vocab{$w}){ print "$w\t$w\n"; $vocab{$w}++; } }
  '| sort -u > $overall_data/transform-dict.txt
  echo "## LOG: step02, done ($overall_data/transform-dict.txt) @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## step04: get oov status with a given lexicon ($source_lexicon)"
  cat $overall_data/transcript.1.txt | \
  perl -e ' use utf8; use open qw(:std :utf8);
   ($from, $lexFile) = @ARGV; open(L, "$lexFile") or die "## ERROR: file $lexFile cannot open\n";
   while(<L>) {chomp; m/(\S+)\s+(.*)/ or die "## bad lexicon line $_\n"; $vocab{lc $1} ++; } close L;
   while(<STDIN>) { chomp; @A = split(/\s+/);
     for($i=$from-1; $i < @A; $i++) {
       $word = $A[$i];
       if(not exists $vocab{$word}) {
         if(not exists $oov{$word}) { $oov{$word} ++; print "$word\t$word\n"; }
       }
     }
   }
  ' 4 $source_lexicon  | sort -u > $overall_data/oov-dict.txt
  echo "## step04: done ($overall_data/oov-dict.txt)"
fi
g2pdir=$tgtdir/data/local/g2p-model-2
if [ ! -z $step05 ]; then
  echo "## step05, train g2p model"
  [ -d $g2pdir ] || mkdir -p $g2pdir
  cat $source_lexicon | \
  perl -e 'use utf8; use open qw(:std :utf8);
    while(<STDIN>) { chomp; next if(/[<]/ || /\p{Han}+/g); @A = split(/\s+/); $w = lc $A[0]; shift @A;   print $w, "\t", join(" ", @A), "\n" ;  }' > $g2pdir/lex.txt 
  source/egs/g2p/run-g2p.sh --steps 1,2,3,4 $g2pdir/lex.txt $g2pdir
  echo "## step05, g2p model training done @ `date`"
fi

if [ ! -z $step06 ]; then
  echo "## step06, transform text again @ `date`"
  cat $overall_data/transcript.1.txt | \
  source/egs/seame/transform-text-2.pl 3 ' ' $overall_data/oov-dict-edited.txt  > $overall_data/transcript.2.txt 
  
  cat $overall_data/transcript.2.txt | \
  perl -e ' use utf8; use open qw(:std :utf8);
   ($from, $lexFile) = @ARGV; open(L, "$lexFile") or die "## ERROR: file $lexFile cannot open\n";
   while(<L>) {chomp; m/(\S+)\s+(.*)/ or die "## bad lexicon line $_\n"; $vocab{lc $1} ++; } close L;
   while(<STDIN>) { chomp; @A = split(/\s+/);
     for($i=$from-1; $i < @A; $i++) {
       $word = $A[$i];
       if(not exists $vocab{$word}) {
         if(not exists $oov{$word}) { $oov{$word} ++; print "$word\t$word\n"; }
       }
     }
   }
  ' 4 $source_lexicon  | sort -u > $overall_data/oov-dict-2.txt
  g2p.py --model /home2/hhx502/seame/data/local/g2p-model-2/model-3 \
  --apply  /home2/hhx502/seame/data/local/overall-data-2/oov-dict-2-list.txt > $overall_data/oov-g2p.txt
  cat $overall_data/oov-g2p.txt $source_lexicon | sort -u \
  > $overall_data/lexicon.txt
  echo "## step06, done ($overall_data) @ `date`"
fi
overall_data_dir=$tgtdir/data/overall-data
if [ ! -z $step07 ]; then
  echo "## prepare overall data @ `date`"
  [ -d $overall_data_dir ] || mkdir -p $overall_data_dir
  cat $overall_data/transcript.2.txt | \
  source/egs/seame/prepare_kaldi_data.pl  $overall_data_dir || exit 1
  echo "## done @ `date`"
fi
train_data=$tgtdir/data/train
dev_data=$tgtdir/data/dev
if [ ! -z $step08 ]; then
  echo "## step08, prepare training and dev data"
  [ -d $dev_data ] || mkdir -p $dev_data
  awk '{print $1;}' $overall_data_dir/spk2utt | \
  utils/shuffle_list.pl  | tail -n +10 | head > $dev_data/uttlist
  utils/subset_data_dir.sh --spk-list $dev_data/uttlist $overall_data_dir $dev_data
  utils/fix_data_dir.sh $dev_data
  [ -d $train_data ] || mkdir -p $train_data
  awk '{print $1;}' $overall_data_dir/spk2utt | \
  perl -e '($uttlist) = @ARGV; open(F, "$uttlist") or die "## ERROR: $uttlist cannot open\n";
    while(<F>) {chomp; m/(\S+)/ or next; $vocab{$1} ++; } close F;
    while(<STDIN>){chomp; m/(\S+)/ or next; if (not exists $vocab{$1}){print "$1\n";}}
  '  $dev_data/uttlist  > $train_data/uttlist
  utils/subset_data_dir.sh --spk-list $train_data/uttlist $overall_data_dir $train_data
  utils/fix_data_dir.sh $train_data
  echo "## step08, done"
fi
tgtdict_dir=$tgtdir/data/local/dict
lang_dir=$tgtdir/data/lang
if [ ! -z $step09 ]; then
  echo "## step09, prepare lang @ `date`"
  [ -d $tgtdict_dir ] || mkdir -p $tgtdict_dir
  source_dict_dir=$(dirname $source_lexicon)
  [ -d $tgtdict_dir ] || mkdir -p $tgtdict_dir
  cat $overall_data/lexicon.txt | \
  perl -ane 'use utf8; use open qw(:std :utf8); 
     chomp; @A = split(/\s+/); $w = shift @A; $w= lc $w; 
     print $w, "\t", join(" ", @A), "\n";' > $tgtdict_dir/lexicon.txt
  cp $source_dict_dir/{extra_questions.txt,nonsilence_phones.txt,optional_silence.txt,silence_phones.txt} $tgtdict_dir/
  utils/validate_dict_dir.pl $tgtdict_dir
  utils/prepare_lang.sh $tgtdict_dir "<unk>" $lang_dir/tmp $lang_dir
  echo "## step09, done @ `date`"
fi
lmdir=$tgtdir/data/local/lm
lmtype=3gram-mincount
if [ ! -z $step10 ]; then
  echo "## step10: make lm @ `date`"
  source/egs/fisher-english/train-kaldi-lm.sh --lmtype $lmtype  $tgtdict_dir/lexicon.txt \
  $train_data $dev_data  $lmdir
  arpa_lm_dir=$lmdir/$lmtype
  [ -f $arpa_lm_dir/lm_unpruned.gz ] || \
  { echo "## ERROR:  lm_unpruned.gz expected"; exit 1; }
  source/egs/fisher-english/arpa2G.sh $arpa_lm_dir/lm_unpruned.gz $lang_dir $lang_dir

  echo "## step10: done"
fi
if [ ! -z $step11 ]; then
  echo "## step11: mfcc & fbank @ `date`"
  for sdata in $train_data $dev_data; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch
    source/egs/swahili/make_feats.sh --cmd slurm.pl --nj 20  --mfcc-for-ivector true \
    --mfcc-cmd "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc16k.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    echo "## LOG: done ($feat)"
    data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd slurm.pl --nj 20  --fbank-pitch true \
    --fbank-pitch-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    echo "## LOG: done ($feat)"
  done
  echo "## step11, mfcc & fbank done `date`"
fi
expdir=$tgtdir/exp
if [ ! -z $step12 ]; then
  echo "## step12: gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd "$cmd" --nj $nj \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 6000 --pdf-num 90000 \
  --devdata $dev_data/mfcc-pitch \
  $train_data/mfcc-pitch $lang_dir $expdir || exit 1
  echo "## step12: gmm-hmm done `date`"
fi
if [ ! -z $step13 ]; then
  echo "## step13: dnn-hybrid @ `date`"
  source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 \
  --delta-opts "--delta-order=2" --cmvn-opts "--norm-means=true" \
  --use-partial-data-to-pretrain true \
  --pretraining-rate 0.5 \
  --nnet-pretrain-cmd "/home/hhx502/w2016/steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048"  \
  --nnet-id nnet5a --graphdir $expdir/tri4a/graph --devdata $dev_data/fbank-pitch \
  $train_data/fbank-pitch $lang_dir $expdir/tri4a/ali_train  $tgtdir/exp || exit 1

  echo "## step13: dnn-hybrid done @ `date`"
fi
