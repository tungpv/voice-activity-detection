#!/usr/bin/perl 
use utf8;
use open qw(:std :utf8);

print STDERR "\n## LOG: " . $0 . ' ' . join(" ", @ARGV) . "\n";
$num_args = scalar @ARGV;
if ($num_args != 3) {
  print STDERR "\nUsage: ";
  print STDERR "cat transcript | $0 <from-field> <empty-word> <word-transfer-dict>\n";
  print STDERR "\n"; 
  exit 1;
}
($from, $empty_word, $dict, $text) = @ARGV;
open(D, "$dict") or die "## ERROR: dict $dict cannot open\n";
while(<D>) {
  chomp;
  @A = split(/\s+/);  $word = lc $A[0]; 
  if(@A == 1) {
    $vocab{$word} = '';
  } else {
    shift @A;
    $vocab{$word} = lc join(" ",  @A);
  }
}
close D;
print STDERR "stdin expected\n";
while(<STDIN>) {
  chomp;
  @A = split(/\s+/);
  $utt = $A[0];
  next if @A <= $from;
  for($i = 1; $i < $from; $i++) {
    $utt .= " $A[$i]";
  }
  for($i = $from; $i < @A; $i++) {
    $w = $A[$i];
    die "## ERROR: word $w is not identified $_\n" if not exists $vocab{$w};
    $w = $vocab{$w}; 
    $utt .= " $w";
  }
  print "$utt\n";
}
print STDERR "stdin ended\n";
