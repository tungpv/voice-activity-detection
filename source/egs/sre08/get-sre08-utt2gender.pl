#!/usr/bin/perl
use strict;
use warnings;

my $numArgs = scalar @ARGV;
if ($numArgs ne 2 ) {
  my $example = <<EOU;
  [examples]:
  
  $0  /data/users/ellenrao/NIST_SRE_Corpus/keyfiles_NIST2008to2010/NIST_SRE08_KEYS.v0.1/model-keys/NIST_SRE08_short2.model.key  /data/users/ellenrao/NIST_SRE_Corpus/keyfiles_NIST2008to2010/NIST_SRE08_KEYS.v0.1/trial-keys/NIST_SRE08_short2-short3.trial.key
 
EOU
  print STDERR $example;  exit 1;
}
# begin sub
my ($train_short2_modelkey, $short2_short3_trial_key) = @ARGV;

sub GetModelIdToGender {
  my ($vocab, $train_short2_modelkey) = @_;
  open(F, "grep -v model_id $train_short2_modelkey|") or die "## ERROR: train-short2-modelkey $train_short2_modelkey cannot open\n";
  while(<F>) {
    chomp;
    my @A = split(/,/);
    $$vocab{$A[0]} = $A[1];
  }
  close F;
}
sub GetModelIdToTrialInfo {
  my ($vocab, $short2_short3_trail_key) = @_;
  open(F, "grep -v model_id $short2_short3_trial_key|") or die "## ERROR: short2-short3-trial-key $short2_short3_trial_key cannot open\n";
  while(<F>) {
    chomp;
    my @A = split(/,/);
    my $modelId = $A[0];
    my $side = uc $A[2];
    my $trial = sprintf("%s_%s",  $A[1], $side);
    my $aRef;
    if(not exists $$vocab{$modelId}) {
      my @B = ();
      $$vocab{$modelId} = [@B];
      $aRef = $$vocab{$modelId};
    } else {
      $aRef = $$vocab{$modelId};
    }
    push (@$aRef, $trial);
  }
  close F;
}
# end sub
my %modelId_to_gender_vocab = ();
GetModelIdToGender(\%modelId_to_gender_vocab, $train_short2_modelkey);
my %modelId_to_trial_utt_vocab = ();
GetModelIdToTrialInfo(\%modelId_to_trial_utt_vocab, $short2_short3_trial_key);

foreach my $modelId (keys %modelId_to_trial_utt_vocab) {
  die "## ERROR: unknown modelId $modelId\n" if not exists $modelId_to_gender_vocab{$modelId};
  my $gender = lc $modelId_to_gender_vocab{$modelId};
  my $aRef = $modelId_to_trial_utt_vocab{$modelId};
  for(my $i = 0; $i < @$aRef; $i++) {
    my $trial = $$aRef[$i];
    print "$trial $gender\n";
  }
}
