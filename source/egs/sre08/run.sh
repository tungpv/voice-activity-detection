#!/bin/bash

. path.sh
. cmd.sh 

echo
echo "$0 $@"
echo 

# begin options
steps=
cmd="slurm.pl --exclude=node01,node02,node03,node04,node05"
nj=20
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 $0 [options] <tgtdir>
 [options]:
 --steps                                    # value, "$steps"
 --cmd                                      # value, "$cmd"
 --nj                                       # value, "$nj"
 [steps]:
 [examples]:

 $0 --steps 1 /home2/hhx502/sre08
 
 $0 --steps 11 --cmd "$cmd" --nj $nj \
 /home2/hhx502/sre08 

 $0 --steps 24 --cmd "$cmd" --nj $nj \
 /home2/hhx502/sre08 

END
}

if [ $# -ne 1 ]; then
  Usage && exit 1
fi

tgtdir=$1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

fisher_data=$tgtdir/data/fisher
if [ ! -z $step01 ]; then
  echo "## step01, prepare fisher data "
  local/make_fisher.sh /data/users/hhx502/ldc-cts2016/fishereng-part1-ldc2004s13 \
  /data/users/hhx502/ldc-cts2016/LDC2004T19 ${fisher_data}1 
  local/make_fisher.sh /data/users/hhx502/ldc-cts2016/fishereng-part2-ldc2005s13 \
  /data/users/hhx502/ldc-cts2016/LDC2005T19 ${fisher_data}2
  echo "## step02, done" 
fi
sre05_test=$tgtdir/data/sre05-test
if [ ! -z $step02 ]; then
  echo "## step02, prepare sre 2005 test"
  source_data_dir=/data/users/ellenrao/NIST_SRE_Corpus/NIST_SRE_2005/r101_1_1/test
  local/make_sre_2005_test.pl $source_data_dir $tgtdir/data/local/sre05-test \
  $sre05_test
  echo "## step02, done ($sre05_test)"
fi
sre04_test01=$tgtdir/data/sre04-test01
keydir=$tgtdir/data/local/sre04_test01
if [ ! -z $step03 ]; then
  echo "## step03, prepare sre04 test"
  source_data_dir=/data/users/ellenrao/NIST_SRE_Corpus/NIST_SRE_2004/DVD5_of_6/sp04-05/test
  local/make_sre_2004_test.pl \
  $source_data_dir $keydir $sre04_test01 
  echo "## step03, done ($keydir)"
fi
sre04_test02=$tgtdir/data/sre04-test02
keydir=$tgtdir/data/local/sre04_test02
if [ ! -z $step04 ]; then
  echo "## step02, prepare sre04 test2"
  source_data_dir=/data/users/ellenrao/NIST_SRE_Corpus/NIST_SRE_2004/DVD6_of_6/sp04-06/test
  local/make_sre_2004_test.pl \
  $source_data_dir $keydir $sre04_test02 
  echo "## step02, done ($sre04_test02)"
fi
sre06_train=$tgtdir/data/sre06-train
if [ ! -z $step05 ]; then
  echo "## step05, prepare sre2006 train data"
  [ -d $sre06_train ] || mkdir -p $sre06_train
  source_data_dir=/data/users/ellenrao/NIST_SRE_Corpus/NIST_SRE_2006/r108_1_1/train
  source/egs/sre08/make_sre_2006_train.pl $source_data_dir $sre06_train
  echo "## step05, done ($sre06_train)"
fi
sre05_train=$tgtdir/data/sre05-train
if [ ! -z $step06 ]; then
  echo "## step06, prepare sre2005 train data"
  source_data_dir=/data/users/ellenrao/NIST_SRE_Corpus/NIST_SRE_2005/r101_1_1/train
  [ -d $sre05_train ] || mkdir -p $sre05_train
  source/egs/sre08/make_sre_2005_train.pl  $source_data_dir $sre05_train || exit 1
  echo "## step06, done ($sre05_train)"
fi
swbd1=$tgtdir/data/swbd_cellular1_train
if [ ! -z $step07 ]; then
  echo "## step07, lowercase the data"
  source_data_dir=/data/users/hhx502/swb-sre 
  for SRC in `find $source_data_dir -depth`; do
    DST=`dirname "${SRC}"`/`basename "${SRC}" | tr '[A-Z]' '[a-z]'`
    if [ "${SRC}" != "${DST}" ]; then
      [ ! -e "${DST}" ] && mv -T "${SRC}" "${DST}" || echo "${SRC} was not renamed"
    fi
  done 
  echo "## step08, done ($swbd1)"
fi
if [ ! -z $step08 ]; then
  echo "## step08, prepare swbd_cellular1_train"
  [ -d $wbd1 ] || mkdir -p $swbd1
  source_data_dir=/data/users/hhx502/swb-sre/switchboard-1-ldc2001s13
  source/egs/sre08/make_swbd_cellular1.pl $source_data_dir  $swbd1
  echo "## step08, done ($swbd1)"
fi

swbd2=$tgtdir/data/swbd_cellular2_train
if [ ! -z $step09 ]; then
  echo "## step09, prepare swbd_cellular2_train"
  source_data_dir=/data/users/hhx502/swb-sre/switchboard-cell-p2-ldc2004s07
  [ -d $swbd2 ] || mkdir -p $swbd2
  source/egs/sre08/make_swbd_cellular2.pl $source_data_dir  $swbd2
  echo "## step09, done ($swbd2)"
fi
train=$tgtdir/data/train
if [ ! -z $step10 ]; then
  echo "## step10, prepare overall training data"
  utils/combine_data.sh $train ${fisher_data}1 ${fisher_data}2 \
  $swbd1 $swbd2  $sre05_train/sre05_train_3conv4w_female \
  $sre05_train/sre05_train_8conv4w_female \
  $sre06_train/sre06_train_3conv4w_female \
  $sre06_train/sre06_train_8conv4w_female \
  $sre05_train/sre05_train_3conv4w_male $sre05_train/sre05_train_8conv4w_male \
  $sre06_train/sre06_train_3conv4w_male $sre06_train/sre06_train_8conv4w_male \
  $sre04_test01 $sre04_test02 $sre05_test
  echo "## step10, done ($train)"
fi
local_data=/local/hhx502/sre08/data
if [ ! -z $step11 ]; then
  echo "## step11, prepare feature for training data @ `date`"
  source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj \
  --mfcc-for-ivector true \
  --mfcc-cmd "steps/make_mfcc.sh --mfcc-config conf/mfcc.conf" \
  $train $train/mfcc $local_data/train/feat/mfcc || exit 1
  echo "## step11, done `date`"
fi
if [ ! -z $step12 ]; then
  echo "## step12, vad @ `date`"
  sid/compute_vad_decision.sh --nj 20 --cmd "$cmd" \
  $train/mfcc $local_data/train/feat/vad/log $local_data/train/feat/vad/data || exit 1
  echo "## step12, vad ($train_vad) done @ `date`"
fi
train_mfcc=$train/mfcc
if [ ! -z $step13 ]; then
  echo "## step13, get subset data @ `date`"
  # Get male and female subsets of training data.
  grep -w m $train_mfcc/spk2gender | awk '{print $1}' > foo;
  utils/subset_data_dir.sh --spk-list foo $train_mfcc $tgtdir/data/train-male
  grep -w f  $train_mfcc/spk2gender | awk '{print $1}' > foo;
  utils/subset_data_dir.sh --spk-list foo $train_mfcc  $tgtdir/data/train-female
  rm foo
  # Get smaller subsets of training data for faster training.
  utils/subset_data_dir.sh $train_mfcc 4000 $tgtdir/data/train-4k
  utils/subset_data_dir.sh $train_mfcc 8000 $tgtdir/data/train-8k
  utils/subset_data_dir.sh $tgtdir/data/train-male  8000 $tgtdir/data/train-male-8k
  utils/subset_data_dir.sh $tgtdir/data/train-female 8000 $tgtdir/data/train-female-8k

  echo "## step13, done @ `date`"
fi
expdir=$tgtdir/exp
if [ ! -z $step14 ]; then
  echo "## step14, ubm @ `date`"
   sid/train_diag_ubm.sh --nj 20 --num-threads 20 \
   --parallel-opts "-pe smp 20" --cmd "$cmd" $tgtdir/data/train-4k 2048 \
   $expdir/diag_ubm_2048
   sid/train_full_ubm.sh --nj 20 --cmd "$cmd" $tgtdir/data/train-8k \
    $expdir/diag_ubm_2048 $expdir/full_ubm_2048
  echo "## ste14, ubm done @ `date`"
fi

if [ ! -z $step15 ]; then
  echo "## step15, gender dependent ubm @ `date`"
  sid/train_full_ubm.sh --nj 20 --remove-low-count-gaussians false \
    --num-iters 1 --cmd "$cmd" \
   $tgtdir/data/train-male-8k $expdir/full_ubm_2048 $expdir/full_ubm_2048_male 
  sid/train_full_ubm.sh --nj 20 --remove-low-count-gaussians false \
    --num-iters 1 --cmd "$cmd" \
   $tgtdir/data/train-female-8k $expdir/full_ubm_2048 $expdir/full_ubm_2048_female
  echo "## step15, done($expdir/full_ubm_2048_male) @ `date`"
fi

if [ ! -z $step16 ]; then
  echo "## step16, gender dependent ivector extractor training @ `date`"
  # Train the iVector extractor for male speakers.
  sid/train_ivector_extractor.sh --cmd "$cmd" \
  --num-iters 5 $expdir/full_ubm_2048_male/final.ubm $tgtdir/data/train-male \
  $expdir/extractor-2048-male

  # The same for female speakers.
  sid/train_ivector_extractor.sh --cmd "$cmd" --nj 5 \
  --num-iters 5 $expdir/full_ubm_2048_female/final.ubm $tgtdir/data/train-female \
  $expdir/extractor-2048-female
  echo "## step16, done ($expdir/extractor-2048-male, $expdir/extractor-2048-female) `date`"
fi
if [ ! -z $step17 ]; then
  echo "## step17, prepare sre08 test data @ `date` "
  source/egs/sre08/make_sre_2008_test.sh   \
  /data/users/ellenrao/NIST_SRE_Corpus/NIST_SRE_2008/sp08-11/test $tgtdir/data
  echo "## step17, done  @ `date`"
fi
featdir=$local_data
if [ ! -z $step18 ]; then
  echo "## step18, feature preparation for sre08 data @ `date`"
  for x  in sre08_train_short2_female sre08_train_short2_male sre08_test_short3_female \
       sre08_test_short3_male; do
     sdata=$tgtdir/data/$x; data=$tgtdir/data/$x/mfcc; feat=$featdir/mfcc/$x
     source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj \
     --mfcc-for-ivector true \
     --mfcc-cmd "steps/make_mfcc.sh --mfcc-config conf/mfcc.conf" \
     $sdata $data  $feat || exit 1
     sid/compute_vad_decision.sh --nj 20 --cmd "$cmd" \
     $data $featdir/vad/$x/log $featdir/vad/$x/data || exit 1
  done
  echo "## step18, done @ `date`"
fi

if [ ! -z $step19 ]; then
  echo "## step19, gender id test @ `date`"
  sid/gender_id.sh --cmd "$cmd" --nj 20 $expdir/full_ubm_2048{,_male,_female} \
  $tgtdir/data/train/mfcc $expdir/gender_id_train
  echo "## step19, done @ `date`"
fi

if [ ! -z $step20 ]; then
  echo "## step20, ivector extraction @ `date`"
  for x in train-male train-female sre08_train_short2_female/mfcc \
    sre08_train_short2_male/mfcc sre08_test_short3_female/mfcc sre08_test_short3_male/mfcc; do
    data=$tgtdir/data/$x;
    dataname=$(echo $x| sed 's:/mfcc::g')
    [ -d $data ] || { echo "## LOG: $data does not exist" && exit 1; }
    gender=$(echo $dataname| perl -ane 'chomp; 
             if(/female/){print "female";} elsif(/male/){print "male";} else { die;}' )
    if [ $? -ne 0 ]; then
      echo "## ERROR: step20, unidentified gender for data $dataname ($gender) " && exit 1
    fi
    echo "## LOG: step20,dataname=$dataname, gender=$gender @ `date`"
    sid/extract_ivectors.sh --cmd "$cmd" --nj 20 \
    $expdir/extractor-2048-${gender} $data \
    $expdir/ivectors_${dataname}  || exit 1
    echo "## LOG: done ($expdir/ivectors_${dataname}) @ `date`"
  done
  echo "## step20, done @ `date`"
fi

if [ ! -z $step21 ]; then
  echo "## step21, simple cosine-distance scoring @ `date`"
  trials=$tgtdir/data/sre08_trials/short2-short3-female.trials
  cat $trials | awk '{print $1, $2}' | \
  ivector-compute-dot-products - \
  scp:$expdir/ivectors_sre08_train_short2_female/spk_ivector.scp \
  scp:$expdir/ivectors_sre08_test_short3_female/ivector.scp \
   foo 
  local/score_sre08.sh $trials foo
   
  echo "## step21, done @ `date`"
fi

if [ ! -z $step22 ]; then
  echo "## step22, feature preparation for sre08 data @ `date`"
  for x  in sre08_train_short2_female sre08_train_short2_male; do
     sdata=$tgtdir/data/$x; data=$tgtdir/data/$x/mfcc; feat=$featdir/mfcc/$x
     source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj \
     --mfcc-for-ivector true \
     --mfcc-cmd "steps/make_mfcc.sh --mfcc-config conf/mfcc.conf" \
     $sdata $data  $feat || exit 1
     sid/compute_vad_decision.sh --nj 20 --cmd "$cmd" \
     $data $featdir/vad/$x/log $featdir/vad/$x/data || exit 1
  done
  echo "## step22, done @ `date`"
fi
if [ ! -z $step23 ]; then
  echo "## step23, ivector extraction for sre08 train data @ `date`"
  for x in sre08_train_short2_female/mfcc    sre08_train_short2_male/mfcc; do
    data=$tgtdir/data/$x;
    dataname=$(echo $x| sed 's:/mfcc::g')
    [ -d $data ] || { echo "## LOG: $data does not exist" && exit 1; }
    gender=$(echo $dataname| perl -ane 'chomp; 
             if(/female/){print "female";} elsif(/male/){print "male";} else { die;}' )
    if [ $? -ne 0 ]; then
      echo "## ERROR: step20, unidentified gender for data $dataname ($gender) " && exit 1
    fi
    echo "## LOG: step20,dataname=$dataname, gender=$gender @ `date`"
    sid/extract_ivectors.sh --cmd "$cmd" --nj 20 \
    $expdir/extractor-2048-${gender} $data \
    $expdir/ivectors_${dataname}  || exit 1
    echo "## LOG: done ($expdir/ivectors_${dataname}) @ `date`"
  done
  echo "## step23, done @ `date`"
fi
if [ ! -z $step24 ]; then
  echo "## step24, simple consin-distance for male @ `date`"
  trials=$tgtdir/data/sre08_trials/short2-short3-male.trials
  cat $trials | awk '{print $1, $2}' | \
  ivector-compute-dot-products - \
  scp:$expdir/ivectors_sre08_train_short2_male/spk_ivector.scp \
  scp:$expdir/ivectors_sre08_test_short3_male/ivector.scp \
   foo
  local/score_sre08.sh $trials foo

  echo "## step24, done @ `date`" 
fi

if [ ! -z $step25 ]; then
  echo "## step25, demo lda for female @ `date`"
  ivector-compute-lda --dim=150  --total-covariance-factor=0.1 \
  "ark:ivector-normalize-length scp:$expdir/ivectors_train-female/ivector.scp ark:- |" \
  ark:$tgtdir/data/train-female/utt2spk \
  $expdir/ivectors_train-female/transform.mat

  trials=$tgtdir/data/sre08_trials/short2-short3-female.trials
  cat $trials | awk '{print $1, $2}' | \
  ivector-compute-dot-products - \
  "ark:ivector-transform $expdir/ivectors_train-female/transform.mat scp:$expdir/ivectors_sre08_train_short2_female/spk_ivector.scp ark:- | ivector-normalize-length ark:- ark:- |" \
  "ark:ivector-transform $expdir/ivectors_train-female/transform.mat scp:$expdir/ivectors_sre08_test_short3_female/ivector.scp ark:- | ivector-normalize-length ark:- ark:- |" \
  foo
local/score_sre08.sh $trials foo
  echo "## step25, done @ `date`"
fi

if [ ! -z $step26 ]; then
  echo "## step26, demo lda for male @ `date`"
  
  ivector-compute-lda --dim=150 --total-covariance-factor=0.1 \
 "ark:ivector-normalize-length scp:$expdir/ivectors_train-male/ivector.scp ark:- |" \
   ark:$tgtdir/data/train-male/utt2spk \
   $expdir/ivectors_train-male/transform.mat
  trials=$tgtdir/data/sre08_trials/short2-short3-male.trials
  cat $trials | awk '{print $1, $2}' | \
  ivector-compute-dot-products - \
   "ark:ivector-transform $expdir/ivectors_train-male/transform.mat scp:$expdir/ivectors_sre08_train_short2_male/spk_ivector.scp ark:- | ivector-normalize-length ark:- ark:- |" \
   "ark:ivector-transform $expdir/ivectors_train-male/transform.mat scp:$expdir/ivectors_sre08_test_short3_male/ivector.scp ark:- | ivector-normalize-length ark:- ark:- |" \
   foo
  local/score_sre08.sh $trials foo

  echo "## step26, done done @ `date`"
fi

if [ ! -z $step27 ]; then
  echo "## step27, demo plda for female @ `date`"
  trials=$tgtdir/data/sre08_trials/short2-short3-female.trials
  ivector-compute-plda ark:$tgtdir/data/train-female/spk2utt \
  "ark:ivector-normalize-length scp:$expdir/ivectors_train-female/ivector.scp  ark:- |" \
    $expdir/ivectors_train-female/plda 2>$expdir/ivectors_train-female/log/plda.log
  ivector-plda-scoring --num-utts=ark:$expdir/ivectors_sre08_train_short2_female/num_utts.ark \
   "ivector-copy-plda --smoothing=0.0 $expdir/ivectors_train-female/plda - |" \
   "ark:ivector-subtract-global-mean scp:$expdir/ivectors_sre08_train_short2_female/spk_ivector.scp ark:- |" \
   "ark:ivector-subtract-global-mean scp:$expdir/ivectors_sre08_test_short3_female/ivector.scp ark:- |" \
   "cat '$trials' | awk '{print \$1, \$2}' |" foo
  local/score_sre08.sh $trials foo

  echo "## step27, done @ `date`"
fi

if [ ! -z $step28 ]; then
  echo "## step28, demo plda for male @ `date`"
  trials=$tgtdir/data/sre08_trials/short2-short3-male.trials
  ivector-compute-plda ark:$tgtdir/data/train-male/spk2utt \
  "ark:ivector-normalize-length scp:$expdir/ivectors_train-male/ivector.scp  ark:- |" \
    $expdir/ivectors_train-male/plda 2>$expdir/ivectors_train-male/log/plda.log
  ivector-plda-scoring --num-utts=ark:$expdir/ivectors_sre08_train_short2_male/num_utts.ark \
   "ivector-copy-plda --smoothing=0.0 $expdir/ivectors_train-male/plda - |" \
   "ark:ivector-subtract-global-mean scp:$expdir/ivectors_sre08_train_short2_male/spk_ivector.scp ark:- |" \
   "ark:ivector-subtract-global-mean scp:$expdir/ivectors_sre08_test_short3_male/ivector.scp ark:- |" \
   "cat '$trials' | awk '{print \$1, \$2}' |" foo; local/score_sre08.sh $trials foo
  echo "## step28, done @ `date`"
fi

if [ ! -z $step29 ]; then
  echo "## step29, demo plda for female with unsupervised adaptation @ `date`"
  trials=$tgtdir/data/sre08_trials/short2-short3-female.trials
  cat $expdir/ivectors_sre08_train_short2_female/spk_ivector.scp $expdir/ivectors_sre08_test_short3_female/ivector.scp > female.scp
  ivector-plda-scoring --num-utts=ark:$expdir/ivectors_sre08_train_short2_female/num_utts.ark \
   "ivector-adapt-plda $adapt_opts $expdir/ivectors_train-female/plda scp:female.scp -|" \
   scp:$expdir/ivectors_sre08_train_short2_female/spk_ivector.scp \
   scp:$expdir/ivectors_sre08_test_short3_female/ivector.scp \
   "cat '$trials' | awk '{print \$1, \$2}' |" foo; local/score_sre08.sh $trials foo
  echo "## step29, done @ `date`"
fi
if [ ! -z $step30 ]; then
  echo "## step30, demo plda for male with unsupervised adaptation @ `date`"
  trials=$tgtdir/data/sre08_trials/short2-short3-male.trials
  cat $expdir/ivectors_sre08_train_short2_male/spk_ivector.scp $expdir/ivectors_sre08_test_short3_male/ivector.scp > male.scp
  ivector-plda-scoring --num-utts=ark:$expdir/ivectors_sre08_train_short2_male/num_utts.ark \
   "ivector-adapt-plda $adapt_opts $expdir/ivectors_train-male/plda scp:male.scp -|" \
   scp:$expdir/ivectors_sre08_train_short2_male/spk_ivector.scp \
   scp:$expdir/ivectors_sre08_test_short3_male/ivector.scp \
   "cat '$trials' | awk '{print \$1, \$2}' |" foo; local/score_sre08.sh $trials foo
  echo "## step30, done @ `date`"
fi
