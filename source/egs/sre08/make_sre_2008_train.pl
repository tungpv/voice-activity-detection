#!/usr/bin/perl

use strict;
use warnings;

my $keyfile = "/data/users/ellenrao/NIST_SRE_Corpus/keyfiles_NIST2008to2010/NIST_SRE08_KEYS.v0.1/model-keys/NIST_SRE08_short2.model.key";
my $data = "/data/users/ellenrao/NIST_SRE_Corpus/NIST_SRE_2008/sp08-11/train/data/short2";
my $tgtdir="/home2/hhx502/sre08/data";

my $male_data_dir = "$tgtdir/sre08_train_short2_male";
my $female_data_dir = "$tgtdir/sre08_train_short2_female";

my $male_spk2gender_file = "$male_data_dir/spk2gender";
my $female_spk2gender_file = "$female_data_dir/spk2gender";

my $male_utt2spk_file = "$male_data_dir/utt2spk";
my $female_utt2spk_file = "$female_data_dir/utt2spk";

my $male_wavscp_file = "$male_data_dir/wav.scp";
my $female_wavscp_file = "$female_data_dir/wav.scp";

open(MALE_GNDER, ">$male_spk2gender_file") or 
die "## ERROR: $male_spk2gender_file cannot open\n";
open(FEMALE_GNDER, ">$female_spk2gender_file") or 
die "## ERROR: $female_spk2gender_file cannot open\n";

open(MALE_U2S, ">$male_utt2spk_file") or
die "## ERROR: $male_utt2spk_file cannot open\n";
open(FEMALE_U2S, ">$female_utt2spk_file") or
die "## ERROR: $female_utt2spk_file cannot open\n";

open(MALE_WAVSCP, ">$male_wavscp_file") or 
die "## ERROR: $male_wavscp_file cannot open\n";
open(FEMALE_WAVSCP, ">$female_wavscp_file") or 
die "## ERROR: $female_wavscp_file cannot open\n";

open (KF, "grep -v 'model_id' $keyfile|") or die "Could not open kefile $keyfile\n";
while(<KF>) {
  chomp;
  my @A = split(/,/);
  my $spkr = $A[0];
  my $gender = lc $A[1];
  my @B = split(/:/, $A[2]);
  my $basename = $B[0]; 
  my $side = uc $B[1];
  my $uttId = $spkr . "-" . $basename . "_" . $side; # prefix spkr-id to utt-id to ensure sorted order.
  my $channel = "";
  if ($side eq "B") { 
    $channel = 2;
  } elsif ($side eq "A") {
    $channel = 1;
  } else { 
    die "## ERROR: unknown channel $side \n";
  }
  my $wave = "$data/$basename.sph";
  if ( ! -e $wave) {
    print STDERR "No wave file $wave\n";
    next;
  }
  if($gender eq "m") {
    print MALE_GNDER "$spkr $gender\n";
    print MALE_U2S "$uttId $spkr\n";
    print MALE_WAVSCP "$uttId sph2pipe -f wav -p -c $channel $wave |\n";
  } elsif ($gender eq "f") {
    print FEMALE_GNDER "$spkr $gender\n";
    print FEMALE_U2S "$uttId $spkr\n";
    print FEMALE_WAVSCP "$uttId sph2pipe -f wav -p -c $channel $wave |\n";
  } else {
    die "## ERROR: unknown gender\n";
  }
}
close KF;

close MALE_GNDER or die;
close FEMALE_GNDER or die;

close MALE_U2S or die;
close FEMALE_U2S or die;

close MALE_WAVSCP or die;
close FEMALE_WAVSCP or die;
my @A = ($male_data_dir, $female_data_dir);
foreach my $dir (@A) {
  if (system("utils/utt2spk_to_spk2utt.pl $dir/utt2spk >$dir/spk2utt") != 0) {
      die "Error creating spk2utt file in directory $dir";
    }
    system("utils/fix_data_dir.sh $dir");
    if (system("utils/validate_data_dir.sh --no-text --no-feats $dir") != 0) {
      die "Error validating directory $dir";
    }
 
}

