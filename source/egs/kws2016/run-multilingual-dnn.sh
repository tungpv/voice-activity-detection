#!/bin/bash

# Copyright 2015  University of Illinois (Author: Amit Das)
# Copyright 2012-2015  Brno University of Technology (Author: Karel Vesely)

# Apache 2.0

# This example script trains Multi-lingual DNN with <BlockSoftmax> output, using FBANK features.
# The network is trained on multiple languages simultaneously, creating a separate softmax layer
# per language while sharing hidden layers across all languages.
# The script supports arbitrary number of languages.

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo
# begin options
cmd=run.pl
nj=40
steps=
validating_rate=0.1
pretrain_rate=0.2
learn_rate=0.008
copy_feats=true
train_tool=source/code2/nnet-train-frmshuff-mling
train_tool_opts="--minibatch-size=2048 --randomizer-size=32768 --randomizer-seed=777"
train_opts="--nn-depth 6 --hid-dim 2048 --splice 10"
pretrain_cmd="/home/hhx502/w2016/steps/nnet/pretrain_dbn.sh --feat-type traps  --copy_feats_tmproot /local/hhx502"
dnn_nnet_init=
train_cmd="/home/hhx502/w2016/steps/nnet/train.sh --copy_feats_tmproot /local/hhx502"
cmvn_opts="--norm-means=true --norm-vars=true"
delta_opts="--delta-order=2"

# end options

. parse_options.sh  || exit 1

function Usage {
 cat<<END

 Usage $(basename $0) [options] <lang_code_csl> <lang_weight_csl> <ali_dir_csl> <data_dir_csl> <tgtdir>
 [options]:
 --cmd                                  # value, "$cmd"
 --steps                                # value, "$steps"
 --validating-rate                      # value, "$validating_rate"
 --pretrain-rate                        # value, $pretrain_rate
 --learn-rate                           # value, $learn_rate
 --copy-feats                           # value, $copy_feats
 --train-tool-opts                      # value, "$train_tool_opts"
 --train-opts                           # value, "$train_opts"
 --pretrain-cmd                         # value, "$pretrain_cmd"
 --dnn-nnet-init                        # value, "$dnn_nnet_init"
 --train-cmd                            # value, "$train_cmd"
 --cmvn-opts                            # value, "$cmvn_opts"
 --delta-opts                           # value, "$delta_opts"

 [steps]: 
 1: prepare features
 2: prepare targets
 3: pretraining
 4: prepare language dependent softmax net 
 5: train dnn

 [example]:

 $0 --steps 1,2,3,4,5 --pretrain-rate 0.05 \
 --train-tool-opts "--minibatch-size=4096 --randomizer-size=32768 --randomizer-seed=777" \
 cant,pash,turk,taga \
 1.0,1.0,1.0,1.0 \
 /home/hhx502/w2015/monoling/cant101/llp2/exp/tri4a/ali_train,/home/hhx502/w2015/monoling/pash104/llp2/exp/tri4a/ali_train,\
/home/hhx502/w2015/monoling/turk105/llp2/exp/tri4a/ali_train,/home/hhx502/w2015/monoling/taga106/llp2/exp/tri4a/ali_train \
/home/hhx502/w2015/monoling/cant101/llp2/data/train/fbank-pitch,/home/hhx502/w2015/monoling/pash104/llp2/data/train/fbank-pitch,\
/home/hhx502/w2015/monoling/turk105/llp2/data/train/fbank-pitch,/home/hhx502/w2015/monoling/taga106/llp2/data/train/fbank-pitch \
/home2/hhx502/kws2016/mling4-run-multilingual-dnn-test

 $0 --steps 4  cant,assa,beng,pashto \
     1.0,1.0,1.0,1.0 \
     /home2/hhx502/kws2016/babel101b-v0.4c-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel102b-v0.5a-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel103b-v0.4b-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel104b-v0.4bY-build/exp/tri4a/ali_merge-train \
    /home2/hhx502/kws2016/babel101b-v0.4c-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel102b-v0.5a-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel103b-v0.4b-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel104b-v0.4bY-build/data/merge_train/fbank-pitch   \
 /home2/hhx502/kws2016/mling4-test

$0 --steps 1,8,3,9,10 cant,pash,turk,taga,viet 1.0,1.0,1.0,1.0,1.0 \
../w2015/monoling/cant101/llp2/exp/tri4a/ali_train,../w2015/monoling/pash104/llp2/exp/tri4a/ali_train,../w2015/monoling/turk105/llp2/exp/tri4a/ali_train,../w2015/monoling/taga106/llp2/exp/tri4a/ali_train,../w2015/monoling/viet107/llp2/exp/tri4a/ali_train \
../w2015/monoling/cant101/llp2/data/train/fbank-pitch,../w2015/monoling/pash104/llp2/data/train/fbank-pitch,../w2015/monoling/turk105/llp2/data/train/fbank-pitch,../w2015/monoling/taga106/llp2/data/train/fbank-pitch,../w2015/monoling/viet107/llp2/data/train/fbank-pitch \
kws2016/llp2-mling/exp/mling5-test

$0 --steps 1,2,3,4,5 --pretrain-rate 0.99 \
--train-tool-opts "--minibatch-size=2048 --randomizer-size=32768 --randomizer-seed=777" \
man,can,jan,kor,rus,viet,indo \
1.0,1.0,1.0,1.0,1.0,1.0,1.0 \
mandarin/exp/tri4a/ali_train,cantonese/exp/tri4a/ali_train,japanese/exp/tri4a/ali_train,korean/exp/tri4a/ali_train,russian/exp/tri4a/ali_train,\
vietnamese/exp/tri4a/ali_train,indonesian/exp/tri4a/ali_train \
mandarin/data/train/fbank-pitch,cantonese/data/train/fbank-pitch,japanese/data/train/fbank-pitch,korean/data/train/fbank-pitch,russian/data/train/fbank-pitch,\
vietnamese/data/train/fbank-pitch,indonesian/data/train/fbank-pitch \
multilingual-dnn/overall-7-exp

$0 --steps 1,2,3,4,5 --pretrain-rate 0.05 \
 --train-tool-opts "--minibatch-size=4096 --randomizer-size=32768 --randomizer-seed=777" \
 cant,assa,beng,pash,turk,taga,viet,hait,swah,lao,tami,kurm,zulu,tokp,cebu,kaza,telu,lith,guar,igbu,amha,mong,java,dhol,mann,feng,span,lara,sge1,sge2 \
1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 \
/home2/hhx502/kws2016/babel101b-v0.4c-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel102b-v0.5a-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel103b-v0.4b-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel104b-v0.4bY-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel105b-v0.5-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel106b-v0.2g-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel107b-v0.7-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel201b-v0.2b-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel202b-v1.0d-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel203b-v3.1a-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel204b-v1.1b-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel205b-v1.0a-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel206b-v0.1e-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel207b-v1.0e-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel301b-v2.0b-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel302b-v1.0a-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel303b-v1.0a-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel304b-v1.0b-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel305b-v1.0c-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel306b-v2.0c-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel307b-v1.0b-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel401b-v2.0b-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel402b-v1.0b-build/exp/tri4a/ali_merge-train,/home2/hhx502/kws2016/babel403b-v1.0b-build/exp/tri4a/ali_merge-train,/home2/hhx502/ldc-cts2016/hkust-mandarin-cts/exp/tri4a/ali_train,/local/hhx502/ldc-cts2016/fisher-english/exp/tri4b/ali_train,/home2/hhx502/ldc-cts2016/spanish-fisher/exp/tri4a/ali_train,/home2/hhx502/ldc-cts2016/levantine-arabic/exp/tri4a/ali_train,/home2/hhx502/sg-en-i2r/exp/mono/tri4a/ali_train,/home2/hhx502/seame/8k-exp/tri4a/ali_train \
/home2/hhx502/kws2016/babel101b-v0.4c-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel102b-v0.5a-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel103b-v0.4b-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel104b-v0.4bY-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel105b-v0.5-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel106b-v0.2g-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel107b-v0.7-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel201b-v0.2b-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel202b-v1.0d-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel203b-v3.1a-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel204b-v1.1b-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel205b-v1.0a-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel206b-v0.1e-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel207b-v1.0e-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel301b-v2.0b-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel302b-v1.0a-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel303b-v1.0a-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel304b-v1.0b-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel305b-v1.0c-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel306b-v2.0c-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel307b-v1.0b-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel401b-v2.0b-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel402b-v1.0b-build/data/merge_train/fbank-pitch,/home2/hhx502/kws2016/babel403b-v1.0b-build/data/merge_train/fbank-pitch,/home2/hhx502/ldc-cts2016/hkust-mandarin-cts/data/train/fbank-pitch,/home2/hhx502/ldc-cts2016/fisher-english/data/train-sub/fbank-pitch,/home2/hhx502/ldc-cts2016/spanish-fisher/data/train-merge/fbank-pitch,/home2/hhx502/ldc-cts2016/levantine-arabic/data/train/fbank-pitch,/home2/hhx502/sg-en-i2r/data/train/fbank-pitch,/home2/hhx502/seame/data/8k-train/fbank-pitch \
/home2/hhx502/kws2016/mling30-dnn

END
}

if [ $# -ne 5 ]; then
  echo "## lOG: $0 $@"
  Usage && exit 1
fi
lang_code_csl=$1
lang_weight_csl=$2
ali_dir_csl=$3
data_dir_csl=$4
tgtdir=$5

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

## set -euxo pipefail
pretrain_cmd="$pretrain_cmd $train_opts"
# [ ! -z $dnn_nnet_init ] && pretrain_cmd="$pretrain_cmd --only-feature-transform true"
train_cmd="$train_cmd --learn-rate $learn_rate"
# Convert 'csl' to bash array (accept separators ',' ':'),
lang_code=($(echo $lang_code_csl | tr ',:' ' ')) 
ali_dir=($(echo $ali_dir_csl | tr ',:' ' '))
data_dir=($(echo $data_dir_csl | tr ',:' ' '))

# Make sure we have same number of items in lists,
! [ ${#lang_code[@]} -eq ${#ali_dir[@]} -a ${#lang_code[@]} -eq ${#data_dir[@]} ] && \
  echo "## ERROR, Non-matching number of 'csl' items: lang_code ${#lang_code[@]}, ali_dir ${ali_dir[@]}, data_dir ${#data_dir[@]}" && \
  exit 1
num_langs=${#lang_code[@]}
# Check if all the input directories exist,
for i in $(seq 0 $[num_langs-1]); do
  echo "lang = ${lang_code[$i]}, alidir = ${ali_dir[$i]}, datadir = ${data_dir[$i]}"
  [ ! -d ${ali_dir[$i]} ] && echo  "Missing ${ali_dir[$i]}" && exit 1
  [ ! -d ${data_dir[$i]} ] && echo "Missing ${data_dir[$i]}" && exit 1
done
data=$tgtdir/data-resource
train_data=$data/combined-tr$validating_rate
cv_data=$data/combined-cv$validating_rate
if [ ! -z $step01 ]; then
  echo "## LOG: step01, prepare data @ `date`"
  train_x=""
  cv_x=""
  for i in $(seq 0 $[num_langs-1]);do
    code=${lang_code[$i]}
    sdata1=${data_dir[$i]}
    sdata=$data/$code
    utils/copy_data_dir.sh --utt-prefix ${code}_ --spk-prefix ${code}_ $sdata1 $sdata
    cur_tr=$data/${code}_train
    cur_cv=$data/${code}_cv
    echo "sdata=$sdata, cur_tr=$cur_tr, cur_cv=$cur_cv"
    source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
    --random true \
    --data2 $cur_tr \
    $sdata  $cur_cv || exit 1
    cat $cur_tr/utt2spk | awk -v c=$code '{$2=c; print;}' > $cur_tr/utt2lang
    cat $cur_cv/utt2spk | awk -v c=$code '{$2=c; print;}' > $cur_cv/utt2lang
    train_x="$train_x $cur_tr"
    cv_x="$cv_x $cur_cv"
  done
  # Merge the datasets
  utils/combine_data.sh $train_data $train_x
  utils/combine_data.sh $cv_data $cv_x
  # Validate
  utils/validate_data_dir.sh $train_data
  utils/validate_data_dir.sh $cv_data
  echo "## LOG: step01, data preparation done @ `date`"
fi

# Extract the tied-state numbers from transition models,
for i in $(seq 0 $[num_langs-1]); do
  ali_dim[i]=$(hmm-info ${ali_dir[i]}/final.mdl | grep pdfs | awk '{ print $NF }')
done
ali_dim_csl=$(echo ${ali_dim[@]} | tr ' ' ',')
echo "## LOG: ali_dim_csl=$ali_dim_csl"

# Total number of DNN outputs (sum of all per-language blocks),
output_dim=$(echo ${ali_dim[@]} | tr ' ' '\n' | awk '{ sum += $i; } END{ print sum; }')
echo "## LOG: Total number of DNN outputs: $output_dim = $(echo ${ali_dim[@]} | sed 's: : + :g')"

tgtalidir=$data/ali-post
if [ ! -z $step02 ]; then
  echo "## LOG: step02, combine post to prepare label @ `date`"
  [ -d $tgtalidir ] || mkdir -p $tgtalidir
  # re-saving the ali in posterior format, indexed by 'scp',
  for i in $(seq 0 $[num_langs-1]); do
    code=${lang_code[$i]}
    ali=${ali_dir[$i]}
    # utt suffix added by 'awk',
    ali-to-pdf $ali/final.mdl "ark:gunzip -c ${ali}/ali.*.gz |" ark,t:- | awk -v c=$code '{ $1=c"_"$1; print $0; }' | \
    ali-to-post ark:- ark,scp:$tgtalidir/$code.ark,$tgtalidir/$code.scp
  done
  # pasting the ali's, adding language-specific offsets to the posteriors,
  featlen="ark:feat-to-len 'scp:cat $train_data/feats.scp $cv_data/feats.scp |' ark,t:- |" # get number of frames for every utterance,
  post_scp_list=$(echo ${lang_code[@]} | tr ' ' '\n' | awk -v d=$tgtalidir '{ printf(" scp:%s/%s.scp", d, $1); }')
  source/code2/paste-post --allow-partial=true --no-merge=true "$featlen" "${ali_dim_csl}" ${post_scp_list} \
  ark,scp:$tgtalidir/combined-label.ark,$tgtalidir/combined-label.scp
  echo "## LOG: step02, done with target label preparation @ `date`"
fi

function make_mling_opts_csl {
  local x_dir=$1
  local x_hid_dim=$2
  for i in $(seq 0 $[num_langs-1]); do
    code=${lang_code[i]}
    local tgt_num=${ali_dim[i]}
    local curdir=$x_dir/$code
    [ -d $curdir ] || mkdir -p $curdir
    utils/nnet/make_nnet_proto.py $x_hid_dim $tgt_num 0 $x_hid_dim > $curdir/nnet.proto
    nnet-initialize $curdir/nnet.proto $curdir/nnet.init
    mnet_dir[i]="$curdir/nnet.init"
  done
  mnet_dir_csl=$(echo "${mnet_dir[*]}" | tr ' ' ',')
  [ -f $x_dir/utt2lang ] || \
  cat $train_data/utt2lang $cv_data/utt2lang > $x_dir/utt2lang
  mling_opts_csl="ark:$x_dir/utt2lang;$lang_code_csl;$mnet_dir_csl"
  echo "$lang_code_csl" >$x_dir/lang_code_csl
  echo "$ali_dir_csl" >$x_dir/ali_dir_csl
  echo "$data_dir_csl" >$x_dir/data_dir_csl
  echo "$ali_dim_csl" >$x_dir/ali_dim_csl
}

nn_depth=$(echo "$train_opts" | perl -pe 'if(m/--nn-depth\s+(\d+)/){$_=$1;}else{exit 1;}')
hid_dim=$(echo "$train_opts" | perl -pe 'if(m/--hid-dim\s+(\d+)/){$_=$1;}else{exit 1;}')
nnet_dir=$tgtdir/dnn-m${num_langs}-layers$nn_depth
dbn=$tgtdir/pretrain_dbn/${nn_depth}.dbn
feature_transform=$tgtdir/pretrain_dbn/final.feature_transform
nnet_init=$nnet_dir/nnet.init
if [ ! -z $step03 ]; then
  echo "## LOG: step03, pretraining started @ `date`"
  pretrain_data=$data/combined-pretrain$pretrain_rate
  source/egs/swahili/subset_data.sh --subset_time_ratio $pretrain_rate \
  --random true \
  $train_data  $pretrain_data || exit 1

  $pretrain_cmd --delta-opts $delta_opts --cmvn-opts "$cmvn_opts" \
  $pretrain_data $tgtdir/pretrain_dbn
  echo "## LOG: step03, done @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## LOG: step04, make language dependent softmax nnet @ `date`"
  make_mling_opts_csl $nnet_dir $hid_dim
  echo "## LOG: step04, mling_opts_csl=$mling_opts_csl"
  echo "## LOG: step04, done @ `date`"
fi
if [ ! -z $step05 ]; then
  echo "## LOG: step05, train mling dnn @ `date`"
  if [ ! -z $dnn_nnet_init ]; then
    source_nnet_dir=$(dirname $dnn_nnet_init)
    nnet_init=$dnn_nnet_init
    feature_transform=$source_nnet_dir/final.feature_transform
  else
    x_nnet_init=$nnet_dir/${lang_code[0]}/nnet.init
    [ -f $x_nnet_init ] || { echo "## ERROR: step09, file $x_nnet_init expected"; exit 1; }
    nnet-concat $dbn  $x_nnet_init $nnet_init
  fi
  for x in $nnet_init $feature_transform; do
    [ -e $x ] || { echo "ERROR: step10, $x expected (nnet_init/feature_transform)"; exit 1; }
  done
  if [ ! -f $nnet_dir/nnet.init ]; then
    srcnnet=$(cd $(dirname $nnet_init); pwd)/$(basename $nnet_init)
    (cd $nnet_dir; ln -s $srcnnet nnet.init)
  fi
  $train_cmd  --nnet-init $nnet_init  \
  --copy-feats $copy_feats \
  --feature-transform $feature_transform \
  --train-tool $train_tool \
  --train-tool-opts  "$train_tool_opts" \
  --schedule-cmd "/home/hhx502/w2016/steps/nnet/train_scheduler_mling.sh" \
  --mling-opts "$mling_opts_csl" \
  --labels "scp:$tgtalidir/combined-label.scp" \
  ${train_data} ${cv_data} lang-dummy ali-dummy ali-dummy $nnet_dir
  echo "## LOG: step05, ended @ `date`"
fi

