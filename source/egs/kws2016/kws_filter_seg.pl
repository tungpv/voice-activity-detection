#!/usr/bin/perl
use strict;
use warnings;
if (@ARGV != 1) {
  die "\n\nExample: cat segments | $0 uttlist > new_uttlist\n\n";
}
my ($sFile) = @ARGV;

open(F,"$sFile") or die "$0: ERROR, file $sFile cannot open\n";
my %vocab = ();
while(<F>) {
  chomp;
  s/ //g;
  $vocab{$_} ++;
}
close F;
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(" ");
  die "$0: ERROR, bad line: $_\n" if @A != 4;
  print "$A[0]\n" if exists $vocab{$A[1]};
}
print STDERR "$0: stdin ended\n";
