#!/bin/bash 

. path.sh
. cmd.sh 

echo
echo "$0 $@"
echo

# begin options
steps=
cmd=slurm.pl
nj=40
# end options

function Usage {
 cat<<EOF

 [Examples]:

 $0 --steps 1 /home/hhx502/w2015/kws2015/G.FLP/data/eval.fbank_pitch \
 /home/hhx502/w2016/kws2016/flp-grapheme-phone/exp/tri4a/graph \
/home/hhx502/w2016/kws2016/flp-grapheme-phone/exp/nnet5a-mxl \
/home/hhx502/w2016/kws2016/flp-grapheme-phone/exp/nnet5a-mxl/decode-eval

EOF

}

. parse_options.sh || exit 1

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

data=$1
graph=$2
srcdir=$3
decode_dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  steps/nnet/decode.sh --cmd "$cmd" --nj $nj  --acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring true \
  $graph $data $decode_dir || exit 1
  echo "## LOG (step01, $0): done & check '$decode_dir'"
fi

