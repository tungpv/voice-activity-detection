#!/bin/bash

. path.sh
. cmd.sh 

echo 
echo "## LOG: $0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=
validating_rate=0.1
learn_rate=0.008
splice=10
cnn_hid_layers=2
cnn_proto_opts=
pretrain_cmd="source/egs/kws2016/nnet/pretrain_dbn.sh --path-file path-home2-hhx502-kaldi.sh  --copy-feats-tmproot /local/hhx502  --nn-depth 4 --hid-dim 2048 --rbm-iter 1"

lstm_train_cmd="steps/nnet/train.sh --network-type lstm --learn-rate 0.0001 --feat-type plain --splice 0"
lstm_proto_opts="--num-cells 512 --num-recurrent 200 --num-layers 2 --clip-gradient 5.0"
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <data> <lang> <alidir> <dir>
 [options]:
 --cmd                                     # value, "$cmd"
 --nj                                      # value, $nj
 --steps                                   # value, "$steps"
 --validating-rate                         # value, $validating_rate
 --splice                                  # vlaue, $splice
 --cnn-hid-layers                          # value, $cnn_hid_layers
 --cnn-proto-opts                          # value, "$cnn_proto_opts"
 --pretrain-cmd                            # value, "$pretrain_cmd"
 --lstm-train-cmd                          # value, "$lstm_train_cmd"
 --lstm-proto-opts                         # value, "$lstm_proto_opts"
 [steps]:
 1: prepare training & cross-validating data
 2: train cnn 
 3: pretrain taking cnn as feature transform
 4: train cnn-dnn (CE)
 5: train lstm (CE)
 [example]:
 $0  --steps 1,2,3,4 kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/cnn-dnn

 $0 --steps 1,5 kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/lstm
 $0 --steps 1,5 --lstm-proto-opts "--num-cells 512 --num-recurrent 800 --num-layers 2 --clip-gradient 5.0" \
    kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/lstm800
 $0 --steps 1,5 --lstm-proto-opts "--num-cells 512 --num-recurrent 800 --num-layers 2 --clip-gradient 5.0" \
  swbd/data/train/fbank_pitch swbd/data/lang  swbd/exp/tri4a/ali_train  swbd/exp/lstm800
 $0 --steps 1,5 --lstm-proto-opts "--num-cells 800 --num-recurrent 512 --num-layers 2 --clip-gradient 30.0" \
  kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/lstm-c800-r512-clip30

 $0 --steps 1,2,3,4 kws2016/flp-grapheme/data/train/fbank40 kws2016/flp-grapheme/data/lang kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/cnn1d-nnet
 $0 --steps 1,2,3,4 --cnn-proto-opts "--patch-dim1 8 --pitch-dim 1" kws2016/flp-grapheme/data/train/fbank40-en kws2016/flp-grapheme/data/lang kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/cnn1d-nnet-en41
 
 $0 --steps 1,5 --lstm-proto-opts "--num-cells 800 --num-recurrent 512 --num-layers 2 --clip-gradient 5.0" \
  wsj/data/train_si284/fbank  wsj/data/lang_test_tg  wsj/exp/tri4a/ali_train_si284  wsj/exp/lstm-c800-r512
 $0 --steps 1,2,3,4 wsj/data/train_si284/fbank wsj/data/lang_test_tg  wsj/exp/tri4a/ali_train_si284 wsj/exp/cnn1d-nnet5a
END
}
if [ $#  -ne 4 ] || [ -z $steps ]; then
  Usage && exit 1
fi
data=$1
lang=$2
alidir=$3 
dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
else
  echo "## WARNING: steps are not specified"
  Usage && exit 0
fi
train=$dir/train
valid=$dir/valid
if [ ! -z $step01 ]; then
  echo "## LOG: step01, prepare training data @ `date`"
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $train \
  $data  $valid || exit 1
  echo "## LOG: step01 done @ `date`"
fi
cnn_dir=$dir/cnn
if [ ! -z $step02 ]; then
  echo "## LOG: step02, train cnn @ `date`"
  x_hid_layers=1
  x_hid_dim=1024
  steps/nnet/train.sh \
  --cmvn-opts "--norm-means=true --norm-vars=true" \
  --delta-opts "--delta-order=2" --splice $splice  \
  --network-type cnn1d \
  ${cnn_proto_opts:+ --cnn-proto-opts "$cnn_proto_opts"} \
  --hid-layers $x_hid_layers --hid-dim $x_hid_dim  --learn-rate $learn_rate \
  $train $valid $lang $alidir $alidir $cnn_dir || exit 1;
  
  nnet-concat $cnn_dir/final.feature_transform \
  "nnet-copy --remove-last-layers=$(((x_hid_layers+1)*2)) $cnn_dir/final.nnet - |" \
  $cnn_dir/final.feature_transform_cnn

  echo "## LOG: step02 ended @ `date`"
fi
pretrain_dir=$dir/pretrain_dbn
transf_cnn=$cnn_dir/final.feature_transform_cnn
nn_depth=$(echo "$pretrain_cmd"| perl -pe 'if(/--nn-depth\s+(\d+)\s+.*/){$_=$1;}else{$_="";}')
if [ ! -z $step03 ]; then
  echo "## LOG: step03, pretraining @ `date`"
  $pretrain_cmd \
  --feature-transform $transf_cnn --input-vis-type bern \
  --param-stddev-first 0.05 --param-stddev 0.05 \
  $train $pretrain_dir || exit 1
  echo "## LOG: step03, done @ `date`"
fi
cnn_dnn_dir=$dir/cnn-dnn
if [ ! -z $step04 ]; then
  echo "## LOG: step04, train cnn-dnn @ `date`"
  [ -d $cnn_dnn_dir/log ] || mkdir -p $cnn_dnn_dir/log
  feature_transform=$cnn_dir/final.feature_transform
  feature_transform_dbn=$pretrain_dir/final.feature_transform
  dbn=$pretrain_dir/$nn_depth.dbn
  cnn_dbn=$cnn_dnn_dir/cnn_dbn.net
  {
    . path-home2-hhx502-kaldi.sh
    num_components=$(nnet-info $feature_transform | grep -m1 num-components | awk '{print $2;}')
    cnn="nnet-copy --remove-first-layers=$num_components $feature_transform_dbn - |"
    nnet-concat "$cnn" $dbn $cnn_dbn 2>$cnn_dnn_dir/log/concat_cnn_dbn.log || exit 1 
  }
  steps/nnet/train.sh --feature-transform $feature_transform --dbn $cnn_dbn --hid-layers 0 \
  $train $valid $lang $alidir $alidir $cnn_dnn_dir || exit 1;
  echo "## LOG: step04, ended @ `date`"
fi
lstm_dir=$dir/lstm-nnet
if [ ! -z $step05 ]; then
  echo "## LOG: step05, train lstm @ `date`"
  $lstm_train_cmd \
  --cmvn-opts "--norm-means=true --norm-vars=true" \
  --scheduler-opts "--momentum 0.9 --halving-factor 0.5" \
  --train-tool "nnet-train-lstm-streams" \
  --train-tool-opts "--num-stream=4 --targets-delay=5" \
  --proto-opts "$lstm_proto_opts" \
  $train $valid $lang $alidir $alidir $lstm_dir || exit 1;
  echo "## LOG: step05, done @ `date`"
fi
cnn1_dir=$dir/cnn1d-nnet-train-perutt
if [ ! -z $step06 ]; then
  echo "## LOG: step06, train cnn use nnet-train-perutt @ `date` "
  echo "## LOG: ste06, done @ `date`"
fi
