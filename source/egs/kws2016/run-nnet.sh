#!/bin/bash

. path.sh
. cmd.sh 

# begin options
nj=40
cmd="slurm.pl"
steps=

validating_rate=0.1
learn_rate=0.008
cmvn_opts="--norm-means=true"
delta_opts=                           # "--delta-order=2"
valid_data_source=
nnet_train_cmd=/home/hhx502/w2016/steps/nnet/train.sh
nnet_pretrain_cmd="/home/hhx502/w2016/steps/nnet/pretrain_dbn.sh --feat-type traps  --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 5 --hid-dim 2048"
use_partial_data_to_pretrain=false
pretraining_rate=0.5
train_tool_opts="--minibatch-size=2048 --randomizer-size=32768 --randomizer-seed=777"
nnet_id=nnet5a

devdata=
graphdir=
decodename=decode-dev
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15 "
# end options

echo
echo LOG: $0 $@
echo

. parse_options.sh || exit 1

function PrintOptions {
  cat <<END
[options]:
--nj				# value, $nj
--cmd				# value, "$cmd"
--steps				# value, "$steps"

--validating_rate		# value, $validating_rate
--learn-rate			# value, $learn_rate
--cmvn_opts			# value, "$cmvn_opts"
--delta_opts			# value, "$delta_opts"
--valid-data-source		# value, "$valid_data_source"
--nnet-train-cmd		# value, "$nnet_train_cmd"
--nnet-pretrain-cmd		# value, "$nnet_pretrain_cmd"
--use-partial-data-to-pretrain  # value, $use_partial_data_to_pretrain
--pretraining-rate              # value, $pretraining_rate
--train-tool-opts               # value, "$train_tool_opts"
--nnet-id			# value, "$nnet_id"

--devdata			# value, "$devdata"
--graphdir                      # value, "$graphdir"
--decodename			# value, "$decodename"
--decode-opts			# value, "$decode_opts"
--scoring-opts			# value, "$scoring_opts"

 [steps]:
 1: prepare data 
 2: pretraining
 3: training
 4: test dev data, if any
 [examples]:
 source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 --delta-opts "--delta-order=2"  --nnet-pretrain-cmd "steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048" \
 --use-partial-data-to-pretrain true --pretraining-rate 0.2   --nnet-id nnet5a --graphdir /local/hhx502/ldc-cts2016/fisher-english/exp/tri4a/graph --devdata fisher-english/data/dev/fbank-pitch \
 fisher-english/data/train/fbank-pitch fisher-english/data/lang  /local/hhx502/ldc-cts2016/fisher-english/exp/tri4a/ali_train  /local/hhx502/ldc-cts2016/fisher-english/exp
 
 $0  --steps 1,2,3,4 --delta-opts "--delta-order=2"  --nnet-pretrain-cmd "steps/nnet/pretrain_dbn.sh --feat-type traps --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048" \
 --use-partial-data-to-pretrain true --pretraining-rate 0.3 --nnet-id nnet5a --graphdir hkust-mandarin-cts/exp/tri4a/graph \
 --devdata hkust-mandarin-cts/data/dev/fbank-pitch \
 hkust-mandarin-cts/data/train/fbank-pitch hkust-mandarin-cts/data/lang hkust-mandarin-cts/exp/tri4a/ali_train  hkust-mandarin-cts/exp 

END
}

if [ $# -ne 4 ]; then
  echo
  echo "Usage: $(basename $0) [options] <data> <lang> <alidir> <dir>"
  PrintOptions
  echo && exit 1
fi

data=$1
lang=$2
alidir=$3
dir=$4

# rewrite cmd string
# train_cmd=$(perl -e '$cmd = shift @ARGV; if($cmd =~ /slurm.pl/) { $cmd .= " --gres=gpu:1"; print $cmd; } else {print $cmd;} ' "$cmd")

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
tgtdir=$dir/$nnet_id
train=$tgtdir/train
valid=$tgtdir/valid
if [ ! -z $step01 ]; then
  if [ ! -z $valid_data_source ]; then
    for f in $valid_data_source/segments $data/segments; do
      [ -f $f ] || \
      { echo "ERROR, segments file $f expected"; exit 1; }
    done
    tot_hour=$(cat $data/segments| awk '{x+=$4-$3;}END{print x/3600;}')
    sel_hour=$(perl -e "print $tot_hour*$validating_rate")
    tot_hour=$(cat $valid_data_source/segments|awk '{x+=$4-$3;}END{print x/3600;}')
    new_time_ratio=$(perl -e "print $sel_hour/$tot_hour")
    echo "new_time_ration=$new_time_ratio"
    if [ $(echo "$new_time_ratio >= 1" | bc -l) -eq 1 ]; then
      cp -r $valid_data_source $valid
    else
      source/egs/swahili/subset_data.sh --subset_time_ratio $new_time_ratio \
      --random true \
      $valid_data_source  $valid || exit 1
    fi
    source/egs/swahili/subset_data.sh --exclude-uttlist $valid/segments $data $train || exit 1
  else
    source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
    --random true \
    --data2 $train \
    $data  $valid || exit 1
  fi
fi
pretrain_dir=$tgtdir/pretrain_dbn
if [ ! -z $step02 ]; then
  train_pretrain=$train
  if $use_partial_data_to_pretrain; then
     train_pretrain=$pretrain_dir/train
     source/egs/swahili/subset_data.sh --subset_time_ratio $pretraining_rate \
    --random true \
    $train  $train_pretrain || exit 1
  fi
  $nnet_pretrain_cmd  ${cmvn_opts:+--cmvn_opts "$cmvn_opts"} ${delta_opts:+--delta-opts "$delta_opts"} \
   --rbm-iter 1 $train_pretrain  $pretrain_dir || exit 1;
fi
nnetdir=$tgtdir/dnn
if [ ! -z $step03 ]; then
  echo "## LOG: $0, step03, training started @ `date`"
  hid_dim=$(echo "$nnet_pretrain_cmd" | perl -pe 'if(m/--hid-dim\s+(\d+)/){$_=$1;}else{exit 1;}')
  nn_depth=$(echo "$nnet_pretrain_cmd" | perl -pe 'if(m/--nn-depth\s+(\d+)/){$_=$1;}else{exit 1;}')
  dbn=$pretrain_dir/$nn_depth.dbn
  feature_transform=$pretrain_dir/final.feature_transform
  $nnet_train_cmd  --hid-dim $hid_dim \
  --feature-transform $feature_transform \
  --dbn $dbn \
  --hid-layers 0  \
  --learn-rate $learn_rate \
  --train-tool-opts "$train_tool_opts" \
  $train $valid $lang $alidir \
  $alidir $nnetdir || exit 1; 
  echo "## LOG: $0, step03, ended @ `date`"
fi

if [ ! -z $step04 ] && [ ! -z $devdata ]; then
  echo "decoding started @ `date`"
  [ ! -z $graphdir ] || { echo "## ERROR: step04, graphdir not specified"; exit 1; }
  source/egs/swahili/decode.sh --cmd "slurm.pl" --nj $nj \
  --steps 2 \
  --scoring-opts "$scoring_opts" \
  --decode-opts "$decode_opts" \
  $devdata $lang $graphdir $nnetdir/$decodename || exit 1
  echo "ended @ `date`"
fi

echo "Done !"
