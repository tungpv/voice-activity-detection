#!/bin/bash

. path.sh
. cmd.sh

# begin options
# end options

. parse_options.sh

dir=kws2016/flp-grapheme/exp/mono/smbr-nnet5a-sub
cur_lhuc=kws2016/flp-grapheme/exp/mono/lhuc-sat/nnet-lhuc/lhuc.init
data=kws2016/flp-grapheme/data/train-sub/fbank_pitch
lhuc_opts_csl="false;ark:$data/utt2spk;$cur_lhuc"
source/egs/kws2016/train_mpe.sh --cmd run.pl  --lhuc-opts-csl "$lhuc_opts_csl"   kws2016/flp-grapheme/data/train-sub/fbank_pitch kws2016/flp-grapheme/data/lang kws2016/flp-grapheme/exp/mono/nnet5a-sub kws2016/flp-grapheme/exp/mono/nnet5a-sub/ali_train-sub kws2016/flp-grapheme/exp/mono/nnet5a-sub/denlat_train-sub kws2016/flp-grapheme/exp/mono/smbr-nnet5a-sub

