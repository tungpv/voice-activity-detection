#!/bin/bash
. path.sh
echo 
echo "## LOG: $0 $@"
echo 
# begin options
steps=
# end options

. parse_options.sh || exit 1
prefix=/data/users/hhx502/kws16/nist-lang-data
function Usage {
 cat<<END
 Usage: $0 [options]:
 [steps]:
 1: Download 101b
 2: Download 102b
 3: Download 103b
 4: Download 104b
 5: Download 107b
 6: Download 201b
 7: Download 202b
 8: Download 203b
 9: Download 204b
 10: Download 205b
 11: Download 206b
 12: Download 207b
 13: Download 301b
 14: Download 302b
 15: Download 303b
 16: Download 304b
 17: Download 305b
 18: Download 306b
 19: Download 307b
 20: Download 401b
 21: Download 402b
 22: Download 403b

 23: 107b-eval
 24: 202b-eval
 25: 204b-eval
 26: Download 105b 
 27: Download 106b

END
}
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

Usage
wget_opts="--user=openkws16 --password=qwsmw4t83ik3j3eiw --quiet  --directory-prefix=$prefix"
if [ ! -z $step01 ]; then
  echo "## LOG: step01, 101b started @ `date`"
  wget $wget_opts  https://mig.nist.gov/OpenKWS16/IARPA-babel101b-v0.4c-build-copr.zip
  echo "##LOG: step01, ended @ `date`"
fi
if [ ! -z $step02 ]; then
  echo "## LOG: step02, 102b started @ `date`"
  wget $wget_opts  https://mig.nist.gov/OpenKWS16/IARPA-babel102b-v0.5a-build-copr.zip
  echo "## LOG: step02, ended @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "## LOG: step03, 103b started @ `date`"
  wget $wget_opts   https://mig.nist.gov/OpenKWS16/IARPA-babel103b-v0.4b-build-copr.zip
  echo "## LOG: step03, ended @ `date`"
fi

if [ ! -z $step04 ]; then
  echo "## LOG: step04, 104b started @ `date`"
  wget $wget_opts   https://mig.nist.gov/OpenKWS16/IARPA-babel104b-v0.4bY-build-copr.zip
  echo "## LOG: step04, ended @ `date`"
fi
if [ ! -z $step05 ]; then
  echo "## LOG: step05, 107b started @ `date`"
  wget  $wget_opts  https://mig.nist.gov/OpenKWS16/IARPA-babel107b-v0.7-build-copr.zip
  echo "## LOG: step05, ended @ `date`"
fi
if [ ! -z $step06 ]; then
  echo "## LOG: step06, 201b started @ `date`"
  wget $wget_opts  https://mig.nist.gov/OpenKWS16/IARPA-babel201b-v0.2b-build-copr.zip
  echo "## LOG: step06, ended @ `date`"
fi

if [ ! -z $step07 ]; then
  echo "## LOG: step07, 202b started @ `date`"
wget $wget_opts  https://mig.nist.gov/OpenKWS16/IARPA-babel202b-v1.0d-build-copr.zip
  echo "## LOG: step07, ended @ `date`"
fi
if [ ! -z $step08 ]; then
  echo "## LOG: step08, 203b started @ `date`"
wget  $wget_opts   https://mig.nist.gov/OpenKWS16/IARPA-babel203b-v3.1a-build-copr.zip
  echo "## LOG: step08, ended @ `date`"
fi
if [ ! -z $step09 ]; then
  echo "## LOG: step09, 204b started @ `date`"
  wget $wget_opts   https://mig.nist.gov/OpenKWS16/IARPA-babel204b-v1.1b-build-copr.zip
  echo "## LOG: step09, ended @ `date`"
fi
if [ ! -z $step10 ]; then
  echo "## LOG: step10, 205b started @ `date`"
  wget $wget_opts https://mig.nist.gov/OpenKWS16/IARPA-babel205b-v1.0a-build-copr.zip
  echo "## LOG: step10, ended @ `date`"
fi
if [ ! -z $step11 ]; then
  echo "## LOG: step11, 206b started @ `date`"
  wget $wget_opts   https://mig.nist.gov/OpenKWS16/IARPA-babel206b-v0.1e-build-copr.zip
  echo "## LOG: step11, ended @ `date`"
fi
if [ ! -z $step12 ]; then
  echo "## LOG: step12, 207b started @ `date`"
  wget $wget_opts  https://mig.nist.gov/OpenKWS16/IARPA-babel207b-v1.0e-build-copr.zip
  echo "## LOG: step12, ended @ `date`"
fi
if [ ! -z $step13 ]; then
  echo "## LOG: step13, 301b started @ `date`"
  wget $wget_opts  https://mig.nist.gov/OpenKWS16/IARPA-babel301b-v2.0b-build-copr.zip
  echo "## LOG: step13, ended @ `date`"
fi
if [ ! -z $step14 ]; then
  echo "## LOG: step14, 302b started @ `date`"
  wget $wget_opts   https://mig.nist.gov/OpenKWS16/IARPA-babel302b-v1.0a-build-copr.zip
  echo "## LOG: step14, ended @ `date`"
fi
if [ ! -z $step15 ]; then
echo "## LOG: step15, 303b started @ `date`"
wget  $wget_opts https://mig.nist.gov/OpenKWS16/IARPA-babel303b-v1.0a-build-copr.zip
  echo "## LOG: step15, ended @ `date`"
fi
if [ ! -z $step16 ]; then
  echo "## LOG: step16, 304b started @ `date` "
  wget $wget_opts   https://mig.nist.gov/OpenKWS16/IARPA-babel304b-v1.0b-build-copr.zip
  echo "## LOG: step16, ended @ `date`"
fi
if [ ! -z $step17 ]; then
  echo "## LOG: step17, 305b started @ `date`"
  wget  $wget_opts  https://mig.nist.gov/OpenKWS16/IARPA-babel305b-v1.0c-build-copr.zip
  echo "## LOG: step17, ended @ `date`"
fi
if [ ! -z $step18 ]; then
  echo "## LOG: step18, 306b started @ `date`"
  wget  $wget_opts  https://mig.nist.gov/OpenKWS16/IARPA-babel306b-v2.0c-build-copr.zip
  echo "## LOG: step18, ended @ `date`"
fi

if [ ! -z $step19 ]; then
  echo "## LOG: step19, 307b started @ `date`"
  wget $wget_opts   https://mig.nist.gov/OpenKWS16/IARPA-babel307b-v1.0b-build-copr.zip
  echo "## LOG: step19 ended @ `date`"
fi
if [ ! -z $step20 ]; then
  echo "## LOG: step20, 401b started @ `date`"
  wget  $wget_opts  https://mig.nist.gov/OpenKWS16/IARPA-babel401b-v2.0b-build-copr.zip
  echo "## LOG: step20, ended @ `date`"
fi
if [ ! -z $step21 ]; then
  echo "## LOG: step21, 402b started @ `date`"
  wget $wget_opts   https://mig.nist.gov/OpenKWS16/IARPA-babel402b-v1.0b-build-copr.zip
  echo "## LOG: step21, ended @ `date`"
fi

if [ ! -z $step22 ]; then
  echo "## LOG: step22, 403b started @ `date`"
  wget  $wget_opts   https://mig.nist.gov/OpenKWS16/IARPA-babel403b-v1.0b-build-copr.zip
 echo "##LOG: step22, ended @ `date`"
fi

if [ ! -z $step23 ]; then
  echo "## LOG: step23, 107b-eval started @ `date`"
  wget  $wget_opts   https://mig.nist.gov/OpenKWS16/IARPA-babel107b-v0.7-eval-copr.zip
  echo "##LOG: step23, ended @ `date`"
fi
if [ ! -z $step24 ]; then
  echo "## LOG: step24, 202b-eval started @ `date`"
  wget  $wget_opts  https://mig.nist.gov/OpenKWS16/IARPA-babel202b-v1.0d-eval-copr.zip
  echo "##LOG: step24, ended @ `date`"
fi
if [ ! -z $step25 ]; then
  echo "## LOG: step25, 204b-eval started @ `date`"
  wget  $wget_opts  https://mig.nist.gov/OpenKWS16/IARPA-babel204b-v1.1b-eval-copr.zip
  echo "##LOG: step25, ended @ `date`"
fi

if [ ! -z $step26 ]; then
  echo "## LOG: step26, 105b started @ `date`"
  wget $wget_opts https://mig.nist.gov/OpenKWS16/IARPA-babel105b-v0.5-build-copr.zip
  echo "## LOG: step26, ended @ `date`"
fi
if [ ! -z $step27 ]; then
  echo "## LOG: step27, 106b started @ `date`"
  wget $wget_opts https://mig.nist.gov/OpenKWS16/IARPA-babel106b-v0.2g-build-copr.zip
  echo "## LOG: step27, 106b ended @ `date`"
fi
