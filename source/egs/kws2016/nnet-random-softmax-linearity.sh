#!/bin/bash

. path.sh
. cmd.sh

if [ $# -ne 3 ]; then
  echo
  echo  "Usage: $0 <source-nnet> <num-tgt> <dir>"
  echo
  exit 1
fi

snnet=$1
num_tgt=$2
dir=$3

[ -f $snnet ] || { echo "## ERROR: $0, source nnet $snnet does not exist"; exit 1; }
[ -d $dir ] || mkdir -p $dir

front_nnet="nnet-copy --remove-last-layers=2 $snnet -|"
hid_dim=$(nnet-info "$front_nnet" | grep output-dim |tail -1 | perl -pe 's/.*output-dim//g; m/(\d+)/g; $_=$1;') 
utils/nnet/make_nnet_proto.py $hid_dim $num_tgt 0  $hid_dim >$dir/softmax.proto || exit 1 
softmax_nnet="nnet-initialize $dir/softmax.proto - |" 
nnet-concat "$front_nnet" "$softmax_nnet"  $dir/nnet.init 2> $dir/nnet-init.log

