#!/bin/bash

. path.sh
. cmd.sh

# begin options

# end options

. parse_options.sh  || exit 1

if [ $# -ne 1 ]; then
  echo 
  echo "Usage: $0 <tgtdir>"
  echo
  exit 1
fi

tgtdir=$1

steps/nnet/align.sh  --cmd slurm.pl --nj 40 \
$tgtdir/data/merge_train/fbank-pitch  $tgtdir/data/lang \
$tgtdir/exp/smbr-nnet5a $tgtdir/exp/smbr-nnet5a/ali-merge_train || exit 1
