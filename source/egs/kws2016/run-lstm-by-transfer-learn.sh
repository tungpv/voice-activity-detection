#!/bin/bash

. path.sh
. cmd.sh

echo 
echo "## LOG: $0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=
validating_rate=0.1
first_training_rate=0.6
learn_rate=0.0001
first_num_stream=4
first_max_iter=8
second_max_iter=14
second_num_stream=32
lstm_train_cmd="steps/nnet/train.sh --network-type lstm --feat-type plain --splice 0"
lstm_proto_opts="--num-cells 800 --num-recurrent 512 --num-layers 2 --clip-gradient 5.0"

devdata=
graphdir=
decodename=decode-dev
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15 "

# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <data> <lang> <alidir> <tgtdir>
 [options]:
 --cmd                                             # value, "$cmd"
 --nj                                              # value, $nj
 --steps                                           # value, "$steps"
 --validating-rate                                 # value, $validating_rate
 --first-training-rate                             # value, $first_training_rate
 --learn-rate                                      # value, $learn_rate
 --first-num-stream                                # value, $first_num_stream
 --first-max-iter                                  # value, $first_max_iter
 --second-max-iter                                 # value, $second_max_iter
 --second-num-stream                               # value, $second_num_stream
 --lstm-train-cmd                                  # value, "$lstm_train_cmd"
 --lstm-proto_opts                                 # value, "$lstm_proto_opts"
 --devdata                                         # value, "$devdata"
 --graphdir                                        # value, "$graphdir"
 --decodename                                      # value, "$decodename"
 --decode-opts                                     # value, "$decode_opts"
 --scoring-opts                                    # value, "$scoring_opts"
 [steps]:
 1: prepare training/cross-validating data
 2: train and decode if any 
 [examples]:
 $0 --steps 1,2,3 --first-max-iter 1  --first-num-stream 8 --second-num-stream 64 \
 --lstm-proto-opts "--num-cells 1024 --num-recurrent 512 --num-layers 3 --clip-gradient 5.0" \
 --devdata kws2016/vllp-grapheme/data/dev/fbank_pitch \
 --graphdir kws2016/flp-grapheme/exp/mono/tri4a/graph \
 kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang \
 kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/lstm-train-plus-tl-3layers 

 $0 --steps 1,2,3 --first-max-iter 1  --first-num-stream 8 --second-num-stream 64 \
 --lstm-proto-opts "--num-cells 1024 --num-recurrent 512 --num-layers 2 --clip-gradient 5.0" \
 --devdata kws2016/vllp-grapheme/data/dev/fbank_pitch \
 --graphdir kws2016/flp-grapheme/exp/mono/tri4a/graph \
 kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang \
 kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/lstm--tl-2layers 

 $0 --steps 1,2,3 --first-max-iter 8 --second-max-iter 14 --first-num-stream 4 --second-num-stream 32 \
 --first-training-rate 0.6 \
 --lstm-proto-opts "--num-cells 1024 --num-recurrent 512 --num-layers 2 --clip-gradient 5.0" \
 --devdata kws2016/vllp-grapheme/data/dev/fbank_pitch \
 --graphdir kws2016/flp-grapheme/exp/mono/tri4a/graph \
 kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang \
 kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/lstm-by-transfer-learn-2layer

 $0 --steps 1,2,3 --first-max-iter 8 --second-max-iter 14 --first-num-stream 4 --second-num-stream 32 \
 --first-training-rate 0.1 \
 --lstm-proto-opts "--num-cells 1024 --num-recurrent 512 --num-layers 2 --clip-gradient 5.0" \
 --devdata swbd/data/eval2000/fbank_pitch --graphdir swbd/exp/tri4a/graph-sw1_tg \
 --decodename decode-eval2000 swbd/data/train/fbank_pitch swbd/data/lang swbd/exp/tri4a/ali_train \
 swbd/exp/lstm-by-transfer-learn-2lyer

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
alidir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
else
  echo "## WARNING: steps are not specified"
  Usage && exit 0
fi

lstm_dir2=$tgtdir/second-train
train=$lstm_dir2/train
valid=$lstm_dir2/valid
if [ ! -z $step01 ]; then
  echo "## LOG: step01, prepare training data @ `date`"
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $train \
  $data  $valid || exit 1
  echo "## LOG: step01 done @ `date`"
fi

lstm_dir1=$tgtdir/first-train
if [ ! -z $step02 ]; then
  echo "## LOG: step02, train lstm @ `hostname`@ `date`"
  first_overall_train=$lstm_dir1/overall-train
  first_train=$lstm_dir1/train
  first_valid=$lstm_dir1/valid
  source/egs/swahili/subset_data.sh --subset_time_ratio $first_training_rate \
  --random true \
  $train  $first_overall_train || exit 1
  
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $first_train \
  $first_overall_train  $first_valid || exit 1

  $lstm_train_cmd --learn-rate $learn_rate\
  --cmvn-opts "--norm-means=true --norm-vars=true" \
  --scheduler-opts "--momentum 0.9 --halving-factor 0.5 --max-iters $first_max_iter" \
  --train-tool "nnet-train-lstm-streams" \
  --train-tool-opts "--num-stream=$first_num_stream --targets-delay=5" \
  --proto-opts "$lstm_proto_opts" \
  $first_train $first_valid $lang $alidir $alidir $lstm_dir1 || exit 1;
  echo "## LOG: step02, done @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "## LOG: step03, train lstm @`hostname`@ `date`"
  [ -d $lstm_dir2 ] || mkdir -p $lstm_dir2
  source/egs/swahili/run-cross-train-nnet-v2.sh \
  --steps 2,3  --learn-rate $learn_rate --train-tool "nnet-train-lstm-streams" \
  --train-tool-opts "--num-stream=$second_num_stream --targets-delay=5" \
  --scheduler-opts  "--momentum 0.9 --halving-factor 0.5 --max-iters $second_max_iter" \
  ${devdata:+ --devdata $devdata} \
  ${graphdir:+ --graphdir $graphdir} \
  ${decodename:+--decodename $decodename} \
  dummy  $lang \
  $lstm_dir1 $alidir $lstm_dir2 

  echo "## LOG: step03, done @ `hostname` @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## LOG: step04, decode @ `hostname`@ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh \
  --steps 3  --learn-rate $learn_rate --train-tool "nnet-train-lstm-streams" \
  --train-tool-opts "--num-stream=$second_num_stream --targets-delay=5" \
  --scheduler-opts  "--momentum 0.9 --halving-factor 0.5 --max-iters $second_max_iter" \
  ${devdata:+ --devdata $devdata} \
  ${graphdir:+ --graphdir $graphdir} \
  ${decodename:+--decodename $decodename} \
  $data  $lang \
  $lstm_dir1 $alidir $lstm_dir2 
  echo "## LOG: step04, done @ `hostname`@ `date`"
fi
