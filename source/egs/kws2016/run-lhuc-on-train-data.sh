#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=
learn_rate=0.008
validating_rate=0.05
lhuc_id=nnet-lhuc
dnn_no_update=true
train_data_name=train
graphdir=
dataname=dev
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15"
silence_word_int=0
learn_rate=0.008
lhuc_learn_rate=0.0001
conf_thresh=0
spk_prune_thresh=0
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END

 Usage $(basename $0) [options] <data> <langdir> <source-nnetdir> <tgtdir>
 [options]:
 --cmd                               # value, "$cmd"
 --nj                                # value, $nj
 --steps                             # value, "$steps"
 --learn-rate                        # value, $learn_rate
 --validating-rate                   # value, $validating_rate
 --lhuc-id                           # value, "$lhuc_id"
 --dnn-no-update                     # value, $dnn_no_update
 --train-data-name                   # value, "$train_data_name"
 [steps]:
 1: make initial nnet (a trained nnet with only hidden-to-softmat layer matrix randomized) and lhuc parameters
 2: do FA using source nnet from source-nnetdir
 3: prepare training and cross-validating data
 4: do training
 
 [example]:
 $0 --steps 1,2,3,4 --learn-rate 0.008  kws2016/flp-grapheme/data/train-sub/fbank_pitch kws2016/flp-grapheme/data/lang  kws2016/flp-grapheme/exp/mono/nnet5a-sub kws2016/flp-grapheme/exp/mono/lhuc-sat

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
snnetdir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
else
  echo "## LOG: WARNING, steps EMPTY !" && exit 1
fi

lhuc_dir=$tgtdir/$lhuc_id
[ -d $lhuc_dir ] || mkdir -p $lhuc_dir

if [ ! -z $step01 ]; then
  echo "## LOG: step01, initialize lhuc parameters @ `date`"
  source/code2/nnet-lhuc-initialize  ark:$data/utt2spk $snnetdir/final.nnet $lhuc_dir/lhuc.init || exit 1
  num_tgt=$(nnet-info  $snnetdir/final.nnet | grep output-dim | tail -1 | perl -pe 's/(.*)\s+input-dim\s+(\d+),(.*)/$2/;')
  if ! $dnn_no_update; then
    source/egs/kws2016/nnet-random-softmax-linearity.sh $snnetdir/final.nnet $num_tgt $lhuc_dir || exit 1
  else
    cp $snnetdir/final.nnet $lhuc_dir/nnet.init
  fi
  echo "## LOG: step01, done @ `date`"
fi
alidir=$snnetdir/ali-${train_data_name}
if [ ! -z $step02 ]; then
  echo "## LOG: step02, do FA with source nnet @ `date`"
  if [ ! -z $alidir/ali.1.gz ]; then
    steps/nnet/align.sh --cmd "$cmd" --nj $nj \
    $data $lang $snnetdir $alidir || exit 1
  fi
  echo "## LOG: step02, done @ `date`"
fi
function subset_train_cv {
  if [ $# -lt 4 ]; then
    echo "## ERROR: subset_train_cv <srcdata> <validating-rate> <train_data> <cv_data>"; return 1
  fi
  local srcdata=$1
  local validating_rate=$2
  local train_data=$3
  local cv_data=$4
  if [ $(bc <<< "$validating_rate <= 0 || $validating_rate >= 1") -eq 1 ]; then
    echo "## ERROR: subset_train_cv, illegal validating-rate $validating_rate"; return 1
  fi
  [ -d $train_data ] || mkdir -p $train_data
  [ -d $cv_data ] || mkdir -p $cv_data
  training_rate=$(bc <<< "1-$validating_rate")
  perl -e '($spk2utt, $trn_rate, $trn_utt2spk, $cv_utt2spk) = @ARGV; 
    open(F, "<", "$spk2utt") or die "## ERROR, subset_train_cv, perl, cannot open source $spk2utt\n";
    open(TRN, ">", "$trn_utt2spk" ) or die "## ERROR, subset_train_cv, perl, cannot open $trn_utt2spk\n";
    open(CV, ">", "$cv_utt2spk") or die "## ERROR, subset_train_cv, perl, cannot open $cv_utt2spk\n";
    while(<F>) {
      chomp;
      @A = split(/[\s+]/);
      $num = (scalar @A - 1) * $trn_rate;
      $num = 1 if $num < 1;
      $spk = shift @A;
      for($i = 0; $i < $num; $i++) {
        print TRN "$A[$i] $spk\n";
      }
      for($i = $num; $i < @A; $i++) {
        print CV "$A[$i] $spk\n";
      }
    }  close F;
    close TRN; close CV;
  ' $srcdata/spk2utt $training_rate  $train_data/.utt2spk $cv_data/.utt2spk || return 1
  utils/subset_data_dir.sh  --utt-list $train_data/.utt2spk  $srcdata $train_data
  utils/subset_data_dir.sh --utt-list $cv_data/.utt2spk $srcdata $cv_data
  return 0
}
train_data=$lhuc_dir/train-data
cv_data=$lhuc_dir/cv-data
if [ ! -z $step03 ]; then
  echo "## LOG: step03, prepare nnet training data @ `date`"
  subset_train_cv $data $validating_rate $train_data $cv_data
  [ $? -eq 0 ] || { echo "## ERROR: step03, subset_train_cv failed"; exit 1; }
  echo "## LOG: step03, done @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## LOG: step04, train nnet @ `date`"
  if ! $dnn_no_update; then
  lhuc_opts_csl="true;ark:$data/utt2spk;$lhuc_dir/lhuc.init"
  else
    lhuc_opts_csl="false;ark:$data/utt2spk;$lhuc_dir/lhuc.init"
  fi
  steps/nnet/train.sh  \
  --copy-feats false \
  --train-tool "source/code2/nnet-train-perutt" \
  --schedule-cmd "steps/nnet/train_scheduler-lhuc.sh" \
  --feature-transform $snnetdir/final.feature_transform \
  --nnet-init $lhuc_dir/nnet.init \
  --learn-hid-unit-contri-opts "$lhuc_opts_csl" \
  --learn-rate $learn_rate \
  $train_data $cv_data $lang $alidir \
  $alidir $lhuc_dir || exit 1; 
  echo "## LOG: step04, done @ `date`"
fi

if [ ! -z $step05 ]; then
  echo "## LOG: step05, train lhuc parameters using mpe criterio @ `date`"
  
  echo "## LOG: step05, done @ `date`"
fi
