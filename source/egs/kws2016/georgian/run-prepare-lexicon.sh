#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
script_transcript_dir=/home2/hhx502/kws2016/georgian/data/raw_script_data/transcription
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage : $0 [options] <train-transcript-dir> <tgtdir>
 [options]:
 --steps                                    # value, "$steps"
 --script-transcript-dir                    # value, "$script_transcript_dir"
 [steps]:
 [examples]:

 $0 --steps 1  --script-transcript-dir $script_transcript_dir \
 /home2/hhx502/kws2016/georgian/data/raw_train_data/transcription /home2/hhx502/kws2016/georgian

END
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

train_transcript_dir=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
dict=$tgtdir/data/local/dict
[ -d $dict ] || mkdir -p $dict
if [ ! -z $step01 ]; then
  echo "## step01, collect all word list @ `date`"
  find $script_transcript_dir $train_transcript_dir -name "*.txt" > $dict/overall-transcript.list
  source/egs/kws2016/georgian/prepare-transcript.pl $dict/overall-transcript.list $dict/overall-transcript.txt
  cat $dict/overall-transcript.txt | \
  perl -e 'use utf8; use open qw(:std :utf8);
    while(<STDIN>){ @A = split(/\s+/); for($i = 2; $i < @A; $i ++){ $w = $A[$i];
      if(not exists $vocab{$w}) { print $w, "\n"; $vocab{$w} ++; }
    }  } ' | sort -u > $dict/overall-wordlist.txt
  echo "## step01, done ($dict) `date`"
fi
if [ ! -z $step02 ]; then
  echo "## step02, clean wordlist"
  cat $dict/overall-wordlist.txt | \
  grep -v '<' | \
  perl -ane 'use utf8; use open qw(:std :utf8);
    chomp; 
    s/[\-]/middledash/g;
    s/[_]/bottomdash/g;
    s/[[:punct:]]//g;
    s/middledash/\-/g;
    s/bottomdash/_/g;
    print "$_\n";
  ' | sort -u | egrep -v '^$'  > $dict/overall-wordlist.1.txt
  cat $dict/overall-wordlist.1.txt | \
  source/egs/kws2016/georgian/prepare-lexicon.pl $dict/lexicon-raw.txt $dict/nonsilence_phones.txt
  
  echo "## step02, done ($dict) `date`"
fi
