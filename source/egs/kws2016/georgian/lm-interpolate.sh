#!/bin/bash 

. path.sh
. cmd.sh 

echo 
echo "## LOG: $0 $@"
echo
# begin options
steps=
indomain_lm_order_range="3 3"
indomain_cutoff_csl="3,011,012,022,023"
out_of_domain_lm_order_range="3 3"
out_of_domain_cutoff_csl="3,011,012,022,023"
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <tgtdir>
 [options]:
 --steps                                     # value, "$steps"
 --indomain-lm-order-range                   # value, "$indomain_lm_order_range"
 --indomain-cutoff-csl                       # value, "$indomain_cutoff_csl"
 --out-of-domain-lm-order-range              # value, "$out_of_domain_lm_order_range"
 --out-of-domain-cutoff-csl                  # value, "$out_of_domain_cutoff_csl"

 [steps]:
 [examples]:
 $0 --steps 1 --indmain-lm-order-range "$indomain_lm_order_range" \
    --indomain-cutoff-csl "$indomain_cutoff_csl" \
    --out-of-domain-lm-order-range "$out_of_domain_lm_order_range" \
    --out-of-domain-cutoff-csl "$out_of_domain_cutoff_csl" \
    /home2/hhx502/kws2016/georgian/data/local/web-data-srilm-int

END
}

if [ $# -ne 1 ]; then
  Usage && exit 1;
fi

tgtdir=$1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $tgtdir ] || { echo "## ERROR: target folder ($tgtdir) not ready"; exit 1; }
for x in indomain_lms out_of_domain_lms; do
  xdir=$tgtdir/$x
  [ -d $xdir ] || { echo "## ERROR: folder $xdir if not ready"; exit 1; }
done

for x in vocab.gz train-text.gz dev-text.gz; do
  for xdir in $tgtdir/indomain_lms $tgtdir/out_of_domain_lms; do
    [ -f $xdir/$x ] || { echo "## ERROR: file $xdir/$x is not ready"; exit 1; }
  done
done

indomain_lmdir=$tgtdir/indomain_lms
if [ ! -z $step01 ]; then
  echo "## step01, train indomain lm @ `date`"
  source/egs/kws2016/georgian/train-srilm-v2.sh --steps 1,2  \
  --lm-order-range "$indomain_lm_order_range" \
  --cutoff-csl "$indomain_cutoff_csl" $indomain_lmdir || exit 1
  echo "## step01, done ($tgtdir/indomain_lms) @ `date`"
fi
out_of_domain_lmdir=$tgtdir/out_of_domain_lms
if [ ! -z $step02 ]; then
  echo "## step02, train out-of-domain lm @ `date` "
  source/egs/kws2016/georgian/train-srilm-v2.sh --steps 1,2  \
  --lm-order-range "$out_of_domain_lm_order_range" \
  --cutoff-csl "$out_of_domain_cutoff_csl" $out_of_domain_lmdir || exit 1
  echo "## step02, done ($tgtdir/out_of_domain_lms) @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "## step03, start to interpolate lm @ `date`"
  for x in $indomain_lmdir/lm.gz $out_of_domain_lmdir/lm.gz; do
    [ -f $x ] || { echo "## ERROR: file $x is not ready for lm interpolation"; exit 1; }
  done
  outd_lmfile=$out_of_domain_lmdir/lm.gz
  ind_lmfile=$indomain_lmdir/lm.gz
  dev_text=$indomain_lmdir/dev-text.gz
  for x in $out_lmfile $ind_lmfile $tev_text; do
    [ -e $x ] || { echo "## ERROR: $x is not ready"; exit 1; }
  done
  outd_order=$(gzip -cd $outd_lmfile |perl -e 'while(<STDIN>){chomp; last if(/\\1-grams:/); if(/ngram\s+(\d+)=\d+/){ $order = $1; } } print $order; ') 
  ind_order=$(gzip -cd $ind_lmfile |perl -e 'while(<STDIN>){chomp; last if(/\\1-grams:/); if(/ngram\s+(\d+)=\d+/){ $order = $1; } } print $order; ')
  int_order=$(x=$outd_order; [ $ind_order -gt $outd_order ] && x=$ind_order; echo -n $x) 
  echo $int_order > $tgtdir/lm_order
  tmpdir=$(mktemp -d -p $tgtdir)
  trap  "echo removing $tmpdir ...; rm -rf $tmpdir " EXIT
  best_ppl=10000000
  best_lm=
  for ifactor in $(seq  0 0.1 0.6); do
    echo "interpolating, factor=$ifactor ..."
    tlm=$tmpdir/lm.int.${ifactor}.gz
    ngram -order $int_order -lm  $outd_lmfile  -lambda $ifactor \
    -mix-lm $ind_lmfile  -write-lm $tlm
    ppl=$(ngram -order $int_order -lm $tlm -unk -ppl $dev_text | paste -s -d ' '| tee -a $tgtdir/perplexities | awk '{print $14;}')
    echo "## LOG: Current PPL, $ppl"
    [ -z $ppl ] &&  { echo "ERROR, lm $tlm ppl is problematic "; exit 1; }
    if [ $(echo "$ppl < $best_ppl"| bc -l) -eq 1 ]; then
      best_lm=$tlm
      best_ppl=$ppl
    fi
  done 
  [ -z $best_lm ] && \
  { echo "ERROR, best_lm not found by interpolating"; exit 1; }
  cp $best_lm $tgtdir/$(basename $best_lm)
  ( cd $tgtdir; [ -f lm.gz ] && unlink lm.gz; ln -sf $(basename $best_lm) lm.gz )
  echo "## step03, done ($tgtdir) @ `date`"
fi
