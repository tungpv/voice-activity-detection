#!/usr/bin/perl

print STDERR "$0: stdin epected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(.*)$/ or next;
  print "$1 1 $3\n";
}
print STDERR "$0: stdin ended\n";
