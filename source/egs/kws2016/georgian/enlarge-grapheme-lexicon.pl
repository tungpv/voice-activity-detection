#!/usr/bin/perl 
use strict;
use warnings;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;

if ($numArgs != 2) {
  print STDERR "\nExample: cat lexicon.txt | $0 \"gzip -cd large-vocab.gz|\" lexicon-letter-set.txt >larger-lexicon.txt\n\n";
  exit 1;
}
my ($word_list_file, $letter_set_file) = @ARGV;

open(L, "$letter_set_file") or die "## ERROR: file $letter_set_file cannot open\n";
my %letter_vocab = ();
while(<L>) {
  chomp;
  next if (! m/(\S+)/);
  $letter_vocab{$1} ++;
}
close L;
my %output_vocab = ();
# begin functions
sub IsUnknownWord {
  my ($vocab, $word) = @_;
  return 1 if length $word == 0;
  my @A = split(//, $word);
  # print STDERR "$word = ", join(" ", @A), "\n";
  for(my $i = 0; $i < @A; $i++ ) {
    my $letter = $A[$i];
    return 1 if not exists $$vocab{$letter};
  } 
  return 0;
}
# end functions
open(W, "$word_list_file") or die "## ERROR: file $word_list_file cannot open\n";
while (<W>) {
  chomp;
  next if (! m/(\S+)/); 
  my $w = $1;
  if (IsUnknownWord(\%letter_vocab, $w) == 1) {
    print STDERR "## unknown word: $w\n";
    next 1;
  }
  my @A = split(//, $w);
  my $w_plus_pron = sprintf("%s\t%s", $w, join(" ", @A));
  if(not exists $output_vocab{$w_plus_pron}){
     $output_vocab{$w_plus_pron} ++;
  }
}
close W;
print STDERR "## $0: stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($w, $pron) = ($1, $2);
  my @A = split(" ", $pron);
  my $w_plus_pron = sprintf("%s\t%s", $w, join(" ", @A));
  if(not exists $output_vocab{$w_plus_pron}) {
    $output_vocab{$w_plus_pron} ++;
  }
}
print STDERR "## $0: stdin ended\n";
foreach my $w_plus_pron (keys %output_vocab) {
  print "$w_plus_pron\n";
}
