#!/bin/bash

. path.sh
. cmd.sh

echo 
echo "$0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=
word_dict_dir=
phone2word_map_csl="SIL,<silence>:<oov>,<unk>:ns,<noise>"
train_gmm_feature=mfcc-pitch
train_phone_lm_with_higher_ngram=false
train_4gram_lm_with_kaldilmtool=false
train_data_csl=
trainname=train
dev_data=
dev_data_csl=
devname=dev

# end options

. parse_options.sh

function Usage {
 cat<<END
 $0 [options] <train-data> <lang> <alidir> <tgtdir>
 [options]:
 --cmd                                    # value, "$cmd"
 --nj                                     # value, "$nj"
 --steps                                  # value, "$steps"
 --word-dict-dir                          # value, "$word_dict_dir"
 --phone2word-map-csl                     # value, "$phone2word_map_csl"
 --train-gmm-feature                      # value, "$train_gmm_feature"
 --train-phone-lm-with-higher-ngram       # value, $train_phone_lm_with_higher_ngram
 --train-4gram-lm-with-kaldilmtool        # value, $train_4gram_lm_with_kaldilmtool
 --train-data-csl                         # value, "$train_data_csl"
 --trainname                              # value, "$trainname"
 --dev-data                               # value, "$dev_data"
 --dev-data-csl                           # value, "$dev_data_csl"
 --devname                                # value, "$devname"

 [steps]:
 [examples]:

 $0  --steps 1 --phone2word-map-csl "SIL,<silence>:<oov>,<unk>:ns,<noise>" \
 --dev-data  cantonese/data/dev/mfcc-pitch \
 --train-data-csl "cantonese/data/train/mfcc-pitch,train/mfcc-pitch:cantonese/data/train/fbank-pitch,train/fbank-pitch" \
 --dev-data-csl "cantonese/data/dev/mfcc-pitch,dev/mfcc-pitch:cantonese/data/dev/fbank-pitch,dev/fbank-pitch" \
 --word-dict-dir cantonese/data/local/dict  cantonese/data/train/mfcc-pitch cantonese/data/lang cantonese/exp/tri4a/ali_train \
 cantonese/phone
 
 $0 --steps 1 --phone2word-map-csl "SIL,<silence>:<sss>,<noise>:<oov>,<unk>:<vns>,<v-noise>" --trainname train --devname dev \
 --dev-data /home/hhx502/w2016/kws2016/vllp-grapheme/data/dev/plp_pitch \
 --train-data-csl "/home/hhx502/w2016/kws2016/flp-grapheme/data/train/plp_pitch,data/train/plp_pitch:/home/hhx502/w2016/kws2016/flp-grapheme/data/train/fbank_pitch,data/train/fbank_pitch" \
 --dev-data-csl "/home/hhx502/w2016/kws2016/vllp-grapheme/data/dev/plp_pitch,data/dev/plp_pitch:/home/hhx502/w2016/kws2016/vllp-grapheme/data/dev/fbank_pitch,data/dev/fbank_pitch" \
 --word-dict-dir /home/hhx502/w2016/kws2016/flp-grapheme/data/local-merge /home/hhx502/w2016/kws2016/flp-grapheme/data/train/plp_pitch /home/hhx502/w2016/kws2016/flp-grapheme/data/lang \
 /home/hhx502/w2016/kws2016/flp-grapheme/exp/mono/tri4a/ali_train   /home/hhx502/w2016/kws2016/flp-grapheme-phone

 $0 --steps 1,2 --cmd "slurm.pl --exclude=node01,node03,node04,node05,node06" --nj 40 \
 --train-gmm-feature mfcc \
 --train-4gram-lm-with-kaldilmtool true \
 --phone2word-map-csl "sil,<silence>:<oov>,<unk>:<sss>,<noise>" \
 --dev-data  /home2/hhx502/ldc-cts2016/fisher-english/data/dev/mfcc \
 --train-data-csl "/home2/hhx502/ldc-cts2016/swbd-fisher-english/data/train/mfcc,train/mfcc:/home2/hhx502/ldc-cts2016/swbd-fisher-english/data/train/fbank-pitch,train/fbank-pitch" \
 --dev-data-csl "/home2/hhx502/ldc-cts2016/fisher-english/data/dev/mfcc,dev/mfcc:/home2/hhx502/ldc-cts2016/fisher-english/data/dev/fbank-pitch,dev/fbank-pitch" \
 --word-dict-dir  /home2/hhx502/ldc-cts2016/swbd-fisher-english/data/local/dict /home2/hhx502/ldc-cts2016/swbd-fisher-english/data/train/mfcc \
 /home2/hhx502/ldc-cts2016/swbd-fisher-english/data/lang  /local/hhx502/ldc-cts2016/swbd-fisher-english/exp/tri4a/ali_train /local/hhx502/ldc-cts2016/phone-swbd-fisher-english

 $0 --cmd run.pl --nj 15 --steps 1 --phone2word-map-csl "SIL,<silence>:<sss>,<noise>:<oov>,<unk>:<vns>,<v-noise>" \
 --train-phone-lm-with-higher-ngram true \
 --trainname merge_train --devname dev \
 --dev-data /home2/hhx502/kws2016/georgian/data/dev/mfcc-pitch \
 --train-data-csl "/home2/hhx502/kws2016/georgian/data/merge_train/mfcc-pitch,merge_train/mfcc-pitch:/home2/hhx502/kws2016/georgian/data/merge_train/fbank-pitch,merge_train/fbank-pitch"  \
 --dev-data-csl "/home2/hhx502/kws2016/georgian/data/dev/mfcc-pitch,dev/mfcc-pitch:/home2/hhx502/kws2016/georgian/data/dev/fbank-pitch,dev/fbank-pitch" \
 --word-dict-dir /home2/hhx502/kws2016/georgian/data/local/dict \
 /home2/hhx502/kws2016/georgian/data/merge_train/mfcc-pitch \
 /home2/hhx502/kws2016/georgian/data/lang  /home2/hhx502/kws2016/georgian/exp/mono/tri4a/ali_merge-train \
 /home2/hhx502/kws2016/georgian-phone

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

train_data=$1
word_lang=$2
ali_dir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  echo "## LOG: step01, prepare training data @ `date`" 
  if [ ! -z "$train_data_csl" ]; then
    source/egs/ss-apsipa/make-phone-data.sh --nj $nj --cmd "$cmd" \
    --steps 1,2,3 --dataname $trainname  --ali-dir $ali_dir \
    --phone2word-map-csl "$phone2word_map_csl" \
    --data-csl "$train_data_csl" \
    data-dummy $word_lang sdir-dummy $tgtdir/data || exit 1
  fi
  echo "## LOG: dev_data_csl=$dev_data_csl, dev_data=$dev_data"
  if [ ! -z "$dev_data_csl" ] && [ ! -z $dev_data ]; then
    source/egs/ss-apsipa/make-phone-data.sh --nj $nj --cmd "$cmd" \
    --steps 1,2,3 --dataname dev  \
    --phone2word-map-csl "$phone2word_map_csl" \
    --data-csl "$dev_data_csl" \
    $dev_data $word_lang $ali_dir $tgtdir/data || exit 1
  fi
  echo "## LOG: step01, train data preparation done @ `date`"
fi

phone_dict_dir=$tgtdir/data/local/dict
phone_lang=$tgtdir/data/lang
if [ ! -z $step02 ]; then
  echo "## step02, prepare phone lang @ `date`"
  [ ! -z $word_dict_dir ] || { echo "## ERROR: word_dict_dir not specified"; exit 1; }
  [ -d $phone_dict_dir ] || mkdir -p $phone_dict_dir
  textfile=$(find $tgtdir/data/$trainname -name "text" | egrep -v '\.backup'| head -1)
  [ -z $textfile ] &&{ echo "## ERROR: textfile text not found ('$tgtdir/data/$trainname') "; exit 1;  }
  cat $textfile | \
  perl -e '($phoneFile) = @ARGV; open(F, "<$phoneFile") or die "## ERROR: file $phoneFile cannot open\n";
    while(<F>) { chomp; m/(\S+)/ or next; $vocab{$1} ++; } close F;
    while(<STDIN>) { m/(\S+)\s+(.*)/ or next; @A = split(/\s+/, $2);
      for($i= 0; $i< @A; $i++) { $s = $A[$i]; if(exists $vocab{$s}) { print $s, "\n";   }  }
    } ' $word_dict_dir/nonsilence_phones.txt |sort -u > $phone_dict_dir/nonsilence_phones.txt || exit 1
  cp $word_dict_dir/silence_phones.txt $phone_dict_dir/silence_phones.txt || exit 1
  cp $word_dict_dir/optional_silence.txt $phone_dict_dir/optional_silence.txt
  cat $phone_dict_dir/silence_phones.txt |awk '{printf("%s ", $0);}END{printf("\n");}' > $phone_dict_dir/extra_questions.txt 
  cat $phone_dict_dir/nonsilence_phones.txt | awk '{printf("%s ", $0);}END{printf("\n");}' >> $phone_dict_dir/extra_questions.txt
  cat $phone_dict_dir/nonsilence_phones.txt | perl -ane 'chomp; print "$_\t$_\n";' > $phone_dict_dir/dict.1
  (
    A=($(echo "$phone2word_map_csl" |  tr ':' ' ')) 
    num=${#A[@]}
    for x in $(seq 0 $[num-1]); do
      B=($(echo ${A[$x]} | tr ',' ' ')) 
      echo -e "${B[1]}\t${B[0]}"
    done
  ) | cat - $phone_dict_dir/dict.1 > $phone_dict_dir/lexicon.txt
  utils/validate_dict_dir.pl  $phone_dict_dir || exit 1
  utils/prepare_lang.sh --position-dependent-phones false  $phone_dict_dir "<unk>" $tgtdir/data/lang/tmp $phone_lang || exit 1
  source/egs/quesst/make_word_boundary_int.sh $phone_lang/phones
  echo "## step02, phone lang preparation done ($phone_lang/phones) @ `date`"
fi

lmdir=$tgtdir/data/local/lm
lmfile=
if $train_phone_lm_with_higher_ngram; then
  echo "## train_phone_lm_with_higher_ngram: started @ `date`"
  step03=
  train_text=$(find $tgtdir/data/$trainname -name "text" |head -1)
  [ -z $train_text ] && { echo "## ERROR: train_text $train_text not ready"; exit 1; }
  dev_text=$(find $tgtdir/data/$devname -name "text" | head -1)
  [ -z $dev_text ] && { echo "## ERROR: dev_text $dev_text not ready"; exit 1; }
  source/egs/kws2016/georgian/train-srilm.sh --steps 1,2 \
  --kaldi-words-text true --train-kaldi-text true --dev-kaldi-text true  \
  --cutoff-csl "4,0111,0112,0122,0123:5,01122,01123,01222:6,011222,011223:7,0112222,0112223,0112233" \
  $phone_lang/words.txt  $train_text \
  $dev_text  $lmdir
  [ -f $lmdir/lm.gz ] || { echo "## ERROR: $lmdir/lm.gz expected"; exit 1; }
  lmfile=$lmdir/lm.gz
  echo "## train_phone_lm_with_higher_ngram: ended @ `date`"
fi
if $train_4gram_lm_with_kaldilmtool; then
  lmtype=4gram-mincount
  textfile=$(find $tgtdir/data/$trainname -name "text" | egrep -v '\.backup'| head -1)
  [ -z $textfile ] && { echo "## ERROR: textfile expected fomr $tgtdir/data/$trainname"; exit 1; }
  train_data_dir=$(dirname $textfile)
  textfile=$(find $tgtdir/data/$devname -name "text" | egrep -v '\.backup'| head -1)
  [ -z $textfile ] && { echo "## ERROR: textfile expected fomr $tgtdir/data/$devname"; exit 1; }
  dev_data_dir=$(dirname $textfile)
  cp $train_data_dir/text  $train_data_dir/text-backup
  cat $train_data_dir/text-backup | perl -pe 's/<silence>//g;' > $train_data_dir/text 
  source/egs/fisher-english/train-kaldi-lm.sh --lmtype 4gram-mincount \
  $phone_dict_dir/lexicon.txt $train_data_dir $dev_data_dir $lmdir 
  cp $train_data_dir/text-backup $train_data_dir/text
  lmfile=$lmdir/$lmtype/lm_unpruned.gz
fi

if [ ! -z $step03 ]; then
  echo "## step03, prepare phone loop @ `date`"
  source/egs/quesst/make-phone-loop.sh $phone_dict_dir/lexicon.txt  $tgtdir/data/local/phone-loop 
  lmfile=$tgtdir/data/local/phone-loop/lm.gz
  echo "## step03, done @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## step04, build fst grammar @ `date`"
  [ -z $lmfile ] && { echo "## ERROR: step04, lmfile not ready"; exit 1; }
  source/egs/fisher-english/arpa2G.sh $lmfile $tgtdir/data/lang $tgtdir/data/lang
  echo "## step04, done "
fi
train_data=$tgtdir/data/$trainname/$train_gmm_feature
dev_data=$tgtdir/data/$devname/$train_gmm_feature
if [ ! -z $step05 ]; then
  echo "## step05, gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7  --cmd "$cmd" --nj 40 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 5000 --pdf-num 70000 \
  --devdata $dev_data\
  $train_data $phone_lang $tgtdir/exp || exit 1
  echo "## step05, done @ `date`"
fi
if [ ! -z $step06 ]; then
  echo "## LOG: step06, multilingual cross-lingual transfer learning @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2,3,4 \
  --devdata $tgtdir/data/$devname/fbank-pitch --graphdir $tgtdir/exp/tri4a/graph \
  --decodename decode-dev $tgtdir/data/train/fbank-pitch \
  $tgtdir/data/lang multilingual-dnn/overall-7-exp/dnn-m7-layers6 $tgtdir/exp/tri4a/ali_train $tgtdir/exp/nnet5a-multilingual-tl || exit 1
  echo "## LOG: step06, done ($tgtdir/exp/nnet5a-multilingual-tl) @ `date`"
fi
