#!/bin/bash 

. path.sh
. cmd.sh

# begin options

# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 
END
}

eval_part1_source=/data/users/hhx502/kws16/surprise/IndusDB.20160810/IARPA-babel404b-v1.0a_conv-evalpart1
source_data=/home2/hhx502/kws2016/georgian/data/eval
tgtdir=/home2/hhx502/kws2016/georgian/data/eval-part1

[ -d $tgtdir ] || mkdir -p $tgtdir

cat $eval_part1_source/IARPA-babel404b-v1.0a_conv-evalpart1.scoring.ecf.xml | \
perl -ane 'if(/.*audio_filename=\"([^\"]+)\".*/){ print "$1\n";}' | sort -u > $tgtdir/wavlist.txt

cat $source_data/segments | \
perl -e '($wavlist) = @ARGV; open(F, "$wavlist") or die "## ERROR: $wavlist cannot open\n"; 
  while(<F>) {chomp; m/(\S+)/ or next; $vocab{$1} ++; } close F;
  while(<STDIN>) {chomp; @A = split(/\s+/); $wav = $A[1]; if(exists $vocab{$wav}) {print "$_\n"; } 
}' $tgtdir/wavlist.txt > $tgtdir/uttlist.txt

utils/subset_data_dir.sh --utt-list $tgtdir/uttlist.txt  $source_data $tgtdir
cp $eval_part1_source/IARPA-babel404b-v1.0a_conv-evalpart1.stm  $tgtdir/stm  || exit 1
echo "done with ($tgtdir)"  && exit 0
