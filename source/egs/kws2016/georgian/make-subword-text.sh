#!/bin/bash

echo 
echo "$0 $@"
echo

. path.sh 
. cmd.sh 

# begin options
steps=
from_field=2
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <morfessor-model> <text> <tgtdir>
 [options]:
 --steps                               # value, "$steps"
 --from-field                          # value, "$from_field"
 [steps]:
 step01: collect word list from text file
 step02: make word-to-subword dict with morfessor
 step03: transfer word text into subword text
 [examples]:

 $0 --steps 1 --from-field $from_field \
 /home2/hhx502/kws2016/georgian/data/local/morfessor-exp/model/morph.mdl \
 /home2/hhx502/kws2016/georgian/data/merge_train/text \
 test/subword-merge_train 

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1;
fi

morfessor_model=$1
source_text=$2
tgtdir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

token_map_dict=$tgtdir/token-map-dict
if [ ! -z $step01 ]; then
  echo "## step01, make word list from text @ `date`"
  [ -d $token_map_dict ] || mkdir -p $token_map_dict
  cat $source_text | \
  perl -e 'use utf8; use open qw(:std :utf8); ($from) = @ARGV; while(<STDIN>) { chomp; @A = split(/\s+/);
    for($i=$from-1; $i< @A; $i++) { print "$A[$i]\n"; } }' $from_field | \
  sort -u | grep -v '<' |  \
  source/egs/kws2016/georgian/split-text-word.pl 1 | \
  perl -ane 'chomp; @A = split(/\s+/); for($i=0; $i < @A; $i++) {
   print "$A[$i]\n"; }' | grep -v '^$' | \
  sort -u > $token_map_dict/word-list.txt
  echo "## step01, done ($token_map_dict) @ `date`"
fi
if [ ! -z $step02 ]; then
  echo "## step02, make word-to-subword dict @ `date`"
  cat $token_map_dict/word-list.txt | \
  morfessor-segment -l $morfessor_model -e utf8 - > $token_map_dict/word-subword-segments.txt
  word_lines=$(wc -l < $token_map_dict/word-list.txt)
  word_subword_lines=$(wc -l < $token_map_dict/word-subword-segments.txt)
  if [ $word_lines -ne $word_subword_lines ]; then
    echo "## ERROR: word_lines ($word_lines) and word_subword_lines ($word_subword_lines) are mismatch"; exit 1;
  fi
  paste $token_map_dict/word-list.txt $token_map_dict/word-subword-segments.txt \
  > $token_map_dict/word-to-subword-dict.txt || exit 1 
  echo "## step02, done ($token_map_dict) @ `date`"
fi

if [ ! -z $step03 ]; then
  echo "## step03, transfer word text to subword text @ `date`"
  cat $source_text | \
  source/egs/kws2016/georgian/split-text-word.pl $from_field | \
  source/egs/kws2016/georgian/token-map.pl $token_map_dict/word-to-subword-dict.txt \
  $from_field  > $tgtdir/text
  echo "## step03, done ($tgtdir) @ `date`"
fi

