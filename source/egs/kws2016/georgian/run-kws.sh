#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=
source_kw_dir=/home/tungtest/OpenKWS16_FullLP/data_dev_Tung
source_kw_id=tung
kw_index_silence_word="<silence>"
kw_index_lmwt=18
kw_model_dir=/home2/hhx502/kws2016/georgian/exp/mono/tri4a
kw_index_opts="--skip-optimization false --max-states 150000  --word-ins-penalty 0 --max-silence-frames 50"
kws_ntrue_scale=1.0
kws_duptime=0.6
kaldi_babel_dir=/home2/hhx502/kaldi/egs/babel/s5c
score_subword=false
morfessor_dir=/home2/hhx502/kws2016/georgian/data/local/morfessor-exp/model
# end options
function Usage {
 cat<<END
 Usage: $0 [options] <lang-dir> <test-data>  <lattice-dir> <tgtdir> 
 [options]:
 --cmd                                          # value, "$cmd"
 --nj                                           # value, $nj
 --steps                                        # value, "$steps"
 --source-kw-dir                                # value, "$source_kw_dir"
 --source-kw-id                                 # value, "$source_kw_id"
 --kw-index-silence-word                        # value, "$kw_index_silence_word"
 --kw-index-lmwt                                # value, $kw_index_lmwt
 --kw-model-dir                                 # value, "$kw_model_dir"
 --kw-index-opts                                # value, "$kw_index_opts"
 --kws-ntrue-scale                              # value, $kws_ntrue_scale
 --kws-duptime                                  # value, $kws_duptime
 --kaldi-babel-dir                              # value, "$kaldi_babel_dir"
 --score-subword                                # value, $score_subword
 --morfessor-dir                                # value, "$morfessor_dir"

 [examples]:
 
 $0 --steps 1 --cmd "$cmd" --nj $nj \
 --source-kw-dir $source_kw_dir \
 --source-kw-id "$source_kw_id" \
 --kw-index-silence-word  "$kw_index_silence_word" \
 --kw-index-lmwt        $kw_index_lmwt \
 --kw-model-dir   $kw_model_dir \
 --kw-index-opts "$kw_index_opts" \
 --morfessor-dir $morfessor_dir \
 /home2/hhx502/kws2016/georgian/data/lang   \
 /home2/hhx502/kws2016/georgian/data/dev/fbank-pitch \
 /home2/hhx502/kws2016/georgian/exp/mono/tri4a/decode-dev  \
 /home2/hhx502/kws2016/georgian/exp/mono/tri4a/decode-dev/kws-exp
 
 $0 --steps 1 --cmd "$cmd" --nj $nj \
 --source-kw-dir $source_kw_dir \
 --source-kw-id "$source_kw_id" \
 --kw-index-silence-word  "$kw_index_silence_word" \
 --kw-index-lmwt        12 \
 --kw-model-dir   /home2/hhx502/kws2016/georgian/exp/cross-lingual/multilingual27-dnn-nnet5a-smbr \
 --kw-index-opts "$kw_index_opts" \
 --score-subword true \
 --morfessor-dir $morfessor_dir \
 /home2/hhx502/kws2016/georgian/data/subword-lang-int \
 /home2/hhx502/kws2016/georgian/data/dev/fbank-pitch \
 /home2/hhx502/kws2016/georgian/exp/cross-lingual/multilingual27-dnn-nnet5a-smbr/subword-decode_dev-beam10-lb8-acwt0.08 \
 /home2/hhx502/kws2016/georgian/exp/cross-lingual/multilingual27-dnn-nnet5a-smbr/subword-decode_dev-beam10-lb8-acwt0.08/kws-exp

END
}

. parse_options.sh || exit 1 

if [ $# -ne 4 ]; then
  Usage && exit 1
fi
lang_dir=$1
test_data=$2
latdir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
[ -z $kw_model_dir ] && kw_model_dir=$(dirname $tgtdir)
[ -f $kw_model_dir/final.mdl ]  || { echo "## ERROR: final.mdl expected from $kw_model_dir"; exit 1; }
model_flags="--model $kw_model_dir/final.mdl"
silence_int=$(grep "$kw_index_silence_word" $lang_dir/words.txt  | awk '{print $2;}')
[ -z $silence_int ] && { echo "## ERROR: no silence word $kw_index_silence_word"; exit 1; }
kwsdata=$tgtdir/${source_kw_id}-kwdata
if [ ! -z $step01 ]; then
  echo "## step01, prepare kw fst @ `date`"
  [ -z $source_kw_dir ] && { echo "## ERROR: step01, source_kw_id not specified"; exit 1; }
  [ -f $source_kw_dir/ecf.xml ] || { echo "## ERROR: step01, ecf.xml ($source_kw_dir) specified"; exit 1; }
  [ -d $kwsdata ] ||  mkdir -p $kwsdata
  cp $source_kw_dir/ecf.xml $kwsdata/ecf.xml
  keywords=$source_kw_dir/kwlist.xml
  [ -f $keywords ] || { echo "## ERROR: $step01, $keywords expected"; exit 1; }
  cat $keywords | \
  perl -e ' binmode STDOUT, ":utf8"; use XML::Simple; use Data::Dumper;
  my $data = XMLin(\*STDIN);
  foreach $kwentry (@{$data->{kw}}) { print "$kwentry->{kwid}\t$kwentry->{kwtext}\n";}' > $kwsdata/keywords.txt
  if $score_subword; then
    morfessor_model=$morfessor_dir/morph.mdl
    [ ! -z $morfessor_model ] || { echo "## ERROR: morfessor_model $morfessor_model not specified"; exit 1; }
    mv $kwsdata/keywords.txt $kwsdata/word-keywords.txt
    source/egs/kws2016/georgian/make-subword-text.sh --steps 1,2,3 \
    $morfessor_model  $kwsdata/word-keywords.txt $kwsdata || exit 1
    [ -f $kwsdata/text ] || { echo "## ERROR: text ($kwsdata) expected !"; exit 1; }
    mv $kwsdata/text $kwsdata/keywords.txt
  fi
  cp $lang_dir/words.txt $kwsdata/words.txt || exit 1
  cat $kwsdata/keywords.txt | \
  sym2int.pl --map-oov 0 -f 2- $kwsdata/words.txt > $kwsdata/keywords_all.int
  
  cat $kwsdata/keywords_all.int | \
  grep -v " 0 " | grep -v " 0$" > $kwsdata/keywords.int || exit 1
  cut -f 1 -d ' ' $kwsdata/keywords.int | \
  $kaldi_babel_dir/local/subset_kwslist.pl $keywords > $kwsdata/kwlist_invocab.xml || exit 1

  cat $kwsdata/keywords_all.int | \
  egrep " 0 | 0$" | cut -f 1 -d ' ' | \
  $kaldi_babel_dir/local/subset_kwslist.pl $keywords > $kwsdata/kwlist_outvocab.xml || exit 1

  transcripts-to-fsts ark:$kwsdata/keywords.int ark,t:- | \
    awk -v 'OFS=\t' -v silint=$silence_int '{if (NF == 4 && $1 != 0) { print $1, $1, silint, silint; } print; }' \
     > $kwsdata/keywords.fsts
  echo "## step01, done ($kwsdata) @ `date`"
fi

silence_opt="--silence-word $silence_int"
acwt=`perl -e "print (1.0/$kw_index_lmwt);"`  || exit 1
index_dir=$tgtdir/kwindex
if [ ! -z $step02 ]; then
  echo "## step02, make index from lattice @ `date`"
  [ -d $index_dir ] || mkdir -p $index_dir
  cat $test_data/segments | \
  awk '{print $1}' |  sort | uniq | perl -e ' $idx=1; while(<>) { chomp; print "$_ $idx\n"; $idx++; }' \
  > $index_dir/utter_id  || { echo "## ERROR: fail to make utter_id with $test_data"; exit 1;  }
  cat $test_data/segments | awk '{print $1" "$2}' | sort | uniq > $index_dir/utter_map
  steps/make_index.sh $silence_opt --cmd "$cmd" \
  --acwt $acwt $model_flags   $kw_index_opts \
  dummy $lang_dir $latdir $index_dir  || exit 1

  echo "## step02, done @ `date`"
fi
kwsoutdir=$tgtdir/${source_kw_id}-kws-res
if [ ! -z $step03 ]; then
  echo "## step03, kws @ `date`"
  [ -d $kwsoutdir ] || mkdir -p $kwsoutdir
  steps/search_index.sh --cmd "$cmd" \
  --indices-dir $index_dir --strict false\
  $kwsdata $kwsoutdir  || exit 1
  echo "## step03, done ($kwsoutdir) @ `date`"
fi
if [ ! -z $step04 ]; then
  echo "## step04, write normalized results @ `date`"
  [ -f $kwsdata/ecf.xml ]  || { echo "## ERROR: ecf.xml ($kwsdata)  expected"; exit 1; }
  duration=`head -1 $kwsdata/ecf.xml |\
  grep -o -E "duration=\"[0-9]*[    \.]*[0-9]*\"" |\
  perl -e 'while($m=<>) {$m=~s/.*\"([0-9.]+)\".*/\1/; print $m/2;}'`
  
  run.pl LMWT=$kw_index_lmwt:$kw_index_lmwt $kwsoutdir/write_normalized.LMWT.log \
  set -e ';' set -o pipefail ';'\
    cat ${kwsoutdir}/result.* \| \
    utils/write_kwslist.pl  --Ntrue-scale=$kws_ntrue_scale --flen=0.01 --duration=$duration \
    --segments=$test_data/segments --normalize=true --duptime=$kws_duptime --remove-dup=true\
    --map-utter=$index_dir/utter_map --digits=6 \
    - $kwsoutdir/kwslist.xml || exit 1

  echo "## step04 done ($kwsoutdir) @ `date`"
fi

if [ ! -z $step05 ]; then
  echo "## step05, soring results @ `date`"
  local/kws_score.sh  $source_kw_dir ${kwsoutdir} || exit 1;
  echo "## step05, done ($kwsoutdir) @ `date`"
fi
