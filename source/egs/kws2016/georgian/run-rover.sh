#!/bin/bash

. path.sh
. cmd.sh

# begin options 
cmd=slurm.pl
steps=
model_dir=
latdir=
lmwt=12
dataname=dev
rover_ctm_files=
rover_opts="-m maxconf -T"
skip_scoring=false
# end options

. parse_options.sh || exit 1
tgtdir=/home2/hhx502/kws2016/georgian/rover-exp
function Usage {
 cat<<END
 Usage: $0 [options] <data> <lang> <tgtdir>
 [options]:
 --steps                                # value, "$steps"
 --model-dir                            # value, "$model_dir"
 --latdir                               # value, "$latdir"
 --lmwt                                 # value, $lmwt
 --dataname                             # value, "$dataname"
 --rover-ctm-files                      # value, "$rover_ctm_files"
 --rover-opts                           # value, "$rover_opts"
 --skip-scoring                         # value, $skip_scoring

 [steps]:
 [examples]:
 
 $0 --steps 1 --skip-scoring false --model-dir /home2/hhx502/kws2016/georgian/exp/cross-lingual/multilingual27-dnn-nnet5a-smbr \
 --latdir /home2/hhx502/kws2016/georgian/exp/cross-lingual/multilingual27-dnn-nnet5a-smbr/decode_dev-beam13-lb10-acwt0.08-biglex \
--lmwt $lmwt  \
${rover_ctm_files:+--rover-ctm-files "$rover_ctm_files"} \
/home2/hhx502/kws2016/georgian/data/dev/fbank-pitch \
/home2/hhx502/kws2016/georgian/data/lang-int \
/home2/hhx502/kws2016/georgian/rover-exp


 $0 --steps 1 --skip-scoring true --dataname eval  --model-dir /home2/hhx502/kws2016/georgian/exp/cross-lingual/multilingual27-dnn-nnet5a-smbr \
 --latdir /home2/hhx502/kws2016/georgian/exp/cross-lingual/multilingual27-dnn-nnet5a-smbr/decode_eval-beam13-lb10-acwt0.08-biglex \
--lmwt $lmwt  \
/home2/hhx502/kws2016/georgian/data/eval/fbank-pitch \
/home2/hhx502/kws2016/georgian/data/lang-int \
/home2/hhx502/kws2016/georgian/rover-exp

 $0 --steps 2 --skip-scoring false \
 --lmwt $lmwt \
 --rover-ctm-files "/home2/hhx502/kws2016/georgian/exp/mono/tri4a/decode-dev/score_18/mfcc-pitch.ctm /home2/hhx502/kws2016/georgian/exp/cross-lingual/multilingual24-dnn-nnet5a/decode_dev/score_14/fbank-pitch.ctm /home2/hhx502/kws2016/georgian/rover-exp/score_12/fbank-pitch.ctm" \
 /home2/hhx502/kws2016/georgian/data/dev/fbank-pitch \
 /home2/hhx502/kws2016/georgian/data/lang-int \
 /home2/hhx502/kws2016/georgian/rover-exp

 $0 --steps 2 --skip-scoring false --dataname dev-bp-plus \
 --lmwt $lmwt \
 --rover-ctm-files "$tgtdir/score_12/fbank-pitch-A.ctm $tgtdir/i2r-3system/bp-dev1.ctm $tgtdir/i2r-3system/bp-dev2.ctm  $tgtdir/i2r-3system/monolingual.DNN.dev10h.ctm $tgtdir/i2r-3system/multilingual10.dev10h.ctm $tgtdir/i2r-3system/multilingual9.dev10h.ctm" \
 /home2/hhx502/kws2016/georgian/data/dev/fbank-pitch \
 /home2/hhx502/kws2016/georgian/data/lang-int \
 /home2/hhx502/kws2016/georgian/rover-exp
 
 # --rover-ctm-files "$tgtdir/eval/score_12/fbank-pitch.ctm $tgtdir/i2r-3system/wav.monolingual.DNN.eval.ctm $tgtdir/i2r-3system/wav.multilingual9.DNN.eval.ctm $tgtdir/i2r-3system/wav.multilingual10.DNN.eval.ctm" 

 $0 --steps 2 --skip-scoring false --dataname eval-bp-plus \
 --lmwt $lmwt \
 --rover-ctm-files "$tgtdir/eval/score_12/fbank-pitch.ctm $tgtdir/i2r-3system/bp-eval1.ctm $tgtdir/i2r-3system/bp-eval2.ctm  $tgtdir/i2r-3system/sorted-wav.monolingual.DNN.eval.ctm $tgtdir/i2r-3system/sorted-wav.multilingual9.DNN.eval.ctm $tgtdir/i2r-3system/sorted-wav.multilingual10.DNN.eval.ctm" \
 /home2/hhx502/kws2016/georgian/data/eval/fbank-pitch \
 /home2/hhx502/kws2016/georgian/data/lang-int \
 /home2/hhx502/kws2016/georgian/rover-exp


END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
tgtdir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
score_dir=
if [ ! -z $step01 ]; then
  echo "## step01: lattice to ctm @ `date`"
  model=
  [ ! -z $model_dir ] && model=$model_dir/final.mdl
  /home/hhx502/w2016/local/lattice_to_ctm.sh --cmd "$cmd" \
  --min-lmwt $lmwt --max-lmwt $lmwt ${model:+--model $model} \
  --latdir $latdir \
  $data $lang $tgtdir/$dataname || exit 1
  score_dir=$tgtdir/$dataname
  echo "## step01: done ($tgtdir) @ `date`"
fi
rover_dir=$tgtdir/rover-${dataname}
if [ ! -z $step02 ]; then
  if [ ! -z "$rover_ctm_files" ]; then
    echo "## rover @ `date`"
    ctms=
    comp_num=0
    for ctm in $rover_ctm_files; do
    [ -f $ctm ] || { echo "## ERROR, step02 ctm file $ctm is not ready"; exit 1;}
    ctms="$ctms -h $ctm ctm"
    comp_num=$[comp_num+1]
  done
    rover_score_dir=$rover_dir/score_${lmwt}
    [ -d $rover_score_dir ] || mkdir -p $rover_score_dir
    echo "## LOG: rover_score_dir=$rover_score_dir"
    cat $rover_ctm_files | awk '{ print $1;}'|sort -u | \
    awk '{s=tolower($1);printf("%s %s\n",s, $1);}' \
    > $rover_score_dir/label-map.txt 
    tgt_ctm=$rover_score_dir/$(basename $data).ctm
    echo  $ctms $rover_opts -o ${tgt_ctm}.1  > $rover_score_dir/rover-${comp_num}.log
    rover $ctms $rover_opts -o ${tgt_ctm}.1 \
    >> $rover_score_dir/rover-${comp_num}.log 2>&1 
    cat ${tgt_ctm}.1 | \
    source/egs/fix_ctm.pl $rover_score_dir/label-map.txt \
    > $tgt_ctm
    rm ${tgt_ctm}.1 >/dev/null 2>&1 
    score_dir=$rover_dir
    echo "## rover done ($tgtdir) @ `date`"
  fi
fi
if ! $skip_scoring; then
  if [ -f $data/stm ] && [ ! -z $score_dir ]; then
    echo "## scoring @ `date`"
    /home/hhx502/w2016/local/score_stm.sh --cmd "$cmd" \
    --min-lmwt $lmwt --max-lmwt $lmwt $data $lang \
    $score_dir || exit 1
    echo "## done ($tgtdir) @ `date`"
  fi
fi
