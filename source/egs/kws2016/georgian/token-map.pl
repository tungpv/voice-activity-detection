#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;

if($numArgs != 2) {
  print STDERR "\nExample: cat text | $0 <token-map-file> <from-field> \n\n\n";
  exit 1;
}
my ($token_map_file, $from) = @ARGV;
my %vocab = ();
open(T, "$token_map_file") or die "## ERROR: token_map_file $token_map_file cannot open\n";
while(<T>) {
  chomp;
  my @A = split(/\s+/);
  next if(@A <= 1);
  my $token = shift @A;
  my $token_map = join(" ", @A);
  $vocab{$token} = $token_map;
}
close T;
print STDERR "stdin expected\n";
my $text = "";
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  my $utt = "";
  for(my $i = 0; $i < $from -1; $i++) {
    $utt .= " $A[$i]";
  }   
  $utt =~ s:^ ::;
  for(my $i = $from-1; $i < @A; $i++) {
    my $w = $A[$i];
    $w = $vocab{$w} if exists $vocab{$w};
    $utt .= " $w";
  }
  $text .= "$utt\n";
}
print $text;
print STDERR "stdin ended\n";
