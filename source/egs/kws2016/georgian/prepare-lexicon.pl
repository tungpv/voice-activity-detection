#!/usr/bin/perl

use utf8;
use open qw(:std :utf8);
use warnings;
use strict;

my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  print STDERR "\nExample: cat wordlist.txt | $0 <lexicon.txt> <nonsilence-phones.txt>\n\n";
  exit 1;
}

my ($output_lex, $output_phone) = @ARGV;

open(LEX, "|sort -u > $output_lex") or die "## ERROR: output_lex $output_lex cannot open\n";
open(PHN, "|sort -u >$output_phone") or die "## ERROR: output_phone $output_phone cannot open\n";
my %vocab = ();
while(<STDIN>) {
  chomp;
  my @WA = split(/[\-]/);
  for(my $j = 0; $j < @WA; $j++) {
    my $w = $WA[$j];
    next if ($w =~ /^$/);
    my @A = split("", $w);
    my $phone = join(" ", @A); $phone =~ s/[_]//g;
    print LEX "$w\t$phone\n";
    @A = split(" ", $phone);
    for(my $i = 0; $i < @A; $i++) {
      my $s = $A[$i];
      if (not exists $vocab{$s}) {
        $vocab{$s} ++;
        print PHN "$s\n";
      }
    }
  }
}
close LEX;
close PHN;
