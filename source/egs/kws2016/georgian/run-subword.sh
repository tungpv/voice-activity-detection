#!/bin/bash

. path.sh
. cmd.sh 

echo
echo "$0 $@"
echo

# begin options
steps=
out_of_domain_word_count_file=/data/users/hhx502/kws16/surprise/ibm-webdata/404_WEBDATA_filtered/word-count.gz
word_dict=/home2/hhx502/kws2016/georgian/data/local/dict-int/lexicon.txt
indomain_train_text=/home2/hhx502/kws2016/georgian/data/merge_train/text
out_of_domain_train_text=/data/users/hhx502/kws16/surprise/ibm-webdata/404_WEBDATA_filtered/lm-source/train-text.gz
devdata=/home2/hhx502/kws2016/georgian/data/dev/fbank-pitch/text
tgt_gmm_dir=/home2/hhx502/kws2016/georgian/exp/mono/tri4a
# end options 

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <training-transcript> <tgtdir>
 [options]
 --steps                                           # value, "$steps"
 --out-of-domain-word-count-file                   # value, "$out_of_domain_word_count_file"
 --word-dict                                       # value, "$word_dict"
 --indomain-train-text                             # value, "$indomain_train_text"
 --out-of-domain-train-text                        # value, "$out_of_domain_train_text"
 --devdata                                         # value, "$devdata"
 --tgt-gmm-dir                                     # value, "$tgt_gmm_dir"

 [steps]:
 step03: prepare word list to make subword dict with word_dict
 step06: language model interpolation
 [examples]:
 
 $0 --steps 1 --out-of-domain-word-count-file $out_of_domain_word_count_file \
 --word-dict $word_dict \
 --indomain-train-text $indomain_train_text \
 --out-of-domain-train-text $out_of_domain_train_text \
 --devdata $devdata \
 --tgt-gmm-dir $tgt_gmm_dir \
 /home2/hhx502/kws2016/georgian/data/merge_train/text \
 /home2/hhx502/kws2016/georgian/data/local 

END
}

if [ $# -ne 2 ]; then
  Usage && exit 1;
fi
train_text=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $tgtdir ] || mkdir -p

morfessor=$tgtdir/morfessor-exp
data=$morfessor/data

if [ ! -z $step01 ]; then
  echo "## step01, prepare data to train morfessor model @ `date`"
  [ -d $data ] || mkdir -p $data
  cut -f2- -d' ' $train_text | \
  perl -pe 'use open qw(:std :utf8); s/<[^<]+>//g;' | \
  source/egs/morfessor/make-word-count-list-from-text.pl | \
  cat - <(gzip -cd $out_of_domain_word_count_file) | \
  source/egs/kws2016/georgian/merge-word-count-list.pl | \
  perl -ane 'use open qw(:std :utf8); m/(\S+)\s+(\d+)/g or next;
    print "$2 $1\n"; ' > $data/word-count.txt  
  echo "## step01, done ($data) @ `date`"
fi
modeldir=$morfessor/model
if [ ! -z $step02 ]; then
  echo "## step02, train model @ `date`"
  [ -f $data/word-count.txt ] || \
  { echo "## ERROR: word-count.txt expected from folder $data"; exit 1; }
  [ -d $modeldir ] || mkdir -p $modeldir
  morfessor-train -s $modeldir/morph.mdl $data/word-count.txt \
  > $modeldir/morfessor-train.log 2>&1  || exit 1
  echo "## step02, done model training ($modeldir) @ `date`"
fi
dict=$tgtdir/dict-subword
if [ ! -z $step03 ]; then
  echo "## step03, prepare for subword dict @ `date`"
  [ -f $word_dict ] || { echo "## step03, word_dict expected"; exit 1; } 
  [ -d $dict ] || mkdir -p $dict
  cat $word_dict | \
  perl -ane 'use utf8; use open qw(:std :utf8); next if (m/</); @A = split(/\s+/); print "$A[0]\n"; ' | \
  perl -ane 'use utf8; use open qw(:std :utf8); @A = split(/[\-_]/); 
    for($i = 0; $i < @A; $i ++) { $w= $A[$i]; print "$w\n"; } ' | \
  sort -u | egrep -v '^$' > $dict/word-list.txt
  cat $dict/word-list.txt | \
  morfessor-segment -l $modeldir/morph.mdl -e utf8 - > $dict/subword-segments.txt
  word_lines=$(wc -l < $dict/word-list.txt)
  segment_lines=$(wc -l < $dict/subword-segments.txt) 
  if [ $word_lines -ne $segment_lines ]; then
    echo "## ERROR: word_lines ($word_lines) and segment_lines ($segment_lines) are mismatched";
    exit 1
  fi 	
  cat $dict/subword-segments.txt | \
  perl -ane 'use utf8; use open qw(:std :utf8); @A = split(/\s+/); 
    for($i = 0; $i < @A; $i++){ $w = $A[$i]; if(not exists $vocab{$w}){ $vocab{$w} ++; print "$w\n"; } }' | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; $w = $_; @A = split(//, $w); $pron = join(" ", @A);  print "$w\t$pron\n";' | \
  sort -u > $dict/lexicon-raw.txt
  paste $dict/word-list.txt $dict/subword-segments.txt > $dict/word-to-subword-dict.txt
  echo "## step03, done ($dict) with subword dict @ `date`"
fi
tgt_lang_dir=$tgtdir/../subword-lang-int
if [ ! -z $step04 ]; then
  echo "## step04, make subword dict @ `date`"
  grep '<' $word_dict | \
  cat - $dict/lexicon-raw.txt > $dict/lexicon.txt
  cp $(dirname $word_dict)/{nonsilence_phones.txt,silence_phones.txt,optional_silence.txt,extra_questions.txt}  $dict/
  utils/validate_dict_dir.pl $dict
  utils/prepare_lang.sh \
  --share-silence-phones true \
  $dict "<unk>" $dict/tmp.lang $tgt_lang_dir
  echo "## step04, done with subword dict ('$dict', '$tgt_lang_dir') @ `date`"
fi
int_lmdir=$tgtdir/subword-srilm-int
if [ ! -z $step05 ]; then
  echo "## step05, prepare text to do language model interpolation @ `date`"
  in_lm_dir=$int_lmdir/indomain_lms
  echo "## LOG: prepare data for indomain lm training ($in_lm_dir)  @ `date`"
  [ -d $in_lm_dir ] || mkdir -p $in_lm_dir
  cat $tgt_lang_dir/words.txt | grep -v '\#' | grep -v '<eps>' | \
  awk '{print $1;}' | gzip -c > $in_lm_dir/vocab.gz

  cut -f2- -d' ' $indomain_train_text | \
  source/egs/kws2016/georgian/split-text-word.pl 1 | \
  source/egs/kws2016/georgian/token-map.pl  $dict/word-to-subword-dict.txt 1 | \
  gzip -c > $in_lm_dir/train-text.gz

  cut -f2- -d' ' $devdata | \
  source/egs/kws2016/georgian/split-text-word.pl 1 | \
  source/egs/kws2016/georgian/token-map.pl  $dict/word-to-subword-dict.txt 1 | \
  gzip -c > $in_lm_dir/dev-text.gz
  out_lm_dir=$int_lmdir/out_of_domain_lms
  echo "## LOG: prepare data for out-of-domain lm training ($out_lm_dir) @ `date`" | \
  [ -d $out_lm_dir ] || mkdir -p $out_lm_dir 
  cp $in_lm_dir/vocab.gz $out_lm_dir/ || exit 1
  gzip -cd $out_of_domain_train_text | \
  source/egs/kws2016/georgian/split-text-word.pl 1 | \
  source/egs/kws2016/georgian/token-map.pl  $dict/word-to-subword-dict.txt 1 | \
  gzip -c > $out_lm_dir/train-text.gz
  cp $in_lm_dir/dev-text.gz  $out_lm_dir/
  echo "## step05, done ($int_lmdir) @ `date`"
fi
if [ ! -z $step06 ]; then
  echo "## step06, lm interpolation @ `date`"
  source/egs/kws2016/georgian/lm-interpolate.sh --steps 1,2,3  \
  --indomain-lm-order-range "3 3"  \
  --indomain-cutoff-csl "3,011,012,022,023" \
  --out-of-domain-lm-order-range "3 3" \
  --out-of-domain-cutoff-csl "3,011,012,022,023" \
  $int_lmdir || exit 1
  echo "## step06, done ($int_lmdir) @ `date`"
fi
if [ ! -z $step07 ]; then
  echo "## step07, make grammar @ `data`"
  [ -f $int_lmdir/lm.gz ] || { echo "## ERROR: file $int_lmdir/lm.gz expected"; exit 1;  }
  /home/hhx502/w2016/local/arpa2G.sh $int_lmdir/lm.gz $tgt_lang_dir $tgt_lang_dir || exit 1
  echo "## step07, done ($tgt_lang_dir) @ `date`"
fi
if [ ! -z $step08 ]; then
  echo "## step08, make HCLG.fst @ `date`"
  if [ ! -z $tgt_gmm_dir ] && [ ! -z $tgt_lang_dir ]; then
    graphdir=$tgt_gmm_dir/subword-graph-int
    utils/mkgraph.sh $tgt_lang_dir $tgt_gmm_dir  $graphdir
  fi
  echo "## step08, making HCLG.fst ($graphdir) done @ `date`"
fi

