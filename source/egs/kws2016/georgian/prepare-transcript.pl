#!/usr/bin/perl

use utf8;
use open qw(:std :utf8);

use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 2) {
  print "\nExample: $0 <transcript-list> <output-transcript-file>\n\n";
  exit 1;
}

my ($listFile, $output_transcript) = @ARGV;

open(LIST, "$listFile") or die "## ERROR: transcript-list $listFile cannot open\n";
open (OUTPUT, ">", "$output_transcript") or die "## ERROR: output_transcript $output_transcript cannot open\n";
while(<LIST>) {
  chomp;
  my $transcript_file = $_;
  open (TRANS, "$transcript_file") or die "## ERROR: transcript_file $transcript_file cannot open\n";
  my $current_time = 0; 
  my $previous_time = 0;
  my $text = "";
  while(<TRANS>) {
    chomp;
    if(m:^\s*\[([0-9]+\.*[0-9]*)\]\s*$:) {
      $current_time = $1;
      if($current_time < $previous_time) {
        die "## ERROR: bad time mark: $_ @('$transcript_file')\n";
      }
      if ($previous_time < 0) {
        $previous_time = $current_time;
        next;
      }
      if ($text eq ""|| $text  eq "<no-speech>") {
        $previous_time = $current_time; next;
      } else {
        print OUTPUT "$previous_time $current_time $text\n";
        $text = "";
      }
      $previous_time = $current_time;
    } else {
        $text = $_;
    } 
  }
  close TRANS;
}
close LIST;
close OUTPUT;
