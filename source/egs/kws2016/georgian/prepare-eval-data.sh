#!/bin/bash

. path.sh
. cmd.sh

echo 
echo "$0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=
word_dict_dir=
phone2word_map_csl="SIL,<silence>:<oov>,<unk>:ns,<noise>"
train_phone_lm_with_higher_ngram=false
train_4gram_lm_with_kaldilmtool=false
train_data_csl=
trainname=train
dev_data=
dev_data_csl=
devname=dev

# end options

. parse_options.sh

function Usage {
 cat<<END
 $0 [options] 
 [options]:
 [steps]:
 [examples]:

END
}

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
source_data=/data/users/hhx502/kws16/surprise/IARPA-babel404b-v1.0a-eval/BABEL_OP3_404/conversational/eval
kaldi_babel_dir=/home2/hhx502/kaldi/egs/babel/s5c/
tgtdir=/home2/hhx502/kws2016/georgian/data
raw_eval_data=$tgtdir/raw_eval
eval_data=$tgtdir/eval
if [ ! -z $step01 ]; then
  echo "## LOG: step01, prepare eval data @ `date`"
  [ -d $eval_data ] || mkdir -p $eval_data
  find $source_data/audio -type f  | \
  perl -ane 'chomp; $wavid = $_; $wavid =~ s/.*\///g; 
    $wavid =~ m/(.*)\.([^\.]+$)/ or die "## ERROR: bad line $_\n"; $wavid=$1; $prefix = $2;
    if($prefix eq "sph") { print "$wavid /opt/kaldi_updated/trunk/tools/sph2pipe_v2.5/sph2pipe -f wav -p -c 1 $_ |\n"; } 
    elsif($prefix eq "wav") {print "$wavid /usr/bin/sox $_ -r 8000 -c 1 -b 16 -t wav - downsample |\n" ; } 
    else { die "## ERROR: unidentified audio file $_ ($prefix)\n"; }' > $eval_data/wav.scp
  echo "## LOG: step01, done ($eval_data) @ `date`"
fi
segment_dir=/home/xx704
if [ ! -z $step02 ]; then
  echo "## LOG: step02, make segments @ `date`"
  cat $segment_dir/{segments_eval_reverb_logmmse_buffer0.30_max20_mean1.6_median1.1_sum9922,segments_eval_wav_buffer0.30_max20_mean2.1_median1.4_sum285255} | \
  perl -e 'while(<STDIN>){chomp; @A = split(/\s+/); die "## ERROR: bad line $_ for segments\n" if (@A != 4);
    if($A[3]<$A[2]+0.1) { print STDERR "## skip $_\n"; next;} print "$_\n";
  }' > $eval_data/segments
  cat $eval_data/segments | \
  perl -ane 'chomp; @A = split(/\s+/); $utt = $A[0]; $utt =~ m/(^\d+_[A|B])_\S+/ or die "## ERROR: bad line $_\n"; 
    $spk = $1;   print "$utt $spk\n"; ' > $eval_data/utt2spk
  utils/utt2spk_to_spk2utt.pl < $eval_data/utt2spk > $eval_data/spk2utt
  fix_data_dir.sh $eval_data
  echo "## LOG: step02, done ($eval_data) @ `date`"
fi 
if [ ! -z $step03 ]; then
  echo "## step03, make feature @ `date`"
  for sdata in $eval_data; do
    echo "## LOG: for data $sdata started @ `date`"
    data=$sdata/mfcc-pitch
    feat=$sdata/feat/mfcc-pitch
    source/egs/swahili/make_feats.sh --cmd "slurm.pl"  --nj 40 \
    --mfcc-for-ivector true --mfcc-cmd \
    "steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc.conf --pitch-config conf/pitch.conf" \
    $sdata $data $feat || exit 1
    echo "## step09, mfcc-pitch done for $sdata"
    data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd slurm.pl --nj 40 \
    --make-fbank true --fbank-cmd "steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf" \
    $sdata $data $feat || exit 1
  done

  echo "## step03, done @ `date`"
fi
