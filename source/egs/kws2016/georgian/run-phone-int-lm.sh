#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
steps=
tgt_gmm_dir=/home2/hhx502/kws2016/georgian-phone/exp/tri4a
# end options

function Usage {
 cat<<END
 $0 [options] <letter-lang> <out-of-domain-word-text> <indomain-letter-text> <dev-letter-text> <tgtdir>
 [options]:
 --steps                                                   # value, "$steps"
 [examples]:

 $0 --steps 2,3,4 --tgt-gmm-dir $tgt_gmm_dir \
 /home2/hhx502/kws2016/georgian-phone/data/lang  /data/users/hhx502/kws16/surprise/ibm-webdata/404_WEBDATA_filtered/lm-source/train-text.gz \
 /home2/hhx502/kws2016/georgian-phone/data/merge_train/fbank-pitch  /home2/hhx502/kws2016/georgian-phone/data/dev/fbank-pitch \
 /home2/hhx502/kws2016/georgian-phone/data

END
}

. parse_options.sh || exit 1 

if [ $# -ne 5 ]; then
  Usage && exit 1
fi

letter_lang=$1
out_of_domain_word_text=$2
indomain_letter_data=$3
dev_letter_data=$4
tgtdir=$5

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

indomain_letter_text=$indomain_letter_data/text
dev_letter_text=$dev_letter_data/text
tgt_lang_dir=$tgtdir/lang-int
[ -d $tgt_lang_dir ] || mkdir -p $tgt_lang_dir
cp -r $letter_lang/*   $tgt_lang_dir/

[ -f $tgt_lang_dir/G.fst ]  && rm $tgt_lang_dir/G.fst

int_lmdir=$tgtdir/local/srilm-int

if [ ! -z $step01 ]; then
  echo "## step01, prepare data for lm interpolation @ `date`"
  in_lm_dir=$int_lmdir/indomain_lms
  [ -d $in_lm_dir ] || mkdir -p $in_lm_dir
  echo "## step01, prepare indomain data"
  cat $tgt_lang_dir/words.txt | grep -v '\#' | grep -v '<eps>' | \
  awk '{print $1;}' | gzip -c > $in_lm_dir/vocab.gz

  cut -f2- -d' ' $indomain_letter_text | \
  gzip -c > $in_lm_dir/train-text.gz
  
  cut -f2- -d' ' $dev_letter_text | \
  gzip -c > $in_lm_dir/dev-text.gz
  echo "## step02, prepare out-of-domain data"
  out_lm_dir=$int_lmdir/out_of_domain_lms
  [ -d $out_lm_dir ] || mkdir -p $out_lm_dir
  cp $in_lm_dir/{vocab.gz,dev-text.gz} $out_lm_dir
  gzip -cd $out_of_domain_word_text | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; s/[[:punct:]]/ /g; print "$_\n";' | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; @A = split(//); print join(" ", @A), "\n";' | \
  gzip -c > $out_lm_dir/train-text.gz
  echo "## step01, done ($int_lmdir) @ `date`"
fi
if [ ! -z $step02 ]; then
  echo "## step02, interpolate lm @ `date`"
  source/egs/kws2016/georgian/lm-interpolate.sh --steps 1,2,3  \
  --indomain-lm-order-range "5 6"  \
  --indomain-cutoff-csl "5,01123,01223:6,011234,012234" \
  --out-of-domain-lm-order-range "5 6" \
  --out-of-domain-cutoff-csl "5,01123,01223:6,011234,012234" \
  $int_lmdir || exit 1
  echo "## step02, done ($int_lmdir) @ `date`"
fi
if [ ! -z $step03 ]; then
  echo "## step03, make grammar @ `data`"
  [ -f $int_lmdir/lm.gz ] || { echo "## ERROR: file $int_lmdir/lm.gz expected"; exit 1;  }
  /home/hhx502/w2016/local/arpa2G.sh $int_lmdir/lm.gz $tgt_lang_dir $tgt_lang_dir || exit 1
  echo "## step03, done ($tgt_lang_dir) @ `date`"
fi
graphdir=$tgt_gmm_dir/graph-int
if [ ! -z $step04 ]; then
  echo "## step04, make HCLG.fst @ `date`"
  if [ ! -z $tgt_gmm_dir ] && [ ! -z $tgt_lang_dir ]; then
    utils/mkgraph.sh $tgt_lang_dir $tgt_gmm_dir  $graphdir
  fi
  echo "## step04, making HCLG.fst ($graphdir) done @ `date`"
fi
if [ ! -z $step05 ]; then
  echo "## step05, decode test @ `date`"
  steps/nnet/decode.sh --cmd "slurm.pl --exclude=node01,node02" \
  --nj 40 --scoring-opts "--min-lmwt 8 --max-lmwt 15"  --acwt 0.08 \
  --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false $tgt_gmm_dir/graph-int  \
  $dev_letter_data /home2/hhx502/kws2016/georgian-phone/exp/multilingual27-dnn-nnet5a/decode_dev-graph-int
  echo "## step05, done @ `date`"
fi


