#!/usr/bin/perl
use strict;
use warnings;
use utf8;
use open qw(:std :utf8);
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/[,\.!\?]/);
  for (my $i = 0; $i < @A; $i++) {
    my $utt = $A[$i];
    next if length $utt == 0;
    $utt =~ s#[[:punct:]]# #g;  
    $utt =~ s/[\s]+/ /g; 
    $utt =~ s/^ //;  
    $utt =~ s/ $//; 
    next if ($utt =~ /^$/);
    print $utt, "\n";
  }
}
print STDERR "$0: stdin ended \n";
