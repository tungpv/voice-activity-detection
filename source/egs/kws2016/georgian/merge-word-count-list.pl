#!/usr/bin/perl
use strict;
use warnings;
use utf8;
use open qw(:std :utf8);

print STDERR "$0 stdin expected\n";
my %vocab = ();
while(<STDIN>) {
  chomp;
  next if (! m/(\S+)\s+(\d+)/g);
  $vocab{$1} += $2;
}
foreach my $word (sort {$vocab{$b} <=> $vocab{$a}} keys %vocab) {
  print "$word $vocab{$word}\n";
}
print STDERR "$0 stdin ended \n";
