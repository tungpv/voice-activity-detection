#!/bin/bash

. path.sh
. cmd.sh

echo 
echo "## LOG: $0 $@"
echo

# begin options
steps=
out_of_domain_word_count_file=/data/users/hhx502/kws16/surprise/ibm-webdata/404_WEBDATA_filtered/word-count.gz
word_dict=/home2/hhx502/kws2016/georgian/data/local/dict-int/lexicon.txt
indomain_train_text=/home2/hhx502/kws2016/georgian/data/merge_train/text
out_of_domain_train_text=/data/users/hhx502/kws16/surprise/ibm-webdata/404_WEBDATA_filtered/lm-source/train-text.gz
devdata=/home2/hhx502/kws2016/georgian/data/dev/fbank-pitch/text
tgt_gmm_dir=/home2/hhx502/kws2016/georgian/exp/mono/tri4a
updateid=update01
# end options 

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <morfessor-model> <source-dict-dir> <new-text> <tgtdir>
 [options]
 --steps                                           # value, "$steps"
 --out-of-domain-word-count-file                   # value, "$out_of_domain_word_count_file"
 --word-dict                                       # value, "$word_dict"
 --indomain-train-text                             # value, "$indomain_train_text"
 --out-of-domain-train-text                        # value, "$out_of_domain_train_text"
 --devdata                                         # value, "$devdata"
 --tgt-gmm-dir                                     # value, "$tgt_gmm_dir"
 --updateid                                        # value, "$updateid"

 [steps]:
 step03: prepare word list to make subword dict with word_dict
 step06: language model interpolation
 [examples]:
 
 $0 --steps 1 --out-of-domain-word-count-file $out_of_domain_word_count_file \
 --word-dict $word_dict \
 --indomain-train-text $indomain_train_text \
 --out-of-domain-train-text $out_of_domain_train_text \
 --devdata $devdata \
 --tgt-gmm-dir $tgt_gmm_dir \
 /home2/hhx502/kws2016/georgian/data/local/morfessor-exp/model/morph.mdl \
 /home2/hhx502/kws2016/georgian/data/local/dict-subword \
 /home2/hhx502/kws2016/georgian/exp/cross-lingual/multilingual27-dnn-nnet5a-smbr/subword-decode_dev-beam10-lb8-acwt0.08/kws-exp/dev-eval-kwdata/word-keywords.txt \
/home2/hhx502/kws2016/georgian/data/local 

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1;
fi

morph_model=$1
source_dict_dir=$2
new_text=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
update_dict_dir=$tgtdir/subword-dict-${updateid}
if [ ! -z $step01 ]; then
  echo "## step01, update dict @ `date`"
  new_text_dir=$update_dict_dir/update-data
  source/egs/kws2016/georgian/make-subword-text.sh --steps 1,2,3 --from-field 2 \
  $morph_model $new_text $new_text_dir || exit 1
  
  cut -f2- -d' ' $new_text_dir/text | \
  cat - <(grep -v '<' $source_dict_dir/lexicon.txt) | \
  cut -f1 | \
  perl -ane 'use utf8; use open qw(:std :utf8); @A = split(/\s+/); 
    for($i = 0; $i < @A; $i++){ $w = $A[$i]; if(not exists $vocab{$w}){ $vocab{$w} ++; print "$w\n"; } }' | \
  perl -ane 'use utf8; use open qw(:std :utf8); chomp; $w = $_; @A = split(//, $w); $pron = join(" ", @A);  print "$w\t$pron\n";' | \
  sort -u > $update_dict_dir/lexicon-raw.txt

  echo "## step01, update dict ($update_dict_dir) done @ `date`"
fi
update_lang=$tgtdir/../subword-update-lang
if [ ! -z $step02 ]; then
  echo "## step02, update lang @ `date`"
  grep '<' $word_dict | \
  cat - $update_dict_dir/lexicon-raw.txt > $update_dict_dir/lexicon.txt
  cp $(dirname $word_dict)/{nonsilence_phones.txt,silence_phones.txt,optional_silence.txt,extra_questions.txt} \
   $update_dict_dir/
  utils/validate_dict_dir.pl $update_dict_dir
  utils/prepare_lang.sh \
  --share-silence-phones true \
  $update_dict_dir "<unk>" $update_dict_dir/tmp.lang $update_lang
  echo "## step02, done ($update_lang) @ `date`"
fi
int_lmdir=$tgtdir/subword-srilm-${updateid}
if [ ! -z $step03 ]; then
  echo "## step03, prepare data to upldate lm @ `date`"
  
  echo "## step03, done ($int_lmdir) @ `date`"
fi
