#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
kaldi_words_text=false
train_kaldi_text=false
dev_kaldi_text=false
lm_order_range="4 7"
cutoff_csl="4,0111,0112,0122,0123:5,01122,01113,01223:6,011222,011223:7,0111222,0111223,0112233"
# end options
function Usage {
 cat<<END

 Usage $0 [options] <wordlist> <train-text> <dev-text> <tgtdir>
 [options]:
 --steps                                        # value, "$steps"
 --kaldi-words-text                             # value, $kaldi_words_text
 --train-kaldi-text                             # value, $train_kaldi_text
 --dev-kaldi-text                               # value, $dev_kaldi_text
 --lm-order-range                               # value, "$lm_order_range"
 --cutoff-csl                                   # value, "$cutoff_csl"
 [steps]:
 [examples]:

 $0 --steps 1 --kaldi-words-text true --train-kaldi-text true --dev-kaldi-text true \
 --cutoff-csl "$cutoff_csl" /home2/hhx502/kws2016/georgian/data/lang/words.txt \
 /home2/hhx502/kws2016/georgian/data/train/text \
 /home2/hhx502/kws2016/georgian/data/dev/text test/train-ngram-test

END
}

. parse_options.sh || exit 1

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

vocab=$1
train_text=$2
dev_text=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $tgtdir ] || mkdir -p $tgtdir
if $kaldi_words_text; then
  echo "## LOG: make  @ `date`"
  cat $vocab | grep -v '\#0' | grep -v '<eps>' | awk '{print $1;}' > $tgtdir/vocab
else
  cp $vocab $tgtdir/vocab 
  echo "## LOG: done ($tgtdir) @ `date`"
fi
[ -e $tgtdir/vocab ] || { echo "## ERROR: failed to make $tgtdir/vocab"; exit 1; }

if $train_kaldi_text; then
  echo "## LOG: reformat train text "
  cat $train_text | cut -f2- -d' ' | gzip -c  > $tgtdir/train-text.gz
else
  cat $train_text | gzip -c > $tgtdir/train-text.gz
  echo "## LOG: done ($tgtdir)"
fi

[ -e $tgtdir/train-text.gz ] || { echo "## ERROR: failed to make $tgtdir/train-text.gz"; exit 1; }

if $dev_kaldi_text; then
  echo "## LOG: reformat dev text @ `date`"
  cat $dev_text | cut -f2- -d' ' | gzip -c > $tgtdir/dev-text.gz
else
  cat $dev_text | gzip -c > $tgtdir/dev-text.gz
  echo "## LOG: done ($tgtdir/dev-text.gz)"
fi

[ -e $tgtdir/dev-text.gz ] || { echo "## ERROR: failed to make $tgtdir/dev-text.gz"; exit 1; }
if [ ! -z $step01 ]; then
  echo "## step01, train ngram lm @ `date`"
  for order in $(seq $lm_order_range); do
     for cutoff in $( perl -e '($cutoff_csl, $order) = @ARGV; @A = split(/:/, $cutoff_csl); 
     for($i=0; $i<@A; $i++) { $order_csl = $A[$i]; @B = split(/[,;]/, $order_csl); $cur_order = shift @B;
       if($cur_order == $order) { print join(" ", @B);  }
     } ' "$cutoff_csl" $order); do
       gt_train_opts="$(perl -e ' ($dir, $order, $cutoff_str) = @ARGV; @A = split(//, $cutoff_str); die "## ERROR: bad cutoff_str $cutoff_str\n" if($order != scalar @A); $lmfile = sprintf("%s/%dgram.gt%s.gz", $dir, $order, $cutoff_str);  $opts = ""; for($i=0; $i < @A; $i++){ $x=$i+1; $switch = sprintf("-gt%dmin %d", $x, $A[$i]); $opts .= "$switch ";   } print "-lm $lmfile $opts -order $order";  ' $tgtdir $order $cutoff)"
       [ $? -ne 0 ] && { echo "## ERROR: failed to make gt_train_opts" && exit 1; }
       kn_train_opts="$(perl -e ' ($dir, $order, $cutoff_str) = @ARGV; @A = split(//, $cutoff_str); die "## ERROR: bad cutoff_str $cutoff_str\n" if($order != scalar @A); $lmfile = sprintf("%s/%dgram.kn%s.gz", $dir, $order, $cutoff_str);  $opts = ""; for($i=0; $i < @A; $i++){ $x=$i+1; $switch = sprintf("-kndiscount%d -gt%dmin %d", $x, $x, $A[$i]); $opts .= "$switch ";   } print "-lm $lmfile $opts -order $order";  ' $tgtdir $order $cutoff)"
       [ $? -ne 0 ] && { echo "## ERROR: failed to make kn_train_opts" && exit 1; }
       echo "## LOG: train with opts='$gt_train_opts' started @ `date`"
       ngram-count $gt_train_opts -text $tgtdir/train-text.gz  -vocab $tgtdir/vocab -unk -sort
       echo "## LOG: done with opts='$gt_train_opts' @ `date`"
       ngram-count $kn_train_opts -text $tgtdir/train-text.gz -vocab $tgtdir/vocab -unk -sort
       echo "## LOG: done with opts='$kn_train_opts' @ `date`"
     done
  done
  echo "## step01, done @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "## step02, computing perplexity"
  ( for order in $(seq $lm_order_range); do
      for f in $tgtdir/${order}gram*; do
        ( echo $f; ngram -order $order -lm $f -unk -ppl $tgtdir/dev-text.gz ) | paste -s -d ' '
      done   
    done ) | sort  -r -n -k 13 | column -t | tee $tgtdir/perplexities.txt
  echo "## step02, done "
fi

if [ -e $tgtdir/perplexities.txt ]; then
  lmfilename=$(head -1 $tgtdir/perplexities.txt | cut -f1 -d' ')
  echo "lmfilename=$lmfilename"
  if [ ! -z $lmfilename ]; then 
    ( cd $tgtdir; [ -f lm.gz ] && unlink lm.gz;  ln -sf $(basename $lmfilename) lm.gz  ) 
  fi
fi

