#!/bin/bash

. path.sh
. cmd.sh 

# begin options
steps=
georgian_letter_set=/home2/hhx502/kws2016/georgian/data/local/dict/nonsilence_phones.txt
indomain_train_text=/home2/hhx502/kws2016/georgian/data/train/text
devdata=/home2/hhx502/kws2016/georgian/data/dev/fbank-pitch/text
in_domain_lmfile=/home2/hhx502/kws2016/georgian/data/local/srilm/lm.gz
in_domain_dict_dir=/home2/hhx502/kws2016/georgian/data/local/dict
tgt_lang_dir=/home2/hhx502/kws2016/georgian/data/lang-int
tgt_gmm_dir=/home2/hhx502/kws2016/georgian/exp/mono/tri4a
# end options

. parse_options.sh || exit 1

function Usage {
 cat <<END
 Usage: $0 [options] <web-data-dir> <tgtdir>
 [options]:
 --steps                              # value, "$steps"
 --georgian-letter-set                # value, "$georgian_letter_set"
 --indomain-train-text                # value, "$indomain_train_text"
 --devdata                            # value, "$devdata"
 --in-domain-lmfile                   # value, "$in_domain_lmfile"
 --in-domain-dict-dir                 # value, "$in_domain_dict_dir"
 --tgt-lang-dir                       # value, "$tgt_lang_dir"
 --tgt-gmm-dir                        # value, "$tgt_gmm_dir"

 [steps]:
 [examples]:
 $0 --steps 1  --georgian-letter-set $georgian_letter_set \
 --devdata $devdata \
 --in-domain-lmfile $in_domain_lmfile \
 --in-domain-dict-dir $in_domain_dict_dir \
 --tgt-lang-dir $tgt_lang_dir \
 --tgt-gmm-dir $tgt_gmm_dir \
 --indomain-train-text $indomain_train_text \
 /data/users/hhx502/kws16/surprise/ibm-webdata/404_WEBDATA_filtered  /home2/hhx502/kws2016/georgian/data/local/web-data-srilm
 
END
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

source_data_dir=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  echo "## step01, json to text for ibm @ `date`"
  cat $source_data_dir/Lorelei/{blogPosts.json,forumPosts.json,tweets.json} | \
  source/egs/kws2016/georgian/extract-text-from-json.pl | \
  cat - $source_data_dir/Lorelei/{ted.txt,wiki.txt} | \
  source/egs/kws2016/georgian/break-text-into-utt.pl | \
  source/egs/kws2016/georgian/filter-georgian-text.pl $georgian_letter_set  | \
  gzip -c > $source_data_dir/Lorelei/overall-text.gz
  echo "## step01, done with json to text ('$source_data_dir/Lorelei/overall-text.gz') @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "## step02, organize text for bbn @ `date`"
  cat $source_data_dir/BBN/BBN-Webtext/WEB_DATA_TXT_FILTERED/weblm* | \
  source/egs/kws2016/georgian/break-text-into-utt.pl | \
  source/egs/kws2016/georgian/filter-georgian-text.pl $georgian_letter_set  | \
  gzip -c > $source_data_dir/BBN/overall-text.gz
  echo "## step02, done with bnn data ('$source_data_dir/BBN/overall-text.gz') @ `date`"
fi

if [ ! -z $step03 ]; then
  echo "## step03, sort word list @ `date`"
  gzip -cd $source_data_dir/BBN/overall-text.gz $source_data_dir/Lorelei/overall-text.gz | \
  perl -e 'use utf8; use open qw(:std :utf8); ($thresh) = @ARGV;  while(<STDIN>){chomp; @A = split(/\s+/); for($i = 0; $i < @A; $i++){$w = $A[$i]; $vocab{$w}++; } }
    for $w (sort {$vocab{$b} <=>$vocab{$a} } keys % vocab) {
      if($vocab{$w} >= $thresh) { print "$w\t$vocab{$w}\n"; } }' 3 | \
  source/egs/kws2016/georgian/filter-georgian-text.pl $georgian_letter_set  | \
  gzip -c > $source_data_dir/word-count.gz
  echo "## step03, done @ `date`"
fi
lm_source=$source_data_dir/lm-source
if [ ! -z $step04 ]; then
  echo "## step04, make organized data @ `date`"
  [ -d $lm_source ] || mkdir -p $lm_source
  gzip -cd $source_data_dir/BBN/overall-text.gz $source_data_dir/Lorelei/overall-text.gz | \
  gzip -c > $lm_source/train-text.gz
  gzip -cd $source_data_dir/word-count.gz | \
  awk '{print $1;}' | gzip -c > $lm_source/vocab.gz
  echo "## step04, done @ `date`"
fi
int_lmdir=${tgtdir}-int
## step07 should be run first
int_dict=$tgtdir/../dict-int
if [ ! -z $step07 ]; then
  echo "## step07, make big dict @ `date`"
  [ -z $in_domain_dict_dir ] && { echo "## ERROR: in_domain_dict_dir not specified"; exit 1; }
  for x in lexicon.txt nonsilence_phones.txt silence_phones.txt optional_silence.txt extra_questions.txt; do
    file=$in_domain_dict_dir/$x
    [ -f $file ] || { echo "## ERROR: file $file not ready in $in_domain_dict_dir"; exit 1; }
  done
  [ -d $int_dict ] || mkdir -p $int_dict
  cat $in_domain_dict_dir/lexicon.txt | \
  source/egs/kws2016/georgian/enlarge-grapheme-lexicon.pl \
   "gzip -cd $lm_source/vocab.gz|" $in_domain_dict_dir/nonsilence_phones.txt | \
   sort -u > $int_dict/lexicon.txt
   cp $in_domain_dict_dir/{nonsilence_phones.txt,silence_phones.txt,optional_silence.txt,extra_questions.txt} \
   $int_dict/
   utils/validate_dict_dir.pl $int_dict 
   if [ ! -z $tgt_lang_dir ]; then
     echo "## step07, making tgt_lang from $int_dict"
     utils/prepare_lang.sh \
     --share-silence-phones true \
     $int_dict "<unk>" $int_dict/tmp.lang $tgt_lang_dir
     echo "## step07, done"
   fi
  echo "## step07 done @ `date`"
fi

if [ ! -z $step10 ]; then
  echo "## step10, language model interpolation @ `date`"
  in_lm_dir=$int_lmdir/indomain_lms
  echo "## LOG: prepare data for indomain lm training ($in_lm_dir)  @ `date`"
  [ -d $in_lm_dir ] || mkdir -p $in_lm_dir
  cat $tgt_lang_dir/words.txt | grep -v '\#' | grep -v '<eps>' | \
  gzip -c > $in_lm_dir/vocab.gz
  cat $indomain_train_text | \
  cut -f2-  -d' ' | gzip -c > $in_lm_dir/train-text.gz
  cut -f2- -d' ' $devdata | gzip -c > $in_lm_dir/dev-text.gz
  out_lm_dir=$int_lmdir/out_of_domain_lms
  echo "## LOG: prepare data for out-of-domain lm training ($out_lm_dir)  @ `date`"
  [ -d $out_lm_dir ] || mkdir -p $out_lm_dir
  cp $in_lm_dir/vocab.gz $out_lm_dir/  || exit 1
  cp $lm_source/train-text.gz $out_lm_dir/  || exit 1
  cp $in_lm_dir/dev-text.gz $out_lm_dir/  || exit 1
  source/egs/kws2016/georgian/lm-interpolate.sh --steps 1,2,3 $int_lmdir
  echo "## step10, language model interpolation done @ `date`"
fi
if [ ! -z $step11 ]; then
  echo "## step11, make grammar @ `date`"
  if [ ! -z $tgt_lang_dir ]; then
    local/arpa2G.sh $int_lmdir/lm.gz $tgt_lang_dir $tgt_lang_dir
  fi
  echo "## step11, done ($tgt_lang_dir) @ `date`"
fi

if [ ! -z $step12 ]; then
  echo "## step12, make HCLG.fst @ `date`"
  if [ ! -z $tgt_gmm_dir ] && [ ! -z $tgt_lang_dir ]; then
    graphdir=$tgt_gmm_dir/graph-int
    utils/mkgraph.sh $tgt_lang_dir $tgt_gmm_dir  $graphdir
  fi
  echo "## step12, making HCLG.fst ($graphdir) done @ `date`"
fi

