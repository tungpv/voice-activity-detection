#!/usr/bin/perl
use strict;
use warnings;
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  print STDERR "\nExample: cat text | $0 <letter-set.txt> > filtered.txt\n\n";
  exit 1;
}

my ($letter_set_file) = @ARGV;
open(F, "$letter_set_file") or die "## ERROR: letter-set file $letter_set_file cannot open\n";
my %vocab = ();
while(<F>) {
  chomp;
  m/(\S+)/ or next;
  $vocab{$1} ++;
}
close F;
# begin functions
sub IsUnknownWord {
  my ($vocab, $word) = @_;
  return 1 if length $word == 0;
  my @A = split(//, $word);
  # print STDERR "$word = ", join(" ", @A), "\n";
  for(my $i = 0; $i < @A; $i++ ) {
    my $letter = $A[$i];
    return 1 if not exists $$vocab{$letter};
  } 
  return 0;
}

sub IsUnknownLine {
  my ($vocab, $line) = @_;
  my @A = split(/\s+/);
  my $unknown_total = 0; my $words_of_line = scalar @A;
  return 1 if $words_of_line == 0;
  for(my $i = 0; $i < @A; $i++) {
    my $word = $A[$i];
    $unknown_total ++ if IsUnknownWord($vocab, $word) == 1;
  }
  return 1 if ($unknown_total/$words_of_line >=0.8);
  return 0;
}
# end functions
print STDERR "$0 stdin expected\n";
while(<STDIN>) {
  chomp;
  if(IsUnknownLine(\%vocab,$_) ==1) {
     print STDERR "unknown line: '$_' && skipped\n";
     next;
  }
  print "$_\n";
}
print STDERR "$0 stdin ended\n";
