#!/usr/bin/perl
use utf8;
use open qw(:std :utf8);

my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  print STDERR "\ncat text | $0 <from-field (int)>\n\n";
  exit 1;
}
my ($from) = @ARGV;

print STDERR "## LOG: $0 stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);  
  my $utt = "";
  for(my $i = 0; $i < $from-1; $i ++) {
    $utt .= "$A[$i] ";
  } 
  for(my $i = $from-1; $i < @A; $i ++) {
    my $w = $A[$i];
    if($w =~ m/</g) {
      $utt .= "$w ";
    } elsif($w =~ m/[\-_]/g) {
      my @B = split(/[\-_]/, $w);
      $w = join(" ", @B);
      $utt  .= "$w ";
    } else {
      $utt .= "$w ";
    }
  } 
  $utt =~ s/ $//g;
  print "$utt\n";
}
print STDERR "## LOG: $0 stdin ended\n";
