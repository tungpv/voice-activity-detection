#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
steps=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <morfessor-model> <source-data> <tgt-data>
 [examples]:

 $0  /home2/hhx502/kws2016/georgian/data/local/morfessor-exp/model/morph.mdl \
 /home2/hhx502/kws2016/georgian/data/dev/fbank-pitch \
 /home2/hhx502/kws2016/georgian/data/subword-dev/fbank-pitch

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

morph_model=$1
sdata=$2
data=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -d $data ] || mkdir -p $data
cp $sdata/*  $data/ 
rm $data/text  || exit 1
[ -f $sdata/text ] || { echo "## ERROR: text expected ($sdata)"; exit 1; }
[ -f $morph_model ] || { echo "## ERROR: model ($morph_model) does not exist"; exit 1; }

source/egs/kws2016/georgian/make-subword-text.sh --steps 1,2,3 --from-field 2 \
$morph_model  $sdata/text $data
