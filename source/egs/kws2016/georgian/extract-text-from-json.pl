#!/usr/bin/perl
use strict;
use warnings;
use utf8;
use open qw(:std :utf8);

print STDERR "stdin expected\n";
while(<STDIN>) {
  chomp;
  m#\"data\"\s*:\s*(.*),\s*\"non_normalized\"#g or  
  m#\"data\"\s*:\s*(.*),\s*\"languageCode\"#g or  next; 
  my $utt = $1; 
  my @A = split(/[,\.!]/, $utt);
  for (my $i = 0; $i < @A; $i++) {
    $utt = $A[$i];
    next if length $utt == 0;
    $utt =~ s#[[:punct:]]# #g;  
    $utt =~ s/[\s]+/ /g; 
    $utt =~ s/^ //;  
    $utt =~ s/ $//; 
    next if ($utt =~ /^$/);
    print $utt, "\n";
  }
}
print STDERR "stdin ended\n";
