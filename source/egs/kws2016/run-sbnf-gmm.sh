#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=
# end options

. parse_options.sh

function Usage {
 cat<<END
 $0 [options] <tgtdir>
 [options]:
 [steps]:
 1: cross-knowledge transfer learning
 2: test sbnf extractor with hybrid mode
 3: making bnf to test bnf based gmm-hmm
 4: test bnf gmm-hmm

END
}
if [ $# -ne 1 ]; then
  Usage && exit 1
fi
tgtdir=$1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
train_data=kws2016/flp-grapheme/data/train/fbank_pitch
alidir=kws2016/flp-grapheme/exp/mono/tri4a/ali_train
lang=kws2016/flp-grapheme/data/lang
sbnf_extractor_dir=$tgtdir/cross-sbnf/sbnf-extractor
if [ ! -z $step01 ]; then
  echo "## LOG: step01, cross-knowledge transfer learning @ `date`"
  source_nnet_dir=/home2/hhx502/kws2016/mling24x/bnf-extractor-part2
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2 $train_data \
  $lang $source_nnet_dir $alidir $sbnf_extractor_dir || exit 1
  echo "## LOG: step01, done @ `date`"
fi

if [ ! -z $step02 ]; then
  echo "## LOG: step02, hybrid decode @ `date`"
  source/egs/swahili/decode.sh --cmd slurm.pl --nj 40 --steps 1,2 \
  --scoring-opts "--min-lmwt 8 --max-lmwt 15" \
  --decode-opts "--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000" \
  kws2016/vllp-grapheme/data/dev/fbank_pitch kws2016/flp-grapheme/data/lang \
  kws2016/flp-grapheme/exp/mono/tri4a/graph $sbnf_extractor_dir/decode_dev || exit 1
  echo "## LOG: step02, done @ `date`"
fi

if [ ! -z $step03 ]; then
  echo "## LOG: step03, making bnf @ `date`"
  for x in kws2016/flp-grapheme/data/train kws2016/vllp-grapheme/data/dev; do
    source/egs/swahili/make_feats.sh --make-bnf true \
    --cmd "$cmd" --nj $nj \
    --bnf-extractor-dir $sbnf_extractor_dir \
    $x/fbank_pitch $x/cross-bnf-mling24x  $x/feat/cross-bnf-mling24x || exit 1
  done
  echo "## LOG: step03, done @ `date`"
fi
sbnf_train=kws2016/flp-grapheme/data/train/cross-bnf-mling24x
sbnf_dev=kws2016/vllp-grapheme/data/dev/cross-bnf-mling24x
if [ ! -z $step04 ]; then
  echo "## LOG: step04, run gmm-hmm @ `date`"
  source/egs/swahili/run-gmm-v2.sh --steps 1,2,3,4,5,6,7 --cmd "$cmd" --nj 40 \
  --train-id a \
  --cmvn-opts "--norm-means=true" --state-num 5000 --pdf-num 75000 \
  --alidir $alidir \
  --devdata $sbnf_dev \
  $sbnf_train $lang $tgtdir/cross-sbnf || exit 1

  echo "## LOG: step04, done @ `date`"
fi
