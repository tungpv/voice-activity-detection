#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo
# begin options
cmd=run.pl
nj=40
nnet_forward=nnet-forward
copy_data=false
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <data> <nnetdir> <tgtdir>
 [options]:
 --cmd                         # value, "$cmd"
 --nj                          # value, $nj
 --nnet-forward                # value, "$nnet_forward"
 --copy-data                   # value, $copy_data
 [steps]:
 [examples]:
 $0 --cmd slurm.pl --nj 40 --copy-data true kws2016/flp-grapheme/data/train-u10-test/fbank-pitch kws2016/flp-grapheme/exp/mono/utt2spk-with-nnet/spk-nnet kws2016/flp-grapheme/exp/mono/utt2spk-with-nnet/decode-train-u10-test

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi

data=$1
nnetdir=$2
tgtdir=$3

sdata=$data/split$nj;
[[ -d $sdata && $data/feats.scp -ot $sdata ]] || split_data.sh $data $nj || exit 1;
for i in $(seq 1 $nj); do
  curdir=$sdata/$i
  cat $curdir/utt2spk | \
  perl -pe 'chomp; @A = split(/\s+/); $utt = $A[0]; $spk = $utt; $spk =~ s/_[^_]+$//;
    $_ = "$utt $spk\n";' > $curdir/short2long-utt
  utils/utt2spk_to_spk2utt.pl < $curdir/short2long-utt > $curdir/long2short-utt
done
mkdir -p $tgtdir/log

echo $nj >> $tgtdir/num_jobs

nnet=$nnetdir/final.nnet
feature_transform=$nnetdir/final.feature_transform
spk2index=$nnetdir/spk2index
for f in $sdata/1/feats.scp $nnet $feature_transform $spk2index; do
  [ -f $f ] || { echo "## ERROR: $0, missing file $f"; exit 1; }
done

# import config,
cmvn_opts=
delta_opts=
D=$nnetdir
[ -e $D/norm_vars ] && cmvn_opts="--norm-means=true --norm-vars=$(cat $D/norm_vars)" # Bwd-compatibility,
[ -e $D/cmvn_opts ] && cmvn_opts=$(cat $D/cmvn_opts)
[ -e $D/delta_order ] && delta_opts="--delta-order=$(cat $D/delta_order)" # Bwd-compatibility,
[ -e $D/delta_opts ] && delta_opts=$(cat $D/delta_opts)
#
# Create the feature stream,
feats="ark,s,cs:copy-feats scp:$sdata/JOB/feats.scp ark:- |"

# apply-cmvn (optional),
[ ! -z "$cmvn_opts" -a ! -f $sdata/1/cmvn.scp ] && echo "$0: Missing $sdata/1/cmvn.scp" && exit 1
[ ! -z "$cmvn_opts" ] && feats="$feats apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp ark:- ark:- |"
# add-deltas (optional),
[ ! -z "$delta_opts" ] && feats="$feats add-deltas $delta_opts ark:- ark:- |"
$cmd JOB=1:$nj $tgtdir/log/decode.JOB.log \
$nnet_forward --feature-transform=$feature_transform --use-gpu=no "$nnet" "$feats" ark,t:-  \| \
 source/code2/map-utterance-to-speaker  ark:$spk2index ark:$sdata/JOB/long2short-utt  ark:- $tgtdir/score.JOB || exit 1
