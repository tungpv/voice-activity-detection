#!/bin/bash

. path.sh
. cmd.sh

echo 
echo "## LOG: $0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=

validating_rate=0.1
learn_rate=0.008
cmvn_opts=
delta_opts=
nnet_train_cmd=steps/nnet/train.sh
nnet_pretrain_cmd="steps/nnet/pretrain_dbn.sh --feat-type traps  --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048"
nnet_id=nnet5a

devdata=
devname=dev
trainname=train
graphdir=
decodename=decode-dev
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15 "
example_dir=
# end options

. parse_options.sh || exit 1

dir=$example_dir

function Usage {
 cat<<END
 Usage: $0 [options] <data> <lang> <alidir> <dir>
 [options]:
 --cmd                                         # value, "$cmd"
 --nj                                          # value, "$nj"
 --steps                                       # value, "$steps"

 --validating-rate                             # value, $validating_rate
 --learn-rate                                  # value, $learn_rate
 --cmvn-opts                                   # value, "$cmvn_opts"
 --delta-opts                                  # value, "$delta_opts"
 --nnet-train-cmd                              # value, "$nnet_train_cmd"
 --nnet-pretrain-cmd                           # value, "$nnet_pretrain_cmd"
 --nnet-id                                     # value, "$nnet_id"
 
 --devdata                                     # value, "$devdata"
 --devname                                     # value, "$devname"
 --trainname                                   # value, "$trainname"
 --graphdir                                    # value, "$graphdir"
 --decodename                                  # value, "$decodename"
 --decode-opts                                 # value, "$decode_opts"
 --scoring-opts                                # value, "$scoring_opts"
 --example-dir                                 # value, "$example_dir"

 [steps]:
 1: run and test ce dnn
 5: run and test smbr dnn
 [example]:

 $0 --steps 1,5  --delta-opts "--delta-order=2" --cmvn-opts "--norm-means=true" \
 --nnet-pretrain-cmd "steps/nnet/pretrain_dbn.sh --feat-type traps  --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 6 --hid-dim 2048" \
--devdata $dir/data/dev/fbank-pitch \
--graphdir $dir/exp/tri4a/graph \
$dir/data/merge_train/fbank-pitch \
$dir/data/lang \
$dir/exp/tri4a/ali_merge-train \
$dir/exp 

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi
data=$1
lang=$2
alidir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  echo "## LOG: step01, run ce dnn training and testing @ `date`"
  source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 \
  --validating-rate $validating_rate \
  --learn-rate $learn_rate \
  ${cmvn_opts:+ --cmvn-opts "$cmvn_opts"} \
  ${delta_opts:+ --delta-opts "$delta_opts"} \
  --nnet-train-cmd "$nnet_train_cmd"\
  --nnet-pretrain-cmd  "$nnet_pretrain_cmd" \
  --nnet-id "$nnet_id" \
  ${devdata:+ --devdata $devdata} \
  ${graphdir:+ --graphdir $graphdir} \
  --decode-opts "$decode_opts" \
  --scoring-opts "$scoring_opts" \
  $data $lang $alidir $tgtdir || exit 1
  echo "## LOG: step01, ended @ `date`"
fi
if [ ! -z $step02 ]; then
  echo "## LOG: step02, run ce dnn training and testing @ `date`"
  source/egs/kws2016/run-nnet.sh --steps 3,4 \
  --validating-rate $validating_rate \
  --learn-rate $learn_rate \
  ${cmvn_opts:+ --cmvn-opts "$cmvn_opts"} \
  ${delta_opts:+ --delta-opts "$delta_opts"} \
  --nnet-train-cmd "$nnet_train_cmd"\
  --nnet-pretrain-cmd  "$nnet_pretrain_cmd" \
  --nnet-id "$nnet_id" \
  ${devdata:+ --devdata $devdata} \
  ${graphdir:+ --graphdir $graphdir} \
  --decode-opts "$decode_opts" \
  --scoring-opts "$scoring_opts" \
  $data $lang $alidir $tgtdir || exit 1
  echo "## LOG: step02, ended @ `date`"
fi

if [ ! -z $step05 ]; then
  echo "## LOG: step05, run smbr DNN training and testing @ `date`"
  source/egs/kws2016/run-sequential-nnet.sh --steps 1,2,3,4 \
  ${devdata:+ --devdata $devdata} \
  ${graphdir:+ --graphdir $graphdir} \
  --decode-opts "$decode_opts" \
  --scoring-opts "$scoring_opts" \
  $data $lang $tgtdir/$nnet_id $tgtdir/smbr-${nnet_id} || exit 1
  echo "## LOG: step05, done @ `date`"
fi

if [ ! -z $step06 ]; then
  echo "## LOG: step06, rerun step05 @ `date`"
  source/egs/kws2016/run-sequential-nnet.sh --steps 3,4 \
  ${devdata:+ --devdata $devdata} \
  ${graphdir:+ --graphdir $graphdir} \
  --decode-opts "$decode_opts" \
  --scoring-opts "$scoring_opts" \
  $data $lang $tgtdir/$nnet_id $tgtdir/smbr-${nnet_id} || exit 1
  echo "## LOG: step06, done @ `date`"
fi
