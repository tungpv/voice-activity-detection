#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo 

# begin options
cmd=slurm.pl
nj=40
steps=
word_merge_tolerance=0.02

# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $(basename $0) [options] <sdata> <utt_ctm> <data>
 [options]:
 [steps]:
 1: remove non-content words from ctm file

 [example]:
 $0 --steps 1  kws2016/flp-grapheme/data/train-sub/fbank_pitch  kws2016/flp-grapheme/exp/mono/lhuc-train-sub/ali_train-sub_nnet5a-sub/utt_ctm  kws2016/flp-grapheme/exp/mono/utt2spk-with-nnet/spk-data

END
}

if [ $# -ne 3 ]; then
  Usage && exit 1
fi
sdata=$1
utt_ctm=$2
data=$3

if [ ! -z "$steps" ]; then
  old_steps=$steps
  steps=$(echo -n "$steps"| perl -pe '@A =split(/[,:]/); $s = ""; for($i = 0; $i < @A; ++$i){ $g=$A[$i]; if($g =~ /[\-]/) { @A_ = split(/[\-]/, $g);   die if (scalar @A_ != 2 || $A_[0] >= $A_[1]); $g_ = ""; for($j=$A_[0]; $j <= $A_[1]; $j++){ $g_ .= "$j,"; } $s .= "$g_";  }else { $s .= "$A[$i]," } } $s =~ s/,$//; $_ = $s;') || { echo "## ERROR: illegal $old_steps"; exit 1; }
  echo "steps=$steps"
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
else
  echo "## LOG: WARNING, no step is specified, nothing to do !" && exit 1
fi
[ -d $data ] || mkdir -p $data
if [ ! -z $step01 ]; then
  echo "## LOG: step01, remove nono-content words started @ `date`"
  source/code/extract-sub-segment-with-conf --conf-thresh=0  \
  $sdata/segments  "grep -v -E '<' $utt_ctm |"  $data/segments $data/text
  cp $sdata/wav.scp $data/wav.scp
  cat $data/segments | \
  perl -e '($utt2spk) = @ARGV; open(F, "<", "$utt2spk") or die "## ERROR: utt2spk $utt2spk cannot open\n";
    while(<F>) {chomp; @A = split(/\s+/); if(@A == 2){ $vocab{$A[0]} = $A[1]; } } close F;
    while(<STDIN>) {chomp; @A = split(/[\s+]/); if(@A == 4) { $new_utt = $A[0]; $utt = $new_utt; $utt =~ s/_\d+$//; 
      if(not exists $vocab{$utt}) {die "## ERROR: no see utt $utt\n";}  print "$new_utt $vocab{$utt}\n";
    }}
  ' $sdata/utt2spk  > $data/utt2spk || { echo "## ERROR: failed to generate utt2spk"; exit 1; }
  utils/utt2spk_to_spk2utt.pl < $data/utt2spk > $data/spk2utt
  utils/fix_data_dir.sh $data

  echo "## LOG: step01, done @ `date`"
fi

