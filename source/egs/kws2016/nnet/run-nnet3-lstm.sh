#!/bin/bash

echo
echo "$0 $@"
echo 

. path.sh
. cmd.sh

# begin options
cmd=slurm.pl
nj=40
steps=
train_stage=-10
use_gpu=true
lstm_delay=" -1 -2 -3"
ivector_dir=
num_lstm_layers=3
splice_indexes="-2,-1,0,1,2 0 0"
cell_dim=1024
hidden_dim=1024
recurrent_projection_dim=256
non_recurrent_projection_dim=256
label_delay=5

num_epochs=8
samples_per_iter=2000000
num_jobs_initial=1
num_jobs_final=1
initial_effective_lrate=0.0003
final_effective_lrate=0.00004
num_chunk_per_minibatch=100
momentum=0.5
chunk_width=20
chunk_left_context=40
chunk_right_context=0
common_egs_dir=
remove_egs=true
reporting_email=

devdata=
graphdir=
decodename=decode_dev
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <data> <lang> <alidir> <tgtdir>
 [options]:
 --cmd                                             # value, "$cmd"
 --nj                                              # value, $nj
 --steps                                           # value, "$steps"
 --train-stage                                     # value, $train_stage
 --use-gpu                                         # value, $use_gpu

 --lstm-delay                                      # value, "$lstm_delay" 
 --ivector_dir                                     # value, "$ivector_dir"
 --num-lstm-layers                                 # value, $num_lstm_layers
 --splice-indexes                                  # value, "$splice_indexes"
 --cell-dim                                        # value, $cell_dim
 --hidden-dim                                      # value, $hidden_dim  
 --recurrent-projection-dim                        # value, $recurrent_projection_dim
 --non-recurrent-projection-dim                    # value, $non_recurrent_projection_dim
 --label-delay                                     # value, $label_delay
 
 --num-epochs                                      # value, $num_epochs
 --samples-per-iter                                # value, $samples_per_iter
 --num-jobs-initial                                # value, $num_jobs_initial
 --num-jobs-final                                  # value, $num_jobs_final
 --initial-effective-lrate                         # value, $initial_effective_lrate
 --final-effective-lrate                           # value, $final_effective_lrate
 --num-chunk-per-minibatch                         # value, $num_chunk_per_minibatch
 --momentum                                        # value, $momentum
 --chunk-width                                     # value, $chunk_width
 --chunk-left-context                              # value, $chunk_left_context
 --chunk-right-context                             # value, $chunk_right_context
 --common-egs-dir                                  # value, "$common_egs_dir"
 --remove-egs                                      # value, $remove_egs
 --reporting_email                                 # value, "$reporting_email"

 --devdata                                         # value, "$devdata"
 --graphdir                                        # value, "$graphdir"
 --decodename                                      # value, "$decodename"
 
 [steps]:
 1: data separation
 2: train lstm
 4: update transitional model, separately
 7: decode
 [examples]:
 
 $0  --steps 1,2 --num-epochs 8 --samples-per-iter 2000000 \
 --num-jobs-initial 1 \
 --num-jobs-final 1 \
 --num-chunk-per-minibatch 100 \
 kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang \
 kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/nnet3-lstm

 $0  --steps 2 --train-stage 17 --use-gpu false --num-epochs 8 --samples-per-iter 1000000 \
 --num-jobs-initial 1 \
 --num-jobs-final 1 \
 --num-chunk-per-minibatch 100 \
 kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang \
 kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/nnet3-lstm

 $0 --steps 1,2 --cmd slurm.pl  --num-epochs 8 --chunk-width 10 --samples-per-iter 1000000 \
 --num-jobs-initial 1 --num-jobs-final 1 --num-chunk-per-minibatch 100 swbd/data/train/fbank_pitch \
 swbd/data/lang swbd/exp/tri4a/ali_train swbd/exp/nnet3-lstm-chkw10 

 $0  --steps 4 --cmd run.pl --num-epochs 8 --chunk-width 10 --samples-per-iter 1000000 \
 --num-jobs-initial 1 --num-jobs-final 1 --num-chunk-per-minibatch 100 \
 swbd/data/train/fbank_pitch swbd/data/lang swbd/exp/tri4a/ali_train swbd/exp/nnet3-lstm-chkw10 
 
 $0 --steps 1,2,4 --cmd run.pl --num-epochs 8 --chunk-width 10 --samples-per-iter 1000000 \
 --num-jobs-initial 1 --num-jobs-final 1 --num-chunk-per-minibatch 100 \
 --chunk-left-context 20  kws2016/flp-grapheme/data/train/cross-bnf-mling24x \
 kws2016/flp-grapheme/data/lang  kws2016/flp-grapheme/exp/cross-sbnf/tri4a/ali_train \
 kws2016/flp-grapheme/exp/cross-sbnf/nnet3-lstm

 $0 --steps 1,2,4 --cmd run.pl --num-epochs 8 --chunk-width 10 --samples-per-iter 100000 \
 --num-jobs-initial 1 --num-jobs-final 1 --num-chunk-per-minibatch 100 \
 --chunk-left-context 20  mandarin/data/train/bnf mandarin/data/lang mandarin/exp/tri4a/ali_train \
 mandarin/exp/nnet3-lstm-use-multilingual-bnf

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
alidir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
else
  echo "## WARNING: steps are not specified"
  Usage && exit 0
fi

if [ ! -z  $step01 ]; then
  echo "$0: creating neural net configs";
  config_extra_opts=()
  [ ! -z "$lstm_delay" ] && config_extra_opts+=(--lstm-delay "$lstm_delay")
  steps/nnet3/lstm/make_configs.py  "${config_extra_opts[@]}" \
    --feat-dir $data \
    ${ivector_dir:+--ivector-dir $ivector_dir} \
    --ali-dir $alidir \
    --num-lstm-layers $num_lstm_layers \
    --splice-indexes "$splice_indexes " \
    --cell-dim $cell_dim \
    --hidden-dim $hidden_dim \
    --recurrent-projection-dim $recurrent_projection_dim \
    --non-recurrent-projection-dim $non_recurrent_projection_dim \
    --label-delay $label_delay \
    --self-repair-scale 0.00001 \
   $tgtdir/configs || exit 1;
fi
if [ ! -z $step02 ]; then
  echo "## step02, training started @ `date`"
  steps/nnet3/train_rnn.py --stage=$train_stage  \
    --cmd="$cmd" \
    --feat.cmvn-opts="--norm-means=false --norm-vars=false" \
    --trainer.num-epochs=$num_epochs \
    --trainer.samples-per-iter=$samples_per_iter \
    --trainer.optimization.num-jobs-initial=$num_jobs_initial \
    --trainer.optimization.num-jobs-final=$num_jobs_final \
    --trainer.optimization.initial-effective-lrate=$initial_effective_lrate \
    --trainer.optimization.final-effective-lrate=$final_effective_lrate \
    --trainer.optimization.shrink-value 0.99 \
    --trainer.rnn.num-chunk-per-minibatch=$num_chunk_per_minibatch \
    --trainer.optimization.momentum=$momentum \
    --egs.chunk-width=$chunk_width \
    --egs.chunk-left-context=$chunk_left_context \
    --egs.chunk-right-context=$chunk_right_context \
    --egs.dir="$common_egs_dir" \
    --cleanup.remove-egs=$remove_egs \
    --cleanup.preserve-model-interval=100 \
    --use-gpu=$use_gpu \
    --feat-dir=$data \
    --ali-dir=$alidir \
    --lang=$lang \
    --reporting.email="$reporting_email" \
    --dir=$tgtdir  || { echo "## ERROR: step02, train_rnn.py failed"; }
fi
if [ ! -z $step04 ]; then
  echo "## step04, update transitional model @ `date`"
  stage_num=$(ls $tgtdir/*.mdl |grep -v combined.mdl |  perl -pe 's/(.*)\/([^\/]+)\.mdl/$2/;' | awk '{if($1>x){x=$1;}}END{print x;}')
  steps/nnet3/train_rnn.py --stage=$stage_num  \
    --cmd="$cmd" \
    --feat.cmvn-opts="--norm-means=false --norm-vars=false" \
    --trainer.num-epochs=$num_epochs \
    --trainer.samples-per-iter=$samples_per_iter \
    --trainer.optimization.num-jobs-initial=$num_jobs_initial \
    --trainer.optimization.num-jobs-final=$num_jobs_final \
    --trainer.optimization.initial-effective-lrate=$initial_effective_lrate \
    --trainer.optimization.final-effective-lrate=$final_effective_lrate \
    --trainer.optimization.shrink-value 0.99 \
    --trainer.rnn.num-chunk-per-minibatch=$num_chunk_per_minibatch \
    --trainer.optimization.momentum=$momentum \
    --egs.chunk-width=$chunk_width \
    --egs.chunk-left-context=$chunk_left_context \
    --egs.chunk-right-context=$chunk_right_context \
    --egs.dir="$common_egs_dir" \
    --cleanup.remove-egs=$remove_egs \
    --cleanup.preserve-model-interval=100 \
    --use-gpu=false \
    --feat-dir=$data \
    --ali-dir=$alidir \
    --lang=$lang \
    --reporting.email="$reporting_email" \
    --dir=$tgtdir  || { echo "## ERROR: step02, train_rnn.py failed"; }
  echo "## step04, done @ `date`"
fi
if [ ! -z $step07 ];then
  echo "## step07, decode @ `date`"
  steps/nnet3/lstm/decode.sh --nj $nj --cmd "slurm.pl" \
          --extra-left-context $chunk_left_context  \
          --extra-right-context $chunk_right_context  \
          --frames-per-chunk $chunk_width \
         $graphdir $devdata $tgtdir/$decodename || exit 1;

  echo "## step07, done @ `date`"

fi
