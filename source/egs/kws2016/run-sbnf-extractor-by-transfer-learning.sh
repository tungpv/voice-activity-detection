#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo

# begin options
cmd=run.pl
steps=
subset_training_rate=0.4
validating_rate=0.1
subset_max_iter=8
train_bn1_cmd="steps/nnet/train.sh --hid-dim 1500 --hid-layers 2 --learn-rate 0.008 --bn-dim 80 --feat-type traps --splice 5"
h1_splice_cmd="utils/nnet/gen_splice.py --fea-dim=80 --splice=2 --splice-step=5 |"
train_bn2_cmd="steps/nnet/train.sh --hid-layers 2 --hid-dim 1500 --bn-dim 30 --learn-rate 0.008"
subset_train_tool_opts="--minibatch-size=256 --randomizer-size=32768 --randomizer-seed=777"
fullset_train_tool_opts="--minibatch-size=1024 --randomizer-size=32768 --randomizer-seed=777"
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 $0 [options] <data> <lang> <alidir> <tgtdir>
 [options]:
 --cmd                                   # value, "$cmd"
 --steps                                 # value, "$steps"
 --subset-training-rate                  # value, $subset_training_rate
 --validating-rate                       # value, $validating_rate
 --subset-max-iter                       # value, $subset_max_iter
 --train_bn1_cmd                         # value, "$train_bn1_cmd"
 --h1-splice-cmd                         # value, "$h1_splice_cmd"
 --train-bn2-cmd                         # value, "$train_bn2_cmd"
 --subset-train-tool-opts                # value, "$subset_train_tool_opts"
 --fullset-train-tool-opts               # value, "$fullset_train_tool_opts"
 [steps]:
 1: split data
 2: train subset h1
 3: train full h1
 4: train subset h2
 5: train full h2
 [examples]:

 $0 --steps 1,2,3,4,5 kws2016/flp-grapheme/data/train/fbank_pitch \
 kws2016/flp-grapheme/data/lang  kws2016/flp-grapheme/exp/mono/tri4a/ali_train \
 kws2016/flp-grapheme/exp/mono/sbnf-extractor-by-tl
 
 $0 --steps 1,2,3,4,5 --subset-training-rate 0.06 \
 swbd-fisher-english/data/train/fbank-pitch swbd-fisher-english/data/lang \
 /local/hhx502/ldc-cts2016/swbd-fisher-english/exp/tri4a/ali_train \
 /local/hhx502/ldc-cts2016/swbd-fisher-english/exp/sbnf-extractor-by-tl

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
alidir=$3
tgtdir=$4

subset_dir=$tgtdir/subset
full_dir=$tgtdir/full

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
else
  echo "## WARNING: steps are not specified"
  Usage && exit 0
fi

train=$full_dir/train
valid=$full_dir/valid
subset_overall_train=$subset_dir/overall-train
subset_train=$subset_dir/train
subset_valid=$subset_dir/valid
if [ ! -z $step01 ]; then
  echo "## LOG: step01, prepare training data @ `date`"
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $train \
  $data  $valid || exit 1
 
 source/egs/swahili/subset_data.sh --subset_time_ratio $subset_training_rate \
 --random true \
 $train  $subset_overall_train || exit 1
 
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $subset_train \
  $subset_overall_train  $subset_valid || exit 1

  echo "## LOG: step01 done @ `date`"
fi
subset_h1=$subset_dir/h1
if [ ! -z $step02 ]; then
 echo "## LOG: step02, train bnf1 with subset data @ `date`"
  $train_bn1_cmd \
  --train-tool-opts "$subset_train_tool_opts" \
  --scheduler-opts "--max-iters $subset_max_iter" \
  --cmvn-opts "--norm-means=true --norm-vars=true" \
  --delta-opts "--delta-order=2"\
  $subset_train $subset_valid $lang $alidir $alidir $subset_h1 || exit 1
 echo "## LOG: step02, done @ `date`"
fi
full_h1=$full_dir/h1
feat_transform=$full_h1/final.feature_transform.bn1
if [ ! -z $step03 ]; then
  echo "## LOG: step03, train bnf1 with full data @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 2 \
  --train-data-dir $full_dir \
  --train-tool-opts "$fullset_train_tool_opts" \
  dummy $lang $subset_h1 $alidir $full_h1 || exit 1
  
  nnet-concat --binary=false $full_h1/final.feature_transform \
  "nnet-copy --remove-last-layers=4 --binary=false $full_h1/final.nnet - |" "$h1_splice_cmd"  $feat_transform || exit 1

  echo "## LOG: step03, train bnf1 done @ `date`"
fi
subset_h2=$subset_dir/h2
if [ ! -z $step04 ]; then
  echo "## LOG: step04, train bnf2 with subset data @ `date`"
  $train_bn2_cmd  \
  --train-tool-opts "$subset_train_tool_opts" \
  --scheduler-opts "--max-iters $subset_max_iter" \
  --feature-transform $feat_transform \
  $subset_train $subset_valid $lang $alidir $alidir $subset_h2 || exit 1;
  echo "## LOG: step04, done @ `date`"
fi
full_h2=$full_dir/h2
if [ ! -z $step05 ]; then
  echo "## LOG: step05, train bnf2 with full data @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 2 \
  --train-data-dir $full_dir \
  --train-tool-opts "$fullset_train_tool_opts" \
  dummy $lang $subset_h2 $alidir $full_h2 || exit 1
  echo "## LOG: step05, done @ `date`"
fi
