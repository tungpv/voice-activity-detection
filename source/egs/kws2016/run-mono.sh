#!/bin/bash

. path.sh
. cmd.sh
. conf/common.fullLP

echo 
echo "## LOG: $0 $@"
echo
# begin options
cmd=slurm.pl
decode_cmd="slurm.pl --exclude=node01,node02"
nj=40
steps=
oov_symbol="<unk>"
lexicon_flags="--romanized --oov <unk>"
phone_mapping=""
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <babel-package> <tgtdir>
 [options]:
 --cmd                                              # value, "$cmd"
 --decode-cmd                                       # value, "$decode_cmd"
 --steps                                            # value, "$steps"
 --oov-symbol                                       # value, "$oov_symbol"
 --lexicon-flags                                    # value, "$lexicon_flags"
 --phone-mapping                                    # value, "$phone_mapping"

 [steps]:
 1: uncompress the data
 4: prepare local
 5: prepare lang
 6: prepare train data
 7: prepare script data
 8: merge train & script data
 9: prepare dev data
 10: build srilm
 11: build G.fst
 12: make plp-pitch feature
 13: make fbank-pitch feature
 14: train GMM-HMM acoustic models using plp-pitch features
 15: redo step14 for decoding part
 [example]:
 $0 --steps 1 /data/users/hhx502/kws16/nist-lang-data/IARPA-babel101b-v0.4c-build-copr.zip  /home2/hhx502/kws2016

END
}
if [ $# -ne 2 ]; then
  Usage && exit 1
fi
babel_zip_file=$1
tgtdir=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
babel_data=$(dirname $babel_zip_file)
data_id=$(echo $babel_zip_file| perl -pe 'chomp; s/.*\///; s/IARPA-//; s/-copr\.zip//;')
babel_datadir=$(find $babel_data -maxdepth 1 -type d -exec ls -ld "{}" \; | grep $data_id| awk '{print $NF;}')
if [ ! -z $step01 ]; then
  echo "## LOG: step01, unzip file to $babel_data started @ `date`" 
  unzip $babel_zip_file -d $babel_data
  echo "## LOG: step01, unzip done ended @ `date`"
fi
train_data_dir=$(find $babel_datadir -maxdepth 3 -mindepth 3 -type d -exec ls -ld "{}" \; | \
 grep conversational | \
 grep training | \
 grep -v untrans | \
 awk '{print $NF;}' )
train_script_dir=$(
 find $babel_datadir -maxdepth 3 -mindepth 3 -type d -exec ls -ld "{}" \; | \
 grep script | \
 grep -v untrans | \
 grep training | awk '{print $NF;}' 
)
train_sub_dir=$(
 find $babel_datadir -maxdepth 3 -mindepth 3 -type d -exec ls -ld "{}" \; | \
 grep 'sub-train' | \
 grep 'conversational' | \
 awk '{print $NF;}' 
)
train_script_sub_dir=$(
 find $babel_datadir -maxdepth 3 -mindepth 3 -type d -exec ls -ld "{}" \; | \
 grep 'sub-train' | \
 grep 'script' | \
 awk '{print $NF;}' 
)

lexicon_dir=$(
 find $babel_datadir -maxdepth 3 -mindepth 3 -type d -exec ls -ld "{}" \; | \
 grep 'reference_materials' | \
 grep 'conversational' | \
 awk '{print $NF;}' 
)
lexicon_file=$lexicon_dir/lexicon.txt
dev_data_dir=$(
 find $babel_datadir -maxdepth 3 -mindepth 3 -type d -exec ls -ld "{}" \; | \
 grep 'dev' | \
 grep 'conversational' | \
 awk '{print $NF;}' 
)

tgtdata=$tgtdir/$data_id/data
raw_data_train=$tgtdata/raw_train_data
if [ ! -z $step02 ]; then
  echo "## LOG: step02, prepare data from $babel_datadir started @ `date`"
  [ -d $raw_data_train ] || mkdir -p $raw_data_train
  echo "babel_data=$babel_data"
  echo "data_id=$data_id"
  echo "babel_datadir=$babel_datadir"
  echo "train_data_dir=$train_data_dir"
  echo "train_script_dir=$train_script_dir"
  echo "sub-train=$train_sub_dir"
  echo "train_script_sub_dir=$train_script_sub_dir"
  echo "lexicon_dir=$lexicon_dir"
  echo "dev_data_dir=$dev_data_dir"
  find $train_data_dir/transcription  -name "*.txt" | \
  perl -pe 'chomp; s/.*\///; s/\.txt//; $_ = "$_\n";' > $train_data_dir/train_data_list
  local/make_corpus_subset.sh $train_data_dir $train_data_dir/train_data_list  $raw_data_train
  echo "## LOG: step02, ended @ `date`"
fi
train_conv_data_dir=$(readlink -f $raw_data_train)
raw_script_data=$tgtdir/$data_id/data/raw_script_data
if [ ! -z $step03 ]; then
  echo "## LOG: step03, prepare script data started @ `date`"
  find $train_script_dir/transcription -name "*.txt" | \
  perl -pe 'chomp; s/.*\///; s/\.txt//; $_ = "$_\n";' > $train_script_dir/train_data_list
  local/make_corpus_subset.sh $train_script_dir $train_script_dir/train_data_list  $raw_script_data
  echo "## LOG: done @ `date`"
fi
train_script_data_dir=$(readlink -f $raw_script_data)
local_dir=$tgtdata/local
if [ ! -z $step04 ]; then
  echo "## LOG: step04, prepare local dir started @ `date`"
  [ -d $local_dir ] || mkdir -p $local_dir
  local/make_lexicon_subset.sh $train_conv_data_dir/transcription  $lexicon_file \
  $local_dir/filtered_lexicon.txt
  local/prepare_lexicon.pl  --phonemap "$phone_mapping" \
 $lexicon_flags $local_dir/filtered_lexicon.txt $local_dir
  echo "## LOG: step04, done @ `date`"
fi
lang_dir=$tgtdata/lang
if [ ! -z $step05 ]; then
  echo "## LOG: step05, prepare lang dir started @ `date`"
  utils/prepare_lang.sh \
  --share-silence-phones true \
  $local_dir $oov_symbol $local_dir/tmp.lang $lang_dir
  echo "## LOG: step05, done @ `date`"
fi
train_data=$tgtdata/train
if [ ! -z $step06 ]; then
  echo "## LOG: step06, prepare train data started @ `date`"
  [ -d $train_data ] || mkdir -p $train_data
  local/prepare_acoustic_training_data.pl \
    --vocab $local_dir/lexicon.txt --fragmentMarkers \-\*\~ \
    $train_conv_data_dir $train_data > $train_data/skipped_utts.log
  echo "## LOG: step06 done @ `date`"
fi
script_data=$tgtdata/scripted_train
if [ ! -z $step07 ]; then
  echo "## LOG: step07, prepare scripted data started @ `date`"
  [ -d $script_data ] || mkdir -p $script_data
  local/prepare_acoustic_training_data.pl \
    --vocab $local_dir/lexicon.txt --fragmentMarkers \-\*\~ \
    $train_script_data_dir $script_data > $script_data/skipped_utts.log
  echo "## LOG: step07, done @ `date`" 
fi
train_merge=$tgtdata/merge_train
if [ ! -z $step08 ]; then
  echo "## LOG: step08, merge train and script data started @ `date`"
  utils/combine_data.sh $train_merge $train_data $script_data 
  source/data-len.sh $train_merge
  echo "## LOG: step08, done @ `date`"
fi
raw_dev_data=$tgtdata/raw_dev_data
dev_data=$tgtdata/dev
if [ ! -z $step09 ]; then
  echo "## LOG: step09, prepare dev data started @ `date`"
  find $dev_data_dir/transcription -name "*.txt" | \
  perl -pe 'chomp; s/.*\///; s/\.txt//; $_ = "$_\n";' > $dev_data_dir/dev_data_list
  local/make_corpus_subset.sh $dev_data_dir $dev_data_dir/dev_data_list  $raw_dev_data
  [ -d $dev_data ] || mkdir -p $dev_data  
  local/prepare_acoustic_training_data.pl \
   --fragmentMarkers \-\*\~ \
   $raw_dev_data  $dev_data > $dev_data/skipped_utts.log || exit 1
  echo "## LOG: prepare stm file"
  local/prepare_stm.pl --fragmentMarkers \-\*\~ $dev_data || exit 1
  echo "## LOG: step09, done @ `date`"
fi
srilm_dir=$tgtdata/srilm
if [ ! -z $step10 ]; then
  echo "## LOG: step10, build srilm started @ `date`"
  local/train_lms_srilm.sh --dev-text data/dev2h/text \
   --train-text data/train/text data data/srilm 
  local/train_lms_srilm.sh --dev-text $dev_data/text \
  --train-text $train_merge/text $tgtdata $srilm_dir 
  echo "## LOG: step10, done @ `date`"
fi
if [ ! -z $step11 ]; then
  echo "## LOG: step11, creating G.fst started @ `date`"
  local/arpa2G.sh $srilm_dir/lm.gz $lang_dir $lang_dir
  echo "## LOG: step11: done @ `date`"
fi
if [ ! -z $step12 ]; then
  echo "## LOG: step12, make plp+pitch features started @ `date`"
  for sdata in $train_merge $dev_data; do
    echo "## LOG: for data $sdata started @ `date`"
    data=$sdata/plp-pitch
    feat=$sdata/feat/plp-pitch
    source/egs/swahili/make_feats.sh --plp-pitch true  $sdata $data $feat 
  done
  echo "## LOG: step12, done @ `date`"
fi
if [ ! -z $step13 ]; then
  echo "# LOG: step13, make fbank+pitch features started @ `date`"
  for sdata in $train_merge $dev_data; do
    echo "## LOG: for data $sdata started @ `date`"
    data=$sdata/fbank-pitch
    feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --fbank-pitch true  $sdata $data $feat 
  done
  echo "## LOG: step13, done @ `date`"
fi
train=$train_merge/plp-pitch
dev=$dev_data/plp-pitch
tgtexpdir=$tgtdir/$data_id/exp
if [ ! -z $step14 ]; then
  echo "## LOG: step14, train GMM-HMM acoustic model started @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --steps 1,2,3,4,5,6,7 \
  --dataname merge-train --state-num 5000 --pdf-num 75000 \
  --devdata $dev --devname dev --decodename decode-dev  $train $lang_dir  $tgtexpdir
  echo "## LOG: step14, done @ `date`"
fi
if [ ! -z $step15 ]; then
  echo "## LOG: step15, redo step14 for decoding @ `date`"
  source/egs/swahili/run-gmm-v2.sh --nj $nj  --cmd "$decode_cmd" --steps 7 \
  --dataname merge-train --state-num 5000 --pdf-num 75000 \
  --devdata $dev --devname dev --decodename decode-dev  $train $lang_dir  $tgtexpdir
  echo "## LOG: step15, redo step14 done @ `date`"
fi
