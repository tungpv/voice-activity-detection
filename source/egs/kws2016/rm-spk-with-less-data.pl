#!/usr/bin/perl
use strict;
use warnings;

print STDERR "## LOG: $0 ", join(" ", @ARGV), "\n";

if (@ARGV != 3) {
  die "\nUsage: $0 <threshold (sec)> <segments> <spk2utt>\n";
}

my($thresh, $segments, $utt2spk) = @ARGV;

open(F, "<", "$utt2spk") or die "ERROR: $0, file $utt2spk cannot open\n";
my %vocab = ();
while(<F>) {
  chomp;
  my @A = split(/[\s+]/);
  $vocab{$A[0]} = $A[1];
}
close F;
my %vocab_ = ();
open (F, "<", "$segments") or die "ERROR: $0, file $segments cannot open\n";
while(<F>) {
  chomp;
  my @A = split(/[\s+]/);
  if(not exists $vocab{$A[0]}) {
    die "ERROR: $0, utterance $A[0] does not exist in utt2spk file\n";
  }
  $vocab_{$vocab{$A[0]}} += $A[3] - $A[2];
}
close F;
foreach my $spk (keys %vocab_) {
  if($vocab_{$spk} < $thresh) {
    print STDERR "## WARNING: spk $spk has $vocab_{$spk} seconds of data, removed\n";
    next;
  }
  print $spk, "\n";
}
