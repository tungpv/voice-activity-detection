#!/bin/bash
. path.sh
. cmd.sh 

echo 
echo "$0 $@"
echo
# begin options
cmd=slurm.pl
nj=40
steps=
validating_rate=0.1
first_training_rate=0.4
first_max_iters=8
x_source_nnet_dir=
x_source_data_dir=
delta_order=2
network_type=cnn2d
cnn_proto_opts="--patch-dim1=4  --pitch-dim=3 --splice=10 --pool-step=2"
# cnn2d_proto_opts="--delta-order=0  --cnn1-filt-x-len=1 --cnn1-filt-y-len=40 --pool1-x-len=3 --pool1-x-step=3  --pool1-y-len=1 --pool1-y-step=1 --cnn2-filt-x-len=1 --cnn2-filt-y-len=128"
first_train_tool_opts="--minibatch-size=256 --randomizer-size=32768 --randomizer-seed=777"
second_train_tool_opts="--minibatch-size=256 --randomizer-size=32768 --randomizer-seed=777"

devdata=
graphdir=
decodename=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <data> <lang> <alidir> <dir>
 [options]:
 --cmd                                     # value, "$cmd"
 --nj                                      # value, $nj
 --steps                                   # value, "$steps"
 --validating-rate                         # value, $validating_rate
 --first_training_rate                     # value, $first_training_rate
 --first-max-iters                         # value, $first_max_iters
 --x-source-nnet-dir                       # value, "$x_source_nnet_dir"
 --x-source-data-dir                       # value, "$x_source_data_dir"
 --delta-order                             # value, $delta_order
 --network-type                            # value, "$network_type"
 --cnn-proto-opts                          # value, "$cnn_proto_opts"
 --first-train-tool-opts                   # value, "$first_train_tool_opts"
 --second-train-tool-opts                  # value, "$second_train_tool_opts"
 
 --devdata                                 # value, "$devdata"
 --graphdir                                # value, "$graphdir"
 --decodename                              # value, "$decodename"
 [steps]:
 1: prepare data 
 2: first train use partial data
 5: second train use overall data
 [examples]:
 $0 --steps 1,2,5 \
 --first-training-rate 0.6 \
 --first-max-iters 8 \
 --devdata kws2016/vllp-grapheme/data/dev/fbank_pitch \
 --graphdir kws2016/flp-grapheme/exp/mono/tri4a/graph \
 --decodename decode-dev \
 kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang \
 kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/cnn1d-3pitch
 
 $0 --steps 5 \
 --first-training-rate 0.6 \
 --first-max-iters 8 \
 --x-source-nnet-dir kws2016/flp-grapheme/exp/mono/cnn1d-3pitch/first-train/cnn-dnn \
 --x-source-data-dir kws2016/flp-grapheme/exp/mono/cnn1d-3pitch/second-train \
 --second-train-tool-opts "--minibatch-size=256 --randomizer-size=32768  --randomizer-seed=777 --dropout-retention=0.8" \
 --devdata kws2016/vllp-grapheme/data/dev/fbank_pitch \
 --graphdir kws2016/flp-grapheme/exp/mono/tri4a/graph \
 --decodename decode-dev \
 kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang \
 kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/cnn1d-3pitch-dropout0.8

 $0 --steps 1,2,5 \
 --network-type cnn2d \
 --first-training-rate 0.6 \
 --first-max-iters 8 \
 --delta-order 0 \
 --cnn-proto-opts "--cnn1-filt-x-len=1 --cnn1-filt-y-len=40 --pool1-x-len=3 --pool1-x-step=3  --pool1-y-len=1 --pool1-y-step=1 --cnn2-filt-x-len=1 --cnn2-filt-y-len=128" \
 --devdata kws2016/vllp-grapheme/data/dev/fbank40 \
 --graphdir kws2016/flp-grapheme/exp/mono/tri4a/graph \
 --decodename decode-dev \
 kws2016/flp-grapheme/data/train/fbank40 kws2016/flp-grapheme/data/lang \
 kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/cnn2d-fbank40


END
}

if [ $#  -ne 4 ]; then
  Usage && exit 1
fi
data=$1
lang=$2
alidir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
else
  echo "## WARNING: steps are not specified"
  Usage && exit 0
fi

sec_train_dir=$tgtdir/second-train
train=$sec_train_dir/train
valid=$sec_train_dir/valid
if [ ! -z $step01 ]; then
  echo "## LOG: step01, prepare training data @ `date`"
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $train \
  $data  $valid || exit 1
  echo "## LOG: step01 done @ `date`"
fi
first_train_dir=$tgtdir/first-train
source_nnet_dir=$first_train_dir/cnn-dnn
if [ ! -z $step02 ]; then
  echo "## LOG: first training started @ `hostname`@`date`"
  first_train_data=$first_train_dir/train-overall
  source/egs/swahili/subset_data.sh --subset_time_ratio $first_training_rate \
  --random true \
  $train  $first_train_data || exit 1

  source/egs/kws2016/run-mono-cnn.sh --steps 1,2,3,4 \
  --network-type $network_type \
  --delta-order "--delta-order=$delta_order" \
  --scheduler-opts "--max-iters $first_max_iters" \
  --cnn-proto-opts "$cnn_proto_opts" \
  --train-tool-opts "$first_train_tool_opts" \
  $first_train_data $lang $alidir $first_train_dir
  echo "## LOG: first training done @ `hostname` @ `date`"
fi
if [ ! -z $step05 ]; then
  echo "## LOG: second training started @ `hostname` @ `date`"
  [ ! -z $x_source_nnet_dir ] && source_nnet_dir=$x_source_nnet_dir
  for x in final.feature_transform final.nnet; do
    [ -e $source_nnet_dir/$x ] || { echo "## LOG: file $x expected from source_nnet_dir ($source_nnet_dir)"; exit 1; }
  done 
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 2,3,4 \
  ${x_source_data_dir:+--train-data-dir $x_source_data_dir} \
  ${devdata:+--devdata $devdata}\
  ${graphdir:+--graphdir $graphdir} \
  --train-tool-opts "$second_train_tool_opts" \
  dummy $lang $source_nnet_dir $alidir $sec_train_dir || exit 1
  echo "## LOG: second training done @ `hostname` @ `date`"
fi
