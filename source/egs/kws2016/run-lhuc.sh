#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=
langdir=
dataname=dev
lhuc_id=lhuc
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15"
silence_word_int=0
learn_rate=0.008
lhuc_learn_rate=0.0001
conf_thresh=0
spk_prune_thresh=0
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END

 Usage $(basename $0) [options] <data> <graphdir> <nnetdir> <tgtdir>
 [options]:
 --cmd                               # value, "$cmd"
 --nj                                # value, $nj
 --steps                             # value, "$steps"
 --langdir                           # value, "$langdir"
 --dataname                          # value, "$dataname"
 --lhuc-id                           # value, "$lhuc_id"
 --decode-opts                       # value, "$decode_opts"
 --scoring-opts                      # value, "$scoring_opts"
 --silence-word-int                  # value, "$silence_word_int"
 --learn-rate                        # value, "$learn_rate"
 --lhuc-learn-rate                   # value, "$lhuc_learn_rate"
 --conf-thresh                       # value, "$conf_thresh"
 --spk-prune-thresh                  # value, $spk_prune_thresh
 [step]:
 1: initialize lhuc parameters use utt2spk file
 2: decode data
 3: make utt based ctm 
 4: make unsupervised data
 5: extract fbank-pitch feature
 6: do forced-alignment
 7: prepare data
 8: train learn-hidden-unit-contribution parameters
 9: decode

 20: prepare data to test the potential of LHUC, we divide the training data into 2 parts, one\
     for training, one for testing, with the same speaker set
 21: run dnn training and testing
 22: train LHUC parameters on training speaker data

 [example]:
 $0 --steps 1 kws2016/vllp-grapheme/data/dev/fbank_pitch  kws2016/flp-grapheme/exp/mono/tri4a/graph  \
 kws2016/flp-grapheme/exp/mono/nnet5a \
 kws2016/flp-grapheme/exp/mono/lhuc-test-nnet5a
 
END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

data=$1
graphdir=$2
nnetdir=$3
tgtdir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
else
  echo "## LOG: WARNING, steps EMPTY !" && exit 1
fi
[ -f $nnetdir/final.mdl ] || \
{ echo "## ERROR, final.nnet expected from $nnetdir"; exit 1; }
[ -d $tgtdir ] || mkdir -p $tgtdir
lhuc_dir=$tgtdir/$lhuc_id
[ -d $lhuc_dir ] || mkdir -p $lhuc_dir

function SubUttList {
  if [ $# -ne 3 ]; then
    echo "## ERROR: SubUttList <source-segments> <sub-segments> <remained-uttlist>"
    return 1
  fi
  local segments=$1
  local sub_segments=$2
  local uttlist=$3
  local tgtdir=$(dirname $uttlist)
  [ -d $tgtdir ] || mkdir -p $tgtdir || return 1
  cat $segments |  awk '{print $1;}' | \
  perl -e '($segments) = @ARGV; open(F, "<$segments") or die "file $segments cannot open\n";
    while(<F>) {chomp; @A = split(/[\s+]/); $vocab{$A[0]} ++;} close F;
    while(<STDIN>) {chomp; @A = split(/[\s+]/); if(not exists $vocab{$A[0]}) { print "$A[0]\n";} }
  ' $sub_segments > $uttlist || return 1
  return 0
}

if [ ! -z $step01 ]; then
  echo "## LOG: step01, initialize lhuc parameters @ `date`"
  source/code2/nnet-lhuc-initialize  ark:$data/utt2spk $nnetdir/final.nnet $lhuc_dir/lhuc.init
  echo "## LOG: step01, done @ `date`"
fi
decode_dir=$tgtdir/decode-${dataname}
if [ ! -z $step02 ]; then
  echo "## LOG: step02, decode data $data @ `date`"
  steps/nnet/decode.sh --cmd "$cmd" --nj $nj \
  $decode_opts \
  --scoring-opts "$scoring_opts" \
  --srcdir $nnetdir \
  $graphdir $data $decode_dir || exit 1
  echo "## LOG: step02, done @ `date`"
fi
function get_best_lmwt {
  local decode_dir=$1
  local x=$(source/show-res.sh $decode_dir | head -1 | awk '{print $1;}')
  [ -z $x ] && { echo "ERROR, no best lmwt @get_best_lmwt"; return 1; }
  echo $(echo $x | perl -pe 'if(/score_(\d+)/){$_=$1;}')
  return 0;
}
if [ ! -z $step03 ]; then
  echo "## LOG: step03, make utt based ctm file @ `date`"
  best_lmwt=$(get_best_lmwt $decode_dir)
  [ $? -eq 1 ] && { echo "ERROR, step04, best lmwt not found"; exit 1; }
  source/egs/swahili/lat_to_utt_ctm.sh --cmd "$cmd" \
  --steps 1,2 --silence-word-int $silence_word_int --lmwt $best_lmwt \
  --model $nnetdir/final.mdl $data $graphdir $decode_dir || exit 1
  echo "## LOG: step03, done @ `date`"
fi
function get_ctm_file {
  local decode_dir=$1
  local lmwt=$2
  local ctm_file=$(ls $decode_dir/ctm_${lmwt}/*.utt.ctm)
  [  -z $ctm_file ] && return 1
  echo "$ctm_file" 
  return 0
}
data_id=${dataname}-${conf_thresh}
asr_data=$lhuc_dir/asr-$data_id
if [ ! -z $step04 ]; then
  echo "## LOG: step04, make unsupervised data @ `date`"
  lmwt=$(get_best_lmwt $decode_dir)
  [ $? -eq 1 ] && { echo "ERROR, step04, best lmwt not found"; exit 1; }
  ctm_file=$(get_ctm_file $decode_dir $lmwt)
  [ $? -eq 1 ] && { echo "ERROR, step04, ctm_file expected @get_ctm_file"; exit 1; }
  source/egs/swahili/make_unsupervised_data.sh --steps 1 \
  --conf-thresh $conf_thresh \
  $data  $ctm_file $asr_data
  source/data-len.sh $asr_data
  echo "## LOG: step04, done @ `date`"
fi
fbank_data=$asr_data/fbank-pitch
if [ ! -z $step05 ]; then
  echo "## LOG: step05, fbank+pitch extraction @ `date`"
  source/egs/swahili/make_feats.sh  --cmd "$cmd" --nj $nj \
  --fbank-pitch true $asr_data $fbank_data $asr_data/feat/fbank-pitch
  echo "## LOG: step05, done @ `date`"
fi
asr_alidir=$lhuc_dir/ali-asr-$data_id
if [ ! -z $step06 ]; then
  echo "## LOG: step06, do FA on asr data @ `date`"
  [ -z $langdir ] && { echo "ERROR: step06, langdir expected"; exit 1; }
  steps/nnet/align.sh --cmd "$cmd" --nj $nj \
  $fbank_data $langdir $nnetdir $asr_alidir || exit 1
  echo "## LOG: step06, done @ `date`"
fi
function subset_train_cv {
  if [ $# -lt 4 ]; then
    echo "## ERROR: subset_train_cv <srcdata> <validating-rate> <train_data> <cv_data>"; return 1
  fi
  local srcdata=$1
  local validating_rate=$2
  local train_data=$3
  local cv_data=$4
  if [ $(bc <<< "$validating_rate <= 0 || $validating_rate >= 1") -eq 1 ]; then
    echo "## ERROR: subset_train_cv, illegal validating-rate $validating_rate"; return 1
  fi
  [ -d $train_data ] || mkdir -p $train_data
  [ -d $cv_data ] || mkdir -p $cv_data
  training_rate=$(bc <<< "1-$validating_rate")
  perl -e '($spk2utt, $trn_rate, $trn_utt2spk, $cv_utt2spk) = @ARGV; 
    open(F, "<", "$spk2utt") or die "## ERROR, subset_train_cv, perl, cannot open source $spk2utt\n";
    open(TRN, ">", "$trn_utt2spk" ) or die "## ERROR, subset_train_cv, perl, cannot open $trn_utt2spk\n";
    open(CV, ">", "$cv_utt2spk") or die "## ERROR, subset_train_cv, perl, cannot open $cv_utt2spk\n";
    while(<F>) {
      chomp;
      @A = split(/[\s+]/);
      $num = (scalar @A - 1) * $trn_rate;
      $num = 1 if $num < 1;
      $spk = shift @A;
      for($i = 0; $i < $num; $i++) {
        print TRN "$A[$i] $spk\n";
      }
      for($i = $num; $i < @A; $i++) {
        print CV "$A[$i] $spk\n";
      }
    }  close F;
    close TRN; close CV;
  ' $srcdata/spk2utt $training_rate  $train_data/.utt2spk $cv_data/.utt2spk || return 1
  utils/subset_data_dir.sh  --utt-list $train_data/.utt2spk  $srcdata $train_data
  utils/subset_data_dir.sh --utt-list $cv_data/.utt2spk $srcdata $cv_data
  return 0
}

trn_data=$lhuc_dir/train-data
cv_data=$lhuc_dir/cv-data
if [ ! -z $step07 ]; then
  echo "## LOG: step07, prepare training data @ `date`"
  pruned_data=$fbank_data
  if [ $(bc <<< "$spk_prune_thresh > 0") -eq 1 ]; then
     pruned_data=$lhuc_dir/pruned${spk_prune_thresh}
     [ -d $pruned_data ] || mkdir -p $pruned_data
     source/egs/kws2016/rm-spk-with-less-data.pl $spk_prune_thresh $fbank_data/segments \
     $fbank_data/utt2spk > $pruned_data/.spklist
     utils/subset_data_dir.sh --spk-list $pruned_data/.spklist $fbank_data $pruned_data
  fi
  subset_train_cv $pruned_data 0.1 $trn_data $cv_data
  echo "## LOG: step07, done @ `date`"
fi
function train_nnet {
  [ $# -eq 5 ] || { echo "## ERROR: train_nnet parameter error, $@"; return 1; }
  local lhuc_opts_csl=$1; shift
  local trn_data=$1
  local cv_data=$2
  local alidir=$3
  local ntgtdir=$4
  steps/nnet/train.sh  \
  --copy-feats false \
  --train-tool "source/code2/nnet-train-perutt" \
  --schedule-cmd "steps/nnet/train_scheduler-lhuc.sh" \
  --feature-transform $nnetdir/final.feature_transform \
  --nnet-init $nnetdir/final.nnet \
  --learn-hid-unit-contri-opts "$lhuc_opts_csl" \
  --learn-rate $learn_rate \
  $trn_data $cv_data $langdir $alidir \
  $alidir $ntgtdir || return 1; 
  return 0
}
function TrainLhucFactor {
  [ $# -eq 6 ] || { echo "## ERROR: train_nnet_lhuc_factor error, $@"; return 1; }
  local lhuc_opts_csl=$1; shift
  local trn_data=$1
  local cv_data=$2
  local alidir=$3
  local snnetdir=$4
  local ntgtdir=$5
  if [ ! -f $snnetdir/final.feature_transform ] -or \
     [ ! -f $snnetdir/final.nnet ]; then
    echo "## ERROR: finale.feature_transform or final.nnet expected"
    return 1
  fi
  steps/nnet/train.sh  \
  --copy-feats false \
  --train-tool "source/code2/nnet-train-perutt" \
  --schedule-cmd "steps/nnet/train_scheduler-lhuc.sh" \
  --feature-transform $snnetdir/final.feature_transform \
  --nnet-init $snnetdir/final.nnet \
  --learn-hid-unit-contri-opts "$lhuc_opts_csl" \
  --learn-rate $lhuc_learn_rate \
  $trn_data $cv_data $langdir $alidir \
  $alidir $ntgtdir || return 1; 
  return 0
}

if [ ! -z $step08 ]; then
  echo "## LOG: step08, train LHUC started @ `date`"
  [ -z $langdir ] && { echo "ERROR: step08, langdir expected"; exit 1; }
  lhuc_opts_csl="ark:$fbank_data/utt2spk;$lhuc_dir/lhuc.init"
  train_nnet "$lhuc_opts_csl" $trn_data $cv_data $asr_alidir $lhuc_dir
  [ $? -eq 0 ] || { echo "## ERROR: step08, train_nnet failed"; exit 1; }
  echo "## LOG: step08, done @ `date`"
fi
function nnet_lhuc_decode {
  local lhuc_opts_csl=$1
  local tgt_decode_dir=$2
  steps/nnet/decode.sh --cmd "$cmd" --nj $nj \
  --nnet-forward "source/code2/nnet-forward" \
  --lhuc-opts-csl "$lhuc_opts_csl" \
  $decode_opts \
  --scoring-opts "$scoring_opts" \
  --srcdir $nnetdir \
  $graphdir $data $tgt_decode_dir || return 1
  return 0
}
if [ ! -z $step09 ]; then
  echo "## LOG: step09, LHUC decode started @ `date`"
  [ -f $lhuc_dir/final.lhuc ] || \
  { echo "ERROR, final.lhuc expected"; exit 1; }
  decode_dir=$lhuc_dir/decode-${dataname}
  lhuc_opts_csl="ark:$data/utt2spk;$lhuc_dir/final.lhuc"
  nnet_lhuc_decode "$lhuc_opts_csl" $decode_dir
  [ $? -eq 0 ] || { echo "## ERROR: step09, decoding error"; exit 1; }
  echo "## LOG: step09, ended @ `date`"
fi
ref_alidir=$tgtdir/data-source/ali-ref-${dataname}
ref_data=$tgtdir/data-source/ref-${dataname}
if [ ! -z $step10 ]; then
  echo "## LOG: step10, do FA on ground-truth data @ `date`"
  [ -d $ref_data ] || mkdir -p $ref_data
  cp $data/* $ref_data/
  [ -z $langdir ] && { echo "ERROR: step10, langdir expected"; exit 1; }
  steps/nnet/align.sh --cmd "$cmd" --nj $nj \
  $ref_data $langdir $nnetdir $ref_alidir || exit 1
  echo "## LOG: step10, done @ `date`"
fi
train_ref=$tgtdir/data-source/train-ref
cv_ref=$tgtdir/data-source/cv-ref
if [ ! -z $step11 ]; then
  echo "## LOG: step11, prepare training data (reference) @ `date`"
  subdir=$tgtdir/data-source/ref-sub40
  utils/subset_data_dir.sh  --per-spk $ref_data 40  $subdir
  subset_train_cv $subdir 0.875 $train_ref $cv_ref 
  [ $? -eq 0 ] || { echo "## ERROR: step11, subset_train_cv error"; exit 1; }  
  echo "## LOG: step11, done @ `date`"
fi
if [ ! -z $step12 ]; then
  echo "## LOG: step12, training lhuc (reference) started @ `date`"
  lhuc_opts_csl="ark:$data/utt2spk;$lhuc_dir/lhuc.init"
  train_nnet "$lhuc_opts_csl" $train_ref $cv_ref $ref_alidir $lhuc_dir
  [ $? -eq 0 ] || { echo "## ERROR: step12, train_nnet failed"; exit 1; }
  echo "## LOG: step12, ended @ `date`"
fi
trn_data=$lhuc_dir/train-data
cv_data=$lhuc_dir/cv-data
if [ ! -z $step14 ]; then
  echo "## LOG: step14, prepare data @ `date`"
  subset_train_cv $fbank_data 0.1 $trn_data $cv_data
  echo "## LOG: step14, done @ `date`"
fi
if [ ! -z $step15 ]; then
  echo "## LOG: step15, training started @ `date`"
  lhuc_opts_csl="ark:$fbank_data/utt2spk;$lhuc_dir/lhuc.init"
  train_nnet "$lhuc_opts_csl" $trn_data $cv_data $asr_alidir $lhuc_dir
  [ $? -eq 0 ] || { echo "## ERROR: step08, train_nnet failed"; exit 1; }
  echo "## LOG: step15, done @ `date`"
fi
if [ ! -z $step13 ]; then
  echo "## LOG: step13, decoding started @ `date`"
  lhuc_opts_csl="ark:$data/utt2spk;$lhuc_dir/final.lhuc"
  decode_dir=$lhuc_dir/decode-${dataname}
  nnet_lhuc_decode "$lhuc_opts_csl" $decode_dir
  [ $? -eq 0 ] || { echo "## ERROR: step13, decoding failed "; exit 1; }
  echo "## LOG: step13, ended @ `date`"
fi

if [ ! -z $step20 ]; then
  echo "## LOG: $0, step20, started @ `date`"
  
  utils/subset_data_dir.sh  --per-spk  kws2016/flp-grapheme/data/train/fbank_pitch 10  kws2016/flp-grapheme/data/train-u10-test/fbank-pitch
  local/prepare_stm.pl  kws2016/flp-grapheme/data/train-u10-test/fbank-pitch
  SubUttList  kws2016/flp-grapheme/data/train/fbank_pitch/segments \
              kws2016/flp-grapheme/data/train-u10-test/fbank-pitch/segments \
              kws2016/flp-grapheme/data/train-sub/fbank_pitch/.uttlist
  [ $? -eq 0 ] || { echo "## ERROR, $0, step20, SubUttList error"; exit 1; }
  utils/subset_data_dir.sh --utt-list kws2016/flp-grapheme/data/train-sub/fbank_pitch/.uttlist \
                           kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/train-sub/fbank_pitch || exit 1
  echo "## LOG: $0, step20, ended @ `date`"
fi

if [ ! -z $step21 ]; then
  echo "## LOG: step21, run dnn training and testing @ `date`"
  source/egs/kws2016/run-nnet.sh --steps 1,2,3,4 \
  --validating-rate 0.1\
  --learn-rate 0.008 \
  --cmvn-opts "--norm-means=true" \
  --delta-opts "--delta-order=2" \
  --nnet-train-cmd "steps/nnet/train.sh"\
  --nnet-pretrain-cmd  "steps/nnet/pretrain_dbn.sh --feat-type traps  --copy_feats_tmproot /local/hhx502 --splice 10 --nn-depth 5 --hid-dim 2048" \
  --nnet-id "nnet5a-sub" \
  --devdata $data \
  --graphdir kws2016/flp-grapheme/exp/mono/tri4a/graph  \
  --decode-opts "$decode_opts" \
   --scoring-opts "$scoring_opts" \
  kws2016/flp-grapheme/data/train-sub/fbank_pitch  $langdir \
  kws2016/flp-grapheme/exp/mono/tri4a/ali_train  $tgtdir || exit 1
  echo "## LOG: step21, done @ `date`"
fi
alidir=$lhuc_dir/ali_train-sub_nnet5a-sub
if [ ! -z $step22 ]; then
  echo "## LOG: step22, do FA @ `date`"
  steps/nnet/align.sh --cmd "$cmd" --nj $nj \
  kws2016/flp-grapheme/data/train-sub/fbank_pitch  $langdir $tgtdir/nnet5a-sub $alidir || exit 1
  echo "## LOG: step22, done @ `date`"
fi
if [ ! -z $step23 ]; then
  echo "## LOG: step23, train lhuc @ `date`"
  source/code2/nnet-lhuc-initialize  ark:kws2016/flp-grapheme/data/train-sub/fbank_pitch/utt2spk \
  $tgtdir/nnet5a-sub/final.nnet $lhuc_dir/lhuc.init
  train_data=$lhuc_dir/train-data
  cv_data=$lhuc_dir/cv-data
  subset_train_cv  kws2016/flp-grapheme/data/train-sub/fbank_pitch 0.1 $lhuc_dir/train-data $lhuc_dir/cv-data
  [ $? -eq 0 ] || { echo "## ERROR: step23, subset_train_cv failed"; exit 1; }
  
  lhuc_opts_csl="ark:kws2016/flp-grapheme/data/train-sub/fbank_pitch/utt2spk;$lhuc_dir/lhuc.init"
  TrainLhucFactor  "$lhuc_opts_csl" $train_data $cv_data $alidir $tgtdir/nnet5a-sub $lhuc_dir || exit 1
  [ $? -eq 0 ] || { echo "## ERROR: step23, TrainLhucFactor failed"; exit 1; }
  echo "## LOG: step23, done @ `date`"
fi
