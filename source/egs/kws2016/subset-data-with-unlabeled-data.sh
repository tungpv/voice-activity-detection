#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=slurm.pl
nj=40
steps=
# end options

. parse_options.sh

function Usage {
 cat<<END
 Usage: $0 [options] <utt-ctm> <source-data> <speaker-nnetdir>  <target-data>
 [options]:
 --cmd                            # value, "$cmd"
 --nj                             # value, $nj
 --steps                          # value, "$steps"
 [steps]:
 1: make asr labelled data as vad data
 2: fbank_pitch feature extraction   
 3: label speaker
 4: make new data (update utt2spk and spk2utt)
 5: extract fbank_pitch feature for updated feature
 6: decode with supervised lhuc
 7: make dev data without speaker information (utt2spk --> utt2utt)

 [example]:
 
 $0 --steps 1 kws2016/flp-grapheme/exp/mono/smbr-nnet5a-sub/decode_dev/score_13/fbank_pitch.utt.ctm  kws2016/vllp-grapheme/data/dev/fbank_pitch kws2016/flp-grapheme/exp/mono/utt2spk-with-nnet/spk-nnet  kws2016/flp-grapheme/exp/mono/add-spk2utt-test
 
 $0 --steps 1,2 kws2016/flp-grapheme/exp/mono/smbr-nnet5a-sub/decode-dev-no-spk/score_13/fbank-pitch.utt.ctm kws2016/flp-grapheme/exp/mono/add-spk2utt-test/no_spk_data/fbank-pitch   kws2016/flp-grapheme/exp/mono/utt2spk-with-nnet/spk-nnet  kws2016/flp-grapheme/exp/mono/add-spk2utt-test-real
 
END
}

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
else
  Usage && exit 1
fi

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

uttctm=$1
sdata=$2
spk_nnetdir=$3
tgtdir=$4

[ -d $tgtdir ] || mkdir -p $tgtdir

asrlabel=$tgtdir/asr-label
if [ ! -z $step01 ]; then
  echo "## LOG: step01, make unsupervised data @ `date`"
  source/egs/swahili/make_unsupervised_data.sh --steps 1 \
  $sdata $uttctm  $asrlabel
  echo "## LOG: step01, done @ `date`"
fi
if [ ! -z $step02 ]; then
  echo "## LOG: step02, fbank_pitch feature @ `date`"
  sdata01=$asrlabel
  source/egs/swahili/make_feats.sh  --fbank-pitch true \
  $sdata01 $sdata01/fbank-pitch $sdata01/feat/fbank-pitch || exit 1
  echo "## LOG: step02, done @ `date`"
fi
labelled_utt2spk=$tgtdir/dnn-labelled-utt2spk
if [ ! -z $step03 ]; then
  echo "## LOG:  step03, label speaker @ `date`"
  source/egs/kws2016/decode-speaker.sh --cmd "$cmd" --nj $nj \
   $asrlabel/fbank-pitch $spk_nnetdir $labelled_utt2spk 
  echo "## LOG: step03, done @ `date`"
fi
new_data=$tgtdir/updated-data
if [ ! -z $step04 ]; then
  echo "## LOG: step04, make new data @ `date`"
  [ -d $new_data ] || mkdir -p $new_data
  cat $labelled_utt2spk/score.* | \
  awk '{print $1;}' > $new_data/uttlist
  utils/subset_data_dir.sh  --utt-list $new_data/uttlist $sdata $new_data
  rm $new_data/{utt2spk,spk2utt,feats.scp,cmvn.scp}
  cat $labelled_utt2spk/score.* | \
  awk '{printf("%s %s\n", $1, $2);}' | sort -u  >$new_data/.old-utt2spk
  mv $new_data/segments $new_data/.old-segments
  cat $new_data/.old-segments | \
  perl -e '($utt2spk) = @ARGV; open(F, "<", "$utt2spk") or die "File $utt2spk cannot open\n";
    while(<F>) {chomp; @A = split(/\s+/); $vocab{$A[0]} = $A[1];} close F;
    while(<STDIN>) {chomp; @A = split(/\s+/); $old_utt = $A[0]; if (not exists $vocab{$old_utt}) {die "## ERROR: $old_utt does not exist\n";}
      $spk = $vocab{$old_utt}; $new_utt = $spk . "-" . $old_utt;  $A[0] = $new_utt;  print join(" ", @A), "\n";
    }
  ' $new_data/.old-utt2spk > $new_data/segments
  mv $new_data/text $new_data/.old-text
  cat $new_data/.old-text | \
  perl -e '($utt2spk) = @ARGV; open(F, "<", "$utt2spk") or die "File $utt2spk cannot open\n";
    while(<F>) {chomp; @A = split(/\s+/); $vocab{$A[0]} = $A[1];} close F;
    while(<STDIN>) {chomp;  m/(\S+)\s+(.*)/ ; $old_utt = $1; $text = $2;  if (not exists $vocab{$old_utt}) {die "## ERROR: utt $old_utt in text does not exist\n";}
      $spk = $vocab{$old_utt}; $new_utt = $spk . "-" . $old_utt;   print join(" ", ($new_utt, $text)), "\n";
    }
  ' $new_data/.old-utt2spk  > $new_data/text
  cat $new_data/.old-utt2spk | \
  perl -pe 'chomp; @A = split(/\s+/); $_ = $A[1] . "-" . $A[0] . " $A[1]" . "\n";' > $new_data/utt2spk
  utils/utt2spk_to_spk2utt.pl < $new_data/utt2spk > $new_data/spk2utt
  utils/fix_data_dir.sh $new_data
  echo "## LOG: step04, done @ `date`"
fi

if [ ! -z $step05 ]; then
  echo "## LOG: step05, fbank_feature @ `date`"
  sdata01=$new_data
  source/egs/swahili/make_feats.sh  --fbank-pitch true \
  $sdata01 $sdata01/fbank-pitch $sdata01/feat/fbank-pitch || exit 1
  echo "## LOG: step05, done @ `date`"
fi
if [ ! -z $step06 ]; then
  echo "## LOG: step06, decode @ `date`"
  steps/nnet/decode.sh --cmd slurm.pl --nj $nj --scoring-opts "--min-lmwt 8 --max-lmwt 15" --nnet-forward source/code2.1/nnet-forward \
  --lhuc-opts-csl "ark:$new_data/fbank-pitch/utt2spk;kws2016/flp-grapheme/exp/mono/smbr-nnet5a-sub/7.lhuc" \
  --acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 kws2016/flp-grapheme/exp/mono/tri4a/graph \
  $new_data/fbank-pitch kws2016/flp-grapheme/exp/mono/smbr-nnet5a-sub/decode-dev-lhuc-label-spk-real 
  echo "## LOG: step06, done @ `date`"
fi
no_spk_data=$tgtdir/no_spk_data
if [ ! -z $step07 ]; then
  echo "## LOG: step07, copy data and remove speaker info @ `date`"
  utils/subset_data_dir.sh  --utt-list $new_data/uttlist $sdata $no_spk_data
  rm $no_spk_data/{utt2spk,spk2utt,feats.scp,cmvn.scp}
  cat $sdata/segments | awk '{printf("%s %s\n", $1, $1);}' > $no_spk_data/utt2spk
  cp $no_spk_data/utt2spk $no_spk_data/spk2utt
  utils/fix_data_dir.sh $no_spk_data
  sdata01=$no_spk_data
  source/egs/swahili/make_feats.sh  --fbank-pitch true \
  $sdata01 $sdata01/fbank-pitch $sdata01/feat/fbank-pitch || exit 1
  echo "## LOG: step07, done @ `date`"
fi

if [ ! -z $step08 ]; then
  echo "## LOG: step08, decode data (no_spk_data) @ `date`"
  steps/nnet/decode.sh --cmd slurm.pl --nj $nj --scoring-opts "--min-lmwt 8 --max-lmwt 15" \
  --acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 kws2016/flp-grapheme/exp/mono/tri4a/graph \
  $no_spk_data/fbank-pitch kws2016/flp-grapheme/exp/mono/smbr-nnet5a-sub/decode-dev-no-spk 
  echo "## LOG: step08, done @ `date`"
fi
