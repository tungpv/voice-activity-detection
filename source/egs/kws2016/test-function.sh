#!/bin/bash

function set_variable {
  local x_dir=$1
  local x_dim=$2
  x_csl=$x_dir:$x_dim
}

set_variable /my/work/w1  50
echo x_csl=$x_csl
set_variable /my/work/w2  100
echo x_csl=$x_csl

echo x_dir=$x_dir
