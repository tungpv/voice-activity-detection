#!/bin/bash
. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=
learn_rate=0.008
validating_rate=0.1
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage $(basename $0) [options] <data> <lang> <alidir> <nnetdir> <tgtdir>
 [options]:
 --cmd                                 # value, "$cmd"
 --nj                                  # value, $nj
 --steps                               # value, "$steps"
 --learn-rate                          # value, $learn_rate
 --validating-rate                     # value, $validating_rate
 [step]:
 1: convert ali to ctm file
 2: remove non-content words from ctm file
 3: make fbank+pitch features for the filtered data
 4: prepare data to train dnn

 [example]:
 $0 --steps 1,2 --learn-rate 0.008 kws2016/flp-grapheme/data/train-sub/fbank_pitch kws2016/flp-grapheme/data/lang  kws2016/flp-grapheme/exp/mono/lhuc-train-sub/ali_train-sub_nnet5a-sub kws2016/flp-grapheme/exp/mono/nnet5a-sub    kws2016/flp-grapheme/exp/mono/utt2spk-with-nnet

END
}

if [ $# -ne 5 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
alidir=$3
nnetdir=$4
tgtdir=$5

if [ ! -z "$steps" ]; then
  old_steps=$steps
  steps=$(echo -n "$steps"| perl -pe '@A =split(/[,:]/); $s = ""; for($i = 0; $i < @A; ++$i){ $g=$A[$i]; if($g =~ /[\-]/) { @A_ = split(/[\-]/, $g);   die if (scalar @A_ != 2 || $A_[0] >= $A_[1]); $g_ = ""; for($j=$A_[0]; $j <= $A_[1]; $j++){ $g_ .= "$j,"; } $s .= "$g_";  }else { $s .= "$A[$i]," } } $s =~ s/,$//; $_ = $s;') || { echo "## ERROR: illegal $old_steps"; exit 1; }
  echo "steps=$steps"
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
else
  echo "## LOG: WARNING, no step is specified, nothing to do !" && exit 1
fi
[ -d $tgtdir ] || mkdir -p $tgtdir

if [ ! -z $step01 ]; then
  echo "## LOG: step01, convert ali to ctm file @ `date`"
  steps/get_train_ctm.sh --cmd "$cmd"  $data $lang  $alidir  || exit 1
  echo "## LOG: step01, done @ `date`"
fi
spk_data=$tgtdir/spk-data
if [ ! -z $step02 ]; then
  echo "## LOG: step02, remove those non-conent words @ `date`"
  source/egs/kws2016/filter-and-reseg-data.sh  --steps 1 $data $alidir/utt_ctm $spk_data || exit 1
  echo "## LOG: step02, done @ `date`"
fi
train_overall_data=$spk_data/fbank-pitch
if [ ! -z $step03 ]; then
  echo "## LOG: step03, fbank-pitch extraction @ `date`"
  source/egs/swahili/make_feats.sh --cmd slurm.pl --nj $nj  --fbank-pitch true $spk_data $spk_data/fbank-pitch $spk_data/feat/fbank-pitch 
  cat $spk_data/spk2utt | awk '{print $1, " ", NR-1;}' > $spk_data/fbank-pitch/spk2target_index
  echo "## LOG: step03, done @ `date`"
fi
train_label=$spk_data/utt-spk-label
if [ ! -z $step04 ]; then
  echo "## LOG: step04, make posterior target @ `date` "
  featlen="ark:feat-to-len 'scp:cat $spk_data/fbank-pitch/feats.scp|' ark,t:- |" # get number of frames for every utterance,
  source/code2/make-speaker-label-target ark:$spk_data/fbank-pitch/utt2spk ark:$spk_data/spk2target_index "$featlen" ark:$spk_data/utt-spk-label  || exit 1  
  echo "## LOG: step04, done @ `date`"
fi
spk_nnet=$tgtdir/spk-nnet
if [ ! -z $step05 ]; then
  echo "## LOG: step05, make task-dependent softmax layer @ `date`"
  front_nnet="nnet-copy --remove-last-layers=2 $nnetdir/final.nnet -|"
  hid_dim=$(nnet-info "$front_nnet" | grep output-dim |tail -1 | perl -pe 's/.*output-dim//g; m/(\d+)/g; $_=$1;') \
  || { echo "## ERROR: step05, failed to get hid_dim"; exit 1; }
 num_tgt=$(cat $spk_data/fbank-pitch/spk2target_index |perl -e '$num_target = 0; while(<STDIN>){chomp; @A =split(/\s+/); if(@A == 2){ if($A[1] > $num_target){$num_target = $A[1];}} } print $num_target + 1; ' )
  [ $num_tgt -gt 0 ] || { echo "## ERROR: step05, number of target is zero"; exit 1; }
  utils/nnet/make_nnet_proto.py $hid_dim $num_tgt 0  $hid_dim >$spk_nnet/softmax.proto || exit 1 
  log=$spk_nnet/softmax-nnet-initialize.log
  nnet-initialize $spk_nnet/softmax.proto $spk_nnet/softmax-nnet.init \
  2>$log || { cat $log; exit 1; }
  nnet_init=$spk_nnet/nnet.init
  nnet-concat "$front_nnet" $spk_nnet/softmax-nnet.init $nnet_init 2> $spk_nnet/nnet-init.log
  echo "## LOG: step05, done @ `date`"
fi 
train_data=$tgtdir/train-data
cv_data=$tgtdir/cv-data
if [ ! -z $step06 ]; then
  echo "## LOG: step06, prepare training data @ `date`"
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $train_data \
  $spk_data/fbank-pitch  $cv_data || exit 1
  echo "## LOG: step06, done @ `date`"
fi
if [ ! -z $step07 ]; then
  echo "## LOG: step07, training spk network started @ `date`"
  steps/nnet/train.sh --learn-rate $learn_rate \
  --feature-transform $nnetdir/final.feature_transform \
  --labels "ark:$train_label" \
  --hid-layers 0 --nnet-init $spk_nnet/nnet.init $train_data $cv_data $lang dummy \
  dummy $spk_nnet || exit 1
  cp $spk_data/fbank-pitch/spk2target_index $spk_nnet/spk2index
  echo "## LOG: step07, done @ `date`"
fi
