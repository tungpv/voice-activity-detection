#!/bin/bash

. path.sh
. cmd.sh 

echo 
echo "## LOG: $0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=
validating_rate=0.1
learn_rate=0.008
splice=10
cnn_hid_layers=1
scheduler_opts="--max-iters 8"
delta_order="--delta-order=2"
network_type=cnn2d
cnn_proto_opts="--patch-dim1=4  --pitch-dim=3 --splice=10 --pool-step=2" 
pretrain_cmd="steps/nnet/pretrain_dbn.sh --copy-feats-tmproot /local/hhx502 --nn-depth 4 --hid-dim 2048 --rbm-iter 1"
train_tool_opts="--minibatch-size=1024 --randomizer-size=32768 --randomizer-seed=777"
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <data> <lang> <alidir> <dir>
 [options]:
 --cmd                                     # value, "$cmd"
 --nj                                      # value, $nj
 --steps                                   # value, "$steps"
 --validating-rate                         # value, $validating_rate
 --splice                                  # vlaue, $splice
 --cnn-hid-layers                          # value, $cnn_hid_layers
 --delta-order                             # value, "$delta_order"
 --network-type                            # value, "$network_type"
 --scheduler-opts                          # value, "$scheduler_opts"
 --cnn-proto-opts                          # value, "$cnn_proto_opts"
 --pretrain-cmd                            # value, "$pretrain_cmd"
 --train-tool-opts                         # value, "$train_tool_opts"
 [steps]:
 1: prepare training & cross-validating data
 2: train cnn 
 3: pretrain taking cnn as feature transform
 4: train cnn-dnn with xe criterion
 [example]:
 $0 --steps 1,2,3,4 --delta-order "$delta_order" --scheduler-opts "$scheduler_opts" \
 --cnn-proto-opts "$cnn_proto_opts" --train-tool-opts "$train_tool_opts" \
 kws2016/flp-grapheme/data/train/fbank_pitch kws2016/flp-grapheme/data/lang \
 kws2016/flp-grapheme/exp/mono/tri4a/ali_train kws2016/flp-grapheme/exp/mono/cnn1d-3pitch

END
}
if [ $#  -ne 4 ]; then
  Usage && exit 1
fi
data=$1
lang=$2
alidir=$3
dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
else
  echo "## WARNING: steps are not specified"
  Usage && exit 0
fi
train=$dir/train
valid=$dir/valid
if [ ! -z $step01 ]; then
  echo "## LOG: step01, prepare training data @ `date`"
  source/egs/swahili/subset_data.sh --subset_time_ratio $validating_rate \
  --random true \
  --data2 $train \
  $data  $valid || exit 1
  echo "## LOG: step01 done @ `date`"
fi
cnn_dir=$dir/cnn
if [ ! -z $step02 ]; then
  echo "## LOG: step02, train cnn @ `date`"
  cnn_proto_opts="$cnn_proto_opts  $delta_order"
  steps/nnet/train.sh \
  --copy-feats false \
  --cmvn-opts "--norm-means=true --norm-vars=true" \
  --scheduler-opts "$scheduler_opts" \
  --delta-opts "$delta_order" --splice $splice \
  --network-type "$network_type" --cnn-proto-opts "$cnn_proto_opts" \
  --hid-layers $cnn_hid_layers --learn-rate $learn_rate \
  $train $valid $lang $alidir $alidir $cnn_dir || exit 1;
  
  nnet-concat $cnn_dir/final.feature_transform \
  "nnet-copy --remove-last-layers=$(((cnn_hid_layers+1)*2)) $cnn_dir/final.nnet - |" \
  $cnn_dir/final.feature_transform_cnn

  echo "## LOG: step02 ended @ `date`"
fi
pretrain_dir=$dir/pretrain_dbn
transf_cnn=$cnn_dir/final.feature_transform_cnn
nn_depth=$(echo "$pretrain_cmd"| perl -pe 'if(/--nn-depth\s+(\d+)\s+.*/){$_=$1;}else{$_="";}')
if [ ! -z $step03 ]; then
  echo "## LOG: step03, pretraining @ `date`"
  $pretrain_cmd \
  --feature-transform $transf_cnn --input-vis-type bern \
  --param-stddev-first 0.05 --param-stddev 0.05 \
  $train $pretrain_dir || exit 1
  echo "## LOG: step03, done @ `date`"
fi
cnn_dnn_dir=$dir/cnn-dnn
if [ ! -z $step04 ]; then
  echo "## LOG: step04, train cnn-dnn @ `date`"
  [ -d $cnn_dnn_dir/log ] || mkdir -p $cnn_dnn_dir/log
  feature_transform=$cnn_dir/final.feature_transform
  feature_transform_dbn=$pretrain_dir/final.feature_transform
  dbn=$pretrain_dir/$nn_depth.dbn
  cnn_dbn=$cnn_dnn_dir/cnn_dbn.net
  
  num_components=$(nnet-info $feature_transform | grep -m1 num-components | awk '{print $2;}')
  cnn="nnet-copy --remove-first-layers=$num_components $feature_transform_dbn - |"
  nnet-concat "$cnn" $dbn $cnn_dbn 2>$cnn_dnn_dir/log/concat_cnn_dbn.log || exit 1 

  steps/nnet/train.sh --feature-transform $feature_transform --dbn $cnn_dbn --hid-layers 0 \
  --scheduler-opts "$scheduler_opts" \
  --train-tool-opts "$train_tool_opts" \
  $train $valid $lang $alidir $alidir $cnn_dnn_dir || exit 1;
  echo "## LOG: step04, ended @ `date`"
fi
