#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=40
cmd=slurm.pl
train_cmd="slurm.pl --gres=gpu:1"
steps=
gmm_like_cmd=
acwt=0.0909
conf=conf/decode_dnn.config
train_ivector_data=
append_vector_to_feats=append-vector-to-feats
append_dec_ivector=
denLatId=
devdata=
trainname=train
graphdir=
decodename=decode_dev
decode_cmd=steps/nnet/decode.sh
decode_opts="--acwt 0.1 --beam 10 --lattice-beam 8 --max-mem 500000000 --skip-scoring false"
scoring_opts="--min-lmwt 8 --max-lmwt 15"
# end options

echo
echo LOG: $0 $@
echo

. parse_options.sh || exit 1

function Usage {
  cat <<END

 $(basename $0) [options] <data> <lang> <srcdir> <dir>
 [options]:
 --nj                           # value, $nj
 --cmd                          # value, "$cmd"
 --train-cmd                    # value, "$train_cmd"
 --steps                        # value, "$steps"

 --gmm-like-cmd                 # value, "$gmm_like_cmd"
 --acwt                         # value, $acwt
 --conf                         # value, "$conf"
 --train-ivector-data           # value, "$train_ivector_data"
 --append-vector-to-feats       # value, "$append_vector_to_feats"
 --append-dec-ivector           # value, "$append_dec_ivector"
 --denLatId                     # value, "$denLatId"
 --devdata                      # value, "$devdata"
 --trainname                    # value, "$trainname"
 --graphdir                     # value, "$graphdir"
 --decodename                   # value, "$decodename"
 --decode-cmd                   # value, "$decode_cmd"
 --decode-opts                  # value, "$decode_opts"
 --scoring-opts                 # value, "$scoring_opts"
 
 [steps]:
 1: make alignment  
 2: make lattice 
 3: sequential training
 4: test dev data, if any

END
}

if [ $# -ne 4 ]; then
  Usage && exit 1
fi

data=$1
lang=$2
srcdir=$3
dir=$4

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
use_gmm_likes=
if [ ! -z "$gmm_like_cmd" ]; then
  use_gmm_likes=true
fi
alidir=$srcdir/ali_${trainname}
if [ ! -z $step01 ]; then
  echo "making alignment started @ `date`"
  steps/nnet/align.sh --cmd "$cmd" --nj $nj \
  ${train_ivector_data:+--ivector $train_ivector_data} \
  --append-vector-to-feats "$append_vector_to_feats" \
  $data $lang $srcdir $srcdir/ali_${trainname} || exit 1
  echo "ended @ `date`"
fi
latdir=$srcdir/denlat_${trainname}${denLatId}
if [ ! -z $step02 ]; then
  echo "making lattice started @ `date`"
  steps/nnet/make_denlats.sh --cmd "$cmd" --nj $nj \
  ${train_ivector_data:+--ivector $train_ivector_data} \
  --append-vector-to-feats "$append_vector_to_feats" \
  --acwt $acwt --config $conf $data $lang $srcdir $latdir || exit 1 
  echo "ended @ `date`"
fi

if [ ! -z $step03 ]; then
  echo "sequential training started @ `date`"
  steps/nnet/train_mpe.sh --cmd "$train_cmd"  --acwt $acwt --do-smbr true \
  ${train_ivector_data:+--ivector $train_ivector_data} \
  --append-vector-to-feats "$append_vector_to_feats" \
  $data $lang $srcdir $alidir $latdir $dir || exit 1
  echo "ended @ `date`"
fi

if [ ! -z $step04 ]; then
  echo "Decoding started @ `date`"
  effect_nj=$(wc -l < $devdata/spk2utt)
  [ $effect_nj -gt $nj ] && effect_nj=$nj
  [ -f $graphdir/HCLG.fst ] || \
  { echo "ERROR, HCLG.fst expected from $graphdir"; exit 1; }
  $decode_cmd ${append_dec_ivector:+--append-vector-to-feats "$append_dec_ivector"} --cmd "$cmd" --nj $effect_nj \
  ${use_gmm_likes:+--gmm-like-cmd "$gmm_like_cmd"} \
  --scoring-opts "$scoring_opts" \
  $decode_opts $graphdir $devdata $dir/$decodename || exit 1
fi


echo "Done !"
