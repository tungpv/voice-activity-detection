#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=run.pl
nj=20
steps=
check_xml=false
prepare_data=false
prepare_data_opts=" <filelist/audio> <wavdir> <xmldir> <mer> <useall/false> <datadir>"
train_lm=false
train_lm_opts=""
normalize_dev_data=false
normalize_dev_data_opts="<sdata> <data>"
make_utt2mer=false
make_utt2mer_opts="<filelist> <xmldir> <dir>"
prepare_lsdecode_cs_data=false
make_lsdecode_data=false
lsdecode_data_opts="<filelist> <wavdir> <xmldir> <dir>"
subset_data_with_mer=false
subset_mer_opts="<lower_mer> <upper_mer> <utt2mer> <sdata> <data>"
make_word_human_stm=false
make_word_human_stm_opts="data lang alisdir alidir"
get_tailored_data_wer=false
get_tailored_data_wer_opts="<lower_mer> <upper_mer> <utt2mer> <sdata> <data> <alisdir> <stm>"
score_opts="-m hyp"
make_ali=false
ali_opts="<data> <lang> <sdir> <alidir>"
ali_cmd="steps/align_fmllr.sh"
make_supervised_ctm=false
supervised_ctm_opts="<data> <lang> <alidir>"
stm_score=false
stm_score_opts="<alidir> <stm> <name>"
select_data_by_voting=false
data_by_voting_opts="<sdata> <source_utt_ctm_dir> <ctm_lsdecode> use_conf <tgtdir>"
use_conf_opts="--use-conf=true --conf-thresh=0.90"
prepare_eval=false

# end options
echo 
echo "$0 $@"
echo 

. parse_options.sh

function PrintOptions {
 cat <<END

$0 [options]:
cmd				# value, "$cmd"
nj				# value, $nj
steps				# value, "$steps"
check_xml			# value, $check_xml
prepare_data			# value, $prepare_data
prepare_data_opts		# value, "$prepare_data_opts"
train_lm			# value, $train_lm
train_lm_opts			# value, "$train_lm_opts"
normalize_dev_data		# value, $normalize_dev_data
normalize_dev_data_opts		# value, "$normalize_dev_data_opts"
make_utt2mer			# value, $make_utt2mer
make_utt2mer_opts		# value, "$make_utt2mer_opts"
prepare_lsdecode_cs_data	# value, $prepare_lsdecode_cs_data
make_lsdecode_data		# value, $make_lsdecode_data
lsdecode_data_opts		# value, "$lsdecode_data_opts"
subset_data_with_mer		# value, $subset_data_with_mer
subset_mer_opts			# value, "$subset_mer_opts"
make_word_human_stm		# value, $make_word_human_stm
make_word_human_stm_opts	# value, "$make_word_human_stm_opts"
get_tailored_data_wer		# value, $get_tailored_data_wer
get_tailored_data_wer_opts	# value, "$get_tailored_data_wer_opts"
score_opts			# value, "$score_opts"
make_ali			# value, $make_ali
ali_opts			# value, "$ali_opts"
ali_cmd				# value, "$ali_cmd"
make_supervised_ctm		# value, $make_supervised_ctm
supervised_ctm_opts		# value, "$supervised_ctm_opts"
stm_score			# value, $stm_score
stm_score_opts			# value, "$stm_score_opts"
select_data_by_voting		# value, $select_data_by_voting
data_by_voting_opts		# value, "$data_by_voting_opts"
use_conf_opts			# value, "$use_conf_opts"
prepare_eval			# value, $prepare_eval

END
}

PrintOptions;

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if $check_xml; then
  echo "$0: check_xml started @ `date`"
  cat ./mgb2015/dev/xml/20080507_222000_bbctwo_parallel_worlds_parallel_lives.xml | \
  source/test/mgb_check_xml.pl 
  echo "$0: check_xml ended `date`"
fi

if $prepare_data; then
  echo "$0: prepare_data started @ `date`"
  if [ ! -z $step01 ]; then
    optNames="filelist wavdir xmldir mer useall dir"
    . source/register_options.sh "$optNames" "$prepare_data_opts" || \
    { echo "$0: prepare_data: ERROR, @register_options.sh "; exit 1; }
   if $useall; then 
     xmer="$mer all"; 
   else
     xmer=$mer
   fi
    local/mgb_data_prep.sh $filelist $wavdir $xmldir "$xmer" $dir
  fi
  echo "$0: prepare_data ended @ `date`"
fi
prepare_lex=false
if $prepare_lex; then
  echo "$0: prepare_lex started @ `date`"
  echo "This is already done off-line"
  echo "$0: prepare_lex ended @ `date`"
fi
if $train_lm; then
  echo "$0: train_lm started @ `date`"
  
  echo "$0: train_lm ended @ `date`"
fi
if $normalize_dev_data; then
  echo "$0: normalize_dev_data started @ `date`"
  optNames="sdata data"
  . source/register_options.sh "$optNames" "$normalize_dev_data_opts" || \
  { echo "$0: normalize_dev_data: ERROR, @register_options.sh"; exit 1; }
  [ -d $data ] || mkdir -p $data
  cp -r $sdata/* $data
  cat $sdata/text | \
  source/egs/mgb_normalize_dev_text.pl | \
  egrep -v '\[|\(' | \
  egrep -v '\]|\)' > $data/text
  utils/fix_data_dir.sh $data
  echo "$0: normalize_dev_data ended @ `date`"
fi
if $make_utt2mer; then
  echo "$0: make_utt2mer started @ `date`"
  optNames="filelist xmldir dir"
  . source/register_options.sh "$optNames" "$make_utt2mer_opts" || exit 1
  [ -d $dir ] || mkdir -p $dir 
  [ -f $dir/utt2mer ] && rm $dir/utt2mer
  cat $filelist | while read basename; do     
    [ ! -e $xmldir/$basename.xml ] && echo "WARNING: Missing $xmldir/$basename.xml" && continue;
    xmlstarlet  sel -t -m '//segments[@annotation_id="transcript_align"]' -m "segment" -n -v  "concat(@who,' ',@starttime,' ',@endtime,' ',@WMER,' ')" -m "element" -v "concat(text(),' ')" $xmldir/$basename.xml | source/egs/mgb_add_to_datadir.py --make-utt2mer --basename=$basename --tgtdir=$dir 
  done
  
  echo "$0: make_utt2mer_ended @ `date`"
fi
if $make_lsdecode_data; then
  echo "$0: make_lsdecode_data started @ `date`"
  optNames="filelist wavdir xmldir dir"
  . source/register_options.sh "$optNames" "$lsdecode_data_opts" || exit 1
  mkdir -p $dir
  rm $dir/*  2>/dev/null
  cat $filelist | while read basename; do     
    [ ! -e $xmldir/$basename.xml ] && echo "WARNING: Missing $xmldir/$basename.xml" && continue;
    xmlstarlet  sel -t -m '//segments[@annotation_id="transcript_lsdecode"]' -m "segment" -n -v  "concat(@who,' ',@starttime,' ',@endtime,' ')" -m "element" -v "concat(text(),' ')" $xmldir/$basename.xml | \
    source/egs/mgb_add_to_datadir.py --basename=$basename --tgtdir=$dir 
    echo $basename $wavdir/$basename.wav >> $dir/wav.scp
done
  sort -k 2 $dir/utt2spk | utils/utt2spk_to_spk2utt.pl > $dir/spk2utt
  utils/fix_data_dir.sh $dir
  utils/validate_data_dir.sh --no-feats $dir
  echo "$0: make_lsdecode_data ended @ `date`"
fi

if $make_word_human_stm; then
  echo "$0: make_word_human_stm started @ `date`"
  optNames="data lang alisdir alidir"
  . source/register_options.sh "$optNames" "$make_word_human_stm_opts" || exit 1
  if [ ! -f $step01 ]; then
    make_ali=true
    ali_opts="$data $lang $alisdir $alidir"
  fi
fi
if $get_tailored_data_wer; then
  echo "$0: get_tailored_data_wer started @ `date`"
  optNames="lower_mer upper_mer utt2mer sdata data lang alisdir stm"
  . source/register_options.sh "$optNames" "$get_tailored_data_wer_opts" || exit 1
  if [ ! -z $step01 ]; then
    subset_mer_opts="$lower_mer $upper_mer $utt2mer $sdata $data"
    subset_data_with_mer=true
  fi
  if [ ! -z $step02 ]; then
     make_ali=true
     ali_opts="$data $lang $alisdir $data/ali"
  fi
fi
if $subset_data_with_mer; then
  echo "$0: subset_data_with_mer started @ `date`"
  optNames="lower_mer upper_mer utt2mer sdata data"
  . source/register_options.sh "$optNames" "$subset_mer_opts" || exit 1
  [ -d $data ] || mkdir -p $data
  cat $utt2mer | \
  perl -e '($min, $max) = @ARGV; $total = 0; $selected = 0; while(<STDIN>) { chomp; $total ++; if(m/(\S+)\s+(\S+)/) { 
    ($utt, $mer) = ($1, $2); if( $mer >=$min && $mer <= $max) { print "$_\n"; $selected ++; }  }  }
    $rate = $selected/$total; $s = sprintf("%.2f",$rate);
    print STDERR "$s\n"; 
    '  $lower_mer $upper_mer \
    > $data/utt2mer 2>$data/rate
  utils/subset_data_dir.sh --utt-list $data/utt2mer $sdata $data 
  echo "$0: subset_data_with_mer ended @ `date`"
fi
if $make_ali; then
  echo "$0: make_ali started @ `date`"
  optNames="data lang sdir alidir"
  . source/register_options.sh "$optNames" "$ali_opts" || exit 1
  njx=$(wc -l < $data/spk2utt)
  [ $njx -le 20 ] || njx=20; 
  $ali_cmd --nj $njx $data $lang $sdir $alidir || exit 1
  echo "$0: make_ali ended @ `date`"
fi
# this is ugly
if $make_word_human_stm; then
  if [ ! -z $step02 ]; then
    [ -f $alidir/ali.1.gz ] || \
    { echo "make_word_human_stm: step02: ERROR, alidir $alidir is not ready"; exit 1; }
    steps/get_train_ctm.sh  $data $lang $alidir  || exit 1
  fi
  if [ ! -z $step03 ]; then
    [ -f $alidir/ctm.1.gz ] || \
    { echo "make_word_human_stm: step03: ERROR, ctm.1.gz expected in $alidir"; exit 1; }
    for f in segments utt2spk reco2file_and_channel; do
      curFile=$data/$f
      [ -f $curFule ] || \
      { echo "make_word_human_stm: step03: ERROR, file $curFile expected in data $data"; exit 1; }
    done
    gzip -cd $alidir/ctm.*.gz | \
    source/egs/mgb_convert_ctm_to_stm.pl $data/segments $data/utt2spk  $data/reco2file_and_channel \
    > $alidir/stm
  fi
  echo "$0: make_word_human_stm ended @ `date`"
fi
if $get_tailored_data_wer; then
  if [ ! -z $step03 ]; then
    [ -f $alidir/ali.1.gz ] || { echo "get_tailored_data_wer: ERROR, file ali.1.gz does not exist in $alidir"; exit 1; }
    make_supervised_ctm=true
    supervised_ctm_opts="$data $lang $alidir"
  fi
fi
if $make_supervised_ctm; then
  echo "$0: make_supervised_ctm started @ `date`"
  optNames="data lang alidir"
  . source/register_options.sh "$optNames" "$supervised_ctm_opts" || exit 1
  
  steps/get_train_ctm.sh $data $lang $alidir || exit 1
  echo "$0: make_supervised_ctm ended @ `date`"
fi
if $get_tailored_data_wer; then
  if [ ! -z $step04 ]; then
    [ -f $alidir/ctm ] || { echo "get_tailored_data_wer: step04: ERROR, ctm file expected in $alidir"; exit 1; }
    stm_score=true
    stm_score_opts="$alidir $stm ali"
  fi
  echo "$0: get_tailored_data_wer ended @ `date`"
fi
if $stm_score; then
  echo "$0: stm_score started @ `date`"
  optNames="alidir stm name"
  . source/register_options.sh "$optNames" "$stm_score_opts" || exit 1
  score_tool=/opt/tools/NIST/sctk-2.4.8/bin/sclite 
  /opt/tools/NIST/sctk-2.4.8/bin/hubscr.pl sortSTM < $stm | \
  source/egs/mgb_filter_stm.pl $alidir/ctm \
   > $alidir/stm.sorted.filtered
  $score_tool  -s -r $alidir/stm.sorted.filtered  stm -h $alidir/ctm ctm  $score_opts \
  -n "$name" -f 0 -D -F  -o  sum rsum prf dtl sgml -e utf-8 || exit 1

  echo "$0: stm_score ended @ `date`"
fi
if $get_tailored_data_wer; then
  echo "$0: get_tailored_data_wer ended @ `date`"
fi
if $prepare_lsdecode_cs_data; then
  echo "$0: prepare_lsdecode_data started @ `date`"
  optNames="filelist wavdir xmldir dir"
  . source/register_options.sh "$optNames" "$lsdecode_data_opts" || exit 1
  mkdir -p $dir
  rm $dir/*  2>/dev/null
  cat $filelist | while read basename; do     
    [ ! -e $xmldir/$basename.xml ] && echo "WARNING: Missing $xmldir/$basename.xml" && continue;
    xmlstarlet  sel -t -m '//segments[@annotation_id="transcript_lsdecode"]' -m "segment" -n -v  "concat(@who,' ')" -m "element" -v "concat(' [ ', @CS,' ',@starttime,' ', @endtime,' ', text(),' ] ')" $xmldir/$basename.xml | \
    source/egs/mgb_make_utt_ctm_cs.pl >> $dir/ctm.has-cs
    echo $basename $wavdir/$basename.wav >> $dir/wav.scp
done
  echo "$0: prepare_lsdecode_data ended @ `date`"
fi
if $select_data_by_voting; then
  optNames="sdata sutt_ctm_dir tcs_ctm use_conf tgtdir"
  . source/register_options.sh "$optNames" "$data_by_voting_opts" || exit 1
  [ -d $tgtdir ] || mkdir -p $tgtdir
  for f in wav.scp utt2spk segments; do
    [ -f $sdata/$f ] ||\
    { echo "$0: select_data_by_voting: ERROR, $f is not there in $sdata"; exit 1; }
  done
  cp $sdata/wav.scp $tgtdir/
  echo "$0: select_data_by_voting started @ `date`"
  if $use_conf; then 
    use_conf=true
  else
    use_conf=
  fi
  
  source/code/mgb-filter-ctm --use-voting=true ${use_conf:+$use_conf_opts} "gzip -cd $sutt_ctm_dir/ctm.*.gz | source/egs/mgb_add_spk_to_ctm.pl $sdata/segments  $sdata/utt2spk|" \
  "cat $tcs_ctm| source/egs/mgb_normalize_ctm-has-cs.pl |" "|cat -" | \
  source/code/mgb-ctm-to-kaldi-data "cat -|" $tgtdir/utt2spk $tgtdir/segments $tgtdir/text
  utils/utt2spk_to_spk2utt.pl < $tgtdir/utt2spk > $tgtdir/spk2utt
  utils/fix_data_dir.sh $tgtdir
  echo "$0: select_data_by_voting ended @ `date`"
fi
if $prepare_eval; then
  echo "$0: prepare_eval started @ `date`"
  dir=./exp-mgb2015/data/eval-tsk01
  tmpdir=$dir/tmp
  if [ $step01 ]; then
    mkdir -p $tmpdir
   xmlstarlet sel -t  -m "//segments[@annotation_id='baseline_segmentation_and_clustering']" -m "segment" -n -v "concat(@who,' ',@starttime,' ',@endtime,' ')"  /data/users/hhx502/mgb2015/eval/clustering/eval.task1.clustering/*.xml | \
   perl -e 'while(<>){if(/^$/){}else{print;}}' > $tmpdir/segments
  fi
  if [ $step02 ]; then
    cat $tmpdir/segments | \
    perl -e 'while(<>){chomp; @A = split(" "); if(@A != 3){die " ERROR, bad line $_\n";} $s = $A[0]; $stem=""; $spkid="";
                       if ($s =~ m/(.*)_SPK(\d+)/){ $stem = $1; $spkid = $2; } else {die "Bad line $_\n";}   $utt = sprintf("%s-spkr%04d-%07d-%07d",$stem, $spkid, $A[1]*100, $A[2]*100);
                          $spk = sprintf("%s-spkr%04d", $stem, $spkid);  print "$utt $spk\n";
                        }' > $dir/utt2spk
    ./utils/utt2spk_to_spk2utt.pl < $dir/utt2spk > $dir/spk2utt
    cat $tmpdir/segments | \
    perl -e 'while(<>){chomp; @A = split(" "); if(@A != 3){die " ERROR, bad line $_\n";} $s = $A[0]; $stem=""; $spkid="";
                       if ($s =~ m/(.*)_SPK(\d+)/){ $stem = $1; $spkid = $2; } else {die "Bad line $_\n";}   $utt = sprintf("%s-spkr%04d-%07d-%07d",$stem, $spkid, $A[1]*100, $A[2]*100);
                          print "$utt $stem $A[1] $A[2]\n";
                        }' > $dir/segments
   find /data/users/hhx502/mgb2015/eval/audio/ -name "*.wav" | \
   perl -pe 'chomp; $lab =$_; $lab =~ s/.*\///g; $lab =~ s/\.wav//g; $_ = "$lab $_\n";' > $dir/wav.scp
   utils/fix_data_dir.sh $dir
  fi
  echo "$0: prepare_eval ended @ `date`"
fi
