#!/bin/bash 

. path.sh 
. cmd.sh 

# begin options
cmd=slurm.pl
steps=
nj=40
graph=exp/tri3a/graph
srcdir=exp/mpe-nnet4a

# end options

function Usage {
 cat<<EOF

 [Examples]: $0 --steps 1 data/wky-test-data-20/audio data/wky-test-data-20/part20

EOF
}

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

## echo "## LOG ($0): steps=$steps"  && exit 0
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
   done
fi

wavdir=$1
tgtdir=$2


[ -d $tgtdir ] || mkdir -p $tgtdir
if [ ! -z $step01 ]; then
  find $(cd $wavdir; pwd) -name '*.wav' | sort -u > $tgtdir/wavlist.txt
fi

if [ ! -z $step02 ]; then
  cat $tgtdir/wavlist.txt | \
  source/egs/wky-2017/prepare-data/make-wavscp.pl > $tgtdir/wav.scp
  cat $tgtdir/wav.scp | awk '{printf("%s %s\n", $1, $1);}' > $tgtdir/utt2spk
  cp $tgtdir/utt2spk $tgtdir/spk2utt
   
  echo "## LOG (step02, $0): done with wav.scp in '$tgtdir'"
fi

if [ ! -z $step03 ]; then
  wav-to-duration scp:$tgtdir/wav.scp  ark,t:$tgtdir/wav-to-duration.txt
  cat $tgtdir/wav-to-duration.txt | \
  perl -ane 'chomp; @A = split(/\s+/); printf ("%s %s 0 %.2f\n", $A[0], $A[0], $A[1]);' > $tgtdir/segments
  echo "## LOG (step03, $0): '$tgtdir/segments' generated @ `date`";
fi

if [ ! -z $step04 ]; then
  utils/fix_data_dir.sh $tgtdir
  validate_data_dir.sh --no-feats --no-text $tgtdir
fi

if [ ! -z $step05 ]; then
  for sdata in $tgtdir; do
    data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch
    source/egs/swahili/make_feats.sh --cmd "$cmd" --nj $nj  --fbank-pitch true \
    --fbank-pitch-cmd \
    "steps/make_fbank_pitch.sh  --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf" \
    $sdata $data $feat || exit 1
    echo "## LOG (step05, $0): fbank-pitch done @ '$sdata' @ `date`"
  done 
fi

if [ ! -z $step06 ]; then
  steps/nnet/decode.sh --cmd "$cmd" --nj $nj \
  $graph  $tgtdir/fbank-pitch  $srcdir/decode-wky-part20
  echo "## LOG (stpe06, $0): decoding done @ `date`"
fi
