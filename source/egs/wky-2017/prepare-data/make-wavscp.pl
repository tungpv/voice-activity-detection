#!/usr/bin/perl
use warnings;
use strict;

print STDERR "## LOG ($0): stdin expected\n";
my %vocab = ();
while(<STDIN>) {
  chomp;
  my $label = $_;  $label =~ s/.*\///g; $label =~ s/-.*//g;
  die if exists $vocab{$label};
  printf("%s-A /usr/bin/sox -r 8k %s -c 1  -r 16k -t wav  - remix 1 |\n", $label, $_);
  printf("%s-B /usr/bin/sox -r 8k %s -c 1  -r 16k -t wav  - remix 2 |\n", $label, $_);
}
print STDERR "## LOG ($0): stdin ended\n";
