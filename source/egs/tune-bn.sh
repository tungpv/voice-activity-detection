#!/bin/bash

. path.sh
. cmd.sh

# begin options
tune_vllp=false
tune_llp=false
tune_learn_rate=0.008
datasrcdir=
# end options
echo 
echo "$0 $@"
echo 
. parse_options.sh

function PrintOptions {
  cat<<END

$0 [options]:
tune_vllp			# value, $tune_vllp
tune_llp			# value, $tune_llp
tune_learn_rate			# value, $tune_learn_rate
datasrcdir			# value, "$datasrcdir"
END
}

PrintOptions

if $tune_vllp; then
  rdir=manual_vllp
  target_dir=$rdir/exp/tune.flp.hierbn2.$tune_learn_rate
  bnName=tune.flp.mling.bn$tune_learn_rate
  source/egs/run-nnet.sh --cmd run.pl --nj 20 \
  --rdir $rdir --tgt-dir $target_dir  ${datasrcdir:+--dnn-datasrcdir $datasrcdir} \
  --tune2_hierbn true  --tune-opts "--tune_learn_rate $tune_learn_rate" \
  --tune_feature_transform ../viet107/llp/exp/mling2.full.hierbn/hierbn2/final.feature_transform \
  --hierbn-hid-dim 1500

 source/egs/run-bnf.sh --run-gmm true --sdataName train:dev10h \
 --gmmtrain-opts "--train-id a --train-dataname train --tri-state 2000 --tri-pdf 30000" \
 --steps 1,2,3,4 --bnName $bnName $target_dir $rdir $rdir/exp/gmm.$bnName
fi

if $tune_llp; then
  rdir=llp2
  target_dir=$rdir/exp/tune.flp.hierbn2.$tune_learn_rate
  bnName=tune.flp.mling.bn$tune_learn_rate
  source/egs/run-nnet.sh --cmd run.pl --nj 20 \
  --rdir $rdir --tgt-dir $target_dir  ${datasrcdir:+--dnn-datasrcdir $datasrcdir} \
  --tune2_hierbn true --tune-opts "--tune_learn_rate $tune_learn_rate" \
  --tune_feature_transform ../viet107/llp/exp/mling2.full.hierbn/hierbn2/final.feature_transform \
  --hierbn-hid-dim 1500

 source/egs/run-bnf.sh --run-gmm true --sdataName train:dev10h \
 --gmmtrain-opts "--train-id a --train-dataname train --tri-state 2500 --tri-pdf 36000" \
 --steps 1,2,3,4 --bnName $bnName $target_dir $rdir $rdir/exp/gmm.$bnName

fi
