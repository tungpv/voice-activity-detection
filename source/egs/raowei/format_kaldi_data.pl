#!/usr/bin/perl
use warnings;
use strict;


if (@ARGV != 2) {
  die "\n\nUsage: $0 <srcdata-dir> <dir>\n\n";
}

my ($olddir, $dir) = @ARGV;

`[ -d $dir ] || mkdir -p $dir`;

foreach my $x ("$olddir/segments", "$olddir/wav.scp", "$olddir/utt2spk") {
  if (! -f $x) {
    die "ERROR, file $x expected"; 
  }
} 

my %old2newWav = ();

open(F, "$olddir/wav.scp") or die "ERROR, file $olddir/wav.scp cannot open\n";
open(OF, ">$dir/wav.scp") or die "ERROR, file $dir/wav.scp cannot open\n";
my $index = 1;
print STDERR "modify wav.scp file\n";
while(<F>) {
  chomp;
  s/^\s+//;
  m/([^\s]+)\s+(.*)/;
  my ($oldname, $rest) = ($1, $2);
  my $newname = sprintf ("seg%07d",$index ++);
  $old2newWav{$oldname}  = $newname;
  print OF "$newname $rest\n"; 
}
close F;
close OF;

open(F, "$olddir/utt2spk") or die "ERROR, file $olddir/utt2spk cannot open\n";
$index = 1;
my %old2newSpk = ();
my %utt2spk = ();
while(<F>) {
  chomp;
  my @A = split(/[\s\t]+/);
  die "ERROR, bad line $_ in $olddir/utt2spk\n" if @A != 2;
  my $oldspk = $A[1];
  if (exists $old2newSpk{$oldspk} ) {
    $utt2spk{$A[0]} = $old2newSpk{$oldspk};
    next;
  }
  my $newspk = sprintf("spk%05d", $index ++);
  $old2newSpk{$oldspk} = $newspk;
  $utt2spk{$A[0]} = $newspk;
}
close F;

# now for modify segments file
my %old2newUtt = ();
open(F, "$olddir/segments") or die "ERROR, file $olddir/segments cannot open\n";
open(OF, ">$dir/segments") or die "ERROR, file $dir/segments cannot open\n";
open(UTT2SPK, ">$dir/utt2spk") or die "ERROR, file $dir/utt2spk cannot open\n";
while(<F>) {
  chomp;
  my @A = split(/\s+/);
  die "ERROR: bad line $_\n" if @A != 4;
  if (not exists $old2newWav{$A[1]}) {
    print STDERR "WARNING: $A[1] at line $_ not found in wav.scp\n";
    next;
  }
  my $newSegName = $old2newWav{$A[1]};
  if (not exists $utt2spk{$A[0]}) {
    print STDERR "WARNING: $A[0] at line $_ not found in utt2spk\n";
    next;
  }
  my $newSpkName = $utt2spk{$A[0]};
  my $newUttName = sprintf("%s_%s_%05d_%05d", $newSpkName, $newSegName, $A[2]*100, $A[3]*100);
  print OF "$newUttName $newSegName $A[2] $A[3]\n";
  print UTT2SPK "$newUttName $newSpkName\n";
  $old2newUtt{$A[0]} = $newUttName;
}
close F;

# now for text file, if any
if ( -f "$olddir/text") {
  open(F, "$olddir/text") or die "ERROR, file $olddir/text cannot open\n";
  open(OF, ">$dir/text") or die "ERROR, file $dir/text cannot open\n";
  while(<F>) {
    chomp;
    s/^\s+//;
    m/(\S+)\s+(.*)/;
    my ($oldUttName, $rest) = ($1, $2);
    if (not exists $old2newUtt{$oldUttName}) {
      print STDERR "WARNING, utt $oldUttName not in segments file\n";
      next;
    }
    print OF "$old2newUtt{$oldUttName} $rest\n";
  }
  close F;
  close OF;
}
close OF;
close UTT2SPK;
print STDERR "making spk2utt to utt2spk, if possible\n";
`[ -f utils/utt2spk_to_spk2utt.pl ] && utils/utt2spk_to_spk2utt.pl < $dir/utt2spk >$dir/spk2utt `;
print STDERR "fix dir $dir, if possible\n";
`[ -f utils/fix_data_dir.sh ] && utils/fix_data_dir.sh $dir`;


