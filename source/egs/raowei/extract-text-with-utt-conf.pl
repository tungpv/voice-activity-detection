#!/usr/bin/perl
use warnings;
use strict;
print STDERR "\nLOG: $0 ", join(" ", @ARGV), "\n";
my $conf_thresh = 0;
sub Usage {
  print STDERR "\nUsage: cat foo.ctm | $0 [ --conf-thresh <thresh>]\n\n";
}
if (@ARGV == 2) {
  if($ARGV[0] eq "--conf-thresh"|| $ARGV[0] eq "--conf_thresh") {
    $conf_thresh = $ARGV[1];
    die "ERROR, illegal conf_thresh $conf_thresh \n" if ($conf_thresh<0 ||$conf_thresh >1);
  } else {
    Usage();
    exit 1;
  }
} else {
  Usage();
  exit 1;
}
my $prev_uttname = "";
my $text_line = "";
my $aver_conf = 0;
my $word_num = 0;
my $word = "";
print STDERR "stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/[\s]+/);
  my $uttname = $A[0];
  my $conf = 0;
  $word = $A[4];
  $conf = $A[5] if (@A == 6);
  if ($uttname ne $prev_uttname && $prev_uttname ne "") {

    $aver_conf /= $word_num;
    if($aver_conf >= $conf_thresh) {
      print $text_line, "\n"; 
    }
    $text_line = "$uttname $word";
    $aver_conf = $conf;
    $word_num = 1;
  } elsif ($prev_uttname eq "") {
    $text_line = "$uttname $word";
    $aver_conf = $conf;
    $word_num = 1;
  } else {
    $text_line .= " $word";
    $aver_conf += $conf;
    $word_num ++;
  }
  $prev_uttname = $uttname;
}
if ($word_num > 0) {
  $aver_conf /=$word_num;
  if($aver_conf >= $conf_thresh) {
    print $text_line, "\n";
  }
}
print STDERR "stdin ended\n";
