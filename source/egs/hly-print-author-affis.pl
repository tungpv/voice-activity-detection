#!/usr/bin/perl 
use strict;
use warnings;
use open qw(:std :utf8);

print STDERR "\n$0 ", join(" ", @ARGV), "\n";

print STDERR "$0: stdin expected\n";
my $lines = 0;
my %vocab = ();
sub CheckAuthorAffis {
  my ($authors, $affis) = @_;
  my $authNum = split(/[,]/, $authors);
  my $affNum = split(/[;]/, $affis);
  if ($authNum != $affNum && $affNum != 1) {
    print "$authors\t$affis\n";
  }
}
while(<STDIN>) {
  chomp;
  if($lines++ == 0) {
    print STDERR "$_\n";
    next;
  } 
  my @A = split(/\t/);
  my $authors = shift @A;
  my $affis = $A[12];
  if (! defined $affis) {
    print "$authors\t$affis\n";
    next;
  }
  CheckAuthorAffis($authors, $affis);
}
print STDERR "$0: stdin ended\n";

