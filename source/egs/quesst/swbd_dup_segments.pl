#!/usr/bin/perl

use strict;
use warnings;
my $usage = <<END;

  Duplicate lines in the semgments file
  cat segments | $0 <new_wav.scp> <id1> <id2> ... > new_segments

END
if (@ARGV < 2) {
  die "$usage\n";
}

my $wav_scp_fname = shift @ARGV;
my @array_ids = @ARGV;

open(F, "$wav_scp_fname") or die "$0: File $wav_scp_fname cannot open\n";
my %vocab = ();
while(<F>) {
  chomp;
  die "$0: ERROR, bad line $_ in $wav_scp_fname\n" if ! m/(\S+)\s+(\S+)/;
  my ($label, $path) = ($1, $2);
  $vocab{$label} = $path;
}
close F;

print STDERR "stdin expected\n";
while(<STDIN>) {
  chomp;
  die "$0: ERROR, bad line $_ in STDIN\n" if ! m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/;
  my ($segId, $fileId, $start, $end) = ($1, $2, $3, $4);
  print "$_\n";
  foreach my $id  (@array_ids) {
    my $new_fileId = "$id-$fileId";
    if (exists $vocab{$new_fileId}) {
      print "$id-$segId $new_fileId $start $end\n";
    }
  }
}
print STDERR "stdin ended\n";

