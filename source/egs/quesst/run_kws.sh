#!/bin/bash

. path.sh
. cmd.sh

# set -u
set -o pipefail

# begin options
steps=

decode_cmd=steps/nnet/decode.sh
query_name=
query_lat_opts="--beam 13 --lattice-beam 10 --max-mem 1000000000 --skip-scoring true" 
query_lat_cmd="slurm.pl --exclude=node01,node02"
query_nj=40

query_fsts_steps=1,2,3,4,5
query_fsts_nbest_acwt=0.1
query_fsts_nbest_num=2000
query_fsts_min_phone_num=6
query_fsts_partial_phone_num=6
query_ground_truth_dir=

tgt_lat_opts="--beam 10 --lattice-beam 7 --max-mem 800000000 --skip-scoring true"
tgt_latdir=
tgt_indexed=false
tgt_nj=40

min_lmwt=10
max_lmwt=10
write_and_score_opts="false 1.0 66.6 0.01 true 0.6 true"
kwseval=/opt/tools/NIST/F4DE-3.2.0/KWSEval/tools/KWSEval/KWSEval.pl
# end options

echo 
echo LOG: $0 $@
echo

. parse_options.sh || exit 1
function PrintOptions {
  cat <<END

[options]:
--steps					# value, "$steps"

--decode-cmd				# value, "$decode_cmd"
--query-name				# value, "$query_name"
--query-lat-opts			# value, "$query_lat_opts"
--query-lat-cmd				# value, "$query_lat_cmd"
--query-nj				# value, $query_nj

--query-fsts-steps			# value, "$query_fsts_steps"
--query-fsts-nbest-acwt			# value, $query_fsts_nbest_acwt
--query-fsts-nbest-num			# value, $query_fsts_nbest_num
--query-fsts-min-phone-num		# value, $query_fsts_min_phone_num
--query-fsts-partial-phone-num		# value, $query_fsts_partial_phone_num

--query-ground-truth-dir		# value, "$query_ground_truth_dir"

--tgt-lat-opts				# value, "$tgt_lat_opts"
--tgt-latdir				# value, "$tgt_latdir", # if specified, use the lattice already generated
--tgt-indexed				# value, $tgt_indexed, # if true, use the old index generated previously
--tgt-nj				# value, $tgt_nj

--min-lmwt				# value, $min_lmwt
--max-lmwt				# value, $max_lmwt
--write-and-score-opts			# value, "$write_and_score_opts", # skip_scoring, ntrue_scale, beta, frame_dur, normalize, duptime, remove_dup
--kwseval				# value, "$kwseval"

[steps]:
1: decode query data
2: prepare query fsts
3: decode test data
4: partial search
5: full search
END
}
if [ $# -ne 7 ]; then
  echo 
  echo "Usage: $(basename $0) [options] <lang> <graphdir> <query_data> <query_decode_dirname> <tgt_data> <tgt_decode_dirname> <sdir>"
  PrintOptions
  echo && exit 1
fi

lang=$1
graphdir=$2
query_data=$3
query_decode_dir=$4
tgt_data=$5
tgt_decode_dir=$6
sdir=$7


if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
gmmdir=$(dirname $graphdir)
if [ ! -z $step01 ]; then
  echo "$0: making lattice for the query data started @ `date`"
  source/egs/run-bnf.sh --decoding_eval true --cmd "$query_lat_cmd"  --nj $query_nj --lattice_opts "$query_lat_opts" \
   --decoding_opts "$query_data $lang $gmmdir $graphdir $sdir/$query_decode_dir $decode_cmd" || exit 1
  echo "$0: Done @ `date`"
fi

query_latdir=$sdir/$query_decode_dir
tgtdir=$query_latdir/nbest.$query_fsts_nbest_num
full_query_fsts_dir=$tgtdir/f.$query_fsts_min_phone_num
partial_query_fsts_dir=$tgtdir/p.${query_fsts_min_phone_num}.${query_fsts_partial_phone_num}
query_tgtdata=$tgt_data
if [ ! -z $step02 ]; then
    echo "$0: prep_kw_fsts started @ `date`"
  [ -f $query_latdir/lat.1.gz ] || \
  { echo "prep_kw_fsts: ERROR, query_latdir $query_latdir is not ready"; exit 1; }
  [ ! -z $query_ground_truth_dir ] || \
  { echo "prep_kw_fsts: ERROR, query_ground_truth_dir is not ready"; exit 1; }
  source/egs/quesst/prep_kw_fsts.sh --cmd "$query_lat_cmd"  --steps $query_fsts_steps \
  --nbest-acwt $query_fsts_nbest_acwt \
  --nbest-num $query_fsts_nbest_num \
  --nbestdir $tgtdir \
  --min-phone-num $query_fsts_min_phone_num \
  --partial-phone-len  $query_fsts_partial_phone_num \
  --tgtdata $tgt_data \
  --ground-truth-dir $query_ground_truth_dir \
  --kwsource-full $full_query_fsts_dir \
  --kwsource-partial $partial_query_fsts_dir \
  $lang $query_latdir || exit 1  
  echo "$0: prep_kw_fsts ended, check $tgtdir @ `date`"
fi
latdir=$sdir/$tgt_decode_dir
if [ ! -z $tgt_latdir ]; then
  latdir=$tgt_latdir
  step03=
fi
if [ ! -z $step03 ]; then
  echo "$0: making lattice on the target data started @ `date`"
  source/egs/run-bnf.sh --decoding_eval true --cmd "$query_lat_cmd"  --nj $tgt_nj --lattice_opts "$tgt_lat_opts" \
  --decoding_opts "$tgt_data $lang $gmmdir $graphdir $latdir $decode_cmd" || exit 1
  echo "$0: Done @ `date`"
fi
run_kws_steps=1,2,3
$tgt_indexed && run_kws_steps=2,3
if [ ! -z $step04 ]; then
  echo "$0: doing partial_query_search started @ `date`"
  source/egs/quesst/run_kws_inline.sh --cmd "$query_lat_cmd" --steps $run_kws_steps \
  --default true \
  --min-lmwt $min_lmwt --max-lmwt $max_lmwt \
  --query-list-name p${query_name}.${query_fsts_min_phone_num}${query_fsts_partial_phone_num} \
  $lang $partial_query_fsts_dir $latdir/index || exit 1
  echo "$0: Done @ `date`"
fi

if [ ! -z $step05 ]; then
  echo "$0: doing full_query_search started @ `date`"
  source/egs/quesst/run_kws_inline.sh --cmd "$query_lat_cmd" --steps 2,3 \
  --default true \
  --min-lmwt $min_lmwt --max-lmwt $max_lmwt \
  --query-list-name f${query_name}.${query_fsts_min_phone_num} \
  $lang $full_query_fsts_dir $latdir/index || exit 1
  echo "$0: Done @ `date`"
fi

echo "$0: Done `date`"
