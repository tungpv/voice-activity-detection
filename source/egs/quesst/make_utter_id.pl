#!/usr/bin/perl -w
use strict;

scalar @ARGV > 0 || die "\nsegments file expected ...\n";
my $sgfile = shift @ARGV;
open (my $file, "<", $sgfile) || die "\nsegments $sgfile cannot open ...\n";
my $index = 1;
while (<$file>) {
  chomp;
  m/(\S+)\s+(\S+)/||next;
  my ($uname) = ($1);
  print "$uname $index\n";
  $index ++;
}
close $file;

