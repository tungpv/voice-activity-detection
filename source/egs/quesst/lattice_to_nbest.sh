#!/bin/bash

. path.sh
. cmd.sh

#  begin options
cmd=run.pl
acwt=0.1
N=1000
# end options
set -o pipefail

. parse_options.sh

if [ $# -ne 2 ]; then
  echo
  echo "Usage: $0 [options] <latdir> <dir>" 
  echo
cat<<END
[options]:
cmd			# value, "$cmd"
acwt			# value, $acwt
N			# value, $N

END
  exit 1
fi

latdir=$1
dir=$2
[ -f $latdir/lat.1.gz ] || \
{ echo "$0: ERROR, latdir $latdir is not ready"; exit 1; }

nj=`cat $latdir/num_jobs` || exit 1;
$cmd JOB=1:$nj $dir/log/nbest.JOB.log \
lattice-to-nbest --acoustic-scale=$acwt --n=$N \
"ark:gunzip -c $latdir/lat.JOB.gz|" ark:- \|  \
lattice-rmali ark,t:- "ark,t:|gzip -c >$dir/nbest.JOB.gz" || exit 1;

