#!/usr/bin/perl 
use strict;
use warnings;

my $usage =<<END;

 cat feats.scp |\\
 $0 <hour: to be selected> <segments> <selected.uttlist>

END

if (@ARGV != 3) {
  die $usage;
}
my ($hrs, $segments, $selUtt) = @ARGV;
open(F, "$segments") or die "$0: ERROR, file $segments cannot open\n";
my %vocab = ();
while(<F>) {
  chomp;
  die "$0: ERROR, bad line $_ in segments" if ! m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/;
  $vocab{$1} = $4 - $3;
}
close F;
open(OF1, ">$selUtt") or die "$0: ERROR, file $selUtt cannot open\n";
print STDERR "$0: stdin expected\n";
my $selHrs = 0;
while(<STDIN>) {
  chomp;
  die "$0: ERROR, bad line $_ in STDIN\n" if ! m/(\S+)\s+(.*)/;
  my $utt = $1;
  die "$0: ERROR, not identified utt $utt ($_) in segments\n"   if not exists $vocab{$utt};
  $selHrs += $vocab{$utt};
  if($selHrs < $hrs*3600) {
     print OF1 $utt, "\n";
  } else {
    print STDOUT $utt, "\n";
  }
}
print STDERR "$0: stdin ended\n";
close OF1;
