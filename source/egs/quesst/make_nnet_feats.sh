#!/bin/bash

. path.sh
. cmd.sh

# begin options
nj=20
cmd=run.pl
use_gpu=no
# end options
echo
echo LOG: $0 $@
echo

. parse_options.sh || exit 1
function print_options {
cat<<END
 $0 [options] <srcdata> <nndir> <data> <featdir>
 [options]:
 nj			# value, $nj
 cmd			# value, "$cmd"
 use_gpu		# value, $use_gpu

END
} 
if [ $# -ne 4 ]; then
  print_options && exit 1
fi

srcdata=$1
nndir=$2
data=$3
featdir=$4


feat=$featdir/data
log=$featdir/log
mkdir -p $data $feat $log

required="$srcdata/feats.scp $nndir/final.nnet $nndir/final.feature_transform"
for f in $required; do
  [ ! -f $f ] && echo "$0: ERROR, missing $f" && exit 1;
done

utils/copy_data_dir.sh $srcdata $data; rm $data/{feats,cmvn}.scp 2>/dev/null


name=$(basename $srcdata)
sdata=$srcdata/split$nj
[[ -d $sdata && $srcdata/feats.scp -ot $sdata ]] || split_data.sh $srcdata $nj || exit 1;


nnet=$data/feature_extractor.nnet
nnet-concat $nndir/final.feature_transform $nndir/final.nnet $nnet 2>$log/feature_extractor.log || exit 1
nnet-info $nnet >$data/feature_extractor.nnet-info

cmvn_opts=
delta_opts=
D=$nndir
[ -e $D/norm_vars ] && cmvn_opts="--norm-means=true --norm-vars=$(cat $D/norm_vars)" # Bwd-compatibility,
[ -e $D/cmvn_opts ] && cmvn_opts=$(cat $D/cmvn_opts)
[ -e $D/delta_order ] && delta_opts="--delta-order=$(cat $D/delta_order)" # Bwd-compatibility,
[ -e $D/delta_opts ] && delta_opts=$(cat $D/delta_opts)
#
# Create the feature stream,
feats="ark,s,cs:copy-feats scp:$sdata/JOB/feats.scp ark:- |"
# apply-cmvn (optional),
[ ! -z "$cmvn_opts" -a ! -f $sdata/1/cmvn.scp ] && echo "$0: Missing $sdata/1/cmvn.scp" && exit 1
[ ! -z "$cmvn_opts" ] && feats="$feats apply-cmvn $cmvn_opts --utt2spk=ark:$srcdata/utt2spk scp:$srcdata/cmvn.scp ark:- ark:- |"
# add-deltas (optional),
[ ! -z "$delta_opts" ] && feats="$feats add-deltas $delta_opts ark:- ark:- |"
#

$cmd JOB=1:$nj $log/make_bnfeats.JOB.log \
nnet-forward --use-gpu=$use_gpu $nnet "$feats" \
ark,scp:$feat/raw_enhanced_$name.JOB.ark,$feat/raw_enhanced_$name.JOB.scp \
    || exit 1;
  # concatenate the .scp files
for ((n=1; n<=nj; n++)); do
  cat $feat/raw_enhanced_$name.$n.scp >> $data/feats.scp
done
