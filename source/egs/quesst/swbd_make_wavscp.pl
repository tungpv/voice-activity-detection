#!/usr/bin/perl
use warnings;
use strict;

if (@ARGV != 2) {
  die "\nExample: ls xpath/*.wav | $0 clean/wav.scp nd1 > new_path/wav.scp\n\n";
}

my ($scpFname, $id) = @ARGV;
open(F, "$scpFname") or die "$0: ERROR, file $scpFname cannot open\n";
my %vocab = ();
while(<F>){
  chomp;
  my @A = split(" ");
  die "$0: ERROR, bad line $_\n" if @A != 2;
  my ($fileId, $path) = @A;
  $path =~ s/.*\///g;
  $path =~ s/\.wav//g;
  $vocab{$path} = $fileId;
  print "$_\n";
}
close F;

print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  my $fname = $_;
  $fname =~ s/.*\///g;
  $fname =~ s/\.wav//g;
  $fname =~ m/([^\-]+)(.*)/;
  my $label = $1;
  die "$0: unseen label $label in $_\n" if not exists $vocab{$label};
  my $new_label = sprintf("%s-%s", $id, $vocab{$label});
  print "$new_label $_\n";
}
print STDERR "$0: stdin ended\n";
