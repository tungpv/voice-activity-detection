#!/bin/bash

function Usage {
 cat<<END
 Usage: $0 <lexicion> <dir>
 [examples]:
 $0  kws2016/flp-grapheme/exp/mono/phone-nnet5a/phone-dict/lexicon.txt  kws2016/flp-grapheme/exp/mono/phone-nnet5a/phone-loop

END
}


if [ $# -ne 2 ]; then
  Usage && exit 1
fi

set -o pipefail

lexicon=$1
dir=$2
[ -d $dir ] || mkdir -p $dir

cat $lexicon | egrep -v '<eps>|#' | awk '{print $1;}' | \
  perl -e '$vocab{"<s>"}++; $vocab{"</s>"} ++; 
     while (<STDIN>) {chomp; $vocab{$_}++;} my $num = keys %vocab;  $prob = sprintf ("%.6f", log(1/$num)/log(10));
     print "\n\\data\\\nngram 1=$num\n\n\\1-grams:\n";
     foreach $w (sort keys %vocab) {
       if($w =~ /<s>/) { 
         print "-99\t$w\n"
       } else { 
         print "$prob\t $w\n";
       }
     }
    print "\n\\end\\";
  '  | gzip > $dir/lm.gz

