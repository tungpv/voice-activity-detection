#!/usr/bin/perl
use warnings;
use strict;

if (@ARGV != 3) {
  die "\nExample cat segments | $0 [left-forward(0.1)] [right-afterward(0.1)] wav2dur > new_segments\n\n";
}

my ($left_inc, $right_inc, $wav2dur) = @ARGV;

open(F, "$wav2dur") or die "$0: ERROR, file $wav2dur cannot open\n";
my  %vocab = ();
while(<F>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  my ($fname, $dur) = ($1, $2);
  die "$0: ERROR, dup $fname @ $_\n" if exists $vocab{$fname};
  $vocab{$fname} = $dur;
}
close F;
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  my @A = split(" ");
  die "$0: ERROR, bad line $_\n" if @A != 4;
  my ($segId, $fileId, $start, $end) = @A;
  die "$0: ERROR, file $fileId is not in wav.scp\n" if not exists $vocab{$fileId};
  if($start - $left_inc >= 0) {
    $start -= $left_inc;
  }
  if ($end + $right_inc <= $vocab{$fileId} ) {
    $end += $right_inc;
  }
  $A[2] = $start;
  $A[3] = $end;
  print join(" ", @A), "\n";
}
print STDERR "$0: stdin ended\n";
