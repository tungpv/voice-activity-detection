#!/bin/bash

. path.sh
. cmd.sh

set -o pipefail
# begin options
cmd="slurm.pl --exclude=node01,node02"
steps=
srcdir=
acwt=0.1
nbest=3
min_phone_len=6
sub_phone_len=6
silence_int=
phone_sequence="6 7 8 9"
# end options

echo
echo LOG: $0 $@
echo 

. parse_options.sh || exit 1

function PrintOptions {
  cat <<END
[options]:
--cmd					# value, "$cmd"
--steps					# value, "$steps"  
--srcdir				# value, "$srcdir", default, srcdir=latdir/..
--acwt					# value, $acwt
--nbest					# value, $nbest
--min-phone-len				# value, $min_phone_len
--sub-phone-len				# value, $sub_phone_len
--silence-int				# value, $silence_int
--phone-sequence            # value, "$phone_sequence"       

[steps]:
1: make word_boundary.int file if necessary
2: lattice to nbest, and to ctm conversion
3: make new segments
4: extract partial boundary 
END
}

if [ $# -ne 3 ]; then
  echo
  echo "Usage: $(basename $0) [options] <data> <lang> <latdir>"
  PrintOptions
  echo && exit 1
fi

data=$1
lang=$2
latdir=$3

for x in $lang/phones/nonsilence.int $lang/phones/silence.int $lang/words.txt; do
  [ -f $x ] || \
  { echo "ERROR, file $x expected"; exit 1; }
done
[ -z $silence_int ] && silence_int=$(grep '<silence>' $lang/words.txt | awk '{print $2;}')
[ -z $silence_int ] && silence_int=0

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  source/egs/quesst/make_word_boundary_int.sh $lang/phones || exit 1
  echo "making word_boundary.int done in $lang/phones "
fi
sdir=$srcdir
[ -z $sdir ] && sdir=$latdir/..
ctmdir=$latdir/nbest.$nbest
if [ ! -z $step02 ]; then
  echo "nbest to ctm started @ `date`"
  nj=$(cat $latdir/num_jobs)
  [ ! -z $nj ] && [ $nj -gt 0 ] || \
  { echo "ERROR, num_jobs expected from $latdir"; exit 1; }
  for x in $sdir/final.mdl $lang/phones/word_boundary.int $latdir/lat.1.gz; do
    [ -f $x ] || \
    { echo "ERROR, file $x expected"; exit 1; }
  done
  $cmd JOB=1:$nj $latdir/nbest.$nbest/log/ctm.JOB.log \
  lattice-to-nbest  --acoustic-scale=$acwt --n=$nbest \
  "ark:gzip -cd $latdir/lat.JOB.gz|" ark:- \| \
  lattice-align-words $lang/phones/word_boundary.int  $sdir/final.mdl  ark:- ark:- \|\
  nbest-to-ctm ark:-  $ctmdir/ctm.JOB.txt || exit 1
  echo "nbest to ctm ended @ `date`"
fi
if [ ! -z $step03 ]; then
  cat $ctmdir/ctm.*.txt | \
  source/egs/quesst/get_query_time_boundary.pl --silence-int $silence_int \
  --sub-phone-len=$sub_phone_len  $data/segments $data/new_segments /dev/null  /dev/null || exit 1
fi

if [ ! -z $step04 ]; then
  [ -f $data/segments ] || \
  { echo "ERROR, segments file expected from $data"; exit 1; }
  for x in $phone_sequence; do 
    sub_phone_len=$x
    cat $ctmdir/ctm.*.txt | \
    source/egs/quesst/get_query_time_boundary.pl --silence-int $silence_int \
    --sub-phone-len=$sub_phone_len  $data/segments /dev/null - /dev/null > $data/p.int_sil_rm.${sub_phone_len}_bound || exit 1
    
  done
fi
