#!/bin/bash 

# begin options
subset_time_ratio=0.1
random=false
# end options
function print_options {
  echo 
  echo "Select subset data from the source data, data2 is optional, and"
  echo "it is complementary with the data, taking sdata as the super set."
  echo
  echo "Usage: $0 [options] <sdata> <data> [data2]"
cat<<END

 [options]:
 --subset_time_ratio			# value, $subset_time_ratio
 --random				# value, $random

END
}

. utils/parse_options.sh || \
{ echo "$0: ERROR, utils/parse_options.sh expected"; exit 1; }

if [ $# -lt 2 ]; then
  print_options
  exit 1
fi

sdata=$1
data=$2
data2=
[ $# -ge 3 ] && data2=$3
x=$subset_time_ratio
if [ $(bc -l <<< "$x < 1 && $x > 0") -ne 1 ]; then
  echo "$0: ERROR, subset_time_ratio($subset_time_ratio) should be (0, 1)" && exit 1
fi

for x in $sdata/{segments,utt2spk}; do
  [ -f $x ] || { echo "$0: ERROR, file $x expected"; exit 1; }
done
[ -d $data ] || mkdir -p $data
[ ! -z $data2 ] &&  mkdir -p $data2

tot_hour=$(cat $sdata/segments| awk '{x+=$4-$3;}END{print x/3600;}')
sel_hour=$(perl -e "print $tot_hour*$subset_time_ratio")
echo "$0: tot_hour=$tot_hour, sel_hour=$sel_hour"
super_utt2spk=$sdata/utt2spk
if $random; then
  super_utt2spk_new=$data/.feats_shuffled.scp
  cat $super_utt2spk | \
  perl -MList::Util -e 'print List::Util::shuffle <>' > $super_utt2spk_new
  line_num_new=$(wc -l < $super_utt2spk_new)
  [ -z $line_num_new ] && \
  { echo "$0: ERROR, file $super_utt2spk_new is failed to generate"; exit 1; }
  line_num=$(wc -l < $super_utt2spk)
  [ $line_num_new -eq $line_num ] || \
  { echo "$0: ERROR, original lines: $line_num ($super_utt2spk), new lines: $line_num_new($super_utt2spk_new), are mismatched"; exit 1; }
  super_utt2spk=$super_utt2spk_new
fi
uttlist2=/dev/null
[ ! -z $data2 ] && uttlist2=$data2/uttlist
cat $super_utt2spk | \
source/egs/quesst/subset_lines.pl $sel_hour $sdata/segments $data/uttlist \
>$uttlist2
utils/subset_data_dir.sh --utt-list $data/uttlist  $sdata $data
[ ! -z $data2 ] && \
utils/subset_data_dir.sh --utt-list $data2/uttlist $sdata  $data2

echo "$0: Done !"


