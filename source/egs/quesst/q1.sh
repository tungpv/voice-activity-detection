#!/bin/bash

. path.sh
. cmd.sh

# begin options

nj=20
cmd=run.pl
steps=
prepare_data=false

# end options

echo
echo "$0 $@"
echo

. parse_options.sh

function PrintOptions {
  cmdName=$(echo $0| perl -pe 's/^.*\///g;')
  cat <<END

$cmdName [options]:
cmd					# value, "$cmd"
nj					# value, "$nj"
steps					# value, "$steps"
prepare_data				# value, $prepare_data

END
}

PrintOptions;

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=true
  done
fi

if $prepare_data; then
  echo "$0: prepare_data started @ `date`"
  if [ ! -z $step01 ]; then
    
  fi
  echo "$0: prepare_data done @ `date`"
fi

