#!/usr/bin/perl
use warnings;
use strict;

my $usage = <<END;

 Duplicate utt2spk file with duplicated segments file.
 e.g.:
 cat utt2spk  |\\
 $0  duplicated/segments id1 id2 ... > duplicated/utt2spk

END

if (@ARGV != 3) {
  die $usage;
}

my ($segFile, $id1, $id2) = @ARGV;
my %vocab = ();
open(F, "$segFile") or die "$0: ERROR, file $segFile cannot open\n";
while(<F>) {
  chomp;
  die "$0: ERROR, bad line $_ in file $segFile\n"
  if ! m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/;
  my ($segId, $fileId, $start, $end) = ($1, $2, $3, $4);
  $vocab{$segId} ++;
}
close F;
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  die "$0: ERROR, bad line $_ in STDIN\n"
  if ! m/(\S+)\s+(\S+)/;
  my ($utt, $spk) = ($1, $2);
  print "$_\n";
  foreach my $id ($id1, $id2) {
    my $utt_new = "$id-$utt";
    if(exists $vocab{$utt_new}) {
      print "$utt_new $id-$spk\n";
    }
  }
}
print STDERR "$0: stdin ended\n";


