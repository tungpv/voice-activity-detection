#!/bin/bash

. path.sh
. cmd.sh

# begin options
write_and_score_opts="skip_scoring(false) ntrue_scale(1.0) beta(66.6) frame_dur(0.01) normalize(true) duptime(0.6) remove_dup(true)"
kwslist=
scoredir=
kwseval=/opt/tools/NIST/F4DE-3.2.0/KWSEval/tools/KWSEval/KWSEval.pl
# end options
echo
echo LOG: $0 $@
echo

. parse_options.sh

function PrintOptions {
  cat <<END

Main [options]:
write_and_score_opts			# value, "$write_and_score_opts"
kwslist					# value, "$kwslist"
scoredir				# value, "$scoredir"
kwseval					# value, "$kwseval"

END
}

if [ $# -ne 2 ]; then
  echo 
  echo "Usage: $0 [options] <kwsdatadir> <resdir>"
  PrintOptions
  echo  && exit 1
fi

kwsdatadir=$1
resdir=$2

optNames="skip_scoring ntrue_scale beta frame_dur normalize duptime remove_dup"
. source/register_options.sh  "$optNames" "$write_and_score_opts" || exit 1

[ -z $kwslist ] && kwslist=$resdir/kwslist.xml
[ -z $scoredir ] && scoredir=$resdir/score

[ -d $scoredir ] || mkdir -p $scoredir

duration=$(cat $kwsdatadir/duration) || exit 1

set -e 
set -o pipefail 
if [ ! -f $kwslist ]; then
  cat $resdir/result.* | \
  utils/write_kwslist.pl  --Ntrue-scale=$ntrue_scale --beta=$beta \
  --flen=$frame_dur --duration=$duration \
  --segments=$kwsdatadir/segments --normalize=$normalize \
  --duptime=$duptime --remove-dup=$remove_dup \
  --map-utter=$kwsdatadir/utter_map --digits=8 \
  - $kwslist || exit 1
else
  echo "$0: kwslist file $kwslist is already there !"
fi
if ! $skip_scoring; then
  source/egs/quesst/kws_score.sh --kwseval $kwseval  $kwsdatadir $kwslist  $scoredir || exit 1
fi


