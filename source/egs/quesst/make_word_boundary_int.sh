#!/bin/bash


if [ $# -ne 1 ]; then
  echo 
  echo "Usage: $0 <phones_dir>"
  echo && exit 1
fi

dir=$1

for x in $dir/silence.int $dir/nonsilence.int; do
  [ -f $x ] || \
  { echo "$0: file $x expected"; exit 1; }
done 

cat $dir/silence.int | \
perl -pe 'chomp; $_="$_\tnonword\n";' > $dir/word_boundary.int
cat $dir/nonsilence.int | \
perl -pe 'chomp; $_="$_\tsingleton\n";' >> $dir/word_boundary.int
