#!/usr/bin/perl

use warnings;
use strict;

my $arg_num = scalar @ARGV;

if ($arg_num != 1) {
  die "\nExample: cat kwslist.xml | $0 term.list > new_kwslist.xml\n\n";
}

my ($termlist) = @ARGV;
open(F, "$termlist") or die "$0: ERROR, file $termlist cannot open\n";
my %vocab_termlist = ();
my $i = 0;
while(<F>) {
  chomp;
  s/ +//g;
  next if ! m/(\S+)/;
  $vocab_termlist{$1} ++;
  $i ++
}
close F;
print STDERR "$0: $i term loaded\n";

print STDERR "$0: stdin expected\n";
my $curkw = "";
my %vocab = ();
while(<STDIN>) {
  chomp;
  if(/language/ || /termslist/) {
    print "$_\n";
    next;
  }
  if(/termid=\"([^\"]+)\"/) {
    $curkw = $1;
   if (exists $vocab_termlist{$curkw}) {
     print "$_\n";
   }
   next;
  }
  if (/detected_termlist/ && exists $vocab_termlist{$curkw} ) {
    print "$_\n";
    next;
  }
   if(exists $vocab_termlist{$curkw}) {
    if(/file=\"([^\"]+)\"/) {
      if(not exists $vocab{$curkw}{$1}) {
        print "$_\n";
        $vocab{$curkw}{$1} ++;
        next;
      }
    } 
    next;
  }
}
print STDERR "$0: stdin ended\n";
