#!/bin/bash

. path.sh
. cmd.sh

# begin options
lowest_score=0
id=
default=false
# end options

function print_options {
  echo
  echo "Usage: $0 [options] <kwsdatadir> <kwslist.xml> [<dir>]"
  cat <<END

 [options]:
 lowest_score			# value, $lowest_score
 id				# value, "$id"
 default			# value, $default

END
}

. parse_options.sh || exit 1

if $default; then
  if [ $# -lt 2 ]; then
    print_options;
    echo && exit 1
  fi
  dir=$(dirname $kwslist.xml)/media-eval$id
else
  if [ $# -ne 3 ]; then
    print_options
    echo && exit 1
  fi
  dir=$3
fi
kwsdatadir=$1
kwslist=$2


[ -d $dir ] || mkdir -p $dir

tlist=$(ls $kwsdatadir/*.tlist.xml)
echo "$0: using $tlist as keyword list"
[ ! -z $tlist ] || \
{ echo "$0: ERROR, tlist is empty !"; exit 1; }
cat $tlist | perl -e 'while(<>){chomp; if(/termid=\"([^\"]+)\"/){print "$1\n";} }'  > $dir/termid.list
term_num=$(wc -l < $dir/termid.list)
if [ -z $term_num ] || [ $term_num -eq 0 ]; then
  echo "$0: ERROR, no term found" && exit 1
fi
cat $kwslist | perl -pe 's/kw/term/g;' | \
source/egs/quesst/rm_dup_term.pl $dir/termid.list > $dir/kwslist.stdlist.xml

./score-TWV-Cnxe.sh  $dir $kwsdatadir   $lowest_score 
