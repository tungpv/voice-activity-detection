#!/bin/bash

. path.sh
. cmd.sh

# set -u
set -o pipefail

# begin options
cmd=run.pl
nj=20
steps=
min_lmwt=10
max_lmwt=10
query_list_name=dev
scoreid=
write_and_score_opts="false 1.0 66.6 0.01 true 0.6 true"
kwseval=/opt/tools/NIST/F4DE-3.2.0/KWSEval/tools/KWSEval/KWSEval.pl
default=false
# end options

echo 
echo LOG: $0 $@
echo

. parse_options.sh || exit 1
function PrintOptions {
  cat <<END

Main [options]:
cmd				# value, "$cmd"
nj				# value, $nj
steps				# value, "$steps", # 1:indexing, 2:search, 3:write&score
min_lmwt			# value, $min_lmwt
max_lmwt			# value, $max_lmwt
query_list_name			# value, "$query_list_name"
scoreid				# value, "$scoreid"
write_and_score_opts		# value, "$write_and_score_opts", # skip_scoring, ntrue_scale, beta, frame_dur, normalize, duptime, remove_dup
kwseval				# value, "$kwseval"
default				# value, $default, # cmd=slurm.pl, steps=1,2,3, just give index dir
END
}
if $default; then
   [ $# -eq 3 ] || { echo "$0: ERROR, argument error"; exit 1; }
  cmd="slurm.pl --exclude=node01,node02"
  lang=$1
  kwsdatadir=$2
  dir=$3
  latdir=$(dirname $dir)
  sdir=$(dirname $latdir)
else
  if [ $# -ne 5 ]; then
    echo 
    echo "Usage: $0 [options] <lang> <kwsdatadir> <sdir> <latdir> <dir>"
    PrintOptions
    echo && exit 1
  fi
  lang=$1
  kwsdatadir=$2
  sdir=$3
  latdir=$4
  dir=$5
fi

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
for lmwt in $(seq $min_lmwt $max_lmwt); do
  indexdir=$dir/$lmwt
  if [ ! -z $step01 ]; then
    echo "$0: making index started @ `date`"
    acwt=$(perl -e "print 1/$lmwt ")
    source/egs/quesst/make_index.sh \
    --cmd "$cmd" --acwt $acwt  \
    --model $sdir/final.mdl \
    $kwsdatadir  $lang $latdir $indexdir  || exit 1
    echo "$0: making index ended @ `date`"
  fi
  resdir=$indexdir/$query_list_name
  if [ ! -z $step02 ]; then
    [ -f $kwsdatadir/keywords.fsts ] || \
    { echo "$0: ERROR, keywords.fsts expected from folder $kwsdatadir, please make it or link it there"; exit 1; }
    echo "$0: searching index started @ `date`"
    steps/search_index.sh --cmd "$cmd" \
    --indices-dir $indexdir $kwsdatadir $resdir || exit 1
    echo "$0: searching index ended @ `date`"
  fi
  if [ ! -z $step03 ]; then
    echo "$0: writing results started @ `date`"
    source/egs/quesst/kws_write_and_score.sh  \
    --write-and-score-opts "$write_and_score_opts" \
    --kwslist $resdir/kwslist.xml \
    --scoredir $resdir/score \
    $kwsdatadir $resdir  &
    echo "$0: writing results ended @ `date`"
  fi
done

echo "$0: Done `date`"
