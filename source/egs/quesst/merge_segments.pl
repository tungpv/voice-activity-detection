#!/usr/bin/perl

use warnings;
use strict;

my $numArgs = scalar @ARGV;
my $cmdName = $0;
$cmdName =~ s/.*\///g;
if ($numArgs != 2) {
  die "\n\ne.g.: $cmdName segments old-bound > new-bound\n\n"
}

my ($segFile, $oldBound) = @ARGV;
my $segHandle;
open($segHandle, "$segFile") or die "ERROR, file $segFile cannot open\n";
my %vocab = ();
while(<$segHandle>) {
  chomp;
  s/[\[\]]//g;
  my @A = split (" ");  
  my $nEle = scalar @A;
  die "ERROR, bad line0 $_\n" if $nEle != 4 && $nEle != 3;
  my $utt = shift @A;
  $vocab{$utt} = \@A;
}
close $segHandle;

my %boundVocab = ();
my $boundHandle;

if ($oldBound eq "-") {
  $boundHandle ="STDIN"
} else {
  open($boundHandle, "$oldBound") or die "ERROR, file $oldBound cannot open\n";
}
my $utt;
while(<$boundHandle>) {
  chomp;
  if(/\[/) {
    my $s = $_;
    $s =~ s/\[/ /;
    if ($s =~ m/\]/) {
      $s =~ s/\]//;
    }
    my @A = split(" ", $s);
    die "ERROR, bad line1 $s\n" if @A != 3;
    $utt = shift @A;
    my @uttA = ();
    push @uttA, \@A;
    $boundVocab{$utt} = \@uttA;
  } elsif (/\]/) {
    my $s = $_;
    $s =~ s/\]//;
    my @A = split(" ", $s);
    die "ERROR, bad line2 $s\n" if @A != 2;
    my $uttArray = $boundVocab{$utt};
    push (@$uttArray, \@A);
  } else {
    my @A = split(" ");
    die "ERROR, bad line3 $_\n" if @A != 2;
    my $uttArray = $boundVocab{$utt};
    push(@$uttArray, \@A);
  }
}
sub Dump {
  my ($utt, $pArray) = @_;
  if(@$pArray == 1) {
    my $pA = $$pArray[0];
    print $utt, " [ ", $$pA[0], " ", "$$pA[1] ]\n";  
  } elsif (@$pArray > 1) {
    for(my $i = 0; $i < @$pArray; $i ++) {
      my $pA = $$pArray[$i];
      if($i == 0) {
        print $utt, " [ ", $$pA[0], " ", "$$pA[1] \n";
      } elsif ($i == @$pArray -1) {
        print "$$pA[0] $$pA[1] ]\n";
      } else {
        print "$$pA[0] $$pA[1]\n";
      }
    }
  } else {
    die "ERROR, no boundary to dump\n";
  } 
}
sub HasIt {
  my ($pArray, $pA) = @_;
  return "true" if @$pArray == 1;
  return "false";
}
foreach $utt (keys %vocab) {
  if(exists $boundVocab{$utt}) {
    my $pArray = $boundVocab{$utt};
    my $pA = $vocab{$utt};
    HasIt($pArray, $pA) eq "true" && next;
    push @$pArray, $pA;
    Dump($utt, $pArray);
  } else {
   my @A = ();
   my $pA = $vocab{$utt};
   push @A, $pA;
   Dump($utt, \@A);
  }
}
