#!/bin/bash

. path.sh
. cmd.sh

# begin options
min_sub_len=5
# end options

. parse_options.sh || exit 1

if [ $# -ne 2 ]; then
  echo
  echo "Usage: $0 <src_keyword_list> <dir>"
cat <<END

[options]:
min_sub_len		# value, $min_sub_len

END
  echo && exit 1
fi

src_keyword_list=$1
dir=$2


[ -d $dir ] || mkdir -p $dir

cat $src_keyword_list | \
perl -e 'while(<STDIN>) { chomp; @A = split(" "); $len = scalar @A;  $vocab{$A[0]} += ($len - 1); $vocab2{$A[0]} ++; }
  foreach $utt (keys %vocab) { $len_avg = $vocab{$utt}/$vocab2{$utt};  
    printf("%s %d %d %d\n", $utt, $vocab{$utt}, $vocab2{$utt}, int($len_avg)); }' | \
sort -k1 > $dir/keyword_avg_len.txt

echo "$0: sub_len=$min_sub_len"
cat $src_keyword_list | \
source/egs/quesst/make_partial_kwlist.pl $min_sub_len > $dir/keywords.int

transcripts-to-fsts ark:$dir/keywords.int ark,t:$dir/keywords.fsts || exit 1
