#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
noise_word=""
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options]  <src-word-dict-dir> <tgt-phone-dict-dir>
 [options]:
 --noise-word                                   # value, "$noise_word"
 [steps]:
 [examples]:
 $0 --noise-word "<noise> <silence> <unk> <v-noise>" \
 kws2016/flp-grapheme/data/local-merge  kws2016/flp-grapheme/exp/mono/phone-nnet5a/phone-dict

END
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

sdictdir=$1
tgtdir=$2

[ -d $tgtdir ] || mkdir -p $tgtdir

for x in nonsilence_phones.txt  optional_silence.txt  \
         silence_phones.txt extra_questions.txt lexicon.txt; do 
  file=$sdictdir/$x
  [ -f $file ] || \
  { echo "ERROR: $0, file $file expected"; exit 1; }
  if [ $x != "lexicon.txt" ]; then
    cp $file $tgtdir/
  fi
done
cat $sdictdir/lexicon.txt | \
perl -e '@Noise = split(/\s+/, shift @ARGV); foreach $w (@Noise) { $vocab{$w} ++; }
  while(<STDIN>) {chomp; @A = split(/\s+/); $w = shift @A;
    if(exists $vocab{$w}) {print "$_\n";}
  }
'  "$noise_word" > $tgtdir/lexicon.txt

cat $sdictdir/nonsilence_phones.txt | \
awk '{print $1,"\t", $1;}' >> $tgtdir/lexicon.txt


