#!/usr/bin/perl
use warnings;
use strict;

my $usage = <<END;

 Duplicate text file with duplicated segments file.
 e.g.:
 cat text  |\\
 $0  duplicated/segments id1 id2 ... > duplicated/text

END

if (@ARGV < 2) {
  die $usage;
}

my $segFile = shift @ARGV;
my @array_id = @ARGV;

my %vocab = ();
open(F, "$segFile") or die "$0: ERROR, file $segFile cannot open\n";
while(<F>) {
  chomp;
  die "$0: ERROR, bad line $_ in file $segFile\n"
  if ! m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/;
  my ($segId, $fileId, $start, $end) = ($1, $2, $3, $4);
  $vocab{$segId} ++;
}
close F;
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  die "$0: ERROR, bad line $_ in STDIN\n"
  if ! m/(\S+)\s+(.*)/;
  my ($utt, $text) = ($1, $2);
  print "$_\n";
  foreach my $id (@array_id) {
    my $utt_new = "$id-$utt";
    if(exists $vocab{$utt_new}) {
      print "$utt_new $text\n";
    }
  }
}
print STDERR "$0: stdin ended\n";


