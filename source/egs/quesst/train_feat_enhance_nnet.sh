#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=run.pl
nj=20
steps=
pretrain_cmd="steps/nnet/pretrain_dbn.sh --copy-feats-tmproot /local/hhx502 --nn-depth 6 --hid-dim 2048 --splice 5"
cmvn_opts="--norm-means=true --norm-vars=true"
delta_opts="--delta-order=2"
num_tgt=22
train_cmd="steps/nnet/train.sh --copy-feats-tmproot /local/hhx502 --learn-rate 0.008"
train_tool="nnet-train-frmshuff --objective-function=mse"
validating_rate=0.1
# end options

function PrintOptions {
cat<<END
[options]:
 --cmd				# value, "$cmd"
 --nj				# value, $nj
 --steps			# value, "$steps", e.g.: 1-pretrain, 2-nnet.init, 3-prepare data, 4-train
 --pretrain-cmd			# value, "$pretrain_cmd"
 --cmvn-opts			# value, "$cmvn_opts"
 --delta-opts			# value, "$delta_opts"
 --num-tgt			# value, $num_tgt
 --train-cmd			# value, "$train_cmd"
 --train-tool			# value, "$train_tool"
 --validating-rate		# value, $validating_rate

[steps]:
1: rbm pretrain
2: make dnn topology
3: prepare data to train
4: train
END
}

. parse_options.sh || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ $# -ne 3 ]; then
  echo
  echo "Usage: $0 [options] <data> <label> <dir>"
  PrintOptions
  echo  && exit 1
fi 

data=$1
label=$2
dir=$3

pretrain_dir=$dir/pretrain_dbn
if [ ! -z $step01 ]; then
  echo "$0: dbn_pretrain started @ `date`"
  $pretrain_cmd ${cmvn_opts:+--cmvn-opts "$cmvn_opts"} ${delta_opts:+--delta-opts "$delta_opts"} \
  $data $pretrain_dir || exit 1
  echo "$0: dnb_pretrain ended @ `date`"
fi
dbn=$(ls $pretrain_dir/*.dbn|sort|tail -1)
feature_transform=$pretrain_dir/final.feature_transform
if [ ! -z $step02 ]; then
  [ -z $dbn ] && { echo "$0: dbn expected"; exit 1; }
  echo "$0: we are using $dbn to initialize the intended neural network"
  feats_tr="ark:copy-feats scp:$data/feats.scp ark:- |"
  if [ ! -z "$cmvn_opts" ]; then
    feats_tr="$feats_tr apply-cmvn $cmvn_opts --utt2spk=ark:$data/utt2spk scp:$data/cmvn.scp ark:- ark:- |"
  fi
  if [ ! -z "$delta_opts" ]; then
    feats_tr="$feats_tr add-deltas $delta_opts ark:- ark:-|"
  fi
  num_fea=$(nnet-forward "nnet-concat $feature_transform $dbn -|" "$feats_tr" ark:- | feat-to-dim ark:- -)
  [ -z $num_fea ] && \
  { echo "$0: Error: Getting nnet input dimension failed !" && exit 1; }
  hid_dim=$(nnet-info  $pretrain_dir/1.dbn 2>/dev/null  | grep -m1 output-dim | awk '{print $2;}')
  [ -z $hid_dim ] && \
  { echo "$0: Error: Getting nnet hidden dim failed !" && exit 1; }
  source/egs/quesst/make_nnet_proto.py  $num_fea $num_tgt 0 $hid_dim >$dir/nnet.proto || exit 1 
  nnet-initialize $dir/nnet.proto $dir/nnet.init.1 2>$dir/nnet.init.log || { cat $dir/nnet.init.log; exit 1; }
  nnet-concat $dbn "nnet-copy --remove-last-components=1 $dir/nnet.init.1 -|" $dir/nnet.init || exit 1
  echo "$0: nnet initialization ended @ `date`"
fi
train=$dir/train
valid=$dir/valid
if [ ! -z $step03 ]; then
  source/egs/quesst/subset_data.sh --random true --subset-time-ratio $validating_rate \
  $data  $valid $train || exit 1
fi

if [ ! -z $step04 ]; then
  echo "$0: training started @ `date`"
  $train_cmd --feature-transform $feature_transform --nnet-init $dir/nnet.init \
  --labels "ark:$label" \
  --train-tool "$train_tool" \
  $train $valid lang "$label" "$label" $dir || exit 1
  echo "$0: training ended @ `date`"
fi



