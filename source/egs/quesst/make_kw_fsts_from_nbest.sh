#!/bin/bash

echo -e "\n## LOG: $0 $@\n";
# set -u;
set -o pipefail

# begin options
steps=
# end options

. path.sh 
. cmd.sh

max_phone_num=1000

. utils/parse_options.sh

if [ $# -ne 4 ]; then
  echo -e "\n## Example: $0 nbestdir lang min_phone_num dir\n"
cat <<END

[options]:
steps			# value, "$steps", 1: make nbest.tra; 2: make keywords.int; 3: make keywords.fsts

END
  echo && exit 1
fi
nbestdir=$1; shift; lang=$1; shift;  min_phone_num=$1; shift;   dir=$1; shift

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=true
  done
fi

if [ ! -f $nbestdir/nbest.1.gz ]; then 
  echo -e "\n## $0: nbest.1.gz expected in $nbestdir `date`\n"; exit 1; 
fi
if [ ! -f $lang/words.txt ]; then
  echo -e "\n## $0: words.txt expected in folder $lang `date`\n"
  exit 1
fi
[ -d $dir ]||mkdir -p $dir;

if [ ! -z $step01 ]; then
  zcat $nbestdir/nbest*.gz | \
  perl -e 'while (<STDIN>) { 
  @A = split(/\s+/); next if(@A ==0); 
  if (@A==1) { $w = $A[0];  
    if($w  =~ /^[\d]+/) { print $s, "\n"; }else { 
      $s=$w; } 
    }else { 
      if($A[2] != 0) {$s .= " $A[2]";} 
    }     }' | utils/int2sym.pl -f 2- $lang/words.txt > $nbestdir/nbest.tra
fi
if [ ! -z $step02 ]; then
  cat $nbestdir/nbest.tra | \
  perl -pe 's/\[[^\[]+\]//g' | \
  perl -pe 's/<[^<]+>//g' | \
  utils/sym2int.pl -f 2- $lang/words.txt | \
  perl -e ' ($mlen, $xlen) = @ARGV; die "min_len=$mlen, max_len=$xlen\n" if ($xlen <= $mlen);   while(<STDIN>) {chomp;  @A = split(/\s+/);  if(@A > $mlen && @A < $xlen ) {
    $A[0] =~ s/-[\d]+$//g; @B = split (/_/,$A[0]); if(@B == 4) { $A[0] =~ s/_[^_]+$//;  }   print join " ", @A, "\n";
  } } ' $min_phone_num  $max_phone_num | \
  perl -e 'while(<STDIN>){chomp; if( not exists $vocab{$_}){ $vocab{$_}++; print "$_\n"; } }'  > $dir/keywords.int || exit 1

  cut -d' ' -f2- $dir/keywords.int  | source/egs/quesst/count_dup_kw.pl  | sort -k1nr > $dir/dup_query_count.txt

  echo -e "\n## $0: check $dir/keywords.int `date`\n";
fi
if [ ! -z $step03 ]; then
  transcripts-to-fsts ark:$dir/keywords.int ark,t:$dir/keywords.fsts || exit 1
fi

echo -e "\n## $0: check $dir/keywords.fsts `date`\n";

