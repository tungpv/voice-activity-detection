#!/bin/bash

. path.sh
. cmd.sh

# begin options
rttm_file=
keywords_fsts=
# end options

echo
echo "LOG: $0 $@"
echo

. parse_options.sh
if [ $# -ne 4 ]; then
  echo
  echo "Usage: $0 [options] <data> <ecf_xml> <kwlist_xml> <dir>"

  echo "main [options]"
  cat <<END
  --rttm-file 			# value "$rttm_file"
  --keywords-fsts		# value "$keywords_fsts"

END
  echo && exit 1
fi

data=$1
ecf_xml=$2
kwlist_xml=$3
dir=$4

[ -d $dir ] || mkdir -p $dir
[ -f $data/segments ] || \
{ echo "$0: ERROR, segments file expected from data $data"; exit 1; }

cat $ecf_xml | \
perl -e 'while(<STDIN>) { chomp; if(/^<\?/){ next; } 
  elsif (/^<ecf/) {
    @A = split(/\s+/);  splice(@A, 2,0, "language=\"multiple\""); print join " ", @A, "\n"; next;
  }
  print $_,"\n";
}'  > $dir/ecf.xml
{
  fpath=$(cd $(dirname $ecf_xml); echo -n $(pwd)/$(basename $ecf_xml))
  (cd $dir; x=$(basename $fpath); [ -f $x ] && unlink $x; ln -s $fpath)
}
head -2 $dir/ecf.xml | \
  grep -o -E "*duration=\"[0-9]*[\.]*[0-9]*\"" | \
  perl -pe 's/.*=//g; s/\"//g;'  > $dir/duration
  echo -e "\n## $dir dutraion=`cat $dir/duration`\n";

cat $kwlist_xml | \
perl -ne 'chomp;  if(/^<termlist/){ s/termlist/kwlist/; @A =split(/\s+/);  splice(@A,3,0, "encoding=\"UTF-8\" compareNormalize=\"\"");  print join " ", @A, "\n"; next; }
    if(/^<term/) { s/(<[^<]+>)//; $m = $1; $m =~ s/termid/kwid/; $m =~ s/term/kw/g; 
      print "  $m\n"; s/(<termtext>[^<]+<\/termtext>)//; $m = $1; $m =~ s/termtext/kwtext/g;
      print "    $m\n";  s/<\/term>/<\/kw>/g;  print "  $_\n";  next;
    }
if (/^<\/termlist/) { s/termlist/kwlist/g; print "$_\n"; } ' >$dir/kwlist.xml
{
  fpath=$(cd $(dirname $kwlist_xml); echo -n $(pwd)/$(basename $kwlist_xml))
  (cd $dir; x=$(basename $fpath); [ -f $x ] && unlink $x; ln -s $fpath)
}
[ ! -z $rttm_file ] && \
{
  fpath=$(cd $(dirname $rttm_file); echo -n $(pwd)/$(basename $rttm_file))
  (cd $dir; [ -f rttm ] && unlink  rttm;  ln -s $fpath rttm; x=$(basename $fpath); [ -f $x ] && unlink $x; ln -s $fpath); 
}
[ ! -z $keywords_fsts ] && \
{
  fpath=$(cd $(dirname $keywords_fsts); echo -n $(pwd)/$(basename $keywords_fsts))
  (cd $dir; [ -f keywords.fsts ] && unlink keywords.fsts; ln -s $fpath keywords.fsts)
}
source/egs/quesst/make_utter_id.pl $data/segments >$dir/utter_id
cat $data/segments | awk '{print $1" "$2}' | \
sort | uniq > $dir/utter_map;
fpath=$(cd $data;  echo -n $(pwd)/segments )  
(cd $dir; [ -f segments ] && unlink segments; ln -s $fpath segments)
