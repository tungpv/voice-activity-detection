#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=slurm.pl
nj=40
steps=

dtw_cmd="source/code/compute-feats-distance"
query_bound=
kwsdatadir=
threshold=2.9
# end options
echo
echo "LOG: $(basename $0) $@"
echo 

. parse_options.sh || exit 1

function PrintOptions {
  cat<<END
[options]:
--cmd				# value, "$cmd"
--nj				# value, $nj
--steps				# value, "$steps"
--dtw-cmd			# value, "$dtw_cmd"
--query-bound			# value, "$query_bound"
--kwsdatadir			# value, "$kwsdatadir"
--threshold			# value, $threshold

[steps]:
1: output raw results
2: write out kwslist.xml
END
}

if [ $# -ne 3 ]; then
  echo
  echo "Usage: $(basename $0) [options] <query-data> <tgt-data> <result-dir>"
  PrintOptions
  echo && exit 1
fi

qdata=$1
tdata=$2
resdir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

for f in $qdata/feats.scp $tdata/feats.scp; do
  [ -f $f ] || \
  { echo "ERROR, file $f expected"; exit 1; }
done
sdata=$qdata/split$nj
[[ -d $sdata && $data/feats.scp -ot $sdata ]] || split_data.sh $qdata $nj || exit 1;

if [ ! -z $step01 ]; then
  echo "dtw-search started @ `date`"
  $cmd JOB=1:$nj  $resdir/log/dtw_search.JOB.log \
  $dtw_cmd  ${query_bound:+--src-time-boundary-file=ark:$query_bound} \
  scp:$sdata/JOB/feats.scp scp:$tdata/feats.scp $resdir/results.JOB.txt || exit 1
  echo "ended @ `date`"
fi

if [ ! -z $kwsdatadir ] && [ ! -z $step02 ]; then
  echo "write out normalized results @ `date`"
  source/egs/quesst/dtw_kws_write_and_score.sh --threshold $threshold $kwsdatadir $resdir || exit 1
  echo "ended @ `date`"
fi
