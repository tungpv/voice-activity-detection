#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
left_forward=0.1
right_afterward=0.1
# end options

. parse_options.sh

if [ $# -ne 2 ]; then
  echo
  echo "Usage: $0 [options] <sdata> <data>"
  cat<<END

[options]: 
steps				# value, "$steps"
left_forward			# value, "$left_forward"
right_afterward			# value, "$right_afterward"
 
END
  echo && exit 1
fi

sdata=$1
data=$2

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

for x in segments wav.scp; do
  file=$sdata/$x
  [ -f $file ] || \
  { echo "$0: ERROR, file $x expected from $sdata folder"; exit 1; }
done

[ -d $data ] || mkdir -p $data
cp $sdata/* $data/ 
rm $data/{feats.scp,cmvn.scp,segments} 2>/dev/null

wav_to_dur=$data/wav-to-duration.txt
if [ ! -z $step01 ]; then
  echo "$0: making wav-to-duration started @ `date`"
  wav-to-duration scp:$sdata/wav.scp ark,t:$wav_to_dur || exit 1
fi

if [ ! -z $step02 ]; then
  [ -f $wav_to_dur ] || \
  { echo "$0: ERROR, file $data/wav-to-duration.txt expected"; exit 1; }
  cat $sdata/segments | \
  source/egs/quesst/reset_segment_boundary.pl $left_forward $right_afterward $wav_to_dur \
  > $data/segments
fi

utils/fix_data_dir.sh $data
