#!/usr/bin/perl
use warnings;
use strict;
my $arg_num = scalar @ARGV;
if ($arg_num ne 1) {
  die "\nExample: cat keywords.int | $0 5 > new_keywords.int\n";
}
my ($min_sub_len) = @ARGV;

print STDERR "$0: stdin expected\n";
my @array_utt = ();
my %vocab_index = ();
my @array_kwlist = ();
my $index = 0;
while(<STDIN>) {
  chomp;
  my @A = split(" ");
  my $utt = $A[0];
  shift @A;
  my $str = join (" ", @A);
  my @space = ();
  my $ptr_array = \@space;
  if(exists $vocab_index{$utt}) {
    $ptr_array = $array_kwlist[$vocab_index{$utt}];
  } else {
    $array_kwlist[$index] = \@space; 
    $ptr_array = $array_kwlist[$index];
    $vocab_index{$utt} = $index;
    $array_utt[$index] = $utt;
    $index ++;
  }
  push @$ptr_array, \$str;
  chomp;
}
print STDERR "$0: stdin ended\n";
sub PartialExpand {
  my ($utt, $min_sub_len, $array_one_kw, $vocab) = @_;
  for(my $i = 0; $i < @$array_one_kw; $i++) {
    my $ptr_str = $$array_one_kw[$i];
    my @array = split(" ", $$ptr_str);
    if (scalar @array <= $min_sub_len) {
      my $s = join(" ", @array);
      $$vocab{$s} ++;
    } else {
      my $end = scalar(@array);
      $end -= $min_sub_len;
      # if ($utt eq "quesst2015_dev_0002") {
      #  die "$0: ", join(" ", @array), " end=", $end, "\n";
      # }
      for(my $j = 0; $j <= $end; $j ++) {
        my @sub = ();
        for(my $k = $j; $k < $j +$min_sub_len; $k ++) {
          push @sub, $array[$k];
        }
        my $s = join(" ", @sub);
        $$vocab{$s} ++;
      }
    }
  }
}
my %dup_vocab = ();
for(my $i = 0; $i < scalar @array_kwlist; $i ++) {
  my $array_one_kw = $array_kwlist[$i];
  my $utt = $array_utt[$i];
  my %vocab = (); 
  PartialExpand($utt, $min_sub_len, $array_one_kw, \%vocab);
  foreach my $query (keys %vocab) {
    print $utt, " $query\n";
    $dup_vocab{$query} ++;
  }
}
my $top = 10;
my $i = 0;
print STDERR "top duplicated:\n";
for my $query (sort {$dup_vocab{$b}<=>$dup_vocab{$a}} keys %dup_vocab ) {
  if($i ++ < $top) {
    print STDERR "$query, $dup_vocab{$query}\n";
  }
}

