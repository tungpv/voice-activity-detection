#!/usr/bin/perl
use strict;
use warnings;

print STDERR "stdin expected\n";
my %vocab = ();
my %vocab_to_be_filtered = ();
while(<STDIN>) {
  chomp;
  my @A = split(" ");
  my $s = $A[0];
  if($s =~ /^clean_/){
    $vocab{$s} ++;
    print "$s $s\n";
  } else {
    $vocab_to_be_filtered{$s} ++;
  }
}
print STDERR "stdin ended\n";
foreach my $utt (keys %vocab_to_be_filtered) {
  my $utt_mapped = $utt;
  $utt_mapped =~ s/^[^_]+/clean/g;
  if(exists $vocab{$utt_mapped}) {
    print "$utt $utt_mapped\n";
  } else {
    print STDERR "$0: WARNING, $utt  no utterance to be mapped\n";
  }

}

