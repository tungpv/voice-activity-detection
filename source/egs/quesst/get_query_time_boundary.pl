#!/usr/bin/perl
use warnings;
use strict;

use Getopt::Long;

my $silence_int = 0;
my $min_phone_len = 6;
my $sub_phone_len = 6;
print STDERR "\n\nLOG: $0 ", join(" ", @ARGV), "\n\n";
my $usage = <<END;
[options]:
--silence-int			: index number for silence word (default = $silence_int)
--min-phone-len			: minimum phone length (default = $min_phone_len)
--sub-phone-len			: subsequential phone length (default = $sub_phone_len)
END

GetOptions("silence-int=i" => \$silence_int,
           "min-phone-len=i" => \$min_phone_len,
           "sub-phone-len=i" => \$sub_phone_len);
my $cmdName = $0;
$cmdName =~ s/.*\///g;
if (@ARGV != 4) {
  print STDERR "\n\nExample: cat ctm.txt | $cmdName [options] <segments> <new_segments> <wpartial_bound> <wphone_bound>\n";
  die "$usage\n\n";
}
my ($segFile, $newSegFile, $partialBoundFile, $phoneBoundFile) = @ARGV;

my %vocab = ();
open(SEG, "$segFile") or die "ERROR, file $segFile cannot open\n";
while(<SEG>) {
  chomp;
  s/[\[\]]//g;
  my @A = split(" ");
  die "ERROR, bad line $_ in $segFile\n" if @A != 4 && @A != 3;
  my $segId = shift @A;
  @A == 3 && shift @A;
  $vocab{$segId} = \@A;
}
close SEG;

sub Save2Vocab {
  my ($vocab, $uttId, $phones, $silence_int) = @_;
  my @A = @$phones;
  my $index_last = scalar @A - 1;
  # remove silence phone 
  my $last_phone_info = $A[$index_last];
  pop @A if $$last_phone_info[2] == $silence_int;
  return if @A < 1;
  my $first_phone_info = $A[0];
  shift @A if $$first_phone_info[2] == $silence_int;
  return if @A < 1;
  if(exists $$vocab{$uttId}) {
    my $exist_phones = $$vocab{$uttId};
    if(scalar @$exist_phones < scalar @A) {
      $$vocab{$uttId} = \@A;
    }  
  } else {
    $$vocab{$uttId} = \@A;
  }
}

print STDERR "$cmdName: stdin expected\n";
my $prevUtt = "";
my @uttA = ();
my %uttVocab = ();
while(<STDIN>) {
  chomp;
  my @A = split(" ");
  die "ERROR, bad line $_ " if @A != 5;
  my $uttId = shift @A;
  shift @A; # remove channel id
  # pop @A;   # remove word id
  if($prevUtt ne "" && $prevUtt ne $uttId) {
    my $new_uttId = $prevUtt;
    $new_uttId =~ s/\-[^-]+//;
    die "ERROR, id $new_uttId does not exist\n" if not exists $vocab{$new_uttId};
    Save2Vocab(\%uttVocab, $new_uttId, \@uttA, $silence_int);
    @uttA = ();
  }
  push @uttA, \@A;
  $prevUtt = $uttId;
}
my $new_uttId = $prevUtt;
$new_uttId =~ s/\-[^-]+//;
die "ERROR, id $new_uttId does not exist\n" if not exists $vocab{$new_uttId};
my $ref_array = $vocab{$new_uttId};
Save2Vocab(\%uttVocab, $new_uttId, \@uttA, $silence_int);

sub Write2Full {
  my ($fHandle, $sbounds, $phone_bounds, $uttId) = @_;
  my $phone_num = scalar @$phone_bounds;
  my $first = $$phone_bounds[0];
  my $start_time = $$sbounds[0] + $$first[0];
  my $last = $$phone_bounds[$phone_num - 1];
  my $end_time = $$sbounds[0] + $$last[0] + $$last[1];
  die "ERROR, Write2Full, current time ($end_time = $$sbounds[0] + $$last[0] + $$last[1])", " > ", "time boundary ($$sbounds[1]) in $uttId\n" if $end_time > $$sbounds[1]; 
   my $st = sprintf("%.2f", $start_time - $$sbounds[0]);
   my $end = sprintf("%.2f", $end_time - $$sbounds[0]);
  print $fHandle $uttId, " [ $st  $end ]\n";
}
sub Write2Partial {
  my ($fHandle, $sbounds, $phone_bounds, $uttId, $sub_phone_len, $silence_int) = @_;
  my $tot_len = scalar @$phone_bounds;
  if ($tot_len <= $sub_phone_len) {
    Write2Full($fHandle, $sbounds, $phone_bounds, $uttId);
    return;
  }
  for(my $i = 0; $i <= $tot_len - $sub_phone_len; $i ++) {
    my $first = $$phone_bounds[$i];
    if ($$first[2] == $silence_int) {
      $first = $$phone_bounds[$i+1];
    }
    my $start_time = $$sbounds[0] + $$first[0];
    my $index_last = $i + $sub_phone_len - 1;
    my $last = $$phone_bounds[$index_last];
    if ($$last[2] == $silence_int) {
      $last = $$phone_bounds[$index_last - 1];
    }
    next if ($$last[0] <= $$first[0]);
    my $end_time = $$sbounds[0] + $$last[0] + $$last[1];
    die "ERROR, Write2Partial, current time ($end_time = $$sbounds[0] + $$last[0] + $$last[1])", " > ", "time boundary ($$sbounds[1]) in $uttId\n" if $end_time > $$sbounds[1];
    my $st = sprintf("%.2f", $start_time - $$sbounds[0]);
    my $end = sprintf("%.2f", $end_time - $$sbounds[0]);
    if ($i == 0) {
      print $fHandle $uttId, " [ $st $end\n";
    } elsif ($i != $tot_len - $sub_phone_len){
      print $fHandle " $st $end\n";
    } else {
      print $fHandle " $st $end ]\n";
    } 
  }
}
sub Write2Phone {
  my ($fHandle, $sbounds, $phone_bounds, $uttId) = @_;
  for(my $i = 0; $i < @$phone_bounds; $i ++) {
    my $cur_phone = $$phone_bounds[$i];
    my $start_time = $$cur_phone[0];
    my $end_time = $$cur_phone[0] + $$cur_phone[1];
    my $st = sprintf("%.2f", $start_time);
    my $end = sprintf("%.2f", $end_time);
    if ($i == 0) {
      print $fHandle $uttId, " [ $st $end\n";
    } elsif ($i != @$phone_bounds - 1) {
      print $fHandle  "$st $end\n";
    } else {
      print $fHandle "$st $end ]\n";
    }
  }
}
print STDERR "$cmdName: stdin ended\n";
my ($fFile, $pFile, $phnFile);
open  $fFile, ">$newSegFile" or  die "ERROR, file $newSegFile cannot open\n";
if ($partialBoundFile eq "-") {
  $pFile = *STDOUT;
} else {
  open $pFile, ">$partialBoundFile" or die "ERROR, file $partialBoundFile cannot open\n";
}
open $phnFile, ">$phoneBoundFile" or die "ERROR, file $phoneBoundFile cannot open\n";
foreach  my $uttId (keys %uttVocab) {
  my $super_bounds = $vocab{$uttId};
  my $phones = $uttVocab{$uttId};
  # next if scalar @$phones < $min_phone_len;
  Write2Full($fFile, $super_bounds, $phones, $uttId);
  Write2Partial($pFile, $super_bounds, $phones, $uttId, $sub_phone_len, $silence_int);
  Write2Phone($phnFile, $super_bounds, $phones, $uttId);
}

close $fFile;
close $pFile;
close $phnFile;
