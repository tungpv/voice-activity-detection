#!/bin/bash

. path.sh
. cmd.sh

# begin options
threshold=2.9
rm_old_kwslist=false
skip_scoring=false
kwslist=
scoredir=
kwseval=/opt/tools/NIST/F4DE-3.2.0/KWSEval/tools/KWSEval/KWSEval.pl
# end options
echo
echo LOG: $0 $@
echo

. parse_options.sh

function PrintOptions {
  cat <<END
[options]:
--threshold				# value, $threshold
--rm-old-kwslist			# value, $rm_old_kwslist
--skip-scoring				# value, $skip_scoring
--kwslist				# value, "$kwslist"
--scoredir				# value, "$scoredir"
--kwseval				# value, "$kwseval"

END
}

if [ $# -ne 2 ]; then
  echo 
  echo "Usage: $0 [options] <kwsdatadir> <resdir>"
  PrintOptions
  echo  && exit 1
fi

kwsdatadir=$1
resdir=$2
$rm_old_kwslist && rm $resdir/kwslist.xml 2>/dev/null
[ -z $scoredir ] && scoredir=$resdir/score

[ -d $scoredir ] || mkdir -p $scoredir


set -e 
set -o pipefail 
kwslist=$resdir/kwslist.xml
if [ ! -f $resdir/kwslist.xml ]; then
  source/code/qnorm-normalize  "cat $resdir/result* |" - | \
  source/egs/quesst/write_dtw_kwslist.pl $threshold > $resdir/kwslist.xml
fi

if ! $skip_scoring; then
  source/egs/quesst/kws_score.sh --kwseval $kwseval  $kwsdatadir $kwslist  $scoredir || exit 1
fi

scoredir=$resdir/mediaeval-score
[ -d $scoredir ] || mkdir -p $scoredir
source/egs/quesst/score.sh $kwsdatadir $kwslist  $scoredir || exit 1

echo "Done @ `date` !"
