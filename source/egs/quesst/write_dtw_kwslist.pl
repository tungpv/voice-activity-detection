#!/usr/bin/perl
use strict;
use warnings;
my $cmdName = $0;
$cmdName =~ s/.*\///g;

if(@ARGV < 1) {
  die "\n\nUsage: cat results* | $cmdName 2.7 > kwslist.xml \n\n";
}
my ($threshold) = @ARGV;
sub WriteOut {
  my ($kwslist, $vocab, $kwsBuffer, $threshold) = @_;
  my $item_num = scalar @$kwsBuffer;
  my $queryName;
  print STDERR "test files: ", $item_num, "\n"; 
  for(my $i = 0; $i < $item_num; $i++) {
    my $str = $$kwsBuffer[$i];
    my @A = split(" ", $str);
    $queryName = $A[0];
    if ($i == 0) {
      die "ERROR, result file is not sorted\n" if exists $$vocab{$queryName};
      $$vocab{$queryName} = 1;
      $$kwslist .= "  <detected_kwlist kwid=\"$queryName\" search_time=\"1\" oov_count=\"0\">\n";
    }
    my $score = $A[4];
    my $decision = "NO";
    if($score >= $threshold) {
      $decision = "YES";
    }
    my $dur = $A[3] - $A[2];
    $$kwslist .= "    <kw file=\"$A[1]\" channel=\"1\" tbeg=\"$A[2]\" dur=\"$dur\" score=\"$score\" decision=\"$decision\"/>\n";
  }
  $$kwslist .= "  </detected_kwlist>\n";
}
my %vocab = ();
print STDERR "$cmdName: std expected\n";
my $prevQuery = "";
my $kwslist = "";
my @kwsBuffer = ();

$kwslist .= "<kwslist kwlist_filename=\"\" language=\"multilingual\" system_id=\"\">\n";
while(<STDIN>) {
  chomp;
  my @A = split(" ");
  die "ERROR, bad line: $_\n" if(@A != 5);
  my $curQuery = $A[0];
  if($prevQuery ne "" && $prevQuery ne $curQuery) {
    WriteOut(\$kwslist, \%vocab, \@kwsBuffer, $threshold);
    @kwsBuffer = ();
    $prevQuery = $curQuery;
    push @kwsBuffer, $_; 
  } else {
    $prevQuery = $curQuery;
    push @kwsBuffer, $_;
  }
}
WriteOut(\$kwslist, \%vocab, \@kwsBuffer, $threshold);
$kwslist .= "</kwslist>\n";
print $kwslist;
print STDERR "$cmdName: std ended\n";
