#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
dir=
# end options

. parse_options.sh
function print_options {
cat<<END

 [options]:
 --steps		# value, "$steps"
 --dir			# value, "$dir"

END
}
print_options
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
dir=./data/nd-train80k
clean_wav_scp=./data/train80k-wav/wav.scp
[ -d $dir/tmp1 ] || mkdir -p $dir/tmp1
if [ ! -z $step01 ]; then
  echo "$0: prep wav.scp"
 ls /data/users/dvh508/SWB/D3_no_clipping/*.wav | \
 ./source/egs/quesst/swbd_make_wavscp.pl $clean_wav_scp nd |sort -u> $dir/tmp1/wav.scp
fi
sdir=./data/train80k
if [ ! -z $step02 ]; then
  echo "$0: prep segments"
  cat $sdir/segments | \
  source/egs/quesst/swbd_dup_segments.pl $dir/tmp1/wav.scp nd  > $dir/tmp1/segments1
  paste <(cut -d' ' -f1 $dir/tmp1/segments1| sed 's/sw0//g') \
  <(cut -d' ' -f2- $dir/tmp1/segments1)  > $dir/tmp1/segments
fi
if [ ! -z $step03 ]; then
  echo "$0: prep utt2spk"
  cut -f1 $dir/tmp1/segments | \
  perl -pe 'chomp; @A=split("_"); $_=sprintf("%s %s\n", $_, $A[0]); ' > $dir/tmp1/utt2spk
  utils/utt2spk_to_spk2utt.pl < $dir/tmp1/utt2spk > $dir/tmp1/spk2utt
fi

if [ ! -z $step04 ]; then
  echo "$0: prep text"
  paste <(cut -d' ' -f1 $sdir/text|sed 's/sw0//g') <(cut -d' ' -f2- $sdir/text) | \
  source/egs/quesst/swbd_dup_text.pl $dir/tmp1/segments nd > $dir/tmp1/text
fi

if [ ! -z $step05 ]; then
  utils/fix_data_dir.sh $dir/tmp1
  cp $dir/tmp1/{wav.scp,segments,text,spk2utt,utt2spk}  $dir/
  echo "$0: prep Done for data $dir"
fi
