#!/usr/bin/perl 
use warnings;
use strict;

print STDERR "$0: stdin expected\n";
my %vocab = ();
while(<STDIN>) {
  chomp;
  $vocab{$_} ++;
}
print STDERR "$0: stdin ended\n";

for my $w (keys %vocab){
  if ($vocab{$w} > 1) {
    print $vocab{$w}, " ", $w, "\n";
  }
}
