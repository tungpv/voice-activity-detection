#!/bin/bash

. path.sh
. cmd.sh


# begin options
cmd=run.pl
steps=
nbest_acwt=0.1
nbest_num=1000
nbestdir=
min_phone_num=6
partial_phone_len=6
rttm_file=
kwsource_full=
kwsource_partial=
tgtdata=
ecf_xml=
kwlist_xml=
ground_truth_dir=
default=false
# end options

. parse_options.sh || exit 1

# show options
function print_options {
cat<<END

[options]:

 --cmd				# value, "$cmd"
 --steps			# value, "$steps" 1: make nbest; 2: make full fsts; 3: make partial fsts; 4: kwsource_full; 5: part~
 --nbest-acwt			# value, $nbest_acwt
 --nbest-num			# value, $nbest_num
 --nbestdir			# value, "$nbestdir"
 --min-phone-num		# value, $min_phone_num
 --partial-phone-len		# value, $partial_phone_len
 --rttm-file			# value, "$rttm_file"
 --tgtdata			# value, "$tgtdir"
 --ecf-xml			# value, "$ecf_xml"
 --kwlist-xml			# value, "$kwlist_xml"
 --kwsource-full		# value, "$kwsource_full"
 --kwsource-partial		# value, "$kwsource_partial"
 --ground-truth-dir		# value, "$ground_truth_dir"
 --default			# value, $default, e.g.: cmd=slurm.pl, nbest_num=2000,  tgtdata=../quesst/q1/data/t, ground_truth_dir=../quesst/xx704-quesst15-data/scoring_quesst2015/groundtruth_quesst15_dev
END

}  # show options

if [ $# -ne 2 ]; then
  echo 
  echo "Usage: $0 [options] <lang> <latdir>"
  print_options
  echo && exit 1
fi

lang=$1
latdir=$2
if $default; then
  cmd="slurm.pl --exclude=node01,node02"
  nbest_num=2000
  tgtdata=../quesst/q1/data/t
  ground_truth_dir=../quesst/xx704-quesst15-data/scoring_quesst2015/groundtruth_quesst15_dev
  echo "$0: default:"
  print_options
fi

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

[ -z $nbestdir ] && nbestdir=$latdir/nbest$nbest_num
if [ ! -z $step01 ]; then
  echo "$0: convert lattice to nbest ($nbestdir)  started @ `date`"
  source/egs/quesst/lattice_to_nbest.sh --cmd "$cmd" --N $nbest_num --acwt $nbest_acwt \
  $latdir $nbestdir || exit 1
  echo "$0: convert lattice to nbest ended @ `date`"
fi

dir_fsts_full=$nbestdir/kw_fsts.full
if [ ! -z $step02 ]; then
  echo "$0: make full fsts ($dir_fsts_full) started @ `date`"
  source/egs/quesst/make_kw_fsts_from_nbest.sh --steps 1,2,3 $nbestdir $lang $min_phone_num $dir_fsts_full || exit 1
  echo "$0: make full fsts ended @ `date`"
fi
dir_fsts_partial=$nbestdir/kw_fsts.partial
if [ ! -z $step03 ]; then
  echo "$0: make partial fsts ($dir_fsts_partial) started @ `date`"
  source/egs/quesst/make_partial_fsts.sh --min-sub-len $partial_phone_len \
  $dir_fsts_full/keywords.int $dir_fsts_partial || exit 1 
  echo "$0: make partial fsts started @ `date`"
fi
if [ ! -z $ground_truth_dir ]; then
  rttm_file=$(ls $ground_truth_dir/*rttm)
  [ -z $rttm_file ] && { echo "$0: WARNING, rttm_file is not ready"; }
  ecf_xml=$(ls $ground_truth_dir/*.xml| grep ecf)
  [ -z $ecf_xml ] && { echo "$0: WARNING, ecf_xml file is not ready"; }
  kwlist_xml=$(ls $ground_truth_dir/*.xml|grep tlist)
  [ -z $kwlist_xml ] && { echo "$0: WARNING, kwlist_xml file is not ready"; }
fi
if [ ! -z $step04 ]; then
  echo "$0: prepare kwsource_full started @ `date`"
  [ -z $kwsource_full ] && { echo "$0: ERROR, kwsource_full dir expected"; exit 1; }
  [ -z $ecf_xml ] && { echo "$0: ERROR, ecf.xml file expected"; exit 1; }
  [ -z $kwlist_xml ] && { echo "$0: ERROR, kwlist_xml file expected"; exit 1;  }
  [ -z $dir_fsts_full/keywords.fsts ] && { echo "$0: ERROR, keywords.fsts file expected"; exit 1; }
    source/egs/quesst/prepare_kwsource.sh ${rttm_file:+--rttm-file $rttm_file} \
    --keywords-fsts $dir_fsts_full/keywords.fsts $tgtdata $ecf_xml $kwlist_xml $kwsource_full || exit 1
  echo "$0: prepare kwsource_full ended @ `date`"
fi

if [ ! -z $step05 ]; then
  echo "$0: prepare kwsource_partial started @ `date`"
  [ -z $kwsource_partial ] && { echo "$0: ERROR, kwsource_partial expected"; exit 1; }
  [ -z $ecf_xml ] && { echo "$0: ERROR, ecf_xml file expected"; exit 1; }
  [ -z $kwlist_xml ] && { echo "$0: ERROR, kwlist_xml file expected"; exit 1; }
  [ -z $dir_fsts_partial/keywords.fsts ] && { echo "$0: ERROR, keywords.fsts file expected"; exit 1; }
  source/egs/quesst/prepare_kwsource.sh ${rttm_file:+--rttm-file $rttm_file} \
  --keywords-fsts $dir_fsts_partial/keywords.fsts \
  $tgtdata  $ecf_xml $kwlist_xml $kwsource_partial || exit 1
  echo "$0: prepare kwsource_partial ended @ `date`"
fi
