#!/bin/bash

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd=slurm.pl
nj=40
steps=

oov="<unk>"
validating_rate=0.1
learn_rate=0.008
cmvn_opts="--norm-means=true"
delta_opts=
mono_gauss=2500
srcnnetdir=
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<END
 Usage: $0 [options] <data> <dictsrcdir> <alidir> <tgtdir>
 [options]:
 --cmd                                            # value, "$cmd"
 --nj                                             # value, $nj
 --steps                                          # value, "$steps"
 
 --oov                                            # value, "$oov"
 --validating-rate                                # value, $validating_rate
 --learn-rate                                     # value, $learn_rate
 --cmvn-opts                                      # value, "$cmvn_opts"
 --delta-opts                                     # value, "$delta_opts"
 --mono-gauss                                     # value, $mono_gauss
 --srcnnetdir                                     # value, "$srcnnetdir"

 [steps]:
 1: prepare lang
 2: train mono-phone hmm
 3: convert ali
 4: transfer learning

 [example]:
 $0 --steps 1 --oov "<unk>"  kws2016/flp-grapheme/data/train/plp_pitch \
 kws2016/flp-grapheme/data/local-merge kws2016/flp-grapheme/exp/mono/tri4a/ali_train \
 kws2016/flp-grapheme/exp/mono/phone-nnet5a
END
} 

if [ $# -ne 4 ] || [ -z $steps ]; then
  Usage && exit 1
fi

data=$1
dictsrcdir=$2
alidir=$3
tgtdir=$4
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
lang=$tgtdir/lang
if [ ! -z $step01 ]; then
  echo "## LOG: step01, making lang @ `date`"
  utils/prepare_lang.sh  --position-dependent-phones false \
  $dictsrcdir "$oov" $lang/tmp $lang  || exit 1
  echo "## LOG: step01, done @ `date`"
fi
sdir=$tgtdir/mono0a
if [ ! -z $step02 ]; then
  echo "## LOG: step02, train mono-phone gmm-hmm @ `date`"
  steps/train_mono.sh --cmd "$cmd" --nj $nj ${cmvn_opts:+--cmvn-opts "$cmvn_opts"} \
  --totgauss $mono_gauss  $data  $lang $sdir || exit 1
  echo "## LOG: step02, done @ `date`"
fi
mono_alidir=$sdir/ali_train
if [ ! -z $step03 ]; then
  echo "## LOG: convert ali @ `date`"
  source/egs/swahili/convert-ali.sh $alidir $sdir $mono_alidir
  echo "## LOG: done @ `date`"
fi
nnetdir=$tgtdir/phone-dnn
if [ ! -z $step04 ]; then
  echo "## LOG: step04, transfer learning @ `date`"
  source/egs/swahili/run-cross-train-nnet-v2.sh --steps 1,2 --learn-rate $learn_rate --validating-rate $validating_rate \
  $data  $lang  $srcnnetdir $mono_alidir $nnetdir
  echo "## LOG: step04, done @ `date`"
fi
