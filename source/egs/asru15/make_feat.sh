#!/bin/bash

. path.sh
. cmd.sh


# begin options

nj=20
cmd=run.pl
steps=

# end options

echo
echo "$0 $@"
echo

set -o pipefail

. parse_options.sh

function PrintOptions {
  cmdName=$(echo $0| perl -pe 's/^.*\///g;')
  cat <<END

$cmdName [options]:
cmd					# value, "$cmd"
nj					# value, "$nj"
steps					# value, "$steps"

END
}

PrintOptions;

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=true
  done
fi

if [ $# -ne 3 ]; then
  echo
  echo "usage: $0 <utt.ctm> <word_to_cv_count> <dir>"
  echo
  exit 1
fi
ctmfile=$1
word_to_cv_count=$2
dir=$3

[ -d $dir ] || mkdir -p $dir/{log,data}

source/code/make-feat-from-ctm  ark:$word_to_cv_count  $ctmfile  ark,scp:$dir/data/feats.ark,$dir/feats.scp || exit 1

cut -d' ' -f1 $dir/feats.scp | \
perl -pe 'chomp; $s = sprintf("%s %07d\n",$_, 1); $_ = $s;' > $dir/utt2spk

utils/utt2spk_to_spk2utt.pl < $dir/utt2spk > $dir/spk2utt 

steps/compute_cmvn_stats.sh $dir  $dir/log $dir/data

utils/fix_data_dir.sh $dir



