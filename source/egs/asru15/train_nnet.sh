#!/bin/bash

. path.sh
. cmd.sh


# begin options
validating_rate=0.1
learn_rate=0.0001
splice=1
hid_layer=1
hid_dim=64
# end options

echo 
echo LOG: $0 $@
echo

. parse_options.sh

if [ $# -ne 5 ]; then
  echo 
  echo "Usage: $0 <data> <word_to_cv_count> <ref_ctm:by aligning data> <hyp_utt_ctm:by decoding data> <dir>"
  echo && exit 1
fi

data=$1
word_to_cv_count=$2
ref_ctm=$3
hyp_utt_ctm=$4
dir=$5

[ -f $data/segments ] || \
{ echo "$0: ERROR, segments expected from data $data"; exit 1; }
trndata=$dir/train.data
[ -d $trndata ] || mkdir -p $trndata
source/code/make-sample-from-ctm  "grep -v '<' $ref_ctm|" $data/segments  ark:$word_to_cv_count  "grep -v '<' $hyp_utt_ctm|" ark,scp:$trndata/feats.ark,$trndata/feats.scp \
ark,t:$trndata/label.txt || \
{ echo "$0: ERROR, make-sample-from-ctm failed "; exit 1; }
total_utt_num=$(wc -l < $trndata/feats.scp)
echo "$0: total_utt_num=$total_utt_num"
valid_utt_num=$(perl -e "print int($total_utt_num*$validating_rate);")
echo "$0: valid_utt_num=$valid_utt_num"
valid_dir=$trndata/valid
[ -d $valid_dir ] || mkdir -p $valid_dir
cat $trndata/feats.scp | perl -MList::Util -e 'print List::Util::shuffle <>' | head -$valid_utt_num >$valid_dir/feats.scp
train_dir=$trndata/train
[ -d $train_dir ] || mkdir -p $train_dir
cat $trndata/feats.scp | \
perl -e ' ($sfile) =@ARGV; open(F, "$sfile") or die "file $sfile cannot open\n";
  while(<F>) {chomp; @A = split(" "); $vocab{$A[0]} ++;} close F;
  while(<STDIN>){chomp; @A = split(" "); if(not exists $vocab{$A[0]}) {print "$_\n";} }
 '  $valid_dir/feats.scp  > $train_dir/feats.scp
feat_num=$(feat-to-dim ark:$trndata/feats.ark -)
input_dim=$(perl -e "print $feat_num*($splice*2+1);")
echo "$0: input_dim=$input_dim"

utils/nnet/make_nnet_proto.py  $input_dim 2 $hid_layer $hid_dim > $dir/nnet.proto || exit 1
# we should remove the soft-max layer
nnet-copy --remove-last-components=1 "nnet-initialize $dir/nnet.proto -|" $dir/nnet.init || exit 1
echo "$0: nnet training started @ `date`"
steps/nnet/train.sh --learn-rate $learn_rate --splice $splice \
--nnet-init $dir/nnet.init --labels ark:$trndata/label.txt $train_dir  $valid_dir \
lang ali-train ali-dev  $dir 
echo "$0: nnet training ended @ `date`"
