#!/bin/bash

. path.sh
. cmd.sh

# scheme: source/egs/asru15/score_ctm_subset.sh  ref_ctm hyp_ctm hour_length dir
# begin options

# end options

. parse_options.sh || exit 1

if [ $# -ne 4 ]; then
  echo 
  echo "Usage: $0 <ref_ctm> <hyp_ctm> <hour_length> <dir>"
  echo && exit 1
fi

ref_ctm=$1
hyp_ctm=$2
hour_length=$3
dir=$4

[ -d $dir ] || mkdir -p $dir

source/code/subset-ctm-with-post --inc-step=0.0001 --hour-length=$hour_length \
$hyp_ctm "|sort -k1| grep -v '<' >$dir/hyp_ctm" || exit 1

source/code/filter-ctm $dir/hyp_ctm $ref_ctm  "|sort -k1 | grep -v '<' >$dir/ref_ctm" || exit 1


# /opt/tools/NIST/sctk-2.4.8/bin/sclite -s -r $dir/ref_ctm ctm -h $dir/hyp_ctm ctm -n hyp -f 0 -D -F -o sum rsum prf dtl sgml -e utf-8 || exit 1
# grep SPKR $dir/hyp.sys
# grep Sum $dir/hyp.sys
source/code/compute-wer-with-time-boundary  $ref_ctm $dir/hyp_ctm

