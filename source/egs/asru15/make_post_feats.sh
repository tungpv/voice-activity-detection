#!/bin/bash

# begin options

nj=4
cmd=run.pl
use_gpu=no

# end options

. path.sh

echo 
echo LOG: $0 $@
echo

. parse_options.sh || exit 1;

data=$1
sdata=$2
nndir=$3

if [ $# -ne 3 ]; then
  echo
  echo "Usage: $0 data sdata nndir"
  echo
  exit 1
fi
[ -d $data ] || mkdir -p $data
cmvn_opts=
delta_opts=
D=$nndir
[ -e $D/norm_vars ] && cmvn_opts="--norm-means=true --norm-vars=$(cat $D/norm_vars)" # Bwd-compatibility,
[ -e $D/cmvn_opts ] && cmvn_opts=$(cat $D/cmvn_opts)
[ -e $D/delta_order ] && delta_opts="--delta-order=$(cat $D/delta_order)" # Bwd-compatibility,
[ -e $D/delta_opts ] && delta_opts=$(cat $D/delta_opts)
#
# Create the feature stream,
feats="ark,s,cs:copy-feats scp:$sdata/feats.scp ark:- |"
# apply-cmvn (optional),
[ ! -z "$cmvn_opts" -a ! -f $sdata/cmvn.scp ] && echo "$0: Missing $sdata/cmvn.scp" && exit 1
[ ! -z "$cmvn_opts" ] && feats="$feats apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/utt2spk scp:$sdata/cmvn.scp ark:- ark:- |"
# add-deltas (optional),
[ ! -z "$delta_opts" ] && feats="$feats add-deltas $delta_opts ark:- ark:- |"
#

nnet=$data/posterior_extractor.nnet
nnet-concat $nndir/final.feature_transform $nndir/final.nnet $nnet 2>$data/feature_extractor.log || exit 1

nnet-forward --use-gpu=$use_gpu $nnet "$feats" ark,t:$data/posterior_feats.ark

