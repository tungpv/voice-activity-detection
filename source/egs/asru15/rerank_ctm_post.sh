#!/bin/bash

. path.sh
. cmd.sh

# begin options

# end options

. parse_options.sh  || exit 1

set -o pipefail

if [ $# -ne 5 ]; then
  echo 
  echo "Usage: $0 <data> <utt_ctm> <word_to_cv_count> <nnetdir> <dir>"
  echo && exit 1
fi

data=$1
utt_ctm=$2
word_to_cv_count=$3
nnetdir=$4
dir=$5

for f in segments reco2file_and_channel; do
  tgtfile=$data/$f
  [ -f $tgtfile ] || \
  { echo "$0: ERROR, file $tgtfile expected from dir $data"; exit 1; }
done

[ -d ] || mkdir -p $dir
source/egs/asru15/make_feat.sh $utt_ctm $word_to_cv_count $dir || exit 1

[ -f $nnetdir/final.nnet ] || \
{ echo "$0: final.nnet expected from dir $nnetdir"; exit 1; }

source/egs/asru15/make_post_feats.sh $dir $dir $nnetdir || \
{ echo "$0: ERROR, make_post_feats.sh failed to make posterior_feats.ark, check $dir/feature_extractor.log"; exit 1; }

source/code/rerank-ctm-post $utt_ctm ark:$dir/posterior_feats.ark $dir/reranked.utt.ctm  || \
{ echo "$0: ERROR, rerank-ctm-post failed"; exit 1; }

cat $dir/reranked.utt.ctm | \
utils/convert_ctm.pl $data/segments $data/reco2file_and_channel > $dir/reranked.ctm  || \
{ echo "$0: ERROR, convert_ctm failed"; exit 1; }


exit 0
