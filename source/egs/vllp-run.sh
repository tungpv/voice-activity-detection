#!/bin/bash

. path.sh
. cmd.sh


# begin options

cmd=run.pl
nj=20
grapheme_prepare_babel_data=false
train_gmm_hmm=false
fmllr_test=false
flp=false
sdata=/data/users/hhx502/openkws15/v204a/BABEL_OP1_204/conversational
lang=conf/lang/204-tamil-limitedLP.official.conf

third_lda_mllt_train=false
third_lda_mllt_test=false
vlp_train=false
rdir=
manual_prepare_babel_data=false
romanized=
run_sgmm2=false
makeplp_feat=false
steps=

# end options


. parse_options.sh

cat <<END

$0 main options:

cmd                              # value, "$cmd"
nj				 # value, $nj
grapheme_prepare_babel_data      # value, $grapheme_prepare_babel_data
train_gmm_hmm                    # value, $train_gmm_hmm
fmllr_test                       # value, $fmllr_test
flp                              # value, $flp
sdata                            # value, $sdata
data                             # value, $data
exp                              # value, $exp
third_lda_mllt_train             # value, $third_lda_mllt_train
third_lda_mllt_test              # value, $third_lda_mllt_test
vlp_train                        # value, $vlp_train
rdir                             # value, "$rdir"
manual_prepare_babel_data        # value, $manual_prepare_babel_data
romanized                        # value, $romanized
run_sgmm2                        # value, $run_sgmm2
makeplp_feat			 # value, $makeplp_feat
steps                            # value, "$steps"
END
if $flp; then
  id=flp
else
  id=$(basename $rdir)
fi

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

data=$rdir/data
exp=$rdir/exp

# stmfile=/data/users/hhx502/openkws15/107/IndusDB.20130327/babel107b-v0.7_conv-dev/babel107b-v0.7_conv-dev.stm
stmfile=/data/users/hhx502/openkws15/204/IndusDB.20140402/IARPA-babel204b-v1.1b_conv-dev/IARPA-babel204b-v1.1b_conv-dev.stm
# featdir=/data/users/hhx502/w2015/monoling/viet107/$id/features
featdir=/home/hhx502/w2015/monoling/tami204/$id/feature
# dev_data_dir=/data/users/hhx502/openkws15/107/BABEL_BP_107/conversational/dev
dev_data_dir=/data/users/hhx502/openkws15/204/IARPA-babel204b-v1.1b/BABEL_OP1_204/conversational/dev

if $grapheme_prepare_babel_data; then
  echo "$0: grapheme_prepare_babel_data started @ `date`"
  grapheme_lexicon_file=$(ls $sdata/training/transcription/*.txt | source/babel_make_raw_trans.pl  | source/babel_print_word_list.pl  | sort -u | egrep -v '[<\(-]' | source/babel_split_word.pl | sort -u >$sdata/training/grapheme_lexicon.txt; ls $sdata/training/grapheme_lexicon.txt)
  ilocal/vllp_prepare_babel_data.sh --cmd "$train_cmd" --user-lexicon-file $grapheme_lexicon_file \
   --flp $flp ${stmfile:+--stmfile $stmfile} --featdir $featdir  --dev-data-dir $dev_data_dir \
  $lang $sdata $data $exp
fi
if $manual_prepare_babel_data; then
  echo "$0: manual_prepare_babel_data started @ `date`"
  manual_source_lexicon=flp/data/local/filtered_lexicon.txt
  manual_lexicon_file=$(ls $sdata/training/transcription/*.txt | source/babel_make_raw_trans.pl  |\
  source/babel_print_word_list.pl  | sort -u | egrep -v '<|\(|-' | \
  source/babel_select_manual_lexicon.pl $manual_source_lexicon | \
  sort -u >$sdata/training/manual_lexicon.txt; ls $sdata/training/manual_lexicon.txt)
  ilocal/vllp_prepare_babel_data.sh --cmd "$train_cmd" --user-lexicon-file $manual_lexicon_file \
   --flp $flp ${stmfile:+--stmfile $stmfile} --featdir $featdir  --dev-data-dir $dev_data_dir ${romanized:+--romanized true} \
  $lang $sdata $data $exp

fi
if $train_gmm_hmm; then
  echo "$0: train_gmm_hmm started @ `date`"
  ilocal/train_gmm_hmm.sh --cmd "$train_cmd" --states 2500 --pdfs 36000 --datadir $data  $data/lang $data/train/plp_pitch $exp 
fi
if $makeplp_feat; then
  echo "$0: makeplp_feat started @ `date`"
  for x in train dev; do
    sdata=$rdir/data/$x; data=$sdata/plp_pitch 
    feat=$rdir/exp/feature/$x/plp_pitch;
    [ -d $data ] || mkdir -p $data;     cp $sdata/* $data/
      utils/fix_data_dir.sh $data
      echo "$0: mkeplp_feat: make plp feature for $data"
      steps/make_plp_pitch.sh --cmd "$cmd" --nj $nj $data $feat/log  $feat/data || exit 1
      utils/fix_data_dir.sh $data
      steps/compute_cmvn_stats.sh $data $feat/log $feat/data || exit 1
      utils/fix_data_dir.sh $data
  done
  data=$(dirname $sdata)
fi
if $vlp_train; then
  echo "$0: vlp_train started @ `date`"
  ilocal/train_gmm_hmm.sh --cmd "$cmd" \
  --states 2000 --pdfs 30000 --datadir $data  $data/lang $data/train/plp_pitch $exp 
fi
if $fmllr_test; then
  echo "$0: fmllr_test started @ `date`"
  sdir=$exp/tri4a
  lang=$data/lang
  graph=$sdir/graph
  mkgraph.sh $lang $sdir $graph
  data=$data/dev/plp_pitch
  decode_dir=$sdir/decode_dev
  steps/decode_fmllr.sh --cmd "$cmd" --nj 20 $graph $data $decode_dir || exit 1
  echo "$0: decode_fmllr done @ `date`"
fi

if $third_lda_mllt_train; then
  echo "$0: third_lda_mllt_train started @ `date`"
  lda_mat=../cant101/flp2/exp/tri2a/final.mat
  lda_mat_opts="--ali $exp/tri2a/ali_train --use-lda-mat $lda_mat"
  ilocal/train_gmm_hmm.sh --cmd "$train_cmd" --states 2500 --pdfs 36000 --lda-mat-opts "$lda_mat_opts" \
  --datadir $data --factor 0.2 $data/lang $data/train/plp_pitch $exp 
fi
if $third_lda_mllt_test; then
  echo "$0: third_lda_mllt_test started @ `date`"
  sdir=$exp/tri7b
  lang=$data/lang
  graph=$sdir/graph
  mkgraph.sh $lang $sdir $graph
  data=$data/dev/plp_pitch
  decode_dir=$sdir/decode_dev
  steps/decode_fmllr.sh --cmd "$train_cmd" --nj 40 $graph $data $decode_dir || exit 1
  echo "$0: decode_fmllr done @ `date`"
fi

if $run_sgmm2; then
  echo "$0: run_sgmm2 started @ `date`"
  [ ! -z $rdir ] || \
  { echo "$0: run_sgmm2: ERROR, rdir expected"; exit 1; }
  lang=$data/lang
  ali=$exp/tri4a/ali_train
  ilocal/run_sgmm2.sh  \
  ${step01:+--ubm-train true} \
  ${step02:+--sgmm2mle-train true} \
  ${step03:+--sgmm2mle-test true --sgmm2mle-test-data $data/dev/plp_pitch --sgmm2mle-test-transform $exp/tri4a/decode_dev} \
  $data/train/plp_pitch $lang $ali $exp  
  
fi
