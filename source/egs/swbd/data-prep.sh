#!/bin/bash

. path.sh
. cmd.sh

# begin options
steps=
# end options

. parse_options.sh || exit 1


if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ ! -z $step01 ]; then
  sdata=swbd/data/train 
  tgtdata=swbd/data/train-fisher-format
  [ -d $tgtdata ] || mkdir -p $tgtdata
  cp $sdata/* $tgtdata
  cat $sdata/text | \
  perl -pe 's/\[laughter\]/<noise>/g; s/\[noise\]/<noise>/g;
    s/\[vocalized-noise\]/<noise>/g;' > $tgtdata/text
fi


