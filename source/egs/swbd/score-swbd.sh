#!/bin/bash

. path.sh
. cmd.sh


if [ $# -ne 3 ]; then
  echo
  echo "Usage: $(basename $0) <data> <lang> <decode_dir>"
  echo && exit 1
fi

data=$1
lang=$2
decode_dir=$3

abs_data=$(cd $data; pwd)
abs_lang=$(cd $lang; pwd)
abs_decode_dir=$(cd $decode_dir; pwd)

srcdir=/home/hhx502/w2015/swbd
if [ ! -d $srcdir ]; then
  echo "ERROR, srcdir $srcdir expected" && exit 1
fi

(
 cd $srcdir ;  local/score.sh --cmd slurm.pl  $abs_data $abs_lang $abs_decode_dir 
)

if [ -f source/show-res.sh ]; then
  source/show-res.sh $abs_decode_dir
fi
