#!/bin/bash 

. path.sh
. cmd.sh 

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd='slurm.pl --quiet'
nj=40
steps=
train_stage=-10
get_egs_stage=-10
frames_per_eg=150
minibatch_size=128
num_epochs=4
num_jobs_initial=3
num_jobs_final=16
remove_egs=false
max_param_change=2.0
initial_effective_lrate=0.001
final_effective_lrate=0.0001
experiment_name=tdnn_7l
disabled=false
min_utt_len=5.0    # minimum utterance length to train ivector
xent_regularize=0.1
# end options

. parse_options.sh || exit 1

function Usage {
 cat<<EOF

 Example: $0 --steps 1   --train-stage $train_stage \
 --experiment-name $experiment_name \
 --xent-regularize $xent_regularize \
 --get-egs-stage $get_egs_stage \
 --frames-per-eg $frames_per_eg \
 --minibatch-size $minibatch_size \
 --num-epochs $num_epochs \
 --num-jobs-initial $num_jobs_initial \
 --num-jobs-final $num_jobs_final \
 --remove-egs true \
 --max-param-change $max_param_change \
 --initial-effective-lrate $initial_effective_lrate \
 --final-effective-lrate $final_effective_lrate \
 --cmd 'slurm.pl --quiet --exclude=node01' --nj $nj  /home3/hhx502/w2017/seame-nnet3-feb-06-2017/data \
 /home3/hhx502/w2017/seame-nnet3-feb-06-2017/exp-with-data-augmentation

EOF
}

if [ $# -ne 2 ]; then
  Usage && exit 1
fi

datadir=$1
expdir=$2
steps=$(echo $steps | perl -e '$steps=<STDIN>; if($steps =~ m:(\d+)\-:g){$start = $1; $steps=$start; 
        for($i=$start+1; $i < $start + 10; $i++){$steps .=":$i"; } } print $steps;')
## echo "## LOG ($0): steps=$steps"  && exit 0
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

traindata=$datadir/train-augment-overall
if [ ! -z $step01 ]; then
  echo "## LOG (step01, $0): data augmentation @ `date`"
  srcdata=$datadir/train/mfcc-pitch
  tgtdata=$datadir/train-augment-overall
  origdata=$tgtdata/original
  utils/copy_data_dir.sh $srcdata $origdata
  rm $origdata/{feats.scp,cmvn.scp} 2>/dev/null
  perturb01=$tgtdata/perturb0.9
  utils/perturb_data_dir_speed.sh 0.9 $origdata $perturb01
  perturb02=$tgtdata/perturb1.1
  utils/perturb_data_dir_speed.sh 1.1 $origdata $perturb02
  data=$tgtdata/combined
  utils/combine_data.sh $data  $origdata $perturb01 $perturb02
  utils/validate_data_dir.sh --no-feats $data
  rm -rf $perturb01 $perturb02 $origdata
  
  echo "## LOG (step01, $0): done `date`"
fi

if [ ! -z $step02 ]; then
  echo "## LOG (step02, $0): mfcc-pitch & mfcc-hires extraction @ `date`"
  sdata=$traindata/combined
  data=$sdata/mfcc-pitch feat=$data/feat/mfcc-pitch log=$data/log/mfcc-pitch
  if true; then
    utils/copy_data_dir.sh $sdata $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1
    utils/fix_data_dir.sh $data
  fi
  echo "## done with mfcc-pitch extraction @ `date`"
  data=$sdata/mfcc-hires  feat=$data/feat/mfcc-hires log=$data/log/mfcc-hires
  utils/copy_data_dir.sh $sdata $data
  steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
  --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
  steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
  utils/fix_data_dir.sh $data  
  echo "## LOG (step02, $0): mfcc-pitch & mfcc-hires extraction done @ `date`"
fi
train_mfcc=$traindata/combined/mfcc-pitch
[ -d $expdir ] || mkdir -p $expdir
if [ ! -z $step03 ]; then
  echo "## LOG ($0, step03): gmm training started @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1-7 \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 6000 --pdf-num 100000 \
  --devdata $datadir/dev-sge-dominant/mfcc-pitch \
  $train_mfcc $datadir/lang $expdir  || exit 1
  echo "## LOG ($0, step03): gmm training done @ `date`"
fi

# make subset of data to train ivector
if [ ! -z $step04 ]; then
  for sdata in data/train-augment-overall/combined/{mfcc-pitch,mfcc-hires}; do
    data=data/train-augment-overall/combined-subset-to-train-ivector/$(basename $sdata)
    [ -d $data ] || mkdir -p $data
    echo "## LOG (step04, $0): segments=$sdata/segments"
    cat $sdata/segments | \
    awk -v thresh=$min_utt_len '{ if($4-$3 >= thresh){print $1 ;} }' > $data/utt-list
    subset_data_dir.sh --utt-list $data/utt-list $sdata $data 
  done
fi
# make alignment
sdir=$expdir/tri4a
data=data/train-augment-overall/combined-subset-to-train-ivector/mfcc-pitch
alidir=$sdir/ali-combined-subset-to-train-ivector
if [ ! -z $step05 ]; then
  echo "## LOG (step05, $0): do alignment started @ `date`"
  steps/align_fmllr.sh --nj $nj --cmd "$cmd" \
  $data  data/lang $sdir   $alidir || exit 1
  echo "## LOG (step05, $0): done with doing alignment @ `date`"
fi
data=data/train-augment-overall/combined-subset-to-train-ivector/mfcc-hires
net3dir=$expdir/nnet3
transform_dir=$net3dir/lda-mllt-transform
if [ ! -z $step06 ]; then
  steps/train_lda_mllt.sh --cmd "$cmd"  --num-iters 13 \
  --realign-iters "" \
  --splice-opts "--left-context=3 --right-context=3" \
  5000 10000 $data  data/lang \
  $alidir $transform_dir || exit 1
fi

if [ ! -z $step07 ]; then
  echo "## LOG (step07, $0): tain_diag_ubm started @ `date`"
  steps/online/nnet2/train_diag_ubm.sh --cmd "$cmd" --nj $nj --num-threads 2 \
  --num-frames 500000 $data  512  $transform_dir $net3dir/diag_ubm || exit 1
  echo "## LOG (step07, $0): done @ `date`"
fi
ivector_extractor=$net3dir/extractor
if [ ! -z $step08 ]; then
  echo "## LOG (step08, $0): train ivector extractor started @ `date`"
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$cmd" --nj $nj \
  --num-threads 1 --num-processes 1 \
  $data  $net3dir/diag_ubm $net3dir/extractor || exit 1;
  echo "## LOG (step08, $0): done @ `date`"
fi
train_ivectors=$net3dir/ivectors-train
if [ ! -z $step09 ]; then
  data=data/train-augment-overall/combined/mfcc-hires
  data2=data/train-augment-overall/combined/mfcc-hires-max2
  steps/online/nnet2/copy_data_dir.sh --utts-per-spk-max 2 $data \
  $data2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj \
  $data2 $ivector_extractor $train_ivectors || exit 1;
fi
if [ ! -z $step10 ]; then
  rm $net3dir/.error 2>/dev/null
  for dataName in  dev-man-dominant dev-sge-dominant; do
    data=data/$dataName/mfcc-hires
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj 8 \
    $data $ivector_extractor $net3dir/ivectors-${dataName} || touch exp/nnet3/.error
   done
fi
latdir=$expdir/tri4a/ali-train-lattice
echo "## LOG ($0): latdir=$expdir"
if [ ! -z $step11 ]; then
  echo "## LOG (step11, $0): align lattice started @ `date`"
  nj=$(cat $expdir/tri4a/ali_train/num_jobs) || exit 1;
  data=data/train-augment-overall/combined/mfcc-pitch
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $data \
  data/lang $(dirname $latdir)  $latdir || exit 1
  rm $latdir/fsts.*.gz # save space
  echo "## LOG (step11, $0): done @ `date`"
fi
chain_lang=$expdir/lang-chain
if [ ! -z $step12 ]; then
  rm -rf $chain_lang
  cp -r data/lang $chain_lang
  silphonelist=$(cat $chain_lang/phones/silence.csl) || exit 1;
  nonsilphonelist=$(cat $chain_lang/phones/nonsilence.csl) || exit 1;
  # Use our special topology... note that later on may have to tune this
  # topology.
  steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >$chain_lang/topo
fi

alidir=$expdir/tri4a/ali_train
leftmost_questions_truncate=-1
treedir=$net3dir/chain-tree
if [ ! -z $step13 ]; then
  echo "## LOG ($0, step13): build tree for chain model"
  # Build a tree using our new topology. This is the critically different
  # step compared with other recipes.
  data=data/train-augment-overall/combined/mfcc-pitch
  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
      --leftmost-questions-truncate $leftmost_questions_truncate \
      --context-opts "--context-width=2 --central-position=1" \
      --cmd "$cmd" 7000 $data $chain_lang $alidir $treedir
  echo "## LOG (step13, $0): done with tree building @ `date`"
fi
chaindir=$net3dir/$experiment_name
if [ ! -z $step14 ]; then
  
  num_targets=$(tree-info $treedir/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)

  mkdir -p $chaindir/configs
  cat <<EOF > $chaindir/configs/network.xconfig
  input dim=100 name=ivector
  input dim=40 name=input
  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor
  fixed-affine-layer name=lda input=Append(-1,0,1,ReplaceIndex(ivector, t, 0)) affine-transform-file=$chaindir/configs/lda.mat
  # the first splicing is moved before the lda layer, so no splicing here
  relu-renorm-layer name=tdnn1 dim=625
  relu-renorm-layer name=tdnn2 input=Append(-1,0,1) dim=625
  relu-renorm-layer name=tdnn3 input=Append(-1,0,1) dim=625
  relu-renorm-layer name=tdnn4 input=Append(-3,0,3) dim=625
  relu-renorm-layer name=tdnn5 input=Append(-3,0,3) dim=625
  relu-renorm-layer name=tdnn6 input=Append(-3,0,3) dim=625
  relu-renorm-layer name=tdnn7 input=Append(-3,0,3) dim=625
  ## adding the layers for chain branch
  relu-renorm-layer name=prefinal-chain input=tdnn7 dim=625 target-rms=0.5
  output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5
  # adding the layers for xent branch
  # This block prints the configs for a separate output that will be
  # trained with a cross-entropy objective in the 'chain' models... this
  # has the effect of regularizing the hidden parts of the model.  we use
  # 0.5 / args.xent_regularize as the learning rate factor- the factor of
  # 0.5 / args.xent_regularize is suitable as it means the xent
  # final-layer learns at a rate independent of the regularization
  # constant; and the 0.5 was tuned so as to make the relative progress
  # similar in the xent and regular final layers.
  relu-renorm-layer name=prefinal-xent input=tdnn7 dim=625 target-rms=0.5
  relu-renorm-layer name=prefinal-lowrank-xent input=prefinal-xent dim=64 target-rms=0.5
  output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF
  steps/nnet3/xconfig_to_configs.py --xconfig-file $chaindir/configs/network.xconfig --config-dir $chaindir/configs/  
  echo "## LOG (step14, $0): done with xconfig-file generation @ `date`"
fi
common_egs_dir=
if [ ! -z $step15 ]; then
  echo "## LOG (step15, $0): tdnn training started @ `date`"
  data=data/train-augment-overall/combined/mfcc-hires
  steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$cmd" \
    --feat.online-ivector-dir $net3dir/ivectors-train \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize $xent_regularize \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --egs.dir "$common_egs_dir" \
    --egs.stage $get_egs_stage \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width $frames_per_eg \
    --trainer.num-chunk-per-minibatch $minibatch_size \
    --trainer.frames-per-iter 1500000 \
    --trainer.num-epochs $num_epochs \
    --trainer.optimization.num-jobs-initial $num_jobs_initial \
    --trainer.optimization.num-jobs-final $num_jobs_final \
    --trainer.optimization.initial-effective-lrate $initial_effective_lrate \
    --trainer.optimization.final-effective-lrate $final_effective_lrate \
    --trainer.max-param-change $max_param_change \
    --cleanup.remove-egs $remove_egs \
    --feat-dir $data \
    --tree-dir $treedir \
    --lat-dir $latdir \
    --dir $chaindir  || exit 1;
  echo "## LOG (step15, $0): tdnn done (chaindir='$chaindir') @ `date`"
fi
if [ ! -z $step16 ]; then
  utils/mkgraph.sh --self-loop-scale 1.0 data/lang $chaindir $chaindir/graph
fi

if [ ! -z $step17 ]; then
  for dataName in dev-man-dominant dev-sge-dominant; do
    decode_set=data/$dataName/mfcc-hires
    steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
          --nj 10 --cmd "$cmd" \
          --online-ivector-dir $net3dir/ivectors-${dataName} \
          $chaindir/graph $decode_set $chaindir/decode-${dataName} || exit 1;
  done 
fi
