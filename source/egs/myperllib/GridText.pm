package GridText;
use strict;
use warnings;
use Exporter;
use utf8;

our @ISA =qw (Exporter);
our @EXPORT_OK = qw(ReadGridText GridTextNormalize);
our @EXPORT = qw(ReadGridText  GridTextNormalize);

sub ReadGridText {
  my ($sFile, $vocab, $sLogDoc) = @_;
  my ($start, $end, $text);
  my ($totalText, $emptyText) = (0, 0);
  my $unit = 100000;
  my $averageWordPerSec = 0;
  my $minWordPerSec = 10000000;
  my $maxWordPerSec = 0;
  $$sLogDoc = "";
  my $speaker = "unknown";
  open(F, "$sFile") or die "## ERROR (ReadGridText, ", __LINE__, "): cannot read $sFile\n";
  while(<F>) {
    chomp;
    if(/name\s+=\s+\"(\S*)\"/) {
      $speaker = lc $1;
      $speaker = "unknown" if($speaker eq '' or $speaker eq "words");
      $speaker =~ s/([_\-])?speaker([_\-])?//;
     #  print STDERR "speaker=$1\n";
    }
    if(/xmin\s+=\s+(\S+)/) {
      $start = $1;
    } elsif(/xmax\s+=\s+(\S+)/) {
      $end = $1;
    } elsif (/text\s+=\s+\"(.*)\"/) {
      $text = $1;  my $update_text = lc $text;
      if ($start >= $end) {
        print STDERR "## ERROR (ReadGridText, ", __LINE__, "): start ($start) >= end ($end), text = $text in '$sFile'\n";
        next;
      }
      if($text ne '' && $update_text  !~  /\-empty\-/) {
        my $timeLabel = sprintf("%05d-%05d", $start*$unit, $end*$unit);
        my $start01 = sprintf("%.2f", $start);
        my $end01 = sprintf("%.2f", $end);
        $text = $start01 . ' ' . $end01 . ' ' . $speaker . ' '. $text; 
        $$vocab{$timeLabel} = $text;
        my $wordNum = scalar split(/\s+/, $text);
        my $wordPerSec = sprintf("%.2f", $wordNum / ($end-$start));
        if($wordPerSec < $minWordPerSec) {
          $minWordPerSec = $wordPerSec;
        }
        if($wordPerSec > $maxWordPerSec) {
          $maxWordPerSec = $wordPerSec;
        }
        $averageWordPerSec += $wordPerSec;
      } else {
        $emptyText ++;
      }
      $totalText ++;
    }
  }
  close F;
  if($totalText - $emptyText == 0) {
    $averageWordPerSec = 0;
  } else {
    $averageWordPerSec = sprintf("%.2f", $averageWordPerSec/($totalText - $emptyText));
  }
  $$sLogDoc .= sprintf("## LOG (ReadGridText, " . __LINE__ . "): totalText=$totalText, emptyText=$emptyText, " .
                "minWordPerSec=$minWordPerSec, maxWordPerSec=$maxWordPerSec, averageWordPerSec=$averageWordPerSec\n");
  return 0;
}

sub GridTextNormalize {
  my ($s) = @_;
  $$s = lc $$s;
}

1;
