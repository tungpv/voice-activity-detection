package LocalDict;
use strict;
use warnings;
use Exporter;
use utf8;

our @ISA =qw (Exporter);
our @EXPORT_OK = qw(CheckAndSequence2SequenceMapping MakeCtxTriphone InitDict InitLocalDict InitTransferDict TransferWord InitDictSimple GetOovWord);
our @EXPORT = qw(CheckAndSequence2SequenceMapping MakeCtxTriphone InitDict InitLocalDict InitTransferDict TransferWord InitDictSimple GetOovWord);

sub Push {
  my ($array, $item, $pos) = @_;
  my @A = ($item, $pos);
  push @$array, \@A;
}
sub Pop {
  my ($array, $item, $pos) = @_;
  my $ref = pop @$array;
  $$item = $$ref[0];
  $$pos = $$ref[1];
}
sub CheckAndSequence2SequenceMapping {
  my ($vocab, $inArray, $outArray, $oovPhone, $maxNum) = @_;
  my $i = 0;
  my $phoneNum = scalar @$inArray;
  my @backtrace = ();
  my @output = ();
  my $pos = 0;
  while(1) {
    my $sElem = $$inArray[$i];
    my $tArray = $$vocab{$sElem};
    if ($tArray eq ref {}) {
      $tArray = (keys%$tArray)
    }
    if (not defined $tArray ) {
      $$oovPhone = $sElem;
      return 0;
    }
    push(@output, $$tArray[$pos]); $pos ++;
    Push(\@backtrace, $tArray, $pos);
    $i ++;
    if($i == $phoneNum) {
      my  @tmpA = @output;
      push @$outArray, \@tmpA;
      if(scalar @$outArray >= $maxNum) {
	return 1;
      }
      pop @output; $i --;
      my $array;
      Pop(\@backtrace, \$array, \$pos);
      my $dim = scalar @$array;
      while($pos >= $dim && scalar @backtrace > 0) {
        pop @output; $i --;
        Pop(\@backtrace, \$array, \$pos);
        $dim = scalar @$array;
      }
      last if ($pos >= $dim);
    } else {
      $pos = 0;
    }
  }
  return 1;
}
sub MakeCtxTriphone {
  my ($monoA, $phoneMapVocab, $silPhoneVocab, $triB) = @_;
  @$triB = ();
  my $i = 0;
  while($i < scalar @$monoA) {
    my $monophone = $$phoneMapVocab{$$monoA[$i]};
    defined ($monophone) or die "## ERROR (LocalDict::MakeCtxTriphone, ", __LINE__, "): $monophone not defined\n" ;
    my $triphone;
    if(exists $$silPhoneVocab{$monophone}) {
      $triphone = $monophone;
    } else {
      my $leftPhone = "";
      if ($i > 0 && not exists $$silPhoneVocab{$$monoA[$i-1]}) {
        $leftPhone = $$monoA[$i-1];
      }
      my $rightPhone = "";
      if($i+1 < scalar @$monoA && not exists $$silPhoneVocab{$$monoA[$i+1]}) {
        $rightPhone = $$monoA[$i+1];
      }
      if($leftPhone ne "" && $rightPhone ne "") {
        $triphone = sprintf("%s-%s+%s", $leftPhone, $monophone, $rightPhone);
      } elsif($leftPhone ne "" && $rightPhone eq "") {
        $triphone = sprintf("%s-%s", $leftPhone, $monophone);
      } elsif ($leftPhone eq "" && $rightPhone ne "") {
        $triphone = sprintf("%s+%s", $monophone, $rightPhone);
      } else {
        $triphone = $monophone;
      }
    }
    push(@$triB, $triphone);
  }
  if(scalar @$triB != scalar @$monoA) {
    die "## ERROR (LocalDict::MakeCtxTriphone, ", __LINE__, "): size mismatched\n";
  }
}
sub InsertWord {
  my ($vocab, $word, $pArray) = @_;
  my $pVocab;
  if(not exists $$vocab{$word}) {
    my %v = ();
    $$vocab{$word} = \%v;
    $pVocab = $$vocab{$word};
  } else {
    $pVocab = $$vocab{$word};
  }
  my $pron = sprintf("%s\t%s", $word, join(" ", @$pArray));
  $$pVocab{$pron} ++;
}
sub InitDict {
  my $numArgs = scalar @_;
  if($numArgs != 5) {
    die "## ERROR (LocalDict::InitDict, ", __LINE__, "): InitDict(dictFile, lowercase, wordVocab, wordListVocab, phoneVocab)\n";
  }
  my ($dictFile, $lowercase, $wordVocab, $wordListVocab, $phoneVocab) = @_;
  %$wordVocab = ();
  %$wordListVocab = ();
  %$phoneVocab = ();
  open(F, "$dictFile") or die "## ERROR (LocalDict::InitDict, ", __LINE__, "): cannot read $dictFile\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    my ($word, $pron) = ($1, $2);
    $word = lc $word if($lowercase > 0);
    $$wordListVocab{$word} ++;
    my @A = split(/\s+/, $pron);
    for(my $i = 0; $i < scalar @A; $i ++) {
      $$phoneVocab{$A[$i]} ++;
    }
    InsertWord($wordVocab, $word, \@A);
  }
  close F;
}
sub ReadPhoneList {
  my ($sFile, $vocab) = @_;
  open (F, "$sFile") or die "## ERROR (LocalDict::ReadPhoneList, ", __LINE__, "): ",
  "cannot read $sFile\n";
  while(<F>) {
    chomp; m/(\S+)/g or next; $$vocab{$1} ++;
  } close F;
}
sub InitPhoneMapVocab {
  my ($sFile, $vocab) = @_;
  open(F, "$sFile") or die "## ERROR (LocalDict::InitPhoneMapVocab, ", __LINE__, "): ",
  "Cannot read $sFile\n";
  while(<F>) {
    chomp;
    m/(\S+)/ or next;
    my $rawPhone = $1;
    $$vocab{$rawPhone} = $rawPhone;
    my $singleton = sprintf("%s_S", $rawPhone);
    $$vocab{$singleton} = $rawPhone;
    my $initial = sprintf("%s_B", $rawPhone);
    $$vocab{$initial} = $rawPhone;
    my $intermediate = sprintf("%s_I", $rawPhone);
    $$vocab{$intermediate} = $rawPhone;
    my $ending = sprintf("%s_E", $rawPhone);
    $$vocab{$ending} = $rawPhone;
  } close F;
}
sub InitLocalDict {
  my $numArgs = scalar @_;
  if($numArgs != 5) {
    die "## ERROR: (LocalDict::InitLocalDict, ", __LINE__, "): ",
        "InitLocalDict(dictDir, optionalSilencePhone, silencePhoneVocab, phoneMapVocab, contextPhoneVocab)\n";
  }
  my ($dictDir, $optionalSilencePhone, $silencePhoneVocab, $phoneMapVocab, $contextPhoneVocab) = @_;
  foreach my $x ("optional_silence.txt", "nonsilence_phones.txt", "silence_phones.txt") {
    my $srcFile = "$dictDir/$x";
    if( ! -e $srcFile) {
      die "## ERROR (LocalDict::InitLocalDict, ", __LINE__, "):",
      "file $srcFile expected\n";
    }
  }
  my $srcFile = "$dictDir/optional_silence.txt";
  open (F, "$srcFile") or
  die "## ERROR (LocalDict::InitLocalDict, ", __LINE__, "):",
      "File $srcFile cannot open\n";
  while(<F>) {chomp; m/(\S+)$/ or next; $$optionalSilencePhone = $1; } close F;
  ReadPhoneList("$dictDir/silence_phones.txt", $silencePhoneVocab);
  ReadPhoneList("cat $dictDir/{silence_phones,nonsilence_phones}.txt |", $contextPhoneVocab);
  InitPhoneMapVocab("cat $dictDir/{silence_phones,nonsilence_phones}.txt |", $phoneMapVocab);
}
our %transferDict = ();
sub InitDictPrivate {
  my ($dictFile, $vocab) = @_;
  open(F, "$dictFile") or 
  die "## ERROR (InitTransferDict, ", __LINE__, "): cannot open $dictFile\n";
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    $$vocab{$1} = $2;
  }
  close F;
}
sub InitTransferDict {
  my ($dictFile) = @_;
  my $vocab = \%transferDict;
  InitDictPrivate($dictFile, $vocab);
}
sub TransferWord01 {
  my ($wordA, $vocab, $wordB) = @_;
  my @A = ();
  for(my $i = 0; $i < @$wordA; $i ++) {
    my $word = $$wordA[$i];
    if(exists $$vocab{$word}) {
      $word = $$vocab{$word};
      my @B = split(/\s+/, $word);
      for(my $i = 0; $i < @B; $i ++) {
	push @A, $B[$i];
      }
    } else {
      push @A, $word;
    }  
  }
  @$wordB = @A;
}
sub TransferWord {
  my ($s) = @_;
  my $vocab = \%transferDict;
  my @A = split(/\s+/, $$s);
  TransferWord01(\@A, $vocab, \@A);
  $$s = join(' ', @A);
  $$s =~ s/^ //g;
  $$s =~ s/ $//g;
}
our %dictSimple = ();
sub InitDictSimple {
  my ($dictFile) = @_;
  my $vocab = \%dictSimple;
  InitDictPrivate($dictFile, $vocab);
}
sub GetOovWord {
  my ($s, $aref) = @_;
  my @A = split(/\s+/, $$s);
  @$aref = ();
  for(my $i = 0; $i < @A; $i ++) {
    my $word = $A[$i];
    if(not exists $dictSimple{$word}) {
      push @$aref, $word;
    }
  }
}
1;
