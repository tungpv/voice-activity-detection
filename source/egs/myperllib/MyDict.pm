#!/usr/bin/perl -w
use strict;
use utf8;

package MyDict;
my (%wordVocab, %wordListVocab, %phoneListVocab, %phoneMapVocab, %silencePhoneVocab, %contextPhoneVocab);
my $optionalSilencePhone;
sub new {
  my $class = shift;
  my $self = {
    _dict_dir => shift,
    _lower_case => shift,
  };
  # print STDERR "dict_dir:", $self->{_dict_dir}, "\n";
  bless $self, $class;
  $self->InitLocalDict();
  return $self;
}
# realize polymorphism
sub Init {
  my $class = shift;
  my $self = {
   _lower_case => shift,
  };
  bless $self, $class;
  return $self;
}
sub InsertWord {
  my ($vocab, $word, $pArray) = @_;
  my $pVocab;
  if(not exists $$vocab{$word}) {
    my %v = ();
    $$vocab{$word} = \%v;
    $pVocab = $$vocab{$word};
  } else {
    $pVocab = $$vocab{$word};
  }
  my $pron = sprintf("%s\t%s", $word, join(" ", @$pArray));
  $$pVocab{$pron} ++;
}
sub PrintWordDict {
  foreach my $word (keys%wordVocab) {
    my $pronVocab = $wordVocab{$word};
    foreach my $pron (keys%$pronVocab) {
      print "$pron\n";
    }
  }
}
sub ReadPhoneList {
  my ($sFile, $vocab) = @_;
  open (F, "$sFile") or die "## ERROR (MyDict::ReadPhoneList, ", __LINE__, "): ",
  "cannot read $sFile\n";
  while(<F>) {
    chomp; m/(\S+)/g or next; $$vocab{$1} ++;
  } close F;
}
sub InitPhoneMapVocab {
  my ($sFile, $vocab) = @_;
  open(F, "$sFile") or die "## ERROR (MyDict::InitPhoneMapVocab, ", __LINE__, "): ",
  "Cannot read $sFile\n";
  while(<F>) {
    chomp;
    m/(\S+)/ or next;
    my $rawPhone = $1;
    $$vocab{$rawPhone} = $rawPhone;
    my $singleton = sprintf("%s_S", $rawPhone);
    $$vocab{$singleton} = $rawPhone;
    my $initial = sprintf("%s_B", $rawPhone);
    $$vocab{$initial} = $rawPhone;
    my $intermediate = sprintf("%s_I", $rawPhone);
    $$vocab{$intermediate} = $rawPhone;
    my $ending = sprintf("%s_E", $rawPhone);
    $$vocab{$ending} = $rawPhone;
  } close F;
}
sub InitWordDict {
  my $self = shift;
  my ($sLexFile) = @_;
  open(F, "$sLexFile") or die "## ERROR (MyDict::InitWordDict, ", __LINE__, 
  "): cannot open $sLexFile\n";
  my $lowercase = $self->{_lower_case};
  while(<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    my ($word, $pron) = ($1, $2);
    $word = lc $word if($lowercase > 0);
    $wordListVocab{$word} ++;
    my @A = split(/\s+/, $pron);
    for(my $i = 0; $i < scalar @A; $i ++) {
      $phoneListVocab{$A[$i]} ++;
    }
    InsertWord(\%wordVocab, $word, \@A);
  } close F;
}
sub InitLocalDict {
  my $self = shift;
  my $dictDir = $self->{_dict_dir};
  my $sLexFile = "$dictDir/lexicon.txt";
  $self->InitWordDict($sLexFile);
  my $srcFile = "$dictDir/optional_silence.txt";
  open(F, "$srcFile") or die "## ERROR (MyDict::InitLocalDict, ", __LINE__, "): cannot open $srcFile";
  while(<F>) {chomp; m/(\S+)/ or next; $optionalSilencePhone = $1; } close F;
  ReadPhoneList("$dictDir/silence_phones.txt", \%silencePhoneVocab);
  ReadPhoneList("cat $dictDir/{silence_phones,nonsilence_phones}.txt |", \%contextPhoneVocab);
  InitPhoneMapVocab("cat $dictDir/{silence_phones,nonsilence_phones}.txt |", \%phoneMapVocab);
}
sub GetOptionalSilencePhone {
  return $optionalSilencePhone;
}
sub IsSilencePhone {
  my ($phone) = @_;
  my $rawPhone;
  GetRawPhone($phone, \$rawPhone);
  return 1 if exists $silencePhoneVocab{$rawPhone};
  return 0;
}
sub GetRawPhone {
  my ($phone, $rawPhone) = @_;
  die "## ERROR (MyDict::GetRawPhone, ", __LINE__, "):  unknown $phone" 
  if not exists $phoneMapVocab{$phone};
  $$rawPhone = $phoneMapVocab{$phone};
}
sub IsLeftBiphone {
  my ($phone) = @_;
  my @A = ();
  @A = split(/[\-]/, $phone);
  return 0 if(scalar @A != 2);
  die "## ERROR (MyDict::IsLeftBiphone, ", __LINE__, "): unknown phone $A[0]\n" 
  if not exists $contextPhoneVocab{$A[0]};
  $phone = $A[1];
  return 0 if not exists $contextPhoneVocab{$phone};
  return 1;
}
sub IsRightBiphone {
  my ($phone) = @_;
  my @A = ();
  @A = split(/[\+]/, $phone);
  return 0 if(scalar @A != 2);
  die "## ERROR (MyDict::IsRightBiphone, ", __LINE__, "): unknown phone $A[1]\n" 
  if not exists $contextPhoneVocab{$A[1]};
  $phone = $A[0];
  return 0 if not exists $contextPhoneVocab{$phone};
  return 1;
}
sub IsTriphone {
  my ($triphone) = @_;
  my @A = split(/[\-\+]/, $triphone);
  return 0 if scalar @A != 3;
  for(my $i = 0; $i < @A; $i ++) {
    return 0 if not exists $contextPhoneVocab{$A[$i]};
  }
  return 1;
}
sub GetLeftBiphone {
  my ($phone, $leftBiphone) = @_;
  return 0 if not IsTriphone($phone);
  $phone =~ s/\+.*$//g;
  $$leftBiphone = $phone;
  return 1;
}
sub GetRightBiphone {
  my ($phone, $rightBiphone) = @_;
  return 0 if not IsTriphone($phone);
  $phone =~ s/^\S+\-//g;
  $$rightBiphone = $phone;
  return 1;
}
sub GetMonophone {
  my ($phone, $monophone) = @_;
  $phone =~ s#^.*\-##g;
  $phone =~ s#\+.*$##g;
  die "## ERROR (MyDict::GetMonophone, ", __LINE__, "): unknown $phone\n"
  if not exists $contextPhoneVocab{$phone};
  $$monophone = $phone;
}
sub MakeCtxTriphone {
  my ($monoA, $triB) = @_;
  @$triB = ();
  my $i = 0;
  while($i < scalar @$monoA) {
    my $monophone = $phoneMapVocab{$$monoA[$i]};
    defined ($monophone) or die "## ERROR (MyDict::MakeCtxTriphone, ", __LINE__, "): $monophone not defined\n" ;
    my $triphone;
    if(exists $silencePhoneVocab{$monophone}) {
      $triphone = $monophone;
    } else {
      my $leftPhone = "";
      if ($i > 0 && not exists $silencePhoneVocab{$$monoA[$i-1]}) {
        $leftPhone = $$monoA[$i-1];
      }
      my $rightPhone = "";
      if($i+1 < scalar @$monoA && not exists $silencePhoneVocab{$$monoA[$i+1]}) {
        $rightPhone = $$monoA[$i+1];
      }
      if($leftPhone ne "" && $rightPhone ne "") {
        $triphone = sprintf("%s-%s+%s", $leftPhone, $monophone, $rightPhone);
      } elsif($leftPhone ne "" && $rightPhone eq "") {
        $triphone = sprintf("%s-%s", $leftPhone, $monophone);
      } elsif ($leftPhone eq "" && $rightPhone ne "") {
        $triphone = sprintf("%s+%s", $monophone, $rightPhone);
      } else {
        $triphone = $monophone;
      }
    }
    push(@$triB, $triphone);
  }
  if(scalar @$triB != scalar @$monoA) {
    die "## ERROR (MyDict::MakeCtxTriphone, ", __LINE__, "): size mismatched\n";
  }
}

1;
