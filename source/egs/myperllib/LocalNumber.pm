package LocalNumber;
use strict;
use warnings;
use Exporter;
use utf8;

our @ISA =qw (Exporter);
our @EXPORT_OK = qw(ReadArabicNumberInEnglish ConvertTimeNumber ConvertPercentNumber ConvertOrdinalNumber ConvertFractionalNumber ConvertDollarNumber ConvertTwoBitYear ConvertFourBitYear DecomposeConcatenatedWord ConvertInteger ConvertNumericData ConvertAlphaNumericString ConvertURLAddress ConvertDecimalNumber);
our @EXPORT = qw(ReadArabicNumberInEnglish ConvertTimeNumber ConvertPercentNumber ConvertOrdinalNumber ConvertFractionalNumber ConvertDollarNumber ConvertTwoBitYear ConvertFourBitYear DecomposeConcatenatedWord ConvertInteger ConvertNumericData ConvertAlphaNumericString ConvertURLAddress ConvertDecimalNumber);

our %under_ten_vocab = (
0 => 'zero', 1 => 'one', 2 => 'two',   3 => 'three', 4 => 'four',
  5 => 'five', 6 => 'six', 7 => 'seven', 8 => 'eight', 9 => 'nine',
);
our %under_twenty_vocab = ( 10 => 'ten',
 11 => 'eleven', 12 => 'twelve', 13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
 16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen', 19 => 'nineteen'
);
our %under_hundred_vocab = (
  20 => 'twenty', 30 => 'thirty', 40 => 'forty', 50 => 'fifty',
  60 => 'sixty', 70 => 'seventy', 80 => 'eighty', 90 => 'ninety'
);
our %card_to_ord_vocab = (
 'one' => 'first', 'two' => 'second', 'three' => 'third', 'five' => 'fifth',
 'nine' => 'ninth', 'twelve' => 'twelfth', 'twenty' => 'twentith', 'thirty' => 'thirtieth',
 'forty' => 'fortieth', 'fifty' => 'fiftieth', 'sixty' => 'sixtieth', 'seventy' => 'seventieth',
 'eighty' => 'eightieth', 'ninety' => 'ninetieth' );
sub BitsOfNumber {
  my ($number) = @_;
  if($number < 10) {
    return 1;
  }
  my $x = $number;
  my $bits = 1;
  while($x >= 10) {
    $x /= 10;
    $bits ++;
  }
  return $bits;
}
sub AddUnit {
  my ($bits, $unit, $base) = @_;
  $$unit = "";
  if($bits <= 0) {
    die "## ERROR (AddUnit, ", __LINE__, "): bits ($bits) should be over zero\n";
  }
  if($bits < 3) {
    $$base = 1;
    $$base = 10 if($bits == 2);
    return;
  }
  if($bits == 3) {
    $$unit = 'hundred'; $$base = 100;
    return;
  }
  if(3 < $bits && $bits <= 6) {
    $$unit = 'thousand'; $$base = 1000;
    return;
  }
  if(6 < $bits && $bits <= 9) {
    $$unit = 'million'; $$base = 1000000;
    return;
  }
  die "## ERROR (AddUnit, ", __LINE__, "): too largish data that has $bits bits\n";
}
sub ConvertOneByOne {
  my ($numString, $engString) = @_;
  if($numString =~ /\D/g) {
    die "## ERROR (ConvertOneByOne, ", __LINE__, "): unknown digit $numString\n";
  }
  my @A = split(//, $numString);
  my @B = ();
  for(my $i = 0; $i < @A; $i ++) {
    if(not exists $under_ten_vocab{$A[$i]}) {
      die "## ERROR (ConvertOneByOne, ", __LINE__, "): illegal digit $A[$i]\n";
    }
    push @B, $under_ten_vocab{$A[$i]};
  }
  $$engString = join (" ", @B);
}
sub GetOneBitWord {
  my ($number) = @_;
  die "## ERROR (GetOneBitWord, ", __LINE__, "): number ($number) should be over zero\n" if $number < 0 || $number >= 10;
  my $word = $under_ten_vocab{$number};
  die "## ERROR (GetOneBitWord, ", __LINE__, "): number ($number) has no word\n" if not defined $word;
  return $word;
}
sub GetTwoBitWord {
  my ($number) = @_;
  die "## ERROR (GetTwoBitWord, ", __LINE__, "): number ($number) should be in [10, 99]\n" if $number < 10 || $number >= 100;
  my $word = "";
  if($number < 20) {
    my $s = $under_twenty_vocab{$number};
    if (not defined $s) {
      die "## ERROR (GetTwoBitWord, ", __LINE__, "): no word for number $number\n";
    }
    $word = $s;
  } else {
    my $x = int($number / 10);
    my $r = $number % ($x * 10);
    $x *= 10;
    my $s = $under_hundred_vocab{$x};
    die "## ERROR GetTwoBitWord, ", __LINE__, "): no word for number $x\n" if not defined $s;
    $word = $s;
    if($r != 0) {
       $word .= " " . GetOneBitWord($r);
    }
  }
  return $word;
}
sub ConvertCombined {
  my ($number, $verbatim) = @_;
  my $nBit = BitsOfNumber($number);
  if($nBit == 1) {
   # print STDERR "## DEBUG (", __LINE__, "): number = $number, nBit=$nBit\n";
    my $v = GetOneBitWord($number);
    if($$verbatim eq "") {
      $$verbatim = $v;
    } else {
      $$verbatim  .= " $v";
    }
  } elsif ($nBit == 2) {
    $$verbatim .= " " . GetTwoBitWord($number);
  } else {
    my ($unit, $base);
    AddUnit($nBit, \$unit, \$base);
    # print STDERR "verbatim=$$verbatim, number=$number, nBit=$nBit, unit=$unit, base=$base\n";
    my $x = int($number / $base);
    ConvertCombined($x, $verbatim);
    $$verbatim .= " " . $unit;
    # print STDERR "verbatim=$$verbatim\n";
    my $r = $number % ($x*$base);
    # print STDERR "r=$r\n";
    if($r != 0) {
      ConvertCombined($r, $verbatim);
    }
  }
}
sub ConvertFloatPointNumber {
  my ($number, $verbatim) = @_;
  $number =~ m/(\d*)\.(\d+)$/g;
  my ($integer, $decimal) = ($1, $2);
  my $is = '';
  if($integer eq "" || $integer == 0) {
    $is = 'point';
  } else {
    ConvertCombined($integer, \$is);
    $is .=  ' ' . 'point';
  }
  # $decimal =~ s/[0]+$//g;
  my $ds = '';
  ConvertOneByOne($decimal, \$ds);
  $is .= ' ' . $ds;
  $$verbatim = $is;
}
sub ReadArabicNumberInEnglish {
  my ($number, $one_by_one) = @_;
  my $verbatim = "";
  if($$number =~ m/\d*\.\d+/g) {
    ConvertFloatPointNumber($$number, \$verbatim);
    $$number = $verbatim;
    return;
  }
  if($$number =~ /\D/g) {
    die "## ERROR (ConvertArabicNumber, ", __LINE__, "): illegal digit $$number\n";
  }
  if($one_by_one || $$number =~ /^0/) {
    ConvertOneByOne($$number, \$verbatim);
    $$number = $verbatim;
    return;
  }
  ConvertCombined($$number, \$verbatim);
  $$number = $verbatim;
}

sub ConvertTimeNumber {
  my ($s) = @_;
  my @A = ();
  while($$s =~ /(\d+:\d+)/g) {
    my $timeStr = $1; my $str = $timeStr;
    my ($hour, $minute)= split(/:/, $timeStr);
    ReadArabicNumberInEnglish(\$hour);
    ReadArabicNumberInEnglish(\$minute);
    $timeStr = "$hour $minute";
    $$s =~ s/$str/$timeStr/;
  }
}
sub ConvertPercentNumber {
  my ($s) = @_;
  while($$s =~ /(\d*\.*\d+%)/g) {
    my $number = $1; my $str = $number;
    $number =~ s/%//;
    # print STDERR "$str\n";
    ReadArabicNumberInEnglish(\$number);
    $$s =~ s/$str/$number percent/;
  }
}
sub GetOrdinalNumber {
  my ($number, $add_s) = @_;
  ReadArabicNumberInEnglish($number);
  my @A = split(/\s+/, $$number);
  my $lastIndex = scalar @A - 1;
  my $lastWord = $A[$lastIndex];
  if(exists $card_to_ord_vocab{$lastWord}) {
    $lastWord = $card_to_ord_vocab{$lastWord};
  } else {
    $lastWord .= 'th';
  }
  if($add_s == 1) {
   $lastWord .= 's';
  }
  $A[$lastIndex] = $lastWord;
  $$number = join(" ", @A);
}
sub ExtractOrdinalNumber {
  my ($s, $suffix) = @_;
  while($$s =~ /(\d+$suffix)/g) {
    my $str = $1; my $number = $str; $number =~ s/$suffix//g;
    # die "(", __LINE__, "): number=$number\n";
    GetOrdinalNumber(\$number, 0);
    $$s =~ s/$str/$number/;
  }
}
sub ConvertOrdinalNumber {
  my ($s) = @_;
  ExtractOrdinalNumber($s, 'st');
  ExtractOrdinalNumber($s, 'nd');
  ExtractOrdinalNumber($s, 'th');
}
sub ConvertFractionalNumber {
  my ($s) = @_;
  while($$s =~ /(\d+\/\d+)/g) {
    my $str = $1;
    my @A = split(/\//, $str);
    my $add_s = 0;
    $add_s = 1 if($A[0] > 1);
    ReadArabicNumberInEnglish(\$A[0]);
    if($A[1] == 2) {
      $A[1] = 'half';
      if($add_s > 0) {
        $A[1] = 'halves';
      }
    } else {
      GetOrdinalNumber(\$A[1], $add_s);
    }
    my $fStr = join(" ", @A);
    $$s =~ s/$str/$fStr/;
  }
}
sub ConvertDollarNumber {
  my ($s) = @_;
  while($$s =~ /^(\$\d*[\.]?\d+)\s+/g || $$s =~ /\s+(\$\d*[\.]?\d+)\s+/g ||
        $$s =~ /\s+(\$\d*[\.]?\d+)$/g) {
    my $str = $1; my $number = $str; $number =~ s/\$//;
    my $prefix = '';
    $prefix = 'zero' if ($number =~ /^0/);
    ReadArabicNumberInEnglish(\$number);
    $number = $prefix . ' ' . $number if $prefix ne '';
    # print STDERR "(", __LINE__, "): $str,  $number\n";
    $$s =~ s/\Q$str\E/ $number dollars /;
  }
  while($$s =~ /(\$\d+[,]?\d+([.]?\d+)?)/g) {
    my $str = $1; my $number = $str; $number =~ s/[\$\,]//g;
    ReadArabicNumberInEnglish(\$number);
    # print STDERR "(", __LINE__, "): $str,  $number\n";
    $$s =~ s/\Q$str\E/ $number dollars /;
  }
}
our %plural_map_vocab = ( 'ten' => 'teens', 'twenty' => 'twenties', 'thirty' => 'thirties',
 'forty' => 'forties', 'fifty' => 'fifties', 'sixty' => 'sixties', 'seventy' => 'seventies',
  'eighty' => 'eighties', 'ninety' => 'nineties');
sub RemoveWhiteSpace {
  my ($s) = @_;
  $$s =~ s/^ +//g;
  $$s =~ s/ $//g;
}
sub ConvertTwoBitYear {
  my ($s) = @_;
  while($$s =~ /(\d{2}s)/g) {
    my $str = $1; my $number = $str; $number =~ s/s$//;
    ReadArabicNumberInEnglish(\$number);
    RemoveWhiteSpace(\$number);
    if(exists $plural_map_vocab{$number}) {
      $number = $plural_map_vocab{$number};
    }
    $$s =~ s/$str/$number/;
  }
}
sub ConvertFourBitYear {
  my ($s, $lower, $upper) = @_;
  while ($$s =~ /(\d{4,}s*)/g) {
    my $str = $1; my $number = $1;
    my $suffix = 0; $suffix = 1 if($number =~ /s$/); $number =~ s/s$//;
    if($lower <= $number && $number <= $upper) {
      my $header = int($number / 100); my $x1 = $header;
      if($header < 20) {
        ReadArabicNumberInEnglish(\$header);
      } else {
       $header *= 100;
       ReadArabicNumberInEnglish(\$header);
      }
      my $tail = $number % ($x1 * 100);
      if($tail != 0) {
        ReadArabicNumberInEnglish(\$tail);
        # print "suffix=$suffix, tail=$tail\n";
        RemoveWhiteSpace(\$tail);
        if($suffix > 0 && exists $plural_map_vocab{$tail}) {
          $tail = $plural_map_vocab{$tail};
        }
      } else {
        $tail = '';
      }
      $$s =~ s/$str/$header $tail/;
    }
  }
}
sub IsDigitalString {
  my ($s) = @_;
  return 1 if($s =~ /^\d*\d+$/);
  return 0;
}
sub IsWordString {
  my ($s) = @_;
  return 1 if ($s =~ /^[A-Za-z]+$/);
  return 0;
}
sub CountWordProperty {
  my ($array, $nDigit, $nWord) = @_;
  ($$nDigit, $$nWord) = (0, 0);
  for(my $i = 0; $i < scalar @$array; $i ++) {
    $$nDigit += IsDigitalString($$array[$i]);
    $$nWord += IsWordString($$array[$i]);
  }
}
sub ReadTwoDigit {
  my ($array) = @_;
  my @A = ();
  for(my $i = 0; $i < scalar @$array; $i ++) {
    my $number = $$array[$i];
    my $verbatim = '';
    if($number < 100) {
      ConvertCombined($number, \$verbatim);
    } else {
      ConvertOneByOne($number, \$verbatim);
    }
    push @A, $verbatim;
  }
  @$array = @A;
}
sub ReadTwoPlusDigit {
  my ($array) = @_;
  my @A = ();
  for(my $i = 0; $i < scalar @$array; $i ++) {
    my $number = $$array[$i];
    my $verbatim;
    ConvertOneByOne($number, \$verbatim);
    push @A, $verbatim;
  }
  @$array = @A;
}
sub ReadWordDigitHybrid {
  my ($array) = @_;
  my @A = ();
  for(my $i = 0; $i < scalar @$array; $i ++) {
    my $str = $$array[$i];
    my $ostr = "";
    if(IsDigitalString($str)) {
      if($str =~ /^0/ || $str > 100) {
        ConvertOneByOne($str, \$ostr);
      } else {
        ConvertCombined($str, \$ostr);
      }
    } elsif(IsWordString($str)) {
      $ostr = $str;
    } else {
      die "## ERROR: (ReadWordDigitHybrid, ", __LINE__, "): unknown hybrid ", join(" ", @$array), "\n";
    }
    push @A, $ostr;
  }
  @$array = @A;
}
sub DecomposeConcatenatedWord {
  my ($s) = @_;
  while($$s =~ /(\+*\S+\-\S+)/g) {
    my $str = $1; my $ostr = $1; $ostr =~ s/^\+//;
    # print STDERR "str=$str, ostr=$ostr\n";
    my @A = split('-', $ostr);
    my $nElem = scalar @A;
    my ($nDigit, $nWord );
    CountWordProperty(\@A, \$nDigit, \$nWord);
    if($nDigit == $nElem) {
      if($nDigit == 2) {
        ReadTwoDigit(\@A);
      } else {
        ReadTwoPlusDigit(\@A);
      }
      $ostr = join(" ", @A);
      $$s =~ s/\Q$str\E/$ostr/;
    } elsif($nWord == $nElem) {
      $ostr = join(" ", @A);
      $$s =~ s/$str/$ostr/;
    } else {
      # die "##elem=$nElem, words=$nWord, digits=$nDigit\n";
      if($nWord + $nDigit == $nElem) {
        ReadWordDigitHybrid(\@A);
        $ostr = join(" ", @A);
        $$s =~ s/\Q$str\E/$ostr/;
      }
    }
  }
}
sub ConvertNumericData {
  my ($s) = @_;
  while($$s =~ /^(\d+(,\d+)?)\s+/g || $$s =~ /\s+(\d+(,\d+)?)\s+/g ||
   $$s =~ /\s+(\d+(,\d+)?)$/g || $$s =~ /^(\d+(,\d+)?)$/g) {
    my $ostr = $1; my $tstr = ''; my $str = $ostr;
    if($str =~ /,/ || $str =~ /^0/ || $str >= 100) {
      if($str =~ /^[1-9]0+$/ || $str =~ /,/g) {
        $str =~ s/,//g;
        ConvertCombined($str, \$tstr);
      } else {
        ConvertOneByOne($ostr, \$tstr);
      }
    } elsif ($ostr < 100) {
      ConvertCombined($ostr, \$tstr);
    }
    $$s =~ s/$ostr/$tstr/;
  }
}
sub ConvertAlphaNumericString {
  my ($s) = @_;
  while($$s =~ /^([A-Za-z]+\d+)/g || $$s =~ /\s+([a-zA-Z]+\d+)\s+/g || $$s =~ /\s+([a-zA-Z]+\d+)$/g) {
    my $ostr = $1; $ostr =~ m/([A-Za-z]+)(\d+)/g;
    my ($str1, $str2) = ($1, $2);
    my $tstr = '';
    if($str2 < 100) {
      ConvertCombined($str2, \$tstr);
    } else {
      ConvertOneByOne($str2, \$tstr);
    }
    $tstr = $str1 . ' ' . $tstr;
    # print STDERR "$ostr $tstr\n";
    $$s =~ s/$ostr/$tstr/;
  }
}

sub ConvertURLAddress {
  my ($s) = @_;
  while($$s =~ /^([^\.\s\d]+(?:\.[^\.\s\d]+)+)\s+/g || $$s =~ /\s+([^\.\s\d]+(?:\.[^\.\s\d]+)+)\s+/g ||
        $$s =~ /\s+([^\.\s\d]+(?:\.[^\.\s\d]+)+)$/g) {
    my $ostr = $1;
    if($ostr !~ /\d/ && $ostr ) {
      my @A = split(/\./, $ostr);
      my $tstr = $ostr;  $tstr =~ s/\./ dot /g;
      $$s =~ s/\Q$ostr\E/$tstr/g;
      # print STDERR "$ostr $tstr\n";
    }
  }
}
sub ConvertDecimalNumber {
  my ($s) = @_;
  while($$s =~ /^(\d*\.\d+)\s+/g || $$s =~ /\s+(\d*\.\d+)\s+/g || $$s =~ /\s+(\d*\.\d+)$/g ||
        $$s =~/^(\d*\.\d+)$/g) {
    my $ostr = $1; my $tstr;
    ConvertFloatPointNumber($ostr, \$tstr);
    # print STDERR "$ostr $tstr\n";
    $$s =~ s/\Q$ostr\E/data/;
  }
}

1;
