#!/usr/bin/perl -w
use strict;
use utf8;
use open qw (:std :utf8);
use lib  qw(source/egs/myperllib);
use LocalDict;
use Getopt::Long;

my $test_all = 0;
GetOptions('test_all|test-all' => \$test_all);
if (!$test_all) {
  die "\n$0 --test-all\nwill test all modules\n\n";
}
my (%wordVocab, %wordListVocab, %phoneVocab);
my $dictFile ="/home2/hhx502/sge2017/data/update-lm-nov-09-2016/dict/lexicon.txt";

InitDict($dictFile, 1, \%wordVocab, \%wordListVocab, \%phoneVocab);

if ($test_all) {
  foreach my $word (keys%wordVocab) {
    my $pronA = $wordVocab{$word};
    foreach my $pron (keys%$pronA) {
      print "$pron\n";
    } 
  }
}

my ($dictDir, $optionalSilencePhone, %silencePhoneVocab, %phoneMapVocab, %contextPhoneVocab)
   = ("/home2/hhx502/sge2017/data/update-lm-nov-09-2016/dict", "", (), (), ());
if ($test_all) {
  InitLocalDict($dictDir, \$optionalSilencePhone, \%silencePhoneVocab, \%phoneMapVocab, \%contextPhoneVocab);
}




