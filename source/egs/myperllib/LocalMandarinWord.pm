package LocalMandarinWord;
use strict;
use warnings;
use Exporter;
use utf8;

our @ISA =qw (Exporter);
our @EXPORT_OK = qw(DecomposeWord DecomposeMandarinWord DecomposeUtterance ConvertDitialWord);
our @EXPORT = qw(DecomposeWord DecomposeMandarinWord DecomposeUtterance ConvertDigitalWord);

sub DecomposeWord {
  my ($word, $array) = @_;
  if($word =~ m/\p{Han}+/) {
    my $s = $word;
    my $start = 0; my $tot_len = length($s);
    while($s =~ m/(\p{Han}+)/g) {
      my $ms = $1;  my $len = length($ms);
      my $end = pos($s);
      my $new_start = $end - $len;
      if($new_start > $start) {
        my $sub_s = substr($s, $start, $new_start - $start);
        push @$array, $sub_s;
      }
      $start = $end;
      my @A = split(//, $ms);
      for(my $i = 0; $i < scalar @A; $i ++) {
        push @$array, $A[$i];
      }
    }
    if($start > 0 && $start < $tot_len) {
      my $sub_s = substr($s, $start, $tot_len - $start);
      push @$array, $sub_s;
    }
    return;
  }
  @$array = ();
  push @$array, $word;
  return;
}
sub DecomposeMandarinWord {
  my ($text) = @_;
  my @A = ();
  DecomposeWord($$text, \@A);
  $$text = join(' ', @A);
}
our %digitConvertDict = ('０' => '0', '１'=> '1', '２' => '2', '３' => '3', '４' => '4', '５' => '5', '６' => '6', '７' => '7', '８' => '8',  '９' => '9', '％' => '%', '／' => '/');
sub LowerArabicDigitalWord {
  my ($word) = @_;
  my @A = split(//, $$word);
  for(my $i = 0; $i < @A; $i ++) {
    $A[$i] = $digitConvertDict{$A[$i]} if exists $digitConvertDict{$A[$i]};
  }
  $$word = join('', @A);
}
sub NormalizeWord {
  my ($w) = @_;
  if($$w =~ /\.$/) {
    $$w =~ s/\.$//;
  }
  if($$w =~ /^([a-zA-Z]+)[\\\/]([a-zA-Z]+)$/) {
    $$w =~ s/\Q$1[\\\/]$2\E/$1 $2/;
  }
  $$w =~ s/\-//g;
  if($$w eq '—— ' || $$w eq '·' || $$w eq '•' || $$w eq '\/' ||
     $$w eq '\’' || $$w eq '］' ||
     $$w eq '［' || $$w eq '\＂') {
     $$w = '';
  }
}
sub ConvertDigitalWord {
  my ($text) = @_;
  my @A = split(/\s+/, $$text);
  for(my $i = 0; $i < @A; $i ++) {
    next if ($A[$i] =~ /\p{Han}/);
    LowerArabicDigitalWord(\$A[$i]);
    NormalizeWord(\$A[$i]);
  }
  $$text = join(' ', @A);
}
sub DecomposeUtterance {
  my ($text) = @_;
  $$text =~ s/(。|\—\—\—|，|？|；|\-\-|\,|！|!|[\?;])/\n/g;
  $$text =~ s/(》|《|、|“|」|”|\)|\(|\)|：|「|\:|‘|›|©|（|）|……|】|\"|『)/ /g;
  $$text =~ s/(【|\[|\]|…|(?:\-))/ /g;
}

1;
