#!/bin/bash

. path.sh
. cmd.sh

# begin sub
cmd='slurm.pl --quiet'
nj=120
steps=
numleaves=7000
hidden_dim=1024
train_stage=-10
get_egs_stage=-10
frames_per_eg=150
minibatch_size=128
num_epochs=4
proportional_shrink=0
chainname=chain1024
num_jobs_initial=3
num_jobs_final=16
remove_egs=false
max_param_change=2.0
initial_effective_lrate=0.001
final_effective_lrate=0.0001
xent_regularize=0.1
remove_egs=false
get_egs_stage=-10
common_egs_dir=
# end sub

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 $0 --steps 1 --cmd "$cmd" --nj $nj \
 ../acumen/data/train-merge-sup/fbank-pitch \
 ../acumen/data/lang \
 ../acumen/update-aug-17-with938-50hrs/exp/mpe-nnet \
 ../acumen/update-sept-23-2017

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 4 ]; then
  Example && exit 1
fi
srcdata=$1
lang=$2
srcdir=$3
tgtdir=$4

lang=../acumen/data/lang-update-sept-19-2017-silprob
data=$tgtdir/data/tmp-data1
[ -d $data ] || mkdir -p $data
if [ ! -z $step01 ]; then
  grep -vP 'sge\d+-rev-' /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/train-merge/fbank-pitch-mix431-part/segments > $data/segment01
  utils/subset_data_dir.sh --utt-list $data/segment01 /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/train-merge/fbank-pitch-mix431-part $data
  utils/combine_data.sh   $tgtdir/data/train $data ../acumen/data/train/fbank-pitch
  rm $tgtdir/data/train/{cmvn.scp,feats.scp}
  utils/fix_data_dir.sh $tgtdir/data/train
fi
dictdir=$tgtdir/data/local/dict
[ -d $dictdir ] || mkdir -p $dictdir
if [ ! -z $step02 ]; then
  srcdict=/home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/dict
  ( echo -e "p.m.\tP IH EH M\na.m.\tAE M\nu.s.\tY UW EH S"
    echo -e "usc\tY UW EH S S IH"
    echo -e "dr.\tD AA K T ER"
  ) | \
  cat - <(grep -v '<silence>' $srcdict/lexicon.txt) \
  | sort -u > $dictdir/lexicon.txt
  cp $srcdict/{nonsilence_phones.txt,silence_phones.txt,optional_silence.txt,extra_questions.txt} $dictdir/
  utils/validate_dict_dir.pl $dictdir
fi
lang=$tgtdir/data/lang
if [ ! -z $step03 ]; then
  utils/prepare_lang.sh  $dictdir "<unk>"  $lang/temp $lang || exit 1
fi
if [ ! -z $step04 ]; then
  for sdata in $tgtdir/data/train; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch/data; log=$sdata/feat/mfcc-pitch/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    utils/fix_data_dir.sh $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step04): done with '$data' @ `date`"
    data=$sdata/mfcc-hires feat=$sdata/feat/mfcc-hires/data log=$sdata/feat/mfcc-hires/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    echo "## LOG (step04): done with '$data' @ `date`"
  done
fi
expdir=$tgtdir/exp
train=$tgtdir/data/train
if [ ! -z $step05 ]; then
  echo "## LOG (step05): straining started @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1-5 \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 10000 --pdf-num 200000 \
  $train/mfcc-pitch $lang $expdir  || exit 1
  echo "## LOG (step05): done @ `date`"
fi
lmdir=$tgtdir/data/local/lm4
[ -d $lmdir ] || mkdir -p $lmdir
if [ ! -z $step06 ]; then
  awk '{print $1; }' $dictdir/lexicon.txt | sort -u \
  | gzip -c > $lmdir/vocab.gz
  srclmdir=../acumen/data/local/lm-update-sept-19-2017
  gzip -cd $srclmdir/train-text.gz | \
  perl -ane 'use utf8; use open qw(:std :utf8); print lc $_;' | \
  gzip -c > $lmdir/train-text.gz
  ngram-count -order 4 -kndiscount -interpolate\
  -vocab $lmdir/vocab.gz  -unk -sort -text $lmdir/train-text.gz -lm $lmdir/lm4.gz
fi
if [ ! -z $step07 ]; then
  echo "## LOG (step07): make G from arpa started @ `date`"
  source/egs/fisher-english/arpa2G.sh $lmdir/lm4.gz $lang $lang
  echo "## LOG (step08): done @ `date`"
fi

if [ ! -z $step08 ]; then
  source/egs/acumen/crack-oov-word.py --dump-oov-of-text --word-lexicon-file ../acumen/update-sept-23-2017/data/local/dict/lexicon.txt  --text-file ../acumen/update-sept-23-2017/data/train/mfcc-pitch/text --tgtdir ../acumen/update-sept-23-2017/data/train-oov-test    --oov-thresh-in-utterance 20
fi

if [ ! -z $step09 ]; then
   seltext=../acumen/update-sept-23-2017/data/train-oov-test/text-selected.txt
   utils/subset_data_dir.sh --utt-list $seltext $train/mfcc-pitch $train/mfcc-pitch-subset
   utils/subset_data_dir.sh --utt-list $seltext $train/mfcc-hires $train/mfcc-hires-subset
fi
if [ ! -z $step10 ]; then
  echo "## LOG (step10): straining started @ `date`"
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1-5 \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 10000 --pdf-num 200000 \
  $train/mfcc-pitch-subset $lang $expdir  || exit 1
  echo "## LOG (step10): done @ `date`"
fi
if [ ! -z $step16 ]; then
   echo "## LOG (step16): retrain tri4a started @ `date`"
    steps/train_sat.sh --cmd 'slurm.pl --exclude=node02  --quiet' 10000 200000 ../acumen/update-sept-23-2017/data/train/mfcc-pitch-subset ../acumen/update-sept-23-2017/data/lang ../acumen/update-sept-23-2017/exp/tri3a/ali_train ../acumen/update-sept-23-2017/exp/tri4a || exit 1
   echo "## LOG (step16): done with tri4a @ `date`"
fi
train_mfcc_pitch=../acumen/update-sept-23-2017/data/train/mfcc-pitch-subset
lang=../acumen/update-sept-23-2017/data/lang
expdir=../acumen/update-sept-23-2017/exp
if [ ! -z $step15 ]; then
  echo "## LOG (step15): started @ `date`"
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj \
  $train_mfcc_pitch $lang $expdir/tri4a  $expdir/tri4a/ali_train || exit 1
  echo "## LOG (step15): done with align_fmllr @ `date`"
fi
srcdir_gmm=$expdir/tri4a
graph_gmm=$srcdir_gmm/graph-4g
if [ ! -z $step11 ]; then
   echo "## LOG (step11): making graph started @ `date`"
  srun --quiet --nodelist=node06 -o ${graph_gmm}.log \
  utils/mkgraph.sh  $lang $srcdir_gmm $graph_gmm || exit 1
  echo "## LOG (step11): done with '$graph' @ `date`"
fi
dev_mfcc_pitch=/home2/hhx502/sge2017/data/fstd-demo-updated/16k/mfcc-pitch
dev_name=fstd-demo-updated
if [ ! -z $step12 ]; then
  echo "## LOG (step12): decoding started @ `date`"
  steps/decode_fmllr.sh --cmd "slurm.pl --quiet --nodelist=node06" --nj 3 \
  $graph_gmm $dev_mfcc_pitch  $srcdir_gmm/decode-${dev_name} || exit 1
  echo "## LOG (step12): decoding ended @ `date`"
fi
lang_silprob=${lang}-silprob
dict_silprob=${dictdir}-silprob
if [ ! -z $step13 ]; then
  echo "## LOG (step13): making silprob dict started @ `date`"
  steps/get_prons.sh --cmd "$cmd"  $train_mfcc_pitch $lang $srcdir_gmm || exit 1
  utils/dict_dir_add_pronprobs.sh --max-normalize true \
  $dictdir $srcdir_gmm/pron_counts_nowb.txt $srcdir_gmm/sil_counts_nowb.txt \
  $srcdir_gmm/pron_bigram_counts_nowb.txt $dict_silprob || exit 1
  echo "## LOG (step13): done @ `date`"
fi
if [ ! -z $step20 ]; then
  utils/prepare_lang.sh $dict_silprob "<unk>" $lang_silprob/tmp $lang_silprob || exit 1;
fi
graph_gmm_silprob=$srcdir_gmm/graph-4g-silprob
if [ ! -z $step21 ]; then
  echo "## LOG (step21): started @ `date`"
  utils/mkgraph.sh  $lang_silprob $srcdir_gmm $graph_gmm || exit 1
  echo "## LOG (step21): ended @ `date`"
fi
if [ ! -z $step22 ]; then
  echo "## LOG (step22): decoding started @ `date`"
  steps/decode_fmllr.sh --cmd "slurm.pl --quiet --nodelist=node06" --nj 3 \
  $graph_gmm_silprob $dev_mfcc_pitch  $srcdir_gmm/decode-${dev_name}-silprob || exit 1
  echo "## LOG (step22): decoding ended @ `date`"
fi

lmdir=$tgtdir/data/local/lm4-updated
[ -d $lmdir ] || mkdir -p $lmdir
if [ ! -z $step23 ]; then
  awk '{print $1; }' ../acumen/update-sept-23-2017/data/local/dict/lexicon.txt | sort -u \
  | gzip -c > $lmdir/vocab.gz
  srclmdir=../acumen/data/local/lm-update-sept-19-2017
  gzip -cd $srclmdir/train-text.gz | \
  perl -ane 'use utf8; use open qw(:std :utf8); print lc $_;' | \
  gzip -c > $lmdir/train-text.gz
  ngram-count -order 4 -kndiscount -interpolate\
  -vocab $lmdir/vocab.gz  -unk -sort -text $lmdir/train-text.gz -lm $lmdir/lm4.gz
fi
if [ ! -z $step24 ]; then
  echo "## LOG (step24): make G from arpa started @ `date`"
  source/egs/fisher-english/arpa2G.sh $lmdir/lm4.gz $lang $lang
  echo "## LOG (step24): done @ `date`"
fi


train_mfcc_hires=../acumen/update-sept-23-2017/data/train/mfcc-hires-subset
train_dir=$(dirname $train_mfcc_hires)
min_utt_len=5.0
if [ ! -z $step30 ]; then
  for sdata in $train_mfcc_pitch $train_mfcc_hires; do
    data=$train_dir/subset-to-train-ivector/$(basename $sdata);
    [ -d $data ] || mkdir -p $data 
    echo "## LOG (step30): segments=$sdata/segments"
    cat $sdata/segments | \
    awk -v thresh=$min_utt_len '{ if($4-$3 >= thresh){print $1 ;} }' > $data/utt-list
    subset_data_dir.sh --utt-list $data/utt-list $sdata $data
    echo "## LOG (step30): subset data '$data' generated"
  done
fi
train_mfcc_pitch_ivector=$train_dir/subset-to-train-ivector/mfcc-pitch-subset
train_mfcc_hires_ivector=$train_dir/subset-to-train-ivector/mfcc-hires-subset
ali_for_ivector=$expdir/tri4a/ali_train-for-ivector
if [ ! -z $step31 ]; then
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj \
  $train_mfcc_pitch_ivector $lang_silprob \
  $expdir/tri4a $ali_for_ivector || exit 1
  echo "## LOG (step31): done with '$ali_for_ivector' @ `date`"
fi
nnetdir=$expdir/tdnn
transform_dir=$nnetdir/lda-mllt-transform-for-ivector-extractor
if [ ! -z $step32 ]; then
  steps/train_lda_mllt.sh --cmd "$cmd"  --num-iters 13 \
  --realign-iters "" \
  --splice-opts "--left-context=3 --right-context=3" \
  8000 80000 $train_mfcc_hires_ivector  $lang_silprob \
  $ali_for_ivector $transform_dir || exit 1
  echo "## LOG (step32): done with '$transform_dir' @ `date`"
fi
if [ ! -z $step33 ]; then
  steps/online/nnet2/train_diag_ubm.sh --cmd "$cmd" --nj $nj \
  --num-threads 2 \
   --num-frames 720000 $train_mfcc_hires_ivector  512  $transform_dir $nnetdir/diag_ubm || exit 1
  echo "## LOG (step33): done with '$nnetdir/diag_ubm' @ `date`"
fi
ivector_extractor=$nnetdir/ivector-extractor
if [ ! -z $step34 ]; then
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$cmd" --nj $nj \
  --num-threads 1 --num-processes 1 \
  $train_mfcc_hires_ivector  $nnetdir/diag_ubm $ivector_extractor || exit 1;
  echo "## LOG (step34): done with '$ivector_extractor' @ `date`"
fi
train_ivectors=$nnetdir/ivector-train
if [ ! -z $step35 ]; then
  data=$train_mfcc_hires
  data2=${train_mfcc_hires}-max2
  utils/data/modify_speaker_info.sh --utts-per-spk-max 2 $data  $data2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data2 $ivector_extractor $train_ivectors || exit 1
fi
dev_ivectors=$nnetdir/ivector-dev
dev_mfcc_hires=/home2/hhx502/sge2017/data/fstd-demo-updated/16k/mfcc-hires
if [ ! -z $step36 ]; then
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj 3 $dev_mfcc_hires \
  $ivector_extractor $dev_ivectors || exit 1   
fi

latdir=$srcdir_gmm/ali_train-lattice
if [ ! -z $step40 ]; then
  echo "## LOG (step40): ali lattice started @ `date`"
  nj=$(cat $srcdir_gmm/num_jobs) ||exit 1
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $train_mfcc_pitch \
  $lang_silprob  $srcdir_gmm  $latdir || exit 1
  echo "## LOG (step40): done @ `date`"
fi
lang_chain=$nnetdir/lang-chain
if [ ! -z $step41 ]; then
  rm -rf $lang_chaing
  cp -r $lang_silprob $lang_chain
  silphonelist=$(cat $lang_chain/phones/silence.csl) || exit 1;
  nonsilphonelist=$(cat $lang_chain/phones/nonsilence.csl) || exit 1;
  # Use our special topology... note that later on may have to tune this
  # topology.
  steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >$lang_chain/topo
fi

leftmost_questions_truncate=-1
treedir=$nnetdir/chain-tree
alidir=$srcdir_gmm/ali_train
numleaves=9000
if [ ! -z $step42 ]; then
  # Build a tree using our new topology. This is the critically different
  # step compared with other recipes.
  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
  --leftmost-questions-truncate $leftmost_questions_truncate \
  --context-opts "--context-width=2 --central-position=1" \
  --cmd "$cmd" $numleaves $train_mfcc_pitch $lang_chain $alidir $treedir
  echo "## LOG (step42): done with tree building '$treedir' @ `date`"
fi

chaindir=$nnetdir/chainname

if [ ! -z $step43 ]; then
  num_targets=$(tree-info $treedir/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)

  mkdir -p $chaindir/configs
  cat <<EOF > $chaindir/configs/network.xconfig
  input dim=100 name=ivector
  input dim=40 name=input
  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor
  fixed-affine-layer name=lda input=Append(-1,0,1,ReplaceIndex(ivector, t, 0)) affine-transform-file=$chaindir/configs/lda.mat
  # the first splicing is moved before the lda layer, so no splicing here
  relu-renorm-layer name=tdnn1 dim=$hidden_dim
  relu-renorm-layer name=tdnn2 input=Append(-1,0,1) dim=$hidden_dim
  relu-renorm-layer name=tdnn3 input=Append(-1,0,1) dim=$hidden_dim
  relu-renorm-layer name=tdnn4 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn5 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn6 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn7 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn8 input=Append(-3,0,3) dim=$hidden_dim
  ## adding the layers for chain branch
  relu-renorm-layer name=prefinal-chain input=tdnn8 dim=$hidden_dim target-rms=0.5
  output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5
  # adding the layers for xent branch
  # This block prints the configs for a separate output that will be
  # trained with a cross-entropy objective in the 'chain' models... this
  # has the effect of regularizing the hidden parts of the model.  we use
  # 0.5 / args.xent_regularize as the learning rate factor- the factor of
  # 0.5 / args.xent_regularize is suitable as it means the xent
  # final-layer learns at a rate independent of the regularization
  # constant; and the 0.5 was tuned so as to make the relative progress
  # similar in the xent and regular final layers.
  relu-renorm-layer name=prefinal-xent input=tdnn8 dim=$hidden_dim target-rms=0.5
  relu-renorm-layer name=prefinal-lowrank-xent input=prefinal-xent dim=64 target-rms=0.5
  output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF
  steps/nnet3/xconfig_to_configs.py --xconfig-file $chaindir/configs/network.xconfig --config-dir $chaindir/configs/  
  echo "## LOG (step43): done with xconfig-file generation ('$chaindir/configs') @ `date`"
fi
if [ ! -z $step44 ]; then
  echo "## LOG (step44): training started @ `date`"
  steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$cmd" \
    --feat.online-ivector-dir $train_ivectors \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize $xent_regularize \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --egs.dir "$common_egs_dir" \
    --egs.stage $get_egs_stage \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width $frames_per_eg \
    --trainer.num-chunk-per-minibatch $minibatch_size \
    --trainer.frames-per-iter 1500000 \
    --trainer.num-epochs $num_epochs \
    --trainer.optimization.num-jobs-initial $num_jobs_initial \
    --trainer.optimization.num-jobs-final $num_jobs_final \
    --trainer.optimization.initial-effective-lrate $initial_effective_lrate \
    --trainer.optimization.final-effective-lrate $final_effective_lrate \
    --trainer.optimization.proportional-shrink $proportional_shrink \
    --trainer.max-param-change $max_param_change \
    --cleanup.remove-egs $remove_egs \
    --feat-dir $train_mfcc_hires \
    --tree-dir $treedir \
    --lat-dir $latdir \
    --dir $chaindir 
  echo "## LOG (step44): tdnn done (chaindir='$chaindir') @ `date`"
fi

graph=$chaindir/graph
if [ ! -z $step45 ]; then
  echo "## LOG (step45): making graph started @ `date`"
  utils/mkgraph.sh --self-loop-scale 1.0 $lang_chain $chaindir $graph || exit 1
  echo "## LOG (step45): done with '$graph' @ `date`"
fi

if [ ! -z $step46 ]; then
  echo "## LOG (step46): decoding started @ `date`"
  steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
  --nj 3 --cmd "$cmd" \
  --online-ivector-dir $nnetdir/$dev_ivectors \
  $graph $dev_mfcc_hires $chaindir/decode-${dev_name} || exit 1;
  echo "## LOG (ste46): done @ `date`"
fi

semidata=../acumen/update-sept-23-2017/data/semi-data200
[ -d $semidata ] || mkdir -p $semidata
if [ ! -z $step50 ]; then
  echo "## LOG (step50): started @ `date`"
  grep '^sge002' ../acumen/data/train-merge-semi/mfcc-hires/segments | \
  awk '{print $1;}' > $semidata/uttlist
  utils/subset_data_dir.sh --utt-list $semidata/uttlist \
  ../acumen/data/train-merge-semi/mfcc-hires $semidata || exit 1
  cat $semidata/text | perl -ane 'use utf8; use open qw(:std :utf8); 
  chomp;  m:^(\S+)\s+(.*):g;
  print $1, " ", lc $2, "\n";'\
  > $semidata/text.1
  
  cp $semidata/text.1 $semidata/text
  utils/fix_data_dir.sh $semidata
  echo "## LOG (step50): done & check '$semidata'"
fi

if [ ! -z $step51 ]; then
  [ -d $semidata/text-normalization ] || mkdir $semidata/text-normalization
  PYTHONIOENCODING=utf-8  source/egs/acumen/crack-oov-word.py \
  --dump-oov-of-text \
  --word-lexicon-file $lang_silprob/words.txt \
  --text-file $semidata/text --tgtdir $semidata/text-normalization
  echo "## LOG (step51): done & check '$semidata/text-normalization'"
fi
ivector_semi=$nnetdir/ivector-semi
semi_mfcc_hires=../acumen/update-sept-23-2017/data/semi-data200
if [ ! -z $step52 ]; then
  echo "## LOG (step52): started @ `date`"
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj \
  $semi_mfcc_hires  $ivector_extractor $ivector_semi || exit 1
  echo "## LOG (step52):  done with '$ivector_semi' @ `date`"
fi
if [ ! -z $step53 ]; then
  echo "## LOG (step53): started @ `date`"
  steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
  --cmd "$cmd" --nj $nj --skip-scoring true \
  --online-ivector-dir $ivector_semi \
  $graph $semi_mfcc_hires $chaindir/decode-semi-data200 || exit 1;
  echo "## LOG (step53): ended  @ `date`"
fi
if [ ! -z $step54 ]; then
  min_lmwt=9
  max_lmwt=9
  wip=0
  dir=$chaindir/decode-semi-data200
  scoring_kaldi=scoring_kaldi
  symtab=$graph/words.txt
  hyp_filtering_cmd=cat
  echo "## LOG (step54): one-best decoding started @ `date`"
  $cmd LMWT=$min_lmwt:$max_lmwt $dir/$scoring_kaldi/penalty_$wip/log/best_path.LMWT.log \
        lattice-scale --inv-acoustic-scale=LMWT "ark:gunzip -c $dir/lat.*.gz|" ark:- \| \
        lattice-add-penalty --word-ins-penalty=$wip ark:- ark:- \| \
        lattice-best-path --word-symbol-table=$symtab ark:- ark,t:- \| \
        utils/int2sym.pl -f 2- $symtab \| \
        $hyp_filtering_cmd '>' $dir/$scoring_kaldi/penalty_$wip/LMWT.txt || exit 1;
  echo "## LOG (step54): done @ `date`"
fi
if false; then
dir=$chaindir/decode-semi-data200/scoring_kaldi/penalty_0
acc_thresh=90
uttlist=$dir/selec${acc_thresh}.txt
selected_mfcc_hires=../acumen/update-sept-23-2017/data/selected${acc_thresh}/mfcc-hires
if [ ! -z $step55 ]; then
  align-text ark:$semi_mfcc_hires/text ark:$dir/9.txt \
  ark,t:$dir/9-alignment.txt
  cat $dir/9-alignment.txt | \
  source/egs/acumen/get-acc-stats-from-text-alignment.pl --thresh=$acc_thresh > $uttlist
  utils/subset_data_dir.sh --utt-list $uttlist $semi_mfcc_hires $selected_mfcc_hires
  cp $dir/9.txt $selected_mfcc_hires/text
  utils/fix_data_dir.sh $selected_mfcc_hires
fi
updatedir=../acumen/update-sept-29-2017
selected_mfcc_pitch=$updatedir/data/selected${acc_thresh}/mfcc-pitch
if [ ! -z $step56 ]; then
  utils/subset_data_dir.sh --utt-list $selected_mfcc_hires/text \
  $semi_mfcc_hires $selected_mfcc_pitch
  cp $selected_mfcc_hires/text $selected_mfcc_pitch/
  utils/fix_data_dir.sh $selected_mfcc_pitch
fi
fi
updatedir=../acumen/update-semi-oct-02-2017
conf=0.7
dur=1.5
hypdata=$updatedir/data/hyp-conf${conf}-dur${dur}
if [ ! -z $step55 ]; then
  [ -d $hypdata ] || mkdir -p $hypdata
  srcdata=../acumen/update-sept-23-2017/data/semi-data200
  source/egs/acumen/select-data-from-ctm-with-confidence-score.py --source-data-dir $srcdata  \
  --ctm-file ../acumen/update-sept-23-2017/exp/tdnn/chainname/decode-semi-data200/ctm-conf-9/score_9/utt.ctm \
  --target-data-dir $hypdata \
  --word-confidence-score-threshold $conf --min-sec-of-selected-segment $dur
  cp $srcdata/wav.scp $hypdata/
  utils/utt2spk_to_spk2utt.pl < $hypdata/utt2spk > $hypdata/spk2utt
  utils/fix_data_dir.sh $hypdata
  for sdata in $hypdata; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch/data; log=$sdata/feat/mfcc-pitch/log
    [ -d $data ] || mkdir -p $data
    cp $sdata/{wav.scp,text,utt2spk,spk2utt,segments} $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    echo "## LOG (step55):  make_mfcc_pitch done with '$data' @ `date`"
    data=$sdata/mfcc-hires; feat=$sdata/feat/mfcc-hires/data; log=$sdata/feat/mfcc-hires/log
    [ -d $data ] || mkdir -p $data
    cp $sdata/{wav.scp,text,utt2spk,spk2utt,segments} $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    echo "## LOG (step55): done with '$data' @ `date`"
  done
fi

semi_ntu_mfcc_pitch=$updatedir/data/semi_ntu/mfcc-pitch
semi_ntu_mfcc_hires=$updatedir/data/semi_ntu/mfcc-hires
if [ ! -z $step57 ]; then
  utils/combine_data.sh $semi_ntu_mfcc_pitch $hypdata/mfcc-pitch $train_mfcc_pitch
  # rm $semi_ntu_mfcc_pitch/{feats.scp,cmvn.scp} 2>/dev/null
  utils/combine_data.sh $semi_ntu_mfcc_hires $hypdata/mfcc-hires $train_mfcc_hires
  # rm $semi_ntu_mfcc_hires/{feats.scp,cmvn.scp} 2>/dev/null
fi
if [ ! -z $step58 ]; then
  for data in $semi_ntu_mfcc_pitch; do
    sdata=$(dirname $data)
    feat=$sdata/feat/mfcc-pitch/data; log=$sdata/feat/mfcc-pitch/log
    utils/fix_data_dir.sh $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step58): done with '$data' @ `date`"
  done
  
fi
if [ ! -z $step59 ]; then
  for data in $semi_ntu_mfcc_hires; do
      sdata=$(dirname $data)
    feat=$sdata/feat/mfcc-hires/data log=$sdata/feat/mfcc-hires/log
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    echo "## LOG (step59): done with '$data' @ `date`"
  done
fi

alidir=$expdir/tri4a/ali_semi_ntu
if [ ! -z $step60 ]; then
  echo "## LOG (step60): started @ `date`"
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj  $semi_ntu_mfcc_pitch $lang_silprob $expdir/tri4a $alidir || exit 1
  echo "## LOG (step60): done with align_fmllr @ `date`"
fi
srcdir_gmm=$updatedir/exp/tri4a
if [ ! -z $step61 ]; then
  echo "## LOG (step61): started @ `date`"
  steps/train_sat.sh --cmd "$cmd" 10000 200000 \
  $semi_ntu_mfcc_pitch $lang_silprob $alidir $srcdir_gmm || exit 1
  echo "## LOG (step61): ended @ `date`"
fi
dictdir=$dict_silprob
dict_silprob=$updatedir/data/local/dict-silprob
lang=$lang_silprob
lang_silprob=$updatedir/data/lang-silprob
if [ ! -z $step62 ]; then
  echo "## LOG (step62): started @ `date`"
  steps/get_prons.sh --cmd "$cmd"  $semi_ntu_mfcc_pitch $lang $srcdir_gmm || exit 1
  utils/dict_dir_add_pronprobs.sh --max-normalize true \
  $dictdir $srcdir_gmm/pron_counts_nowb.txt $srcdir_gmm/sil_counts_nowb.txt \
  $srcdir_gmm/pron_bigram_counts_nowb.txt $dict_silprob || exit 1
  utils/validate_dict_dir.pl $dict_silprob
  utils/prepare_lang.sh $dict_silprob "<unk>" $lang_silprob/tmp $lang_silprob || exit 1;
  cp $lang/G.fst $lang_silprob/
  echo "## LOG (step62): done with align_fmllr @ `date`"
fi

graph_gmm_silprob=$srcdir_gmm/graph-4g-silprob
if [ ! -z $step63 ]; then
  echo "## LOG (step63): started @ `date`"
  utils/mkgraph.sh  $lang_silprob $srcdir_gmm $graph_gmm_silprob || exit 1
  echo "## LOG (step63): ended @ `date`"
fi
if [ ! -z $step64 ]; then
  echo "## LOG (step64): decoding started @ `date`"
  steps/decode_fmllr.sh --cmd "slurm.pl --quiet --nodelist=node06" --nj 3 \
  $graph_gmm_silprob  $dev_mfcc_pitch  $srcdir_gmm/decode-${dev_name} || exit 1
  echo "## LOG (step64): decoding ended @ `date`"
fi
alidir=$srcdir_gmm/ali_train
if [ ! -z $step65 ]; then
  echo "## LOG (step65): doing alignment started @ `date`"
  if [ ! -d $alidir ]; then
    steps/align_fmllr.sh --cmd "$cmd" --nj $nj \
    $semi_ntu_mfcc_pitch $lang_silprob $srcdir_gmm $alidir || exit 1
  fi
  echo "## LOG (step65): done @ `date`"
fi
latdir=$srcdir_gmm/ali_train-lattice
if [ ! -z $step66 ]; then
  echo "## LOG (step66): ali lattice started @ `date`"
  nj=$(cat $alidir/num_jobs) ||exit 1
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $semi_ntu_mfcc_pitch \
  $lang_silprob  $srcdir_gmm  $latdir || exit 1
  echo "## LOG (step66): done @ `date`"
fi

if [ ! -z $step70 ]; then
  echo "## LOG (step70): online-ivector training started @ `date`"
  
  source/egs/acumen/train-online-ivector-extractor.sh --steps "1-5" \
  --cmd "$cmd" --nj $nj \
  $semi_ntu_mfcc_pitch $semi_ntu_mfcc_hires $lang_silprob \
  $srcdir_gmm $updatedir/exp/tdnn || exit 1
  echo "## LOG (step70): done with online-ivector training @ `date`"
fi
expdir=$updatedir/exp
nnetdir=$expdir/tdnn
ivector_extractor=$nnetdir/ivector-extractor
train_ivectors=$nnetdir/ivector-train
if [ ! -z $step71 ]; then
  data=$semi_ntu_mfcc_hires
  data2=${data}-max2
  utils/data/modify_speaker_info.sh --utts-per-spk-max 2 $data  $data2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data2 $ivector_extractor $train_ivectors || exit 1
fi
dev_ivectors=$nnetdir/ivector-dev
dev_mfcc_hires=/home2/hhx502/sge2017/data/fstd-demo-updated/16k/mfcc-hires
if [ ! -z $step72 ]; then
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj 3 $dev_mfcc_hires \
  $ivector_extractor $dev_ivectors || exit 1   
fi

if [ ! -z $step80 ]; then
  echo "## LOG (step80): tdnn training started @ `date`"
  source/egs/acumen/train-tdnn.sh --steps 1-7 --cmd "$cmd" \
  --numleaves 9000  --hidden-dim 1024 --chainname chain1024  \
  --train-ivectors $train_ivectors  \
  $semi_ntu_mfcc_pitch  $semi_ntu_mfcc_hires \
 $lang_silprob  $alidir $latdir  $nnetdir
 echo "## LOG (step80): tdnn training done @ `date`"
fi
chaindir=$nnetdir/chain1024
graph=$chaindir/graph
lang_chain=$nnetdir/lang-chain
if [ ! -z $step81 ]; then
  echo "## LOG (step81): making graph started @ `date`"
  utils/mkgraph.sh --self-loop-scale 1.0 $lang_chain $chaindir $graph || exit 1
  echo "## LOG (step81): done with '$graph' @ `date`"
fi
if [ ! -z $step82 ]; then
  echo "## LOG (step82): decoding started @ `date`"
  steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
  --nj 3 --cmd "$cmd" \
  --online-ivector-dir $dev_ivectors \
  $graph $dev_mfcc_hires $chaindir/decode-${dev_name} || exit 1;
  echo "## LOG (ste82): done @ `date`"
fi
