#!/cm/shared/apps/python3.5.2/bin/python3.5
# -*- coding: utf-8 -*-

# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu
from __future__ import print_function
import argparse
import sys
import os
import re
import numpy as np
import logging
import random
# sys.path.insert(0, 'steps')
# import libs.common as common_lib
sys.path.insert(0,'source/egs')
import python_libs.mipireadnumber as mipinumreader

logger = logging.getLogger('python_libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting crack-oov-word.py')

def InsertDict(elem, nIndex, myDict):
    if elem in myDict:
        myDict[elem] += 1
    else:
        myDict[elem] = dict()
        dofd=myDict[elem] 
        dofd['z'] = 'y'
def Test():
    print("{0}".format('hello word !'))
    myDict = dict()
    for nIndex, elem in enumerate(['a','b', 'c']):
        InsertDict(elem, nIndex, myDict)
    for word in myDict:
        print("{0}\t{1}".format(word, myDict[word]))
def main():
    Test()


if __name__ == "__main__":
    main()
