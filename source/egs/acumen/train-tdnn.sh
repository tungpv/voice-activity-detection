#!/bin/bash

. path.sh
. cmd.sh
echo 
echo "## LOG: $0 $@"
echo
# begin sub
cmd='slurm.pl --quiet'
nj=120
steps=
numleaves=9000
hidden_dim=1024
train_stage=-10
get_egs_stage=-10
frames_per_eg=150
minibatch_size=128
num_epochs=4
proportional_shrink=0
chainname=chain1024
num_jobs_initial=3
num_jobs_final=16
remove_egs=false
max_param_change=2.0
initial_effective_lrate=0.001
final_effective_lrate=0.0001
leftmost_questions_truncate=-1
xent_regularize=0.1
remove_egs=false
get_egs_stage=-10
common_egs_dir=
train_ivectors=
# end sub

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 $0 --steps 1 --cmd "$cmd" --nj $nj \
 --numleaves 9000 \
 --hidden-dim 1024\
 --chainname chain1024 \
  --train-ivectors ../acumen/update-sept-26-2017/exp/tdnn/ivector-train \
 ../acumen/update-sept-23-2017/data/semi_ntu/mfcc-pitch \
 ../acumen/update-sept-23-2017/data/semi_ntu/mfcc-hires \
 ../acumen/update-sept-26-2017/data/lang-silprob \
 ../acumen/update-sept-26-2017/exp/tri4a/ali_train \
 ../acumen/update-sept-26-2017/exp/tri4a/ali_train-lattice \
 ../acumen/update-sept-26-2017/exp/tdnn

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 6 ]; then
  Example && exit 1
fi
train_mfcc_pitch=$1
train_mfcc_hires=$2
lang=$3
alidir=$4
alilatdir=$5
tgtdir=$6

lang_chain=$tgtdir/lang-chain
if [ ! -z $step03 ]; then
  rm -rf $lang_chain
  cp -r $lang $lang_chain
  silphonelist=$(cat $lang_chain/phones/silence.csl) || exit 1;
  nonsilphonelist=$(cat $lang_chain/phones/nonsilence.csl) || exit 1;
  # Use our special topology... note that later on may have to tune this
  # topology.
  steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >$lang_chain/topo
fi
treedir=$tgtdir/chain-tree
if [ ! -z $step04 ]; then
  # Build a tree using our new topology. This is the critically different
  # step compared with other recipes.
  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
  --leftmost-questions-truncate $leftmost_questions_truncate \
  --context-opts "--context-width=2 --central-position=1" \
  --cmd "$cmd" $numleaves $train_mfcc_pitch $lang_chain $alidir $treedir
  echo "## LOG (step04): done with tree building '$treedir' @ `date`"
fi

chaindir=$tgtdir/$chainname
if [ ! -z $step05 ]; then
  num_targets=$(tree-info $treedir/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)

  mkdir -p $chaindir/configs
  cat <<EOF > $chaindir/configs/network.xconfig
  input dim=100 name=ivector
  input dim=40 name=input
  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor
  fixed-affine-layer name=lda input=Append(-1,0,1,ReplaceIndex(ivector, t, 0)) affine-transform-file=$chaindir/configs/lda.mat
  # the first splicing is moved before the lda layer, so no splicing here
  relu-renorm-layer name=tdnn1 dim=$hidden_dim
  relu-renorm-layer name=tdnn2 input=Append(-1,0,1) dim=$hidden_dim
  relu-renorm-layer name=tdnn3 input=Append(-1,0,1) dim=$hidden_dim
  relu-renorm-layer name=tdnn4 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn5 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn6 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn7 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn8 input=Append(-3,0,3) dim=$hidden_dim
  ## adding the layers for chain branch
  relu-renorm-layer name=prefinal-chain input=tdnn8 dim=$hidden_dim target-rms=0.5
  output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5
  # adding the layers for xent branch
  # This block prints the configs for a separate output that will be
  # trained with a cross-entropy objective in the 'chain' models... this
  # has the effect of regularizing the hidden parts of the model.  we use
  # 0.5 / args.xent_regularize as the learning rate factor- the factor of
  # 0.5 / args.xent_regularize is suitable as it means the xent
  # final-layer learns at a rate independent of the regularization
  # constant; and the 0.5 was tuned so as to make the relative progress
  # similar in the xent and regular final layers.
  relu-renorm-layer name=prefinal-xent input=tdnn8 dim=$hidden_dim target-rms=0.5
  relu-renorm-layer name=prefinal-lowrank-xent input=prefinal-xent dim=64 target-rms=0.5
  output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF
  steps/nnet3/xconfig_to_configs.py --xconfig-file $chaindir/configs/network.xconfig --config-dir $chaindir/configs/  
  echo "## LOG (step05): done with xconfig-file generation ('$chaindir/configs') @ `date`"
fi

if [ ! -z $step06 ]; then
  echo "## LOG (step06): training started @ `date`"
  steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$cmd" \
    --feat.online-ivector-dir $train_ivectors \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize $xent_regularize \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --egs.dir "$common_egs_dir" \
    --egs.stage $get_egs_stage \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width $frames_per_eg \
    --trainer.num-chunk-per-minibatch $minibatch_size \
    --trainer.frames-per-iter 1500000 \
    --trainer.num-epochs $num_epochs \
    --trainer.optimization.num-jobs-initial $num_jobs_initial \
    --trainer.optimization.num-jobs-final $num_jobs_final \
    --trainer.optimization.initial-effective-lrate $initial_effective_lrate \
    --trainer.optimization.final-effective-lrate $final_effective_lrate \
    --trainer.optimization.proportional-shrink $proportional_shrink \
    --trainer.max-param-change $max_param_change \
    --cleanup.remove-egs $remove_egs \
    --feat-dir $train_mfcc_hires \
    --tree-dir $treedir \
    --lat-dir $alilatdir \
    --dir $chaindir 
  echo "## LOG (step06): tdnn done (chaindir='$chaindir') @ `date`"
fi

echo "## LOG ($0): done !"
