#!/bin/bash

. path.sh
. cmd.sh

# begin sub
cmd='slurm.pl --quiet'
nj=120
steps=
numleaves=7000
hidden_dim=1024
train_stage=-10
get_egs_stage=-10
frames_per_eg=150
minibatch_size=128
num_epochs=4
proportional_shrink=0
chainname=chain1024
num_jobs_initial=3
num_jobs_final=16
remove_egs=false
max_param_change=2.0
initial_effective_lrate=0.001
final_effective_lrate=0.0001
xent_regularize=0.1
remove_egs=false
get_egs_stage=-10
common_egs_dir=
# end sub

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 $0 --steps 1 --cmd "$cmd" --nj $nj \
 ../acumen/data/train-merge-sup/fbank-pitch \
 ../acumen/data/lang \
 ../acumen/update-aug-17-with938-50hrs/exp/mpe-nnet \
 ../acumen/update-aug-17-with938-50hrs

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 4 ]; then
  Example && exit 1
fi
srcdata=$1
lang=$2
srcdir=$3
tgtdir=$4

lang=../acumen/data/lang-update-sept-19-2017
if [ ! -z $step01 ]; then
  utils/prepare_lang.sh /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/dict  "<unk>" \
  $lang/tmp $lang || exit 1
fi
trainbig=../acumen/data/train-merge-semi
tmpdir=../acumen/data/tmp-data
 [ -d $tmpdir ] || mkdir -p $tmpdir
if [ ! -z $step02 ]; then
  cat /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/train-merge/fbank-pitch/wav.scp | \
  source/egs/acumen/check-wavscp.pl  "find  /data/users/nhanh/speech/  -name '*.wav' | grep -v diarization|" > $tmpdir/wav.scp  2>$tmpdir/wav-files-do-not-exist.txt
  cp /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/train-merge/fbank-pitch/{text,utt2spk,spk2utt,segments} \
  $tmpdir
  utils/fix_data_dir.sh $tmpdir
  utils/data/combine_data.sh $trainbig  $tmpdir ../acumen/data/train
  rm -rf $tmpdir 2>/dev/null
  echo "## LOG (step02): done & check '$trainbig'"
fi
train=$trainbig
traindata=$train/fbank-pitch
if [ ! -z $step03 ]; then
  utils/subset_data_dir.sh --utt-list $train/segments \
  /home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/train-merge/fbank-pitch $traindata
fi
alidir=$srcdir/ali_train
if [ ! -z $step04 ]; then
  echo "## LOG (step04): proceed with '$alidir' started @ `date`"
  steps/nnet/align.sh  --cmd "slurm.pl --quiet --gres=gpu:1" --use-gpu yes  --nj $nj \
  $traindata $lang $srcdir $alidir || exit 1
  echo "## LOG (step04): done with '$alidir' ended @ `date`"
fi
if [ ! -z $step06 ]; then
  for sdata in $trainbig; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch/data; log=$sdata/feat/mfcc-pitch/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    utils/fix_data_dir.sh $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step06): done with '$data' @ `date`"
    data=$sdata/mfcc-hires feat=$sdata/feat/mfcc-hires/data log=$sdata/feat/mfcc-hires/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    echo "## LOG (step06): done with '$data' @ `date`"
  done
fi
expdir=../acumen/update-sept-19-2017/exp
if [ ! -z $step07 ]; then
  echo "## LOG (step07): training started @ `date`"
  [ -d $expdir/tri3a ] || mkdir -p $expdir/tri3a
  source/egs/swahili/run-gmm-v2.sh --cmd "$cmd" --nj $nj --steps 1-5 \
  --train-id a --cmvn-opts "--norm-means=true"  --state-num 10000 --pdf-num 200000 \
  $trainbig/mfcc-pitch $lang $expdir  || exit 1

  echo "## LOG (step07): done with '$expdir' @ `date`"
fi
lang_silprob=../acumen/data/lang-update-sept-19-2017-silprob
olddict=/home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/dict
datadir=$trainbig/mfcc-pitch
dictdir=../acumen/data/local/dict-silprob
[ -d $dictdir ] || mkdir -p $dictdir
if [ ! -z $step08 ]; then
  steps/get_prons.sh --cmd "$cmd" \
  $datadir $lang $expdir/tri4a || exit 1
  echo "## LOG (step08): done with '$expdir/tri4a' @ `date`"
fi
if [ ! -z $step09 ]; then
  utils/dict_dir_add_pronprobs.sh --max-normalize true \
  $olddict $expdir/tri4a/pron_counts_nowb.txt $expdir/tri4a/sil_counts_nowb.txt \
  $expdir/tri4a/pron_bigram_counts_nowb.txt $dictdir || exit 1
  echo "## LOG (step09): done with '$dictdir'" 
fi
if [ ! -z $step10 ]; then
  utils/prepare_lang.sh $dictdir "<unk>" $lang_silprob/tmp $lang_silprob || exit 1;
fi

min_utt_len=5.0
if [ ! -z $step11 ]; then
  for sdata in $trainbig/{mfcc-pitch,mfcc-hires}; do
    data=$trainbig/subset-to-train-ivector/$(basename $sdata);
    [ -d $data ] || mkdir -p $data 
    echo "## LOG (step11, $0): segments=$sdata/segments"
    cat $sdata/segments | \
    awk -v thresh=$min_utt_len '{ if($4-$3 >= thresh){print $1 ;} }' > $data/utt-list
    subset_data_dir.sh --utt-list $data/utt-list $sdata $data
    echo "## LOG (step11, $0): subset data '$data' generated"
  done
fi
ali_for_ivector=$expdir/tri4a/ali_train-merge-semi-for-ivector
if [ ! -z $step12 ]; then
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj \
  $trainbig/subset-to-train-ivector/mfcc-pitch $lang_silprob \
  $expdir/tri4a $ali_for_ivector || exit 1
  echo "## LOG (step12): done with '$ali_for_ivector' @ `date`"
fi
nnetdir=$expdir/tdnn
transform_dir=$nnetdir/lda-mllt-transform-for-ivector-extractor
if [ ! -z $step13 ]; then
  steps/train_lda_mllt.sh --cmd "$cmd"  --num-iters 13 \
  --realign-iters "" \
  --splice-opts "--left-context=3 --right-context=3" \
  8000 80000 $trainbig/subset-to-train-ivector/mfcc-hires  $lang_silprob \
  $ali_for_ivector $transform_dir || exit 1
  echo "## LOG (step13): done with '$transform_dir' @ `date`"
fi
data_for_ivector=$trainbig/subset-to-train-ivector/mfcc-hires
if [ ! -z $step14 ]; then
  steps/online/nnet2/train_diag_ubm.sh --cmd "$cmd" --nj $nj \
  --num-threads 2 \
   --num-frames 720000 $data_for_ivector  512  $transform_dir $nnetdir/diag_ubm || exit 1
  echo "## LOG (step14): done with '$nnetdir/diag_ubm' @ `date`"
fi
ivector_extractor=$nnetdir/ivector-extractor
if [ ! -z $step15 ]; then
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$cmd" --nj $nj \
  --num-threads 1 --num-processes 1 \
  $data_for_ivector  $nnetdir/diag_ubm $ivector_extractor || exit 1;
  echo "## LOG (step15): done with '$ivector_extractor' @ `date`"
fi
ivector_extractor=$nnetdir/ivector-extractor
train_ivectors=$nnetdir/ivectors-train
if [ ! -z $step16 ]; then
  data=$trainbig/mfcc-hires
  data2=$trainbig/mfcc-hires-max2
  utils/data/modify_speaker_info.sh --utts-per-spk-max 2 $data  $data2
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data2 $ivector_extractor $train_ivectors || exit 1
fi
if [ ! -z $step17 ]; then
  for sdata in /home2/hhx502/sge2017/data/fstd-demo-updated/16k; do
    data=$sdata/mfcc-hires feat=$sdata/feat/mfcc-hires/data log=$sdata/feat/mfcc-hires/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    utils/fix_data_dir.sh $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
  done
  data_name=fstd-demo-updated
  data=/home2/hhx502/sge2017/data/fstd-demo-updated/16k/mfcc-hires
  data_ivectors=$nnetdir/ivectors-${data_name}
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj $nj $data \
  $ivector_extractor $data_ivectors || exit 1 
  echo " ## LOG (step17): done with '$data_ivectors' @ `date`"
fi
srcdir=$expdir/tri4a
latdir=$srcdir/ali_lattice-train
alidir=$srcdir/ali_train
if [ ! -z $step18 ]; then
  data=$trainbig/mfcc-pitch
  nj=$(cat $alidir/num_jobs) ||exit 1
  steps/align_fmllr_lats.sh --nj $nj --cmd "$cmd" $data \
  $lang_silprob  $srcdir  $latdir || exit 1
  echo "## LOG (step18): done & check '$latdir' @ `date`"
fi
chain_lang=$nnetdir/lang-chain
if [ ! -z $step19 ]; then
  rm -rf $chain_lang
  cp -r $lang_silprob $chain_lang
  silphonelist=$(cat $chain_lang/phones/silence.csl) || exit 1;
  nonsilphonelist=$(cat $chain_lang/phones/nonsilence.csl) || exit 1;
  # Use our special topology... note that later on may have to tune this
  # topology.
  steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >$chain_lang/topo
fi
leftmost_questions_truncate=-1
treedir=$nnetdir/chain-tree
datadir=$trainbig/mfcc-pitch
alidir=$expdir/tri4a/ali_train
numleaves=9000
if [ ! -z $step20 ]; then
  # Build a tree using our new topology. This is the critically different
  # step compared with other recipes.
  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
  --leftmost-questions-truncate $leftmost_questions_truncate \
  --context-opts "--context-width=2 --central-position=1" \
  --cmd "$cmd" $numleaves $datadir $chain_lang $alidir $treedir
  echo "## LOG (step20): done with tree building '$treedir' @ `date`"
fi

chaindir=$nnetdir/$chainname

if [ ! -z $step21 ]; then
  num_targets=$(tree-info $treedir/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)

  mkdir -p $chaindir/configs
  cat <<EOF > $chaindir/configs/network.xconfig
  input dim=100 name=ivector
  input dim=40 name=input
  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor
  fixed-affine-layer name=lda input=Append(-1,0,1,ReplaceIndex(ivector, t, 0)) affine-transform-file=$chaindir/configs/lda.mat
  # the first splicing is moved before the lda layer, so no splicing here
  relu-renorm-layer name=tdnn1 dim=$hidden_dim
  relu-renorm-layer name=tdnn2 input=Append(-1,0,1) dim=$hidden_dim
  relu-renorm-layer name=tdnn3 input=Append(-1,0,1) dim=$hidden_dim
  relu-renorm-layer name=tdnn4 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn5 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn6 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn7 input=Append(-3,0,3) dim=$hidden_dim
  relu-renorm-layer name=tdnn8 input=Append(-3,0,3) dim=$hidden_dim
  ## adding the layers for chain branch
  relu-renorm-layer name=prefinal-chain input=tdnn8 dim=$hidden_dim target-rms=0.5
  output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5
  # adding the layers for xent branch
  # This block prints the configs for a separate output that will be
  # trained with a cross-entropy objective in the 'chain' models... this
  # has the effect of regularizing the hidden parts of the model.  we use
  # 0.5 / args.xent_regularize as the learning rate factor- the factor of
  # 0.5 / args.xent_regularize is suitable as it means the xent
  # final-layer learns at a rate independent of the regularization
  # constant; and the 0.5 was tuned so as to make the relative progress
  # similar in the xent and regular final layers.
  relu-renorm-layer name=prefinal-xent input=tdnn8 dim=$hidden_dim target-rms=0.5
  relu-renorm-layer name=prefinal-lowrank-xent input=prefinal-xent dim=64 target-rms=0.5
  output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF
  steps/nnet3/xconfig_to_configs.py --xconfig-file $chaindir/configs/network.xconfig --config-dir $chaindir/configs/  
  echo "## LOG (step21): done with xconfig-file generation ('$chaindir/configs') @ `date`"
fi

traindata=$databig/mfcc-hires
latdir=$expdir/tri4a/ali_lattice-train
if [ ! -z $step22 ]; then
  echo "## LOG (step22): training started @ `date`"
  steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$cmd" \
    --feat.online-ivector-dir $train_ivectors \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize $xent_regularize \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --egs.dir "$common_egs_dir" \
    --egs.stage $get_egs_stage \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width $frames_per_eg \
    --trainer.num-chunk-per-minibatch $minibatch_size \
    --trainer.frames-per-iter 1500000 \
    --trainer.num-epochs $num_epochs \
    --trainer.optimization.num-jobs-initial $num_jobs_initial \
    --trainer.optimization.num-jobs-final $num_jobs_final \
    --trainer.optimization.initial-effective-lrate $initial_effective_lrate \
    --trainer.optimization.final-effective-lrate $final_effective_lrate \
    --trainer.optimization.proportional-shrink $proportional_shrink \
    --trainer.max-param-change $max_param_change \
    --cleanup.remove-egs $remove_egs \
    --feat-dir $trainbig/mfcc-hires \
    --tree-dir $treedir \
    --lat-dir $latdir \
    --dir $chaindir 
  echo "## LOG (step38): tdnn done (chaindir='$chaindir') @ `date`"
fi
lmdir=../acumen/data/local/lm-update-sept-19-2017
[ -d $lmdir ] || mkdir -p $lmdir
if [ ! -z $step23 ]; then
  awk '{print $1; }' ../acumen/data/local/dict-silprob/lexicon.txt | \
  sort -u | gzip -c > $lmdir/vocab.gz
  cut -d' ' -f2- ../acumen/data/train-merge-semi/fbank-pitch/text |\
  gzip -c |\
  cat - ../acumen/data/local/web-lm-kn/train-text.gz > $lmdir/train-text.gz
  cp ../acumen/data/local/web-lm-kn/dev-text.gz $lmdir/
  echo "## LOG (step23): 4-gram lm training started @ `date`"
  ngram-count -order 4 -kndiscount -interpolate\
  -vocab $lmdir/vocab.gz  -unk -sort -text $lmdir/train-text.gz -lm $lmdir/lm4.gz
  echo "## LOG (step23): done & check '$lmdir/lm4.gz' @ `date`"
fi
if [ ! -z $step24 ]; then
  source/egs/fisher-english/arpa2G.sh $lmdir/lm4.gz $chain_lang $chain_lang
  echo "## LOG(step24): done with '$chain_lang'"
fi
graph=$chaindir/graph
if [ ! -z $step25 ]; then
  echo "## LOG (step25): making graph started @ `date`"
  utils/mkgraph.sh --self-loop-scale 1.0 $chain_lang $chaindir $graph
  echo "## LOG (step25): done with '$graph' @ `date`"
fi
if [ ! -z $step26 ]; then
   echo "## LOG (step26): decoding started @ `date`"
  data=/home2/hhx502/sge2017/data/fstd-demo-updated/16k/mfcc-hires
  dataName=fstd-demo-updated
  steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
  --nj 3 --cmd "$cmd" \
  --online-ivector-dir $nnetdir/ivectors-${dataName} \
  $graph $data $chaindir/decode-${dataName} || exit 1;
  echo "## LOG (step26): decoding ended @ `date`"
fi

if [ ! -z $step27 ]; then
  echo "## LOG (step27): making G started @ `date`"
  source/egs/fisher-english/arpa2G.sh $lmdir/lm4.gz $lang_silprob $lang_silprob || exit 1
  echo "## LOG (step27): done & check '$lang_silprob' @ `date`"
fi
if [ ! -z $step28 ]; then
  echo "## LOG (step29): making graph @ `date`"
  utils/mkgraph.sh $lang_silprob $expdir/tri4a $expdir/tri4a/graph || exit 1
  echo "## LOG (step29): done @ `date`"
fi

if [ ! -z $step29 ]; then
  for sdata in /home2/hhx502/sge2017/data/fstd-demo-updated/16k; do
    data=$sdata/mfcc-pitch feat=$sdata/feat/mfcc-pitch/data log=$sdata/feat/mfcc-pitch/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    utils/fix_data_dir.sh $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj 3 $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh  $data
  done
fi
if [ ! -z $step30 ]; then
  echo "## LOG (step30): decoding started @ `date`"
  steps/decode_fmllr.sh --cmd "slurm.pl --quiet --nodelist=node06" --nj 3 \
  $expdir/tri4a/graph /home2/hhx502/sge2017/data/fstd-demo-updated/16k/mfcc-pitch  \
  $expdir/tri4a/decode-fstd-demo-updated || exit 1
  echo "## LOG (step30): decoding ended @ `date`"
fi
