#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);

# begin sub
sub InsertVocab {
  my ($vocab, $word, $pron) = @_;
  $pron =~ s:\s+: :g; $pron =~ s:^\s+::g; $pron =~ s:\s+$::g; 
  if (exists $$vocab{$word}) {
    my $pronVocab = $$vocab{$word};
    if (not exists $$pronVocab{$pron}) {
       $$pronVocab{$pron} ++;
    }
  } else {
    my %temp = ();
    my $pronVocab = $$vocab{$word} = \%temp;
    $$pronVocab{$pron} ++;
  }
}
sub LoadVocab {
  my ($inFile, $vocab) = @_;
  open (F, "$inFile") or die;
  while(<F>) {
    chomp;
    m:^(\S+)\s*(.*)$:g or next;
    my ($word, $pron) = ($1, $2);
    InsertVocab($vocab, $word, $pron);
  }
  close F;
}
# end sub

my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  die "\nExample: cat word-pron-with-count.txt | $0 lexicon.txt\n\n";
}

my ($lexFile) = @ARGV;
my %vocab = ();
my %oovVocab = ();
LoadVocab($lexFile, \%vocab);
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  for(my $i = 0; $i < scalar @A; $i++) {
    my $word = $A[$i];
    if(not exists $vocab{$word}) {
      $oovVocab{$word} ++;
    }
  }
}
open(F, "|sort -k2nr") or die;
foreach my $word (keys%oovVocab) {
  print F "$word\t$oovVocab{$word}\n";
}
close F;
