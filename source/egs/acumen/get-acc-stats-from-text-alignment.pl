#!/usr/bin/perl
use warnings;
use strict;
use utf8;
use open qw(:std :utf8);
use Getopt::Long;

my $thresh = 0;
GetOptions('thresh=i'=> \$thresh) or die;
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m:^(\S+)\s+(.*)$:g or next;
  my ($label, $words) = ($1, $2);
  my @A = split(";", $words);
  my ($nCorr, $nTotal) = (0, 0);
  for(my $i = 0; $i < scalar @A; $i ++) {
    my $pair = $A[$i];
    next if $pair =~ /^$/;
    my @P = split(" ", $pair);
    $nCorr ++ if($P[0] eq $P[1]);
    $nTotal ++;
  }
  next if $nTotal == 0;
  my $acc = int(100*$nCorr/$nTotal);
  if ($thresh <= $acc) {
    print "$label $acc\n";
  }
}
print STDERR "## LOG ($0): stdin ended\n";
