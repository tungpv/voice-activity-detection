#!/cm/shared/apps/python3.5.2/bin/python3.5
# -*- coding: utf-8 -*-

# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu
from __future__ import print_function
import argparse
import sys
import os
import re
import numpy as np
import logging
import random
from langconv import *
# sys.path.insert(0, 'steps')
# import libs.common as common_lib
sys.path.insert(0,'source/egs')
import python_libs.mipireadnumber as mipinumreader

logger = logging.getLogger('python_libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting crack-oov-word.py')

# begin utility
def IsRealNumber(s):
    try:
        val = float(s)
    except ValueError:
        return False
    return True
def IsInteger(s):
    try:
        val = int(s)
    except ValueError:
        return False
    return True
def _ReadPostiveInteger(num):
    convertDict = {0 : 'zero', 1 : 'one', 2 : 'two', 3 : 'three', 4 : 'four', 5 : 'five',
                   6 : 'six', 7 : 'seven', 8 : 'eight', 9 : 'nine', 10 : 'ten',
                  11 : 'eleven', 12 : 'twelve', 13 : 'thirteen', 14 : 'fourteen',
                  15 : 'fifteen', 16 : 'sixteen', 17 : 'seventeen', 18 : 'eighteen',
                  19 : 'nineteen', 20 : 'twenty',
                  30 : 'thirty', 40 : 'forty', 50 : 'fifty', 60 : 'sixty',
                  70 : 'seventy', 80 : 'eighty', 90 : 'ninety'}
    if num <= 20:
        return convertDict[num]
    if num < 100:
        if num % 10 == 0:
            return convertDict[num]
        else: 
            return convertDict[num // 10 * 10] + '-' + convertDict[num % 10]
    k = 1000
    m = k * 1000
    b = m * 1000
    t = b * 1000
    if num < k:
        if num % 100 == 0: 
            return ( convertDict[num // 100] + ' hundred')
        else: 
            return convertDict[num // 100] + ' hundred and ' + ReadIntegerWithEnglish(num % 100)
    if num < m:
        if num % k == 0: return ReadIntegerWithEnglish(num // k) + ' thousand'
        else: return ReadIntegerWithEnglish(num // k) + ' thousand ' + ReadIntegerWithEnglish(num % k)
    if num < b:
        if (num % m) == 0: return ReadIntegerWithEnglish(num // m) + ' million'
        else: return ReadIntegerWithEnglish(num // m) + ' million ' + ReadIntegerWithEnglish(num % m)
    return num
    # raise AssertionError("Number '{0}' is too large".format(str(num)))
def ReadIntegerWithEnglish(inStr):
    if not IsInteger(inStr):
        logger.info("unidentified integer string '{0}'".format(inStr))
        return inStr
    if int(inStr) < 0:
        logger.info("minus number '{0}'".format(inStr))
        return inStr
    return _ReadPostiveInteger(int(inStr))
    
    
def ReadDecimalWithEnglish(inStr):
    if not IsInteger(inStr):
        logger.info("identified integer string '{0}'".format(inStr))
        return inStr
    tmpList = list(inStr)
    retS = ''
    for sItem in tmpList:
        retS += ReadIntegerWithEnglish(sItem) + ' '
    retS = re.sub(r' $', '', retS)
    return retS

def IsCurrencyNumber(s):
    if not s: return False
    if not re.match(r'^\$\d+', s): return False
    return True
def ReadNormalNumber(word):
    utterance = str()
    word0 = word
    if re.match(r'^\-\d+', word): utterance = 'minus '
    word = re.sub(r'\-|,', '', word)
    word = re.sub(r'\.$|,$', '', word)
    if not IsRealNumber(word): return word0
    m = re.search(r'(\d+)?((\.\d+)?)', word)
    if m.group(1):
        utterance += ReadIntegerWithEnglish(m.group(1))
    if m.group(2):
        numStr = m.group(2)
        numStr = re.sub(r'\.', '', numStr)
        utterance += ' point ' + ReadDecimalWithEnglish(numStr)
    return utterance
def ReadEnglishCurrencyNumber(word):
    if not IsCurrencyNumber(word):
        return word
    unit = 'dollars'
    word0 = word
    word = re.sub(r'[\$,]', '', word)
    utterance = ReadNormalNumber(word)
    if utterance == word: return word0
    utterance += ' dollars'
    return utterance
def IsPercentageNumber(word):
    if not word: return False
    if not re.search(r'%$', word): return False
    return True

def ReadEnglishPercentage(word):        
    if not IsPercentageNumber(word):
        return word
    if word == '%': return 'percent'
    word0 = word
    word = re.sub('%', '', word)
    utterance = ReadNormalNumber(word)
    if not utterance: return word0
    utterance += ' percent'
    return utterance
def ReadTimeNumber(word):
    m = re.match(r'^(\d+):(\d+)$', word)
    if not m: return word
    x = m.group(1)
    y = m.group(2)
    m = re.match(r'^0(\d)$', y)
    if m:
        y = m.group(1)
        return _ReadPostiveInteger(int(x)) + ' o ' + _ReadPostiveInteger(int(y))
    return _ReadPostiveInteger(int(x)) + ' ' + _ReadPostiveInteger(int(y))
def IsYearNumber(word):
    m =re.match(r'^(1[6-9][0-9]{2}|20[0-9]{2})$', str(word))
    if not m: return False
    return True
def ReadYearByFourNumber(word):
    if not IsYearNumber(word): return word
    numYear = int(word)
    if numYear >= 2000: return _ReadPostiveInteger(numYear)
    if numYear %100 ==0: return _ReadPostiveInteger(numYear)
    m = re.search(r'^(\d{2})(\d{2})$', str(numYear))
    x = m.group(1)
    y = m.group(2)
    m = re.search(r'^0(\d)$', y)
    if m:
        y = m.group(1)
        return _ReadPostiveInteger(int(x)) + ' o ' + _ReadPostiveInteger(int(y))
    return _ReadPostiveInteger(int(x)) + ' ' + _ReadPostiveInteger(int(y))
def IsOrdinalNumber(word):
    if re.match(r"^\d+(st|nd|rd|th)", str(word)):
        return True
    return False
def _ReadOrdinal(n):
    convertDict = {1:'first', 2:'second', 3:'third', 4:'fourth', 5:'fifth', 6:'sixth',
                   7:'seventh', 8:'eighth', 9:'ninth', 10:'tenth', 11:'eleventh', 12:'twelfth',
                   13:'thirteenth', 14:'fourteenth', 15:'fifteenth', 16:'sixteenth', 17:'seventeenth',
                   18:'eighteenth', 19:'nineteenth', 20:'twentieth', 30:'thirtieth', 40:'fortieth', 50:'fiftieth',
                   60:'sixtieth', 70:'seventieth', 80:'eightieth', 90:'ninetieth'}
    if n <= 20: return convertDict[n]
    if n < 100:
        if n % 10 == 0: return convertDict[n]
        else: return ReadIntegerWithEnglish((n//10)*10) + '-' + convertDict[n%10]
    k = 1000
    if n < k:
        if (n % 100) == 0: 
            return ReadIntegerWithEnglish(n//100) + ' hundredth'
        else:
            return ReadIntegerWithEnglish(n//100) + ' hundred and ' + _ReadOrdinal(n%100)
    m = k * 1000
    if n < m:
        if n % k == 0:
            return ReadIntegerWithEnglish(n//k) + ' thousandth'
        else:
            return ReadIntegerWithEnglish(n//k) + ' thousand ' + _ReadOrdinal(n%k)
    b = m * 1000
    if n < b:
        if n % m == 0:
            return ReadIntegerWithEnglish(n//m) + ' millionth'
        else:
            return ReadIntegerWithEnglish(n//m) + ' million ' + _ReadOrdinal(n % m)
    #    raise AssertionError("Number '{0}' is too large".format(word))
    return n
def ReadOrdinalNumber(word):
    if not IsOrdinalNumber(word): return word
    word0 = word
    word = re.sub(r'^(\d+)(st|nd|rd|th)$', r'\1', word)
    if not IsInteger(word): return  word0
    return _ReadOrdinal(int(word))
def ReadFractionNumber(word):
    m = re.search(r'^(\d+)\/(\d+)$', word)
    if not m: return word
    x = int(m.group(1))
    y = int(m.group(2))
    if x == 1 and y == 2: return 'half'
    if x == 24 and y == 7: return 'twenty-four seven'
    if x== 9 and y ==11: return 'nine eleven'
    if x == y : return _ReadPostiveInteger(x) + ' ' + _ReadPostiveInteger(x)
    if x == 1 and y >x: return _ReadPostiveInteger(x) + ' ' + _ReadOrdinal(y)
    if x > 1: return _ReadPostiveInteger(x) + ' ' + _ReadOrdinal(y) + 's'
    return word
def ReadRangeNumber(word):
    word0 = word
    word = re.sub(r'\+', '', word)
    m = re.search(r'(\d+)\-(\d+)$', word)
    if not m: return  word0
    tmpList = word.split("-")
    for x in tmpList:
        if not IsInteger(x): return word0
    nSize = len(tmpList)
    if nSize == 2:
       x0 = int(tmpList[0])
       x1 = int(tmpList[1])
       if IsYearNumber(x0):
           if IsYearNumber(x1):
               if x0 < x1:
                   return ReadYearByFourNumber(x0) + ' to ' + ReadYearByFourNumber(x1)
               elif x0%100 < x1: 
                   return ReadYearByFourNumber(x0) + ' to ' + _ReadPostiveInteger(x1)
       elif (x0 < x1 and x1 < 1000): 
            return _ReadPostiveInteger(x0) + ' to ' + _ReadPostiveInteger(x1)
       elif(x1 < 1000):
            return _ReadPostiveInteger(x0)  + ' ' + _ReadPostiveInteger(x1)
    word = str()
    for x in tmpList:
        word += ReadDecimalWithEnglish(str(x)) + ' '
    word = word.strip()
    return word            
def IsYearPlural(word):
    m = re.search(r'\d0s', word)
    if m: return True
    return False
def ReadYearPlural(word):
    if not IsYearPlural(word): return word
    word0 = word
    word =  re.sub(r"\'s", 's', word)
    m = re.search(r'^(\d+)s', word)
    if not m: return word0
    word = m.group(1)
    convertDict = {'ten':'teens', 'twenty':'twenties', 'thirty':'thirties', 'forty':'forties', 'fifty':'fifties',
                   'sixty':'sixties', 'seventy':'seventies', 'eighty':'eighties', 'ninety':'nineties', 'hundred':'hundreds', 'thousand':'thousands'}
    wordList = list()
    if re.match(r'^\d{4}$', word):
        if IsYearNumber(word):
            word = ReadYearByFourNumber(word)
        else:
            word = _ReadPostiveInteger(int(word))
    else:
        word = _ReadPostiveInteger(int(word))
    wordList = word.split() 
    nSize = len(wordList)
    lastWord = wordList[nSize-1]
    if not lastWord in convertDict:
        logger.info("word '{0}' is not in convertDict".format(lastWord))
        return word0
    wordList[nSize-1] = convertDict[lastWord]
    return ' '.join(wordList)
def ReadCompositeWord(word, checkDict):
    if not re.match(r'.*\-.*', word): return word
    wordList = word.split("-")
    nwWord = 0
    ndWord = 0
    transWord = str()
    for xword in wordList:
        if re.match(r'^[a-z]+$', xword):
            nwWord += 1
            yword = xword
            if not yword in checkDict: yword = ' '.join(list(yword))
            transWord += yword + ' '
        elif re.match(r'^[\d]+$', xword): 
            ndWord += 1
            transWord += _ReadPostiveInteger(int(xword)) + ' '
    if nwWord == 0 or ndWord == 0: return word
    if nwWord + ndWord != len(wordList): return word
    return transWord.strip()
def ReadHybridWord(word, checkDict):
    if not (re.match(r'[a-z]+\d+$', word) or re.match(r'^\d+[a-z]+$', word)): return word
    if re.match(r'^\d+(th|st|rd|nd)$', word): return word
    if re.search(r'(0s)$', word): return word
    wordList = re.split(r'(\d+)', word)
    transfered = str()
    for xword in wordList:
        if not xword: continue
        if re.match(r'^\d+$', xword):
            transfered += _ReadPostiveInteger(int(xword)) + ' '
        else:
            yword = xword
            if yword not in checkDict: 
                yword = ' '.join(list(yword))
            transfered += yword + ' '
    return transfered.strip()
def ReadWordContainingPunct(word, checkDict):
    if not re.match(r'[a-z]+[\.,!;]([a-z]+)?', word): return word
    while re.search(r'([a-z]+)\.([a-z]+)', word):
        word = re.sub(r'([a-z]+)\.([a-z]+)', r'\1 dot \2', word)
    wordList = re.split(r'\.|,|!|;', word)
    transfered = str()
    for xword in wordList:
        if not xword: continue
        transfered += xword + ' '
    return transfered.strip()
def ReadAmpersandWord(word):
    if word == '&': return 'and'
    while re.search('([a-z]+|[0-9]+)&([a-z]+|[0-9]+)', word):
        word = re.sub('([a-z]+|[0-9]+)&([a-z]+|[0-9]+)', r'\1 and \2', word)
    return word
def IsOovWord(word, wordDict):
    if word not in wordDict:
        return True
    return False
# end utility
class OovNormalizer:
    def __init__(self, args):
        self._args = args
        self._wordDict = dict()
        self._oovDict = dict()
        self._textDict = dict()
        self._transferDict = dict()
        self._num_of_total_words = 0
        self._num_of_oov_words = 0
    def IsOovWord(self, word):
        return IsOovWord(word, self._wordDict)
    def AddWordToOovDict(self, word):
        if not word in self._oovDict:
            self._oovDict[word] = int(1)
        else: self._oovDict[word] += 1
    def AddWordsToOovDict(self, words):
        num_of_oov = 0
        total_words = 0
        for word in words.split():
            word = word.strip()
            if not word: continue
            total_words += 1
            if self.IsOovWord(word):
                num_of_oov += 1
                self.AddWordToOovDict(word)
        self._num_of_total_words += total_words
        self._num_of_oov_words += num_of_oov
        
        return num_of_oov*100/total_words
    def DumpOovDict(self):
        retStr = str()
        for word, nCount in sorted(self._oovDict.items(), key = lambda x: int(x[1]), reverse=True):
            retStr += "{0:20} {1:4}".format(word, nCount) + "\n"
        return retStr
    def IsOovWordList(self, wordList):
        for word in wordList:
            if self.IsOovWord(word):
                return True
        return False
    def BuildLexicon(self, word, wordPron, tgtDict):
        if word in tgtDict:
            pronDict = tgtDict[word]
            if not wordPron in pronDict:
                pronDict[wordPron] = int(1)
        else:
            tgtDict[word] = dict()
            pronDict = tgtDict[word]
            pronDict[wordPron] = int(1)
    def GetWordDict(self): return self._wordDict
    def GetWordPron(self, word):
        if word not in self._wordDict: return None
        return list(self._wordDict[word].keys())
    def DumpWordDict(self):
        retstr = str()
        for word in self._wordDict:
            for pron in self.GetWordPron(word):
                retstr += "{0}\t{1}\n".format(word, pron)
        return retstr
    def DumpWordDict2(self, wordDict):
        s = str()
        for word in wordDict:
            for pron in wordDict[word]:
                s += "{0}\t{1}\n".format(word, pron)
        return s
    def LabelPronunciationForWordSequence(self, wordList, maximum=1):
        pronDict = dict()
        pronList = list()
        nWord = len(wordList)
        nIndex = 0
        backTrace = list()
        while(True):
            if nIndex < nWord:
                word = wordList[nIndex]
                if self.IsOovWord(word):
                    logger.info("oov word '{0}' seen".format(word))
                    return pronDict
                curPronList = self.GetWordPron(word)
                curPron = curPronList[0]
                # we only get one
                if maximum == 1:
                    del curPronList[:]
                    curPronList.append(curPron)
                backTrace.append([curPronList, 1])
                pronList.append(curPron)
                nIndex += 1
            elif nIndex == nWord:
                tmpPron = ' '.join(pronList)
                tmpWord = ' '.join(wordList)
                self.BuildLexicon(tmpWord, tmpPron, pronDict)
                nIndex -= 1
                del pronList[-1]
                [curPronList, nPron] = backTrace.pop()
                nTotalPron = len(curPronList)
                while(nIndex >=0 and nPron >= nTotalPron):
                    nIndex -= 1
                    if nIndex < 0:
                        return pronDict
                    pronList.pop()
                    [curPronList, nPron] = backTrace.pop()
                    nTotalPron = len(curPronList)
                curPron = curPronList[nPron]
                pronList.append(curPron)
                backTrace.append([curPronList, nPron+1])
                nIndex += 1
    def WriteArabicNumberLexicon(self, word, arabicDict):
        word = str(word)
        if not re.match(r'(\d+(\.\d+)?)', word): return None
        cnVerbatimDict = mipinumreader.ReadNormalNumberInChineseMutable(word)
        enVerbatimDict = mipinumreader.ReadNormalNumberInEnglishMutable(word)
        retDict = cnVerbatimDict.copy()
        retDict.update(enVerbatimDict)
        for wordsequence in retDict:
            wordList = re.split(r'[\s\-]', wordsequence)
            wordList = list(filter(None, wordList))
            # logger.info('wordList={0}'.format(wordList))
            labelDict = self.LabelPronunciationForWordSequence(wordList)
            if not labelDict:
                logger.info("word={0}, cnVerbatimDict={1}".format(word, cnVerbatimDict))
                continue
            for ws in labelDict:
                newWord = "{0} [{1}]".format(word, ws)
                for pron in labelDict[ws]:
                    self.BuildLexicon(newWord, pron, arabicDict)
    def LoadDicts(self):
        if self._args.oov_word_count_file:
            with open(self._args.oov_word_count_file) as istream:
                for line in istream:
                    wList = [x.strip() for x in line.split()]
                    if len(wList) == 2:
                        self._oovDict[wList[0]] = wList[1]
                    else:
                        logger.warn("bad line {0}".format(line))
        if self._args.word_lexicon_file:
            with open(self._args.word_lexicon_file, encoding='utf-8') as istream:
                for line in istream:
                    wList = [x.strip() for x in line.split()]
                    word = wList.pop(0)
                    wordPron = ' '.join(wList).strip()
                    self.BuildLexicon(word, wordPron, self._wordDict)
        # if self._args.text_file:
        #    with open(self._args.text_file, encoding='utf-8') as istream:
        #        for line in istream:
        #            line = re.sub(r'\s+', '', line.strip())
        #            if line and not line in self._textDict:
        #                self._textDict[line] = int(1)
        #            else:
        #                self._textDict[line] += 1
        if self._args.word_transfer_dict_file:
            with open(self._args.word_transfer_dict_file, encoding='utf-8') as istream:
                for line in istream:
                    m = re.search(r'^(\S+)\s+(.*)$', line)
                    if not m: continue
                    word = m.group(1)
                    transfer = m.group(2)
                    if word in self._transferDict:
                        logger.info("Duplicated line '{0}' in transfer dict".format(line))
                        continue
                    self._transferDict[word] = transfer
    def TransferWords(self, words):
        wordList = words.split()
        if not wordList: return None
        newWords = str()
        for word in wordList:
            word = ReadYearPlural(word)
            # word = ReadYearByFourNumber(word)
            if word in self._transferDict:
                word = self._transferDict[word]
            newWords += word + ' '
        newWords = re.sub(r'\s+', ' ', newWords)
        return newWords.strip()
    def GetLabelAndWords(self, line, lowercase=False):
        m = re.search(r'^(\S+)\s+(.*)$', line)
        retList = list()
        if not m: return None
        label = m.group(1)
        words = m.group(2).strip()
        if not words: return None
        retList.append(label)
        if lowercase: words = words.lower()
        retList.append(words)
        return retList
    def ReadOovWordList(self):
        if self._args.oov_word_count_file:
            for word, nCount in sorted(self._oovDict.items(), key = lambda x: int(x[1]), reverse=True):
                transfered = ReadEnglishCurrencyNumber(word)
                if word == transfered:
                    transfered = ReadAmpersandWord(word)
                if word == transfered:
                    transfered = ReadEnglishPercentage(word)
                if word == transfered:
                    transfered = ReadOrdinalNumber(word)
                if word == transfered:
                    transfered = ReadFractionNumber(word)
                if word == transfered:
                    transfered = ReadTimeNumber(word)
                if word == transfered:
                    transfered = ReadYearByFourNumber(word)
                if word == transfered:
                    transfered = ReadRangeNumber(word)
                if word == transfered:
                    transfered = ReadYearPlural(word)
                if word == transfered:
                    transfered = ReadCompositeWord(word, self._wordDict)
                if word == transfered:
                    transfered = ReadHybridWord(word, self._wordDict)
                if word == transfered:
                    transfered = ReadWordContainingPunct(word, self._wordDict)
                if word == transfered:
                    transfered = ReadNormalNumber(word)
                print("{0:20}\t{1:4}\t{2}".format(word, nCount, transfered))
    def CollectOovOfTransferDict(self):
        oovWordDict = dict()
        if not self._wordDict:
            raise Exception("word dict is not loaded, try to use --word-lexicon-file to load word dict")
        if self._args.word_transfer_dict:
            with open(self._args.word_transfer_dict, encoding='utf-8') as istream:
                for line in istream:
                    m = re.search(r'^(\S+)\s+(\d+)\s+(.*)$', line)
                    if not m: continue
                    nCount = m.group(2)
                    transfered = m.group(3)
                    if not transfered: continue
                    wordList = re.split(r'[\s\-]', transfered)
                    for word in wordList:
                        if not word in self._wordDict:
                            if not word in oovWordDict:
                                oovWordDict[word] = int(nCount)
                            else:
                                oovWordDict[word] += int(nCount)
            for word, nCount in sorted(oovWordDict.items(), key = lambda x:x[1], reverse=True):
                print("{0:4}\t{1}".format(nCount, word))
        

def get_args():
    parser = argparse.ArgumentParser(description='Arguments_parser')
    parser.add_argument('--oov-word-count-file', dest='oov_word_count_file', type=str, help='file record oov count info')
    parser.add_argument('--word-lexicon-file', dest='word_lexicon_file', type=str, help='lexicon file')
    parser.add_argument('--text-file', dest='text_file', type=str, help='text_file that oov words come from')
    parser.add_argument('--target-dir', dest='target_dir', type=str, help='target exp dir')
    parser.add_argument('--transfer-dict-file', dest='word_transfer_dict_file', type=str, help='word transfer dict')
    parser.add_argument('--read-oov-for-transfer-dict', dest='read_oov_for_transfer_dict', action='store_true')
    parser.add_argument('--collect-oov-from-transfer-dict', dest='collect_oov_from_transfer_dict', action='store_true')
    parser.add_argument('--tgtdir', dest='tgtdir', type=str, help='target folder for output if any')
    parser.add_argument('--normalize-mipitalk-mobile-data', dest='normalize_mipitalk_mobile_data', action='store_true')
    parser.add_argument('--normalize-text', dest='normalize_text', action='store_true')
    parser.add_argument('--transfer-text', dest='transfer_text', action='store_true')
    parser.add_argument('--dump-oov-of-text', dest='dump_oov_of_text', action='store_true')
    parser.add_argument('--label-pronunciation-for-number-word', dest='label_pronunciation_for_number_word', action='store_true')
    parser.add_argument('--oov-thresh-in-utterance', dest='oov_thresh_in_utterance', type=int, default=3, help='maximum oov words permitted in each utterances')

    parser.add_argument('--test', dest='test', action='store_true')
    parser.add_argument('--test-chinese', dest='test_chinese', action='store_true')
    parser.add_argument('--test-english', dest='test_english', action='store_true')
    parser.add_argument('--test-chinese-segmentation', dest='test_chinese_segmentation', action='store_true')
    parser.add_argument('--test-label-pronunciation-for-wrod-sequence', dest='test_label_pronunciation_for_word_sequence', action='store_true', help='test word sequence pronunciation labeling')
    parser.add_argument('--test-read-arabic-number-in-english', dest='test_read_arabic_number_in_english', action='store_true')
    parser.add_argument('--write-arabic-number-lexicon', dest='write_arabic_number_lexicon', action='store_true')
    return parser.parse_args()
def TestReadArabicNumberInEnglish(args):
    translateDict = dict()
    number1 = '$21'
    print("number={0}, translated={1}".format(number1, ReadEnglishCurrencyNumber(number1)))
    number2 = '51.4%'
    print("number={0}, translated={1}".format(number2, ReadEnglishPercentage(number2)))
    for x in [1, 2, 3, 4, 5, 10, 100, 102, 1002, 100002, 123]:
        print("number={0}, ordinal={1}".format(x, _ReadOrdinal(x)))
    for x in ["1st", "2nd","3rd","4th","5th","10th","100th", "102nd", "1002nd", "100002nd"]:
        print("number={0}, ordinal={1}".format(x, ReadOrdinalNumber(x)))
    for x in ['1/2', '2/3', '9/11', '20/20', '24/7' ]:
        print("number={0}, fration={1}".format(x, ReadFractionNumber(x)))
    for x in ['1:2', '2:05', '7:09','10:40']:
        print ("number={0}, transfer={1}".format(x, ReadTimeNumber(x)))
        for x in [1605, 1905, 1657,2011,2016,2015,2017,2014,2013,2000,2010,2012,2008,1965,2009,2001,2003,1000,2007,1991,2004,2006,2020,4000,1500]:
            print ("number={0}, transfer={1}".format(x, ReadYearByFourNumber(x)))
    for x in [ '10-15', '669-1193', '20-30', '669-1938', '691-1938', '3-4', '+1-800-255-0000', '2-3', '5-10', '15-20', '1-1', '50-50', '2016-2017', '30-40', '3-1', '9-11', '9-3', '10-20', '10-4', '20-25', '40-50', '4-5', '669-119-3821', '7-8' ]:
        print ("word={0}, transfer={1}".format(x, ReadRangeNumber(x)))
        for x in ["80s","90s","1980s","60s","70s","1960s","1970s","50s","1990s","1950s","20s","30s","2000s","40s","1920s","1930s","1940s","1800s","1880s","1900s","100s","1600s","1840s","10s","1700s","1780s","1790s","1830s","1890s","200s","2980s","360s","570s"]:
            print ("number={0}, transfer={1}".format(x, ReadYearPlural(x)))
        for x in [ "f-35","i-10","ar-15","i-20","a-1","c-130","omega-3","one-77","ak-47","br-2","c-123","carbon-12","catch-22","f-150","i-17","i-25","i-27","i-35","i-65","i-70","i-81","in-37","mx-5","perrin-410","sg-1","su-50","a-10","ar-22","b-17","b-24","b-52","beta-2","blink-182","c-17","ch-47","csf-3","diane-35","dm-810","dsm-5","e-40","eco-1","ecs-12","end-2016","euro-6","f-16","ffx-2","fm-544","gc-2","gs-1","gs-7","h-60","hi-5","hu-100","i-15","i-380","i-39","i-394","i-40","i-5","i-55","i-57","i-74","i-76","i-77","i-85","i-90","icd-10","jp-4","july-2014","k-30","l-188","m-43","mid-2015","mp-35","ms-13","mxvi-4000","ny-32","p-8","pac-12","par-5","pcg-10","pon-3","pro-1","pt-2030","spp-1","sr-71","sub-2","t-1000","tb-1","tier-2","tv-14","type-1","type-2","u-22","u-32","uc-11","under-12","under-19","under-23","uo-1","us-224","vp-1","vp-30","vr-57","vsx-52","x-360","xv-700","24-hour","7-eleven","10-year","10-minute","20-something","36-hour","30-year","45-minute","20-year","360-degree","10-day","10-second","25-year","30-day"]:
            print ("number={0}, transfer={1}".format(x, ReadCompositeWord(x, dict())))
        for x in ["3d", "4k", "938live", "2g", "4g", "10k", "3g", "5c", "2d", "2k", "5s", "ps4", "f1", "sg100", "co2", "ps3", "s5", "fy17", "mp4"]:
            print("number={0}, transfered={1}".format(x, ReadHybridWord(x,dict())))
        for x in ["dr.", "p.m.", "u.s.", "a.m.", "that.", "facebook.com", "it.", "this.", "you.", "me.", "vs.", "okay.", "now.", "well.", "it,", "the.", "too.", "red.", "google,", "time.", "on.", "one.", "up.", "us.", "again.", "long.", "so,", "so.", "then.", "right.", "that,", "a,", "go.", "like.", "nice.", "okay,", "today.", "morning.", "much.", "be.", "know.", "thing.", "way.", "all.", "campaign.", "mediacom.com"]:
            print ("number={0}, transfered={1}".format(x, ReadWordContainingPunct(x,dict())))

def NormalizeUtteranceContainChineseWords(words, normalizer):
    words = re.sub(r"([A-Za-z0-9])([\u4e00-\u9fa5])", r'\1 \2', words)
    words = re.sub(r"([\u4e00-\u9fa5])([A-Z]a-z0-9)", r'\1 \2', words)
    wordList = words.split()
    words = str()
    for word in wordList:
        if normalizer.IsOovWord(word) and re.search(r'[\u4e00-\u9fa5]', word):
            charList = list(word)
            for char in charList:
                if normalizer.IsOovWord(char):
                    normalizer.AddWordToOovDict(char)
            words += ' '.join(charList) + ' '
        elif normalizer.IsOovWord(word):
            normalizer.AddWordToOovDict(word)
            words += word + ' '
        else: words += word + ' '
    return words.strip()
            
def NormalizeEnglishUtterance(words, normalizer):
    wordList = words.split()
    words = str()
    for word in wordList:
        word1 = ReadEnglishCurrencyNumber(word)
        if word1 == word:
            word1 = ReadOrdinalNumber(word)
        if word1 == word:
            word1 = ReadYearPlural(word)
        if word1 == word:
            word1 = ReadCompositeWord(word, normalizer.GetWordDict())
        words += word1 + ' '
    wordList = words.strip().split()
    for word in wordList:
        if normalizer.IsOovWord(word):
            normalizer.AddWordToOovDict(word)
    words = ' '.join(wordList)
    return words.strip()
def NormalizeMipitalkMobileData(args):
    oovNormalizer =OovNormalizer(args)
    oovNormalizer.LoadDicts()
    outputLines = str()
    if args.text_file:
        with open(args.text_file, encoding='utf-8') as istream:
            for line in istream:
                m = re.search(r'^(\S+)\s+(.*)$', line)
                if not m: continue
                label = m.group(1)
                words = m.group(2).strip()
                if not words: continue
                if re.search(r'[\u4e00-\u9fa5]', line):
                    words = NormalizeUtteranceContainChineseWords(words, oovNormalizer)
                else:
                    words = NormalizeEnglishUtterance(words, oovNormalizer)
                outputLines += label + ' ' + words + "\n"
    if args.tgtdir:
        if not os.path.exists(args.tgtdir):
            os.makedirs(args.tgtdir)
        ostream = open("{0}/text".format(args.tgtdir), 'w', encoding='utf-8')
        ostream.write(outputLines)
        ostream.close()
        oovStrs = oovNormalizer.DumpOovDict()
        ostream = open("{0}/oov-word-count.txt".format(args.tgtdir), 'w', encoding='utf-8')
        ostream.write(oovStrs)
        ostream.close()        
def NormalizeWhiteSpace(s):
    s = re.sub(r"^\s+|\s+$", '', s)
    s = re.sub(r"\s+", ' ', s)
    return s
def strQ2B(ustring):
    """把字符串全角转半角 obtained with baidu"""
    rstring = ""
    for uchar in ustring:
        inside_code=ord(uchar)
        if inside_code==0x3000:
            inside_code= 32
        elif inside_code >= 65281 and inside_code <= 65374:   #转完之后不是半角字符返回原来的字符
            inside_code -= 65248
        rstring += chr(inside_code)
    return rstring
def ConvertT2SChinese(s): 
    retList = list()
    for w in [w for w in s.split() if w ]:
        w1 = strQ2B(w)
        w1 = Converter('zh-hans').convert(w1)
        retList.append(w1)
    return ' '.join(retList)
def ConvertYear(s):
    m = re.search(r'(\d{4})\s+(年)', s)
    while m:
        sstr = m.group(1)
        digitList = list(sstr)
        for idx, digit in enumerate(digitList):
            digitList[idx] = mipinumreader.ReadPositiveInChinese(digit)
        year = ' '.join(digitList)
        s = s.replace(m.group(1), year)
        m = re.search(r'(\d{4})\s+(年)', s)
    return s
def IsChineseWord(w):
    m = re.match(r'^[\u4e00-\u9fa5]+$', w)
    if m: return True
    return False
def IsEnglishWord(w):
    if w.isalpha(): return True
    return False
def ReadPercenNumber(s):
    m = re.search(r'(\S+)\s+(\S+)\s*%', s)
    while m:
        w1 = m.group(1)
        w2 = m.group(2)
        transfered = False
        if IsRealNumber(w2):
            if IsEnglishWord(w1):
               w2 = mipinumreader.ReadNormalNumberWithBilingual(w2, 'english')
               w2 = w2 + ' percent'
               transfered = True
            if IsChineseWord(w1):
                w2 = '百 分 之 '
                w2 += mipinumreader.ReadNormalNumberWithBilingual(m.group(2), 'chinese')
                transfered = True
        logger.info("number={0}, target={1}, utterance={2}".format(m.group(2), w2, s))
        if transfered:
            s = s.replace('{0}%'.format(m.group(2)), w2)
        else: break
        m = re.search(r'(\S+)\s+(\S+)%', s)
    return s
        
        
    
def SegmentChineseUtterance (s):
    s = re.sub(r"([\u4e00-\u9fa5])([\u4e00-\u9fa5])", r' \1 \2 ', s)
    s = re.sub(r"([\u4e00-\u9fa5])([A-Za-z0-9])", r'\1 \2', s)
    s = re.sub(r"([A-Za-z0-9])([\u4e00-\u9fa5])", r'\1 \2', s)
    return s
def ReadContextIndependentNumber(snum):
    m = re.match(r'^\d+$', str(snum))
    if not m: return snum
    nDigit = len(str(snum))
    if nDigit < 6:
        return snum
    digitList = list(str(snum))
    for idx, digit in enumerate(digitList):
        digitList[idx] = mipinumreader.ReadPositiveInChinese(digit)
    snum = ' '.join(digitList)
    return snum
def ReadTimeNumber(s):
    m = re.search(r'(\d+:\d+)\s+([\u4e00-\u9fa5])', s)
    while m:
        tnumber = m.group(1)
        tnumList = m.group(1).split(':')
        thour = mipinumreader.ReadPositiveInChinese(int(tnumList[0]))
        thour = re.sub(r'^一\s+十','十',thour) 
        ratio = False
        if int(tnumList[0]) >= 24 or int(tnumList[1]) >= 60 or re.match(r'\d{1}$', tnumList[0]) and re.match(r'\d{1}$', tnumList[1]):
            ratio = True
        if not ratio:    
            thour += ' 点'
        else: thour += ' 比 '
        tminute = mipinumreader.ReadPositiveInChinese(int(tnumList[1]))
        tminute = re.sub(r'^一\s+十','十',tminute)
        if ratio == False and int(tnumList[1]) > 0:
            thour += ' ' + tminute + ' 分'
        elif ratio: thour +=  tminute
        s = s.replace(m.group(1), thour)
        # logger.info('ntime={0}, ctime={1}'.format(m.group(1), thour))
        m = re.search(r'(\d+:\d+)\s+([\u4e00-\u9fa5])', s)
    m = re.search(r'([\u4e00-\u9fa5])\s+(\d+:\d+)', s)
    while m:
        tnumber = m.group(2)
        tnumList =m.group(2).split(':')
        thour = mipinumreader.ReadPositiveInChinese(int(tnumList[0]))
        thour = re.sub(r'^一\s+十','十',thour)
        ratio = False
        if int(tnumList[0]) >= 24 or int(tnumList[1]) >= 60 or re.match(r'\d{1}$', tnumList[0]) and re.match(r'\d{1}$', tnumList[1]):
            ratio = True
        if not ratio:
            thour += ' 点'
        else: thour += ' 比 '
        tminute = mipinumreader.ReadPositiveInChinese(int(tnumList[1]))
        tminute = re.sub(r'^一\s+十','十',tminute)
        if ratio == False and int(tnumList[1]) > 0:
            thour +=' ' + tminute + ' 分'
        elif ratio: thour +=  tminute
        s = s.replace(m.group(2), thour)
        # logger.info('ntime1={0}, ctime1={1}'.format(m.group(2), thour))
        m = re.search(r'([\u4e00-\u9fa5])\s+(\d+:\d+)', s)
    return s
        
def ConvertArabicToChineseNumber(s):
    m = re.search(r'(\d+)\s+([\u4e00-\u9fa5])', s)
    while m:
        anumber = int(m.group(1))
        cnumber = mipinumreader.ReadYearNumber(m.group(1))
        if cnumber == m.group(1):
            cnumber = ReadContextIndependentNumber(m.group(1))
        if cnumber == m.group(1):
            cnumber = mipinumreader.ReadPositiveInChinese(anumber)
        # logger.info("anumber={0}, cnumber={1}".format(anumber, cnumber))
        cnumber1 = SegmentChineseUtterance(cnumber)

        s = s.replace(m.group(1), cnumber1)
        m = re.search(r'(\d+)\s+([\u4e00-\u9fa5])', s)
    m = re.search(r'([\u4e00-\u9fa5])\s+(\d+)', s)
    while m:
        anumber = int(m.group(2))
        cnumber = mipinumreader.ReadYearNumber(m.group(2))
        if cnumber == m.group(2):
            cnumber = ReadContextIndependentNumber(m.group(2))
        if cnumber == m.group(2):
            cnumber = mipinumreader.ReadPositiveInChinese(anumber)
        cnumber1 = SegmentChineseUtterance(cnumber)
        s = s.replace(m.group(2), cnumber1)
        m = re.search(r'([\u4e00-\u9fa5])\s+(\d+)', s)
    return s

def NormalizeText(args):
    if args.text_file:
        global ostream
        if args.tgtdir:
            ostream = open("{0}/text.normalized".format(args.tgtdir), 'w', encoding='utf-8')
        lineNum = 0
        with open(args.text_file, encoding='utf-8') as istream:
            for line in istream:
                line = line.strip()
                if not line: continue
                oldLine = line
                line = ConvertT2SChinese(line)
                line = SegmentChineseUtterance(line)
                line = NormalizeWhiteSpace(line)
                line = ConvertYear(line)
                line = ReadTimeNumber(line)
                line = ReadPercenNumber(line)
                line = ConvertArabicToChineseNumber(line)
                # logger.info("oldLine={0}\nline={1}".format(oldLine, line))
                if args.tgtdir and line:
                    ostream.write("{0}\n".format(line))
                lineNum += 1
                if(lineNum%1000 ==0):
                    logger.info("Processed line {0}".format(lineNum))
        if args.tgtdir:
            ostream.close()
                

def TransferMipitalkMobileData(args):
    oovNormalizer =OovNormalizer(args)
    oovNormalizer.LoadDicts()
    outputLines = str()
    arabicLex = dict()
    if args.text_file:
        with open(args.text_file, encoding='utf-8') as istream:
            for line in istream:
                retList = oovNormalizer.GetLabelAndWords(line, True)
                if not retList: continue
                label = retList[0]
                words = retList[1]
                words = oovNormalizer.TransferWords(words)
                outputLines += label + ' ' + words + "\n"
                for word in words.split():
                    if not word: continue
                    if re.match(r'(\d+(\.\d+)?)', word):
                        m = re.match(r'(\d+(\.\d+)?)', word)
                        curword = m.group(1)
                        # logger.info("curword={0}".format(curword))
                        oovNormalizer.WriteArabicNumberLexicon(word, arabicLex)
                    elif oovNormalizer.IsOovWord(word):
                        oovNormalizer.AddWordToOovDict(word)
    if args.tgtdir:
        ostream = open("{0}/text".format(args.tgtdir), 'w', encoding='utf-8')
        ostream.write(outputLines)
        ostream.close()
        oovStrs = oovNormalizer.DumpOovDict()
        ostream = open("{0}/oov.txt".format(args.tgtdir), 'w', encoding='utf-8')
        ostream.write(oovStrs)
        ostream.close()
        ostream = open("{0}/arabic-lexicon.txt".format(args.tgtdir), 'w', encoding='utf-8')
        s = oovNormalizer.DumpWordDict2(arabicLex)
        ostream.write(s)
        ostream.close()
def DumpOovOfText(args):
    oovNormalizer =OovNormalizer(args)
    oovNormalizer.LoadDicts()
    ostream_sel = None
    ostream_rmv = None
    if args.tgtdir:
        ostream_sel = open("{0}/text-selected.txt".format(args.tgtdir), 'w', encoding='utf-8')
        ostream_rmv = open("{0}/text-removed.txt".format(args.tgtdir), 'w', encoding = 'utf-8')
    removed_text = str()
    if args.text_file:
        with open(args.text_file, encoding='utf8') as istream:
            for line in istream:
                retList = oovNormalizer.GetLabelAndWords(line, True)
                if not retList: continue
                num_of_oov = oovNormalizer.AddWordsToOovDict(retList[1])
                if num_of_oov >= args.oov_thresh_in_utterance:
                    if args.tgtdir: ostream_rmv.write("{0} {1}\n".format(retList[0], retList[1]))
                elif args.tgtdir: ostream_sel.write("{0} {1}\n".format(retList[0], retList[1]))
        oovWords = oovNormalizer.DumpOovDict()
    if args.tgtdir:
        ostream = open("{0}/oov-count.txt".format(args.tgtdir), 'w', encoding='utf-8')
        ostream.write("{0}".format(oovWords))
        ostream.close()
        ostream_sel.close()
        ostream_rmv.close()
    
"""
  begin functions to do test
"""
def ReadChineseNumberTest(oovNormalizer):
    from random import seed, randint, uniform
    random.seed(777)
    for x in [randint(0,10000) for p in range(100)]:
        verbatim1 = mipinumreader.ReadNormalNumberWithBilingual(str(x), 'chinese')
        verbatim2 = mipinumreader.ReadNormalNumberWithBilingual(str(x), 'english')
        print("number = {0}, verbatim = {1} ({2})".format(x, verbatim1,verbatim2))
        verbatims = mipinumreader.ReadNormalNumberInEnglishMutable(str(x))
        print ("{0}".format(verbatims))
    for x in [uniform(0, 10) for p in range(100)]:
        verbatim1 = mipinumreader.ReadNormalNumberWithBilingual(str(x), 'chinese')
        verbatim2 = mipinumreader.ReadNormalNumberWithBilingual(str(x), 'english')
        print("number = {0}, verbatim = {1} ({2})".format(x, verbatim1, verbatim2))
    logger.info("test ReadNormalNumberInChineseMutable")
    for x in  ['0.15', '3.4']:    # ['210', '120', '12589456', '15', '1400', '1040', '012', '0.15', '3.4']:
        # verbatims = mipinumreader.ReadNormalNumberInChineseMutable(x)
        # print("{0}".format(verbatims))
        # verbatims = mipinumreader.ReadNormalNumberInEnglishMutable(x)
        # print("{0}".format(verbatims))
        xDict = dict()
        oovNormalizer.WriteArabicNumberLexicon(x, xDict)
        for word in xDict:
            for pron in xDict[word]:
                print("{0}\t{1}".format(word, pron))
    for x in ['一千二百', '一千二百四十六', '一千二百三十一', '壹仟贰佰', '一千二', '一千两百零五', '一千〇二']:
        number = mipinumreader.ReadThousandLevelNumber(x)
        print ("chinese={0}, number={1}".format(x, number))
    for x in ['一万四千','一万零四', '两万二', '两万四千八百七十', '五千万']:
        number = mipinumreader.Read10ThousandLevelNumber(x)
        print ("chinese={0}, number={1}".format(x, number))
    for x in ['一亿五千万','一亿零两百万']:
        number = mipinumreader.ReadUpToBillionNumber(x)
        print ("chinese={0}, number={1}".format(x, number))
    for x in ['幺三零九七八七','五十五', '一亿五千万', '柒佰贰拾', '五万三千二百']:
        number = mipinumreader.ReadChineseNumber(x)
        print ("chinese={0:20}, number={1}".format(x, number))
    for utterance in ['新加坡 香格里拉 酒店 均价　是　六百　新币　每晚', '最高价　三千　二百　五十五', '评价　三　点　四　分']:
        newUtterance = mipinumreader.TransferChineseNumberInUtterance(utterance)
        print("utterance = {0}, newUtterance = {1}".format(utterance, newUtterance))
def ChineseSegmentationTest():
    with open("/home2/wjc505/mipitalk_data/mobile-dat-sept07/text", encoding='utf-8') as istream:
        for line in istream:
            line0 = line
            if re.search(r'[\u4e00-\u9fa5]', line):
                line = re.sub(r"([A-Za-z0-9])([\u4e00-\u9fa5])", r'\1 \2', line)
                line = re.sub(r"([\u4e00-\u9fa5])([A-Z]a-z0-9)", r'\1 \2', line)
                print("line={0}    updated_line={1}".format(line0, line))
def TestLabelPronunciationForWordSequence(args):
    oovNormalizer = OovNormalizer(args)
    oovNormalizer.LoadDicts()
    s = str()
    for wordSequence in ['i love games', 'basketball']:
        wordList = wordSequence.split()
        s2sDict = oovNormalizer.LabelPronunciationForWordSequence(wordList)
        logger.info("pronDict={0}".format(s2sDict))
        for word in s2sDict:
            for pron in s2sDict[word]:
                s += "{0}\t{1}\n".format(word, pron)
    print ("{0}".format(s))
def TestConvertEnglishNumberInUtterance(oovNormalizer):
    for utterance in ['the price is three thousand five hundred dollars', 'the price is one hundred point seven eight dollars', 'nineteen eighty']:
        transfered = mipinumreader.TransferEnglishNumberInUtterance(utterance)
        print ("{0}".format(transfered))
"""
    end functions to do test
"""
def TestEntrance(args):
    oovNormalizer = OovNormalizer(args)
    oovNormalizer.LoadDicts()
    if args.test_chinese:
        logger.info("conduct ReadChineseNumberTest")
        ReadChineseNumberTest(oovNormalizer)
    if args.test_english:
        TestConvertEnglishNumberInUtterance(oovNormalizer)
    if args.test_chinese_segmentation:
        ChineseSegmentationTest()
    if args.test_label_pronunciation_for_word_sequence:
        TestLabelPronunciationForWordSequence(args)
    if args.test_read_arabic_number_in_english:
        TestReadArabicNumberInEnglish(args)
def main():
    args = get_args()
    if args.test:
        TestEntrance(args)
    # oovNormalizer = OovNormalizer(args)
    # oovNormalizer.LoadDicts()
    if args.normalize_mipitalk_mobile_data:
        NormalizeMipitalkMobileData(args)
    if args.read_oov_for_transfer_dict:
        oovNormalizer.ReadOovWordList()
    if args.collect_oov_from_transfer_dict:
        oovNormalizer.CollectOovOfTransferDict()
    if args.transfer_text:
        TransferMipitalkMobileData(args)
    if args.dump_oov_of_text:
        DumpOovOfText(args)
    if args.normalize_text:
        NormalizeText(args)

if __name__ == "__main__":
    main()
