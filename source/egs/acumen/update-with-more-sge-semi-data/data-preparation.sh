#!/bin/bash 

. path.sh
. cmd.sh

echo
echo "## LOG: $0 $@"
echo

# begin options
cmd='slurm.pl --quiet --gres=gpu:1'
steps=

# end options

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 [Example]: $0 --steps 1   \
 /data/users/lyvt/SingaporeEnglish_database/summary_filepath.txt \
 /home3/hhx502/w2017/acumen/update-with-more-sge-semi-data-23-aug
EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 2 ]; then
  Example && exit 1
fi

srcdata=$1
tgtdir=$2

datadir=$tgtdir/data/preparation
[ -d $datadir ] || mkdir -p $datadir
if [ ! -z $step01 ]; then
  cat $srcdata | \
  source/egs/acumen/update-with-more-sge-semi-data/make-wave-and-textgrid-list.pl /data/users/lyvt/SingaporeEnglish_database  $datadir
  echo "## LOG (step01, $0): done with '$datadir'"
fi

if [ ! -z $step02 ]; then
  
fi
