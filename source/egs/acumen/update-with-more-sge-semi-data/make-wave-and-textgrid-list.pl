#!/usr/bin/perl -w
use utf8;
use strict;
use open qw(:std :utf8);
my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  die "\nExample cat /data/users/lyvt/SingaporeEnglish_database/summary_filepath.txt |$0 /data/users/lyvt/SingaporeEnglish_database /home3/hhx502/w2017/acumen/update-with-more-sge-semi-data-23-aug/data/preparation\n\n";
}
my ($srcdatadir, $tgtdir) = @ARGV;
my $labname = '';
my $textgrid = '';
my $wavfile = '';
`[ -d $tgtdir ] || mkdir -p $tgtdir`;
open(WAVLIST, ">$tgtdir/wavlist.txt") or die;
open(TEXTLIST, ">$tgtdir/textgridlist.txt") or die;
while(<STDIN>) {
  chomp;
  if(/\"(.*)\":\s*\{/) {
     $labname = $1;
   } elsif(/\"textgrid_filepath\":\s*\"(.*)\"/) {
     $textgrid = $1;
   } elsif (/\"wav_filepath\":\s*\"(.*)\"/) {
     $wavfile = $1;
     print WAVLIST "$srcdatadir/$wavfile\n";
     die if ($textgrid eq '');
     print TEXTLIST "$srcdatadir/$textgrid\n";
     $textgrid = '';
   }
}

close WAVLIST;
close TEXTLIST;
