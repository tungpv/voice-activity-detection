#!/cm/shared/apps/python3.5.2/bin/python3.5
# -*- coding: utf-8 -*-

# Copyright 2017 mipitalk
#           2017 Zhiping Zeng
#           2017 Haihua Xu
from __future__ import print_function
import argparse
import sys
import os
import re
import numpy as np
import logging
import random
# sys.path.insert(0, 'steps')
# import libs.common as common_lib
sys.path.insert(0,'source/egs')
import python_libs.mipireadnumber as mipinumreader

logger = logging.getLogger('python_libs')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s [%(pathname)s:%(lineno)s - "
                              "%(funcName)s - %(levelname)s ] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting select-data-from-ctm-with-confidence-score.py')

# begin utility
def InsertDict(key, value, tgtDict):
    if key in tgtDict:
        logger.warn("duplicated key '{0}'".format(key))
        return
    tgtDict[key] = value
def LoadSegments(args, segmentDict):
    if args.source_data_dir:
        segments = "{0}/segments".format(args.source_data_dir)
        if os.path.exists(segments):
            with open(segments, encoding='utf-8') as istream:
                for line in istream:
                    segList = line.split()
                    if len(segList) != 4:
                        raise Exception("bad line: {0}".format(line))
                    segId = segList.pop(0)
                    InsertDict(segId, segList, segmentDict)
            return
    raise Exception("Cannot find 'segments' in folder 'source_data_dir'")
def LoadUtt2Spk(args, utt2spkDict):
    if args.source_data_dir:
        utt2spk = "{0}/utt2spk".format(args.source_data_dir)
        if os.path.exists(utt2spk):
            with open(utt2spk, encoding='utf-8') as istream:
                for line in istream:
                    utt2spkList = line.split()
                    if len(utt2spkList) != 2:
                        raise Exception("bad line: {0}".format(line))
                    utt = utt2spkList.pop(0)
                    spk = utt2spkList.pop(0)
                    InsertDict(utt, spk, utt2spkDict)
            return
    raise Exception("cannot find 'utt2spk' in folder 'source_data_dir'")
# end utility
def get_args():
    parser = argparse.ArgumentParser(description='Arguments_parser')
    parser.add_argument('--source-data-dir', dest='source_data_dir',  type=str, help='source data folder')
    parser.add_argument('--ctm-file', dest='ctm_file', type=str, help='ctm file with confidence score')
    parser.add_argument('--target-data-dir', dest='target_data_dir', type=str, help='target data folder to be built')
    parser.add_argument('--word-confidence-score-threshold', dest='word_confidence_score_threshold', type=float, default=0.9, help='word confidence score threshold')
    parser.add_argument('--min-sec-of-selected-segment', dest='min_sec_of_selected_segment', type=float, default=1.5, help='minimum second of selected segment')
    parser.add_argument('--test', dest='test', action='store_true')
    return parser.parse_args()

def Test(args):
    pass
def DumpSelectedSegment(segmentDict, utt2spkDict, args, selectedList):
    if len(selectedList) < 1:
        return
    duration = 0
    uttName = str()
    start_in_sec = 0
    end_in_sec = 0
    nWord = len(selectedList)
    text = str()
    updated_list =list()
    for nIndex, utterance in enumerate(selectedList):
        uttList = utterance.split()
        text += uttList[4] + ' '
        if nIndex == 0: 
            uttName = uttList[0]
            start_in_sec = float(uttList[2])
        if nIndex == nWord-1:
            end_in_sec = float(uttList[2]) + float(uttList[3])
        duration += float(uttList[3])
    if duration <= args.min_sec_of_selected_segment:
        return None
    if not uttName in segmentDict:
        raise Exception("utterance '{0}' not seen in segments".format(uttName))
    segmentList = segmentDict[uttName]
    updated_start_in_sec = "{0:.2f}".format(float(segmentList[1]) +  start_in_sec)
    updated_end_in_sec = "{0:.2f}".format(float(segmentList[1]) + end_in_sec)
    # print ("end_in_sec={0:.2f}".format(float(end_in_sec)))
    updated_utt_name = "{0}-{1:05d}".format(uttName, int(float(end_in_sec)*100))
    # print ("updated_start_in_sec={0:.2f}".format(float(updated_end_in_sec)))
    updated_segment_line = "{0} {1} {2:.2f} {3:.2f}\n".format(updated_utt_name, segmentList[0], float(updated_start_in_sec), float(updated_end_in_sec))
    if not uttName in utt2spkDict:
        raise Exception("utterance '{0}'not seen in utt2spk".format(uttName))
    spk = utt2spkDict[uttName]
    updated_utt2spk_line = "{0} {1}\n".format(updated_utt_name, spk)
    updated_text = updated_utt_name + ' ' + text.strip() + "\n"
    updated_list.append(updated_segment_line)
    updated_list.append(updated_utt2spk_line)
    updated_list.append(updated_text)
    return updated_list

def ProcessUtterance(segmentDict, utt2spkDict, args, uttList):
    selectedList = list()
    updated_list =list()
    for utterance in uttList:
        uttList = utterance.split()
        uttName = uttList[0]
        confidence = uttList[5]
        if float(confidence) >= args.word_confidence_score_threshold:
            selectedList.append(utterance)
        else:
            retList = DumpSelectedSegment(segmentDict, utt2spkDict, args, selectedList)
            if retList:
                updated_list.append(retList)
            del selectedList[:]
    retList = DumpSelectedSegment(segmentDict, utt2spkDict, args,selectedList)
    if retList:
        updated_list.append(retList)
    return updated_list
            
def SelectDataFromCTMFile(args):
    segmentDict = dict()
    utt2spkDict = dict()
    LoadSegments(args, segmentDict)
    LoadUtt2Spk(args, utt2spkDict)
    if not args.target_data_dir:
        raise Exception("target dir '{0}' is not ready".format(args.target_data_dir))
    if not os.path.exists(args.target_data_dir):
        try:
            os.makedirs(args.target_data_dir)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise Exception("failed to make folder '{0}'".format(args.target_data_dir))
    seg_ostream = open("{0}/segments".format(args.target_data_dir), 'w', encoding='utf-8')
    utt2spk_ostream = open("{0}/utt2spk".format(args.target_data_dir), 'w', encoding='utf-8')
    txt_ostream = open("{0}/text".format(args.target_data_dir), 'w', encoding = 'utf-8')
    if args.ctm_file:
        with open(args.ctm_file, encoding='utf-8') as istream:
            prevUttName = str()
            uttList = list()
            for line in istream:
                ctmList = line.split()
                if len(ctmList) != 6:
                    raise Exception("bad line '{0}'".format(line))
                if ctmList[0] != prevUttName and prevUttName:
                   retList = ProcessUtterance(segmentDict, utt2spkDict, args, uttList)
                   for xList in retList:
                       seg_ostream.write(xList[0])
                       utt2spk_ostream.write(xList[1])
                       txt_ostream.write(xList[2])
                   del uttList[:]
                uttList.append(line)
                prevUttName = ctmList[0]
            if uttList:
                retList = ProcessUtterance(segmentDict, utt2spkDict, args, uttList)
                for xList in retList:
                    seg_ostream.write(xList[0])
                    utt2spk_ostream.write(xList[1])
                    txt_ostream.write(xList[2])
    seg_ostream.close()
    utt2spk_ostream.close()
    txt_ostream.close()
def main():
    args = get_args()
    if args.test:
        Test(args)
    SelectDataFromCTMFile(args)

if __name__ == "__main__":
    main()
