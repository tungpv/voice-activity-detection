#!/bin/bash

echo
echo "## LOG: $0 $@"
echo

. path.sh
. cmd.sh

# begin option
cmd='slurm.pl --quiet'
nj=120
steps=
# end options


function Example {
  cat<<EOF
 $0 --steps 1 --cmd "$cmd" --nj $nj \
 ../acumen/update-sept-23-2017/data/semi_ntu/mfcc-pitch \
 ../acumen/update-sept-23-2017/data/semi_ntu/mfcc-hires \
 ../acumen/update-sept-26-2017/data/lang-silprob \
 ../acumen/update-sept-26-2017/exp/tri4a \
 ../acumen/update-sept-26-2017/exp/tdnn

EOF
 
}

. utils/parse_options.sh || exit 1

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if [ $# -ne 5 ]; then
  Example && exit 1
fi

train_mfcc_pitch=$1
train_mfcc_hires=$2
lang=$3
srcdir_gmm=$4
tgtdir=$5

[ -d $tgtdir ] || mkdir -p $tgtdir


train_dir=$(dirname $train_mfcc_hires)
min_utt_len=5.0
if [ ! -z $step01 ]; then
  for sdata in $train_mfcc_pitch $train_mfcc_hires; do
    data=$train_dir/subset-to-train-ivector/$(basename $sdata);
    [ -d $data ] || mkdir -p $data 
    cat $sdata/segments | \
    awk -v thresh=$min_utt_len '{ if($4-$3 >= thresh){print $1 ;} }' > $data/utt-list
    subset_data_dir.sh --utt-list $data/utt-list $sdata $data
    echo "## LOG (step01): subset data '$data' generated"
  done
fi

train_mfcc_pitch_ivector=$train_dir/subset-to-train-ivector/$(basename $(echo $train_mfcc_pitch| sed 's:/$::g'))
train_mfcc_hires_ivector=$train_dir/subset-to-train-ivector/$(basename $(echo $train_mfcc_hires| sed 's:/$::g'))
ali_for_ivector=$srcdir_gmm/ali_train-for-ivector
if [ ! -z $step02 ]; then
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj \
  $train_mfcc_pitch_ivector $lang \
  $srcdir_gmm $ali_for_ivector || exit 1
  echo "## LOG (step02): done with '$ali_for_ivector' @ `date`"
fi
expdir=$(dirname $srcdir_gmm)
nnetdir=$expdir/tdnn
transform_dir=$nnetdir/lda-mllt-transform-for-ivector-extractor
if [ ! -z $step03 ]; then
  steps/train_lda_mllt.sh --cmd "$cmd"  --num-iters 13 \
  --realign-iters "" \
  --splice-opts "--left-context=3 --right-context=3" \
  8000 80000 $train_mfcc_hires_ivector  $lang \
  $ali_for_ivector $transform_dir || exit 1
  echo "## LOG (step72): done with '$transform_dir' @ `date`"
fi
if [ ! -z $step04 ]; then
  steps/online/nnet2/train_diag_ubm.sh --cmd "$cmd" --nj $nj \
  --num-threads 2 \
   --num-frames 720000 $train_mfcc_hires_ivector  512  $transform_dir $nnetdir/diag_ubm || exit 1
  echo "## LOG (step04): done with '$nnetdir/diag_ubm' @ `date`"
fi
ivector_extractor=$nnetdir/ivector-extractor
if [ ! -z $step05 ]; then
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$cmd" --nj $nj \
  --num-threads 1 --num-processes 1 \
  $train_mfcc_hires_ivector  $nnetdir/diag_ubm $ivector_extractor || exit 1;
  echo "## LOG (step05): done with '$ivector_extractor' @ `date`"
fi

echo "## LOG ($0): done !"
