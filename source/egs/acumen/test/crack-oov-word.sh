#!/bin/bash

. path.sh
. cmd.sh

# begin option
steps=
test=
# end options


. parse_options.sh || exit 1

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi


if [ ! -z $step01 ]; then
  source/egs/acumen/crack-oov-word.py ${test:+--test} \
  --read-oov-for-transfer-dict \
 --oov-word-count-file ../acumen/update-with-more-sge-semi-data-23-aug/data/preparation/oov-count.txt  --word-lexicon-file /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/lang/words.txt \
--text-file ../acumen/update-with-more-sge-semi-data-23-aug/data/preparation/text > ../acumen/update-with-more-sge-semi-data-23-aug/data/preparation/oov-label-dict.txt
echo "## LOG ($0) done with '../acumen/update-with-more-sge-semi-data-23-aug/data/preparation/oov-label-dict.txt'"

fi
tgtdir=../acumen/update-with-more-sge-semi-data-23-aug/data/preparation
if [ ! -z $step02 ]; then
  source/egs/acumen/crack-oov-word.py --collect-oov-from-transfer-dict \
  --transfer-dict ../acumen/update-with-more-sge-semi-data-23-aug/data/preparation/oov-label-dict.txt \
  --word-lexicon-file  /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/lang/words.txt \
  > $tgtdir/oov-in-transfer-dict.txt
  echo "## LOG (step02): done & check '$tgtdir/oov-in-transfer-dict.txt'"
fi

if [ ! -z $step03 ]; then
   head -1854  $tgtdir/oov-in-transfer-dict.txt  | awk '{print $2; }' > $tgtdir/to-be-transcribed-by-human.txt
   utils/shuffle_list.pl  ../acumen/update-with-more-sge-semi-data-23-aug/data/preparation/to-be-transcribed-by-human.txt  | head -927 > $tgtdir/to-be-transcribed-by-human-part01.txt
   utils/shuffle_list.pl  ../acumen/update-with-more-sge-semi-data-23-aug/data/preparation/to-be-transcribed-by-human.txt  | tail -n +928 > $tgtdir/to-be-transcribed-by-human-part02.txt
   echo "## LOG (step03): done & check '$tgtdir/to-be-transcribed-by-human-part02.txt'"
fi
if [ ! -z $step04 ]; then
 PYTHONIOENCODING=utf-8  \
 source/egs/acumen/crack-oov-word.py --test  --test-label-pronunciation-for-wrod-sequence \
 --word-lexicon-file   source/egs/acumen/test/lexicon.txt
fi
