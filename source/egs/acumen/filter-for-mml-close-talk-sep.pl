#!/usr/bin/perl -w
use strict;
use utf8;
use open qw(:std :utf8);
# begin sub
sub RemoveUttUnit {
  my ($s) = @_;
  $$s =~ s:<[bcs]\/>::g;
}
sub RemoveSpokenNoiseWord {
  my ($s) = @_;
  $$s =~ s:\(pp[lob]\)::g;
}
sub RemoveUnintelligible {
  my ($s) = @_;
  $$s =~ s:\#::g;
}
sub DecomposeWordWithDash {
  my ($s) = @_;
  $$s =~ s:[\-_]: :g;
}
sub Trim {
  my ($s) = @_;
  $$s =~ s/^\s+|\s+$//g;
}
sub RemoveUtterance {
  my ($s) = @_;
  Trim($s);
  if ($$s eq '') {
    return;
  }
  my @A = split(" ", $$s);
  my $nNoncontent = 0;
  my $nTotal = 0;
  for(my $i = 0; $i < scalar @A; $i ++) {
    if($A[$i] =~ /[\(\[\<]/) {
      $nNoncontent ++;
    }
    $nTotal ++;
  }
  if($nNoncontent == $nTotal) {
    $$s = '';
  }
}
sub RemovePunctuation {
  my ($s) = @_;
  $$s =~ s:[\(\)\[\]\~]::g;
}
# end sub
print STDERR "## LOG ($0): stdin expected\n";
while(<STDIN>) {
  chomp;
  m:(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(.*)$:m or next;
  my ($uttId, $wavId, $start, $end, $words) = ($1, $2, $3, $4, lc $5);
  my $label = sprintf("%s %s %.2f %.2f", $uttId, $wavId, $start, $end);
  next if($words =~ /\-\-empty\-\-/);
  RemoveUttUnit(\$words);
  RemoveSpokenNoiseWord(\$words);
  RemoveUnintelligible(\$words);
  DecomposeWordWithDash(\$words);
  RemoveUtterance(\$words);
  if($words eq '') {
    print STDERR "## WARNING ($0): '$_' is removed !\n";
    next;
  }
  RemovePunctuation(\$words);
  my $utt = $label . ' ' . $words;
  print "$utt\n";
}
print STDERR "## LOG ($0): stdin ended\n";
