#!/bin/bash

. path.sh
. cmd.sh

# begin option
cmd="slurm.pl --quiet"
nj=4
steps=
# end option

. parse_options.sh || exit 1

function Example {
 cat<<EOF
 $0 --steps 1 \
  /data/users/zin/data/mml-recording/mml-feb-2017/close-talk/mml-feb-recording/7_manual \
 /data/users/zin/data/mml-recording/mml-feb-2017/close-talk/mml-feb-recording/0_raw \
 ../acumen/update-sept-26-2017/data/mml-close-talk-sep-27-2017

EOF
}


steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 3 ]; then
  Example && exit 1
fi

srctext=$1
srcwav=$2
tgtdir=$3

tmpdir=$tgtdir/temp
[ -d $tmpdir ] || mkdir -p $tmpdir

if [ ! -z $step01 ]; then
  find $srctext -name "*.TextGrid" > $tmpdir/textgrid-list.txt
  find $srcwav -name "*.wav" > $tmpdir/wav-list.txt
fi

if [ ! -z $step02 ]; then
  ./source/egs/acumen/textgrid-cracker.pl $tmpdir/textgrid-list.txt mml-close-talk $tmpdir > $tmpdir/transcript.txt
fi

if [ ! -z $step03 ]; then
  source/egs/acumen/make-mml-close-talk-sep-27-20170-kaldi-data.pl  "cat $tmpdir/transcript.txt |source/egs/acumen/filter-for-mml-close-talk-sep.pl |"  $tmpdir/wav-list.txt \
    $tgtdir
  utils/fix_data_dir.sh
  echo "## LOG (step03): done '$tgtdir'"
fi

if [ ! -z $step04 ]; then
  for sdata in $tgtdir; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch/data; log=$sdata/feat/mfcc-pitch/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    utils/fix_data_dir.sh $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step04): done with '$data' @ `date`"
    data=$sdata/mfcc-hires feat=$sdata/feat/mfcc-hires/data log=$sdata/feat/mfcc-hires/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    echo "## LOG (step04): done with '$data' @ `date`"
  done
fi
dev_mfcc_hires=$tgtdir/mfcc-hires
nnetdir=../acumen/update-sept-26-2017/exp/tdnn
ivector_extractor=../acumen/update-sept-26-2017/exp/tdnn/ivector-extractor
dev_ivectors=$nnetdir/ivectors-mml-close-talk-sep-27-2017
chaindir=$nnetdir/chain1024
graph=$chaindir/graph
dev_name=mml-close-talk-sep-27-2017
if [ ! -z $step05 ]; then
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj 3 $dev_mfcc_hires \
  $ivector_extractor $dev_ivectors || exit 1
fi
if [ ! -z $step06 ]; then
  echo "## LOG (step06): decoding started @ `date`"
  steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
  --nj 4 --cmd "$cmd" \
  --online-ivector-dir $dev_ivectors \
  $graph $dev_mfcc_hires $chaindir/decode-${dev_name} || exit 1;
  echo "## LOG (step06): done @ `date`"
fi
nnetdir=../acumen/update-sept-23-2017/exp/tdnn
chaindir=$nnetdir/chainname
ivector_extractor=$nnetdir/ivector-extractor
dev_ivectors=$nnetdir/ivectors-mml-close-talk-sep-27-2017
graph=$chaindir/graph
if [ ! -z $step07 ]; then
  steps/online/nnet2/extract_ivectors_online.sh --cmd "$cmd" --nj 4 $dev_mfcc_hires \
  $ivector_extractor $dev_ivectors || exit 1
fi
if [ ! -z $step08 ]; then
  steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
  --nj 4 --cmd "$cmd" \
  --online-ivector-dir $dev_ivectors \
  $graph $dev_mfcc_hires $chaindir/decode-${dev_name} || exit 1;

fi

if [ ! -z $step09 ]; then
  for sdata in $tgtdir; do
    data=$sdata/fbank-pitch; feat=$sdata/feat/fbank-pitch/data; log=$sdata/feat/fbank-pitch/log
    steps/make_fbank_pitch.sh --cmd "$cmd" --nj 4 \
    --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf \
    $data $log  $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
  done
fi
dev_data=$tgtdir/fbank-pitch
graph=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938/exp/tri4a/graph-with-dict-from-ali
nnetdir=../acumen/update-aug-17-with938-50hrs/exp/mpe-nnet
if [ ! -z $step10 ]; then
  steps/nnet/decode.sh --cmd 'slurm.pl --quiet --gres=gpu:1' --nj 4 --use-gpu yes  \
  $graph $dev_data $nnetdir/decode-${dev_name} || exit 1
fi
