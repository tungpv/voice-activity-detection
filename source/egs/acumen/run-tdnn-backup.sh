#!/bin/bash

. path.sh
. cmd.sh

# begin sub
cmd='slurm.pl --quiet'
nj=120
steps=
numleaves=7000
hidden_dim=1024
train_stage=-10
get_egs_stage=-10
frames_per_eg=150
minibatch_size=128
num_epochs=4
proportional_shrink=0
chainname=chain1024
num_jobs_initial=3
num_jobs_final=16
remove_egs=false
max_param_change=2.0
initial_effective_lrate=0.001
final_effective_lrate=0.0001
xent_regularize=0.1
remove_egs=false
get_egs_stage=-10
common_egs_dir=
# end sub

. parse_options.sh || exit 1

function Example {
 cat<<EOF

 $0 --steps 1 --cmd "$cmd" --nj $nj \
 ../acumen/data/train-merge-sup/fbank-pitch \
 ../acumen/data/lang \
 ../acumen/update-aug-17-with938-50hrs/exp/mpe-nnet \
 ../acumen/update-aug-17-with938-50hrs

EOF
}

steps=$(echo $steps | perl -e '$steps=<STDIN>;  $has_format = 0;
  if($steps =~ m:(\d+)\-$:g){$start = $1; $end = $start + 10; $has_format ++;}
        elsif($steps =~ m:(\d+)\-(\d+):g) { $start = $1; $end = $2; if($start == $end){}elsif($start < $end){ $end = $2 +1;}else{die;} $has_format ++; }  
      if($has_format > 0){$steps=$start;  for($i=$start+1; $i < $end; $i++){$steps .=":$i"; }} print $steps;' 2>/dev/null)  || exit 1

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 4 ]; then
  Example && exit 1
fi
srcdata=$1
lang=$2
srcdir=$3
tgtdir=$4

lang=../acumen/data/lang-update-sept-19-2017
if [ ! -z $step01 ]; then
  utils/prepare_lang.sh /home2/hhx502/sge2017/update-lm-lexicon-july-13-2017/dict  "<unk>" \
  $lang/tmp $lang || exit 1
fi
tmpdir=../acumen/data/temp-data
trainbig=../acumen/data/train-merge-semi
if [ ! -z $step02 ]; then
  cursrcdata=/home2/hhx502/sge2017/update-dec-04-2016-with-fm938/data/train-merge/fbank-pitch
  [ -d $tmpdir ] || mkdir -p $tmpdir
  grep -vP 'sge\d+-rev-' $cursrcdata/text > $tmpdir/subtext
  utils/subset_data_dir.sh --utt-list $tmpdir/subtext $cursrcdata  $tmpdir
  utils/data/combine_data.sh $trainbig  $tmpdir ../acumen/data/train 
  rm -rf $tmpdir
  echo " ## LOG (step02): check '$trainbig'"
fi
alidir=$srcdir/ali_train
if [ ! -z $step05 ]; then
  echo "## LOG (step05): proceed with '$alidir' started @ `date`"
  steps/nnet/align.sh  --cmd "$cmd" --nj $nj \
  $srcdata $lang $srcdir $alidir || exit 1
  echo "## LOG (step05): done with '$alidir' ended @ `date`"
fi
if [ ! -z $step06 ]; then
  for sdata in $trainbig ; do
    data=$sdata/mfcc-pitch; feat=$sdata/feat/mfcc-pitch/data; log=$sdata/feat/mfcc-pitch/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    utils/fix_data_dir.sh $data
    steps/make_mfcc_pitch.sh --cmd "$cmd" --nj $nj $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    utils/fix_data_dir.sh $data
    echo "## LOG (step06): done with '$data' @ `date`"
    data=$sdata/mfcc-hires feat=$sdata/feat/mfcc-hires/data log=$sdata/feat/mfcc-hires/log
    [ -d $data ] || mkdir -p $data
    utils/data/copy_data_dir.sh $sdata $data
    steps/make_mfcc.sh --cmd "$cmd" --nj $nj \
    --mfcc-config conf/mfcc_hires.conf $data $log $feat || exit 1
    steps/compute_cmvn_stats.sh $data $log $feat || exit 1;
    echo "## LOG (step06): done with '$data' @ `date`"
  done
fi
