#!/usr/bin/perl 
use warnings;
use strict;
use open qw(:std :utf8);
use utf8;

print STDERR "\n$0 ", join(" ", @ARGV), "\n";
my $nARGV = scalar @ARGV;
if ($nARGV != 2) {
  print STDERR "\nUsage: $0 <source_word_list> <sub_word_list>\n\n";
  exit 1;
}

my ($sWordList, $sSubWordList) = @ARGV;

my %vocab = ();

sub GetGraphemeVocab {
  my ($sFileName, $vocab) = @_;
  open(FILE, "$sFileName") or die "$0: ERROR, file $sFileName cannot open\n";
  while(<FILE>) {
    chomp;  
    m/(\S+)/ or next;  
    my $sWord = $1;
    my @A = split(//,$sWord);
    # print STDERR "$sWord\t", join(" ", @A), "\n";
    for(my $i = 0; $i < @A; $i ++) {
      my $sGrapheme = $A[$i];
      $$vocab{$sGrapheme} ++;
    }
  }
  close FILE; 
}
sub InGraphemeSet {
  my ($vocab, $sWord) = @_;
  my @A = split(//, $sWord);
  for(my $i = 0; $i < @A; $i++) {
    my $sGrapheme = $A[$i];
    if(not exists $$vocab{$sGrapheme}) {
      print STDERR "$sWord\t", join (" ", @A), "\n";
      return 0;
    }
  }
  return 1;
}
GetGraphemeVocab("$sSubWordList", \%vocab);
print STDERR "$0: phone set\n\n";
foreach my $sg (keys %vocab) {
  print STDERR "$sg ";
}
print STDERR "\n\n";
open(FILE, "$sWordList") or 
die "$0: ERROR, file $sWordList cannot open\n";
while (<FILE>) {
  chomp;
  s/^\s+//g;
  m/(\S+)/ or next;
  my $sWord = $1;
  next if InGraphemeSet(\%vocab, lc $sWord) == 0;
  print $sWord, "\n";
}
close FILE;
