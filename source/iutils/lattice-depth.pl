#!/usr/bin/env perl
use warnings;
use strict;
# Example: iutils/lattice-depth.pl "lattice-depth 'ark:gzip -cdf icassp/exp/tandem_llp/tri3c/decode_dev3ha.si/lat.1.gz |' ark,t:-"
my $cmdStr = shift @ARGV;
my $str = $cmdStr;
$str =~ s/\s+//g;
if ($str !~ /.*\|$/) {
  $cmdStr .= " |";
}
open FILE, "$cmdStr" or die "$cmdStr cannot open";
my ($average_depth, $num) = (0, 0);
while (<FILE>) {
  chomp;
  m/(\S+)\s+(\S+)/;
  my ($lab, $depth) = ($1, $2);
  $average_depth += $depth;
  $num ++;
}
print "\naverage_depth=", $average_depth/$num, "\n" if $num > 0;

close FILE;
