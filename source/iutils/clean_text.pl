#!/usr/bin/perl -w
use strict;

while (<STDIN>) {
  chomp;
  my @tarray = split /\s+/;
  my $lab = shift @tarray;
  next if scalar @tarray <= 0;
  my @a = ();
  for (my $idx = 0; $idx < scalar @tarray; $idx ++) {
    my $w = $tarray[$idx];
    next if ($w =~ m/^\[.+\]/|| $w =~ m/^<.+>/);
    push @a, $w;
  }
  if (scalar @a > 0) {
    print "$lab ", join " ",@a, "\n";
  }
}
