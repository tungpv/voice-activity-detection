#!/bin/bash

# koried, 2/13/2012

# Tandem acoustic model training template;  uses settings from config file.
# Models will be stored in features2/ directory (assuming that feature1 is
# the "constant" for all tandem systems

if [ $# -lt 3 ]; then
  echo "Usage: $0 [ options ] config features1 features2"
  exit 1;
fi

. cmd.sh

# config;  change any of the following variables via command line (e.g. --feat-dim 45)
stage=1
num_train=20
exp_dir=exp
lang_dir=data/lang


# what do you want to train
do_sgmm=true
do_mmi=false
do_sgmm_mmi=false

. utils/parse_options.sh

. $1

# make sure we run the right number of cores
if [ -z "$SLURM_NPROCS" ]; then
  if [ $SLURM_NPROCS != $num_train ]; then
    echo "WARNING resetting num_train from $num_train to $SLURM_NPROCS to match slurm allocation"
    num_train=$SLURM_NPROCS
  fi
fi

feats1=$2
feats2=$3

echo slurm job id $SLURM_JOB_ID

echo "stage=$stage"
echo "num_train=$num_train"
echo "exp_dir=$exp_dir"
echo "lang_dir=$lang_dir"

echo `date` Starting training sequence


# don't forget to call with --feat-dim!
if [ $stage -le 1 ]; then
  steps/tandem/train_mono.sh --nj $num_train --cmd "$train_cmd" \
    {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/mono0a || exit 1;
fi

# step up to 20k shortest;  train deltas
if [ $stage -le 2 ]; then
  echo `date` Entering stage 2
  steps/tandem/align_si.sh --nj $num_train --cmd "$train_cmd" \
    {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/mono0a $feats2/$exp_dir/mono0a_ali || exit 1;
  steps/tandem/train_deltas.sh --cmd "$train_cmd" \
    $tp_s2_ns $tp_s2_nd {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/mono0a_ali $feats2/$exp_dir/tri1 || exit 1;
fi

# step up to 37k (about 50% of training data)
if [ $stage -le 3 ]; then
  echo `date` Entering stage 3
  steps/tandem/align_si.sh --nj $num_train --cmd "$train_cmd" \
    {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/tri1 $feats2/$exp_dir/tri1_ali || exit 1 ;
  steps/tandem/train_deltas.sh --cmd "$train_cmd" \
    $tp_s3_ns $tp_s3_nd {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/tri1_ali $feats2/$exp_dir/tri2 || exit 1;
fi

# step up to all data;  train LDA+MLLT system
if [ $stage -le 4 ]; then
  echo `date` Entering stage 4
  steps/tandem/align_si.sh --nj $num_train --cmd "$train_cmd" \
    {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/tri2 $feats2/$exp_dir/tri2_ali || exit 1;
  steps/tandem/train_lda_mllt.sh --cmd "$train_cmd" \
    --splice-opts "$tp_so" \
    $tp_s4_ns $tp_s4_nd {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/tri2_ali $feats2/$exp_dir/tri3a || exit 1;
fi

# re-align all the data, increase states+gaussians
if [ $stage -le 5 ]; then
  echo `date` Entering stage 5
  steps/tandem/align_fmllr.sh --nj $num_train --cmd "$train_cmd" \
    {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/tri3a $feats2/$exp_dir/tri3a_ali || exit 1;
  steps/tandem/train_sat.sh --cmd "$train_cmd" \
    $tp_s5_ns $tp_s5_nd {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/tri3a_ali $feats2/$exp_dir/tri4a || exit 1;
fi

# some more sophisticated models based on tri4a
if [ $stage -le 6 ]; then
  echo `date` Entering stage 6

  # align data using tri4a
  if [ ! -d $feats2/$exp_dir/tri4a_ali ]; then
    echo `date` Aligning training data
    steps/tandem/align_fmllr.sh --nj $num_train --cmd "$train_cmd" \
      {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/tri4a $feats2/$exp_dir/tri4a_ali || exit 1;
  fi

  # sgmm model
  if $do_sgmm; then
    if [ ! -f $feats2/$exp_dir/ubm5a/final.ubm ]; then
      echo `date` Training UBM
      steps/tandem/train_ubm.sh --cmd "$train_cmd" $tp_s6_ubm {$feats1,$feats2}/data/train $lang_dir \
        $feats2/$exp_dir/tri4a_ali $feats2/$exp_dir/ubm5a || exit 1;
    fi
    echo `date` Training SGMM system
    steps/tandem/train_sgmm2.sh --cmd "$train_cmd" \
      $tp_s6_ns $tp_s6_nd {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/tri4a_ali \
      $feats2/$exp_dir/ubm5a/final.ubm $feats2/$exp_dir/sgmm5a || exit 1;
  fi

  # MMI trainig
  if $do_mmi; then
    echo `date` Making dense lattices
    steps/tandem/make_denlats.sh --nj $num_train --cmd "$decode_cmd" --config conf/decode.config \
      --transform-dir $feats2/$exp_dir/tri4a_ali \
      {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/tri4a $feats2/$exp_dir/tri4a_denlats || exit 1;

    echo `date` Starting MMI training
    steps/tandem/train_mmi.sh --cmd "$decode_cmd" --boost 0.1 \
      --transform-dir $feats2/$exp_dir/tri4a_ali \
      {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/tri4a_{ali,denlats} $feats2/$exp_dir/tri5a_mmi_b0.1 || exit 1;
  fi

  # SGMM/MMI training
  if $do_sgmm_mmi; then
    echo `date` Aligning training data with SGMM
    steps/tandem/align_sgmm2.sh --nj $num_train --cmd "$train_cmd" --use-graphs true --use-gselect true \
      --transform-dir $feats2/$exp_dir/tri4a_ali \
      {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/sgmm5a $feats2/$exp_dir/sgmm5a_ali || exit 1;

    # Took the beam down to 8 to get acceptable decoding speed.
    echo `date` Making dense lattices
    steps/tandem/make_denlats_sgmm2.sh --nj $num_train --beam 8.0 --lattice-beam 6 --cmd "$decode_cmd" \
      --transform-dir $feats2/$exp_dir/tri4a_ali \
      {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/sgmm5a_{ali,denlats} || exit 1;

    echo `date` Starting SGMM MMI training
    steps/tandem/train_mmi_sgmm2.sh --cmd "$decode_cmd" --boost 0.1 \
      --transform-dir $feats2/$exp_dir/tri4a_ali \
      {$feats1,$feats2}/data/train $lang_dir $feats2/$exp_dir/sgmm5a_{ali,denlats,mmi_b0.1} || exit 1;
  fi
fi

echo "Finished training run $feats on" `date`

