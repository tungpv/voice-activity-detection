#!/usr/bin/perl -w

while (<>) {
  @A = split;
  if (@A > 1 and $A[-1] =~ m/\d+(\.\d+)?,\d+(\.\d+)?,/) {
    @B = split (/,/, $A[-1]);
    if (@B == 3) {
      $B[2] =~ s/[0-9]//g;
      $A[-1] = 1 + length $B[2];
    }
  }
  print join(" ", @A)."\n";
}
