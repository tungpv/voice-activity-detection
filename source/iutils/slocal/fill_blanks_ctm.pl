#!/usr/bin/perl

# koried, 3/8/2013

# Fill the blanks in a CTM file;  the reference stm file might have files which
# we did not decode (properly) and thus don't show any output.

binmode(STDIN, ":encode(utf8)");
binmode(STDOUT, ":encode(utf8)");

if (@ARGV < 1) {
  print STDERR "usage: $0 stm [ctm] > blanked_ctm\n";
  exit 1;
}

@ref = ();
$lref = "";
open (FI, "<${ARGV[0]}") or die "no such file ${ARGV[0]}\n";
while(<FI>) {
  next if m/^;;/;
  @A = split /\s+/;
  next if @A < 5;
  if ($A[0] ne $lref) {
    push @ref, $A[0];
    $lref = $A[0];
  }
}
close (FI);

shift @ARGV;

$l = "";
$c = 0;
while (<>) {
  @A = split /\s+/;

  if ($A[0] ne $ref[0]) {
    if ($c > 0) {
      $c = 0;
      shift @ref;
    }

    if ($c == 0) {
      while ($A[0] ne $ref[0] and @ref ne 0) {
        printf "%s 1 0.01 0.10 @\n", shift @ref;
      }
    }
  }

  $c += 1;
  printf "%s\n", join(" ", @A);
}

if ($c > 0) {
  shift @ref;
}

# output trailing missing segments
while (@ref > 0) {
  printf "%s 1 0.01 0.10 @\n", shift @ref;
}

