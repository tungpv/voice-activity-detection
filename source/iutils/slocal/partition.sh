#!/bin/bash

function error_exit { echo ${1:-"unknown error"};  exit 1; }

. path.sh

if [ $# -lt 1 ]; then
  echo "Usage: $0 config clone-set1 [clone-set2 ...]"
  exit 1
fi

config=$1
shift

[ -f "$config" ] || error_exit "No such file $config";
. $config



# verify the master partitions and create them if necessary
if [ -d $part_master/data/train ]; then
for i in $part_train; do # this will be something like "train1 train2 train3"
  part_name=part_$i
  part_num=part_${i}_num

  if [ ! -d $part_master/data/train_${!part_name} ]; then
    echo "generating $part_master/data/train_${!part_name} (${!part_num} utt)"
    if [ "$i" == "train1" ]; then
      # train1 is the initial set of phonetically based short utterances
      slocal/make_initial_set.pl data/local/dict/lexicon.txt ${!part_num} $part_master/data/train \
        > data/local/train_${!part_name}.list 2> data/local/train_${!part_name}.stats
      utils/reduce_data_dir.sh $part_master/data/train data/local/train_${!part_name}.list $part_master/data/train_${!part_name}

    elif [ "$i" == "train2" ]; then
      # train2 is a larger number of shortest utterances, hopefully somewhat phonetically representative
      utils/subset_data_dir.sh --shortest $part_master/data/train ${!part_num} $part_master/data/train_${!part_name}

    elif [ "$i" == "train3" ]; then
      # train3 is typically about 50% of the training data
      utils/subset_data_dir.sh --first $part_master/data/train ${!part_num} $part_master/data/train_${!part_name}

    else
      echo "Unknown partition type $i"
      exit 1
    fi

    # make a turn list for this partition
    awk '{print $1}' $part_master/data/train_${!part_name}/utt2spk > $part_master/data/train_${!part_name}/list
  fi
done

else
  echo "WARNING no partition master $part_master/train"
fi


# dev partitions
#if [ -d $part_master/data/dev ]; then
if [ 1 -le 0 ]; then
for i in $part_dev; do
  part_name=part_$i
  part_list=part_${i}_list

  if [ ! -d $part_master/data/dev_${!part_name} ]; then
    echo "generating $part_master/data/dev_${!part_name}"
    
    # lists may have .sph trailing
    sed -e 's:.sph$::' ${!part_list} > $$.list
    utils/reduce_data_dir_by_reclist.sh $part_master/data/dev $$.list $part_master/data/dev_${!part_name}
    rm $$.list
    
    awk '{print $1}' $part_master/data/dev_${!part_name}/utt2spk > $part_master/data/dev_${!part_name}/list
  fi
done
else
  echo "WARNING no partition master $part_master/dev"
fi

# duplicate the training partitions for the given ets
for i in $@; do
  echo "$i"

  if [ -d $part_master/data/train ]; then
  for p in $part_train; do
    part_name=part_$p
    echo "  $i/data/train_${!part_name}"
    utils/reduce_data_dir.sh $i/data/train $part_master/data/train_${!part_name}/list $i/data/train_${!part_name}
  done
  fi

#  if [ -d $part_master/data/dev ]; then
  if [ 1 -le 0 ]; then
  for p in $part_dev; do
    part_name=part_$p
    echo "  $i/data/dev_${!part_name}"
    utils/reduce_data_dir.sh $i/data/dev $part_master/data/dev_${!part_name}/list $i/data/dev_${!part_name}
  done
  fi
done


