#!/usr/bin/perl -W

# koried, 2/15/2013

# Convert a HTK segments file with lines
#   BABEL_BP_101_spk_date_recid_inLine_timestamp_timestamp=/path/to/file[start,end]
# to a kaldi segments file with lines
#   BABEL_BP_101_spk_date_recid_inLine_timestamp_timestamp BABEL_BP_101_spk_date_recid_inLine start end

while (<>) {
  chomp;
  m:(BABEL_[A-Z]+_[0-9]{3}_[0-9]+_[0-9]+_[0-9]+_[a-zA-Z]+_[0-9]+_[0-9]+)=[0-9a-zA-Z\+_\-/\.]+\[([0-9]+),([0-9]+)\]:;
  ($turn, $t1, $t2) = ($1, $2, $3);
  $file = $turn;
  $file =~ s/_[0-9]+_[0-9]+$//;

  # shift last two digits
  $t1 =~ s/([0-9]{2})$/.$1/;

  $t2 += 2; # frames are inclusive, so adjust by frame length (25ms)
  $t2 =~ s/([0-9]{2})$/.$1/; $t2 .= 5;

  print "$turn $file $t1 $t2\n";
}

