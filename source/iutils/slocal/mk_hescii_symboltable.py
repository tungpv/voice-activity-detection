#!/usr/bin/env python
#
# Quick and dirty script to downcase and encode a symbol table with hescii.

import sys
import hescii

for line in sys.stdin:
    if line.startswith('!') or line.startswith('#') or line.startswith('[') or line.startswith('<'):
      print line,
    else:
      sym = line.split()
      sym[0] = '-'.join([hescii.dumps(x) for x in sym[0].split('-')])

      print ' '.join(sym)
