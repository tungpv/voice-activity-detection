#!/usr/bin/perl

(@ARGV > 0) or die "Usage:  $0 utt2spk depthfile\n";

$u2sf = shift @ARGV;
if (defined $u2sf) {
  open(F, "<$u2sf") or die "Error opening utt2spk $u2sf\n";
  while(<F>) {
    @A = split (" ", $_);
    @A == 2 or die "bad line in utt2spk: $_\n";
    $utt2spk{$A[0]} = $A[1];

    $logp{$A[1]} = 0;
    $n{$A[1]} = 0;
    $spk{$A[1]} = 1;
  }
}
$nn = 0;
$lp = 0;
while (<>) {
  @A = split;
  $logp{$utt2spk{$A[0]}} += $A[1];
  $n{$utt2spk{$A[0]}} += 1;

  $nn += 1;
  $lp += $A[1];
}

$logpspk = 0;

for $s (sort keys %spk) {
  printf "%s %.4f\n", $s, $n{$s} > 0 ? ($logp{$s} / $n{$s}) : 0;
  $logpspk += $n{$s} > 0 ? ($logp{$s} / $n{$s}) : 0;
}

printf STDERR "overall avg lat-depth: %.4f\n", ($lp/$nn);
printf STDERR "spk-average lat-depth: %.4f\n", ($logpspk/(scalar keys %spk));
