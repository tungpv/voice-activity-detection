#!/bin/bash

num_train=20
train_cmd=slocal/slurm.pl
feats1=srs_mfccpp
feats2=srs_bn

# steps/tandem/train_lda_mllt.sh --cmd slocal/slurm.pl --extra-lda true --splice-opts '--left-context=3 --right-context=3' 3000 50000 srs_{mfccpp,bn}/data/train data/lang srs_bn/exp/{tri2_ali,tri3a_elda}

steps/tandem/align_fmllr.sh --nj $num_train --cmd "$train_cmd" \
  {$feats1,$feats2}/data/train data/lang $feats2/exp/tri3a_elda $feats2/exp/tri3a_elda_ali

steps/tandem/train_sat.sh --cmd "$train_cmd" \
  5000 80000 {$feats1,$feats2}/data/train data/lang $feats2/exp/tri3a_elda_ali $feats2/exp/tri4a_elda

