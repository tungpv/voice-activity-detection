#!/usr/bin/perl -w

if(@ARGV != 2) {
  print STDERR "Usage: $0 map lexicon > output\n";
  exit 1;
}

($map, $file) = @ARGV;
open(M, "<$map") or die "Error opening map file $map: $!";

while (<M>) {
  @A = split(" ", $_);
  @A >= 1 || die "apply_map.pl: empty line.";
  $i = shift @A;
  $o = join(" ", @A);
  $map{$i} = $o;
}

open(F, "<$file") or die "Error opening lexicon";

binmode(F, ":encoding(utf8)");
binmode(STDOUT, ":encoding(utf8)");

while(<F>) {
  @A = split(" ", $_);
  for ($x = 1; $x < @A; $x++) {
    $a = $A[$x];
    if (defined $map{$a}) { 
      $A[$x] = $map{$a};
    }
  }
  print join(" ", @A) . "\n";
}
