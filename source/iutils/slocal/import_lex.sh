#!/bin/bash

# koried, 3/28/2013

# import a lexicon file as in

. path.sh

if [ $# != 3 ]; then
	echo "Usage: $0 config lexicon data/local/dict"
	exit 1;
fi

conf=$1
lexicon=$2
dir=$3

. $conf

mkdir -p $dir
cp $TEMPLATES/{optional_silence,silence_phones}.txt $dir/

# make lowercase lexicon, if required
if [ "$dict_lc" == "true" ]; then
  LC_ALL=C slocal/make_lc.pl -f 1 $lexicon | sort -u > $dir/lexicon2.txt
else
  cp $lexicon $dir/lexicon2.txt
fi

# Add noises to the lexicon
cat $TEMPLATES/silwords.asc $dir/lexicon2.txt > $dir/lexicon.txt

# make phone table;  do that on un-augmented lexicon (no silence, no nonspeech phones)
awk '{ for (i=2; i <= NF; i++) print $i }' $dir/lexicon2.txt | sort -u > $dir/nonsilence_phones.txt
