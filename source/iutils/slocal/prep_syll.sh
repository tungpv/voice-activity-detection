#!/bin/bash

conf=$1
lm_syll_order=$2

slocal/wali_to_syll_text.sh --cmd slocal/slurm.pl data/local/lang/lexicon.txt data/local/syll_dict/w2c_lex.txt \
  data/lang srs_bn/data/train srs_bn/exp/tri4a_ali srs_bn/exp/tri4a_ali_text || exit 1

slocal/train_lm.sh $conf $lm_syll_order data/local/syll_dict/lexicon.txt \
  srs_bn/exp/tri4a_ali_text/text data/local/syll.arpa.gz || exit 1

slocal/prepare_lang_test.sh data/syll_lang data/local/syll.arpa.gz data/syll_lang_test || exit 1

