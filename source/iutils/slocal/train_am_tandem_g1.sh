#!/bin/bash

# koried, 2/13/2012

# Tandem acoustic model training template;  uses settings from config file.
# Models will be stored in features2/ directory (assuming that feature1 is
# the "constant" for all tandem systems

if [ $# -lt 3 ]; then
  echo "Usage: $0 [ options ] config features1 features2"
  exit 1;
fi

. cmd.sh

# config;  change any of the following variables via command line (e.g. --feat-dim 45)
stage=1
num_train=20

# what do you want to train
do_sgmm=true
do_mmi=false
do_sgmm_mmi=false

. utils/parse_options.sh

. $1

feats1=$2
feats2=$3

echo `date` Starting training sequence

# don't forget to call with --feat-dim!
if [ $stage -le 1 ]; then
  steps/tandem/train_mono.sh --nj $num_train --cmd "$train_cmd" \
    {$feats1,$feats2}/data/train_$part_train1 data/lang_g1 $feats2/exp_g1/mono0a || exit 1;
fi

# step up to 20k shortest;  train deltas
if [ $stage -le 2 ]; then
  echo `date` Entering stage 2
  steps/tandem/align_si.sh --nj $num_train --cmd "$train_cmd" \
    {$feats1,$feats2}/data/train_$part_train2 data/lang_g1 $feats2/exp_g1/mono0a $feats2/exp_g1/mono0a_ali || exit 1;
  steps/tandem/train_deltas.sh --cmd "$train_cmd" \
    $tp_s2_ns $tp_s2_nd {$feats1,$feats2}/data/train_$part_train2 data/lang_g1 $feats2/exp_g1/mono0a_ali $feats2/exp_g1/tri1 || exit 1;
fi

# step up to 37k (about 50% of training data)
if [ $stage -le 3 ]; then
  echo `date` Entering stage 3
  steps/tandem/align_si.sh --nj $num_train --cmd "$train_cmd" \
    {$feats1,$feats2}/data/train_$part_train3 data/lang_g1 $feats2/exp_g1/tri1 $feats2/exp_g1/tri1_ali || exit 1 ;
  steps/tandem/train_deltas.sh --cmd "$train_cmd" \
    $tp_s3_ns $tp_s3_nd {$feats1,$feats2}/data/train_$part_train3 data/lang_g1 $feats2/exp_g1/tri1_ali $feats2/exp_g1/tri2 || exit 1;
fi

# step up to all data;  train LDA+MLLT system
if [ $stage -le 4 ]; then
  echo `date` Entering stage 4
  steps/tandem/align_si.sh --nj $num_train --cmd "$train_cmd" \
    {$feats1,$feats2}/data/train data/lang_g1 $feats2/exp_g1/tri2 $feats2/exp_g1/tri2_ali || exit 1;
  steps/tandem/train_lda_mllt.sh --cmd "$train_cmd" \
    --splice-opts "$tp_so" \
    $tp_s4_ns $tp_s4_nd {$feats1,$feats2}/data/train data/lang_g1 $feats2/exp_g1/tri2_ali $feats2/exp_g1/tri3a || exit 1;
fi

# re-align all the data, increase states+gaussians
if [ $stage -le 5 ]; then
  echo `date` Entering stage 5
  steps/tandem/align_fmllr.sh --nj $num_train --cmd "$train_cmd" \
    {$feats1,$feats2}/data/train data/lang_g1 $feats2/exp_g1/tri3a $feats2/exp_g1/tri3a_ali || exit 1;
  steps/tandem/train_sat.sh --cmd "$train_cmd" \
    $tp_s5_ns $tp_s5_nd {$feats1,$feats2}/data/train data/lang_g1 $feats2/exp_g1/tri3a_ali $feats2/exp_g1/tri4a || exit 1;
fi


# some more sophisticated models based on tri4a
if [ $stage -le 6 ]; then
  echo `date` Entering stage 6

  # align data using tri4a
  if [ ! -d $feats2/exp_g1/tri4a_ali ]; then
    echo `date` Aligning training data
    steps/tandem/align_fmllr.sh --nj $num_train --cmd "$train_cmd" \
      {$feats1,$feats2}/data/train data/lang_g1 $feats2/exp_g1/tri4a $feats2/exp_g1/tri4a_ali || exit 1;
  fi

  # sgmm model
  if $do_sgmm; then
    if [ ! -f $feats2/exp_g1/ubm5a/final.ubm ]; then
      echo `date` Training UBM
      steps/tandem/train_ubm.sh --cmd "$train_cmd" $tp_s6_ubm {$feats1,$feats2}/data/train data/lang_g1 \
        $feats2/exp_g1/tri4a_ali $feats2/exp_g1/ubm5a || exit 1;
    fi
    echo `date` Training SGMM system
    steps/tandem/train_sgmm2.sh --cmd "$train_cmd" \
      $tp_s6_ns $tp_s6_nd {$feats1,$feats2}/data/train data/lang_g1 $feats2/exp_g1/tri4a_ali \
      $feats2/exp_g1/ubm5a/final.ubm $feats2/exp_g1/sgmm5a || exit 1;
  fi

  # MMI trainig
  if $do_mmi; then
    echo `date` Making dense lattices
    steps/tandem/make_denlats.sh --nj $num_train --cmd "$decode_cmd" --config conf/decode.config \
      --transform-dir $feats2/exp_g1/tri4a_ali \
      {$feats1,$feats2}/data/train data/lang_g1 $feats2/exp_g1/tri4a $feats2/exp_g1/tri4a_denlats || exit 1;

    echo `date` Starting MMI training
    steps/tandem/train_mmi.sh --cmd "$decode_cmd" --boost 0.1 \
      --transform-dir $feats2/exp_g1/tri4a_ali \
      {$feats1,$feats2}/data/train data/lang_g1 $feats2/exp_g1/tri4a_{ali,denlats} $feats2/exp_g1/tri5a_mmi_b0.1 || exit 1;
  fi

  # SGMM/MMI training
  if $do_sgmm_mmi; then
    echo `date` Aligning training data with SGMM
    steps/tandem/align_sgmm2.sh --nj $num_train --cmd "$train_cmd" --use-graphs true --use-gselect true \
      --transform-dir $feats2/exp_g1/tri4a_ali \
      {$feats1,$feats2}/data/train data/lang_g1 $feats2/exp_g1/sgmm5a $feats2/exp_g1/sgmm5a_ali || exit 1;

    # Took the beam down to 8 to get acceptable decoding speed.
    echo `date` Making dense lattices
    steps/tandem/make_denlats_sgmm2.sh --nj $num_train --beam 8.0 --lattice-beam 6 --cmd "$decode_cmd" \
      --transform-dir $feats2/exp_g1/tri4a_ali \
      {$feats1,$feats2}/data/train data/lang_g1 $feats2/exp_g1/sgmm5a_{ali,denlats} || exit 1;

    echo `date` Starting SGMM MMI training
    steps/tandem/train_mmi_sgmm2.sh --cmd "$decode_cmd" --boost 0.1 \
      --transform-dir $feats2/exp_g1/tri4a_ali \
      {$feats1,$feats2}/data/train data/lang_g1 $feats2/exp_g1/sgmm5a_{ali,denlats,mmi_b0.1} || exit 1;
  fi
fi

echo "Finished training run $feats on" `date`

