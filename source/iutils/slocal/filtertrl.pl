#!/usr/bin/perl -W

# koried, 10/24/2012

# Filter a transcript file based on some hand-crafted rules and
# the lexicon (place UNK-word if token not present in lexicon).

use utf8;
use Unicode::Normalize;

# set unicode/utf8
binmode(STDIN, ":encoding(utf8)");
binmode(STDOUT, ":encoding(utf8)");
binmode(STDERR, ":encoding(utf8)");

$alladverse_list_file = '';
$badturns_list_file = '';
$noisy_list_file = '';
$overlap_list_file = '';
$spkchange_list_file = '';
$empty_list_file = '';

while (@ARGV gt 0 and $ARGV[0] =~ m/^--/) {
	$param = shift @ARGV;

	if ($param eq "--write-adverse-turns") {
		$alladverse_list_file = shift @ARGV;
	} elsif ($param eq "--write-bad-turns") {
		$badturns_list_file = shift @ARGV;
	} elsif ($param eq "--write-noisy-turns") {
		$noisy_list_file = shift @ARGV;
	} elsif ($param eq "--write-overlap-turns") {
		$overlap_list_file = shift @ARGV;
	} elsif ($param eq "--write-spkchange-turns") {
		$spkchange_list_file = shift @ARGV;
  } elsif ($param eq "--write-empty-turns") {
    $empty_list_file = shift @ARGV;
	} else {
		die "unknown parameter $param\n";
	}
}

(@ARGV eq 1) or die "
Filter a transcript file based on a lexicon and some hand-crafted rules.

usage: $0 lexicon < text-in > text-out

  --write-adverse file
    Write all adverse turns to file;  this is the same as writing out the individual
	turn files using the options below followed by a `cat list* | uniq`.

  --write-bad-turns file
    Write out a list of bad turns, i.e., turns with only noise, etc.

  --write-noisy-turns file
    Write out a list of noisy turns;  useful to exclude them from later training

  --write-overlap-turns file
    Write out a list of turns that contain the overlap tag

  --write-spkchange-turns file
    Write out a list of turns where the speaker changes;  useful to exclude them
	from SAT
";

($flexicon) = @ARGV;

# read in lexicon for OOV detection
%lex = ();
open (FI, "<:encoding(UTF-8)", $flexicon) or die "Could not read lexicon $flexicon\n";
while (<FI>) {
	chomp;
	($w, @p) = split /\s+/;
	$lex{$w} = join(' ', @p);
}
close (FI);

# make sure our new words are in the lexicon
for $w ( "<unk>", "[NOISE]", "[BREATH]", "[COUGH]", "[LAUGH]", "[MUSIC]" ) {
	$lex{$w} = 1;
}

# ignore ( = remove) the followig tags
@ignore = ( "<sta>", "<no-speech>", "<lipsmack>", "<click>", "<male-to-female>", "<female-to-male>" );

# map the following tags to <unk>-word
@tounk = ( "<overlap>", "<prompt>", "\\(\\(\\)\\)", "<foreign>", "~" );

# keep some turn-lists
@tl_bad = ();
@tl_noisy = ();
@tl_overlap = ();
@tl_spkchange = ();
@tl_empty = ();

# read in transcription, validate each word
while (<STDIN>) {
	($turn, @trl) = split /\s+/;

	$trlflat = join(' ', @trl);	
	
	# un-comment if you plan on using the later OOV warning
	# $trlcopy = $trlflat;

	# detect some junk;  we'll see if a turn is "bad" later on
	push (@tl_noisy, $turn) if ($trlflat =~ m/<sta>/);
	push (@tl_overlap, $turn) if ($trlflat =~ m/<overlap>/);
	push (@tl_spkchange, $turn) if ($trlflat =~ m/<male-to-female>/ or $trlflat =~ m/<female-to-male>/);

	# strip and replace some junk
	for $rem (@ignore) { $trlflat =~ s/$rem//g; }
 	for $unk (@tounk) { $trlflat =~ s/$unk/<unk>/g; }

	# now some specific replaces
	$trlflat =~ s/\<int\>/[NOISE]/g;		# "regular" noise
	$trlflat =~ s/\<breath\>/[BREATH]/g;	# speaker noises
	$trlflat =~ s/\<cough\>/[COUGH]/g;
	$trlflat =~ s/\<laugh\>/[LAUGH]/g;
	$trlflat =~ s/\<dtmf\>/[MUSIC]/g;		# musical noise, e.g. ring or dtmf
	$trlflat =~ s/\<ring\>/[MUSIC]/g;
	
	# we are ok with truncations as long as they appear in the lexicon;  if not, put unk
	$trlflat =~ s/\*//g;
	$trlflat =~ s/-//g;

	# trim; seems to cause problems if not done
	$trlflat =~ s/^\s+//g;
	$trlflat =~ s/\s+$//g;
	$trlflat =~ s/\s+/ /g;

	# get the individual words, perform OOV check, count garbage words
	@trl = split(/ /, $trlflat);

  push (@tl_empty, $turn) if (scalar @trl == 0);

	$numjunk = 0;
	for $i ( 0 .. $#trl ) {
		if (not defined $lex{$trl[$i]}) {
			# we don't need this warning-- it basically tells us that in most of the cases, 
			# the fragments could not be resolved to an actual word
			# print STDERR "$turn\n$trlcopy\n$trlflat\n  OOV $i '".$trl[$i]."'\n";
			$trl[$i] = "<unk>";
		}
	
		$numjunk += 1 if ($trl[$i] eq "<unk>" or $trl[$i] =~ m/[A-Z+]/);
	}

	push (@tl_bad, $turn) if ($numjunk eq @trl);

	print "$turn ".join(' ', @trl)."\n";
}

sub arraytofile {
	($file, @list) = @_;

	open (FO, ">$file") or die "Could not write to $file\n";
	for $l (@list) {
		print FO "$l\n";
	}
	close (FO);
}

arraytofile($badturns_list_file, @tl_bad) if (length $badturns_list_file gt 0);
arraytofile($noisy_list_file, @tl_noisy) if (length $noisy_list_file gt 0);
arraytofile($overlap_list_file, @tl_overlap) if (length $noisy_list_file gt 0);
arraytofile($spkchange_list_file, @tl_spkchange) if (length $spkchange_list_file gt 0);
arraytofile($empty_list_file, @tl_empty) if (length $empty_list_file gt 0);

if (length $alladverse_list_file gt 0) {
	%aturns = ();
	for $t (@tl_bad, @tl_noisy, @tl_overlap, @tl_spkchange, @tl_empty) {
		$aturns{$t} = 1;
	}
	arraytofile($alladverse_list_file, sort keys %aturns);
}

