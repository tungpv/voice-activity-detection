#!/usr/bin/perl -w

if (@ARGV != 4) {
  print STDERR "Make size-balanced batches\n";
  print STDERR "$0 utt2spk utt-lengths-file nbatches list-base-out\n";
  exit 1;
}

($utt2spk_file, $uttlength_file, $nbatches, $outbase) = @ARGV;

# read in utt2spk
open (U, "<$utt2spk_file") or die "Failed to open utt2spk file $utt2spk_file";
while(<U>) {
  @A = split;
  @A == 2 or die "Bad line $_ in utt2spk file $utt2spk_file";
  ($u, $s) = @A;
  $utt2spk{$u} = $s;
}
close (U);

(scalar keys %utt2spk > $nbatches) or die "Unable to make good batches, nbatches <= #spk\n";

# read utt lengths
open (F, "<$uttlength_file") or die "Failed to open utt-lengths-file $uttlength_file\n";
while (<F>) {
  @A = split;
  @A == 2 or die "Bad line $_ in utt-lengths-file\n";
  ($u, $l) = @A;
  (defined $utt2spk{$u}) or die "Missing utt2spk entry for $u\n";
  $spk{$utt2spk{$u}}  = 0 unless defined $spk{$utt2spk{$u}};
  $spk{$utt2spk{$u}} += $l;
}
close (F);

# init batches
@batches = ();
for ($i = 0; $i < $nbatches; $i++) {
  push @batches, ();
}

# distribute round-robin on batches
$i = 0;
for $s (sort { $spk{$a} <=> $spk{$b} } keys %spk) {
  # printf "%d: %s\n", $i, $s;
  push @{$batches[$i]}, $s;
  $i = ($i + 1) % $nbatches;
}

for ($i = 0; $i < $nbatches; $i++) {
  printf "batch %d: %d spk\n", $i, scalar @{$batches[$i]};
  open (F, ">$outbase$i");
  for $s (@{$batches[$i]}) {
    print F "$s\n";
  }
  close F;
}

