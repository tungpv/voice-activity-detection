#!/bin/bash

sil_prob=0.5

if [ $# -ne 2 ]; then
  echo "Usage:  $0 lexicon langdir"
  exit 1;
fi

lexicon=$1
dir=$2

if [ -f $dir/L_align.fst ]; then
  echo "$dir/L_align.fst already exists;  delete it first."
  exit 1;
fi

for f in $dir/{words,phones}.txt $dir/phones/optional_silence.txt; do
  if [ ! -f $f ]; then echo "No such file $f"; exit 1; fi
done

silphone=`cat $dir/phones/optional_silence.txt`

cat $lexicon | \
  awk '{printf("%s #1 ", $1); for (n=2; n <= NF; n++) { printf("%s ", $n); } print "#2"; }' | \
  utils/make_lexicon_fst.pl - $sil_prob $silphone | \
  fstcompile \
    --isymbols=$dir/phones.txt --osymbols=$dir/words.txt \
    --keep_isymbols=false --keep_osymbols=false | \
  fstarcsort --sort_type=olabel > $dir/L_align.fst || exit 1

