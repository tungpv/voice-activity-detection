#!/bin/bash

# koried, 3/28/2013

# Extract the training information from the corpus;  to be run from the 
# language root directory


function error_exit { echo ${1:-"unknown error"};  exit 1; }
  
. path.sh

if [ $# != 1 ]; then
	echo "Usage: $0 path/to/config"
	exit 1;
fi

config=$1

[ -f "$config" ] || error_exit "No such file $config"
. $config

# verify a couple of required settings
[ -d $BABEL_DIR ] || error_exit "No such directory $BABEL_DIR"
[ -f $srs_lexicon_compiled ] || error_exit "No such lexicon file: $srs_lexicon_compiled"

echo "Preparing partitions (transcriptions, etc.)"


# specify the partitions to import and if bad utterances should be pruned; for
# pruning, specify one of the following (list);  multiple selections: "bad noisy"
#   adverse    all of the below
#   bad        turns that have either no words or only junk words ([...])
#   noisy      turns that have the noisy indicator <sta>
#   overlap    turns annotated with overlap
#   spkchange  turns annotated with speaker change
#   empty      turns with no speech (<no-speech>) only
#
# optionally specify STM file

partitions=( \
  "train" "conversational/training" ""           "adverse" \
  "dev"   "conversational/dev"      "$BABEL_STM" "empty" \
)


num_partitions=$[${#partitions[@]} / 4 - 1]
for x in `seq 0 $num_partitions`; do
  pname=${partitions[$[$x * 4]]}
  pdir=${partitions[$[$x * 4 + 1]]}
  pstm=${partitions[$[$x * 4 + 2]]}
  pprune=${partitions[$[$x * 4 + 3]]}

  echo "Importing $pdir as $pname..."

  dir=data/local/$pname
  mkdir -p $dir

  # Prepare file list, generate segments and transcripts file
  /bin/ls $BABEL_DIR/$pdir/transcription | \
  grep -v '\~$' | \
  sort | \
  sed s:^:$BABEL_DIR/$pdir/transcription/: > $dir/flist

  # Prepare the transcription of the turns
  slocal/preptrl.pl $dir/flist $dir/segments-all $dir/text-all.raw

  # Make master transcription lower-case, if required
  if [ "$dit_lc" == "true" ]; then
    mv $dir/text-all.raw $dir/text-all.raw.case_sensitive
    slocal/make_lc.pl -f 2- $dir/text-all.raw.case_sensitive > $dir/text-all.raw
  fi

  # Filter the transcripts to a usable format;  collect info on bad turns.
  # Note that there is no special treatment for the syllable stuff, as we train
  # on the word constraints!

  slocal/filtertrl.pl \
    --write-adverse-turns $dir/tlist-adverse \
    --write-bad-turns $dir/tlist-bad \
    --write-noisy-turns $dir/tlist-noisy \
    --write-overlap-turns $dir/tlist-overlap \
    --write-spkchange-turns $dir/tlist-spkchange \
    --write-empty-turns $dir/tlist-empty \
    $srs_lexicon_compiled < $dir/text-all.raw > $dir/text-all

  # Generate speaker info:  utt2spk, spk2gender.map
  awk '{print $1}' $dir/text-all | \
    sed -e 's:\(BABEL_[A-Z]*_[0-9]*_[0-9]*_[0-9]*_[0-9]*_[a-zA-Z]*\)\(_[0-9]*_[0-9]*\):\1\2 \1:' > $dir/utt2spk-all

  utils/utt2spk_to_spk2utt.pl < $dir/utt2spk-all > $dir/spk2utt-all

  for spk in `awk '{print $1}' $dir/spk2utt-all`; do 
    echo -n "$spk "; 
    grep $spk $BABEL_DIR/conversational/reference_materials/demographics.tsv | \
      head -n1 | awk '{print tolower($8)}'; # make sure it's only one
  done > $dir/spk2gender.map

  # Generate reco2file_and_channel;  here we only have one channel per file 
  # (other than with switchboard, for example), thus this is straight forward
  # Mapping is reco-id base-file channel-no
  awk '{print $1" "$1" 1"}' $dir/spk2utt-all > $dir/reco2file_and_channel

  # make wav.scp file
  sph2pipe=$KALDI_ROOT/tools/sph2pipe_v2.5/sph2pipe
  awk -v cmd="$sph2pipe" -v dir="$BABEL_DIR/$pdir/audio" \
    '{printf "%s %s -p -f wav %s/%s.sph |\n", $1, cmd, dir, $1}' $dir/spk2utt-all > $dir/wav.scp

  # Prune utterances, if required
  if [ -z "$pprune" ]; then
    # no pruning, just copy turn lists
    for i in text segments utt2spk spk2utt; do
      cp $dir/${i}-all $dir/$i
    done
  else
    # do required pruning
    for i in $pprune; do cat $dir/tlist-$i; done | sort -u > $dir/tlist-ignore

    slocal/reducelist.pl $dir/tlist-ignore < $dir/text-all > $dir/text
    slocal/reducelist.pl $dir/tlist-ignore < $dir/segments-all > $dir/segments
    slocal/reducelist.pl $dir/tlist-ignore < $dir/utt2spk-all > $dir/utt2spk
    utils/utt2spk_to_spk2utt.pl < $dir/utt2spk > $dir/spk2utt
  fi

  # make actual data directory (these files will be copied by all features)
  mkdir -p data/$pname
  cp $dir/{text,segments,utt2spk,spk2utt,reco2file_and_channel,wav.scp} data/$pname/

  # copy STM file, if present
  [ -f "$pstm" ] && cp $pstm data/$pname/stm

done

# EOF
