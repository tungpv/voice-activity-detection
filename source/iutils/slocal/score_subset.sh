#!/bin/bash

# koried, 2/25/2013

# Score subsets of a stm/ctm pair (e.g. for llp list)

sclite=/u/drspeech/opt/kaldi/kaldi-trunk/tools/sctk-2.4.5/bin/sclite
do_char=false

[ -f ./path.sh ] && . ./path.sh
. parse_options.sh || exit 1;

if [ $# != 3 ]; then
  echo "Usage: $0 [options] stm ctm"
  echo "Options:"
  echo "  --do-char true|false  Do char-based scoring (e.g. cantonese)"
  echo "  --sclite /path/to/sclite"
  exit 1;
fi

list=$1
stm=$2
ctm=$3

utils/filter_scp.pl $list $stm > $$.stm
utils/filter_scp.pl $list $ctm > $$.ctm

if [ "$do_char" == "false" ]; then
  $sclite -f 0 -e utf-8 -r $$.stm stm -h $$.ctm ctm
else
  $sclite -f 0 -c NOASCII -e utf-8 -r $$.stm stm -h $$.ctm ctm
fi

rm $$.{stm,ctm}

