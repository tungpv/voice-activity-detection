#!/usr/bin/perl

use utf8;

binmode(STDIN, ":encoding(utf8)");
binmode(STDOUT, ":encoding(utf8)");
binmode(STDERR, ":encoding(utf8)");

if(@ARGV != 1) {
    die "Usage: filter_w2c.pl charlist < word-to-syll-dict > out ";
}


$idlist = shift @ARGV;
open(F, "<$idlist") || die "Could not open id-list file $idlist";
binmode(F, ":encoding(utf8)");
while(<F>) {
  @A = split;
  @A>=1 || die "Invalid id-list file line $_";
  $seen{$A[0]} = 1;
}

while (<>) {
  @A = split;
  @A > 0 || die "Invalid line $_";
  $ignore = 0;
  for ($i = 1; $i < @A; $i++) {
    unless (defined $seen{$A[$i]}) {
      print STDERR "Missing character ".$A[$i].";  ignoring word ".$A[0]."\n";
      $ignore = 1;
      last;
    }
  }
  if ($ignore == 0) {
    print join(" ", @A)."\n"; 
  }
}
