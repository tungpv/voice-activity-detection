#!/bin/bash

cmd=utils/run.pl
stage=1
#lang=data/lang_dialect_final
#alidir1=srs_bn_0413/exp_dialect_final/tri4a_ali

[ -f path.sh ] && . ./path.sh
. parse_options.sh || exit 1;

if [ $# != 4 ]; then
  echo "Usage:  $0 lang-dir data-dir ali-dir output-dir"
  exit 1;
fi

lang=$1
data=$2
alidir=$3
dir=$4

for f in $alidir/{num_jobs,final.mdl} $lang/phones/optional_silence.txt $lang/L_align.fst; do
  if [ ! -f $f ]; then echo "No such file $f"; exit 1; fi
done

nj=`cat $alidir/num_jobs`
silword=`cat $lang/phones/optional_silence.txt`
oov=`cat $lang/oov.int`

wbegin=`awk '/#1 / { print $2 }' $lang/phones.txt`
wend=`awk '/#2 / { print $2 }' $lang/phones.txt`

mkdir -p $dir/log

# --write-lengths for ali-ti-phones?

# make word prons out of forced alignments
if [ $stage -le 1 ]; then
  $cmd JOB=1:$nj $dir/log/ali-to-prons.JOB.log \
    ali-to-phones $alidir/final.mdl "ark:gunzip -c $alidir/ali.JOB.gz|" ark:- \| \
    phones-to-prons $lang/L_align.fst $wbegin $wend ark:- \
      "ark:utils/sym2int.pl --map-oov $oov -f 2- $lang/words.txt $data/split$nj/JOB/text |" "ark,t:|gzip -c > $dir/pron.JOB.int.gz" || exit 1;

  stage=$[$stage+1]
fi

# get phonetic strings with word boundaries (remove position markers, if present)
if [ $stage -le 2 ]; then
  $cmd JOB=1:$nj $dir/log/phn-wdbdr.JOB.log \
    gunzip -c $dir/pron.JOB.int.gz \| slocal/stripw.pl \| \
    slocal/int2sym_nsc.pl -f 2- $lang/phones.txt \| sed -e 's/_[BEIS] / /g' \| gzip -c ">" $dir/phn-wdbdry.JOB.gz || exit 1;
  
  gunzip -c $dir/phn-wdbdry.*.gz | sort -k1,1 | cut -d ' ' -f 2- > $dir/phn-wdbdry_nouttid.txt || exit 1;

  stage=$[$stage+1]
fi

# get global and speaker stats
if [ $stage -le 3 ]; then
  gunzip -c $dir/pron.*.int.gz | slocal/pronstat.pl $lang/{words,phones}.txt $data/utt2spk | \
    sort -k1,1 -k2,2 -k3nr,3 > $dir/pronstats.txt || exit 1;
fi


