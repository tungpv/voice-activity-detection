#!/bin/bash

# BABEL_BP_101 recipe
# koried, 11/18/2012

. cmd.sh

decode_set=data/dev
num_decode=20
exp_dir=exp
lang_dir=data/lang_test
config_file=conf/decode.config

. utils/parse_options.sh || exit 1;

if [ $# -lt 2 ]; then
  echo "usage: $0 features rec-dir"
  echo " e.g. $0 --decode-set data/dev1 --lang_dir data/syll_lang --exp_dir exp_llp --num-decode 10 mfcc tri4a"
  exit 1;
fi

feats=$1
rec_dir=$2

graph_dir=`basename $lang_dir`
data_dir=`basename $decode_set`
conf_name=`basename $config_file`

[ -f $feats/$exp_dir/$rec_dir/final.mdl ] || ( echo "No such file $feats/$exp_dir/$rec_dir/final.mdl";  exit 1; )

if [ ! -f $feats/$exp_dir/$rec_dir/graph_$graph_dir/HCLG.fst ]; then
  echo "Building decoding graph..."
  $decode_cmd /dev/stderr utils/mkgraph.sh $lang_dir $feats/$exp_dir/$rec_dir $feats/$exp_dir/$rec_dir/graph_$graph_dir || exit 1;
fi

steps/decode_fmllr.sh --nj $num_decode --cmd "$decode_cmd" --config $config_file \
  $feats/$exp_dir/$rec_dir/graph_$graph_dir $feats/$decode_set $feats/$exp_dir/$rec_dir/decode_${data_dir}_${graph_dir}_$conf_name

