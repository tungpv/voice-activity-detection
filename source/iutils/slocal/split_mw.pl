#!/usr/bin/perl -w

# koried, 4/22/2013

# Split a CTM file with multiwords (a__b[__d]) into the actual words;  the time
# will be allocated according to their characters, the postior, if present will
# be copied.

use utf8;

$delimiter = '__';

while (@ARGV > 0) {
  if ($ARGV[0] eq "-d" or $ARGV[0] eq "--delimiter") {
    shift @ARGV;
    $delimiter = shift @ARGV;
  } elsif ($ARGV[0] eq "-h" or $ARGV[0] eq "--help") {
    print STDERR "usage: $0 [-d delimiter] < in.ctm > out.ctm\n";
    exit 1;
  } else {
    print STDERR "ignoring nknown argument ${ARGV[0]}\n";
  }
}

binmode(STDIN, ":encoding(utf8)");
binmode(STDOUT, ":encoding(utf8)");

while (<STDIN>) {
  @A = split /\s+/;
  if ($A[4] =~ m/$delimiter/) {
    # split multiword
    @W = split ("$delimiter", $A[4]);

    # compute time share of each word
    $nchar = 0;
    for $w (@W) {
      $nchar += length $w;
    }

    # print new CTM lines
    $begin = $A[2];
    $duration = $A[3];

    for $w (@W) {
      $dur = $duration * (length $w) / $nchar;
      printf STDOUT "%s %d %.2f %.2f %s", $A[0], $A[1], $begin, $dur, $w; 
      if (@A == 5) {
        printf STDOUT "\n";
      } else {
        printf STDOUT "%s\n", $A[5];
      }
      $begin += $dur;
    }
  } else {
    print join(" ", @A)."\n";
  }
}

