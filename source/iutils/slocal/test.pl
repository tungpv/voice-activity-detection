#!/usr/bin/perl -w

binmode(STDIN, ":encoding(utf8)");
binmode(STDOUT, ":encoding(utf8)");

@prev = ('', 0, 0);
@prevp = ();

while (<>) {
  ($w, $n, @pron) = split;

  if ($prev[1] == 0 or $w ne $prev[0]) {
    #printf "%s %d %s ;\n", $prev[0], $prev[1], join(" ; ", @prevp) unless $prev[1] eq 0;
    unless ($prev[1] eq 0) {
      printf "%6s %6d %6d", $prev[0], $prev[1], $prev[2];
      for $v (@prevp) { printf " %.2f", ($v / $prev[2]); }
      print "\n";
    }

    @prev = ($w, 1, $n);
#    @prevp = (join(" ", $n, @pron));
    @prevp = ( $n );
    next;
  } else {
    $prev[1] += 1;
    $prev[2] += $n;
#    push @prevp, join(" ", $n, @pron);
    push @prevp, $n;
  }
}

#printf "%s %d %s ;\n", $prev[0], $prev[1], join(" ; ", @prevp);
printf "%6s %6d %6d", $prev[0], $prev[1], $prev[2];
for $v (@prevp) { printf " %.2f", ($v / $prev[2]); }
print "\n";
