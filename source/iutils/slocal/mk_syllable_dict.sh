#!/bin/bash

# koried, 2/5/2013

# Make a syllable based lexicons for the given babel lexicon

has_romanization=false

. utils/parse_options.sh

if [ $# != 3 ]; then
  echo "Usage: $0 babel-lex syl-lex-out word-to-syl-out";
  exit 1;
fi

babel=$1
syllable_lexicon=$2
w2syll_lexicon=$3
offset=2

if [ "$has_romanization" == "true" ]; then
  offset=3
fi

# make syllable lexicon
cut -f ${offset}- $babel | \
  sed -e 's/#/./g' -e 's/% //g' -e 's/" //g' -e 's/ \. /\n/g' -e 's/\t/\n/g' | sort -u | \
  awk '{a=$0; gsub(" ", "_"); print $1"\t"a;}' > $syllable_lexicon

# make syllabified lexicon ( word -> syllables )
cat $babel | \
  sed -e 's/" //g' -e 's/% //g' -e "s/#/./g" -e 's/ /_/g' -e 's/_\._/ /g' | \
  awk -F'\t' -v offset=$offset '{for (i=offset; i <= NF; ++i) { print $1"\t"$i }}' > $w2syll_lexicon

