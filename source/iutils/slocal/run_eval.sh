#!/bin/bash

# BABEL_BP_101 recipe
# koried, 11/18/2012

. cmd.sh

data=data/eval
num_decode=20
exp_dir=exp
lang_dir=data/lang_test
config_file1=conf/decode.config.tri4a
config_file2=conf/decode.config.sgmm5a

. utils/parse_options.sh || exit 1;

if [ $# -lt 2 ]; then
  echo "usage: $0 features1 features2"
  echo " e.g. $0 --data data/dev1 --lang_dir data/syll_lang --exp_dir exp_llp --num-decode 10 mfcc bn"
  exit 1;
fi

feats1=$1
feats2=$2

graph_dir=`basename $lang_dir`
data_dir=`basename $data`
conf_name1=`basename $config_file1`
conf_name2=`basename $config_file2`

rec_dir1=tri4a
rec_dir2=sgmm5a

decode_dir1=$feats2/$exp_dir/$rec_dir1/decode_${data_dir}_${graph_dir}_$conf_name1
decode_dir2=$feats2/$exp_dir/$rec_dir2/decode_${data_dir}_${graph_dir}_$conf_name2

# verify models, make graphs
[ -f $feats2/$exp_dir/$rec_dir1/final.mdl ] || ( echo "No such file $feats2/$exp_dir/$rec_dir1/final.mdl";  exit 1; )

if [ ! -f $feats2/$exp_dir/$rec_dir1/graph_$graph_dir/HCLG.fst ]; then
  echo "Building decoding graph $rec_dir1"
  $decode_cmd /dev/stderr utils/mkgraph.sh $lang_dir $feats2/$exp_dir/$rec_dir1 $feats2/$exp_dir/$rec_dir1/graph_$graph_dir || exit 1;
fi

[ -f $feats2/$exp_dir/$rec_dir2/final.mdl ] || ( echo "No such file $feats2/$exp_dir/$rec_dir2/final.mdl";  exit 1; )

if [ ! -f $feats2/$exp_dir/$rec_dir2/graph_$graph_dir/HCLG.fst ]; then
  echo "Building decoding graph..."
  $decode_cmd /dev/stderr utils/mkgraph.sh $lang_dir $feats2/$exp_dir/$rec_dir2 $feats2/$exp_dir/$rec_dir2/graph_$graph_dir || exit 1;
fi


# decode first pass
echo `date` Starting decoding $rec_dir1

steps/tandem/decode_fmllr.sh --nj $num_decode --cmd "$decode_cmd" --config $config_file1 \
  $feats2/$exp_dir/$rec_dir1/graph_$graph_dir {$feats1,$feats2}/$data $decode_dir1

echo `date` Finished decoding $rec_dir1

# decode second pass
echo `date` Starting decoding $rec_dir2

steps/tandem/decode_sgmm2.sh --nj $num_decode --cmd "$decode_cmd" --config $config_file2 --transform-dir $decode_dir1 \
  $feats2/$exp_dir/$rec_dir2/graph_$graph_dir {$feats1,$feats2}/$data $decode_dir2

echo `date` Finished decoding $rec_dir2

# generate SLF lattices for second pass
echo `date` Generating SLF lattices
steps/tandem/mk_aslf_sgmm2.sh --nj $num_decode --transform-dir $decode_dir1 --cmd "$decode_cmd" $feats2/$exp_dir/$rec_dir2/graph_$graph_dir {$feats1,$feats2}/$data $decode_dir2 $decode_dir2/slf

echo `date` Done.

