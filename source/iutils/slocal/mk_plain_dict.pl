#!/usr/bin/perl -W

# koried, 10/24/2012

# Generate a phone/tone set and a lexicon based on the BABEL master lexicon.
# Note that this script will generate a "full" phone set, i.e., for each tonal
# and syllabic symbol, all the tones will be generated regardless of their
# appearence in the lexicon-- the phone set is cast into the decision tree and
# can't be changed later on.

use utf8;
use Unicode::Normalize;

# set unicode/utf8
binmode(STDIN, ":encoding(utf8)");
binmode(STDOUT, ":encoding(utf8)");
binmode(STDERR, ":encoding(utf8)");

$phone_map_file = '';
$has_romanization = 'false';

while (@ARGV gt 0 and $ARGV[0] =~ m/^--/) {
	$param = shift @ARGV;

	if ($param eq "--map-phones") {
		$phone_map_file = shift @ARGV;
  } elsif ($param eq "--has-romanization") {
    $has_romanization = shift @ARGV;
  } else {
		die "unknown parameter $param\n";
	}
}

%phone_map = ();
if ($phone_map_file ne '') {
	open (FI, "<$phone_map_file");
	while (<FI>) {
		chomp;
		my @l = split /\s+/;
		for $i (1 .. $#l) {
			$phone_map{$l[$i]} = $l[0];
		}
	}
	close (FI);
}

if (@ARGV ne 4) {
	print STDERR "Extract the phone set and dictionary from the BABEL-style master-lexicon
and generate the extra_questions.txt for the later tree building.  This is the non-tonal 
version.
usage: $0 babel-lexicon phones-out lexicon-out questions-out
options:
  --map-phones (mapfile)
    A set of phones to the same identifier
";
	exit 1;
}

# empiric list of tonal symbols, will be used to generate phoneset
%symbols = ();

($flexin, $fphonesout, $flexout, $fqstout) = @ARGV;

# read in lexicon
open (FI, "<:encoding(UTF-8)", $flexin) or die "Could not read lexicon file $flexin\n";
open (FO, ">:encoding(UTF-8)", $flexout) or die "Could not write lexicon file $flexout\n";
while (<FI>) {
	chomp;

	# line format is word\t[romanization\t]pron1[\tpron2 ...]
	@ln = split /\t/;

	$w = shift @ln;

	#print STDERR "$w\n";

  if ($has_romanization eq 'true') {
    shift @ln;
  }

	for $pron (@ln) {
		# print out word (alt-prons just give an extra line)
		#print FO "$w ";

    # clean up:  remove stress markers ("%) and syllabification
    $pron =~ s/["%#]//g;
    $pron =~ s/\.//g;
    $pron =~ s/^\s+//;
    $pron =~ s/\s+$//;
    $pron =~ s/\s+/ /g;

    @pp = split(/ /, $pron);

			# keep track of the actually seen symbols, substitute if required
		for $i ( 0 .. $#pp ) {
		  $s = $pp[$i];
				
		  if (defined $phone_map{$s}) {
				$pp[$i] = $phone_map{$s};
				$s = $pp[$i];
			}

			$symbols{$s}  = 0 unless defined $symbols{$s};
			$symbols{$s} += 1;
		}

		# print out this pronunciation
		print FO "$w ".join(' ', @pp)."\n";
		#print FO "\n";
	}

}
close (FI);
close (FO);

# write out every symbol, add tonality where applicable
open (FO, ">$fphonesout") or die "could not write phoneset\n";
for $s (sort keys %symbols) {
# print STDERR "$s\n";
	print FO "$s\n";
}
close (FO);

# write out questions
open (FO, ">$fqstout") or die "could not write questions\n";
  # no extra questions for regular dictionary
close (FO);

