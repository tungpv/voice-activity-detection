#!/usr/bin/perl -W

# Use perl internal lc function to lowercase utf-8 input 

use utf8;
use open qw (:encoding(UTF-8) :std);

undef $field_begin;
undef $field_end;

if (@ARGV > 0 && $ARGV[0] eq "-f") {
  shift @ARGV;
  $field_spec = shift @ARGV;
  if ($field_spec =~ m/^\d+$/) {
    $field_begin = $field_spec - 1; $field_end = $field_spec - 1;
  }
  if ($field_spec =~ m/^(\d*)[-:](\d*)/) { # accept e.g. 1:10 as a courtesty (properly, 1-10)
    if ($1 ne "") {
      $field_begin = $1 - 1; # Change to zero-based indexing.
    }
    if ($2 ne "") {
      $field_end = $2 - 1; # Change to zero-based indexing.
    }
  }
  if (!defined $field_begin && !defined $field_end) {
    die "Bad argument to -f option: $field_spec";
  }
}

binmode(STDIN, ":encoding(utf8)");
binmode(STDOUT, ":encoding(utf8)");

while (<>) {
  @l = split /\s+/;
  for ($i = 0; $i < @l; $i++) {
    if ( (!defined $field_begin || $i >= $field_begin)
         &&  (!defined $field_end || $i <= $field_end)) {
      next if ($l[$i] =~ m/^[\!<\[]/);
      $l[$i] = lc $l[$i];
    }
  }

  print join(" ", @l)."\n";
}

