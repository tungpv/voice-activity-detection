#!/usr/bin/perl

# koried, 10/24/2012
# Generate the segments and text file from the BABEL-style transcriptions

# Get file length from sph file
# compute num frames (1 + floor((samples/rate - 0.025)/0.01)
# Make sure that last index is within file!

use utf8;
use Unicode::Normalize;
use File::Basename;
use POSIX qw(floor);

$lowercase = "true";

$framerate = 100;
$framelength = 0.025;
# set unicode/utf8
binmode(STDIN, ":encoding(utf8)");
binmode(STDOUT, ":encoding(utf8)");
binmode(STDERR, ":encoding(utf8)");

(@ARGV eq 3) or die "usage: $0 file-list segments-out text-out\n";


($flist, $fsegout, $ftextout) = @ARGV;

open (FI, "<:encoding(UTF-8)", $flist) or die "Could not open BABEL transcription file $fbabel\n";
open (FO_SEG, ">$fsegout") or die "Could not write segments file $fsegout\n";
open (FO_TXT, ">:encoding(UTF-8)", $ftextout) or die "Could not write text file $ftextout\n";

# read id-list
while (<FI>) {
	chomp;
	$file = $_;
	$recid = basename($file, ".txt");
  $dirname = dirname($file, ".txt");

	# read transcription file
	open (FTRL, "<:encoding(UTF-8)", "$file") or die "Could not read transcription $file\n";

  # get number of total frames from sph file
  $reclen = `head -c1024 $dirname/../audio/$recid.sph | awk '/sample_count/ {c=\$3} /sample_rate/ {r=\$3} END {printf "%f", c/r; }'`;
  $numfrm = 1 + floor(($reclen - $framelength) * $framerate);

	$t1 = '';
	$t2 = '';
	$trl = '';
	$l = 0;

	while (<FTRL>) {
		chomp;
		$l++;

		# even lines are text, odd are times
		if ($l % 2 eq 0) {
			$trl = $_;

      if ($lowercase eq "true") {
        $trl = lc $trl;
      }
		} else {
		  m:([0-9.]+):;
      
      # As Adam does it:  Use 2 digits, and round it.
      $tt = sprintf("%.2f", $1);

			# initial time
			if (length $t1 eq 0) {
				$t1 = $tt;
				next;
			}
			
			$t2 = $tt;

      # "manually" multiply that two digit string by 100 (otherwise it might fail due to rounding issues)
      $t1i = $t1; $t1i =~ s/\.//;
      $t2i = $t2; $t2i =~ s/\.//;

      if ($t2i >= $numfrm) { 
        $t2i = $numfrm; 
      }
      
			printf FO_SEG "%s_%07d_%07d %s %.2f %.2f\n", $recid, $t1i, $t2i - 1, $recid, $t1, $t2;
			printf FO_TXT "%s_%07d_%07d %s\n", $recid, $t1i, $t2i - 1, $trl;

			$t1 = $t2;
		}
	}

	close (FTRL);
}

close (FI);

