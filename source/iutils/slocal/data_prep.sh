#!/bin/bash

# koried, 10/25/2012

# Extract the training information from the corpus;  to be run from the 
# language root directory

. path.sh

if [ $# != 1 ]; then
	echo "Usage: $0 configs/swordfish.106.config"
	exit 1;
fi

config=$1

# read config file
[ -f "$config" ] || ( echo "No such file $config"; exit 1; )
. $config

# verify a couple of required settings
[ -d $BABEL_DIR ] || ( echo "No such directory $BABEL_DIR"; exit 1; )
[ -f $BABEL_STM ] || ( echo "No such file $BABEL_STM"; exit 1; )

# 1  prepare word dictionary
echo "Preparing word dictionary..."

dir=data/local/dict
mkdir -p $dir

# Generate the lexicon and phoneset;  note that the phone set will remain fixed
# for the whole setup

babel_lex=$BABEL_DIR/conversational/reference_materials/lexicon.txt

if [ "$dict_tonal" == "true" ]; then
  slocal/mk_tonal_dict.pl $lex_arguments $map_phones $babel_lex $dir/nonsilence_phones.txt $dir/lexicon1.txt $dir/extra_questions.txt
else
  slocal/mk_plain_dict.pl $lex_arguments $map_phones $babel_lex $dir/nonsilence_phones.txt $dir/lexicon1.txt $dir/extra_questions.txt
fi

# Generate silphones
( echo SIL; echo SPN; echo NSN; echo LAU; echo BRE; echo COU; echo MUS ) > $dir/silence_phones.txt
echo SIL > $dir/optional_silence.txt

# Add noises to the lexicon
( echo '!SIL SIL'; echo '<unk>' SPN; echo '[NOISE]' NSN; echo '[BREATH]' BRE; echo '[COUGH]' COU; echo '[LAUGH]' LAU; echo '[MUSIC]' MUS ) | \
	cat - $dir/lexicon1.txt > $dir/lexicon2.txt

# make lowercase lexicon, if required
if [ "$dict_lc" == "true" ]; then
  LC_ALL=C slocal/make_lc.pl -f 1 $dir/lexicon2.txt | sort -u > $dir/lexicon.txt
else
  cp $dir/lexicon2.txt $dir/lexicon.txt
fi

# Generate syllable/character based lexicon;  here we distinguish between 
# character based languages (where 1 char = 1 syll) and "regular" languages
if [ "$make_syllable_dict" == "true" ]; then
  syll_dir=data/local/syll_dict
  mkdir -p $syll_dir

  # copy the phonetic information (this will be the same)
  cp $dir/{silence_phones,nonsilence_phones,extra_questions,optional_silence}.txt $syll_dir/
  
  # make the dictionary, keep track of the word-to-character mapping
  if [ "$dict_tonal" == "true" ]; then
    slocal/mk_char_dict.pl $lex_arguments $map_phones $babel_lex $syll_dir/lexicon1.txt $syll_dir/w2c_lex.txt
  else
    # this script assumes that there is no romanization
    slocal/mk_syllable_dict.sh $lex_arguments $babel_lex $syll_dir/lexicon1.txt $syll_dir/w2c_lex.txt
  fi

  # Add silence words;  no lower-casing here as the syllables need to match the phonetic symbols!
  ( echo '!SIL SIL'; echo '<unk>' SPN; echo '[NOISE]' NSN; echo '[BREATH]' BRE; echo '[COUGH]' COU; echo '[LAUGH]' LAU; echo '[MUSIC]' MUS ) | \
    cat - $syll_dir/lexicon1.txt > $syll_dir/lexicon.txt

  # However, do lowercase the w2c lexicon!
  if [ "$dict_lc" == "true" ]; then
    mv $syll_dir/w2c_lex.txt $syll_dir/w2c_lex.mixed.txt
    LC_ALL=C slocal/make_lc.pl -f 1 $syll_dir/w2c_lex.mixed.txt | sort -u > $syll_dir/w2c_lex.txt
    rm $syll_dir/w2c_lex.mixed.txt
  fi
fi

# The following lines will extract the basic information required for the 
# training set
echo "Preparing training partition (transcriptions, etc.)"

dir=data/local/train
mkdir -p $dir

# Prepare file list, generate segments and transcripts file
/bin/ls $BABEL_DIR/conversational/training/transcription | sort | \
  sed s:^:$BABEL_DIR/conversational/training/transcription/: > $dir/flist

# Prepare the transcription of the turns
slocal/preptrl.pl $dir/flist $dir/segments-all $dir/text-all.raw

# Make master transcription lower-case, if required
if [ "$dit_lc" == "true" ]; then
  mv $dir/text-all.raw $dir/text-all.raw.case_sensitive
  slocal/make_lc.pl -f 2- $dir/text-all.raw.case_sensitive > $dir/text-all.raw
fi

# Filter the transcripts to a usable format;  collect info on bad turns.
# Note that there is no special treatment for the syllable stuff, as we train
# on the word constraints!

slocal/filtertrl.pl \
  --write-bad-turns $dir/tlist-bad \
  --write-noisy-turns $dir/tlist-noisy \
  --write-overlap-turns $dir/tlist-overlap \
  --write-spkchange-turns $dir/tlist-spkchange \
  data/local/dict/lexicon.txt < $dir/text-all.raw > $dir/text-all

# Generate speaker info:  utt2spk, spk2gender.map
awk '{print $1}' $dir/text-all | \
  sed -e 's:\(BABEL_[A-Z]*_[0-9]*_[0-9]*_[0-9]*_[0-9]*_[a-zA-Z]*\)\(_[0-9]*_[0-9]*\):\1\2 \1:' > $dir/utt2spk-all

utils/utt2spk_to_spk2utt.pl < $dir/utt2spk-all > $dir/spk2utt-all

for spk in `awk '{print $1}' $dir/spk2utt-all`; do 
  echo -n "$spk "; 
  grep $spk $BABEL_DIR/conversational/reference_materials/demographics.tsv | \
    head -n1 | awk '{print tolower($8)}'; # make sure it's only one
done > $dir/spk2gender.map

# Generate reco2file_and_channel;  here we only have one channel per file 
# (other than with switchboard, for example), thus this is straight forward
# Mapping is reco-id base-file channel-no
awk '{print $1" "$1" 1"}' $dir/spk2utt-all > $dir/reco2file_and_channel

# Throw away all potentially bad utterances (plenty...)
cat $dir/tlist-{bad,noisy,overlap,spkchange} | sort -u > $dir/tlist-ignore
slocal/reducelist.pl $dir/tlist-ignore < $dir/text-all > $dir/text
slocal/reducelist.pl $dir/tlist-ignore < $dir/segments-all > $dir/segments
slocal/reducelist.pl $dir/tlist-ignore < $dir/utt2spk-all > $dir/utt2spk
utils/utt2spk_to_spk2utt.pl < $dir/utt2spk > $dir/spk2utt

# make actual data directory (these files will be copied by all features)
mkdir -p data/train
cp $dir/{text,segments,utt2spk,spk2utt,reco2file_and_channel} data/train/

# The dev data will be imported by a seperate script (via the features, actually)

