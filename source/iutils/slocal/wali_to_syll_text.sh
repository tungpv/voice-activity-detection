#!/bin/bash

# koried, 2/18/2013

# Convert forced word alignments to sequences of syllables to be used for 
# syllable LM training;  outputs utt ids.

cmd=utils/run.pl

[ -f path.sh ] && . ./path.sh;
. parse_options.sh || exit 1;

if [ $# != 6 ]; then
  echo "Usage: $0 wlexicon.txt slexicon.txt lang-dir data-dir ali-dir out-dir"
  echo " e.g.: $0 data/local/{dict/lexicon,syll_dict/w2c_lex}.txt data/lang data/train exp/tri4a_ali exp/tri4a_ali/text"
  exit 1;
fi

lexicon=$1
syll=$2
lang=$3
data=$4
alidir=$5
dir=$6

for f in $lexicon $syll $alidir/final.mdl $lang/{phones,words}.txt $alidir/num_jobs; do
  [ -f $f ] || ( echo "wali_to_syll_text.sh:  no such file $f";  exit 1; )
done

nj=`cat $alidir/num_jobs`
silword=`cat $lang/phones/optional_silence.txt`

if [ ! -f $lang/L_align.fst ]; then
  echo "Building L_align.fst"
  cat $lexicon | \
    awk '{printf("%s #1 ", $1); for (n=2; n <= NF; n++) { printf("%s ", $n); } print "#2"; }' | \
    utils/make_lexicon_fst.pl - 0.5 $silword | \
    fstcompile --isymbols=$lang/phones.txt --osymbols=$lang/words.txt \
      --keep_isymbols=false --keep_osymbols=false | \
    fstarcsort --sort_type=olabel > $lang/L_align.fst
fi

# word begin/end symbols
wbegin=`awk '/#1/ { print $2 }' $lang/phones.txt`
wend=`awk '/#2/ { print $2 }' $lang/phones.txt`

oov=`cat $lang/oov.int`

mkdir -p $dir/log

echo "Converting word alignments to pronunciations..."
# convert to intermediate files;  they will contain lines like
#   uttid  w1 phn1 phn2 phn3 ; word2 phn4 phn5 phn6 [; ...]
$cmd JOB=1:$nj $dir/log/ali2pron.JOB.log \
  ali-to-phones $alidir/final.mdl "ark:gunzip -c $alidir/ali.JOB.gz |" ark:- \| \
  phones-to-prons $lang/L_align.fst $wbegin $wend ark:- \
    "ark:utils/sym2int.pl --map-oov $oov -f 2- $lang/words.txt $data/split$nj/JOB/text |" "ark,t:|gzip -c > $dir/pron.JOB.int.gz" || exit 1;

echo "Pronunciations to syllables..."
# convert the pronunciations to syllabifications
$cmd JOB=1:$nj $dir/log/pron2syl.JOB.log \
  gunzip -c $dir/pron.JOB.int.gz \| \
    slocal/prons_to_syll.pl $syll $lang/{words,phones}.txt \| \
    gzip -c ">" $dir/syll.JOB.tra.gz || exit 1;

echo "Merging text"
# merge files, strip uttids
gunzip -c $dir/syll.*.tra.gz | cut -d' ' -f 2- > $dir/text

