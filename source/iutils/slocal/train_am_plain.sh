#!/bin/bash

# koried, 2/13/2012

# Acoustic model training template;  uses settings from config file
if [ $# -lt 2 ]; then
  echo "Usage: $0 [ options ] config features"
  exit 1;
fi

. cmd.sh

# config;  change any of the following variables via command line (e.g. --feat-dim 45)
stage=1
num_train=20
feat_dim=30

# what do you want to train
do_sgmm=true
do_mmi=false
do_sgmm_mmi=false

. utils/parse_options.sh

. $1

feats=$2

echo `date` Starting training sequence

# don't forget to call with --feat-dim!
if [ $stage -le 1 ]; then
  steps/precomp/train_mono.sh --feat_dim $feat_dim --nj $num_train --cmd "$train_cmd" \
    $feats/data/train_$part_train1 data/lang $feats/exp_plain/mono0a || exit 1;
fi

# step up to 20k shortest;  train deltas
if [ $stage -le 2 ]; then
  echo `date` Entering stage 2
  steps/precomp/align_si.sh --nj $num_train --cmd "$train_cmd" \
    $feats/data/train_$part_train2 data/lang $feats/exp_plain/mono0a $feats/exp_plain/mono0a_ali || exit 1;
  steps/precomp/train_deltas.sh --cmd "$train_cmd" \
    $tp_s2_ns $tp_s2_nd $feats/data/train_$part_train2 data/lang $feats/exp_plain/mono0a_ali $feats/exp_plain/tri1 || exit 1;
fi

# step up to 37k (about 50% of training data)
if [ $stage -le 3 ]; then
  echo `date` Entering stage 3
  steps/precomp/align_si.sh --nj $num_train --cmd "$train_cmd" \
    $feats/data/train_$part_train3 data/lang $feats/exp_plain/tri1 $feats/exp_plain/tri1_ali || exit 1 ;
  steps/precomp/train_deltas.sh --cmd "$train_cmd" \
    $tp_s3_ns $tp_s3_nd $feats/data/train_$part_train3 data/lang $feats/exp_plain/tri1_ali $feats/exp_plain/tri2 || exit 1;
fi

# step up to all data;  train LDA+MLLT system
if [ $stage -le 4 ]; then
  echo `date` Entering stage 4
  steps/precomp/align_si.sh --nj $num_train --cmd "$train_cmd" \
    $feats/data/train data/lang $feats/exp_plain/tri2 $feats/exp_plain/tri2_ali || exit 1;
  steps/precomp/train_mllt.sh --cmd "$train_cmd" \
    $tp_s4_ns $tp_s4_nd $feats/data/train data/lang $feats/exp_plain/tri2_ali $feats/exp_plain/tri3a || exit 1;
fi

# re-align all the data, increase states+gaussians
if [ $stage -le 5 ]; then
  echo `date` Entering stage 5
  steps/precomp/align_fmllr.sh --nj $num_train --cmd "$train_cmd" \
    $feats/data/train data/lang $feats/exp_plain/tri3a $feats/exp_plain/tri3a_ali || exit 1;
  steps/precomp/train_sat.sh --cmd "$train_cmd" \
    $tp_s5_ns $tp_s5_nd $feats/data/train data/lang $feats/exp_plain/tri3a_ali $feats/exp_plain/tri4a || exit 1;
fi

# some more sophisticated models based on tri4a
if [ $stage -le 6 ]; then
  echo `date` Entering stage 6

  # align data using tri4a
  if [ ! -d $feats/exp_plain/tri4a_ali ]; then
    echo `date` Aligning training data
    steps/precomp/align_fmllr.sh --nj $num_train --cmd "$train_cmd" \
      $feats/data/train data/lang $feats/exp_plain/tri4a $feats/exp_plain/tri4a_ali || exit 1;
  fi

  # sgmm model
  if $do_sgmm; then
    if [ ! -f $feats/exp_plain/ubm5a/final.ubm ]; then
      echo `date` Training UBM
      steps/precomp/train_ubm.sh --cmd "$train_cmd" $tp_s6_ubm $feats/data/train data/lang \
        $feats/exp_plain/tri4a_ali $feats/exp_plain/ubm5a || exit 1;
    fi
    echo `date` Training SGMM system
    steps/precomp/train_sgmm2.sh --cmd "$train_cmd" \
      $tp_s6_ns $tp_s6_nd $feats/data/train data/lang $feats/exp_plain/tri4a_ali \
      $feats/exp_plain/ubm5a/final.ubm $feats/exp_plain/sgmm5a || exit 1;
  fi

  # MMI trainig
  if $do_mmi; then
    echo `date` Making dense lattices
    steps/precomp/make_denlats.sh --nj $num_train --cmd "$decode_cmd" --config conf/decode.config \
      --transform-dir $feats/exp_plain/tri4a_ali \
      $feats/data/train data/lang $feats/exp_plain/tri4a $feats/exp_plain/tri4a_denlats || exit 1;

    echo `date` Starting MMI training
    steps/precomp/train_mmi.sh --cmd "$decode_cmd" --boost 0.1 \
      --transform-dir $feats/exp_plain/tri4a_ali \
      $feats/data/train data/lang $feats/exp_plain/tri4a_{ali,denlats} $feats/exp_plain/tri5a_mmi_b0.1 || exit 1;
  fi

  # SGMM/MMI training
  if $do_sgmm_mmi; then
    echo `date` Aligning training data with SGMM
    steps/precomp/align_sgmm2.sh --nj $num_train --cmd "$train_cmd" --use-graphs true --use-gselect true \
      --transform-dir $feats/exp_plain/tri4a_ali \
      $feats/data/train data/lang $feats/exp_plain/sgmm5a $feats/exp_plain/sgmm5a_ali || exit 1;

    # Took the beam down to 8 to get acceptable decoding speed.
    echo `date` Making dense lattices
    steps/precomp/make_denlats_sgmm2.sh --nj $num_train --beam 8.0 --lattice-beam 6 --cmd "$decode_cmd" \
      --transform-dir $feats/exp_plain/tri4a_ali \
      $feats/data/train data/lang $feats/exp_plain/sgmm5a_{ali,denlats} || exit 1;

    echo `date` Starting SGMM MMI training
    steps/precomp/train_mmi_sgmm2.sh --cmd "$decode_cmd" --transform-dir $feats/exp_plain/tri4a_ali --boost 0.1 \
      $feats/data/train data/lang $feats/exp_plain/sgmm5a_{ali,denlats,mmi_b0.1} || exit 1;
  fi
fi

echo "Finished training run $feats on" `date`

