#!/bin/bash

i=$1


grep -v $i ../data/local/train/text-all | \
  cut -d ' ' -f 2- | awk '{if (NF > 0) print; }' | \
  ngram-count -order 3 -text - -kndiscount -lm $i.arpa

grep $i ../data/local/train/text-all | \
  cut -d ' ' -f 2- | awk '{if (NF>0) print;}' | \
  ngram -lm $i.arpa -ppl - > $i.ppl

rm -f $i.arpa

