#!/usr/bin/perl -w

(@ARGV >= 1) or die "usage:  $0 syll-lex [word-to-syll] > valid\n";


open (FI, "<:encoding(utf8)", "${ARGV[0]}") or die "could not read ${ARGV[0]}\n";
while (<FI>) {
  @A = split /\s+/;
  $syll{$A[0]} = 1;
}
close (FI);

while (<>) {
  @A = split /\s+/;
  $m = 0;
  for ($i = 1; $i < @A; $i++) {
    if (defined $syll{$A[$i]}) {
      $m += 1;
    } else {
      last;
    }
  }
  if ($m + 1 == @A) {
    print STDOUT join(" ", @A)."\n";
  }
}

