#!/bin/bash

conf=$1

. $conf

slocal/wali_to_syll_text.sh data/local/lang/lexicon.txt data/local/syll_dict/w2c_lex.txt \
  data/lang srs_bn/data/train_llp srs_bn/exp_llp/tri4a_ali srs_bn/exp_llp/tri4a_ali_text

slocal/train_lm.sh $conf $lm_syll_order data/local/syll_dict/lexicon.txt \
  srs_bn/exp_llp/tri4a_ali_text/text data/local/syll.llp.arpa.gz

slocal/prepare_lang_test.sh data/syll_lang data/local/syll.llp.arpa.gz data/syll_lang_test_llp
