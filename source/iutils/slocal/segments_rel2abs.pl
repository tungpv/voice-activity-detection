#!/usr/bin/perl

# koried, 6/14/2013

# convert utterance timestamps into absolute recording timestamps
# input is lines of "utt-id start end"

if (@ARGV < 1) {
  print STDERR "usage:  $0 segments [timestampfile]\n";
  exit 1;
}

my $segf = shift @ARGV;
open (FI, "<$segf") or die "could not open file $segf\n";
while (<FI>) {
  @A = split;
  @A == 4 or die "malformed line in $segf\n";
  $utt = shift @A;
  $utt2seg{$utt} = [ @A ];
}
close (FI);

while (<>) {
  ($utt, $s, $e) = split;
  unless (defined $utt2seg{$utt}) {
    print STDERR "Warning:  Undefined utterance $utt\n";
    next;
  }
  printf "%s %f %f\n", $utt2seg{$utt}[0], $utt2seg{$utt}[1] + $s, $utt2seg{$utt}[1] + $e;
}
