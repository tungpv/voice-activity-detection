#!/usr/bin/perl

while (<>) {
  @A = split;
  @N = ($A[0]);
 
  for ($i = 2; $i < @A; $i++) {
    # ignore first word, and any following word (w = preceding ';')
    next if ($A[$i - 1] eq ";");
    
    push @N, $A[$i];
  }

  printf "%s\n", join(" ", @N);
}

