#!/usr/bin/perl -W

# koried, 6/23/2013

# Generate a syllable-to-word lexicon;  for "regular" languages, we allow the
# (penalized) insertion of extra syllables that don't fit into words, for
# syllabic languages, every syllable may also stand for itself as a word.  Note
# that this will lead to very large lattices, so you might want to use 
# lattice-prune afterwards.

# Use this lexicon to build a syllable-to-word FST that can be used to map
# syllable lattices to word lattices.

$unk = "<unk>";
$syllabic = 0;


while ($ARGV[0] =~ m/^--/) {
  $arg = shift @ARGV;
  if ($arg eq "--syllabic") {
    $syllabic = 1;
  } elsif ($arg eq "--unk") {
    $unk = shift @ARGV;
  } else {
    print STDERR "Invalid argument $arg\n";
    exit 1;
  }
}  

if (@ARGV != 2) {
  print STDERR "usage:  $0 syll-vocab w2s-dict > lexicon.txt\n";
  exit 1;
}

($syll_vocab, $w2s_dict_file) = @ARGV;


open (F, "<:encoding(utf8)", $syll_vocab) or die "could not read syllable vocabulary $syll_vocab\n";
while (<F>) {
  chomp;
  $vocab{$_} = 1
}
close (F);

# make sure we output UTF8
binmode(STDOUT, ":encoding(utf8)");

open (F, "<:encoding(utf8)", $w2s_dict_file) or die "could not read word-to-syllable dictionary $w2s_dict_file\n";
while (<F>) {
  @A = split;
  $w = shift @A;
  if (@A == 1) { $singletons{$A[0]} = 1; }

  print "$w ".join(" ", @A)."\n";
}
close (F);

# for "regular" languages:  output an unk-word for each non-singleton syllable
# to allow the insertion of non-mappable syllables.
# for syllabic languages:  make sure that each of the syllables also appears
# independently of a word.
foreach $s (keys %vocab) {
  if (! defined $singletons{$s}) {
    if ($syllabic == 0) { print "$unk $s\n"; }
    else { print "$s $s\n"; }
  }
}

