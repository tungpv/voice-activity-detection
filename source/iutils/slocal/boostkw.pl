#!/usr/bin/perl

use utf8;

binmode(STDIN, ":encoding(utf8)");
binmode(STDOUT, ":encoding(utf8)");
binmode(STDERR, ":encoding(utf8)");

$order = 3;

if (@ARGV != 2) {
  print STDERR "usage:  $0 counts kw.txt\n";
  exit 1;
}

($fcounts, $fkw) = @ARGV;

# read in keywords
open (FI, "<:encoding(UTF-8)", $fkw) or die "Could not read counts file $flexin\n";
while (<FI>) {
  @A = split;
  $kw{join(" ", @A)} = scalar @A;
  print STDERR join(" ", @A)." ".scalar @A."\n";
}
close (FI);

# read in counts
@bins = ();
for ($i = 0; $i < $order; $i++) {
  push @bins, {};
}

open (FI, "<:encoding(UTF-8)", $fcounts) or die "Could not read counts file $flexin\n";
while (<FI>) {
  @A = split;

  $c = pop @A;
  $n = join (" ", @A);

  $bins[scalar @A]{$n} = $c;
}
close (FI);

# now work on keyword ngrams, 
