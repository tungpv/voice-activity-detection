#!/usr/bin/perl

# koried, 6/14/2013

# convert utterance timestamps into segment timestamps
# input is lines of "rec-id start end"

if (@ARGV < 1) {
  print STDERR "usage:  $0 segments [timestampfile]\n";
  exit 1;
}

my $segf = shift @ARGV;
open (FI, "<$segf") or die "could not open file $segf\n";
while (<FI>) {
  ($utt, $spk, $s, $e) = split;
  $spk2utt{$spk} = () unless defined $spk2utt{$spk};
  push @{$spk2utt{$spk}}, $utt;
  $utt2seg{$utt} = [ $s, $e ];
}
close (FI);

while (<>) {
  ($rec, $s, $e) = split;
  unless (defined $spk2utt{$spk}) {
    print STDERR "Warning:  Undefined recording $rec\n";
    next;
  }
  
  # find right segment
  $found=0;
  for $utt (@{$spk2utt{$rec}}) {
    # printf "? %s %f %f %f %f\n", $utt, $utt2seg{$utt}[0], $utt2seg{$utt}[1], ($utt2seg{$utt}[0] - $s), $e - ($utt2seg{$utt}[1]);
    if ($s >= $utt2seg{$utt}[0] and $e <= $utt2seg{$utt}[1]) {
      printf "%s %f %f\n", $utt, $s - $utt2seg{$utt}[0], $e - $utt2seg{$utt}[0];
      $found=1;
      last;
    }
  }
  print STDERR "Warning:  No matching segment found for $rec, $s, $e\n" if $found eq 0;
}

