#!/bin/bash

# koried, 2/5/2013

# Import a dev/decoding partition based on a HTK scp and an stm file and
# store it in the given output directory.  You may specify to import certain
# dimensions only (e.g. if you import only static features from the full
# (static+d+dd+bn vector).
# Use an empty stm file for eval runs.

compute_cmvn=true
fr=

. cmd.sh

if [ -f path.sh ]; then . ./path.sh; fi

. parse_options.sh || exit 1;

if [ $# -lt 4 ]; then
  echo "usage: $0 [--compute-cmvn true ] [ --fr 0:14 ] scp-file stm-file base-dir partition-name";
  exit 1;
fi

# adjust -fr argument
[ "$fr" != "" ] && fr="--fr $fr";

scp_file=$1
stm_file=$2
outdir=$3
name=$4

# some checks
[ -e $outdir ] || ( echo "No such directory $outdir";  exit 1; )
[ -f $scp_file ] || ( echo "No such file $scp_file";  exit 1; )
[ -f $stm_file ] || ( echo "No such file $stm_file";  exit 1; )

# generate the data and exp directory
ddir=$outdir/data/$name
fdir=$outdir/exp/feats

mkdir -p $ddir $fdir

# convert the features to kaldi format
cat $scp_file | \
  slocal/htk2kaldi.pl $fr | \
  copy-feats ark,t,cs:- ark,scp:$fdir/feats_$name.ark,$ddir/feats.scp || exit 1;
  
# generate the required files:  stm, utt2spk (spk2utt), reco2file_and_channel
awk '{print $1}' $ddir/feats.scp | \
  sed -e 's:\(BABEL_[A-Z]*_[0-9]*_[0-9]*_[0-9]*_[0-9]*_[a-zA-Z]*\)\(_[0-9]*_[0-9]*\):\1\2 \1:' > $ddir/utt2spk

utils/utt2spk_to_spk2utt.pl $ddir/utt2spk > $ddir/spk2utt

# make recording-to-channel file (trivial)
awk '{print $1" "$1" 1"}' $ddir/spk2utt > $ddir/reco2file_and_channel

# make segments file from scp file
slocal/scp2segments.pl $scp_file > $ddir/segments

awk '{print $1}' $ddir/spk2utt > $ddir/reclist
utils/filter_scp.pl $ddir/reclist $stm_file > $ddir/stm

# compute cmvn stats
if $compute_cmvn; then
  steps/compute_cmvn_stats.sh $ddir $outdir/exp/make_feats/$name.log $fdir || exit 1;
fi

echo "Done preparing dev features.  Don't forget to generate data subsets as applicable!"

