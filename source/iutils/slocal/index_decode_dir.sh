#!/bin/bash

if [ $# == 0 ]; then
  echo "usage:  $0 [ -c config-dir ] decode_dir1 [decode_dir2 ...]"
  exit 1
fi

if [ $1 == "-c" ]; then
  shift;
  confdir=$1
  shift;
fi

for i in $@; do
  id=${i##decode_}
  if [ -f $i/config ]; then 
    . $i/config
    echo "$i,$id,$data,$rec1_lang,$rec2_lang,$num_decode"
  elif [ -f $confdir/$id.config ]; then
    . $confdir/$id.config
    echo "$i,$id,$data,$rec1_lang,$rec2_lang,$num_decode"
#  else
#    echo "Ignoring unconfigured directory $i (id=$id)"
  fi
done

