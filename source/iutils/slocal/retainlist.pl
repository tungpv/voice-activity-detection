#!/usr/bin/perl -W

# koried, 10/24/2012

# reduce a list (considering the first element per line)

(@ARGV eq 1) or die "usage: $0 to-retain < in-list > out-list\n";

open (FI, "<${ARGV[0]}") or die "Could not open to-remove list\n";
%torem = ();
while (<FI>) {
	chomp;
	@fld = split /\s+/;
	$torem{$fld[0]} = 1;
}
close (FI);

while (<STDIN>) {
  chomp;
	$h = $_;
	@fld = split /\s+/;

	print "$h\n" if defined $torem{$fld[0]};
}

