#!/usr/bin/perl -W

# koried, 10/24/2012

# Generate a phone/tone set and a lexicon based on the BABEL master lexicon.
# Note that this script will generate a "full" phone set, i.e., for each tonal
# and syllabic symbol, all the tones will be generated regardless of their
# appearence in the lexicon-- the phone set is cast into the decision tree and
# can't be changed later on.

use utf8;
use Unicode::Normalize;

# set unicode/utf8
binmode(STDIN, ":encoding(utf8)");
binmode(STDOUT, ":encoding(utf8)");
binmode(STDERR, ":encoding(utf8)");

$cluster_by_tone = 'false';
$has_romanization = 'true';
$phone_map_file = '';

%tonal = map { $_ => 0 } split(/ /,  'i: E: a: 6 u: y: O: 9: iw ej 6w 6j a:j a:w O:j ow u:j 9y');
@tones = ( 1 .. 6 );

%tonal_if_syllabic = map { $_ => 0 } split(/ /, 'm n N l');
$syllabic_marker = '=';
@syllabic_tones = ( 4 .. 6 );

$syllable_delimiter = '.';

while (@ARGV gt 0 and $ARGV[0] =~ m/^--/) {
	$param = shift @ARGV;

	if ($param eq "--cluster-by-tone") {
		$cluster_by_tone = shift @ARGV;
	} elsif ($param eq "--has-romanization") {
		$has_romanization = shift @ARGV;
	} elsif ($param eq "--map-phones") {
		$phone_map_file = shift @ARGV;
	} else {
		die "unknown parameter $param\n";
	}
}

%phone_map = ();
if ($phone_map_file ne '') {
	open (FI, "<$phone_map_file");
	while (<FI>) {
		chomp;
		my @l = split /\s+/;
		for $i (1 .. $#l) {
			$phone_map{$l[$i]} = $l[0];
		}
	}
	close (FI);
}

if (@ARGV ne 4) {
	print STDERR "Extract the phone set and dictionary from the BABEL-style master-lexicon
and generate the extra_questions.txt for the later tree building.
usage: $0 babel-lexicon phones-out lexicon-out questions-out
options:
  --cluster-by-tone (true|false)
    Generate the extra_questions.txt by grouping phonemes with the same
    tone;  default: false
  --has-romanization (true|false)
    Specifies if the BABEL-style lexicon has the romanization column;  default:  true
  --map-phones (mapfile)
    A set of phones to the same identifier
";
	exit 1;
}

# empiric list of tonal symbols, will be used to generate phoneset
%symbols = ();

($flexin, $fphonesout, $flexout, $fqstout) = @ARGV;

# read in lexicon
open (FI, "<:encoding(UTF-8)", $flexin) or die "Could not read lexicon file $flexin\n";
open (FO, ">:encoding(UTF-8)", $flexout) or die "Could not write lexicon file $flexout\n";
while (<FI>) {
	chomp;

	# line format is word\t[romanization\t]pron1[\tpron2 ...]
	@ln = split /\t/;

	$w = shift @ln;

	#print STDERR "$w\n";

	# get rid of romanization, if present
	shift @ln if ($has_romanization eq 'true');

	for $pron (@ln) {
		# print out word (alt-prons just give an extra line)
		print FO "$w ";

		@sl = split(/ /, $pron);

		# get syllables
		while (@sl > 0) {
			@syl = ();
			$ton = 0;
			while (@sl > 0 and $sl[0] ne $syllable_delimiter) {
				$s = shift @sl;

				# see if it's the tonality information
				if ($s =~ m/^_(\d)/) {
					$ton = $1;
				} else {
					push (@syl, $s);
				}
			}

			#print STDERR "  ".(scalar @syl)." ".join(' ', @syl)." $ton\n";

			# this should not happen...
			if ($ton eq 0) {
				print STDERR "Warning: $w syllable ".join(" ", @syl)." has no tonality?\n";
			}

			# see if that is a syllabic nasal/liquid;  otherwise, go through the syllable
			# and attach the tonality indicator to the vowel
			if (@syl eq 1 and defined $tonal_if_syllabic{$syl[0]}) {
				$syl[0] .= $syllabic_marker."_".$ton;
			} else {
				for $i ( 0 .. $#syl ) {
					if (defined $tonal{$syl[$i]}) {
						$syl[$i] .= "_$ton";
					}
				}
			}

			# keep track of the actually seen symbols, substitute if required
			for $i ( 0 .. $#syl ) {
				$s = $syl[$i];
				
				if (defined $phone_map{$s}) {
					$syl[$i] = $phone_map{$s};
					$s = $syl[$i];
				}

				$symbols{$s}  = 0 unless defined $symbols{$s};
				$symbols{$s} += 1;
			}

			# print out this syllable
			print FO join(' ', @syl);

			if (@sl gt 0) {
				shift @sl;
				print FO ' ';
			}
		}

		print FO "\n";
	}
}
close (FI);
close (FO);

# write out every symbol, add tonality where applicable
open (FO, ">$fphonesout") or die "could not write phoneset\n";
for $s (sort keys %symbols) {
# print STDERR "$s\n";
	print FO "$s\n";
}
close (FO);

# write out questions
open (FO, ">$fqstout") or die "could not write questions\n";
if ($cluster_by_tone eq 'true') {
# ask questiuons about the tonality
	for $t (@tones) {
		$ps = '';
		for $s (sort keys %tonal) {
			$st = $s."_".$t;

			# map if applicable
			$st = $phone_map{$st} if defined $phone_map{$st};

			# only print if the symbol actually exists
			next unless defined $symbols{$st};

			$ps .= "$st " unless $ps =~ m/\b$st\b/; # append, but no duplicates
		}
		chomp $ps;
		print FO "$ps\n" if length $ps gt 0;
		print STDERR "mk_tonal_dict.pl: tone $t was not seen!\n" if length $ps eq 0;
	}

	for $t (@syllabic_tones) {
		$ps = '';
		for $s (sort keys %tonal_if_syllabic) {
			$st = $s."=_".$t;
			$st = $phone_map{$st} if defined $phone_map{$st};
			next unless defined $symbols{$st};
			$ps .= "$st " unless $ps =~ m/\b$st\b/;
		}
		chomp $ps;
		print FO "$ps\n" if length $ps gt 0;
		print STDERR "mkdict.pl: syllabic tone $t was not seen!\n" if length $ps eq 0;
	}
} else {
# ask questions about the same phone
	for $s (sort keys %tonal) {
		$ps = '';
		for $i ( 0 .. $#tones ) {
			$st = $s."_".$tones[$i];
			
			# map if applicable
			$st = $phone_map{$st} if defined $phone_map{$st};

			# only print if the symbol actually exists
			next unless defined $symbols{$st};

			$ps .= "$st " unless $ps =~ m/\b$st\b/; # append, but no duplicates
		}
		chomp $ps;
		print FO "$ps\n" if length $ps gt 0;
		print STDERR "mkdict.pl: tone $s was not seen!\n" if length $ps eq 0;
	}
	for $s (sort keys %tonal_if_syllabic) {
		$ps = '';
		for $i ( 0 .. $#syllabic_tones ) {
			$st = $s."=_".$syllabic_tones[$i];
			$st = $phone_map{$st} if defined $phone_map{$st};
			next unless defined $symbols{$st};
			$ps .= "$st " unless $ps =~ m/\b$st\b/;
		}
		chomp $ps;
		print FO "$ps\n" if length $ps gt 0;
		print STDERR "mkdict.pl: syllabic tone $s was not seen!\n" if length $ps eq 0;
	}
}
close (FO);

