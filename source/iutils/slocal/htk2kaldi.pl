#!/usr/bin/perl -W

$feacat = "feacat";

$fr = "";
$segfile = "";

while (@ARGV > 0) {
  if ($ARGV[0] eq "--fr") {
  	shift @ARGV;
  	$fr = "-fr ".(shift @ARGV);
  } else {
    $segfile = shift @ARGV;
  }
}

$enforce_segments = 'false';
%segments_required = ();
if (length $segfile > 0) {
	open (FI, "<$segfile") or die "couldn't read segment list\n";
	shift @ARGV;
	$enforce_segments = 'true';
	while (<FI>) {
		chomp;
		$segments_required{$_} = 0;
	}
	close (FI);
}

@ARGV eq 0 or die "usage: $0 [ --fr 0:14 ] [ seg-list ] < segment-list > kaldi-ascii\n";

$ntossed = 0;

# autoflush STDOUT
$| = 1;

while (<STDIN>) {
	chomp;
	
	m:(BABEL_[A-Z]+_[0-9]{3}_[0-9]+_[0-9]+_[0-9]+_[a-zA-Z]+_[0-9]+_[0-9]+)=([0-9a-zA-Z\+_\-/\.]+)\[([0-9]+),([0-9]+)\]:;
	($turn, $file, $t1, $t2) = ($1, $2, $3, $4);

	if ($enforce_segments eq 'true') {
		unless (defined $segments_required{$turn}) {
			# print STDERR "Warning: input segment $turn not present in enforced list\n";
			$ntossed += 1;
			next;
		}
		$segments_required{$turn} = 1;
	}

  # open feacat pipe, provide it with the respective feature selection and truncation
  $cmd = "$feacat -ip htk -i $file -op ascii -o - $fr -pr $t1:$t2";
	open (FC, "-|", "$cmd") or die "Failed to open pipe to feacat with '$cmd'\n";
  print "$turn  [\n";
	while (<FC>) {
    s/^[0-9]+ [0-9]+ //;
    print;
	}
  print "]\n";
	close (FC);
}

# see which segments were not seen
for $t (sort keys %segments_required) {
	print STDERR "Warning: missing required segment $t\n" if $segments_required{$t} eq 0;
}

print STDERR "Tossed $ntossed segments\n" if $ntossed > 0;

