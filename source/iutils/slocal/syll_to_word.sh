#!/bin/bash

# koried, 6/26/2013

# Convert syllable lattices to aligned word lattices

cmd=utils/run.pl
latbase=lat
beam=13.0
max_mem=500000000

[ -f ./path.sh ] && . ./path.sh
. parse_options.sh || exit 1;

if [ $# -ne 3 ]; then
  echo "Usage:  $0 [--cmd (run.pl|queue.pl|...)] <lang-dir> <decode-dir> <new-decode-dir>"
  echo "  e.g.  $0 data/syll_to_words srs_bn/exp/sgmm5a/decode_dev_syll srs_bn/exp/sgmm5a/decode_dev_syll_toword"
  echo "Options:"
  echo "  --latbase <lattice-basename>   # default: lat (for lat.1.gz_"
  exit 1;
fi

lang=$1
olddir=$2
dir=$3

model=$dir/../final.mdl

for f in $lang/{Ldet,G}.fst $lang/word_align.int $lang/{phones,syllables,words}.txt $olddir/num_jobs $olddir/$latbase.1.gz; do
  [ ! -f $f ] && echo "$0: missing file $f" && exit 1;
done

[ -d $dir ] && echo "$0: directory $dir already exists" && exit 1;

mkdir -p $dir/log

nj=`cat $olddir/num_jobs`
cp {$olddir,$dir}/num_jobs

#
#  lattice-push ark:- ark:- \| \
#  lattice-align-words-lexicon --output-if-empty=true --max-expand=100 $lang/word_align.int $model ark:- ark:- \| \
#  lattice-determinize-pruned --beam=$beam --max-mem=$max_mem ark:- ark:- \| \

# determinize, compose(Ldet.fst), determinize
$cmd JOB=1:$nj $dir/log/syll_to_word.JOB.log \
  gunzip -c $olddir/${latbase}.JOB.gz \| \
  lattice-compose ark:- $lang/Ldet.fst ark:- \| \
  lattice-determinize-pruned --beam=$beam --max-mem=$max_mem ark:- ark:- \| \
  gzip -c - \> $dir/${latbase}.JOB.gz || exit 1;

# word alignment is done prior to KWS
