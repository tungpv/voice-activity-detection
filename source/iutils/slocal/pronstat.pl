#!/usr/bin/perl -W

# koried, 3/26/2013

# Count prevalent pronunciations for each words based on word/pronunciation 
# symbol vectors into text ones;  ignores non-words (id: 0).  If a utt2spk
# table is provided, stats will be speaker dependent.
# Input will be in form of 
#   utt-id  word1 phn1 phn2 phn3 ; word2 phn1 phn2 phn3 ;

if (scalar @ARGV < 2 or scalar @ARGV > 3) {
  print STDERR "usage: $0 words.txt phones.txt [utt2spk] < in.tra.int > stats.txt\n";
  exit 1;
}

$symtab1 = shift @ARGV;
open(F, "<$symtab1") or die "Error opening symbol table file $symtab1";
binmode(F, ":encoding(utf8)");
while(<F>) {
  @A = split(" ", $_);
  @A == 2 || die "bad line in symbol table file: $_";
  $int2sym1{$A[1]} = $A[0];
}

sub int2sym1 {
  my $a = shift @_;
  my $pos = shift @_;
  if($a !~  m:^\d+$:) { # not all digits..
    $pos1 = $pos+1; # make it one-based.
    die "int2sym.pl: found noninteger token $a [in position $pos1]\n";
  }
  $s = $int2sym1{$a};
  if(!defined ($s)) {
    die "int2sym.pl: integer $a not in symbol table $symtab1.";
  }
  return $s;
}

$symtab2 = shift @ARGV;
open(F, "<$symtab2") || die "Error opening symbol table file $symtab2";
while(<F>) {
  @A = split(" ", $_);
  @A == 2 || die "bad line in symbol table file: $_";
  $int2sym2{$A[1]} = $A[0];
}

sub int2sym2 {
  my $a = shift @_;
  my $pos = shift @_;
  if($a !~  m:^\d+$:) { # not all digits..
    $pos1 = $pos+1; # make it one-based.
    die "int2sym.pl: found noninteger token $a [in position $pos1]\n";
  }
  $s = $int2sym2{$a};
  if(!defined ($s)) {
    die "int2sym.pl: integer $a not in symbol table $symtab2.";
  }
  return $s;
}

sub trim {
  $a = shift @_;
  $a =~ s/^\s+//;
  $a =~ s/\s+$//;
  return $a;
}

$u2sf = shift @ARGV;
if (defined $u2sf) {
  open(F, "<$u2sf") or die "Error opening utt2spk $u2sf\n";
  while(<F>) {
    @A = split (" ", $_);
    @A == 2 or die "bad line in utt2spk: $_\n";
    $utt2spk{$A[0]} = $A[1];
  }
}

while (<>) {
  chomp;

  # insert ; after utt-id for consistent processing
  s/ / ; /;

  @A = split /;/;
  $utt = trim(shift @A);

  if (scalar @A == 0) {
    next;
  }

  for ($i = 0; $i < @A; $i++) {
    $w = trim($A[$i]);
    next if (length $w == 0 or $w =~ m/^0 /);
    
    @B = split (/ /, $w);
    $w = int2sym1(shift @B);

    next if $w =~ m/^\[/;

    for ($j = 0; $j < @B; $j++) {
      $B[$j] = int2sym2($B[$j], $i);
      $B[$j] =~ s/_[BEIS]$//; # remove position information, if present
    }
    $p = join(" ", @B);

    $hash = "$w $p";

    # general stats
    $proncount{$hash} =  0 unless defined $proncount{$hash};
    $proncount{$hash} += 1;
      
    # speaker dependent stats, if u2sf
    if (defined $u2sf) {
      $spkseen{$utt2spk{$utt}}{$w} = 1;
      $spkpcnt{$utt2spk{$utt}}{$hash} =  0 unless defined $spkpcnt{$utt2spk{$utt}}{$hash};
      $spkpcnt{$utt2spk{$utt}}{$hash} += 1;
    }
  }
}

binmode(STDOUT, ":encoding(utf8)");
# print stats
while (($k, $v) = each %proncount) {
  ($w, @pron) = split(" ", $k);
  printf "%s %s %d %s\n", "-", $w, $v, join(" ", @pron);
}

# speaker stats
if (defined $u2sf) {
  for $s (sort keys %spkpcnt) {
    while (($k, $v) = each %{$spkpcnt{$s}}) {
      ($w, @pron) = split(" ", $k);
      # don't print words we don't have prons for
      next unless defined $spkseen{$s}{$w};

      printf "%s %s %d %s\n", $s, $w, $v, join(" ", @pron);
    }
  }
}
