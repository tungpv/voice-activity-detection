#!/bin/bash

# koried, 6/17/2013

# make a syllable-to-word transducer that allows the insertion of non-singleton
# syllables (i.e. syllables that would only occur within a word, but may be 
# present in the syllable lattice)

unk_word="<unk>"

. utils/parse_options.sh

if [ $# -ne 6 ]; then
  echo "usage:  $0 data/local/syll_dict data/local/syll_lang data/syll_lang_test path/to/arpa.gz data/local/syll_to_word data/syll_to_word"
  exit 1;
fi

dict=$1
lang=$2
data=$3
arpa_lm=$4
tmpdir=$5
dir=$6

mkdir -p $tmpdir $dir

# get syllable vocabulary
awk '{print $1}' $dict/lexicon1.txt > $tmpdir/syll_vocab

# save the phones
cp $data/phones.txt $dir/phones.txt

# make syllable to word lexicon
slocal/make_syll_to_word_lex.pl $tmpdir/syll_vocab $dict/word_to_syllable.txt \
  > $tmpdir/lexicon1.txt

# add the silence words (those are just translated to themselves)
awk '{print $1, $1}' ../templates/silwords.asc | \
  cat - $tmpdir/lexicon1.txt > $tmpdir/lexicon.txt

# update disambiguation symbols
ndisambig=`utils/add_lex_disambig.pl $tmpdir/lexicon.txt $tmpdir/lexicon_disambig.txt`
ndisambig=$[$ndisambig+1];
echo $ndisambig > $tmpdir/lex_ndisambig
( for n in `seq 0 $ndisambig`; do echo '#'$n; done ) > $dir/disambig.txt

cat $tmpdir/lexicon.txt | awk '{print $1}' | sort | uniq | \
  awk 'BEGIN{print "<eps> 0";} {printf("%s %d\n", $1, NR);} END{printf("#0 %d\n", NR+1);} ' > $dir/words.txt

# the old words will become the new phones;  append the new disambiguation
# symbols that we need in the lexicon
( for n in `seq 1 $ndisambig`; do echo '#'$n; done) | \
  cat $data/words.txt - | awk '{if (NF==1) print $1, (NR-1); else print;}' > $dir/syllables.txt

utils/sym2int.pl $dir/syllables.txt $dir/disambig.txt > $dir/disambig.int

# make lexicon transducer, determinize, minimize, remove disambiguation symbols
utils/make_lexicon_fst.pl $tmpdir/lexicon_disambig.txt | \
  fstcompile --isymbols=$dir/syllables.txt --osymbols=$dir/words.txt \
    --keep_isymbols=false --keep_osymbols=false | \
  fstdeterminizestar | fstarcsort --sort_type=olabel > $dir L_disambig.fst

fstrmsymbols $dir/disambig.int $dir/L_disambig.fst | \
  fstarcsort --sort_type=olabel > $dir/Ldet.fst

# make integer lexicon for later alignment (use new word map but phone map of
# the syllable dir);  make sure to add epsilon entries for the optional silences
awk '{print "<eps> $1"; }' $data/phones/optional_silence.txt | \
  cat - $tmpdir/lexicon.txt | \
  utils/apply_map.pl -f 2- $lang/lexicon.txt | \
  awk '{ print $1, $0; }' > $dir/word_align.txt

# convert to int
utils/sym2int.pl -f 1-2 $dir/words.txt $dir/word_align.txt | \
  utils/sym2int.pl -f 3- $dir/phones.txt > $dir/word_align.int


# make the G transducer;  remove #0 and !SIL word
disambig_word=`grep -w "#0" $dir/words.txt | awk '{print $2}'`
sil_word=`grep -w "\!SIL" $dir/words.txt | awk '{print $2}'`
gunzip -c "$arpa_lm" | \
  grep -v '<s> <s>' | \
  grep -v '</s> <s>' | \
  grep -v '</s> </s>' | \
  arpa2fst - | fstprint | \
  utils/remove_oovs.pl /dev/null | \
  utils/eps2disambig.pl | utils/s2eps.pl | fstcompile --isymbols=$dir/words.txt \
    --osymbols=$dir/words.txt  --keep_isymbols=false --keep_osymbols=false | \
  fstrmepsilon | \
  fstrmsymbols --remove-from-output=false "echo $disambig_word|" | \
  fstrmsymbols --remove-from-output=true "echo $sil_word|"  | \
  fstarcsort > $dir/G.fst

