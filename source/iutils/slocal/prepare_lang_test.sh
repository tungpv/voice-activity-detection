#!/bin/bash 

# koried, 2/11/2013

# Starting from a data/lang directory, make a lang_test directory based on the given arpa LM

if [ $# != 3 ]; then 
  echo "Usage: $0 data/lang arpa.gz data/lang_test"
  exit 1;
fi

if [ -f path.sh ]; then . path.sh; fi

silprob=0.5

lang=$1
arpa_lm=$2
lang_test=$3

[ -d $lang ] || ( echo "No such directory $lang";  exit 1; )
[ -f $arpa_lm ] || ( echo "No such file $arpa_lm";  exit 1; )

if [ -d $lang_test ]; then
  echo "Directory $lang_test already exists.";
  exit 1;
fi

cp -r $lang $lang_test

# grep -v '<s> <s>' etc. is only for future-proofing this script.  Our
# LM doesn't have these "invalid combinations".  These can cause 
# determinization failures of CLG [ends up being epsilon cycles].
# Note: remove_oovs.pl takes a list of words in the LM that aren't in
# our word list.  Since our LM doesn't have any, we just give it
# /dev/null [we leave it in the script to show how you'd do it].
echo LANG=$LANG LC_ALL=$LC_ALL
gunzip -c "$arpa_lm" | \
   grep -v '<s> <s>' | \
   grep -v '</s> <s>' | \
   grep -v '</s> </s>' | \
   arpa2fst - | fstprint | \
   utils/remove_oovs.pl /dev/null | \
   utils/eps2disambig.pl | utils/s2eps.pl | fstcompile --isymbols=$lang_test/words.txt \
     --osymbols=$lang_test/words.txt  --keep_isymbols=false --keep_osymbols=false | \
     fstrmepsilon > $lang_test/G.fst || exit 1;


echo  "Checking how stochastic G is (the first of these numbers should be small):"
fstisstochastic $lang_test/G.fst 

## Check lexicon.
## just have a look and make sure it seems sane.
echo "First few lines of lexicon FST:"
fstprint   --isymbols=$lang/phones.txt --osymbols=$lang/words.txt $lang/L.fst | head

echo Performing further checks

# Checking that G.fst is determinizable.
fstdeterminize $lang_test/G.fst /dev/null || echo Error determinizing G.

# Checking that L_disambig.fst is determinizable.
fstdeterminize $lang_test/L_disambig.fst /dev/null || echo Error determinizing L_disambig.

# Checking that disambiguated lexicon times G is determinizable
# Note: we do this with fstdeterminizestar not fstdeterminize, as
# fstdeterminize was taking forever (presumbaly relates to a bug
# in this version of OpenFst that makes determinization slow for
# some case).
fsttablecompose $lang_test/L_disambig.fst $lang_test/G.fst | \
  fstdeterminizestar > /dev/null || echo Error

# Checking that LG is stochastic:
fsttablecompose $lang/L_disambig.fst $lang_test/G.fst | \
  fstisstochastic || echo LG is not stochastic

