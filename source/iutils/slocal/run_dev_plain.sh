#!/bin/bash

# BABEL_BP_101 recipe
# koried, 11/18/2012

. cmd.sh

decode_set=data/dev
num_decode=20
exp_dir=exp_plain
lang_dir=data/lang_full_test
config_file=conf/decode.config

. utils/parse_options.sh || exit 1;

if [ $# -ne 1 ]; then
  echo "usage: $0 features"
  echo " e.g. $0 --decode-set data/dev1 --lang_dir data/syll_lang --exp_dir exp_llp --num-decode 10 mfcc bn"
  exit 1;
fi

feats1=$1

graph_dir=`basename $lang_dir`
data_dir=`basename $decode_set`
conf_name=`basename $config_file`

rec_dir1=tri4a
rec_dir2=sgmm5a

decode_dir1=$feats1/$exp_dir/$rec_dir1/decode_${data_dir}_${graph_dir}_$conf_name
decode_dir2=$feats1/$exp_dir/$rec_dir2/decode_${data_dir}_${graph_dir}_$conf_name

# verify models, make graphs
[ -f $feats1/$exp_dir/$rec_dir1/final.mdl ] || ( echo "No such file $feats1/$exp_dir/$rec_dir1/final.mdl";  exit 1; )

if [ ! -f $feats1/$exp_dir/$rec_dir1/graph_$graph_dir/HCLG.fst ]; then
  echo "Building decoding graph $rec_dir1"
  $decode_cmd /dev/stderr utils/mkgraph.sh $lang_dir $feats1/$exp_dir/$rec_dir1 $feats1/$exp_dir/$rec_dir1/graph_$graph_dir || exit 1;
fi

[ -f $feats1/$exp_dir/$rec_dir2/final.mdl ] || ( echo "No such file $feats1/$exp_dir/$rec_dir2/final.mdl";  exit 1; )

if [ ! -f $feats1/$exp_dir/$rec_dir2/graph_$graph_dir/HCLG.fst ]; then
  echo "Building decoding graph $rec_dir2"
  $decode_cmd /dev/stderr utils/mkgraph.sh $lang_dir $feats1/$exp_dir/$rec_dir2 $feats1/$exp_dir/$rec_dir2/graph_$graph_dir || exit 1;
fi


# decode first pass
echo `date` Starting decoding $rec_dir1

steps/precomp/decode_fmllr.sh --nj $num_decode --cmd "$decode_cmd" --config $config_file \
  $feats1/$exp_dir/$rec_dir1/graph_$graph_dir $feats1/$decode_set $decode_dir1

echo `date` Finished decoding $rec_dir1

# decode second pass
echo `date` Starting decoding $rec_dir2

steps/precomp/decode_sgmm2.sh --nj $num_decode --cmd "$decode_cmd" --config $config_file --transform-dir $decode_dir1 \
  $feats1/$exp_dir/$rec_dir2/graph_$graph_dir $feats1/$decode_set $decode_dir2

echo `date` Finished decoding $rec_dir2

