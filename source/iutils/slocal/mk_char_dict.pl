#!/usr/bin/perl -W

# koried, 1/15/2013

# Generate a character lexicon based on the BABEL master lexicon.  This script
# does not generate phonesets and alike, but assumes that you used the 
# local/mkdict.pl script before (and based your training on that).

use utf8;
use Encode qw(encode decode);
#use Unicode::Normalize;

# set unicode/utf8
binmode(STDIN, ":encoding(utf8)");
binmode(STDOUT, ":encoding(utf8)");
binmode(STDERR, ":encoding(utf8)");

$has_romanization = 'false';
$phone_map_file = '';

%tonal = map { $_ => 0 } split(/ /,  'i: E: a: 6 u: y: O: 9: iw ej 6w 6j a:j a:w O:j ow u:j 9y');
#@tones = ( 1 .. 6 );

%tonal_if_syllabic = map { $_ => 0 } split(/ /, 'm n N l');
$syllabic_marker = '=';
#@syllabic_tones = ( 4 .. 6 );

$syllable_delimiter = '.';

while (@ARGV gt 0 and $ARGV[0] =~ m/^--/) {
	$param = shift @ARGV;

	if ($param eq "--has-romanization") {
		$has_romanization = shift @ARGV;
	} elsif ($param eq "--map-phones") {
		$phone_map_file = shift @ARGV;
	} else {
		die "unknown parameter $param\n";
	}
}

%phone_map = ();
if ($phone_map_file ne '') {
	open (FI, "<$phone_map_file");
	while (<FI>) {
		chomp;
		my @l = split /\s+/;
		for $i (1 .. $#l) {
			$phone_map{$l[$i]} = $l[0];
		}
	}
	close (FI);
}

if (@ARGV ne 3) {
	print STDERR "
Extract a character dictionary from the BABEL-style master-lexicon;  assumes
you ran local/mkdict.pl before to generate the phonesets and alike (and based
your training on that).
usage: $0 babel-lexicon char-lex word-to-char-lex
options:
  --has-romanization (true|false)
    Specifies if the BABEL-style lexicon has the romanization column;  default:  true
  --map-phones (mapfile)
    A set of phones to the same identifier
";
	exit 1;
}

# empiric list of tonal symbols, will be used to generate phoneset
%symbols = ();

($flexin, $flexout, $fcout) = @ARGV;

# read in lexicon
open (FI, "<:encoding(UTF-8)", $flexin) or die "Could not read lexicon file $flexin\n";
open (FO, ">:encoding(UTF-8)", $flexout) or die "Could not write lexicon file $flexout\n";
open (FC, ">:encoding(UTF-8)", $fcout) or die "Could not write word to character lexicon $fcout\n";

# character lexicon (incrementally built up;  multiple pronunciations possible)
%cdict = ();

while (<FI>) {
	chomp;

	# line format is word\t[romanization\t]pron1[\tpron2 ...]
	@ln = split /\t/;

  # split the word into its characters, if applicable
	$w = shift @ln;

  @chars = ();
  if ($w =~ m/^[<a-zA-Z_\->]*$/) {
    # "regular" word, work out the lexicon the old way
  } elsif ($w =~ m/[a-zA-Z]/) {
    # we will ignore the mixed words (no reliable character-syllable matching)
    next;
  } else {
    # these are "pure cantonese" words, where we will map characters and syllables
    @chars = split(//, $w);
  }

	# get rid of romanization, if present
	shift @ln if ($has_romanization eq 'true');

  # process the different pronunciations
	for $pron (@ln) {
		@sl = split(/ /, $pron);

		# get syllables;  syll will contain the actual syllable including the tonalized symbol
    @syll = ();
		while (@sl > 0) {
			@syl = ();
			$ton = 0;
			while (@sl > 0 and $sl[0] ne $syllable_delimiter) {
				$s = shift @sl;

				# see if it's the tonality information
				if ($s =~ m/^_(\d)/) {
					$ton = $1;
				} else {
					push (@syl, $s);
				}
			}

			# this should not happen...
			if ($ton eq 0) {
				print STDERR "Warning: $w syllable ".join(" ", @syl)." has no tonality?\n";
			}

			# see if that is a syllabic nasal/liquid;  otherwise, go through the syllable
			# and attach the tonality indicator to the vowel
			if (@syl eq 1 and defined $tonal_if_syllabic{$syl[0]}) {
				$syl[0] .= $syllabic_marker."_".$ton;
			} else {
				for $i ( 0 .. $#syl ) {
					if (defined $tonal{$syl[$i]}) {
						$syl[$i] .= "_$ton";
					}
				}
			}

			# keep track of the actually seen symbols, substitute if required
			for $i ( 0 .. $#syl ) {
				$s = $syl[$i];
				
				if (defined $phone_map{$s}) {
					$syl[$i] = $phone_map{$s};
					$s = $syl[$i];
				}

				$symbols{$s}  = 0 unless defined $symbols{$s};
				$symbols{$s} += 1;
			}

			# push this syllable
      push @syll, join(' ', @syl);
			shift @sl unless @sl == 0;
		}

    if (@chars == 0) {
      # single line per pronunciation for regular words;  the word to syllable dict is word->word
		  print FO "$w ".join(' ', @syll)."\n";
      print FC "$w $w\n";
    } else {
      # for the char based lex, we just keep track of the alt-prons
      if (@chars != @syll) {
        print STDERR "ambiguous character to syllable matching (#char = ".(@chars)." #syl = ".(@syll).");  ";
        print STDERR "skipping line: $w $pron\n";
        next;
      }
      
      for ($j = 0; $j < @chars; $j++) {
        $cdict{$chars[$j]} = () unless defined $cdict{$chars[$j]};
        $cdict{$chars[$j]}{$syll[$j]} = 1;
      }

      print FC join("", @chars)." ".join(" ", @chars)."\n";
    }
	}
}
close (FI);
close (FC);

for $c (sort keys %cdict) {
  for $p (keys %{$cdict{$c}}) {
    print FO "$c $p\n";
  }
}

close (FO);

