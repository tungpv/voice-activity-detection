#!/bin/bash

# koried, 2/11/2013

# Use srilm to train a regular n-gram model with the given options in the 
# config file.

if [ $# != 5 ]; then
  echo "Usage: $0 config n-gram-order words.txt text.txt arpa.out.gz"
  exit 1;
fi

[ -f $1 ] || ( echo "Could not find config file $1";  exit 1; )

. $1

order=$2
words=$3
text=$4
arpa=$5

for f in "$words" "$text"; do
  [ ! -f $f ] && echo "$0: No such file $f" && exit 1;
done

# get the vocabulary (just to make sure we have all the words in it!)
grep -v '^!SIL' $words | awk '{print $1}' > data/local/wlist.tmp

# assume that you have SRILM in your path from a proper installation
LC_ALL=$lm_locale ngram-count -text $text -order $order \
  $lm_args -lm - | gzip -c - > $arpa

rm data/local/wlist.tmp

# print out the first lines of the LM
echo "First lines of ARPA LM"
gunzip -c $arpa | head -n25
