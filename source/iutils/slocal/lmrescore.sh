#!/bin/bash

# koried, 6/26/2013

# Apply a language model to aligned word lattices

cmd=utils/run.pl
latbase=lat
beam=13.0
max_mem=500000000

[ -f ./path.sh ] && . ./path.sh
. parse_options.sh || exit 1;

if [ $# -ne 4 ]; then
  echo "Usage:  $0 [--cmd (run.pl|queue.pl|...)] <lang-dir> <G.fst> <decode-dir> <latbase-out>"
  echo "  e.g.  $0 data/syll_to_words data/syll_to_words/G.fst srs_bn/exp/sgmm5a/decode_dev_syll_words lat_G"
  echo "Options:"
  echo "  --latbase <lattice-basename>   # default: lat (resulting in lat.1.gz)"
  exit 1;
fi

lang=$1
fst=$2
dir=$3
out=$4

model=$dir/../final.mdl

for f in $lang/word_align.int $dir/num_jobs $dir/$latbase.1.gz $fst $model; do
  [ ! -f $f ] && echo "$0: missing file $f" && exit 1;
done

nj=`cat $dir/num_jobs`

#  lattice-determinize ark:- ark:- \| \
#  lattice-prune --beam=$beam ark:- ark:- \| \
#  lattice-push ark:- ark:- \| \
#  lattice-align-words-lexicon --max-expand=10 --output-if-empty=true $lang/word_align.int $model ark:- ark:- \| \
#  lattice-determinize-pruned --beam=$beam --max-mem=$max_mem ark:- ark:- \| \
# (determinized lattices) compose(Ldet.fst), determinize
$cmd JOB=1:$nj $dir/log/lmrescore_$out.JOB.log \
  gunzip -c $dir/${latbase}.JOB.gz \| \
  lattice-compose ark:- $fst ark:- \| \
  lattice-scale --lm-scale=0.5 ark:- ark:- \| \
  gzip -c - \> $dir/${out}.JOB.gz || exit 1;

