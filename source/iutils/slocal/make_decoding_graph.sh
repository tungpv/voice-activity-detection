#!/bin/bash 

# koried, 2/11/2013

# Starting from a data/lang directory, make a lang_test directory based on the given arpa LM

position_dependent_phones=true
sil_prob=0.5

. utils/parse_options.sh


if [ $# != 4 ]; then 
  echo "Usage: $0 [options] lexicon arpa.gz data/lang data/lang_test"
  exit 1;
fi

if [ -f path.sh ]; then . path.sh; fi


silprob=0.5

lexicon=$1
arpa_lm=$2
lang=$3
lang_test=$4

[ -d $lang ] || ( echo "No such directory $lang";  exit 1; ) 
[ -f $arpa_lm ] || ( echo "No such file $arpa_lm";  exit 1; )
[ -f $lexicon ] || ( echo "No such file $lexicon";  exit 1; )

if [ -d $lang_test ]; then
  echo "Directory $lang_test already exists."
  exit 1;
fi

cp -r $lang $lang_test

# make the lexicon transducer
echo "Preparing lexicon"
if $position_dependent_phones; then
  # create a version of the lexicon with markers
  perl -ane '@A=split(" ",$_); $w = shift @A; @A>0||die;
    if(@A==1) { print "$w $A[0]_S\n"; } else { print "$w $A[0]_B ";
    for($n=1;$n<@A-1;$n++) { print "$A[$n]_I "; } print "$A[$n]_E\n"; } ' \
    < $lexicon > $lang_test/lexicon.txt || exit 1;
else
  cp $lexicon $lang_test/lexicon.txt
fi

# update disambig files
echo "Updating disambig symbols"
ndisambig=`utils/add_lex_disambig.pl $lang_test/lexicon.txt $lang_test/lexicon_disambig.txt`
ndisambig=$[$ndisambig+1]; # add one disambig symbol for silence in lexicon FST.
echo $ndisambig > $lang_test/lex_ndisambig

( for n in `seq 0 $ndisambig`; do echo '#'$n; done ) >$lang_test/phones/disambig.txt

echo "<eps>" | cat - $lang_test/phones/{silence,nonsilence,disambig}.txt | \
  awk '{n=NR-1; print $1, n;}' > $lang_test/phones.txt

cat $lang_test/lexicon.txt | awk '{print $1}' | sort | uniq  | \
 awk 'BEGIN{print "<eps> 0";} {printf("%s %d\n", $1, NR);} END{printf("#0 %d\n", NR+1);} ' \
   > $lang_test/words.txt || exit 1;

utils/sym2int.pl $lang_test/phones.txt < $lang_test/phones/disambig.txt > $lang_test/phones/disambig.int
utils/sym2int.pl $lang_test/phones.txt < $lang_test/phones/disambig.txt | \
  awk '{printf(":%d", $1);} END{printf "\n"}' | sed s/:// > $lang_test/phones/disambig.csl || exit 1;

phone_disambig_symbol=`grep \#0 $lang_test/phones.txt | awk '{print $2}'`
word_disambig_symbol=`grep \#0 $lang_test/words.txt | awk '{print $2}'`
   
silphone=`cat $lang/phones/optional_silence.txt` || exit 1;
[ -z "$silphone" ] && \
  ( echo "You have no optional-silence phone; it is required in the current scripts"
    echo "but you may use the option --sil-prob 0.0 to stop it being used." ) && \
    exit 1;

echo "Making lexicon transducer L.fst"
utils/make_lexicon_fst.pl $lang_test/lexicon_disambig.txt $sil_prob $silphone '#'$ndisambig | \
  fstcompile --isymbols=$lang_test/phones.txt --osymbols=$lang_test/words.txt \
    --keep_isymbols=false --keep_osymbols=false |   \
  fstaddselfloops  "echo $phone_disambig_symbol |" "echo $word_disambig_symbol |" | \
  fstarcsort --sort_type=olabel > $lang_test/L_disambig.fst || exit 1; 

echo "Making grammar transducer G.fst"
# grep -v '<s> <s>' etc. is only for future-proofing this script.  Our
# LM doesn't have these "invalid combinations".  These can cause 
# determinization failures of CLG [ends up being epsilon cycles].
# Note: remove_oovs.pl takes a list of words in the LM that aren't in
# our word list.  Since our LM doesn't have any, we just give it
# /dev/null [we leave it in the script to show how you'd do it].
echo LANG=$LANG LC_ALL=$LC_ALL
gunzip -c "$arpa_lm" | \
   grep -v '<s> <s>' | \
   grep -v '</s> <s>' | \
   grep -v '</s> </s>' | \
   arpa2fst - | fstprint | \
   utils/remove_oovs.pl /dev/null | \
   utils/eps2disambig.pl | utils/s2eps.pl | fstcompile --isymbols=$lang_test/words.txt \
     --osymbols=$lang_test/words.txt  --keep_isymbols=false --keep_osymbols=false | \
     fstrmepsilon > $lang_test/G.fst || exit 1;


echo  "Checking how stochastic G is (the first of these numbers should be small):"
# Checking that G.fst is determinizable.
fstdeterminize $lang_test/G.fst /dev/null || echo Error determinizing G.

# Checking that L_disambig.fst is determinizable.
fstdeterminize $lang_test/L_disambig.fst /dev/null || echo Error determinizing L_disambig.

# Checking that disambiguated lexicon times G is determinizable
# Note: we do this with fstdeterminizestar not fstdeterminize, as
# fstdeterminize was taking forever (presumbaly relates to a bug
# in this version of OpenFst that makes determinization slow for
# some case).
fsttablecompose $lang_test/L_disambig.fst $lang_test/G.fst | \
  fstdeterminizestar > /dev/null || echo LG not stochastic

