#!/usr/bin/perl -w
# Copyright 2012 Korbinian Riedhammer

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.


# This script takes a list of utterance-ids or any file whose first field
# of each line is an utterance-id, and filters an scp
# file (or any file whose first field is an utterance id), printing
# out only those lines whose first field is in id_list.

$feattolen = '/u/drspeech/opt/kaldi/kaldi-trunk/src/featbin/feat-to-len';
$minlength = 3;

(scalar @ARGV == 3) or die "Usage: $0 lexicon num-utts data-dir > turn-list";

my ($flex, $numutts, $dir) = @ARGV;

die "invalid data directory\n" unless (-e "$dir/feats.scp" and -e "$dir/text");

# read in lexicon
my %lex = ();
open (FIN, "<$flex") or die "Could not open lexicon: $flex\n";
while (<FIN>) {
  my ($w, @t) = split /\s+/;
  $lex{$w} = join(" ", @t) unless defined $lex{$w};

  # print STDERR "$w => ".join(" ", @t)."\n";
}
close (FIN);

# print STDERR "read ".(scalar %lex)." lexicon entries (no alt-prons).\n";

# read in transcriptions, expand to symbols, build up symbol table
my %trl = ();
my %len = ();
my %sym = (); my $nsym = 0;
open (FIN, "<$dir/text") or die "Could not read transcription: $dir/text\n";
while (<FIN>) {
  my ($id, @t) = split /\s+/;
  
  $len{$id} = scalar @t;
  
  my $e = $lex{shift @t};
  for my $w (@t) {
    print STDERR "Missing transcription for $w\n" unless defined $lex{$t[0]};
    $e .= " ".$lex{shift @t};
  }
  $trl{$id} = $e;

  # build up phone table
  for my $p (split (/\s+/, $e)) {
    $sym{$p} = $nsym++ unless defined $sym{$p};
  }
  
  # print STDERR "$id => $e\n";
}
close (FIN);

# print STDERR "read ".(scalar keys %trl)." transcriptions.\n";
# print STDERR "symtab = ".(scalar keys %sym)." - ".join(" ", sort values %sym)."\n";

# read in the sorted list of utt-ids
my @utts = ();
my %uttl = ();
open (FIN, "-|", "$feattolen scp:$dir/feats.scp ark,t:- | sort -n -k2 | cut -d ' ' -f 1") or die "Could not read sorted-id list from $dir/feats.scp\n";
while (<FIN>) {
  chomp;
  $id = $_;
  push (@utts, $id);

  # prepare the occ vector
  @ov = ();
  for ($i = 0; $i < $nsym; $i++) { push (@ov, 0); }
  @tl = split /\s+/, $trl{$id};
  $uttl{$id} = scalar @tl;
  for my $s (@tl) {
    $ov[$sym{$s}] += 1;
  }
  
  # print STDERR "$id=".join(",", @ov)."\n";
  
  $trl{$id} = [ @ov ];
}

close (FIN);

sub fillszero {
  ($n, $p1, $p2) = @_;

  $incs = 0;
  $init = 0;
  for ($i = 0; $i < $n; $i++) {
    $init++ if (@{$p1}[$i] == 0 && @{$p2}[$i] > 0);
    $incs++ if (@{$p1}[$i] > 0 && @{$p2}[$i] > 0);
  }

  return ($init, $incs);
}

# init occurrence array
my @occ = ();
for ($i = 0; $i < $nsym; $i++) { push (@occ, 0); }

my %sel = ();
my $change = 1;

print STDERR "Selecting utterances to cover all phones\n";
while (scalar keys %sel < $numutts) {
  
  # print STDERR "cur obj ".stdev($nsym, @occ)."\n";

  $ind = "";

  # first, we look for turns that will will add an unseen phone only do so 
  # if there is actually an empty slot
  $zc = 0;
  for $c (@occ) {
    $zc += 1 if $c == 0;
  }

  # if there are no empty slots, finish here and fill up with shortest utts
  last if ($zc == 0);

  # find the first utterance that fills at least one empty slot
  for $c (@utts) {
    next if defined $sel{$c};

    # see if that turn fills more zeros than increments others
    @stat = fillszero($nsym, \@occ, \@{$trl{$c}});
    if ($stat[0] > 0 || $stat[0] - $stat[1] > 0) {
      $ind = $c;
      last;
    }
  }

  # see if we could find a good utterance
  last if ($ind eq "");

  # select utterance, update phone counts
  $sel{$ind} = 1;
  for ($i = 0; $i < $nsym; $i++) { $occ[$i] += @{$trl{$ind}}[$i]; }
 
}

# fill up with shortest utterances, but require a min-length, so we get
# somewhat meaningful utterances

print STDERR "Filling up with utts longer than $minlength\n";
for $c (@utts) {
  unless (defined $sel{$c}) {
    $sel{$c} = 1 if $len{$c} >= $minlength;
  }

  last if (scalar keys %sel == $numutts);
}


$minl = 1000;
$maxl = 0;
$avgl = 0;
for $k (sort keys %sel) {
  $ul = $uttl{$k};
  $minl = $ul if $minl > $ul;
  $maxl = $ul if $maxl < $ul;
  $avgl += $ul;
}

$avgl /= (scalar keys %sel);

printf STDERR "min/avg/max length phones: %d/%d/%d\n", $minl, int($avgl), $maxl;

$minl = 1000;
$maxl = 0;
$avgl = 0;
for $k (sort keys %sel) {
  $ul = $len{$k};
  $minl = $ul if $minl > $ul;
  $maxl = $ul if $maxl < $ul;
  $avgl += $ul;
}

$avgl /= (scalar keys %sel);

printf STDERR "min/avg/max length characters: %d/%d/%d\n", $minl, int($avgl), $maxl;

for $k (sort keys %sel) {
  print "$k\n";
}

for $s (sort keys %sym) {
  printf STDERR "%7s %d\n", $s, $occ[$sym{$s}];
}
