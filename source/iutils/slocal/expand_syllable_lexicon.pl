#!/usr/bin/perl -w

# koried, 6/25/2013

# expand a syllable-to-word lexicon to a phonetic lexicon based on the 
# underlying syllable lexicon

if (@ARGV != 2) {
  print STDERR "usage:  $0 syll-to-phone word-to-syll > word-to-phone\n";
  exit 1;
}

($fs2p, $fw2s) = @ARGV;

open (FI, "<:encoding(utf8)", $fs2p) or die "could not read $fs2p\n";
while (<FI>) {
  @A = split;
  $s = shift @A;
  $s2p{$s} = join(" ", @A);
}
close (FI);

binmode(STDOUT, "encoding(utf8)");

open (FI, "<:encoding(utf8)", $fw2s) or die "could not read $fw2s\n";
while (<FI>) {
  @A = split;
  $w = shift @A;
  print "$w";
  foreach $s (@A) {
    defined $s2p{$s} or die "Missing syllable $s\n";
    print " ".$s2p{$s};
  }
  print "\n";
}

