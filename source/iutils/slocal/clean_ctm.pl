#!/usr/bin/perl -W

# koried, 2/20/2013

# Clean an input CTM by removing any optional words (in <> or []).

$lf = "";
$na = 0;
$ns = 0;
while (<>) {
  chomp;
  @A = split /\s+/;

  $cf = $A[0];
  $w  = $A[4];

  if ($lf eq "") { $lf = $cf; }

  if ($lf ne $cf) {
    # if we skipped all words of one file, we need to output a dummy line in
    # order to not crash sclite...
    if ($na == $ns) {
      printf "%s 1 0.01 0.10 @\n", $lf;
    }

    $lf = $cf;
    $na = 0;
    $ns = 0;
  }

  $na += 1;

  if ($w =~ m/^\[.+\]$/ || $w =~ m/^<.+>$/) {
    $ns += 1;
    next;
  } 

  print join(" ", @A)."\n";
}

