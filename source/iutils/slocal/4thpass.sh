#!/bin/bash

# BABEL decoding script
# koried, 3/25/2013

function error_exit { echo ${1:-"unknown error"};  exit 1; }

. cmd.sh

# required variables, defaults

if [ $# -lt 3 ]; then
  echo "usage: $0 features1 features2"
  echo " e.g. $0 mfcc bn decode_dir"
  exit 1;
fi

feats1=$1
feats2=$2
decode_dir=$3

# document the configuration 
. $decode_dir/config

# a few tests
for i in $decode_dir/config $config_file1 $config_file2 $lang_dir/{G,L}.fst; do
  [ -f $i ] || error_exit "$0: no such file $i"
done

echo $SLURM_ID > $decode_dir/jobid_4thpath

# build graphs, if necessary
graph_dir=`basename $lang_dir`

identifier=`basename $decode_dir`

echo `date` Generating new speaker-dep. lattices
steps/tandem/decode_sgmm2_extra.sh --latbase denlat1513 --cmd "$decode_cmd" --config $config_file2 --transform-dir $decode_dir/../../tri4a/$identifier \
  $feats2/$exp_dir/$rec_dir2/graph_$graph_dir {$feats1,$feats2}/$data $decode_dir || exit 1;

# generate SLF lattices for second pass
echo `date` Generating SLF lattices
steps/tandem/mk_aslf_sgmm2.sh --latbase denlat1513 --nj $num_decode --transform-dir $decode_dir/../../tri4a/$identifier --cmd "$decode_cmd" $feats2/$exp_dir/$rec_dir2/graph_$graph_dir {$feats1,$feats2}/$data $decode_dir $decode_dir/slf_denlat1513 || exit 1;

echo `date` Done.

