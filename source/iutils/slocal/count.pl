#!/usr/bin/perl

($val, $inv) = @ARGV;

open (FI, "<:encoding(utf8)", "$val") or die "could not open $val\n";
while (<FI>) {
  chomp;
  $valid{$_} = 1;
}
close (FI);

open (FI, "<:encoding(utf8)", "$inv") or die "could not open $inv\n";
while (<FI>) {
  chomp;
  $invalid{$_} = 1;
}
close (FI);

shift @ARGV;
shift @ARGV;

binmode (STDIN, ":encoding(utf8)");
$n = 0;
$v = 0;
$i = 0;
while (<>) {
  chomp;
  @A = split /\s+/;
  for $w (@A) {
    next if $w =~ m/<unk>/;
    next if $w =~ m/\[/;
    $n += 1;
    
    $v += 1 if defined $valid{$w};
    $i += 1 if defined $invalid{$w};
  }
}

print "$n $v $i\n";
