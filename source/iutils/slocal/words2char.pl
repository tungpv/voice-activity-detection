#!/usr/bin/perl -W

use utf8;
use Encode qw(encode decode);

# we cant do this here, we will get in trouble with the wide characters
#binmode(STDIN,  ":encoding(utf8)");
#binmode(STDOUT, ":encoding(utf8)");

while (<>) {
  chomp;
  @l = split /\s+/;

  for ($i = 0; $i < @l; $i++) {
    $w = $l[$i];

    # nothing fancy for regular ascii
    next if ($w =~ m/^[\[<a-zA-Z\_\->\]]*$/);

    # now any mixed or cant:  split and splice (a b c_d e) -> (a b c d e)
    if ($w =~ m/_/) {
      @a = split(/_/, $w);
      splice @l, $i, 1, @a;
    }
  }
  
  # blow up the mixed ascii/cantonese words
  for ($i = 0; $i < @l; $i++) {
    $w = $l[$i];

    if ($w =~ m/^[\[<a-zA-Z\_\->\]]*$/) {
      print "$w";
    } else {
      $cw = decode('UTF-8', $w);
      @cc = split //, $cw;
      for ($j = 0; $j < @cc; $j++) { $cc[$j] = encode('UTF-8', $cc[$j]); };
      print join(" ", @cc);
    }
    
    print " " unless $i == $#l;
  }
  print "\n";
}
