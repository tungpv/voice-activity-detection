#!/bin/bash

# BABEL_BP_101 recipe
# koried, 11/18/2012

. cmd.sh

decode_set=data/dev
num_decode=20
exp_dir=exp
lang_dir=data/lang_test
config_file=conf/decode.config

. utils/parse_options.sh || exit 1;

if [ $# -lt 4 ]; then
  echo "usage: $0 features1 features2 trans-dir rec-dir"
  echo " e.g. $0 --decode-set data/dev1 --lang_dir data/syll_lang --exp_dir exp_llp --num-decode 10 mfcc tri4a"
  exit 1;
fi

feats1=$1
feats2=$2
trans_dir=$3
rec_dir=$4

graph_dir=`basename $lang_dir`
data_dir=`basename $decode_set`
conf_name=`basename $config_file`

[ -f $feats2/$exp_dir/$rec_dir/final.mdl ] || ( echo "No such file $feats2/$exp_dir/$rec_dir/final.mdl";  exit 1; )

if [ ! -f $feats2/$exp_dir/$rec_dir/graph_$graph_dir/HCLG.fst ]; then
  echo "Building decoding graph..."
  $decode_cmd /dev/stderr utils/mkgraph.sh $lang_dir $feats2/$exp_dir/$rec_dir $feats2/$exp_dir/$rec_dir/graph_$graph_dir || exit 1;
fi

echo `date` Starting decoding

steps/tandem/decode_sgmm2.sh --nj $num_decode --cmd "$decode_cmd" --config $config_file --transform-dir $trans_dir \
  $feats2/$exp_dir/$rec_dir/graph_$graph_dir {$feats1,$feats2}/$decode_set $feats2/$exp_dir/$rec_dir/decode_${data_dir}_${graph_dir}_$conf_name

echo `date` Finished decoding
