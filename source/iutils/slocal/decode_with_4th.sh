#!/bin/bash

# BABEL decoding script
# koried, 3/25/2013

function error_exit { echo ${1:-"unknown error"};  exit 1; }

. cmd.sh

# required variables, defaults
exp_dir=exp
lang_dir=data/lang_test
data=
rec_dir1=tri4a
rec_dir2=sgmm5a
config_file1=conf/decode.config.tri4a
config_file2=conf/decode.config.sgmm5a
config_file3=conf/decode.config.4th
num_decode=20

. utils/parse_options.sh || exit 1;

if [ $# -lt 2 ]; then
  echo "usage: $0 features1 features2"
  echo " e.g. $0 --data data/dev1 --lang_dir data/syll_lang --exp_dir exp_llp --num-decode 10 mfcc bn"
  exit 1;
fi

feats1=$1
feats2=$2

if [ -z "$SLURM_NPROCS" ]; then
  if [ $SLURM_NPROCS != $num_decode ]; then
    echo "WARNING resetting num_train from $num_train to $SLURM_NPROCS to match slurm allocation"
    num_decode=$SLURM_NPROCS
  fi
fi

# a few tests
for i in {$feats1,$feats2}/$data/feats.scp $config_file1 $config_file2 $lang_dir/{G,L}.fst; do
  [ -f $i ] || error_exit "$0: no such file $i"
done

# document the configuration 
cat > $$.config << EOF
# decoding run startetd: `date --rfc-3339=ns`
exp_dir=$exp_dir
lang_dir=$lang_dir
data=$data
rec_dir1=$rec_dir1
rec_dir2=$rec_dir2
config_file1=$config_file1
config_file2=$config_file2
config_file3=$config_file3
num_decode=$num_decode
EOF


# we use a unique identifier for each decoding run
identifier=`md5sum $$.config | awk '{print $1}'`

[ -n "$identifier" ] || error_exit "Could not generate MD5 identifier;  exitting."

# set the output directories
decode_dir1=$feats2/$exp_dir/$rec_dir1/decode_$identifier
decode_dir2=$feats2/$exp_dir/$rec_dir2/decode_$identifier

# with this script, we do not want to overwrite decoding runs!
[ -e $decode_dir1 ] && error_exit "Decoding directory $decode_dir1 already exists;  if you're resuming a decoding run, do it manually!"
[ -e $decode_dir2 ] && error_exit "Decoding directory $decode_dir2 already exists;  if you're resuming a decoding run, do it manually!"

mkdir -p $decode_dir1 $decode_dir2
cp $$.config $decode_dir1/config
cp $$.config $decode_dir2/config

echo $SLURM_JOB_ID > $decode_dir1/jobid
echo $SLURM_JOB_ID > $decode_dir2/jobid

rm $$.config

# build graphs, if necessary
graph_dir=`basename $lang_dir`

# verify models, make graphs
[ -f $feats2/$exp_dir/$rec_dir1/final.mdl ] || error_exit "No such file $feats2/$exp_dir/$rec_dir1/final.mdl"

if [ ! -f $feats2/$exp_dir/$rec_dir1/graph_$graph_dir/HCLG.fst ]; then
  echo "Building decoding graph $rec_dir1"
  $decode_cmd /dev/stderr utils/mkgraph.sh $lang_dir $feats2/$exp_dir/$rec_dir1 $feats2/$exp_dir/$rec_dir1/graph_$graph_dir || exit 1;
fi


# decode first pass
echo `date` Starting decoding $rec_dir1

touch $decode_dir1/.begin
steps/tandem/decode_fmllr.sh --nj $num_decode --cmd "$decode_cmd" --config $config_file1 \
  $feats2/$exp_dir/$rec_dir1/graph_$graph_dir {$feats1,$feats2}/$data $decode_dir1 || exit 1;
touch $decode_dir2/.end

echo `date` Finished decoding $rec_dir1

# decode second pass
[ -f $feats2/$exp_dir/$rec_dir2/final.mdl ] || ( echo "No such file $feats2/$exp_dir/$rec_dir2/final.mdl";  exit 1; )

if [ ! -f $feats2/$exp_dir/$rec_dir2/graph_$graph_dir/HCLG.fst ]; then
  echo "Building decoding graph..."
  $decode_cmd /dev/stderr utils/mkgraph.sh $lang_dir $feats2/$exp_dir/$rec_dir2 $feats2/$exp_dir/$rec_dir2/graph_$graph_dir || exit 1;
fi

echo `date` Starting decoding $rec_dir2

touch $decode_dir2/.begin
steps/tandem/decode_sgmm2.sh --nj $num_decode --cmd "$decode_cmd" --config $config_file2 --transform-dir $decode_dir1 \
  $feats2/$exp_dir/$rec_dir2/graph_$graph_dir {$feats1,$feats2}/$data $decode_dir2 || exit 1;
touch $decode_dir2/.end

echo `date` Finished decoding $rec_dir2

# generate SLF lattices for second pass
echo `date` Generating SLF lattices
steps/tandem/mk_aslf_sgmm2.sh --nj $num_decode --transform-dir $decode_dir1 --cmd "$decode_cmd" $feats2/$exp_dir/$rec_dir2/graph_$graph_dir {$feats1,$feats2}/$data $decode_dir2 $decode_dir2/slf || echo "Warning: SLF conversion failed"

echo `date` Done.

echo `date` Compute 4th pass with spk-vecs and large beam
steps/tandem/decode_sgmm2_extra.sh --cmd "$decode_cmd" --config $config_file3 --transform-dir $decode_dir1 \
  $feats2/$exp_dir/$rec_dir2/graph_$graph_dir {$feats1,$feats2}/$data $decode_dir2 || exit 1;

echo `date` Generating SLF lattices
steps/tandem/mk_aslf_sgmm2.sh --latbase denlat --nj $num_decode --transform-dir $decode_dir/../../tri4a/$identifier --cmd "$decode_cmd" $feats2/$exp_dir/$rec_dir2/graph_$graph_dir {$feats1,$feats2}/$data $decode_dir2 $decode_dir2/slf_denlat || exit 1;

echo `date` Done.
