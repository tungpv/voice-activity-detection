#!/usr/bin/env perl
use strict;
use warnings;

my($lex) = @ARGV;

my %vocab = ();
open L, "<$lex" or die "file $lex cannot open\n";
while (<L>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $vocab{$1} = 0;
}
close L;

print STDERR "stdin expected\n";
my $total = 0; 
my $oovNum = 0;
while (<STDIN>) {
  chomp;
  my @A = split / /; my $oov = 0;
  for(my $i = 0; $i < scalar @A; $i ++) {
    my $w = $A[$i];
    if (not exists $vocab{$w}) {
       $oov = 1; last;  
    }
  }
  if ($oov ==1) {
    $oovNum ++;  print $_, "\n";
  }
  $total ++;
}
my $s =sprintf "%.4f", $oovNum/$total;
print STDERR  "oov_rate = $s\n";
print STDERR "stdin ended\n";

