#!/usr/bin/env perl
use warnings;
use strict;

sub TranProcess {
  my ($fname) = @_;
  open F, "<$fname";
  $fname =~ s/.*\///g; $fname =~ s/\.txt//g;
  my ($start, $end) = (0, 0); my $counter = 0; my $s = "";
  while (<F>) {
    chomp;
    if (m/\[(\S+)\]/) {
      my $time = $1;
      $counter ++;
      if ($counter%2 == 0) {
        $end = $time;     
        print "$fname $start $end $s\n";
        $start = $end; $counter = 1; 
      } else {
        $start = $time;
      }
    } else {
      $s = $_;
    }   
  }
  close F;
}


my ($trans) = @ARGV;

open TRAN, "$trans|" or die "trans file cannot open \n";
while (<TRAN>) {
 chomp;
 TranProcess($_);
}
close TRAN;
