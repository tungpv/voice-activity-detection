#!/usr/bin/env perl


my $nArgs = scalar @ARGV;

$nArgs == 1 or die "\nUsage: $0 <segments> \n";
my ($segfile) = @ARGV;
my %vocab = ();
while (<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(.*)/ or next;
  my ($labname, $start, $end, $trans) = ($1, $4, $5, $6);
  # print "$labname, $start, $end, $trans\n";
  my @ta = ();
  my @trans_array = split / /, $trans;
  my @array = ($start, $end, \@trans_array, \@ta);
  if (exists $vocab{$labname}) {
    my $x = $vocab{$labname};
    push @$x, \@array;
  } else {
    my @x = ();
    $vocab{$labname} = \@x;
    my $refx=$vocab{$labname};
    push @$refx, \@array;
  }
}

sub Overlap {
  my ($seg_start, $seg_end, $stm_start, $stm_end) = @_;
  return 0 if ($seg_end <=$stm_start or $seg_start >=$stm_end);
  return 1;
}
sub OverlapRate {
  my ($seg_start, $seg_end, $stm_start, $stm_end) = @_;
  my $new_start = $seg_start;
  $new_start = $stm_start if $seg_start < $stm_start ;
  my $new_end = $seg_end;
  $new_end = $stm_end if $stm_end < $seg_end;
  return 0 if $new_end <= $new_start or $stm_start == $stm_end;
  return ($new_end -$new_start) / ($stm_end - $stm_start);
}

open SEG, "<$segfile" or die "file $segfile cannot open";
my %vocab2 = ();
my %vocab3 = ();
while (<SEG>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/ or next;
  my ($labname, $segname, $start, $end) = ($1, $2, $3, $4);
  my @k_array = ($start, $end);
  $vocab3{$labname} = \@k_array;
  if (exists $vocab{$segname}) {
    my $x = $vocab{$segname};
    my $found;
    for (my $idx= 0; $idx < scalar @$x; $idx ++) {
      my $e = $$x[$idx];
      my ($stm_start, $stm_end) = ($$e[0], $$e[1]);
      my $f = Overlap($start, $end, $stm_start, $stm_end);
      $found += $f;     
      if ($f != 0) {
        my $ref_p = $$e[3];
        my @new_array =($labname, $start, $end);
        push @$ref_p, \@new_array;
        if (exists $vocab2{$labname}) {
          my $ref_array = $vocab2{$labname};
          push @$ref_array, $e;
        } else {
          my @array = ();
          $vocab2{$labname} = \@array;
          my $ref_array = $vocab2{$labname};
          push @$ref_array, $e;
        }
      }
    }
    if ($found == 0) {
      print STDERR "$_ is a out-of-boundary label\n";
    }
  } else {
    print STDERR "$_ is a out-of-segment label\n"
  }
}

for my $labname (keys %vocab2) {
  my $ref_array = $vocab2{$labname};
  my $overlapped_num = scalar @$ref_array;
  if ($overlapped_num == 1) {
    my $ref_pa = $$ref_array[0];
    my $ref_pb = $$ref_pa[3];
    my $sharing_num = scalar @$ref_pb;
    my $ref_trans = $$ref_pa[2]; my $words = scalar @$ref_trans;
    my $utt_trans = join " ", @$ref_trans;
    if ($sharing_num == 1) {
     # print "$labname  $utt_trans\n";
    } else {  ## stm item shared by n(n>1) segments
      # print "xxxxx\n";
      my $ref_bound = $vocab3{$labname};
      my ($start, $end) = ($$ref_bound[0], $$ref_bound[1]);
      my ($stm_start, $stm_end) = ($$ref_pa[0], $$ref_pa[1]); 
      my $overlap_rate = OverlapRate ($start, $end, $stm_start, $stm_end); 
      my $dur = $end - $start;
      print "overlap_rate=$overlap_rate, $dur,  $words, $utt_trans\n";
    }
  } else {
    # print "yyyy\n";
  }
}

