#!/usr/bin/env perl
use warnings;
use strict;
use Getopt::Long;
use XML::Simple;
use Data::Dumper;

sub LoadXML {
  my ($xml, $thresh, $vocab) = @_;
  my $data = XMLin ($xml);
  foreach my $kent (@{$data->{detected_kwlist}}) {
    if (ref $kent->{kw} eq 'HASH') { # single detection
      if ($kent->{kw}->{decision} eq 'YES'){      # print Dumper ($kent);
        # print $kent->{kwid}, "\n";
        # push @array, $kent;
        $$vocab{$kent->{kwid}} = $kent;
      }
    } else { # array
      my $aRef = $kent->{kw};
      # for icsi empty detection
      next if ! defined @$aRef;
      my @tA = ();
      my $insert = 0;
      for (my $i = 0; $i < @$aRef; $i ++) {
        my $vRef = $$aRef[$i];
        # print "$vRef->{tbeg}\n"; exit 0;
        if ($vRef->{score} >= $thresh &&
          $vRef->{decision} eq 'YES') {
          $insert ++;
          push @tA, $vRef;  
        }
      }
      if ($insert > 0) {
        $kent->{kw} = \@tA;
        # push @array, $kent;
        $$vocab{$kent->{kwid}} = $kent;
      }
    }
  }
  return $data;
}

# main entrance point 
my $numArgs = scalar @ARGV;
if ($numArgs != 4) {
  die "\n\nExample: $0 <mxml> <axml> <duptime> <prune> > merge.xml\n\n";
}
my ($mxml, $axml, $gaptime, $thresh) = @ARGV;

# my $mdata = XMLin ($xml);
# print Dumper($data);
my %vocab = ();
my $adata= &LoadXML ($axml, $thresh, \%vocab);
# $data = &LoadXML ($xml, 0, \%mVocab);

# now merge
# if ($mxml eq '-') {
#   print STDERR "standard input\n";
my   $data = XMLin($mxml);
# } else {
#   print STDERR "normal input\n";
#   $data = XMLin($mxml);
# }

# my @tArray = ();
# foreach my $kwid (keys %vocab) {
#   my $kent = $vocab{$kwid};
#   push @tArray, $kent;
# }
# $data->{detected_kwlist} = \@tArray;
# my $xml2 = XMLout($data, RootName => "kwslist", NoSort=>1);
# print $xml2;
# exit 0;
#
# insert the main set to the auxiliary set to merge
my $nCount = 0;
foreach my $kent (@{$data->{detected_kwlist}}) {
  $nCount ++;
  my $kwid = $kent->{kwid};
  if (ref($kent->{kw}) eq 'HASH') { # single detection
    if (exists $vocab{$kwid}) { # exist
      my $xkent = $vocab{$kwid}; # target
      my $hRef = $kent->{kw};
      if (ref ($xkent->{kw}) eq 'HASH') { # single
        my $xhRef = $xkent->{kw};
        # to determine if insertion is necessary
        if (abs ($hRef->{tbeg} - $xhRef->{tbeg}) < $gaptime &&
            $hRef->{channel} == $xhRef->{channel} &&
            $hRef->{file} eq $xhRef->{file} ) { # no insert
        } else {
          my @array = ();
          push @array, $hRef;
          push @array, $xhRef;
          $xkent->{kw} = \@array;
        }
      } else { # array
        my $xaRef = $xkent->{kw};
        my $found = 0;
        foreach my $xkw (@$xaRef) {
          if ( abs($xkw->{tbeg} - $hRef->{tbeg}) < $gaptime &&
               $hRef->{channel} == $xkw->{channel} &&
               $hRef->{file} eq $xkw->{file} ) {
            $found = 1; last;
          }
        }
        if ($found == 0) {
          push @$xaRef, $hRef;
        }
      }
    } else {  # direct insert
      $vocab{$kwid} = $kent;
    }
  } else { # array
    if (exists $vocab{$kwid}) {
       my $xkent = $vocab{$kwid};
       my $aRef = $kent->{kw};
       if (ref ($xkent->{kw} ) eq 'HASH' ) { # single
         my $xkw = $xkent->{kw};
         my $found  = 0;
         my @array = ();
         foreach my $kw (@$aRef) {
           if ( abs ($kw->{tbeg} - $xkw->{tbeg}) < $gaptime &&
                $kw->{channel} == $xkw->{channel} &&
                $kw->{file} eq $xkw->{file} ) {
              $found = 1;
           }
           push @array, $kw;
         }
         if ($found == 0) {
           push @array, $xkw;
         }
         $xkent->{kw} = \@array;  
       } else {  # array
         # insert if necessary
         my $xaRef = $xkent->{kw};
         my @array = @$xaRef;
         foreach my $kw (@$aRef) {
           my $found = 0;
           foreach my $xkw (@$xaRef) {
             if ( abs ($xkw->{tbeg} - $kw->{tbeg} < $gaptime) &&
                  $xkw->{channel} == $kw->{channel} &&
                  $xkw->{file} eq $kw->{file} ) {
              $found = 1;  last;
            }
          }
          if ($found == 0) {
            push @array, $kw;
          }
        }
        $xkent->{kw} = \@array; 
      }
    } else {  # direct insert
      $vocab{$kwid} = $kent;
    }
  } # end of array case 
}
# print "entry_num = $nCount\n";
# prepare to dump
my @array = ();
foreach my $kwid (keys %vocab) {
  # print "kwdi=$kwid\n";
  my $xkent = $vocab{$kwid};
  push @array, $xkent;
}
$data->{detected_kwlist} = \@array;

my $output_xml = XMLout($data, RootName => "kwslist", NoSort=>1);
print $output_xml;
