#!/usr/bin/env perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 2) {
  die "\n\nExample: cat word_keywords.txt | $0
        lexicon.txt phone.txt > phone_keywords.int\n\n";
}
my ($wlex, $phones) = @ARGV;

sub LoadWords {
  my ($fName, $vocab, $reverse) = @_;
  open F, "<$fName" or die "file $fName cannot open";
  while (<F>) {
    chomp;
    m/(\S+)\s+(\S+)/;
    $$vocab{$2} = $1 if $reverse == 1;
    $$vocab{$1} = $2 if $reverse != 1;
  }
  close F;
}

sub LoadLexicon {
  my ($fName, $vocab) = @_;
  open F, "$fName" or die "file $fName cannot open";
  while (<F>) {
    chomp;
    s/ +/ /g;
    my @a = split /\s+/;
    my $word = shift @a;
    if ( not exists $$vocab{$word}) {
      my @array = ();
      $$vocab{$word} = \@array;
      my $pArray = $$vocab{$word};
      push @$pArray, \@a;
    } else {
      my $pArray = $$vocab{$word};
      push @$pArray, \@a;
    }
  }
  close F;
}

sub PronConversion {
  my ($pArray, $oArray, $vocab) = @_;
  for(my $idx = 0; $idx < scalar @$pArray; $idx ++) {
    my $phone = $$pArray[$idx];
    if (not exists $$vocab{$phone}) {
      die "phone $phone is not seen\n";
    }
    my $phoneId = $$vocab{$phone};
    push @$oArray, $phoneId;
  }
}

sub OutputKW {
  my ($kwId, $oArray) = @_;
  my $size = scalar @$oArray; 
  if($size <1) {
    print STDERR "$kwId\n"; return;
  }
  print "$kwId"; 
  for(my $idx = 0 ; $idx < scalar @$oArray; $idx ++) {
    my $pArray = $$oArray[$idx];
    for (my $j = 0; $j < scalar @$pArray; $j ++ ) {
      print " $$pArray[$j]";
   }
  }
  print "\n";
}

sub WriteOutPhoneKW {
  my ($kwId, $pronArray, $vocab) = @_;
  my $aLen = scalar @$pronArray;
  
  my (@bArray, @fArray) = (); 
  for (my $idx = 0; $idx < $aLen; $idx ++) {
    my @a = ();
    push @bArray, \@a;
  }
  my ($i, $j) = (0,0);
  while ($i < $aLen && $i >= 0) {
    my $pArray = $$pronArray[$i];
    my $bLen = scalar @$pArray;
      
    my $p2A = $$pArray[$j];
    my @iArray = ();
    PronConversion($p2A, \@iArray, $vocab);
    push @fArray, \@iArray;
    my $bStack = $bArray[$i];
    push @$bStack, $j;
    if ($i == $aLen -1) { # sweep finished
      OutputKW($kwId, \@fArray);
      pop @fArray;
      $i --;
    }
   
    $i ++  if $i < $aLen -1;
    $bStack = $bArray[$i];
    if (scalar @$bStack == 0) {
      $j = 0;
    } else {
      $j = pop @$bStack;
      $pArray = $$pronArray[$i];
      $bLen = scalar @$pArray;
      while ($j == $bLen -1) {
        $i --;
        last if ($i < 0);
        pop @fArray;
        $bStack = $bArray[$i];
        $j = pop @$bStack;
        $pArray = $$pronArray[$i];
        $bLen = scalar @$pArray;
      }
      last if ($i < 0);
      $j ++;
    }
  }   
}
## ------ main entrance ------
my (%phoneVocab, %lexVocab);
LoadWords($phones, \%phoneVocab, 0);
LoadLexicon($wlex, \%lexVocab);

print STDERR "\nstdin expected\n";
while (<STDIN>) {
  chomp;
  s/ +/ /g;
  my @a = split /\s+/;
  my $kwId = shift @a;
  my $num = scalar @a;
  my @newArray = (); my $oov = 0;
  for (my $idx = 0; $idx < $num; $idx ++) {
    my $word = $a[$idx ];
    if (not exists $lexVocab{$word}) {
      print STDERR "$_: oov word"; $oov = 1; $idx = $num;
    }
    my $pArray = $lexVocab{$word};
    push @newArray, $pArray;
  }
  next if ($oov == 1);
  if (scalar @newArray <1) {
    print STDERR "$_: no pron\n"; next;
  }
  WriteOutPhoneKW($kwId, \@newArray, \%phoneVocab);
}
print STDERR "\nstdin ended\n";

