#!/usr/bin/env perl
use warnings;
use strict;

scalar @ARGV == 1 || 
die "\n\nUsage:\n get_merge_results.pl <sum.txt file list>\n\n";

my ($flist) = @ARGV;

open (FILE, "$flist") or
die "$flist cannot open";
my %vocab_all = ();
while (<FILE>) {
  chomp;
  my $file;
  my $fname= $_;
  open $file, "<$fname";
  my @info = split "/", $fname;
  my $N = scalar @info;
  my ($merge, $prune) = ($info[$N-3], $info[$N-2]);
  # print "merge=$info[$N-3], prune=$info[$N-2]\n";
  while (<$file>) {
    chomp;
    if(/Occurrence/) {
      s/\s+//g;
      s/\S[^\|]+\.png//g;
      if (/^\|Occurrence/) {
        # print "$_\n";
        my @fields = split /\|/;
        my $atwv = $fields[12];
        # print "fields=",$fields[12], "\n";
        $vocab_all{$merge}{$prune} = $atwv;
        # print "$merge $prune $atwv \n";
      }
    }
  }
  close $file;
}
close FILE;
for  my $merge (keys %vocab_all) {
  my $vref = $vocab_all{$merge};
  my $best_atwv = 0; my $best_prune ="";
  for my $prune (keys %$vref) {
    my $atwv = $$vref{$prune};
    print STDERR "$prune $atwv \n";
    if ($atwv > $best_atwv) {
      $best_atwv = $atwv; 
      $best_prune = $prune;
    }
  }
  print "$merge $best_prune $best_atwv\n";
}
