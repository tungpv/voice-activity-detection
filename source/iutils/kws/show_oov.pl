#!/usr/bin/env perl
use strict;
use warnings;
my  $numArg = scalar @ARGV;
$numArg == 2 or 
die "\n\nExample: cat keywords.txt | $0 <from> <lexicont.txt> > oov.txt\n\n";
my($from, $lex) = @ARGV;

$from >= 1 or die "from means field, being over 0, but yours is $from";

my %vocab = ();
open L, "<$lex" or die "file $lex cannot open\n";
while (<L>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $vocab{$1} = 0;
}
close L;

print STDERR "stdin expected\n";
my $total = 0; 
my $oovNum = 0;
while (<STDIN>) {
  chomp;
  
  my @A = split(' ', $_); 
  for(my $i = $from - 1; $i < scalar @A; $i ++) {
    my $w = $A[$i];
    if (not exists $vocab{$w}) {
      $oovNum ++; print "$w\n";
    }
    $total ++;
  }
}
my $s =sprintf "%.4f", $oovNum/$total;
print STDERR  "oov_rate = $s\n";
print STDERR "stdin ended\n";

