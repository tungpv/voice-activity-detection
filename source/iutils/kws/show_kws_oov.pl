#!/usr/bin/env perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 2) {
  die "\n\nUsage:\n $0 <base> <target>\n\n";
}

my($bfname, $tgfname) = @ARGV;

open BF, "$bfname" or die "file $bfname cannot open\n";
my %vocab = (); my $total = 0;
while (<BF>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $vocab{$1} = 0; 
  $total ++;
}
close BF; 

open TF, "$tgfname" or die "file $tgfname cannot open\n";
while (<TF>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my $kwid = $1;
  if (exists $vocab{$kwid}) {
    $vocab{$kwid} ++;
  }
}
close TF;
my $oovNum = 0;
foreach my $kwid (keys %vocab ) {
  if ($vocab{$kwid} ==0) {
    $oovNum ++;
  }
}

print "\ntotal_instance=$total, oov_instance=$oovNum\n";


