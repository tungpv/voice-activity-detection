#!/usr/bin/perl -w
use strict;

use Getopt::Long;
use XML::Simple;
use Data::Dumper;
use Scalar::Util qw(reftype);

#Command useage: systemComb_CombSum.pl <System 1> <System 2> ... <System n> <weights-vector> <output> <keyword file> <normalization> <corpus length> <global threshold>

#<System k>: A .xml file that contains search results from system k.
#<weights-vector>: list of MTWV of systems on dev set. It is list of floating-point values, separated by the ",". Example: 0.4461,0.4050,0.4126
#<output> : Output of system combination
#<keyword file> : a file that contain keyword list in format: keyword_id<tab>keyword_content
#<normalization> : Normalization method after system combination (1 - no Norm, 2- STO, 3- KST)
#<corpus length> : Needed for KST normalization
#<global threshold>: For KST norm, it is set as 0.5. 
#					 For STO norm: For dev data, just set it arbitrary. For eval data, it is estimated as: eval_threshold = best_dev_threshold * dev_corpus_length / eval_corpus_length

#Command example: perl systemComb_WComMNZ.pl kwslist_dev_STO_S1.xml kwslist_dev_STO_S2.xml kwslist_dev_STO_S3.xml 0.4461,0.4050,0.4126 dev_STO_WCombMNZ_STO.xml kwid_dev.txt 2 39465.5 0.025

#Load a result file in xml format
sub LoadXML {
	my $xml = shift;
	my $ha = shift;
	my $data = XMLin($xml);
	#my @arr = ();
	foreach my $kwentry (@{$data->{detected_kwlist}}) {
  	#print "$kwentry->{kwid}\n";
		my @arr = ();
		
		#For current keyword which has more than one detection
		if(ref($kwentry->{kw}) eq 'ARRAY'){
    			my @arrTemp = @{$kwentry->{kw}};
				foreach my $eachTemp (@arrTemp) {
					push (@arr, [$eachTemp, 0]);
				}
		}
		else { 
			push (@arr, [$kwentry->{kw}, 0]);
		}
		@{$$ha{$kwentry->{kwid}}} = @arr;
	}
	 
}

#Load keyword list in text file. Each line in keyword list contain a pair(keyword_id keywor_content)
#The result of this function is a list of keyword stored in an array
sub LoadKWList {
	my $filein = shift;
	my $arr = shift;
	my $source;
	if (!open($source, "$filein")) {print "Fail to open input file: $filein\n"; exit 1;}
	while (<$source>) {
  		chomp;
  		my @col = split(" ", $_);
		push @$arr, $col[0];
  	}
	close $source;
}

sub megerForAquery {


	my $kid = $_[0]; #get keyword id
	my @listAllSystems = @{$_[1]}; # This array store all detection lists of all systems. Each element is a detection list for all keyword of a system.
	my $listFinal = $_[2]; #A reference to array contain final results after merging
	my $combMethod = $_[3]; #Combination method
	my $listWeight = $_[4]; #Weight vector indicates the contribution of each system


	my $sum = 0;
	for my $i (0 .. $#listAllSystems) {
		#print "----------System ($i + 1) -------- \n";
		my %h = %{$listAllSystems[$i]};
		if(!defined $h{$kid}) {
			next;
		}
		my @listOfDec = @{$h{$kid}}; #Get list of detection for the selected keyword
		my $numOfDec = $#listOfDec;

		foreach my $kw (@listOfDec) {# Go through the detection list

			if(@{$kw}[1] == 1) { #This item is already processed (actually it is already merged with an item in detection list of a previous system) => Ignore it
				next;
			}

			my $currentDec = @{$kw}[0] ;  #get current detection
			my @result = (); #list of detection from other system that overllap with current detection
			my %nullR = ();

			for(my $k = 0; $k < $i; $k++) { # Of course, all previous system have been investigated. Thus, there is no overllap detection from previous systems => Push null item into results list
				push(@result, [%nullR,0]);
			}

			@{$kw}[1] = 1; # Mark the current detection as processed
			#print "Current detection: $currentDec->{tbeg}, $currentDec->{dur}, $currentDec->{file}, $currentDec->{score},  $currentDec->{channel}, $currentDec->{decision} \n"; 
			
			push(@result, $kw);# Also put current detection into overlapped list

			#Find overllap detections in detection list of next systems.
			for(my $k = $i+1; $k <= $#listAllSystems;$k++) 
            {
                    my %htemp = %{$listAllSystems[$k]};
					if(!defined $htemp{$kid}) {
						push(@result, [%nullR,0]);
					}
					else{
						my @listOfDecTemp = @{$htemp{$kid}};
						my $isExist = 0;
						
						foreach my $kwtemp (@listOfDecTemp) {
							my $tmptmp = @{$kwtemp}[0];						
							#print "Compared detection: $tmptmp->{tbeg}, $tmptmp->{dur}, $tmptmp->{file}, $tmptmp->{score},  $tmptmp->{channel}, $tmptmp->{decision} \n";
							# Condition of overllap: the gap of mid times smaller than 0.5s
            				if($tmptmp->{file} eq $currentDec->{file} && abs($tmptmp->{tbeg}+ $tmptmp->{dur}/2 - $currentDec->{tbeg} -  $currentDec->{dur}/2) < 0.5){
								@{$kwtemp}[1] = 1;
								push(@result, $kwtemp);
								$isExist = 1;
								last;
							}   
		      			
        				} #end foreach
						
						if($isExist == 0){
							push(@result, [%nullR,0]);
						}
			
					} #end if-else
                    
                } #end for

				#print "List of overllaped detections \n";
				foreach my $kwtemp (@result) {
					my $isExist = @{$kwtemp}[1];
					my $tmptmp = @{$kwtemp}[0];
					if(defined $isExist) {
						
						#print "$tmptmp->{tbeg}, $tmptmp->{dur}, $tmptmp->{file}, $tmptmp->{score},  $tmptmp->{channel}, $tmptmp->{decision} \n"; 
					}
					else {
						#print "IGNORE \n";
					}
				}
				
				#Do merging on list of overllap detection to get a single detection
				my $megeredRes = &megerOverlap(\@result, $listWeight, $combMethod, $kid);
				#print "After megering, detection = $megeredRes \n";
				push @{$listFinal}, $megeredRes;
				my @col = split(" ", $megeredRes);
				$sum += $col[4];
			} #end foreach 
		
	}#end for
	return $sum;
}

#Do merging on list of overllaped detection to get a single detection
sub megerOverlap {
	my @listOverlap = @{$_[0]};
	my @weight = @{$_[1]};
	
	my $comMethod = $_[2];
	my $kid = $_[3];
	my $st = 0;
	my $dur = 0;
	my $score = 0;
    my $doc = "";
    my $countNotNull = 0; #Count number of detection in overllaped list (number of system having non-zero score)
	my $idx = -1;
    foreach my $kwtemp (@listOverlap)
    {
        my $isExist = @{$kwtemp}[1];
		my $tmptmp = @{$kwtemp}[0];
		$idx++;
		if(!defined $isExist) {
			next; 
		}

        $countNotNull++;
        $st += $tmptmp->{tbeg};
        $dur += $tmptmp->{dur};
        $doc = $tmptmp->{file};
		#CombMax
        if($comMethod == 1) { 
     
            if($score < $tmptmp->{score}){
                    $score = $tmptmp->{score};
			}
        }
        elsif($comMethod==2 || $comMethod== 3) {
			#combSum and WCombMNZ
			#print "Current sum =". $score.", current score = ". $tmptmp->{score}. ", idx = ".$idx.", current weight = ".$weight[$idx]. "\n";
            $score +=  $weight[$idx] * $tmptmp->{score};
            #denorm += weight[i];
        }
                
    }
	#print "Sum = ".$score. "\n";
	#print "Count not null = ".$countNotNull. "\n";
    $st = $st/$countNotNull;
    $dur = $dur/$countNotNull;
	$st = sprintf("%.2f", $st);
    $dur = sprintf("%.2f", $dur);
    if($comMethod == 3) #WCombMNZ
    {
        $score = $score * $countNotNull;
        #score = score / denorm;
    }
    #if(score > 1)
    #    score = 1;
    $score = sprintf("%.5f", $score);
	my $result = "$kid $doc $st $dur $score";
	return $result;
}

#This function is used to normalize score after merging
sub normalize{
	my $listOfDetection = $_[0];
	my $sum = $_[1];
	my $normMethod = $_[2];
	my $duration = $_[3];
	
    if($normMethod==1) { #No norm
            return;
	}
    elsif($normMethod == 2) # Sum to one
    {
        for(my $i = 0; $i < @{$listOfDetection};$i++)
        {
                my $d = @{$listOfDetection}[$i];
				my @col = split(" ", $d);
				my $newScore;
                if($sum > 0) {
                    $newScore = sprintf("%.5f", $col[4]/$sum);
				}
                else {
                    $newScore = 1;
				}
				@{$listOfDetection}[$i] = "$col[0] $col[1] $col[2] $col[3] $newScore";
        }
    }
    else # KST
    {
            my $termSPTH = $sum/($duration/999.9+(999.9-1)/999.9 * $sum);
            for(my $i = 0; $i < @{$listOfDetection};$i++)
            {
                my $d = @{$listOfDetection}[$i];
				my @col = split(" ", $d);
				my $newScore = ($col[4] + 1 - $termSPTH)/2;
				$newScore = sprintf("%.5f", $newScore);
				@{$listOfDetection}[$i] = "$col[0] $col[1] $col[2] $col[3] $newScore";
            }
     }
}

#MAIN ENTRY
my @listAll = ();
my @listFinal = ();
#my ($xml1, $xml2, $xml3, $xmlOut) = @ARGV;
my $total = $#ARGV + 1;
for(my $i = 0; $i < $total - 6;$i++)
{
	my %arr = ();
	my $xmlIn = $ARGV[$i];
	&LoadXML ($xmlIn, \%arr);
	push @listAll, \%arr;
}
my $weightVec = $ARGV[$total - 6]; # List of MTWVs, separated by comma
my $xmlOut = $ARGV[$total - 5]; # output file
my $queryList = $ARGV[$total - 4]; #Query list to help merging
my $normMethod = $ARGV[$total - 3]; #Normalization method after system combination (1 - no Norm, 2- STO, 3- KST)
my $copusLength = $ARGV[$total - 2]; # Only useful if $normMethod = 3. It is used to estimate keyword specific threshold
my $global_th = $ARGV[$total - 1]; # Global threshold

my @kwlist = ();
&LoadKWList($queryList, \@kwlist);

#For WCombMNZ, the value combMethod = 3 and weight are proportional to MTWV of participated systems
my $combMethod = 3;
my @weight = split(',', $weightVec);
my $s_weight = 0;
foreach my $wi (@weight) {
	$s_weight += $wi;
}
foreach my $wi (@weight) {
	$wi = $wi / $s_weight;
}



#my $normMethod = 3; #Normalization method after system combination (1 - no Norm, 2- STO, 3- KST)
#my $copusLength = 39465.5; # Only useful if $normMethod = 3. It is used to estimate keyword specific threshold
#my $global_th = 0.0521; # Global threshold



open (MYFILE, ">$xmlOut");
print MYFILE "<kwslist system_id=\"\" language=\"Vietnamese\" kwlist_filename=\"\"> \n";
foreach my $keyword (@kwlist) {
                print "Consider keyword $keyword \n";
                my @listOfResults = ();
                my $sum = &megerForAquery($keyword, \@listAll, \@listOfResults, $combMethod, \@weight);
				 
                &normalize(\@listOfResults, $sum, $normMethod, $copusLength);
				#print $#listOfResults."\n" ;
				if($#listOfResults == -1) {
					#next;
					print "No detection. \n";
				}
                print MYFILE "\t<detected_kwlist search_time=\"1\" kwid=\"$keyword\" oov_count=\"0\"> \n" ;
                for(my $idx = 0; $idx < @listOfResults;$idx++)
                {
                    my $d = $listOfResults[$idx];
					my @col = split(" ", $d);
                    my $decision = "";
                    if($col[4] >= $global_th) {
                        $decision = "YES";
					}
                    else {
                        $decision = "NO";
					}
                    print MYFILE "\t\t<kw tbeg=\"$col[2]\" dur=\"$col[3]\" file=\"$col[1]\" score=\"$col[4]\" channel=\"1\" decision=\"$decision\" /> \n";
                }
                print MYFILE "\t</detected_kwlist> \n";
                
                
}
print MYFILE "</kwslist> \n";
close (MYFILE); 

#&megerForAquery('KW107-0270', \@listAll, \@listFinal, 3, \@weight);


