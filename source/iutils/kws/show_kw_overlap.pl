#!/usr/bin/env perl
use warnings;
use strict;


print STDERR "\n DESIGN: iutils/kws/show_kw_overlap.pl   <kwlist.1> <kwlist.2>\n";

my $numArgs = scalar @ARGV;

if ($numArgs != 2) {
  die "\n\nUsage:\n $0 <kwlist.1> <kwlist.2>\n\n";
}

my ($kwlist1, $kwlist2) = @ARGV;

sub LoadKWList {
  my ($vocab, $fname) = @_;
  open F, "< $fname" or die "file $fname cannot open\n";
  my $total = 0;
  while (<F>) {
    chomp;
    my @A = split (' ', $_);
    shift @A; 
    my $s = join (' ', @A);
    $$vocab{$s} ++;
    $total ++;
  }
  close F;
  return $total;
}
my %vocab = ();
my $kwlist1Total = LoadKWList(\%vocab, $kwlist1);

open F, "<$kwlist2" or die "file $kwlist2 cannot open\n";
my ($kwlist2Total, $overlap) = (0, 0);
while (<F>) {
  chomp;
  my @A = split (' ', $_);
  shift @A; 
  my $s = join (' ', @A);
  if (exists $vocab{$s}) {
    $overlap ++;
  }
  $kwlist2Total ++;
}
close F;
print "\n$kwlist1Total  $kwlist2Total $overlap\n";


