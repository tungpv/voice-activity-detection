#!/usr/bin/env perl 
use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 2 ) {
  die "\n\nExample:cat lang/phones.txt |$0 <phone_map.int> <phone_map.txt>\n";
}

my ($lex, $txt) = @ARGV;

print STDERR "\nstdin expected\n";
my $maxId = 0;
my %vocab = ();
open LEX, ">$lex" or die "lex  $lex file cannot open";
open TXT, ">$txt" or die "map_txt $txt file cannot open";
my %mapVocab = ();
while (<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)/;
  if ($2 > 0 ) {
    $vocab{$1} = $2;
    $maxId = $2 if ($2 > $maxId);
  }
}
foreach my $phone (keys %vocab) {
  my $map_phone = $phone;
  $map_phone =~ s/_[SBIE]$//g;
  my $mapId = $maxId + 1;
  my $id = $vocab{$phone};
  if (exists $vocab{$map_phone}) {
    $mapId = $vocab{$map_phone};
  } else {
    if (exists $mapVocab{$map_phone}) {
      $mapId = $mapVocab{$map_phone};
    } else {
      $mapVocab{$map_phone} = $mapId;
      $maxId ++;
    }
  }
  print TXT $map_phone, " ", $mapId, "\n";
  print LEX $phone, " ", $map_phone, "\n"; 
}
print STDERR "\nstdin ended\n";

close LEX;
close TXT;
