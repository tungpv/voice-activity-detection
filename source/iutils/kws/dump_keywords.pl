#!/usr/bin/env perl
use warnings;
use strict;

my $numArg = scalar @ARGV;

if ($numArg != 4) {
  die "\n\nUsage:\n
  $0 <lexicon.txt> <source_keywords.txt> <inv_keywords.txt> <oov_keywords.txt>\n\n";
}
my ($lex, $skw, $invkw, $oovkw) = @ARGV;

my %vocab = ();
open L, "<$lex" or die "lex $lex cannot open";
while (<L>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $vocab{$1} = 0;
}
close L;
open S, "<$skw" or die "source keyword $skw cannot open";
open I , ">$invkw" or die "inv keyword $invkw cannot open";
open O, ">$oovkw" or die "oov keyword $oovkw cannot open";
my ($oovNum, $total) = (0, 0);
while (<S>){
  chomp;
  $total ++;
  my @A  = split(' ', $_) ;
  my $kwid = shift @A;
  my $num = scalar @A;
  $num > 0 or die "empty keyword sequence found: $_, ", scalar @A, "\n"  ;
  my $oov = 0; 
  for (my $i = 0; $i < $num; $i ++) {
    my $kw = $A[$i];
    if(not exists $vocab{$kw}) {
      $oov = 1; $i = $num + 1; last;
    }
  }
  if ($oov == 1) {
    $oovNum ++; 
    print O "$kwid\t", join " ", @A, "\n";
  } else {
    print I "$kwid\t", join " ", @A, "\n";
  }
}
my $s = sprintf "%.4f", $oovNum/$total;
print STDERR "## oov_rate: $s\n";

close I;
close O;
close S;

