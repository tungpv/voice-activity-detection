#!/bin/bash


function die {
  printf "\nERROR: $1\n"; exit -1;
}
function doit {
  return 0;
}
function passit {
  return 1;
}

function Usage {
  exit -1;
}

. ./path.sh || die "path.sh expected ..."

# do two system combination first
if doit; then 
  main_xml=data/local/kws/i2r/sgmm_mmi.kwslist.xml
  sdir=exp/sgmm2_5a_mmi_b0.5
  gaptime=0.6
  thresh=0.9
  dir=$sdir/xml_merge_i2rc1c2c3_6_${thresh}
  [ -d $dir ] || mkdir -p $dir
  aux1=exp/sgmm2_5a_mmi_b0.5/kws_eval/kwslist.xml
  aux2=../c2/exp/kws_eval/kwslist.xml
  aux3=data/local/kws/i2r/sgmm.kwslist.xml
  aux5=data/local/kws/i2r/ml.kwslist.xml
  aux4=../c4/exp/sgmm2_5a/decode_dev10/kws_eval_no_prune/kwslist.xml

  local_utils/kws/merge_kwslist_xml_aux_yes02.pl $main_xml  $aux1 $gaptime $thresh 2>$dir/LOG.merge  | \
  local_utils/kws/merge_kwslist_xml_aux_yes02.pl -  $aux2  $gaptime $thresh 2>>$dir/LOG.merge | \
  local_utils/kws/merge_kwslist_xml_aux_yes02.pl -  $aux5  $gaptime $thresh 2>>$dir/LOG.merge | \
  local_utils/kws/merge_kwslist_xml_aux_yes02.pl -  $aux4  $gaptime 0.95 2>>$dir/LOG.merge | \
  local_utils/kws/merge_kwslist_xml_aux_yes02.pl -  $aux3  $gaptime $thresh 2>>$dir/LOG.merge  > $dir/kwslist.xml
  kwsdata=data/local/kws
  kwsoutdir=$dir
  local_utils/kws/kws_score.sh $kwsdata $kwsoutdir
fi

exit 0;
mdirs=(../c4  ../c2)
aux_xmls=(exp/sgmm2_5a/decode_dev10/kws_eval_no_prune/kwslist.xml  exp/kws_eval/kwslist.xml)
threshs=(0.95 0.9)
main_xml=exp/sgmm2_5a_mmi_b0.5/kws_eval/kwslist.xml
sdir=exp/sgmm2_5a_mmi_b0.5
gaptime=0.6
for idx in 0 1; do
  mdir=${mdirs[$idx]}
  base=`basename $mdir`
  rpath=${aux_xmls[$idx]}
  thresh=${threshs[$idx]}
  aux_xml=$mdir/$rpath
  [ -e $aux_xml ] || die "file $aux_xml does not exist ..."
  echo -e "\nmerge with file $aux_xml ...\n"
  dir=$sdir/xml_merge_${base}_${thresh}
  echo -e "\ntarget folder is $dir ...\n"
  [ -d $dir ] || mkdir -p $dir
  local_utils/kws/merge_kwslist_xml_aux_yes.pl $main_xml $gaptime $thresh < $aux_xml   > $dir/kwslist.xml 2>$dir/LOG.aux_yes
  kwsdata=data/local/kws
  kwsoutdir=$dir
  local_utils/kws/kws_score.sh $kwsdata $kwsoutdir
  echo -e "\nDone with merging from $mdir, check $dir !\n"
done

exit 0

if doit; then
  main_xml=exp/sgmm2_5a_mmi_b0.5/kws_eval/kwslist.xml
  sdir=exp/sgmm2_5a_mmi_b0.5
  mdir=../c2
  base=`basename $mdir`
  aux_xml=$mdir/exp/kws_eval/kwslist.xml
  gaptime=0.6
  thresh=0.9
  dir=$sdir/xml_merge_${base}_${thresh}
  for x in $main_xml $aux_xml; do
    [ -e $x ] || die "file $x does not exist ..."
  done 
  [ -d $dir ] || mkdir -p $dir
  local_utils/kws/merge_kwslist_xml_aux_yes.pl $main_xml $gaptime $thresh < $aux_xml   > $dir/kwslist.xml 2>$dir/LOG.aux_yes
  kwsdata=data/local/kws
  kwsoutdir=$dir
  local_utils/kws/kws_score.sh $kwsdata $kwsoutdir
fi

