#!/usr/bin/env perl

use warnings;
use strict;

my ($lex) = @ARGV;
my %vocab = ();
open L, "<$lex" or die "lex $lex cannot open\n";
while(<L>) {
  chomp;  my $line = $_;
  s/\t/ /g; s/ +/ /g;
  my @A = split / /;
  my $w = $A[0];
  if (exists $vocab{$w}) {
    my $pa = $vocab{$w};  push @$pa, $line;
  } else {
    my @B ; $vocab{$w} = \@B; my $pa = $vocab{$w}; push @$pa, $line;
  }
}
close L; my $added = 0;

while (<STDIN>) {
  chomp;
  my $line = $_;  
  s/\t/ /g;
  my @A = split / /;
  my $w = $A[0];
  if (exists $vocab{$w}) { print STDERR "$line \n"; } else {
    my $pa = $vocab{$w}; 
    print "$line\n";
    $added ++; 
  }
}
foreach my $w (keys %vocab) {
  my $pa = $vocab{$w}; my $num = scalar @$pa;
  for(my $i = 0; $i < $num; $i ++) {
    my $line = $$pa[$i];  print "$line\n";
  }
}
print STDERR "added=$added\n";

