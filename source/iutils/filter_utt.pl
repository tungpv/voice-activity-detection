#!/usr/bin/perl -w 
use strict;

scalar @ARGV == 1 or
die "\nnonspeech_word list file expected\n";
my ($nonspeech) = @ARGV;

sub LoadVocab {
  my ($fname, $vocab) = @_;
  open F, "<$fname" or 
  die "file $fname cannot open";
  while (<F>) {
    chomp;
    m/(\S+)/ or next;
    my $w =$1;
    $$vocab{$w} = 0;
  }
  close F;
}
# check if all words in a utterance are
# nonspeech words.
sub CheckUttWord {
  my ($ustr, $vocab) = @_;
  my @au = split /\s+/, $ustr;
  my $lab = shift @au;
  my $N = scalar @au;
  for(my $i = 0; $i < $N; $i ++) {
    my $w = $au[$i];
    return 0 if not exists $$vocab{$w}; 
  }
  return 1;
}

my %vocab = ();

LoadVocab($nonspeech ,\%vocab);
# std in
while (<STDIN>) {
  chomp;
  my $ustr = $_;
  my $ret = CheckUttWord($ustr, \%vocab); 
  if ($ret == 1) {
    print STDERR "$_\n";
  } else  {
    print "$_\n";
  }
}
