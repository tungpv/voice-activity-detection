#!/usr/bin/env perl

use warnings;
use strict;

my ($lex) = @ARGV;
my %vocab = (); 
open L, "<$lex" or die "lex $lex cannot open\n";
while(<L>) {
  chomp;  my $line = $_;
  s/\t/ /g; s/ +/ /g;
  my @A = split / /;
  my $w = $A[0];
  if (exists $vocab{$w}) {
    my $pa = $vocab{$w};  push @$pa, $line;
  } else {
    my @B =() ; $vocab{$w} = \@B; my $pa = $vocab{$w}; push @$pa, $line;
  }
}
close L; my $borrowed = 0;
my %v = ();
while (<STDIN>) {
  chomp;
  my $line = $_;  
  s/\t/ /g;
  my @A = split / /;
  my $w = $A[0];
  if (exists $vocab{$w}  && not exists $v{$w}) { 
    $borrowed ++;  $v{$w} = 0;
    my $pa = $vocab{$w};
    for(my $i = 0; $i< scalar @$pa; $i ++) {
      my $bline = $$pa[$i];
      print "$bline\n";
    }
  } else {
    my $pa = $vocab{$w}; 
    print "$line\n";
  }
}

print STDERR "borrowed=$borrowed\n";

