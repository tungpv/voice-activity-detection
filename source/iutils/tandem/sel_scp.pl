#!/usr/bin/perl -w

use strict;

scalar @ARGV == 2 || 
die "\n\nUsage:$0 <select_list> <candidate_list>\n\n";

sub LoadVocab {
  my ($fname, $vocab) = @_;
  open File, "$fname";
  while (<File>) {
    chomp;
    m/(\S+)\s+(.*)/|| next;
    $$vocab{$1} = 0;
  }
  close File;
}


my ($selFName,$candFName) = @ARGV;
my %vocab = ();
LoadVocab($selFName, \%vocab);

open File, "$candFName";
while (<File>) {
  chomp;
  m/(\S+)\s+(.*)/|| next;  
  my $lab = $1;
  if(exists $vocab{$lab}) {
    print "$_\n";
  }
}
close File;
