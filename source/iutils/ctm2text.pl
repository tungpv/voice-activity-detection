#!/usr/bin/env perl
use warnings;
use strict;

sub OverlapRate {
  my ($uStart, $uEnd, $wStart, $wEnd) = @_;
  return 0 if $uStart >=$wEnd or $uEnd <= $wStart;
  my $oStart = $uStart;
  $oStart = $wStart if $wStart > $oStart;
  my $oEnd = $uEnd;
  $oEnd = $wEnd if $wEnd > $oEnd;
  return ($oEnd - $oStart) / ($wEnd - $wStart);
}

sub GetText {
  my ($start, $end, $pArray, $pText) = @_;
  $$pText = "";
  my $num = scalar @$pArray;
  for (my $idx = 0; $idx < $num; $idx ++) {
    my $pWordArray = $$pArray[$idx];
    my ($wStart, $wEnd, $word) = @$pWordArray;
    my $overlap_rate =OverlapRate ($start, $end, $wStart, $wEnd);
    $$pText .= " $word" if ($overlap_rate > 0);
  }
}

my $numArgs = scalar @ARGV;

die "\nUsage:\n cat segments\| $0 ctm >text\n" if $numArgs != 1;
my ($ctm) = @ARGV;

open CTM, "<$ctm" or die "file $ctm cannot open \n";
my %vocab = ();
while (<CTM>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/ or next;
  my ($segName, $chan, $start, $end, $word) = ($1, $2, $3, $3+$4, $5);
  # print "$_ \n $segName $start $end $word \n";
  my @new_array = ($start, $end, $word);
  if (exists $vocab{$segName}) {
    my $p_array = $vocab{$segName};
    push @$p_array, \@new_array;
  } else {
    my @new_array01 = ();
    $vocab{$segName} = \@new_array01; my $pArray = $vocab{$segName};
    push @$pArray, \@new_array;
  }
}
close CTM;
# open segments file from standard input
while (<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/ or next;
  my ($labName, $segName, $start, $end) = ($1, $2, $3, $4);
  if (exists $vocab{$segName}) {
    my $pArray = $vocab{$segName};
    my $text;
    GetText ($start, $end, $pArray, \$text);
    if (length $text > 0) {
      print "$labName $text\n";
    } else {
      print STDERR "$labName is out-of-timeboundary\n";
    }
  } else  {
    print STDERR "$labName is out-of-segment\n";
  }
}


