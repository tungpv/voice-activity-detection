#!/bin/bash



echo -e "\n$0 $@\n" 1>&2

field_num=1;
if [ $# -lt 2 ]; then
  echo -e "\n\nExample: $0 <vocab> <text> <field_num>\n\n"
  exit 1
fi
vocab=$1;
text=$2;
[ $# -ge 3 ] && field_num=$3;
[ $field_num -gt 0 ] | exit 1;
cat $text | \
perl -e '
  ($wlist, $findex) = @ARGV;
  open WL, "<$wlist" or die "\n## file $wlist cannot open\n";
  while(<WL>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    $vocab{$1} = 0;
  }
  close WL;
  print STDERR "\nstdin expected\n";
  $total = 0; $oov_num = 0;
  while(<STDIN>) {
    chomp;
    s/\t/ /g;
    @A = split / /;
    for($i = $findex -1; $i < scalar @A; $i ++) {
      $w = $A[$i];
      $total ++;
      if (length($w)> 0 && not exists $vocab{$w}) {
        $oov_num ++;
        # print STDERR "$_,", $w, $i+1,  " is oov word\n";
        $oov_vocab{$w} ++;
      }
    }
  }
  $oov_rate=0;
  if ($total > 0) {
    $oov_rate = sprintf("%.4f",$oov_num/$total);
  }
  foreach $w (keys %oov_vocab ) {
    print $w, " ", $oov_vocab{$w}, "\n";
  }
  print STDERR "oov stats: $oov_num, $total, $oov_rate\n";
  print STDERR "\nstdin ended\n";
' $vocab  $field_num | sort -k2nr
