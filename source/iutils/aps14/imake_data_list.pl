#!/usr/bin/env perl

use warnings;
use strict;

print STDERR "\n\nLOG: $0 ", join(' ', @ARGV), "\n\n";
print STDERR "\n\nDESIGN:$0 babel_trans_dir > datalist.txt \n\n";

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\n\nUsage: $0 <babel_trans_dir>\n\n";
}

my ($dir) = @ARGV;

open (F, "ls $dir/*.txt|") or 
die "canot open file\n";
while (<F>) {
  chomp;
  my @A = split ('/', $_);
  my $s = $A[scalar @A -1];
  $s =~ s/\.txt//g;
  print "$s\n";
}

close F;

