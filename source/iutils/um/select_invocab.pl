#!/usr/bin/perl -w
use strict;
print STDERR "\nInput select_invocab.pl ", join " ", @ARGV, "\n";
scalar @ARGV == 2 or
die "\n\nUsage:\n select_invocab.pl <sub_set> <candidate_set>\n";
my ($subName, $candiName) = @ARGV;

sub LoadVocab {
  my ($fname, $vocab) = @_;
  open FILE, "$fname";
  while (<FILE>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    my ($lab) = ($1);
    $$vocab{$lab} = 0;
  }
  close FILE;
}

my %vocab = ();
LoadVocab($subName, \%vocab);
open FILE, "$candiName";
while (<FILE>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($lab) =($1);
  next if not exists $vocab{$lab};
  print "$_\n";
}
close FILE;
