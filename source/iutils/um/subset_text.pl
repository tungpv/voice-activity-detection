#! /usr/bin/env perl
use warnings;
use strict;


print STDERR "\n$0 ", join " ", @ARGV, "\n";

sub LoadVocab {
  my ($vocab, $fname) = @_;
  open (FILE, "<", $fname) or
  die "file $fname cannot open to read";
  while (<FILE>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    my ($lab) = ($1);
    $$vocab{$lab} ++;
  }
  close FILE;
}
my ($cand) = @ARGV; 
my %vocab = ();
LoadVocab(\%vocab, $cand);
while (<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next; 
  my ($lab) = ($1);
  if (exists $vocab{$lab}) {
    print "$_\n";
  }
}
