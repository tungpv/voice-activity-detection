#!/usr/bin/perl -w
use strict;

scalar @ARGV == 2 or
die "\n\nUsage:\ncut_uconf.pl <cutoff> <uconfFile>\n\n";
my ($cutoff, $uconfName) = @ARGV;
$cutoff > 0 or 
die "cutoff $cutoff is illegal";
open FILE, "$uconfName" or 
die "file $uconfName cannot open";
while (<FILE>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($lab, $score ) = ($1, $2);
  if ($score >= $cutoff ) {
    print "$_\n";
  }
}
close FILE;
