#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

export LC_ALL=C

# begin user define variable
ratio=1:1;

passit=false;
# end user defined variable

. utils/parse_options.sh || die "utils/parse_options.sh needed";


if [ $# -ne 3 ]; then
  echo -e "\n\n";
  echo  "Usage: $0 --ratio ratio-string <sdir1> <sdir2> <dir>";
  echo "Example: $0 --ratio 2:1 data/train data/utrain data/merge";
  echo -e "\n\n"; 
  exit 1;
fi


sdir1=$1; shift;
sdir2=$1;shift;
dir=$1; shift;

[ -d $dir ] || mkdir -p $dir
rm -f $dir/* 2>/dev/null

part1=1;
part2=1;
part1=`echo $ratio | awk -F ':' '{ print $1;}' `
part2=`echo $ratio | awk -F ':' '{print $2}'`

# prepare to duplicate the data set from sdir1
dupdir1=$sdir1;
if [ $part1 -gt 1  ]; then
  echo -e "duplicate data set from $sdir1 to $part1 times larger in folder $dir/1";
  dupdir1=$dir/1;
  iutils/um/duplicate_data_set.sh --ntimes $part1 $sdir1 $dupdir1
fi
# prepare to duplicate the data set from sdir2
dupdir2=$sdir2;
if [ $part2 -gt 1 ]; then
  dupdir2=$dir/2;
  iutils/um/duplicate_data_set.sh --ntimes $part2 $sdir2 $dupdir2
fi

# restore them back
sdir1=$dupdir1;
sdir2=$dupdir2;
for x in text wav.scp utt2spk feats.scp segments reco2file_and_channel cmvn.scp; do
  if [ -e $sdir1/$x  -a  -e $sdir2/$x ]; then
    s_num=`wc -l < $sdir1/$x`
    u_num=`wc -l < $sdir2/$x` 
    cat $sdir1/$x $sdir2/$x | sort -u > $dir/$x
    num=`wc -l < $dir/$x` 
    [ $[$s_num+$u_num] -eq $num ] || \
    echo -e "WARNING:two data $sdir1/$x and $sdi2/$x sets are not disjoint: $s_num + $u_num != $num";
  fi
done
rm -f $dir/spk2utt 2>/dev/null
    utils/utt2spk_to_spk2utt.pl < $dir/utt2spk >$dir/spk2utt ||\
    die "spk2utt file $dir/spk2utt cannot be created";

