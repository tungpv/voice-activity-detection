#!/usr/bin/env perl
use warnings;
use strict;
sub select_utt_with_segment {
  my ($spk2utt, $segments, $expected_num) = @_;
  my $selected = 0;
  foreach my $spk (keys %$spk2utt) {
    my $utts = $$spk2utt{$spk};
    my @A = split (/\s+/, $utts);
    for(my $i =0; $i< @A && $i < $expected_num; $i++) {
      my $utt_name = $A[$i];
      my $item  = $$segments{$utt_name};
      if (not defined ($item)) {
        die "unidentified utt name $utt_name in segments file\n";
      }
      if ( $item =~ m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/) {
        $selected += $4 - $3;
      } else {
        die "item $item is not normal in segments file";
      }
    }
  }
  $selected /= 3600;
  return $selected;
}

my $numArgs = scalar @ARGV;

if ($numArgs != 5) {
  die "\n\nUsage Example:\n$0  spk2utt segments  hour_length new_spk2utt new_segments\n$0 train/spk2utt train/segments  100.0 /dev/null /dev/null \n\n";
}

my ($src_spk2utt, $src_segments, $hour_len, $spk2utt, $segments) = @ARGV;

open (F, "$src_segments") or die "file $src_segments cannot open\n";
my %seg_vocab = ();  my $total_hlen = 0;
while(<F>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/ or next;
   $total_hlen += $4 -$3;
  if (exists $seg_vocab{$1}) {
    die "$_ in $src_segments is duplicated\n";
  }
  $seg_vocab{$1} = $_;
}
$total_hlen /= 3600;
if ($total_hlen <= $hour_len) {
  `cat $src_spk2utt > $spk2utt`;
  `cat $src_segments > $segments`
}
close F;
my $num_line = scalar (keys %seg_vocab);
if ($num_line <= 0) {
  die "empty segments file $src_segments\n";
}
my %spk2utt_vocab = ();
my $max_num = 0;
open (F, "$src_spk2utt") or die "file $src_spk2utt cannot open\n";
while (<F>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $spk2utt_vocab{$1} = $2; 
  my @A = split (/\s+/);
  if (@A -1 > $max_num) {
    $max_num = @A -1;
  }
}
close F;
print STDERR "\nmax_num=$max_num\n";

my $low = 0; my $high = $max_num;  
my $max_iter = int( log($max_num)/log(2) ); my $iter_index = 0; 
my $gap = 10000000; my $best_selected = 0;
while ($low <= $high) {
  my $mid_point = int( ($low + $high)/2);
  my $selected = select_utt_with_segment(\%spk2utt_vocab, \%seg_vocab, $mid_point); 
  print STDERR "## utts_per_speaker=$mid_point, selected_data_hour_length=$selected\n";
  if (abs($selected - $hour_len) < $gap) {
    $gap = abs($selected - $hour_len);
    $best_selected = $mid_point;
  }
  if ($selected > $hour_len) {
    $high = $mid_point - 1;
  } elsif ($selected < $hour_len) {
    $low = $mid_point + 1;
  } else {
    last;  ## rare case :-)
  }
  $iter_index ++;
}
## final selection deterministically
open (SPK2UTT, ">$spk2utt") or die "file $spk2utt cannot open\n";
open (SEG, ">$segments") or die "file $segments cannot open\n";
my $selected = 0;
foreach my $spk  (keys %spk2utt_vocab) {
  print SPK2UTT $spk2utt;
  my $utts = $spk2utt_vocab{$spk};
  my @A = split (/\s+/, $utts);
  for(my $i = 0; $i < @A && $i < $best_selected; $i ++) {
    my $utt_name = $A[$i];
    print SPK2UTT " $utt_name";
    my $seg_entry = $seg_vocab{$utt_name};
    if (not defined($seg_entry)) {
      die "$seg_entry is not in $src_segments file \n";
    }
    print SEG $seg_entry, "\n";
    $seg_entry =~ m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/;
    $selected += $4 -$3;
  }
  print SPK2UTT "\n";
}
$selected /= 3600;
print STDERR "\n## finally utts_per_speaker is $best_selected, total hour length is $selected\n";
print "$best_selected";
close SPK2UTT;
close SEG;
