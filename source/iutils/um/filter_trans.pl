#!/usr/bin/perl -w
use strict;

sub LoadVocab {
  my ($vocab, $fname) = @_;
  my $fp;
  open ($fp, "<", "$fname") 
  or die "input file $fname cannot open";
  while (<$fp>) {
    chomp;
    m/(^\S+)/ || next;
    $$vocab{$1} = 0;
  }
  close ($fp);
}

# ------ main entrance point ----
my ($fname) = @ARGV;
my %vocab = ();
LoadVocab ( \%vocab, $fname );
while (<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ || next;
  my ($lab, $str) = ($1, $2);
  next if not exists $vocab{$lab};
  print "$_\n";
}

