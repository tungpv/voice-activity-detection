#!/usr/bin/perl -w
use strict;

scalar @ARGV == 2 ||
die "\n\nUsage:\n duplicate_labels.pl ntime( >1) kaldi-scp-data\n",
    "Example: duplicate_label.pl 3 text\n\n";

my ($ntime, $fname) = @ARGV;

open FILE, "$fname" || die "cannot open file $fname";
while (<FILE>) {
  chomp;
  m/(\S+)\s+(.*)$/||next;
  my ($labname, $others)= ($1, $2);
  print "$_\n";
  my $idx = 1;
  my $n = $ntime; 
  $n --;
  while ($n > 0) {
    my $dup_labname = sprintf "%s_d%d", $labname, $idx;
    $n --;
    $idx ++;
    print "$dup_labname $others\n";
  }  
}
close FILE;
