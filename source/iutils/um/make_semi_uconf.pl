#!/usr/bin/perl -w
use strict;

scalar @ARGV == 2 or 
die "\n\nUsage:\nmake_semi_uconf.pl <unsup_confFile> <sup_seg>\n\n";

my ($uconfName, $supSegName ) = @ARGV;
open FILE, "$uconfName" or
die "file $uconfName cannot open";
while (<FILE>) {
 print "$_";
}
close FILE;
open FILE, "$supSegName";
while (<FILE>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($lab) = ($1);
  print "$lab 1.00\n";
}
close FILE;
