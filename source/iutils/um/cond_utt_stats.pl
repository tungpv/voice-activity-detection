#!/usr/bin/perl -w 
use strict;

scalar @ARGV == 5 ||
die "\n\nselect automatically labelled utterances that\n",
    "are satisfied with a specific accuracy and length\n",
    "requirement.\n\n",
    "Example: cond_utt_stats.pl acc_thres utt_length_thres reorg_hresults_output_file input-segments
output-segments\n\n";

my ($acc_thres, $utt_length_thres,$hresult_fname, $inSeg, $outSeg) = @ARGV;

open HRES, "<$hresult_fname" || die "file $hresult_fname cannot open";

my %vocab = ();
while (<HRES>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\d+)\s+(.*)\s+(\d+)$/|| next;
  my ($labname, $acc, $uLen) = ($1, $2, $5);
  $acc =~ /^\d+/  || next;
  if ($acc >= $acc_thres && $uLen >= $utt_length_thres) {
    $vocab{$labname} = 0;
  }
}
open INSEG, "<$inSeg" || die "input segments file $inSeg cannot open";
open OUTSEG, ">$outSeg"|| die "output segments file $outSeg cannot open";
my ($totFiles, $totLen) = (0, 0);
while (<INSEG>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)$/|| next;
  my ($labname, $srcName, $start, $end) = ($1, $2, $3, $4);
  if (exists $vocab{$labname}) {
    print OUTSEG "$_\n";
    $totFiles ++;
    $totLen += $end - $start;
  }
}
$totLen /= 3600;
my $sTotLength = sprintf "%.2f", $totLen;
print "$acc_thres $utt_length_thres $totFiles $sTotLength\n";
close OUTSEG;
close INSEG;
close HRES;


