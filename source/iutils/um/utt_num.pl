#!/usr/bin/perl -w
use strict;
use lib '/usr/local/lib/perl5/5.8.9';
use Getopt::Long;

my $exclude = "";
my $include = "";
my $segment = "";
my $hlen = 10000000000; # default should be all data
my $def_hlen = $hlen;
my $upost_thresh = 0;
my $upost_list = "";
my $utt2spk = "";
my $destdir = "";
my $fullspk = 0;
sub Usage() {
  print "\nUsage:\nutt_num.pl [options]\n";
  exit -1;
}
sub FillVocab {
  my ($fname, $vocab) = @_;
  open (my $file, "<", $fname) or 
  die "$fname cannot open";
  while (<$file>) {
    chomp;
    m/(\S+)/ || next;
    my $word = $1;
    $$vocab{$word} = 0;
  }
  close ($file);
}
#
sub FillVocab2 {
  my ($fname, $vocab ) = @_;
  open (my $file, "<", $fname ) or
  die "$fname cannot open";
  while (<$file>) {
    m/(\S+)\s+(\S+)$/ or next;
    my ($lab, $score) = ($1, $2);
    $$vocab{$lab} = $score;
  }
  close ($file);
}
sub FillSegVocab {
  my ($segment, $vocab) = @_;
  open(SEG, "$segment") or die "file $segment cannot open \n";
  while (<SEG>) {
    chomp;
    m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)$/ || next;
    my ($seg, $file, $start, $end) = ($1, $2, $3, $4);
    $$vocab{$seg} = $4 - $3;
  }
  close SEG;
}
##
sub FillSpkVocab {
  my ($utt2spk, $vocab, $segVocab) = @_;
  open F, "$utt2spk" or die "file $utt2spk cannot open\n";
  while (<F>) {
    chomp;
    m/(\S+)\s+(\S+)/ or next;
    my ($utt, $spk) = ($1, $2);
    if (exists $$segVocab{$utt}) {
      $$vocab{$spk} += $$segVocab{$utt};
    } else {
      print STDERR "WARNING: utterance $utt has no time info\n";
    }
  }
  close F;
}
##
sub FullSpkrSelection { 
  my ($segment, $utt2spk, $hlen, $destdir) = @_;
  my %segVocab = ();
  FillSegVocab($segment, \%segVocab);
  my %spkVocab = ();
  FillSpkVocab($utt2spk, \%spkVocab, \%segVocab);  
  my $secLen = $hlen*3600; my $selTotal = 0; my $totSpk = 0;
  my %selSpkVocab = ();
  for my $spk (sort {$spkVocab{$b} <=>$spkVocab{$a} } keys %spkVocab ) {
    if ($selTotal <= $secLen ) {
      $selTotal += $spkVocab{$spk}; ## print STDERR "$spk $spkVocab{$spk}\n";
      $selSpkVocab{$spk} = $spkVocab{$spk};
      $totSpk ++;
    }
  }
  my $str = sprintf "%.4f", $selTotal/3600;
  print STDERR "\n## total_spk_selected=$totSpk, total_data_hour_selected=$str\n" ;
  open F, "$utt2spk" or die "file $utt2spk cannot  open\n";
  if (length $destdir > 0) {
   open DF, ">$destdir/utt2spk" or die "file $destdir/utt2spk cannot open\n";
  }
  while (<F>) {
    chomp;
    m/(\S+)\s+(\S+)/ or next;
    if (exists $selSpkVocab{$2}) {
      print STDOUT "$_\n"  if length $destdir <=0;
      print DF "$_\n" if length $destdir > 0;
    }
  }
  close F;
  close DF if length $destdir > 0;
}

print STDERR "\n## LOG: utt_num.pl ", join " ", @ARGV, "\n";
#
if (scalar @ARGV <= 0) {
  print "\n\nUsage:\nutt_num.pl --exclude file", "\n--segment segment", 
        "--hlen hour-length\n --utt2spk utt2spk \n",
        "--upost-list upost_list_file \n",
        "--upost-thresh post_thresh \n",
        "--include file\n", "--fullspk\n",
        "--destdir dir\n\n";
  exit 1;
}
GetOptions ("exclude=s"=>\$exclude,
            "include=s"=>\$include,
            "segment=s"=>\$segment,
            "hlen=f"=>\$hlen,
            "upost-thresh=f"=>\$upost_thresh,
            "upost-list=s"=>\$upost_list,
            "utt2spk=s"=>\$utt2spk,
            "fullspk=i"=>\$fullspk,
            "destdir=s"=>\$destdir 
            ) or Usage();

$hlen > 0 || die "hlen is illegal ($hlen)";
$segment ne "" || die "segment file must be specified";
my %vocab = ();
if ($exclude ne "") {
  FillVocab($exclude, \%vocab); 
}
my %inv_vocab = ();
if ($include ne "") {
  FillVocab ($include, \%inv_vocab);
}
my %uconf_vocab = ();
if ($upost_list ne "") {
  FillVocab2($upost_list, \%uconf_vocab);
}
my ($SSEG, $DSEG, $DUPOST);
if ($destdir ne "") {
  open ($DSEG, ">", "$destdir/segments") || die "cannot open $destdir/segments";
  if ($upost_list ne "") {
    open ($DUPOST, ">", "$destdir/uconf.txt") or
    die "cannot open $destdir/uconfi.txt as output";
  }
}

my $totlen = 0;
my $objlen = $hlen*3600;
my $utt_num = 0;
my %sel_vocab = ();
if ($fullspk ==1 && $utt2spk ne "" && $hlen < $def_hlen ) {
  FullSpkrSelection($segment, $utt2spk, $hlen, $destdir);
} else {
  open ($SSEG, "<", $segment) || die "file $segment cannot open";
  while (<$SSEG>) {
    chomp;
    m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)$/ || next;
    my ($seg, $file, $start, $end) = ($1, $2, $3, $4);
  
    next if $exclude ne "" && exists $vocab{$seg};
    next if $include ne "" && not exists $inv_vocab{$seg};
    my $score = 0;
    if (exists $uconf_vocab{$seg}) {  # to threshold
      $score = $uconf_vocab{$seg};
      $score >= $upost_thresh || next;
    }
    $sel_vocab{$seg} = 0;
    if ($destdir ne "") {
      print $DSEG "$_\n";
      if ($upost_list ne "") {
        print $DUPOST "$seg $score\n";     
      }
    }
    $totlen += $end - $start;
    $utt_num ++;
    $totlen < $objlen || last;
  }
  close ($SSEG);
  print STDERR "\n$utt_num utterances selected with ", $totlen/3600, " hours\n";
  if ($destdir ne "") {
    close ($DSEG);
    if ($upost_list ne "") {
      close ($DUPOST);
    }
  }

  if ($utt2spk ne "" && $destdir ne "") {
    open (my $SFile, $utt2spk) or die "file $utt2spk cannot open";
    open (my $DFile, ">", "$destdir/utt2spk") || die "file destdir $destdir/utt2spk cannot open";
    while (<$SFile>) {
      chomp;
      m/(\S+)\s+(\S+)/ ||next;
      my ($lab, $spk) = ($1,$2);
      if (exists $sel_vocab{$lab}) {
        print $DFile "$_\n";
      }
    }
    close ($SFile);
    close ($DFile);
  }
}
