#!/usr/bin/perl -w
use strict;

print STDERR "input: make_merged_uconf.pl ",
      join " ", @ARGV, "\n";

scalar @ARGV == 2 or
die "\n\nUsage:\nmake_merged_uconf.pl \n",
    "\t<sup_seg> # segments for the supervised data\n",
    "\t<unsup utt. conf> # utterance confidence list for unsupervised data\n\n\n";

my ($supSeg, $unsupConf) = @ARGV;

my %vocab = ();
open FILE, "$supSeg" or
die "file $supSeg cannot open";
while (<FILE>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($lab) = ($1);
  $vocab{$lab} = 0;
  print "$lab 1.0\n";
}
close FILE;
open FILE ,"$unsupConf" or 
die "file $unsupConf cannot open";
while (<FILE>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($lab, $conf) = ($1, $2);
  next if exists $vocab{$lab};
  my $p = sprintf "%.2f", $conf;
  print "$lab $p\n";
}
close FILE; 
