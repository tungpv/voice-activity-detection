#!/usr/bin/perl -w
use strict;
print "your input: ", join " ", @ARGV, "\n";
scalar @ARGV ==3 or die "\n\nUsage: make_unsup_seg.pl\n",
"\t<semi_seg> # segments selected from a threshold\n",
"\t<sup_seg>  # supervised segment that should be removed from the semi_seg\n",
"\t<unsup_seg> # the remaining segment from unsupervised data set\n\n";

my ($semiSeg, $supSeg, $unsupSeg) = @ARGV;
my %vocab = ();
open FILE, "$supSeg" or
die "file $supSeg cannot open";
while (<FILE>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($lab) = ($1);
  $vocab{$lab} = 0;
}
close FILE;

open FILE, "$semiSeg" or die "file $semiSeg cannot open";
open OFILE, ">$unsupSeg" or 
die "file $unsupSeg cannot open";
while (<FILE>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($lab, $rest) = ($1, $2);
  next if exists $vocab{$lab};
  print OFILE "$_\n";
}
close OFILE;
close FILE;

