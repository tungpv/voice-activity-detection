#!/usr/bin/perl
use strict;

scalar @ARGV == 2 || 
die "\n\nExample: iutils/filter_hresults.pl hresults_output reorganized_output\n\n";

my ($iname, $oname) = @ARGV;

open (FILE, "<$iname")|| die "input file $iname cannot open";
open (OFILE, ">$oname") || die "output file $oname cannot open";

while (<FILE>) {
  chomp;
  /^---------/ && next;
  /^SENT:/ && next;
  /^WORD:/ && next;
  /^=======/ && next;
  s/\.rec://g;
  s/\(.*\)//g;
  s/[\[\]]//g;
  s/\,/ /g;
  s/[HDSIN]=//g;
  print OFILE "$_\n";
}

close OFILE;
close FILE;
