#!/usr/bin/perl -w
use strict;

scalar @ARGV == 3 ||
die "\n\nUsage:\n make_stm_file segments-file reco2channel-file kaldi-text-file\n\n";

my ($segments, $reco, $text) = @ARGV;

sub LoadText {
  my ($text, $vocab) = @_;
  open TEXT, "$text";
  while (<TEXT>) {
    chomp;
    m/(\S+)\s+(.*)/|| next;
    my ($lab, $str) = ($1, $2);
    $$vocab{$1} = $2;
  }
  close TEXT;
}
#
sub LoadReco {
  my ($reco, $vocab) = @_;
  open RECO, "$reco";
  while (<RECO>) {
    chomp;
    m/(\S+)\s+(.*)/ || next;
    my ($fname, $chinfo) = ($1, $2);
    $$vocab{$fname} = $chinfo;
  }
  close RECO;
}

sub CheckSegments {
  my ($segments, $srcVocab, $destVocab) = @_;
  open SEG, "$segments";
  while (<SEG>) {
    chomp;
    m/(\S+)\s+(\S+)\s+(.*)/ || next;
    my ($lab, $fname, $tinfo) = ($1, $2, $3);
    if (exists $$srcVocab{$lab}) {
      $$destVocab{$fname} = 0;
    }
  }
  close SEG;
}

# ------------- main entrance function ----
my (%text_vocab, %reco_vocab, %fname_vocab) = ();
LoadText ($text, \%text_vocab);
LoadRec ($reco, \%reco_vocab);
CheckSegments ($segments, \%text_vocab, \%fname_vocab);

open (SEG, "<$segments");
while (<SEG>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(.*)/ || next;
  my ($lab, $fname, $tinfo) = ($1, $2, $3);
  if (exists $reco_vocab{$fname}) {
    my $hstr = $reco_vocab{$fname}." Aggregated ". " $tinfo";
    if (exists $text_vocab{$lab}) {
      $hstr .= " $text_vocab{$lab}";
    }
    print "$hstr\n"; 
  }
}
close SEG;
