#!/usr/bin/perl -w
use strict;

scalar @ARGV == 2 || die "\nUsage:\n filter_scp.pl utt_list spk2utt\n\n";

my ($uttlist, $spk2utt) = @ARGV;

my %vocab = ();

sub FillVocab {
  my ($fname, $vocab) = @_;
  open FILE,"<", "$fname" or die "utterance list file $fname cannot open";
  while (<FILE>) {
    chomp;
    m/(\S+)\s+(.*)/ || next;
    my $uttlab = $1;
    $$vocab{$uttlab} = 0;
  }
  close FILE;
}
FillVocab ($uttlist, \%vocab);
open FILE, "<$spk2utt"|| die "spk2utt file $spk2utt cannot open";
while (<FILE>) {
  chomp;
  /^$/ && next;
  my @a = split(" ", $_);
  my $spk = shift @a;
  scalar @a > 0 || die "abnormal spk2utt file $_";
  my @b =();
  push @b, $spk;
  for(my $idx = 0; $idx <= $#a; $idx ++) {
    my $labname = $a[$idx];
    if(exists $vocab{$labname}) {
      push @b, $labname;
    }
  }
  if (scalar @b > 1) {
    print join " ", @b, "\n";
  }
}
close FILE;
