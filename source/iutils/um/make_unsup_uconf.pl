#!/usr/bin/perl -w 
use strict;

print "\nyour input: mk_unsup_uconf_seg.pl ", join " ", @ARGV, "\n";
scalar @ARGV ==4 or die
"\n\nUsage:mk_unsup_uconf_seg.pl\n\t<utterance-confidence-file> # obtained from lattice forward-backward algorithm\n",
"\t<supervised-data-segment-file> # segments from corresponding kaldi data in supervised data set\n",
"\t<threshold> # threshold used to select utterance from ASR transcribed data\n",
"\t<unsupervised utt confidence list output> # confidence list from unsupervised ata\n\n";

my ($uttConfFName, $supSeg,
   $thresh,$unsupUttConf) = @ARGV;
my %vocab = ();
open FILE, "$supSeg" or die "file $supSeg cannot open";
while (<FILE>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($lab) = ($1);
  $vocab{$lab} = 0;

}
close FILE;
# make confidence list from unsupervised 
# utterance list
open FILE, "$uttConfFName" or 
die "file $uttConfFName cannot open";
open OFILE, ">$unsupUttConf" or 
die "file $unsupUttConf cannot open";
while (<FILE>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  my ($lab, $conf) = ($1, $2);
  next if ( exists $vocab{$lab} or $conf < $thresh);
  print OFILE "$_\n"; 
}
close FILE;
close OFILE;
