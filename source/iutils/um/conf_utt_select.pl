#!/usr/bin/perl -w
use strict;
sub LoadVocab {
  my ($vocab, $fName) = @_;
  open (FILE, "$fName") 
  or die "File $fName cannot open";
  while (<FILE>) {
    chomp;
    m/(\S+)\s+(\S+)/ || next;
    my ($lab, $uconf) = ($1, $2);
    $$vocab{$lab} = $uconf;
  }
  close FILE;
}
#
sub LoadLenVocab {
  my ($vocab, $fName) = @_;
  open (FILE, "$fName")
  or die "File $fName cannot open";
  while (<FILE>) {
    chomp;
    m/(\S+)\s+(.*)$/|| next;
    my ($lab, $utt) = ($1, $2);
    my @a = split /\s+/, $utt;
    $$vocab{$lab} = scalar @a;
  }
  close FILE;
}
# ------------- main entrance point -----
my $argNum = scalar @ARGV;
my $idx = 0;
my $thresh = 0;
my $hithresh=1.2;
my $selected = "";
my $utt_len = "";
my $lengThresh = 0;
while ($idx < $argNum) {
  my $swStr = $ARGV[0];
  if ($swStr =~ /--thresh/) {
    shift @ARGV; $idx ++;
    $thresh = $ARGV[0]; shift @ARGV; $idx ++;
    next;
  }
  if ($swStr =~ /--hithresh/) {
    shift @ARGV; $idx ++;
    $hithresh = $ARGV[0]; shift @ARGV; $idx ++;
    next;
  }
  if ($swStr =~ /--selected/) {
    shift @ARGV; $idx ++;
    $selected = $ARGV[0]; shift @ARGV; $idx ++;
    next;
  }
  if ($swStr =~ /--utt-length/) {
    shift @ARGV; $idx ++;
    $utt_len = $ARGV[0]; shift @ARGV; $idx ++;
    $lengThresh = $ARGV[0]; shift @ARGV; $idx ++;
    next;
  }
  last;
}
#
print STDERR "INPUT= $0 ", join " ", @ARGV, "\n";
scalar @ARGV == 2 || die "\n\nUsage:\n",
"conf_utt_select.pl --thresh thresh\n",
"--selected outputSegFile\n",
"--utt-length utteranceLengthFile lengThresh\n",
"utt-conf-file utt-seg-file\n\n";
my ($uconfFile, $segFile) = @ARGV;

my %vocab = ();
LoadVocab (\%vocab, $uconfFile);
my %len_vocab = ();
if (length($utt_len) != 0) {
  LoadLenVocab (\%len_vocab, $utt_len);
}
my $selFile;
if (length ($selected) > 0 ) {
  open ($selFile, ">$selected")
  or die "File $selFile cannot open"; 
}
open (FILE, "$segFile") 
or die "segments $segFile cannot open";
my ($totHLen, $totUtts)= (0, 0);
while (<FILE>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/|| next;
  my ($lab, $fName, $start, $end) = ($1, $2, $3, $4);
  if (exists $vocab{$lab}) {
    my $uConf = $vocab{$lab};
    $uConf >= $thresh && $uConf <=$hithresh  or next;
    if (length $utt_len > 0 ) {
      if ( exists $len_vocab{$lab} ) {
        my $curUttLen = $len_vocab{$lab};
        $curUttLen >= $lengThresh || next;
      }
    }
    $totHLen += $end - $start;
    $totUtts ++;
    if (length $selected) {
      print $selFile "$_\n";
    }
    printf "%s\t%.4f\n",$lab, $uConf;
  }
}
$totHLen /= 3600;
my $len = sprintf "%.2f",$totHLen;
print STDERR "selected: $thresh $lengThresh $totUtts $len\n";
close (FILE);
length $selected == 0 || close $selFile;
