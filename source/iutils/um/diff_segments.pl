#!/usr/bin/perl -w
use strict;
print STDERR "\ninput: diff_segments.pl ", join " ", @ARGV, "\n";
scalar @ARGV == 2 or
die "\nUsage: diff_segments.pl <segments1> <segments2>\n";
my ($segName1, $segName2) = @ARGV;
sub LoadVocab {
  my ($vocab, $fname) = @_;
  open FILE, "$fname" || die "file $fname cannot open";
  while (<FILE>) {
    chomp;
    m/(\S+)\s+(.*)/ || next;
    my ($lab) =($1);
    $$vocab{$lab} = 0;
  }
  close FILE;
}
my %vocab = ();
LoadVocab (\%vocab, $segName1);
open FILE, "$segName2";
while (<FILE>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($lab) = ($1);
  next if exists $vocab{$lab}; 
  print "$_\n";
}
close FILE;
