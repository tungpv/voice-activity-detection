#!/usr/bin/perl -w
use strict;

my ($fname) = @ARGV;

open FILE, "$fname|";
my %vocab = ();
while (<FILE>) {
  chomp;
  my @a = split /\s+/;  
  my $nField = scalar @a;
  # print "nField=$nField\n";
  my $lab = shift @a;
  if (/^BABEL_/ && ($nField == 7 || $nField == 2)) {
    if (exists $vocab{$lab}) { 
      my $a1 = $vocab{$lab};
      if (scalar @a == 6) {
        print "$$a1[0] $a[0] $a[5]\n";
      } else {
        print "$$a1[0] $$a1[1] $a[0]\n";  
      }
    } else {
      if (scalar @a == 1) {
        $vocab{$lab} = \@a;
      } else {
        my @a2 = ($a[0], $a[5]);
        $vocab{$lab} = \@a2;
      }
    }
  }
}

close FILE;
