#!/usr/bin/perl -w
use strict;
sub LoadVocab {
  my ($vocab, $fname) = @_;
  open FILE, "$fname"
  or die "file $fname cannot open";
  while (<FILE>) {
    chomp;
    my @a = split /\s+/;
    # print STDERR "a_size=", scalar @a, "\n";
    scalar @a >= 7 || next;
    my ($lab, $acc) = ($a[0], $a[1]);
    $$vocab{$lab} = $acc;
  }
  close FILE;
}


# ------------- main entrance point -----
my $argNum = scalar @ARGV;
my $idx = 0;
my $acc_thresh = 0;
while ($idx < $argNum) {
  my $swStr = $ARGV[0];
  if ($swStr =~ /--wer/) {
    shift @ARGV; $idx ++;
    $acc_thresh = 100 - $ARGV[0]; shift @ARGV; $idx ++;
    next;
  }
  last;
}
#
scalar @ARGV == 3 || die "\n\nUsage:\n",
"wer_utt_select.pl --wer <wer_number>\n",
"<segment file> # segment file in kaldi data folder\n",
"<acc file> # re-organized accuracy info list, obtained from hresults command in htk\n",
"<selected segments> # selected segment file list\n",
"# in which utterance WER is lower than threshold\n\n";
my ($source_seg, $source_acc, $dest_seg) = @ARGV;

my %vocab = ();
LoadVocab(\%vocab, $source_acc);

open FILE, "$source_seg"
or die "file $source_seg cannot open";
my ($totHour, $totUtt) = (0, 0);
while (<FILE>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/ || next;
  my ($lab, $start, $end) = ($1, $3, $4);
  if (exists $vocab{$lab}) {
    my $acc = $vocab{$lab};
    if ($acc >= $acc_thresh ) {
      $totHour += $end - $start;
      $totUtt ++;
      print "$_\n";
    }
  }
}
$totHour /= 3600;
$totHour = sprintf "%.2f", $totHour;
my $wer = 100 - $acc_thresh;
print STDERR "$wer $totHour $totUtt\n";
close FILE;

