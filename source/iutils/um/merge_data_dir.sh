#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

export LC_ALL=C

if [ $# -ne 3 ]; then
  echo -e "\n\n";
  echo  "Usage: $0 <sdir1> <sdir2> <dir>";
  echo -e "\n\n"; 
  exit 1;
fi

sdir1=$1; shift;
sdir2=$1;shift;
dir=$1; shift;

[ -d $dir ] || mkdir -p $dir
rm $dir/* 2>/dev/null
for x in text wav.scp utt2spk feats.scp segments reco2file_and_channel cmvn.scp; do
  if [ -e $sdir1/$x  -a  -e $sdir2/$x ]; then
    s_num=`wc -l < $sdir1/$x`
    u_num=`wc -l < $sdir2/$x` 
    cat $sdir1/$x $sdir2/$x | sort -u > $dir/$x
    num=`wc -l < $dir/$x` 
    [ $[$s_num+$u_num] -eq $num ] || \
    echo -e "WARNING:two data $sdir1/$x and $sdir2/$x sets are not disjoint: $s_num + $u_num != $num";
  fi
done

rm  $dir/spk2utt 2>/dev/null
    utils/utt2spk_to_spk2utt.pl < $dir/utt2spk >$dir/spk2utt ||\
    die "spk2utt file $dir/spk2utt cannot be created";

utils/fix_data_dir.sh $dir || exit 1;
