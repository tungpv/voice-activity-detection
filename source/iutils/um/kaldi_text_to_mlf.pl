#!/usr/bin/perl -w
use strict;

scalar @ARGV == 3 || die "\n\nusage example:kaldi_text_to_mlf.pl text rec/lab my.mlf\n\n";

my ($tname, $ename, $oname) = @ARGV;

open MLF, ">$oname" || die "mlf file $oname cannot open";
print MLF "#!MLF!#\n";
open TEXT, "<$tname" || die "text file $tname cannot open";

while (<TEXT>) {
  chomp;
  /^$/ && next;
  m/(\S+)\s+(.*)$/|| next;
  my ($lab, $wseq) = ($1, $2);
  my $mlf_lab = sprintf ("\"*/%s.%s\"", $lab, $ename);
  print MLF "$mlf_lab\n";
  my @array = split ' ', $wseq;
  for (my $idx = 0; $idx < scalar @array; $idx ++) {
    my $wstr = $array[$idx];
    my $str = $wstr;
    if ($str =~ /^\d/) {
      $str = sprintf "NUM_%s",$wstr;
    }
    print MLF $str, "\n";
  }
  print MLF ".\n";
}

close TEXT;
close MLF;

