#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

export LC_ALL=C

# begin user define variable
ntimes=1;
# end user defined variable

. utils/parse_options.sh || die "utils/parse_options.sh needed";

[ $ntimes -gt 1 ] || die "ntimes should be over zero, while what you provided is $ntimes ";

if [ $# != 2 ]; then
  echo -e "\n\n Duplicate a data set to its n times larger";
  echo "Example: duplicate_data_set.sh --ntimes 2 source-data-dir dir";
  echo -e "\n\n";
  exit 1;
fi
sdir=$1;shift;
dir=$1; shift;

[ -e $sdir/text ] || die "source data dir $sdir is not ready";
mkdir -p $dir 2> /dev/null
for x in text wav.scp utt2spk feats.scp segments; do
  [[ -e  $sdir/$x ]] || continue;
  [[ ! -e $dir/$x ]] || rm -f $dir/$x
  iutils/um/duplicate_labels.pl $ntimes $sdir/$x | sort -u > $dir/$x
done
# just copy the following files
for x in reco2file_and_channel cmvn.scp; do
  [[ -e $sdir/$x ]] || continue;
  cp $sdir/$x $dir/$x || die "copy file error";
done
utils/utt2spk_to_spk2utt.pl < $dir/utt2spk >$dir/spk2utt

echo -e "dup data $sdir $ntimes times larger into new folder $dir";
