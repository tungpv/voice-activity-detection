#!/usr/bin/perl -w
use strict;

scalar @ARGV == 6 ||
die "\n\nSelect utterance list with utterance confidence score threshold\n",
    "and utterance word number threshold. Thus utterance confidence file\n",
    "should be prepared for this, and the output is \n",
    "selected_seg_file_list\n",
    "\nUsage example: select_utt_with_ulen_conf.pl \\\n",
    "utt_conf_thresh  utt_word_number_thresh \\\n",
    "utterance_conf_score_list  source_data_segments \\\n",
    "source_data_text_file selected_seg_file_list\n\n";
# print "ARGV=", join " ", @ARGV, "\n";
my ($conf_thresh, $word_num_thresh, $conf_list_fname, 
    $source_segment_fname, $source_text, $output_segment_fname) = @ARGV;
sub LoadSourceText {
  my ($fname, $vocab) = @_;
  open FILE, "$fname";
  while (<FILE>) {
    chomp;
    m/(\S+)\s+(.*)/ || next;
    my ($labname, $ustr) = ($1, $2);
    my @array = split ' ', $ustr;
    $$vocab{$labname} = scalar @array;
  }
  close FILE;
}
#
sub LoadConfList {
  my ($fname, $vocab) = @_;
  open (FILE, "$fname");
  while (<FILE>) {
    chomp;
    m/(\S+)\s+(\S+)/ || next;
    my($labname, $conf) = ($1, $2);
    $$vocab{$labname} = $conf;
  }
  close FILE;
}
#
my %text_vocab = ();
LoadSourceText ($source_text, \%text_vocab);
my %conf_vocab = ();
LoadConfList ($conf_list_fname, \%conf_vocab);
my $totUtt = 0; my $totLen = 0;
open SEG, ">$output_segment_fname";
open FILE, "$source_segment_fname";
while (<FILE>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/ || next;
  my ($labname, $fname, $start, $end) = ($1, $2, $3, $4);
  if (exists $text_vocab{$labname} &&
      exists $conf_vocab{$labname}) {
    my $utt_len = $text_vocab{$labname};
    my $conf = $conf_vocab{$labname};
    if ($utt_len >= $word_num_thresh &&
        $conf >= $conf_thresh) {
       print SEG "$_\n";
       $totUtt ++;
       $totLen += $end - $start;
    }
  } else {
    print STDERR "WARNING: $labname is oov in text or conf file\n";
  }
}
my $x = sprintf "%.2f", $totLen/3600;
print "$conf_thresh $word_num_thresh $totUtt $x\n";
close FILE;
close SEG;
