#!/usr/bin/env perl

use strict;
use warnings;
# Example: ./iutils/show_atwv.pl --pipe "ls icassp/exp/tandem_semi/nnet5a/*/index/*/kws/sum.txt"  5 7 
my $pipe = 0;
for(my $idx = 0; $idx <scalar @ARGV; $idx ++) {
  my $arg_str = $ARGV[$idx];
  if ($arg_str eq "--pipe") {
    splice @ARGV, $idx, 1;
    $pipe = 1;
    print STDERR "pipe input enabled\n";
  } 
}

scalar @ARGV == 3 or  
    die "\n\nUsage: $0 [--pipe] <nist sum.txt file list pipe> 
        <pos_scale4lat_in_dirname> <pos_scale4post_in_dirname>\n\n";

my($fileList, $pos1, $pos2) = @ARGV;
if ($pipe == 0) {
  print STDERR "normal file list is provided\n";
}

$fileList .= " |" if $pipe == 1;
print STDERR "\nfileList=$fileList, pos1=$pos1, pos2=$pos2\n\n";

# print "fileList=$fileList\n";
open (IN_FILE, "$fileList") or 
    die "pipe file $fileList cannot open";
my %vocab = ();
while (<IN_FILE>) {
  chomp;
  my $sumFile = $_;
  my @array_str = split /\//, $sumFile;
  my ($pos1_str, $pos2_str) = ($array_str[$pos1-1], $array_str[$pos2-1]);
  open (FILE, "<$sumFile") or die "file $sumFile cannot open";
  while (<FILE>) {
    chomp;
    s/[ ]+//g;
    if (/^\|Occurrence/) {
      s/^\|Occurrence\|//g;
      @array_str = split /[\|]/;
      # print "$array_str[10], $array_str[14]\n";
      my @array = ();
      $vocab{$pos1_str}{$pos2_str} = \@array;
      my $ref = $vocab{$pos1_str}{$pos2_str};
      push @$ref, $array_str[10];
      push @$ref, $array_str[14];
    }   
  }
  close FILE; 
  # print "$array_str[$pos1-1], $array_str[$pos2-1]\n";
}
# dump file
for my $pos1 (keys %vocab) {
  my $href = $vocab{$pos1};
  print "$pos1: ";
  for my $pos2 (sort keys %$href) {
    my $aref = $$href{$pos2};
    print "$pos2 $$aref[0] $$aref[1], ";
  }
  print "\n";
}
close(IN_FILE);
