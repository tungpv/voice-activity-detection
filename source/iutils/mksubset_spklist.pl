#!/usr/bin/env perl
use warnings;
use strict;

print STDERR "$0 ", join " ", @ARGV, "\n"; 

my $numArgs = scalar @ARGV;
if ($numArgs != 3) {
  die "\nUsage: $0 <fraction-of-total-frames> <spk2utt> <feat-len-list>\n\n";
}
my ($fraction, $spk2utt, $pipe) = @ARGV;
die "fraction $fraction should be in (0,1)\n" if $fraction < 0 or $fraction > 1.0;

my %vocab = ();
open (F, "$pipe") || die "file $pipe cannot open\n";
my $total_frames = 0;
while (<F>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  my ($utt, $frames) = ($1, $2);
  $vocab{$utt} = $frames;
  $total_frames += $frames;
}
close F;
my $frames = $total_frames * $fraction;
open (F, "$spk2utt") or die "file $spk2utt cannot open\n";
my %spk2utt_vocab = ();
while (<F>) {
  chomp;
  my @A = split(/\s+/);
  my $spk = shift @A;
  $spk2utt_vocab{$spk} = \@A;
}
my $selected_frames = 0;
foreach my $spk (keys %spk2utt_vocab) {
  my $pa = $spk2utt_vocab{$spk};
  for(my $i = 0; $i < @$pa; $i++) {
    my $utt = $$pa[$i];
    if (not exists $vocab{$utt}) {
      print STDERR "utterance $utt has not feature \n";
      next;
    }
    my $frame_len = $vocab{$utt};
    $selected_frames += $frame_len; 
  }
  print $spk, "\n";
  last if ($selected_frames > $frames);
}

