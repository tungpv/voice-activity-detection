#!/usr/bin/perl -w
use strict;

print "\nicsi_oov_check.pl ", join " ", @ARGV, "\n";

my ($ecf, $index) = @ARGV;

my %vocab = ();
sub load_ecf {
  my ($fname, $vocab) = @_;
  open (my $FILE, "$fname");
  while (<$FILE>) {
    chomp;
    next if not  m/<[^=]+="(BABEL\S+)"/;
    my $lab = $1;
    $$vocab{$lab} = 0;
  }
  close $FILE;
}

load_ecf($ecf, \%vocab);

open (FILE, "$index")
or die "index $index cannot open";
while (<FILE>) {
  chomp;
  next if not m/(\S+)\s+(.*)/;
  my ($lab, $rest) = ($1, $2);
  print "$_\n" if not exists $vocab{$lab};
}

close FILE;
