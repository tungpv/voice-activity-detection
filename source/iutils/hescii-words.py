#!/usr/bin/env python
#
# Quick and dirty script to downcase and encode a slf with hescii.
#

import re
import sys
import hescii

for line in sys.stdin:
	dat = line.split()
	word = dat[0].lower()
	if not (re.match(r'[<\[]\S+[>\]]', word) or re.match(r'^\!', word)):
		dat[0] = '-'.join([hescii.dumps(x) for x in word.split('-')])
	print " ".join(dat)
