#!/usr/bin/env perl

use warnings;
use strict;

my $pipe = 0;
my $showAll = 0;

for(my $idx = 0; $idx <scalar @ARGV; $idx ++) {
  my $arg_str = $ARGV[$idx];
  if ($arg_str eq "--pipe") {
    splice @ARGV, $idx, 1;
    $pipe = 1; $idx --;
  }
  if ($arg_str eq "--showAll") {
    splice @ARGV, $idx, 1;
    $showAll = 1; $idx --;
  }
}

my $numArgs = scalar @ARGV;
die "\nUsage:\n$0 [--pipe] [--showAll] <nist sum.txt file list> $numArgs\n" if $numArgs !=1;
my ($fileList) = @ARGV;
$fileList .=" |" if $pipe == 1;

open (FLIST, "$fileList") or die "file $fileList cannot open\n";
my @bestPairs = ("", 0, 0);
my @allPairs = ();
while (<FLIST>) {
  chomp;
  my $file = $_;
  open FILE, "<$file" or die "file $file cannot open\n";
  while (<FILE>) {
    chomp;
    s/ //g;
    if (/^\|Occurrence/) {
      s/^\|Occurrence\|//g;
      my @array_str = split /[\|]/;
      # print "$array_str[10], $array_str[14]\n";
      my ($atwv, $mtwv) = ($array_str[10], $array_str[14]);
      my @array = ($file, $atwv, $mtwv);
      push @allPairs, \@array;
      @bestPairs = ($file, $atwv, $mtwv) if ($atwv > $bestPairs[1]);
    } 
  }
  close FILE;
}
close FLIST;
if ($showAll == 0 && length $bestPairs[0] > 0) {
  print join " ", @bestPairs, "\n";
} else {
  for(my $idx = 0; $idx < scalar @allPairs; $idx++) {
    my $pArray = $allPairs[$idx];
    print join " ", @$pArray, "\n";
  }
}


