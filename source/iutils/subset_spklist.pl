#!/usr/bin/env perl
use warnings;
use strict;

print STDERR "$0 ", join " ", @ARGV, "\n"; 

my $numArgs = scalar @ARGV;
if ($numArgs != 3) {
  die "\nUsage: $0 <validate-frames> <spk2utt> <feat-len-list>\n";
}
my ($frames, $spk2utt, $pipe) = @ARGV;
my %vocab = ();
open (F, "$pipe") || die "file $pipe cannot open\n";
while (<F>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  $vocab{$1} = $2;
}
close F;
open (F, "$spk2utt") or die "file $spk2utt cannot open\n";
my %spk2utt_vocab = ();
while (<F>) {
  chomp;
  my @A = split(/\s+/);
  my $spk = shift @A;
  $spk2utt_vocab{$spk} = \@A;
}
my $selected_frames = 0;
foreach my $spk (keys %spk2utt_vocab) {
  my $pa = $spk2utt_vocab{$spk};
  for(my $i = 0; $i < @$pa; $i++) {
    my $utt = $$pa[$i];
    if (not exists $vocab{$utt}) {
      print STDERR "utterance $utt has not feature \n";
      next;
    }
    my $frame_len = $vocab{$utt};
    $selected_frames += $frame_len; 
  }
  print $spk, "\n";
  last if ($selected_frames > $frames);
}

