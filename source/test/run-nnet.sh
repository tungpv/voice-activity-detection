#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd="run.pl"
nj=20
validating_rate=0.1
crossling_fmllr_train=false
train_trap_nnet=false
prepare_fmllr_test=false
prepare_fbank_data=false
prepare_fbank_test=false
crossling_fmllr_test=false
test_trap_nnet=false
pretrain_opts="--nn-depth 6 --hid-dim 2048 --splice 5"
prepare_feat=false
train_mfcc=false
train_fbank=false
train_fbank_trap=false
test_dev_data=false
steps=
mling_dnn_dir=
tgt_dir=
crossling_fbank_train=false
crossling_fbank_test=false
transfer_opts="--learn-rate 0.008 --hid-dim 2048 --remove-last-layers 2 --hid-layers 1"
preTrnCmd="isteps/nnet/pretrain_dbn_trap.sh --feat-type trap --apply-cmvn true --norm-vars true  --splice 5 --delta-order 2  --nn-depth 5  --hid-dim 2048"
fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
sequence_opts=
fbanktest_prepare_data=false
fbanktest_decode=false
sequence_opts=
hierbn_train=false
hierbn_hid_dim=1500
hierbn_hid_layers=2
hierbn_remove_last_layers=4
hierbn1_bn_dim=80
hierbn2_bn_dim=30
hierbn1_splice=5
hierbn2_splice=2
hierbn2_step=5
learn_rate=0.008
tune2_hierbn=false
tunebn_feature_transform=
fbank_tune_nnet=false
fbank_limited_tune_nnet=false
tune_learn_rate=0.002
tune_layers=1
fbank_cross_train_nnet=false
nnet_srcdir=

# end options
echo 
echo "$0 $@"
echo 

. parse_options.sh || exit 1

function Options {
  cat <<END

$0 [options]:
cmd                             # value, "$cmd"
nj		                # value, $nj
validating_rate		        # value, $validating_rate
crossling_fmllr_train           # value, $crossling_fmllr_train
train_trap_nnet                 # value, $train_trap_nnet
prepare_fmllr_test              # value, $prepare_fmllr_test
prepare_fbank_data              # value, $prepare_fbank_data
prepare_fbank_test              # value, $prepare_fbank_test
crossling_fmllr_test            # value, $crossling_fmllr_test
test_trap_nnet                  # value, $test_trap_nnet
pretrain_opts                   # value, "$pretrain_opts"
prepare_feat                    # value, $prepare_feat
train_mfcc                      # value, $train_mfcc
train_fbank                     # value, $train_fbank
train_fbank_trap                # value, $train_fbank_trap
test_dev_data                   # value, $test_dev_data
steps                           # value, "$steps"
mling_dnn_dir                   # value, "$mling_dnn_dir"
tgt_dir                         # value, "$tgt_dir"
crossling_fbank_train           # value, $crossling_fbank_train
crossling_fbank_test            # value, $crossling_fbank_test
transfer_opts                   # value, "$transfer_opts"
preTrnCmd                       # value, "$preTrnCmd"
fbankCmd              	        # value, "$fbankCmd"
fbanktest_prepare_data		# value, $fbanktest_prepare_data
fbanktest_decode		# value, $fbanktest_decode
sequence_opts          	        # value, "$sequence_opts"
hierbn_train			# value, $hierbn_train
hierbn_hid_dim    	        # value, $hierbn_hid_dim
hierbn_hid_layers		# value, $hierbn_hid_layers
hierbn_remove_last_layers       # value, $hierbn_remove_last_layers
hierbn1_bn_dim                  # value, $hierbn1_bn_dim
hierbn2_bn_dim                  # value, $hierbn2_bn_dim
hierbn1_splice                  # value, $hierbn1_splice
hierbn2_splice			# value, $hierbn2_splice
hierbn2_step 		        # value, $hierbn2_step		
learn_rate                      # value, $learn_rate
tune2_hierbn			# value, $tune2_hierbn
tunebn_feature_transform        # value, "$tunebn_feature_transform"
fbank_tune_nnet			# value, $fbank_tune_nnet
fbank_limited_tune_nnet		# value, $fbank_limited_tune_nnet
tune_learn_rate			# value, "$tune_learn_rate"
tune_layers			# value, $tune_layers
fbank_cross_train_nnet		# value, $fbank_cross_train_nnet
nnet_srcdir			# value, "$nnet_srcdir"
END
}

Options;
echo 
echo "$0 $@"
echo 
if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi
if [ $# -ne 4 ]; then
  echo "Usage: $0 <rdata> <alidata> <rexp> <sdir> ($#)"
  exit 1
fi

rdata=$1
alidata=$2
rexp=$3
sdir=$4
lang=$rdata/lang
sdir_name=$(dirname $sdir)

if $crossling_fmllr_train; then
  [ ! -z $mling_dnn_dir ] && \
  transfer_opts="$transfer_opts --mling-dnn-dir $mling_dnn_dir"
  [ -z $tgt_dir ] && \
  { echo "$0: crossling_fmllr_train: ERROR, tgt_dir expected"; exit 1; }
  [ -d $tgt_dir ] || mkdir -p $tgt_dir
  # cp $source_nnet_dir/{train_feat,validating_feat,train_label,validating_label} $dir/ || { echo "$0: train_fmllr_nnet: ERROR, copy data failed"; exit 1; }
  # cp $dir/train_feat $dir/train_fmllr
  # cp $dir/validating_feat  $dir/validating_fmllr
  # preTrnCmd="steps/nnet/pretrain_dbn.sh $pretrain_opts --copy_feats_tmproot $dir"
  source/run-nnet.sh  \
  ${step01:+--PrepareData true} ${step02:+--MakefMLLRFeat true} \
  --usefmllrdata true ${step03:+--DoAli true} \
  ${step04:+--PreTrain true }  --preTrnCmd "$preTrnCmd"\
  ${step05:+--TrainNnet true} --validating-rate $validating_rate \
  ${step06:+--crossling-transfer-train true --crossling-transfer-opts "$transfer_opts"} \
  --sdir $sdir $lang $alidata $tgt_dir || exit 1

fi

data=$rdata/dev/fmllr.$sdir_name
if $prepare_fmllr_test; then
  echo "$0: prepare_fmllr_test started @ `date`"
  sdata=$rdata/dev/plp_pitch
  featdir=$rexp/feature/fmllr.$sdir_name
  feat=$featdir/dev
  transform_dir=$sdir/decode_dev
  nj=$(cat $transform_dir/num_jobs)
  [ -z $nj -o $nj -eq 0 ] && { echo "$0: ERROR, nj is not given"; exit 1; }
  steps/nnet/make_fmllr_feats.sh --cmd "$train_cmd"  --transform-dir $transform_dir \
  --nj $nj $data $sdata $sdir   $feat/log $feat/data || exit 1
  steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1

fi
if $crossling_fmllr_test; then
  echo "$0: crossling_fmllr_test started @ `date`"
  graph=$sdir/graph
  final_nnet=$(find $tgt_dir/ -name "final.nnet")
  nnet_dir=$(dirname $final_nnet)
  decode_dir=$nnet_dir/decode_dev
  steps/nnet/decode.sh --nj 20 --cmd "$cmd" $graph $data $decode_dir || exit 1

fi

# sdir=$rexp/tri4a
if $crossling_fbank_train; then
  [ -z $tgt_dir ] && \
  { echo "$0: crossling_fbank_train: ERROR, tgt_dir expected"; exit 1; }
  [ -d $tgt_dir ] || mkdir -p $tgt_dir
  # preTrnCmd="steps/nnet/pretrain_dbn.sh --splice 5 --copy_feats_tmproot $tgt_dir"
  [ ! -z $mling_dnn_dir ] && \
  transfer_opts="$transfer_opts --mling-dnn-dir $mling_dnn_dir"
  source/run-nnet.sh --cmd "$cmd" --nj $nj \
  ${step01:+--PrepareData true} ${step02:+--MakeFBankFeat true --fbankCmd "$fbankCmd" } \
  --usefbankdata true ${step03:+--DoAli true} \
  ${step04:+--PreTrain true} --preTrnCmd "$preTrnCmd" \
  ${step05:+--TrainNnet true} --validating-rate $validating_rate \
  ${step06:+--crossling-transfer-train true --crossling-transfer-opts "$transfer_opts"} \
  ${step07:+--sequence-prepare-data true} \
  ${step08:+--sequence-train true} $sequence_opts \
  --sdir $sdir $lang $alidata $tgt_dir || exit 1
fi
if $fbank_cross_train_nnet; then
  echo "$0: fbank_cross_train_nnet started @ `date`"
  [ -z $tgt_dir ] && \
  { echo "$0: crossling_fbank_train: ERROR, tgt_dir expected"; exit 1; }
  [ -d $tgt_dir ] || mkdir -p $tgt_dir
  if [ ! -z $step04 ]; then
    [ ! -z $nnet_srcdir ] || \
    { echo "$0: fbank_cross_train_nnet: ERROR, nnet_srcdir not specified"; exit 1; }
  fi
  source/run-nnet.sh --cmd "$cmd" --nj $nj \
  --validating-rate $validating_rate --usefbankdata true  --sdir $sdir  \
  ${step01:+--PrepareData true} \
  ${step02:+--MakeFBankFeat true --fbankCmd "$fbankCmd" } \
  ${step03:+--DoAli true} \
  ${step04:+--cross-train-nnet true --nnet-srcdir $nnet_srcdir --learn-rate $learn_rate} \
  $lang $alidata $tgt_dir || exit 1
  echo "$0: fbank_cross_train_nnet ended @ `date`"
fi
if $fbank_tune_nnet; then
  echo "$0: fbank_tune_nnet started @ `date`"
  [ -z $tgt_dir ] && \
  { echo "$0: fbank_tune_nnet: ERROR, tgt_dir expected"; exit 1; }
  [ -d $tgt_dir ] || mkdir -p $tgt_dir
  [ -z $nnet_srcdir ] && \
  { echo "$0: fbank_tune_nnet: ERROR, nnet_srcdir not specified"; exit 1; }
  cp $nnet_srcdir/{train_fbank,train_feat,train_label,validating_fbank,validating_feat,validating_label}  $tgt_dir/
  source/run-nnet.sh --cmd "$cmd" --nj $nj \
  --validating-rate $validating_rate --usefbankdata true  --sdir $sdir  \
  ${step01:+--tune-nnet true --nnet-srcdir $nnet_srcdir --tune-learn-rate $tune_learn_rate --tune-layers $tune_layers } \
  $lang $alidata $tgt_dir || exit 1
  echo "$0: fbank_tune_nnet ended @ `date`"
fi
if $fbank_limited_tune_nnet; then
  echo "$0: fbank_limited_tune_nnet started @ `date`"
  [ -z $tgt_dir ] && \
  { echo "$0: fbank_limited_tune_nnet: ERROR, tgt_dir expected"; exit 1; }
  [ -d $tgt_dir ] || mkdir -p $tgt_dir
  [ -z $nnet_srcdir ] && \
  { echo "$0: fbank_limited_tune_nnet: ERROR, nnet_srcdir not specified"; exit 1; }
  cp $nnet_srcdir/{train_fbank,train_feat,train_label,validating_fbank,validating_feat,validating_label}  $tgt_dir/
  source/run-nnet.sh --cmd "$cmd" --nj $nj \
  --validating-rate $validating_rate --usefbankdata true  --sdir $sdir  \
  ${step01:+--limited-tune-nnet true --nnet-srcdir $nnet_srcdir --tune-learn-rate $tune_learn_rate --tune-layers $tune_layers } \
  $lang $alidata $tgt_dir || exit 1
  echo "$0: fbank_limited_tune_nnet ended @ `date`"
fi

if $hierbn_train; then
  echo "$0: hierbn_train started @ `date`"
  [ ! -z $tgt_dir ] || \
  { echo "$0: hierbn_train: ERROR, tgt_dir not specified"; exit 1; }
  hierbn_opts="--hid-dim $hierbn_hid_dim --copy_feats_tmproot $(pwd) --hid-layers $hierbn_hid_layers --learn-rate $learn_rate"
  hierbn1_opts="$hierbn_opts --apply-cmvn true --norm-vars true"
  hierbn1_opts="$hierbn1_opts --bn-dim $hierbn1_bn_dim --feat-type traps --splice $hierbn1_splice "
  hierbn1_cmd="steps/nnet/train.sh $hierbn1_opts"
  hierbn_splice_cmd="utils/nnet/gen_splice.py --fea-dim=$hierbn1_bn_dim --splice=$hierbn2_splice --splice-step=$hierbn2_step |"
  hierbn_copy_cmd="nnet-copy --remove-last-layers=$hierbn_remove_last_layers --binary=false"
  hierbn2_opts="$hierbn_opts --bn-dim $hierbn2_bn_dim "
  hierbn2_cmd="steps/nnet/train.sh $hierbn2_opts"
  source/run-nnet.sh --cmd "$cmd" --nj $nj --validating-rate $validating_rate \
  --sdir $sdir  --usefbankdata true \
  ${step01:+--PrepareData true} \
  ${step02:+--MakeFBankFeat true --fbankCmd "$fbankCmd" } \
  ${step03:+--DoAli true}  \
  ${step04:+--hierbn1-train true --hierbn1_cmd "$hierbn1_cmd"} \
  ${step05:+--hierbn-feature-transform true --hierbn-splice-cmd "$hierbn_splice_cmd" --hierbn-copy-cmd "$hierbn_copy_cmd" } \
  ${step06:+--hierbn2-train true --hierbn2-cmd "$hierbn2_cmd"} \
  $lang $alidata $tgt_dir || exit 1
  echo "$0: hierbn_train ended @ `date`"
fi
if $tune2_hierbn; then
  echo "$0: tune2_hierbn started @ `date`"
  [ ! -z $tgt_dir ] || \
  { echo "$0: tune2_hierbn: ERROR, tgt_dir not specified"; exit 1; }
  [ ! -z $tunebn_feature_transform ] || \
  { echo "$0: tune2_hierbn: ERROR, tunebn_feature_transform expected"; exit 1; }
  nnet_srcdir=$(dirname $tunebn_feature_transform)
  if [ ! -f $nnet_srcdir/final.nnet ] || [ -z $tunebn_feature_transform ]; then
    echo "$0: tune2_hierbn: ERROR, final.nnet or tunebn_feature_transform expected in $nnet_srcdir"; exit 1
  fi
  if [ ! -f $sdir/final.mdl ]; then
    echo "$0: tune2_hierbn: ERROR, final.mdl expected in $sdir"; exit 1
  fi
  tune_opts="--tunebn-feature-transform $tunebn_feature_transform --tune-learn-rate $tune_learn_rate --nnet-srcdir $nnet_srcdir"
  source/run-nnet.sh --cmd "$cmd" --nj $nj --validating-rate $validating_rate \
  --sdir $sdir  --usefbankdata true \
  ${step01:+--PrepareData true} \
  ${step02:+--MakeFBankFeat true --fbankCmd "$fbankCmd" } \
  ${step03:+--DoAli true}  \
  ${step04:+--tune-bn true $tune_opts } \
  $lang $alidata $tgt_dir || exit 1
  echo "$0: tune2_hierbn ended @ `date`"
fi
data=$rdata/dev/fbank22_pitch
if $prepare_fbank_test; then
  echo "$0: prepare_fbank_test started @ `date`"
  sdata=$rdata/dev/plp_pitch
  feat=$rexp/feature/fbank22_pitch/dev
  [ -d $data ] || mkdir -p $data
  cp $sdata/* $data/; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
  $fbankCmd --cmd "run.pl" --nj 10 $data $feat/log $feat/data  || exit 1
  steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1
  utils/fix_data_dir.sh $data
fi
if $crossling_fbank_test; then
  echo "$0: crossling_fbank_test started @ `date`"
  graph=$sdir/graph
  [ ! -z $dotest_graph ] && graph=$dotest_graph
  final_nnet=$(find $tgt_dir/ -name "final.nnet")
  nnet_dir=$(dirname $final_nnet)
  decode_dir=$nnet_dir/decode_dev
  steps/nnet/decode.sh --nj 20 --cmd "$train_cmd" $graph $data $decode_dir || exit 1
fi

if $prepare_fbank_data; then
  echo "$0: prepare_fbank_data started @ `date`"
  alidir=$dir/ali
  featdir=$dir/fmllr.feat
  source/run-nnet.sh --MakeFBankFeat true  --featdir $featdir \
  --sdir $sdir --alidir $alidir \
  $lang $trndata $alidata $dir || exit 1

fi

splice=5
delta_order=2
dataid=fbank22_pitch
featdir=$dir/$dataid
fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
nn_depth=5
hid_dim=2048
sdir=llp/exp/tri4a; dir=llp/exp/nnet.test/$dataid.trap.$nn_depth.$hid_dim.splice$splice
featdir=/media/kiwi_hhx502_usb/kws15/monoling/viet107/llp/exp/feature/$dataid

if $train_trap_nnet; then
  [ -d $dir ] || mkdir -p $dir
  source_dir=$(pwd)/llp/exp/nnet.test/fbank22_pitch.trap
  for x in ali  fbank22_pitch  trndata; do
    (cd $dir; ln -s $source_dir/$x )
  done
  echo "$0: train_trap_nnet started @ `date`"
  trndata=llp/data/train/plp_pitch
  alidir=$dir/ali
  preTrnCmd="isteps/nnet/pretrain_dbn_trap.sh  --copy_feats_tmproot $dir --apply-cmvn true --norm-vars true  --feat-type trap --splice $splice --delta-order $delta_order --nn-depth $nn_depth --hid-dim $hid_dim "
  # preTrnCmd="steps/nnet/pretrain_dbn.sh  --copy_feats_tmproot $dir --apply-cmvn true  --norm-vars true --splice $splice --delta-order $delta_order"
  trnCmd="steps/nnet/train.sh --copy_feats_tmproot $dir"
  source/run-nnet.sh --dataid $dataid --featdir $featdir \
  ${enabled:+--DoAli true   --fbankCmd "$fbankCmd" --MakeFBankFeat true} --usefbankdata true  \
  --preTrnCmd "$preTrnCmd" --PreTrain true --trnCmd "$trnCmd"  --TrainNnet true \
  --sdir $sdir --alidir $alidir  $lang $trndata $trndata $dir || exit 1

fi


if $test_trap_nnet; then
  echo "$0: test_trap_nnet started @ `date`"
  graph=llp/exp/tri4a/graph
  decode_dir=$dir/nnet/decode_dev
  steps/nnet/decode.sh --nj 30 --cmd "$train_cmd" $graph $data $decode_dir || exit 1
fi
if $prepare_feat; then
  lang=llp2/data/lang
  sdir=llp2/exp/tri4a
  alidata=llp2/data/train/plp_pitch
  dir=llp2/exp/dnn.feat
  fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank23.conf"
  mfccCmd="steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc23.conf"
  enabled=1
  echo "$0: prepare_feat started @ `date`"
  source/run-nnet.sh ${enabled:+--PrepareData true} --MakeFBankFeat true --fbankCmd "$fbankCmd"  \
  ${enabled:+--DoAli true}  --MakefMLLRFeat true  \
  ${enabled:+--MakeMFCCFeat true --mfccCmd "$mfccCmd"}  --usemfccdata true \
  --sdir $sdir  $lang $alidata $dir || exit 1

fi
splice=5
delta_order=2
nn_depth=7
hid_dim=1024
sdir=llp2/exp/tri4a
lang=llp2/data/lang
if $train_mfcc; then
  source_dir=llp2/exp/dnn.feat
  dir=$source_dir/mfcc.nnet
  [ -d $dir ] || mkdir -p $dir
  cp $source_dir/{train_mfcc,validating_mfcc,train_feat,validating_feat,train_label,validating_label} $dir/ || exit 1
  preTrnCmd="steps/nnet/pretrain_dbn.sh  --copy_feats_tmproot $dir --apply-cmvn true --norm-vars true  --splice $splice --delta-order $delta_order --nn-depth $nn_depth --hid-dim $hid_dim "
  trnCmd="steps/nnet/train.sh --copy_feats_tmproot $dir"
  source/run-nnet.sh --usemfccdata true  \
  --preTrnCmd "$preTrnCmd" --PreTrain true --trnCmd "$trnCmd"  --TrainNnet true \
  --sdir $sdir $lang unknown $dir || exit 1
fi
if $train_fbank; then
  source_dir=llp2/exp/dnn.feat
  dir=$source_dir/fbank.nnet
  [ -d $dir ] || mkdir -p $dir
  cp $source_dir/{train_fbank,validating_fbank,train_feat,validating_feat,train_label,validating_label} $dir/ || exit 1
  preTrnCmd="steps/nnet/pretrain_dbn.sh  --copy_feats_tmproot $dir --apply-cmvn true --norm-vars true  --splice $splice --delta-order $delta_order --nn-depth $nn_depth --hid-dim $hid_dim "
  trnCmd="steps/nnet/train.sh --copy_feats_tmproot $dir"
  source/run-nnet.sh --usefbankdata true  \
  --preTrnCmd "$preTrnCmd" --PreTrain true --trnCmd "$trnCmd"  --TrainNnet true \
  --sdir $sdir $lang unknown $dir || exit 1
fi
if $train_fbank_trap; then
  source_dir=llp2/exp/dnn.feat
  dir=$source_dir/fbank.trap.nnet
  [ -d $dir ] || mkdir -p $dir
  cp $source_dir/{train_fbank,validating_fbank,train_feat,validating_feat,train_label,validating_label} $dir/ || exit 1
  preTrnCmd="isteps/nnet/pretrain_dbn_trap.sh --feat-type trap --copy_feats_tmproot $dir --apply-cmvn true --norm-vars true  --splice $splice --delta-order $delta_order --nn-depth $nn_depth --hid-dim $hid_dim "
  trnCmd="steps/nnet/train.sh --copy_feats_tmproot $dir"
  source/run-nnet.sh --usefbankdata true  \
  --preTrnCmd "$preTrnCmd" --PreTrain true --trnCmd "$trnCmd"  --TrainNnet true \
  --sdir $sdir $lang unknown $dir || exit 1
fi

sdata=llp2/data/dev/plp_pitch
data_array=(llp2/data/dev/mfcc23_pitch llp2/data/dev/fbank23_pitch)
cmd_array=("steps/make_mfcc_pitch.sh --mfcc-config conf/mfcc23.conf" "steps/make_fbank_pitch.sh --fbank-config conf/fbank23.conf")
feat_array=(llp2/exp/dnn.feat/mfccdata/dev llp2/exp/dnn.feat/fbankdata/dev)
dnn_array=(llp2/exp/dnn.feat/mfcc.nnet/nnet llp2/exp/dnn.feat/fbank.nnet/nnet llp2/exp/dnn.feat/fbank.trap.nnet/nnet)
if $test_dev_data; then
  for i in $(seq 0 1); do
    data=${data_array[$i]}
    [ -d $data ] || mkdir -p $data
    cp $sdata/*  $data/; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
    cmd=${cmd_array[$i]} ;    feat=${feat_array[$i]}
    $cmd --cmd "run.pl" --nj 10 $data $feat/log $feat/data  || exit 1
    steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1
    utils/fix_data_dir.sh $data
    graph=llp2/exp/tri4a/graph
    dnn_dir=${dnn_array[$i]}
    decode_dir=$dnn_dir/decode_dev
    steps/nnet/decode.sh --nj 40 --cmd "$train_cmd" $graph $data $decode_dir || exit 1
    if [ $i -eq 1 ]; then
      i=$[i+1]
      dnn_dir=${dnn_array[$i]}
      decode_dir=$dnn_dir/decode_dev
      steps/nnet/decode.sh --nj 40 --cmd "$train_cmd" $graph $data $decode_dir || exit 1
    fi
  done
fi
