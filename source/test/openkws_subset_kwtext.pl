#!/usr/bin/perl

use strict;
use warnings;
use open qw(:std :utf8);
use XML::Simple;
use Data::Dumper;

if(scalar @ARGV != 1) {
  print STDERR "\n\nExample: cat keywords.int | $0 keywords.txt > keywords_oov.txt\n\n";
  exit 1;
}
my ($sFileName) = @ARGV;
my %vocab = ();
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)/ or next;
  $vocab{$1} ++;
}
print STDERR "$0: stdin ended\n";
open(F, "$sFileName") or die "$0: ERROR, file $sFileName cannot open\n";
while(<F>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  next if not exists $vocab{$1};
  print $_, "\n";
}
close F;
