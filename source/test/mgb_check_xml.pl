#!/usr/bin/perl 
use strict;
use warnings;
use open qw(:std :utf8);
use XML::Simple;
use Data::Dumper;

print STDERR "\n$0 ", join(" ", @ARGV), "\n";

print STDERR "$0: stdin expected\n";
my $data = XMLin(\*STDIN);
my $segmArray = $data->{body}->{segments};
my $segmNum = scalar @$segmArray;
print "$0: segments=", $segmNum, "\n"; 
for (my $i = 0; $i < $segmNum; $i++) {
  my $curSegm = $$segmArray[$i];
}
# print Dumper($data);
print STDERR "$0: stdin ended\n";
