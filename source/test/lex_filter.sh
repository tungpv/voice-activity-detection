#!/bin/bash

. path.sh
. cmd.sh

echo
echo "$0 $@"
echo 

# begin options
do_filtering=false
source_vocab=
filter_lex=
tgtdir=
# end options

. parse_options.sh

function PrintOpts {
  cat<<END

$0 [options]:
do_filtering			# value, $do_filtering
source_vocab			# value, "$source_vocab"
filter_lex			# value, "$filter_lex"
tgtdir				# value, "$tgtdir"
END
}

PrintOpts

if $do_filtering; then
  echo "$0: do_filtering started @ `date`"
  [ ! -z $filter_lex ] || \
  { echo "$0: do_filtering: ERROR, filter_lex expected or not exist"; exit 1; }
  [ ! -z $source_vocab ] || \
  { echo "$0: do_filtering: ERROR, source_vocab expected or not exist"; exit 1; }
  subWordList="cat $filter_lex|cut -f1|grep -v '<'|sort -u|"
  [ ! -z $tgtdir ] || \
  { echo "$0: do_filtering: ERROR, tgtdir expected"; exit 1; }
  [ -d $tgtdir ] || mkdir -p $tgtdir
  source/egs/lex_filter.pl "$source_vocab" "$subWordList"  > $tgtdir/filtered_vocab 2> $tgtdir/oov_vocab  
  echo "$0: do_filtering ended @ `date`"
fi

