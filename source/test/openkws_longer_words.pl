#!/usr/bin/perl

use strict;
use warnings;
use open qw(:std :utf8);
use XML::Simple;
use Data::Dumper;

print STDERR "\n$0 ", join(" ", @ARGV), "\n";
sub LetterDupTimes {
  my ($aRef, $sStr) = @_;
  my $nMaxDup = 0;
  for(my $i = 0; $i < scalar @$aRef; $i++) {
    my $curLetter = $$aRef[$i];
    my $nDupTimes = () = $sStr =~ /$curLetter/g;
    my $s = $curLetter;
    for(my $j = 1; $j < $nDupTimes; $j++) {
      $s .= $curLetter;
      if($sStr =~ m/\Q$s\E/) {
        my $x = length($s);
        $nMaxDup = $x if($x > $nMaxDup);
      }
    }
  } 
  return $nMaxDup;
}
if (scalar @ARGV != 3) {
  print STDERR "\n\nExample: cat wordlist.txt| $0 3 9 4\n\n";
  exit 1;
}
my ($nLower, $nUpper, $nMaxDup) = @ARGV;
if ($nLower < 1 || $nUpper < $nLower) {
  print STDERR "$0: ERROR, word len limit [$nLower, $nUpper] is illegal\n";
  exit 1;
}
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)/ or next;
  my $w = $1;
  my $nlen = length($w);
  next if $nlen < $nLower || $nlen > $nUpper;
  my @A = split(//, $w);
  if (LetterDupTimes(\@A, $w) > $nMaxDup) {
    # print STDERR "over-duplicated: $w\n";
    next;  
  }
  print "$_\n";
}
print STDERR "$0: stdin ended\n";
