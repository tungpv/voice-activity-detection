#!/bin/bash

. path.sh
. cmd.sh


# begin options
nj=20
cmd=run.pl
make_glang=false
make_glang_opts="source_vocab source_local glang_tgtdir langId"
make_grammar=false
make_grammar_opts="source_lm_file source_lang target_lang"
graphName=graph.x
decoding_eval=false
decoding_cmd=
decoding_opts="data lang sdir graph decode_dir"
subsetlm_extract_subvocab=false
subsetlm_subvocab=2000
subsetlm_extract_sublm=false
subsetlm_source_lm=
subsetlm_train=false
lexlm_slang=
lexlm_slocal=
lexlm_prepare_vidata2012=false
lexlm_prepare_tamiweb=false
lexlm_prepare_swahweb=false
lexlm_wordcount_swah=false
lexlm_prepare_vidata2013=false
lexlm_prepare_viweb=false
lexlm_wordcount_vi=false
lexlm_wordcount_tamil=false
lexlm_make_vocab=false
make_vocab_cmd="source/test/openkws_make_vocab.pl 0.01 0.0001 1000"
vocab_filter_cmd="cat -"
lexlm_train_srilms=false
ngram_count_source_opts="vocab text lmfname"
ngram_count_opts="-kndiscount1 -gt1min 0 -kndiscount2 -gt2min 1 -kndiscount3 -gt3min 1 -order 3 -unk -sort"
lexlm_interpolating=false
interpolating_opts="out-of-domain-lm  in-domain-lm  dev-data  lmdir intId"
lexlm_pruning=false
pruning_source="sourc_lm_file pruned_lm_file"
pruning_opts="-order 3 -prune 1e-9"
lexlm_vocab_size=200000
subword_train=false
subword_make_vocab=false
subword_vocab_opts="source_vocab morfessor_model targetdir"
subword_make_map=false
subword_make_map_opts="<count_word_list> <model> <map>"
subword_make_text=false
subword_text_opts="source_text transform_map targetfile"
kws_make_index=false
kws_index_opts="data lang sdir latdir indexdir"
kws_latscale_opts="--acoustic-scale=0.1 --lm-scale=1.0"
kws_make_query=false
kws_query_opts="data source_kw_text lang_dir kwsdatadir"
kws_search_index=false
search_index_opts="indexdir kwsdatadir kwsoutput keyword_fsts"
kws_write_kwslist=false
write_kwslist_opts="<kwsdatadir> <kwsoutput> --Ntrue-scale=1.0 --flen=0.01 --normalize=true --duptime=0.6 --remove-dup=true --digits=5"
kws_score=false
kws_score_cmd="<KWSEval-tool> -e <ecf> -r <rttm> -t <kwlist>"
kws_score_opts="<kwsoutputdir> <scoredir>"
tgtdir=
# end options

echo
echo "$0 $@"
echo

. parse_options.sh

function PrintOptions {
  cat<<END
$0 [options]:
nj					# value, $nj
cmd					# value, "$cmd"
make_glang				# value, $make_glang
make_glang_opts				# value, "$make_glang_opts"
make_grammar				# value, $make_grammar
make_grammar_opts			# value, "$make_grammar_opts"
graphName				# value, "$graphName"
decoding_eval				# value, $decoding_eval
decoding_cmd				# value, "$decoding_cmd"	
decoding_opts				# value, "$decoding_opts"
subsetlm_extract_subvocab		# value, $subsetlm_extract_subvocab
subsetlm_subvocab			# value, $subsetlm_subvocab
subsetlm_extract_sublm			# value, $subsetlm_extract_sublm
subsetlm_source_lm			# value, "$subsetlm_source_lm"
subsetlm_train				# value, $subsetlm_train
lexlm_prepare_vidata2012		# value, $lexlm_prepare_vidata2012
lexlm_prepare_tamiweb			# value, $lexlm_prepare_tamiweb
lexlm_prepare_swahweb			# value, $lexlm_prepare_swahweb
lexlm_wordcount_swah			# value, $lexlm_wordcount_swah
lexlm_prepare_vidata2013		# value, $lexlm_prepare_vidata2013
lexlm_prepare_viweb			# value, $lexlm_prepare_viweb
lexlm_wordcount_vi			# value, $lexlm_wordcount_vi
lexlm_wordcount_tamil			# value, $lexlm_wordcount_tamil
lexlm_slang				# value, "$lexlm_slang"
lexlm_slocal				# value, "$lexlm_slocal"
lexlm_make_vocab			# value, $lexlm_make_vocab
make_vocab_cmd				# value, "$make_vocab_cmd"
vocab_filter_cmd			# value, "$vocab_filter_cmd"
lexlm_train_srilms			# value, $lexlm_train_srilms
ngram_count_source_opts                 # value, "$ngram_count_source_opts"
ngram_count_opts			# value, "$ngram_count_opts"
lexlm_interpolating			# value, $lexlm_interpolating	
interpolating_opts			# value, "out-of-domain-lm  in-domain-lm  dev-data  lmdir"
lexlm_pruning				# value, $lexlm_pruning
pruning_source				# value, "$pruning_source"
pruning_opts				# value, "$pruning_opts"
ngramPruneCmd				# value, "$ngramPruneCmd"  
lexlm_vocab_size			# value, $lexlm_vocab_size
subword_train				# value, $subword_train
subword_make_vocab			# value, $subword_make_vocab
subword_vocab_opts			# value, "$subword_vocab_opts"	
subword_make_map			# value, $subword_make_map
subword_make_map_opts			# value, "$subword_make_map_opts"
subword_make_text			# value, $subword_make_text
subword_text_opts			# value, "$subword_text_opts"
kws_make_index				# value, $kws_make_index
kws_index_opts				# value, "$kws_index_opts"
kws_latscale_opts			# value, "$kws_latscale_opts"
kws_make_query				# value, $kws_make_query
kws_query_opts				# value, "$kws_query_opts"
kws_search_index			# value, $kws_search_index
search_index_opts			# value, "$search_index_opts"
kws_write_kwslist			# value, $kws_write_kwslist
write_kwslist_opts			# value, "$write_kwslist_opts"
kws_score				# value, $kws_score
kws_score_cmd				# value, "$kws_score_cmd"
kws_score_opts				# value, "$kws_score_opts"

tgtdir					# value, "$tgtdir"

END
}

PrintOptions

if $subsetlm_extract_subvocab; then
  echo "$0: subsetlm_extract_subvocab started @ `date`"
  for x in $tgtdir/train.txt $tgtdir/dev.txt; do
    [ -e $x ] || \
    { echo "$0: subsetlm_extract_subvocab: ERROR, file $x expected"; exit 1; }
  done  
    cat $tgtdir/train.txt | \
    source/test/word-count.pl | sort -k2rn |\
    head -$subsetlm_subvocab| awk '{print $1;}' > $tgtdir/vocab
  echo "$0: subsetlm_extract_subvocab ended @ `date`"
fi
if $subsetlm_extract_sublm; then
  echo "$0: subsetlm_extract_sublm started @ `date`"
  [ -e $tgtdir/vocab ] || \
  { echo "$0: subsetlm_extract_sublm: ERROR, vocab expected in $tgtdir"; exit 1; }
  [ -z $subsetlm_source_lm ] && \
  { echo "$0: subsetlm_extract_sublm: ERROR, subsetlm_source_lm file $subsetlm_source_lm expected"; exit 1; } 
  ngram -renorm  -limit-vocab -vocab $tgtdir/vocab -lm  $subsetlm_source_lm -write-lm $tgtdir/subset.lm.gz
  (
  for f in $tgtdir/subset.lm.gz ; do ( echo $f; ngram -order 3 -lm $f -unk -ppl $tgtdir/dev.txt ) | paste -s -d ' ' ; done 
  ) | sort  -r -n -k 13 | column -t | tee $tgtdir/subset.perplexities.txt
  echo "$0: subsetlm_extract_sublm ended @ `date`"
fi
if $subsetlm_train; then
  echo "$0: subsetlm_train started @ `date`"
  ilocal/train_lms_srilm.sh $tgtdir  
  echo "$0: subsetlm_train ended @ `date`"
fi

if $lexlm_prepare_vidata2012; then
  echo "$0: lexlm_viprepare_data2012 started @ `date`"
  gzip -cd openkws15/OpenSubtitles2012/raw/vi/*/*/*.xml.gz | \
  source/test/openkw_print_xml.pl | \
  gzip -c > $tgtdir/subtext2012.gz
  echo "$0: lexlm_prepare_vidata2012 ended @ `date`"
fi
if $lexlm_prepare_vidata2013; then
  echo "$0: lexlm_prepare_vidata2013 started @ `date`"
  gzip -cd openkws15/OpenSubtitles2013/OpenSubtitles2013/xml/vi/*/*/*.xml.gz | \
  source/test/openkw_print2013_xml.pl | \
  gzip -c > $tgtdir/subtext2013.gz
  echo "$0: lexlm_prepare_vidata2013 ended @ `date`"
fi
if $lexlm_prepare_viweb; then
  echo "$0: lexlm_prepare_viweb started @ `date`"
  gzip -cd openkws15/vietnamese-webdata/bbn-webdata/*/*.txt.gz | \
  source/test/openkw_print_web.pl | \
  gzip -c > $tgtdir/webtext.gz
  echo "$0: lexlm_prepare_viweb ended @ `date`"
fi
if $lexlm_wordcount_vi; then
  echo "$0: lexlm_wordcount_vi started @ `date`"
  lexlm_graphemes=$lexlm_slocal/nonsilence_phones.txt
  [ ! -e $lexlm_graphemes ] && \
  { echo "$0: lexlm_wordcount_vi: ERROR, file nonsilence_phones.txt expected in lexlm_slocal"; exit 1; }
  gzip -cd  openkws15/vi_websub_data/{subtext2012,subtext2013,webtext}.gz | \
  gzip -c > openkws15/vi_websub_data/text.gz
  gzip -cd  openkws15/vi_websub_data/text.gz | \
  source/test/openkws_wordcount.pl $lexlm_graphemes "|gzip -c  > $tgtdir/in-vocab-count.gz" \
  "|gzip -c > $tgtdir/oov-vocab-count.gz"
  echo "$0: lexlm_wordcount_vi ended @ `date`"
fi
if $lexlm_prepare_tamiweb; then
  echo "$0: lexlm_prepare_tamiweb started @ `date`"
  gzip -cd openkws15/tamil-webdata/bbn-webdata/*/*.txt.gz | \
  source/test/openkw_print_web.pl | \
  gzip -c > $tgtdir/webtext.gz
  echo "$0: lexlm_prepare_tamiweb ended @ `date`"
fi
if $lexlm_prepare_swahweb; then
  echo "$0: lexlm_prepare_swahweb started @ `date` "
  gzip -cd 202-webdata/bbn-webdata/*/*.txt.gz | \
  source/test/openkw_print_web.pl | \
  gzip -c > $tgtdir/webtext.gz
  echo "$0: lexlm_prepare_swahweb ended @ `date`"
fi
if $lexlm_wordcount_swah; then
  echo "$0: lexlm_wordcount_swah started @ `date`"
  lexlm_graphemes=$lexlm_slocal/nonsilence_phones.txt
  [ ! -e $lexlm_graphemes ] && \
  { echo "$0: lexlm_wordcount_tamil: ERROR, file nonsilence_phones.txt expected in lexlm_slocal"; exit 1; }
  cat $lexlm_graphemes > $tgtdir/nonsilence_phones.txt
  mv $tgtdir/webtext.gz $tgtdir/text.gz
  gzip -cd  $tgtdir/text.gz | \
  source/test/openkws_wordcount.pl $lexlm_graphemes "|gzip -c  > $tgtdir/in-vocab-count.gz" \
  "|gzip -c > $tgtdir/oov-vocab-count.gz"
  echo "$0: lexlm_wordcount_swah ended @ `date`"
fi
if $lexlm_wordcount_tamil; then
  echo "$0: lexlm_wordcount_tamil started @ `date`"
  lexlm_graphemes=$lexlm_slocal/nonsilence_phones.txt
  [ ! -e $lexlm_graphemes ] && \
  { echo "$0: lexlm_wordcount_tamil: ERROR, file nonsilence_phones.txt expected in lexlm_slocal"; exit 1; }
  cat $lexlm_graphemes > $tgtdir/nonsilence_phones.txt
  mv $tgtdir/webtext.gz $tgtdir/text.gz
  gzip -cd  $tgtdir/text.gz | \
  source/test/openkws_wordcount.pl $lexlm_graphemes "|gzip -c  > $tgtdir/in-vocab-count.gz" \
  "|gzip -c > $tgtdir/oov-vocab-count.gz"
  echo "$0: lexlm_wordcount_tamil ended @ `date`"
fi
if $lexlm_make_vocab; then
  echo "$0: lexlm_make_vocab started @ `date`"
  [ -e $tgtdir/in-vocab-count.gz ] || \
  { echo "$0: lexlm_make_vocab: ERROR, file in-vocab-count.gz expected"; exit 1; }
  [ ! -z $tgtdir/in_domain_vocab ] && [ ! -z $tgtdir/dev.txt ]  || \
  { echo "$0: lexlm_make_vocab: ERROR, in_domain_vocab and dev.txt expected"; exit 1; }
  words=$lexlm_slang/words.txt
  [ -z $words ] && \
  { echo "$0: lexlm_make_vocab: ERROR, words.txt expected in lexlm_slang $lexlm_slang"; exit 1; }
  cat $words | grep -v '<eps>' | grep -v '\#'|\
  source/test/openkws_print_field.pl 1 > $tgtdir/in_domain_vocab
  svocab=$tgtdir/vocab$lexlm_vocab_size
  gzip -cd $tgtdir/in-vocab-count.gz| head -$lexlm_vocab_size| \
  source/test/openkws_print_field.pl 1 > $svocab
 oovr=$(echo "$make_vocab_cmd" | awk '{s=sprintf( "%.2f", $2); print s;}')
  cat $tgtdir/dev.txt | \
  $make_vocab_cmd $svocab $tgtdir/in_domain_vocab 2> $tgtdir/vocab_oov_stats.log | $vocab_filter_cmd | \
  cat - $tgtdir/in_domain_vocab | sort -u > $tgtdir/vocab 
  x=$(wc -l < $tgtdir/vocab)
  cp $tgtdir/vocab  $tgtdir/vocab.$oovr.$x
  rm $svocab
  echo "$0: lexlm_make_vocab ended @ `date`"
fi
if $subword_train; then
  echo "$0: subword_train started @ `date`"
  [ -e $tgtdir/traindata_list ] || \
  { echo "$0: subword_train: ERROR, traindata_list expected in tgtdir $tgtdir"; exit 1; }
  morfessor  -s $tgtdir/morfessor_model.bin -t $tgtdir/traindata_list  --traindata-list
  echo "$0: subword_train ended @ `date`"
fi
if $subword_make_vocab; then
  echo "$0: subword_make_vocab started @ `date`"
  optNames="svocab model dir"
  . source/register_options.sh "$optNames" "$subword_vocab_opts" || \
  { echo "$0: subword_make_vocab: ERROR, @register_options.sh"; exit 1; }
  [ -e $svocab ] || { echo "$0: subword_make_vocab: ERROR, source_vocab $svocab not exist"; exit 1; }
  [ -e $model ] || { echo "$0: subword_make_vocab: ERROR, morfessor model $model not exist"; exit 1; }
  [ -d $dir ] || mkdir -p $dir
  grep -v '<' $svocab | \
  morfessor-segment -l $model -e utf8  - | \
  awk '{for(i=1; i<=NF; i++){print $i; }}' | sort -u | \
  cat - <(grep  '<' $svocab) | \
  perl -e "while(<>){ if(/^[\'_]$/){next;} print; }"|sort > $dir/vocab
  echo "$0: subword_make_vocab ended @ `date`"
fi
if $subword_make_map; then
  echo "$0: subword_make_map started @ `date`" 
  optNames="cwlist model map"
  . source/register_options.sh "$optNames" "$subword_make_map_opts" || \
  { echo "$0: subword_make_map: ERROR, @register_options.sh"; exit 1; }
  
  cat $cwlist | awk '{print $2;}'| \
  morfessor-segment -l $model -e utf8  - > $tgtdir/subword_list
  nsub=$(wc -l < $tgtdir/subword_list)
  nword=$(wc -l < $cwlist)
  if [ $nsub -ne $nword ]; then
    echo "$0: subword_make_map: ERROR, making subword($nsub) from word ($nword) list failed"
    exit 1
  fi
  paste <(awk '{print $2;}' $cwlist) $tgtdir/subword_list | gzip -c > $tgtdir/word2subword_map.gz
  echo "$0: subword_make_map ended @ `date`"
fi
if $subword_make_text; then
  echo "$0: subword_make_text started @ `date`"
  stext=$( echo "$subword_text_opts"| awk '{print $1;}')
  [ -f $stext ] || \
  { echo "$0: subword_make_text: ERROR, source_text file expected"; exit 1; }
  map=$(echo "$subword_text_opts"| awk '{print $2;}')
  [ ! -z $map ] && [ -f $map ] || \
  { echo "$0: subword_make_text: ERROR, transform_map file expected"; exit 1; }
  tgttext=$(echo "$subword_text_opts"| awk '{print $3;}')
  [ ! -z $tgttext ] || 
  { echo "$0: subword_make_text: ERROR, target text expected"; exit 1;  }
  dir=$(dirname $tgttext)
  [ -d $dir ] || mkdir -p $dir
  fileName=$(basename $tgttext)
  extName=${fileName##*.}
  [ "$extName" != "gz" ] && tgttext=${tgttext}.gz
  extName=$(e=`basename $stext`; x=${e##*.}; echo $x)
  catCmd="cat $stext"
  [ "$extName" == "gz" ] && catCmd="gzip -cd $stext"
  $catCmd | \
  source/test/openkws_transform_text.pl "gzip -cd $map|" | \
  gzip -c > $tgttext
  echo "$0: subword_make_text ended @ `date`"
fi
if $lexlm_train_srilms; then
  echo "$0: lexlm_train_srilms started @ `date`"
  vocab=$(echo "$ngram_count_source_opts"| awk '{print $2;}')
  [ -f $vocab ] || { echo "$0: train_srilms: ERROR, file vocab ($vocab) expected"; exit 1; }
  text=$(echo "$ngram_count_source_opts"| awk '{print $4;}')
  [ -f $text ] || { echo "$0: train_srilms: ERROR, file text ($text) expected"; exit 1; }
  lmfname=$(echo "$ngram_count_source_opts"| awk '{print $6;}')
  dir=$(dirname $lmfname)
  [ -d $dir ] || mkdir -p $dir
  ngram-count $ngram_count_source_opts $ngram_count_opts 
  echo "$0: lexlm_train_srilms ended @ `date`"
fi
if $lexlm_interpolating; then
  echo "$0: lexlm_interpolating started @ `date`"
  optNames="olm ilm devdata lmdir intId"
  . source/register_options.sh "$optNames" "$interpolating_opts" || \
  { echo "$0: lexlm_interpolating: ERROR, @ register_options.sh"; exit 1; }
  [ ! -z $olm ] && [ -e $olm ] || \
  { echo "$0: interpolating: ERROR, out-of-domain lm $olm file expected"; exit 1; }
  [ ! -z $ilm ] && [ -e $ilm ] || \
  { echo "$0: interpolating: ERROR, in-domain lm $ilm file expected"; exit 1; }
  [ ! -z $devdata ] && [ -e $devdata ] || \
  { echo "$0: interpolating: ERROR, devdata  ($devdata) file expected"; exit 1; }
  [ ! -z $lmdir ] || \
  { echo "$0: interpolating: ERROR, lmdir expected"; exit 1; }
  [ -d $lmdir ] || mkdir -p $lmdir 
  tmpdir=$(mktemp -d -p $lmdir)
  trap  "echo removing $tmpdir ...; rm -rf $tmpdir" EXIT
  best_ppl=10000000
  best_lm=
  [ -e $lmdir/perplexities.$intId ] && rm $lmdir/perplexities.$intId
  for ifactor in $(seq 0.1 0.1 0.6); do
    echo "$0: lexlm_interpolating: factor=$ifactor ..."
    tlm=$tmpdir/lm.int.$intId.${ifactor}.gz
    ngram -order 3 -lm  $olm  -lambda $ifactor \
    -mix-lm $ilm  -write-lm $tlm
    ppl=$(ngram -order 3 -lm $tlm -unk -ppl $devdata | paste -s -d ' '| tee -a $lmdir/perplexities.$intId | awk '{print $14;}')
    [ -z $ppl ] && \
    { echo "$0: lexlm_interpolating: ERROR, lm $lm ppl is problematic "; exit 1; }
    if [ $(echo "$ppl < $best_ppl"| bc -l) -eq 1 ]; then
      best_lm=$tlm
      best_ppl=$ppl
    fi
  done 
  [ -z $best_lm ] && \
  { echo "$0: lexlm_interpolating: ERROR, best_lm not found by interpolating"; exit 1; }
  cp $best_lm $lmdir/$(basename $best_lm)
  rm $lmdir/final.int.$intId.lm.gz 2>/dev/null
  (cd $lmdir; ln -s $(basename $best_lm) final.int.$intId.lm.gz)
  echo "$0: lexlm_interpolating ended @ `date`"
fi
if $lexlm_pruning; then
  echo "$0: lexlm_pruning started @ `date`"
  slm=$(echo "$pruning_source"| awk '{print $1;}')
  [ ! -z $slm ] && [ -f $slm ] || \
  { echo "$0: lexlm_pruning: ERROR, source_lm_file($slm) expected"; exit 1; }
  plm=$(echo "$pruning_source"| awk '{print $2;}')
  [ ! -z $plm ] || \
  { echo "$0: lexlm_pruning: ERROR, target_lm_file($plm) expected"; exit 1; } 
  dir=$(dirname $plm); [ -d $dir ] || mkdir -p $dir
  ngram  $pruning_opts -lm $slm -write-lm $plm
  echo "$0: lexlm_pruning ended @ `date`"
fi
if $make_glang; then
  echo "$0: make_glang started @ `date`"
  optNames="vocab slocal dir langId"
  . source/register_options.sh  "$optNames" "$make_glang_opts" || \
  { echo "$0: make_glang: ERROR, @register_options.sh, for $make_glang_opts"; exit 1; }
  [ ! -z $vocab ] && [ -f $vocab ] || \
  { echo "$0: make_glang: ERROR, source vocab $vocab expected"; exit 1; }
  [ ! -z $slocal ] && [ -f $slocal/nonsilence_phones.txt ] || \
  { echo "$0: make_glang: ERROR, slocal ($slocal) or nonsilence_phones.txt file not specified"; exit 1; }
  [ -z $dir ] &&   \
  { echo "$0: make_glang: ERROR, glang_tgtdir ($dir) expected"; exit 1; }
  localdir=$dir/local.$langId
  [ -d $localdir ] || mkdir -p $localdir
  grep -v '<' $vocab | \
  source/babel_split_word.pl | sort -u > $localdir/grapheme_lex_src.txt
  local/prepare_lexicon.pl --oov "<unk>"   $localdir/grapheme_lex_src.txt $localdir
  if ! diff $slocal/nonsilence_phones.txt $localdir/nonsilence_phones.txt >/dev/null; then
    echo "$0: make_glang: ERROR, nonsilence_phones.txt is not agreed with that in $slocal"
    exit 1
  fi
  utils/prepare_lang.sh \
  --share-silence-phones true \
  $localdir "<unk>" $localdir/tmp.lang $dir/lang.$langId
  echo "$0: make_glang ended @ `date`"
fi
if $make_grammar; then
  echo "$0: make_grammar started @ `date`"
  optNames="lmfile slang lang"
  . source/register_options.sh "$optNames" "$make_grammar_opts" || \
  { echo "$0: make_grammar: ERROR, @register_options.sh for $make_grammar_opts"; exit 1; }
  [ ! -z $lmfile ] && [ -f $lmfile ] || \
  { echo "$0: make_grammar: ERROR, lmfile ($lmfile) expected"; exit 1; }
  [ ! -z $slang ] && [ -f $slang/words.txt ] || \
  { echo "$0: make_grammar: ERROR, slang ($lang) or words.txt not ready"; exit 1; }
  [ -d $lang ] || mkdir -p $lang
  if [ "$slang" != "$lang" ]; then
    cp -rL $slang/*  $lang/
  fi
  local/arpa2G.sh  $lmfile  $lang $lang 
  echo "$0: make_grammar ended @ `date`"
fi
if $decoding_eval; then
  echo "$0: decoding_eval started @ `date`"
  [ -z "$decoding_cmd" ] && \
  { echo "$0: decoding_eval: ERROR, decoding_cmd expected"; exit 1; }
  data=$(echo "$decoding_opts"| awk '{print $1;}')
  [ ! -z $data ] && [ -f $data/feats.scp ] || \
  { echo "$0: decoding_eval: ERROR, data not specified"; exit 1; }
  lang=$(echo "$decoding_opts"| awk '{print $2;}')
  [ ! -z $lang ] && [ -f $lang/words.txt ] || \
  { echo "$0: decoding_eval: ERROR, lang not specified"; exit 1; }
  sdir=$( echo "$decoding_opts" | awk '{print $3;}')
  [ ! -z $sdir ] && [ -f $sdir/final.mdl ] || \
  { echo "$0: decoding_eval: ERROR, sdir ($sdir) or final.mdl not ready"; exit 1; }
  graph=$( echo "$decoding_opts" | awk '{print $4;}' )
  [ ! -z $graph ] || \
  { echo "$0: decoding_eval: ERROR, graph expected"; exit 1; }
  decode_dir=$(echo "$decoding_opts" | awk '{print $5;}')
  [ ! -z $decode_dir ] || \
  { echo "$0: decoding_eval: ERROR, decode_dir not specified"; exit 1; }
  [ -f $graph/HCLG.fst ] ||  utils/mkgraph.sh $lang $sdir $graph
  if [ "$decoding_cmd" != " " ]; then
    $decoding_cmd --cmd "$cmd" --nj $nj $graph $data $decode_dir  || exit 1 
  fi
  echo "$0: decoding_eval ended @ `date`"
fi
if $kws_make_index; then
  echo "$0: kws_make_index started @ `date`"
  data=$(echo "$kws_index_opts" | awk '{print $1;}')
  [ ! -z $data ] && [ -f $data/segments ] || \
  { echo "$0: kws_make_index: ERROR, segments file expected in data($data) folder"; exit 1; }
  lang=$(echo "$kws_index_opts"| awk '{print $2;}')
  [ ! -z $lang ] && [ -f $lang/phones/word_boundary.int ] || \
  { echo "$0: kws_make_index: ERROR, lang($lang) or word_boundary.int expected"; exit 1; }
  wbound=$lang/phones/word_boundary.int
  sdir=$(echo "$kws_index_opts"| awk '{print $3; }')
  [ ! -z $sdir ] && [ -f $sdir/final.mdl ] || \
  { echo "$0: kws_make_index: ERROR, sdir ($sdir) or final.mdl expected "; exit 1; }
  latdir=$(echo "$kws_index_opts" | awk '{print $4;}')
  [ ! -z $latdir ] && [ -f $latdir/lat.1.gz ]  || \
  { echo "$0: kws_make_index: ERROR, latdir or lat.1.gz expected"; exit 1; }
  indexdir=$(echo "$kws_index_opts"| awk '{print $5;}')
  [ ! -z $indexdir ] || \
  { echo "$0: kws_make_index: ERROR, indexdir expected"; exit 1; }
  utter_id=$data/utter_id
  cat $data/segments | awk '{print $1;}' | \
  perl -e '$index = 1; while(<>) {chomp; print "$_ $index\n"; $index ++; }' > $utter_id
  cat $data/segments | awk '{printf ("%s %s\n", $1, $2);}'  > $data/utter_map
  nj=$(cat $latdir/num_jobs)
  [ ! -z $nj ] || { echo "$0: kws_make_index: ERROR, nj ($nj) not defined"; exit 1; }
  $cmd JOB=1:$nj $indexdir/log/index.JOB.log \
  lattice-align-words $wbound  $sdir/final.mdl "ark:gzip -cdf $latdir/lat.JOB.gz|" ark:- \| \
  lattice-scale $kws_latscale_opts  ark:- ark:- \| \
  lattice-to-kws-index ark:$utter_id ark:- ark:- \| \
  kws-index-union ark:- "ark:|gzip -c > $indexdir/index.JOB.gz"
  cp $latdir/num_jobs $indexdir/num_jobs

  echo "$0: kws_make_index ended @ `date`"
fi
if $kws_make_query; then
  echo "$0: kws_make_query started @ `date`"
  data=$(echo "$kws_query_opts"| awk '{print $1;}')
  [ ! -z $data ] && [ -f $data/utter_id ] && [ -f $data/utter_map ] || \
  { echo "$0: kws_make_query: ERROR, data or utter_id or utter_map expected"; exit 1; }
  kwtext=$(echo "$kws_query_opts"|awk '{print $2;}')
  [ ! -z $kwtext ] && [ -e $kwtext ] || \
  { echo "$0: kws_make_query: ERROR, kwtext($kwtext) is not ready"; exit 1; }
  lang=$(echo "$kws_query_opts"| awk '{print $3;}')  
  [ ! -z $lang ] && [ -e $lang/words.txt ] || \
  { echo "$0: kws_make_query: ERROR, lang ($lang) or $lang/words.txt not ready"; exit 1; }
  kwsdatadir=$(echo "$kws_query_opts"| awk '{print $4;}')
  [ ! -z $kwsdatadir ] || \
  { echo "$0: kws_make_query: ERROR, kwsdatadir($kwsdatadir) not specified"; exit 1; }
  [ -d $kwsdatadir ] || mkdir -p $kwsdatadir
  cp $data/{segments,utter_id,utter_map} $kwsdatadir/
  cat $lang/words.txt | uconv -f utf8 -t utf8 -x "Any-Lower" > $kwsdatadir/words.txt
  paste <(cut -f 1  $kwtext  ) \
        <(cut -f 2  $kwtext | uconv -f utf8 -t utf8 -x "Any-Lower" ) |\
  local/kwords2indices.pl --map-oov 0  $kwsdatadir/words.txt > $kwsdatadir/keywords_all.int
  cat $kwsdatadir/keywords_all.int | grep -v " 0 " | grep -v " 0$" > $kwsdatadir/keywords.int 
  cat $kwsdatadir/keywords_all.int | egrep " 0 | 0$" | cut -f 1 -d ' ' | \
  source/test/openkws_subset_kwtext.pl $kwtext > $kwsdatadir/keywords_oov.txt
  echo;  paste <(cut -d" " -f2- $kwsdatadir/keywords_all.int) <(cut -f2- $kwtext) | \
  perl -e 'while(<STDIN>) { chomp; @A = split(/[\s]+/); $nWord = scalar @A/2; for($i=0; $i<$nWord; $i++) {
    $w =$A[$i]; if($w==0) { $w1 = $A[$nWord+$i]; print $w1, " "; }   } }'; echo
  total=$(wc -l < $kwtext)
  oovNum=$(wc -l < $kwsdatadir/keywords_oov.txt)
  oovRate=$(perl -e '($nTotal, $nOovNum) = @ARGV; $fOovRate = 0; 
  $fOovRate = $nOovNum/$nTotal if ($nTotal > 0); 
  $sOovRate = sprintf("%.4f", $fOovRate); print $sOovRate; ' $total $oovNum)
  echo "$0: total_kws=$total, oov_kws=$oovNum, oov_rate=$oovRate"
  transcripts-to-fsts ark:$kwsdatadir/keywords.int ark,t:$kwsdatadir/keywords.fsts
  echo "$0: kws_make_query ended @ `date`"
fi
if $kws_search_index; then
  echo "$0: kws_search_inex started @ `date`"
  indexdir=$(echo "$search_index_opts"|awk '{print $1;}')
  [ ! -z $indexdir ] && [ -f $indexdir/index.1.gz ] || \
  { echo "$0: kws_search_index: ERROR, indexdir($indexdir) is not ready"; exit 1; }
  kwsdatadir=$(echo "$search_index_opts"| awk '{print $2;}')
  [ ! -z $kwsdatadir ] && [ -f $kwsdatadir/utter_id ] || \
  { echo "$0: kws_search_index: ERROR, kwsdatadir($kwsdatadir) is not ready"; exit 1; }
  kwsoutput=$(echo "$search_index_opts"| awk '{print $3;}') 
  [ ! -z $kwsoutput ] || \
  { echo "$0: kwsoutput($kwsoutput) not specified"; exit 1; }
  keyword_fsts=$(echo "$search_index_opts"| awk '{print $4; }')
  [ ! -z $keyword_fsts ] && [ -e $keyword_fsts ] || \
  { echo "$0: kws_search_index: ERROR, keyword_fsts ($keyword_fsts) not specified"; exit 1; }
  nj=$(cat $indexdir/num_jobs)
  [ ! -z $nj ] || { echo "$0: kws_search_index: ERROR, num_jobs($indexdir) expected"; exit 1; }

  $cmd JOB=1:$nj $kwsoutput/log/search.JOB.log \
  kws-search --strict=true --negative-tolerance=-1 \
  "ark:gzip -cdf $indexdir/index.JOB.gz|" ark:$keyword_fsts \
  "ark,t:|int2sym.pl -f 2 $kwsdatadir/utter_id > $kwsoutput/result.JOB" || exit 1;

  echo "$0: kws_search_index ended @ `date`"
fi

if $kws_write_kwslist; then
  echo "$0: kws_write_kwslist started @ `date`"
  kwsdatadir=$(echo "$write_kwslist_opts"| awk '{print $1;}')
  [ ! -z $kwsdatadir ] || \
  { echo "$0: kws_write_kwslist: ERROR, kwsdatadir not specified"; exit 1; }
  for x in duration segments utter_map; do
    [ -f $kwsdatadir/$x ] || { echo "$0: kws_write_kwslist: ERROR, file $x expected in kwsdatadir ($kwsdatadir)"; exit 1; }
  done
  kwsoutput=$(echo "$write_kwslist_opts"| awk '{print $2;}')
  [ ! -z $kwsoutput ] && [ -f $kwsoutput/result.1 ] || \
  { echo "$0: kws_write_kwslist: ERROR, kwsoutput($kwsoutput) is not ready"; exit 1; }
  write_opts=$( echo "$write_kwslist_opts" |cut -d' ' -f3- )
  echo "$0: kws_write_kwslist: write_opts=$write_opts"
  cat $kwsoutput/result.* | \
  utils/write_kwslist.pl $write_opts  --duration=$(cat $kwsdatadir/duration) --segments=$kwsdatadir/segments \
        --map-utter=$kwsdatadir/utter_map  - $kwsoutput/kwslist.xml || exit 1
  echo "$0: kws_write_kwslist ended @ `date`"
fi
if $kws_score; then
  echo "$0: kws_score started @ `date`"
  kwsoutputdir=$(echo "$kws_score_opts"| awk '{print $1;}')
  [ ! -z $kwsoutputdir ] && [ -f $kwsoutputdir/kwslist.xml ] || \
  { echo "$0: kws_score: ERROR, kwsoutputdir ($kwsoutputdir) is not ready"; exit 1; }
  scoredir=$(echo "$kws_score_opts"| awk '{print $2;}')
  [ ! -z $scoredir ] || \
  { echo "$0: kws_score: ERROR, scoredir not specified"; exit 1; }
  [ -d $scoredir ] || mkdir -p $scoredir
  $kws_score_cmd -s $kwsoutputdir/kwslist.xml -c -o -b -d -f $scoredir/ || exit 1;
  duration=`cat $scoredir/sum.txt | grep TotDur | cut -f 3 -d '|' | sed "s/\s*//g"`
  local/kws_oracle_threshold.pl --duration $duration $scoredir/alignment.csv > $scoredir/metrics.txt
  cat $scoredir/metrics.txt
  echo "$0: kws_score ended @ `date`"
fi
