#!/bin/bash

. path.sh
. cmd.sh

# begin options
test_fmllr_feat=false
test_make_fbank_feat=false
test_doali=false
test_pretrain=false
test_train=false
make_lang_dep_nnet=false
test_decode=false
link_data=false
mling_train=false
train_front_layers=false
cnn_train=false
nnet_opts="--apply-cmvn true --norm-vars true --delta-order 2 --splice 5 --hid-layers 2 --hid-dim 1024 --learn-rate 0.008"
cnn_pretrain_opts="--input-vis-type bern --param-stddev-first 0.05 --param-stddev 0.05"
preTrnCmd="steps/nnet/pretrain_dbn.sh --nn-depth 2  --hid-dim 1024"
cross_ling_transfer=false
cross_ling_test=false
cross_hid_layers=0
cross_learn_rate=0.004
cnn_source_dir=llp2/exp/mling.hid_dim1024.splice5.layer2
# end options

. parse_options.sh


function Options {
  cmdName=`echo $0 | perl -pe 's/^.*\///g;'`
  cat<<END

$cmdName [options]:
test_fmllr_feat               # value, $test_fmllr_feat
test_make_fbank_feat          # valie, $test_make_fbank_feat
test_doali                    # vlaue, $test_doali
test_pretrain                 # value, $test_pretrain
test_train                    # value, $test_train
make_lang_dep_nnet            # value, $make_lang_dep_nnet
test_decode                   # value, $test_decode
link_data                     # value, $link_data
mling_train                   # value, $mling_train
train_front_layers            # value, $train_front_layers
cnn_train                     # value, $cnn_train
nnet_opts                     # value, "$nnet_opts"
cnn_pretrain_opts             # value, "$cnn_pretrain_opts"
preTrnCmd                     # value, "$preTrnCmd"
cross_ling_transfer           # value, $cross_ling_transfer
cross_ling_test               # value, $cross_ling_test
cross_hid_layers              # value, $cross_hid_layers
cross_learn_rate              # value, $cross_learn_rate
cnn_source_dir                # value, $cnn_source_dir

END
}

Options;

langnames="viet:cant"
langs="llp/data/lang:../cant101/llp/data/lang"
trndatas="llp/data/train/plp_pitch:../cant101/llp/data/train/plp_pitch"
alidatas="llp/data/train/plp_pitch:../cant101/llp/data/train/plp_pitch"
sdirs="llp/exp/tri4a:../cant101/llp/exp/tri4a"
dir=llp/exp/mling.test

if $test_fmllr_feat; then
  source/run-nnet-mling.sh  --PrepareData true   \
  "$langnames" "$langs" "$trndatas" "$alidatas" "$sdirs" $dir
fi

fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
if $test_make_fbank_feat; then
  echo "$0: test_make_fbank_feat started @ `hostname` `date`"
  featdir=/media/kiwi_hhx502_usb/kws15/monoling/viet107/llp/exp/feat.fbank
  dataid=fbank22_pitch
  source/run-nnet-mling.sh --featdir $featdir --dataid $dataid --fbankCmd "$fbankCmd" --MakeFBankFeat true   \
  "$langnames" "$langs" "$trndatas" "$alidatas" "$sdirs" $dir

fi

if $test_doali; then
  echo "$0: test_doali started @ `hostname` `date`"
  source/run-nnet-mling.sh --DoAli true  \
  "$langnames" "$langs" "$trndatas" "$alidatas" "$sdirs" $dir
 
fi
rdata=llp2/data
rexp=llp2/exp
dataid=fbank22_pitch
hid_dim=2048
splice=5
nn_depth=6
delta_order=2
dir=$rexp/mling.trap.hid_dim$hid_dim.splice$splice.layer$nn_depth
learn_rate=0.008
enabled=1
if $mling_train; then
  echo "$0: mling_train started @ `date`"
  [ -d $dir ] || mkdir -p $dir
  langnames="cant:pash:taga:turk"
  langs="../cant101/flp2/data/lang:../pash104/flp2/data/lang:../taga106/flp2/data/lang:../turk105/flp2/data/lang"
  alidatas="../cant101/flp2/data/train/plp_pitch:../pash104/flp2/data/train/plp_pitch:../taga106/flp2/data/train/plp_pitch:../turk105/flp2/data/train/plp_pitch"
  sdirs="../cant101/flp2/exp/tri4a:../pash104/flp2/exp/tri4a:../taga106/flp2/exp/tri4a:../turk105/flp2/exp/tri4a"
  alidirs=$dir/ali
  preTrnCmd="isteps/nnet/pretrain_dbn_trap.sh  --copy_feats_tmproot $dir --hid-dim $hid_dim --nn-depth $nn_depth   --apply-cmvn true --norm-vars true   --feat-type trap --splice $splice --delta-order $delta_order"
  train_scheduler="steps/nnet/train_scheduler_mling.sh"
  trnCmd="steps/nnet/train_mling.sh --copy-feats false --train-scheduler $train_scheduler"
  source/run-nnet-mling.sh --dataid $dataid --featdir $dir/feature \
  ${disabled:+--PrepareData true --MakeFBankFeat true --fbankCmd "$fbankCmd" --usefbankdata true}  \
  ${disabled:+--DoAli true} \
  ${disabled:+--PreTrain true --preTrnCmd "$preTrnCmd"} \
  --TrainNnet true --trnCmd "$trnCmd" \
  "$langnames" "$langs" "$alidatas" "$sdirs" "$alidirs" $dir
  
fi
if $link_data; then
  echo "$0: link_data started @ `date`"
  sdata_abs=$(cd $sdata; pwd)
  [ -d $dir ] || mkdir -p $dir
  (cd $dir; 
    for x in ali  fbank22_pitch  trndata  trnlabel; do
      [ -h $x ] && unlink $x
      ln -s $sdata_abs/$x
    done
  )
fi

if $test_pretrain; then
  echo "$0: test_pretrain started @ `date`"
  preTrnCmd="isteps/nnet/pretrain_dbn_trap.sh  --copy_feats_tmproot $dir --hid-dim $hid_dim --nn-depth $nn_depth   --apply-cmvn true --norm-vars true   --feat-type trap --splice $splice --delta-order $delta_order"
  source/run-nnet-mling.sh --dataid $dataid  --PreTrain true --preTrnCmd "$preTrnCmd" --usefbankdata true \
  "$langnames" "$langs" "$trndatas" "$alidatas" "$sdirs" $dir
fi

if $test_train; then
  learn_rate=0.008
  dnn_dir=$dir/nnet
  pretrain_dir=$dir/pretrain_dbn$dataid
  feature_transform=$pretrain_dir/final.feature_transform
  dbn=$pretrain_dir/$nn_depth.dbn
  lang=llp/data/lang
  alidirs="llp/exp/mling.test/ali/viet:llp/exp/mling.test/ali/cant"
  train=./llp/exp/mling.test/$dataid/train
  validating=./llp/exp/mling.test/$dataid/validating
  train_label="ark:gzip -cd llp/exp/mling.test/trnlabel/train/ali2post.gz|"
  validating_label="ark:gzip -cd llp/exp/mling.test/trnlabel/validating/ali2post.gz|"
  dnn_dir=$dir/nnet

  train_scheduler="steps/nnet/train_scheduler_mling.sh"
  trnCmd="steps/nnet/train_mling.sh --hid-dim $hid_dim  --copy-feats false --train-scheduler $train_scheduler"

  echo "$0: test_train started @ `date`"
  $trnCmd \
  --feature-transform $feature_transform \
  --dbn $dbn \
  --hid-layers 0  \
  --learn-rate $learn_rate \
  $train $validating  "$train_label" \
  "$validating_label" $lang "$alidirs"  $dnn_dir || exit 1;
fi
nnet_dir=$dir/nnet_viet
if $make_lang_dep_nnet; then
  echo "$0: make_lang_dep_nnet started @ `date`"
  lang=llp/data/lang
  lang_nnet=$dir/nnet/viet/final.nnet
  lang_alidir=$dir/ali/viet/train
  alidir=$lang_alidir
  mling_nnet=$dir/nnet/final.nnet
  [ -d $nnet_dir/log ] || mkdir -p $nnet_dir/log
  labels_tr_pdf="ark:ali-to-pdf $alidir/final.mdl \"ark:gunzip -c $alidir/ali.*.gz |\" ark:- |" # for analyze-counts.
  analyze-counts --verbose=1 --binary=false "$labels_tr_pdf" $nnet_dir/ali_train_pdf.counts 2>$nnet_dir/log/analyze_counts_pdf.log || exit 1
  copy-transition-model --binary=false $alidir/final.mdl $nnet_dir/final.mdl || exit 1
  cp $alidir/tree $nnet_dir/tree || exit 1
  labels_tr_phn="ark:ali-to-phones --per-frame=true $alidir/final.mdl \"ark:gunzip -c $alidir/ali.*.gz |\" ark:- |"
  analyze-counts --verbose=1 --symbol-table=$lang/phones.txt "$labels_tr_phn" /dev/null 2>$nnet_dir/log/analyze_counts_phones.log || exit 1
  nnet-concat "nnet-copy --remove-last-layers=2 $mling_nnet - |" \
  $lang_nnet $nnet_dir/final.nnet 2> $nnet_dir/log/concat_nnet.log || exit 1 
  [ ! -f $nnet_dir/final.feature_transform ] && cp -rL $(dirname $mling_nnet)/final.feature_transform  $nnet_dir || exit 1
  cp $(dirname $mling_nnet || { echo "$0: ERROR, mling_nnet $mling_nnet is not ready !"; exit 1; } )/{delta_order,norm_vars} $nnet_dir/  2>/dev/null
fi
hid_layers=1
dir=llp/exp/mling.trap.hid_dim1024.splice5.layer5
nnet_dir=$dir/nnet_viet_retrained_hid_layers$hid_layers
if $train_front_layers; then
  echo "$0: train_front_layers started @ `hostname` "
  lang=llp/data/lang
  [ -d $nnet_dir ] || mkdir -p $nnet_dir
  feature_transform=$dir/nnet/tmp.feature_transform
  train=llp/exp/nnet.test/fbank22_pitch.trap/fbank22_pitch/train
  validating=llp/exp/nnet.test/fbank22_pitch.trap/fbank22_pitch/validating
  mling_nnet=$dir/nnet/final.nnet
  mling_feature_transform=$dir/nnet/final.feature_transform
  train_ali=llp/exp/nnet.test/fbank22_pitch.trap/ali/train
  validating_ali=llp/exp/nnet.test/fbank22_pitch.trap/ali/validating

  nnet-concat  $mling_feature_transform  "nnet-copy --remove-last-layers=2 $mling_nnet -|" $feature_transform
  steps/nnet/train.sh --tanh true --learn-rate 0.008 --feature-transform $feature_transform \
  --hid-layers $hid_layers  $train $validating $lang $train_ali $validating_ali $nnet_dir || exit 1
fi
if $test_decode; then
  echo "$0: test_decode started @ `date`"
  data=llp/data/fbank22_pitch/dev
  sdir=llp/exp/tri4a
  graph=$sdir/graph
  decode_dir=$nnet_dir/decode_dev
  steps/nnet/decode.sh --nj 30 --cmd "$train_cmd" $graph $data $decode_dir || exit 1

fi

rexp=llp2/exp
rdata=llp2/data

langnames="viet:cant"
langs="$rdata/lang:../cant101/$rdata/lang"
trndatas="$rdata/train/plp_pitch:../cant101/$rdata/train/plp_pitch"
sdirs="$rexp/tri4a:../cant101/$rexp/tri4a"
dataid=fbank22_pitch
hid_dim=$(echo $preTrnCmd | perl -e '$s=<>; if($s =~ m/--nn-depth\s+(\d+)/){print $1; }else{exit 1;}') || exit 1
splice=$(echo $nnet_opts | perl -e '$s=<>; if($s =~ m/--splice\s+(\d+)/){print $1;}else{exit 1;}') || exit 1
hid_layers=$(echo $nnet_opts | perl -e '$s=<>; if($s =~ m/--hid-layers\s+(\d+)/){print $1;}else{exit 1;}')  || exit 1
dir=$rexp/mling.hid_dim$hid_dim.splice$splice.layer$hid_layers
enabled=
featdir=$(pwd)/$rexp/features
if $cnn_train; then
  echo "$0: cnn_train started @ `date`"
  if [ ! -z $cnn_source_dir ]; then
    for x in train_feat validating_feat train_label validating_label alidirs; do
      [ ! -e $cnn_source_dir/$x ] && { echo "$0: cnn_train: ERROR, source file $x expeced in $cnn_source_dir "; exit 1; }
    done
    [ -d $dir ] || mkdir -p $dir
    cp $cnn_source_dir/{train_feat,validating_feat,train_label,validating_label,alidirs} $dir/ || exit 1
  fi
  alidirs=$dir/ali
  fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
  train_scheduler="steps/nnet/train_scheduler_mling.sh"
  trnCmd="steps/nnet/train_mling.sh --copy-feats false $nnet_opts  --prepend-cnn-type cnn1d --train-scheduler $train_scheduler"
  source/run-nnet-mling.sh --dataid $dataid --featdir $featdir \
  ${enabled:+--PrepareData true --DoAli true} \
  ${enabled:+--MakeFBankFeat true --fbankCmd "$fbankCmd"}  --usefbankdata true \
  --cnn-initial-train true --trnCmd "$trnCmd" \
  --cnn-pretrain true --preTrnCmd "$preTrnCmd" --cnn-xent-train true \
  "$langnames" "$langs" "$trndatas" "$trndatas" "$sdirs" ali_unknown $dir
fi
nnet_dir=$dir/nnet_viet;
if $cross_ling_transfer; then
  echo "$0: cross_ling_transfer started @ `date`"
  lang=$rdata/lang
  [ -d $nnet_dir ] || mkdir -p $nnet_dir
  feature_transform_cnn=$dir/cnn_xent_train/export.cnn.feature_transform
  train=llp2/exp/cnn.4.1024/fbank22_pitch/train
  validating=llp2/exp/cnn.4.1024/fbank22_pitch/validating
  mling_nnet=$dir/cnn_xent_train/final.nnet
  mling_feature_transform=$dir/cnn_xent_train/final.feature_transform
  train_ali=llp2/exp/cnn.4.1024/ali/train
  validating_ali=llp2/exp/cnn.4.1024/ali/validating
  nnet-concat  $mling_feature_transform  "nnet-copy --remove-last-layers=2 $mling_nnet -|" $feature_transform_cnn
  steps/nnet/train.sh --learn-rate $cross_learn_rate --feature-transform $feature_transform_cnn \
  --hid-layers $cross_hid_layers  $train $validating $lang $train_ali $validating_ali $nnet_dir || exit 1

fi

if $cross_ling_test; then
  echo "$0: cross_ling_test started @ `date`"
  data=$rdata/dev/fbank22_pitch
  sdir=$rexp/tri4a
  graph=$sdir/graph
  decode_dir=$nnet_dir/decode_dev
  steps/nnet/decode.sh --nj 40 --cmd "$train_cmd" $graph $data $decode_dir || exit 1

fi
