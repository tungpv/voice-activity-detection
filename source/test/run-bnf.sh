#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=run.pl
nj=20
make_fbank=false
fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
sdatadir=
sdataName=
make_bnf=false
src_bndir=
bnName=
gmm_train=false
gmmtrain_opts="--train-id a --train-dataname train --tri-state 2500 --tri-pdf 40000"
gmm_test=false
scoring_opts="--min-lmwt 8 --max-lmwt 24"
tgt_dir=
# end options

echo
echo "$0 $@"
echo

. utils/parse_options.sh || exit 1

function PrintOptions {
  cmdName=$(echo $0| perl -pe 's/^.*\///g;')
  cat <<END
$cmdName [options]:
cmd					# value, "$cmd"
nj					# value, "$nj"
make_fbank				# value, $make_fbank
fbankCmd				# value, "$fbankCmd"
make_bnf				# value, $make_bnf
sdatadir				# value, "$sdatadir"
sdataName				# value, "$sdataName"
src_bndir				# value, "$src_bndir"
bnName					# value, "$bnName"
gmm_train				# value, $gmm_train
gmmtrain_opts				# value, "$gmmtrain_opts"				
gmm_test				# value, $gmm_test
scoring_opts				# value, "$scoring_opts"
sdir					# value, "$sdir"
tgt_dir					# value, "$tgt_dir"

END
}

PrintOptions;

rexp=$rdir/exp
if $make_fbank; then
  echo "$0: make_fbank started @ `date`"
  [ ! -z $sdatadir ] || \
  { echo "$0: fbanktest_prepare_data: ERROR, dotest_data is not specified"; exit 1; }
  num=1
  for sdata in $(echo $sdatadir| perl -pe 's/[:]/ /g;'); do
    data=$(dirname $sdata)/fbank_pitch
    dataname=$(echo "$sdataName" | cut -d: -f$num)
    feat=$rexp/feature/fbank_pitch/$dataname
    [ -d $data ] || mkdir -p $data 
    cp $sdata/* $data/; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
    $fbankCmd --cmd "$cmd" --nj $nj $data $feat/log $feat/data  || exit 1
    steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1
    utils/fix_data_dir.sh $data
    num=$[num+1]
  done
  "$0: make_fbank ended @ `date`"
fi
gmmdir=$tgt_dir/bnf$bnName.gmm
if $make_bnf; then
  echo "$0: make_bnf started @ `date`"
  [ -d $gmmdir ] || mkdir -p $gmmdir
  testdata=
  testdata_name=
  num=1
  [ ! -z  $sdatadir ] && [ ! -z $src_bndir ] || \
  { echo "$0: make_bnf: ERROR, sdatadir and src_bndir not specified"; exit 1; }
  for x in $(echo $sdatadir | perl -pe 's/[:]/ /g;'); do
    data=$(dirname $x)/bnf$bnName
    sdata=$(dirname $x)/fbank_pitch
    dataname=$(echo "$sdataName" | cut -d: -f$num)
    feat=$rexp/feature/bnf$bnName/$dataname
    steps/nnet/make_bn_feats.sh --cmd "$cmd" --nj $nj $data $sdata $src_bndir $feat/log $feat/data || exit 1
    steps/compute_cmvn_stats.sh $data $feat/log $feat/data || exit 1
    utils/fix_data_dir.sh $data
    [ $num -eq 2 ] && { testdata=$data; testdata_name=$dataname; }
    [ $num -gt 2 ] && { testdata="$testdata:$data"; testdata_name="$testdata_name:$dataname"; }
    num=$[num+1]
  done
  [ ! -z $testdata ] && { echo "$testdata" > $gmmdir/testdata; }
  [ ! -z $testdata_name ] && echo "$testdata_name" > $gmmdir/testdata_name
  echo "$0: make_bnf done @ `date`"
fi

if $gmm_train; then
  echo "$0: train_gmm started @ `date`"
  sdata=$(echo $sdatadir | cut -d: -f1)
  data=$(dirname $sdata )/bnf$bnName
  source/run-gmm-hmm.sh --cmd "$cmd" --nj $nj \
  --mono-train true \
  --tri1_train true --tri2_train true --tri3_train true \
  --tri5_train true $gmmtrain_opts $data $lang $gmmdir || exit 1
  echo "$0: train_gmm ended @ `date`"
fi
if $gmm_test; then
  echo "$0: gmm_test started @ `date`"
  [ ! -f $gmmdir/testdata ] && \
  { echo "$0: gmm_test: ERROR, testdata expected in $gmmdir"; exit 1; }
  num=1
  for data in $( cat $gmmdir/testdata| perl -pe 's/[:]/ /g;'); do
    dataname=$(cat $gmmdir/testdata_name|cut -d: -f$num)
    sdir=$gmmdir/tri4a
    graph=$sdir/graph
    mkgraph.sh $lang $sdir  $graph
    decode_dir=$sdir/decode_$dataname
    steps/decode_fmllr.sh --cmd "$cmd" --nj $nj --scoring-opts "$scoring_opts" \
    $graph $data  $decode_dir || exit 1
  done
  echo "$0: gmm_test ended @ `date`"
fi
