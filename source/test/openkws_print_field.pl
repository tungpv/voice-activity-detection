#!/usr/bin/perl

use strict;
use warnings;
use open qw(:std :utf8);
use XML::Simple;
use Data::Dumper;

print STDERR "\n$0 ", join(" ", @ARGV), "\n";
if (scalar @ARGV != 1) {
  print STDERR "\n\nExample: cat x | $0 1 ";
}
my ($nIndex) = @ARGV;
if ($nIndex < 1) {
  print STDERR "$0: ERROR, field index($nIndex) should be over zero\n";
  exit 1;
}
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/[\s]+/);
  print $A[$nIndex-1], "\n";
}
print STDERR "$0: stdin ended\n";
