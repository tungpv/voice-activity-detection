#!/usr/bin/perl

use strict;
use warnings;
use open qw(:std :utf8);
use XML::Simple;
use Data::Dumper;
use utf8;
if (scalar @ARGV != 1) {
  print STDERR "\n\nExample: $0 kwlist.xml > keywords.txt\n\n";
  exit 1;
}
my $sFileName =shift @ARGV;
my $data = XMLin($sFileName);
foreach my $kwentry (@{$data->{kw}}) {
  # print Dumper ($kwentry);
  print "$kwentry->{kwid}\t", lc $kwentry->{kwtext},"\n";
}
