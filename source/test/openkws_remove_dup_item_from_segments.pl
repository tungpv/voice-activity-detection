#!/usr/bin/perl 
use warnings;
use strict;

print STDERR "\n$0 ", join " ", @ARGV, "\n";
if(scalar @ARGV != 1) {
  die "$0: ERROR, source segment expected\n";
}
sub TimeOverlapped {
  my ($a1, $a2) = @_;
  return 0 if($$a1[0]>=$$a2[1] || $$a1[1]<=$$a2[0]);
  return 1; 
}
sub Overlapped {
  my ($lab, $stInfo, $vocab) = @_;
  if (exists $$vocab{$lab}) {
    my @aInfo = split(/\s+/, $stInfo);
    my $aRef = $$vocab{$lab};
    for(my $i=0; $i < scalar @$aRef; $i++) {
      my $ref_stInfo = $$aRef[$i];
      my @ref_aInfo = split(/\s+/, $ref_stInfo); 
      return 1 if TimeOverlapped(\@aInfo, \@ref_aInfo);
    }
  }
  return 0;
}

my $sFileName = shift @ARGV;
open (F, "$sFileName") or
die "$0: file $sFileName cannot open\n";
my %vocab = ();
while(<F>) {
  chomp;
  my @A =split(/\s+/);
  die "$0: ERROR, abnormal segment\n" if scalar @A != 4;
  my $lab = $A[1];
  my $sbInfo = "$A[2] $A[3]";
  if( exists $vocab{$lab}) {
    my $refA = $vocab{$lab};
    push @$refA, $sbInfo;
  } else {
    my @tempA = ();
    $vocab{$lab} = \@tempA;
    my $refA = $vocab{$lab};
    push @$refA, $sbInfo;
  }
}
close F;
print STDERR "stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A =split(/\s+/);
  die "$0: ERROR, abnormal segment\n" if scalar @A != 4;
  my $lab = $A[1];
  my $sbInfo = "$A[2] $A[3]";
  next if Overlapped ($lab, $sbInfo, \%vocab) == 1;
  print "$_\n";
}
print STDERR "stdin ended\n";
