#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd=run.pl
nj=20
dataname=
run3_gmm_hmm=false
factor1_opts="--factor1 0.3 --factor1-state 1500 --factor1-pdf 15000"
factor2_opts="--factor2 0.6 --factor2-state 2100 --factor2-pdf 21000"
full_opts=" --full-state 2500 --full-pdf 36000"
run1_gmm_hmm=false
run1_opts=
dotest_opts=
steps=
# end options

. parse_options.sh

function PrintOptions {
cat <<END

$0 [options]:
cmd			# value, "$cmd"
nj                      # value, $nj
dataname                # value, "$dataname"
run3_gmm_hmm		# value, $run3_gmm_hmm
factor1_opts		# value, "$factor1_opts"
factor2_opts		# value, "$factor2_opts"
full_opts		# value, "$full_opts"
run1_gmm_hmm		# value, $run1_gmm_hmm
run1_opts		# value, "$run1_opts"
dotest_opts		# value, "$dotest_opts"
steps			# value, "$steps"

END
}

PrintOptions;
if [ $# -ne 3 ]; then
  echo 
  echo "$0 <data> <lang> <tgt_dir>"
  echo
  exit 1
fi
data=$1
lang=$2
rdir=$3

if [ ! -z "$steps" ]; then
  for x in $(echo $steps|sed 's/[,:]/ /g'); do
    index=$(printf "%02d" $x);
    declare step$index=1
  done
fi

if $run3_gmm_hmm; then
  echo "$0: run3_gmm_hmm started @ `date`"
  source/run3-gmm-hmm.sh  --cmd "$cmd" --nj $nj \
  $dotest_opts \
  ${step01:+--factor1-train true $factor1_opts}   \
  ${step02:+--factor1-test true} \
  ${step03:+--factor2-train true $factor2_opts} \
  ${step04:+--factor2-test true} \
  ${step05:+--full-train true $full_opts} \
  ${step06:+--full-test true} \
  $data $lang  $rdir || exit 1
fi

if $run1_gmm_hmm; then
  echo "$0: run1_gmm_hmm started @ `date`"
  source/run3-gmm-hmm.sh --cmd "$cmd" --nj $nj \
  $dotest_opts    $run1_opts \
  ${disabled:+--onepass-train true} --onepass-test true \
  $data $lang $rdir || exit 1
fi
