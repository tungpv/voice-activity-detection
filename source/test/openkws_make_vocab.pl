#!/usr/bin/perl 
use strict;
use warnings;
use open qw(:std :utf8);
use XML::Simple;
use Data::Dumper;

print STDERR "\n$0 ", join(" ", @ARGV), "\n";

if (scalar @ARGV != 5 ) {
  print STDERR "\n\nExample: cat dev.txt| $0 <oov_rate> <min_oov_decrease> <words-per-batch> <vocab-to-select> <source-vocab>\n\n";
  exit 1;
}
my ($fOovRate, $fMiniDelta, $nStride, $sVocabFile1, $sVocabFile2) = @ARGV;
if ($fOovRate < 0 || $fOovRate > 1.0) {
  die "$0: ERROR, oov_rate ($fOovRate) should be [0 ,1]\n";
}
my %vocab = ();
sub LoadVocab {
  my ($sFile, $vocab) = @_;
  open(File, "$sFile") or 
  die "$0: LoadVocab: ERROR, file $sFile cannot open\n";
  while(<File>) {
    chomp;
    s/^[\s]+//g;
    m/(\S+)/;
    $$vocab{$1} ++;
  }
  close File;
}
sub GetOovRate {
  my ($vocab, $devVocab, $aOovRate) = @_;
  my $nInV = 0;
  my $nOoV = 0;
  foreach my $w (keys %$devVocab) {
    if(exists $$vocab{$w}) {
      $nInV += $$devVocab{$w};
    } else {
      $nOoV += $$devVocab{$w};
    }
  }
  if ($nInV ==0 && $nOoV == 0) {
    die "$0: ERROR, empty dev file\n";
  }
  my $nVocabSize = keys %$vocab; 
  my $f_oov_rate = $nInV/($nInV+$nOoV);
  my $fOovRate = sprintf ("%.4f",1 - $f_oov_rate);
  print STDERR "$nVocabSize, $nInV, $nOoV, $fOovRate\n";
  push (@$aOovRate, $fOovRate);
  return $fOovRate;
}
LoadVocab($sVocabFile2, \%vocab);
print STDERR "$0: stdin expected\n";
my %devVocab = ();
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  for(my $i = 0; $i < @A; $i ++) {
    my $w = $A[$i];
    $devVocab{$w} ++;
  }
}
print STDERR "$0: stdin ended\n";
open (File, "$sVocabFile1") or 
die "$0: ERROR, file $sVocabFile1 cannot open\n";
my @A = ();
while(<File>) {
  chomp;
  s/^[\s]+//g;
  m/^(\S+)/;
  push (@A, $1);
}
close File;
my $nWord = scalar @A;
my ($i, $j) = (0, $nStride); 
my $fCurOovRate = 1.0;
my @aOovRate = ();
my $fDelta = 1.0;
for($j = $nStride; $j < $nWord; $i=$j, $j+=$nStride) {
  while($i < $j) {
    my $w = $A[$i]; 
    $vocab{$w} ++; 
    $i ++;
  } 
  $fCurOovRate = GetOovRate(\%vocab, \%devVocab, \@aOovRate);
  last if ($fCurOovRate < $fOovRate);
  my $nIter = scalar @aOovRate;
  if( $nIter > 1) {
    $fDelta = $aOovRate[$nIter-2] - $aOovRate[$nIter-1]; 
  }
  last if $fDelta < $fMiniDelta;
}
if ($fCurOovRate > $fOovRate && $i < $nWord && $fDelta > $fMiniDelta ) {
  while($i < $nWord) {
    my $w = $A[$i];
    $vocab{$w} ++; 
    $i ++;
  }
  GetOovRate(\%vocab, \%devVocab);
}
foreach my $w ( sort {$a cmp $b} keys %vocab ) {
  print $w, "\n";
}

