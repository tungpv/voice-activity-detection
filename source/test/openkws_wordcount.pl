#!/usr/bin/perl 
use strict;
use warnings;
use open qw(:std :utf8);
use XML::Simple;
use Data::Dumper;

print STDERR "\n$0 ", join(" ", @ARGV), "\n";
if (scalar @ARGV != 3) {
  print STDERR "\n\nUsage: $0 <graphemes> <in-vocab-count.txt> <oov-vocab-count.txt>\n\n";
  exit 1;
}
sub LoadGraphemeSet {
  my ($sFName, $vocab) = @_;
  open(File, "$sFName") or die "$0: ERROR, file $sFName cannot open";
  while(<File>) {
    chomp;
    s/^[\s]+//g;
    m/(\S+)/ or next;
    $$vocab{$1} ++;
  }
  close File;
}
sub IsBelongTo {
  my ($sWord, $vocab) = @_;
  my $sOldWord = $sWord;
  # $sWord =~ s/[\'\-_]//g;
  my @A = split(//, $sWord);
  my $nCount = 0;
  for(my $i = 0; $i < @A; $i ++) {
    my $sGrapheme = $A[$i];
    return 0 if(not exists $$vocab{$sGrapheme});
    $nCount ++;
  }
  return 0 if $nCount == 0;
  # print STDERR "in-vocab-word: $sOldWord\t$sWord\t", join(" ", @A), "\n";
  return 1;
}
my ($graphemes, $inVocabFile, $oovVocabFile) = @ARGV;
my %vocab = ();
LoadGraphemeSet($graphemes, \%vocab);
print STDERR "$0: stdin expected\n";
my %countInVocab = ();
my %countOovVocab = ();
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  for(my $i = 0; $i < @A; $i++) {
    my $sWord = $A[$i];
    if(IsBelongTo($sWord, \%vocab)) {
      $countInVocab{$sWord} ++;
    } else {
      $countOovVocab{$sWord} ++;
    }
  }
}
print STDERR "$0: stdin ended\n";
open (File, "$inVocabFile") or 
die "$0: ERROR, file $inVocabFile cannot open\n";
foreach my $w (sort{$countInVocab{$b}<=>$countInVocab{$a}} keys %countInVocab) {
  print File "$w\t$countInVocab{$w}\n";
}
close File;
open (File2, "$oovVocabFile") or 
die "$0: ERROR, file $oovVocabFile cannot open\n";
foreach my $w (sort{$countOovVocab{$b}<=>$countOovVocab{$a}} keys %countOovVocab) {
  print File2 "$w\t$countOovVocab{$w}\n";
}
close File2;

