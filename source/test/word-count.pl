#!/usr/bin/perl 
use warnings;
use strict;

use open qw(:std :utf8);

print STDERR "\n$0 ", join(" ", @ARGV), "\n";

print STDERR "$0: stdin expected\n";
my %vocab = ();
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  for(my $i = 0; $i < @A; $i ++) {
    my $w = $A[$i];
    $vocab{$w} ++;
  }
}
print STDERR "$0: stdin ended\n";

foreach my $w (keys %vocab) {
  print "$w\t$vocab{$w}\n";
}
