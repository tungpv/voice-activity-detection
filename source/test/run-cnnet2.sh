#!/bin/bash

. path.sh
. cmd.sh

# begin options

prepare_fbank_data=false
cnn_initial_train=false
cnn_pretrain=false
cnn_xent_train=false
cnn_test=false
cnn_train=false
hid_dim=1024
nn_depth=4
dataid=fbank22_pitch
linkdata=
DoAll=false
enabled=
nnetdir=

# end options

. parse_options.sh || exit 1

function PrintOptions {
  cat <<END

Usage $0 [options]:
prepare_fbank_data           # value, $prepare_fbank_data
cnn_initial_train            # value, $cnn_initial_train
cnn_pretrain                 # value, $cnn_pretrain
cnn_xent_train               # value, $cnn_xent_train
cnn_test                     # value, $cnn_test
cnn_train                    # value, $cnn_train
hid_dim                      # value, $hid_dim
nn_depth                     # value, $nn_depth
dataid                       # value, $dataid
linkdata                     # value, "$linkdata"
DoAll                        # value, $DoAll
enabled                      # value, $enabled
nnetdir                      # value, "$nnetdir"

END
}
PrintOptions
rdata=llp2/data
rexp=llp2/exp
lang=$rdata/lang
trndata=$rdata/train/plp_pitch
alidata=$trndata
dataname=train
sdir=$rexp/tri4a
dir=$rexp/cnn.$dataid.$hid_dim
fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf "
if $DoAll; then
  [ -z $linkdata ] && prepare_fbank_data=true
  cnn_initial_train=true
  cnn_pretrain=true
  cnn_xent_train=true
  cnn_xent_test=true
fi
if $prepare_fbank_data; then
  echo "$0: prepare_fbank_data started @ `date`"
  [ -d $dir ] || mkdir -p $dir
  for x in train validating; do
    sdata=$rexp/nnet.fmllr40/trndata/$x
    data=$dir/$dataid/$x
    [ -d $data ] || mkdir -p $data
    feat=/data/users/hhx502/w2015/monoling/viet107/$rexp/features/$dataid/$x
    cp $sdata/* $data
    rm $data/{feats.scp,cmvn.scp} 2>/dev/null
    $fbankCmd --cmd "$train_cmd" --nj 40 $data $feat/log $feat/data  || exit 1
    steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1
    utils/fix_data_dir.sh $data
  done
  fi
alidir=$dir/ali
if $cnn_initial_train; then
  echo "$0: cnn_initial_train started @ `date`"
  if [ ! -z $linkdata ]; then
    source_link=$linkdata
    [ ! -e $source_link ] && { echo "$0:cnn_initial_train:ERROR, source_link $source_link does not exist"; exit 1; }
    abs_dir=$(cd $source_link; pwd)
    [ -z $abs_dir ] && { echo "$0:cnn_initial_train:ERROR, source_link $source_link is empty"; exit 1; }
    tgt_dir=$dir/$dataid
    [ -d $tgt_dir ] || mkdir -p $tgt_dir
    for x in train validating; do
      [ ! -d $abs_dir/$x ] && { echo "$0:cnn_initial_train:ERROR, source folder $abs_dir/$x expected"; exit 1; }
      (cd $tgt_dir; [ -h $x ] && unlink $x;  ln -s $abs_dir/$x $x)
    done
  fi
  [ -d $alidir ] || mkdir -p $alidir
  source_alidir=$rexp/tri4a
  abs_alidir=$(cd $source_alidir; pwd)
  for x in train validating; do
    [ -h $alidir/$x ] && unlink $alidir/$x
    ( cd $alidir; ln -s $abs_alidir/ali_train $x )
  done
  trnCmd="steps/nnet/train.sh --apply-cmvn true --norm-vars true --delta-order 2 --hid-dim $hid_dim --splice 10 --prepend-cnn-type cnn1d --hid-layers 2 --learn-rate 0.008"
  source/run-cnnet.sh --dataid $dataid \
  --trnCmd "$trnCmd" --usefbankdata true --cnn-initial-train true --alidir $alidir  $lang $trndata $trndata $dir
fi 

if $cnn_pretrain; then
  echo "$0: cnn_pretrain started @ `date`"
  preTrnCmd="steps/nnet/pretrain_dbn.sh --nn-depth $nn_depth --hid-dim $hid_dim --rbm-iter 2 --input-vis-type bern --param-stddev-first 0.05 --param-stddev 0.05"
  source/run-cnnet.sh --dataid $dataid --preTrnCmd "$preTrnCmd" --usefbankdata true --cnn-pretrain true --alidir $alidir \
  $lang $trndata $trndata $dir || exit 1
fi

if $cnn_xent_train; then
  echo "$0: cnn_xent_train started @ `date`"
  source/run-cnnet.sh --dataid $dataid --usefbankdata true  --alidir $alidir --cnn-xent-train true \
  $lang $trndata $trndata $dir || exit 1

fi
if $cnn_test; then
  echo "$0: cnn_test started @ `date`"
  [ -z $nnetdir ] && { echo "$0:cnn_test:ERROR, nnetdir is not specified"; exit 1; }
  [ ! -f $nnetdir/final.nnet ] && { echo "$0:cnn_test:ERROR, final.nnet is not in $nnetdir"; exit 1; }
  x=dev
  data=$rdata/$x/fbank22_pitch
  if [ ! -f $data/feats.scp ]; then
    sdata=$rdata/$x;
    [ -d $data ] || mkdir -p $data
    cp $sdata/* $data
    rm $data/{feats.scp,cmvn.scp} 2>/dev/null
    utils/fix_data_dir.sh $data
    feat=/data/users/hhx502/w2015/monoling/viet107/$rexp/features/fbank22_pitch/$x
    $fbankCmd --cmd "run.pl" --nj 20 $data $feat/log $feat/data  || exit 1
    steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1
    utils/fix_data_dir.sh $data
  fi
  graph=$sdir/graph
  decode_dir=$nnetdir/decode_$x
  steps/nnet/decode.sh --nj 40 --cmd "$train_cmd"  $graph $data $decode_dir || exit 1;
fi

if $cnn_train; then
  echo "$0: cnn_train started @ `date`"
  [ -z $linkdata ] && { echo "$0:cnn_train:ERROR,linkdata is not specified"; exit 1; }
  source_dir=$(cd $linkdata; pwd)
  for x in ali fbank22_pitch; do
    [ ! -e $source_dir/$x ] && { echo "$0:cnn_train:ERROR, source $x expected in $source_dir"; exit 1; }
  done
  dataid=fbank22_pitch
  dir=$rexp/cnn.$nn_depth.$hid_dim
  [ -d $dir ] || mkdir -p $dir
  for x in ali  fbank22_pitch; do
    (cd $dir; [ -h $x ] || ln -s $source_dir/$x)
  done
  alidir=$dir/ali
  train=$dir/$dataid
  trnCmd="steps/nnet/train.sh --apply-cmvn true --norm-vars true --delta-order 2 --splice 10 --hid-dim $hid_dim --prepend-cnn-type cnn1d --hid-layers 2 --learn-rate 0.008"
  preTrnCmd="steps/nnet/pretrain_dbn.sh --nn-depth $nn_depth --hid-dim $hid_dim --rbm-iter 2 --input-vis-type bern --param-stddev-first 0.05 --param-stddev 0.05"
  source/run-cnnet.sh --dataid $dataid \
  --trnCmd "$trnCmd" --usefbankdata true ${enabled:+--cnn-initial-train true}  \
  --preTrnCmd "$preTrnCmd" --cnn-pretrain true \
  --cnn-xent-train true \
  --alidir $alidir  $lang $train $train $dir

fi
