#!/bin/bash

. path.sh
. cmd.sh

# begin options

prepare_fbank_data=false
cnn_initial_train=false
cnn_pretrain=false
cnn_xent_train=false
cnn_xent_test=false
cnn_train=false
# end options

. parse_options.sh || exit 1

function PrintOptions {
  cat <<END

Usage $0 [options]:
prepare_fbank_data           # value, $prepare_fbank_data
cnn_initial_train            # value, $cnn_initial_train
cnn_pretrain                 # value, $cnn_pretrain
cnn_xent_train               # value, $cnn_xent_train
cnn_xent_test                # value, $cnn_xent_test
cnn_train                    # value, $cnn_train

END
}
PrintOptions
rdata=llp2/data
rexp=llp2/exp
lang=$rdata/lang
trndata=$rdata/train/plp_pitch
alidata=$trndata
dataname=train
sdir=$rexp/tri4a
dataid=fbank22_pitch
dir=$rexp/cnn.$dataid
if $prepare_fbank_data; then
  echo "$0: prepare_fbank_data started @ `date`"
  fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf "
  [ -d $dir ] || mkdir -p $dir
  for x in train validating; do
    sdata=$rexp/nnet.fmllr40/trndata/$x
    data=$dir/$dataid/$x
    [ -d $data ] || mkdir -p $data
    feat=/data/users/hhx502/w2015/monoling/viet107/$rexp/features/$dataid/$x
    cp $sdata/* $data
    rm $data/{feats.scp,cmvn.scp} 2>/dev/null
    $fbankCmd --cmd "$train_cmd" --nj 40 $data $feat/log $feat/data  || exit 1
    steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1
    utils/fix_data_dir.sh $data
  done
fi
source_alidir=$rexp/tri4a
alidir=$dir/ali
abs_dir=$(cd $source_alidir; pwd)
if $cnn_initial_train; then
  echo "$0: cnn_initial_train started @ `date`"
  [ -d $alidir ] || mkdir -p $alidir
  for x in train validating; do
    [ -h $alidir/$x ] && unlink $alidir/$x
    ( cd $alidir; ln -s $abs_dir/ali_train $x )
  done
  trnCmd="steps/nnet/train.sh --apply-cmvn true --norm-vars true --delta-order 2 --splice 10 --prepend-cnn-type cnn1d --hid-layers 2 --learn-rate 0.008"
  source/run-cnnet.sh --dataid $dataid \
  --trnCmd "$trnCmd" --usefbankdata true --cnn-initial-train true --alidir $alidir  $lang $trndata $trndata $dir
fi 

if $cnn_pretrain; then
  echo "$0: cnn_pretrain started @ `date`"
  preTrnCmd="steps/nnet/pretrain_dbn.sh --nn-depth 4 --hid-dim 1024 --rbm-iter 20 --input-vis-type bern --param-stddev-first 0.05 --param-stddev 0.05"
  source/run-cnnet.sh --dataid $dataid --preTrnCmd "$preTrnCmd" --usefbankdata true --cnn-pretrain true --alidir $alidir \
  $lang $train $train $dir || exit 1
fi

if $cnn_xent_train; then
  echo "$0: cnn_xent_train started @ `date`"
  source/run-cnnet.sh --dataid $dataid --usefbankdata true  --alidir $alidir --cnn-xent-train true \
  $lang $train $train $dir || exit 1

fi
if $cnn_xent_test; then
  echo "$0: cnn_xent_test started @ `date`"
  data=./llp/data/fbank22_pitch/dev
  graph=$sdir/graph
  decode_dir=./llp/exp/cnn.test/cnn_xent_train/decode_dev
  steps/nnet/decode.sh --nj 30 --cmd "$train_cmd"  $graph $data $decode_dir || exit 1;
fi

if $cnn_train; then
  echo "$0: cnn_train started @ `date`"
  source_dir=$(pwd)/llp/exp/nnet.test/nnet.fbank22_pitch
  dataid=fbank22_pitch
  dir=llp/exp/cnn.test
  [ -d $dir ] || mkdir -p $dir
  for x in ali  fbank22_pitch trndata; do
    (cd $dir; [ -h $x ] || ln -s $source_dir/$x)
  done
  alidir=$dir/ali
  train=$dir/$dataid
  trnCmd="steps/nnet/train.sh --apply-cmvn true --norm-vars true --delta-order 2 --splice 10 --prepend-cnn-type cnn1d --hid-layers 2 --learn-rate 0.008"
  preTrnCmd="steps/nnet/pretrain_dbn.sh --nn-depth 4 --hid-dim 1024 --rbm-iter 2 --input-vis-type bern --param-stddev-first 0.05 --param-stddev 0.05"
  source/run-cnnet.sh --dataid $dataid \
  --trnCmd "$trnCmd" --usefbankdata true ${disabled01:+--cnn-initial-train true}  \
  --preTrnCmd "$preTrnCmd" --cnn-pretrain true \
  --cnn-xent-train true \
  --alidir $alidir  $lang $train $train $dir

fi
