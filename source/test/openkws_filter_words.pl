#!/usr/bin/perl

use strict;
use warnings;
use open qw(:std :utf8);
use XML::Simple;
use Data::Dumper;

print STDERR "\n$0 ", join(" ", @ARGV), "\n";

if (scalar @ARGV < 1) {
  print STDERR "\n\nExample: cat wordlist.txt| $0 words_to_be_removed \n\n";
  exit 1;
}
my $sFileName = shift @ARGV;
open(F, "$sFileName") or die "$0: ERROR, file $sFileName cannot open\n";
my %vocab = ();
while(<F>) {
  chomp;
  m/(\S+)/ or next;
  my $w = $1;
  print STDERR "$0: $w\n";
  $vocab{$w} ++;
}
close F;
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  m/(\S+)/ or next;
  my $w = $1;
  next if exists $vocab{$w};
  print "$_\n";
}
print STDERR "$0: stdin ended\n";
