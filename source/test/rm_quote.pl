#!/usr/bin/perl
use strict;
use warnings;

use open qw(:std :utf8);

print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  s/[\']/ /g;
  print;
}
print STDERR "$0: stdin ended\n";
