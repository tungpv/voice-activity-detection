#!/bin/bash

. path.sh
. cmd.sh

# begin options
train_fmllr_nnet=false
train_fbank_nnet=false
prepare_fmllr_feat=false
prepare_train_data=false
prepare_fbank_test=false
test_fmllr_nnet=false
test_trap_nnet=false
# end options

. parse_options.sh || exit 1

function Options {
  cat <<END

$0 [options]:
train_fmllr_nnet         # value, $train_fmllr_nnet
train_fbank_nnet         # value, $train_fbank_nnet
prepare_fmllr_feat       # value, $prepare_fmllr_feat
prepare_train_data       # value, $prepare_train_data
prepare_fbank_test       # value, $prepare_fbank_test
test_fmllr_nnet          # value, $test_fmllr_nnet
test_trap_nnet           # value, $test_trap_nnet
END
}

Options

lang=llp/data/lang
trndata=llp/data/train/plp_pitch
dataname=train
sdir=llp/exp/tri4a
dataid=fmllr40
dir=llp/exp/nnet.test/nnet.$dataid
if $train_fmllr_nnet; then
  dir=llp/exp/nnet.test/nnet.$dataid
  alidir=$dir/ali.$dataid
  validating_rate=0.1
  featdir=/media/kiwi_hhx502_usb/kws15/monoling/cant101/llp/exp/feat.$dataid
  splice=5
  nn_depth=7
  hid_dim=1024
  preTrnCmd="steps/nnet/pretrain_dbn.sh --nn-depth $nn_depth --hid-dim $hid_dim  --copy_feats_tmproot $dir --splice $splice"
  source/run-nnet.sh  --dataid $dataid   --usefmllrdata true \
  --PreTrain true --preTrnCmd "$preTrnCmd" --TrainNnet true --featdir $featdir \
  --sdir $sdir --alidir $alidir \
  $lang $trndata $trndata $dir || exit 1
fi
data=llp/data/fmllr/dev
if $prepare_fmllr_feat; then
  echo "$0: prepare_fmllr_feat started @ `date`"
  sdata=llp/data/dev/plp_pitch
  featdir=$dir/fmllr.feat
  feat=$featdir/dev
  sdir=llp/exp/tri4a
  transform_dir=$sdir/decode_dev
  nj=$(cat $transform_dir/num_jobs)
  [ -z $nj -o $nj -eq 0 ] && { echo "$0: ERROR, nj is not given"; exit 1; }
  steps/nnet/make_fmllr_feats.sh --cmd "$train_cmd"  --transform-dir $transform_dir \
  --nj $nj $data $sdata $sdir   $feat/log $feat/data || exit 1
  steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1
  fi

if $test_fmllr_nnet; then
  echo "$0: test_fmllr_nnet started @ `date`"
  graph=llp/exp/tri4a/graph
  decode_dir=$dir/nnet/decode_dev
  steps/nnet/decode.sh --nj 30 --cmd "$train_cmd" $graph $data $decode_dir || exit 1

fi

featdir=/media/kiwi_hhx502_usb/kws15/monoling/cant101/llp/exp/feat.$dataid
fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
if $prepare_train_data; then
  echo "$0: prepare_fbank_data started @ `date`"
  validating_rate=0.1
  source/run-nnet.sh --dataid $dataid --fbankCmd "$fbankCmd" --PrepareData true --MakeFBankFeat true --usefbankdata true --LinkAli true --featdir $featdir \
  --sdir $sdir --alidir $alidir \
  $lang $trndata $trndata $dir || exit 1
fi

splice=10
delta_order=2
nn_depth=7
hid_dim=1024
expid=".trap"
alidir=$dir/ali
if $train_fbank_nnet; then
  echo "$0: train_trap_nnet started @ `date`"
  preTrnCmd="isteps/nnet/pretrain_dbn_trap.sh --nn-depth $nn_depth --hid-dim $hid_dim  --copy_feats_tmproot $dir --apply-cmvn true --norm-vars true  --feat-type trap --splice $splice --delta-order $delta_order"
  # preTrnCmd="steps/nnet/pretrain_dbn.sh  --copy_feats_tmproot $dir --apply-cmvn true  --norm-vars true --splice $splice --delta-order $delta_order"
  trnCmd="steps/nnet/train.sh --copy_feats_tmproot $dir"
  source/run-nnet.sh --dataid $dataid   --usefbankdata true --PreTrain true --preTrnCmd "$preTrnCmd" \
  --expid "$expid" --trnCmd "$trnCmd"  --TrainNnet true \
  --sdir $sdir --alidir $alidir \
  $lang $trndata $trndata $dir || exit 1

fi

data=llp/data/$dataid/dev
if $prepare_fbank_test; then
  echo "$0: prepare_fbank_test started @ `date`"
  sdata=llp/data/dev/plp_pitch
  feat=$featdir/dev
  [ -d $data ] || mkdir -p $data
  cp $sdata/* $data/; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
  $fbankCmd --cmd "$train_cmd" --nj 10 $data $feat/log $feat/data  || exit 1
  steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1
  utils/fix_data_dir.sh $data
fi

if $test_trap_nnet; then
  echo "$0: test_trap_nnet started @ `date`"
  graph=llp/exp/tri4a/graph
  decode_dir=$dir/nnet/decode_dev
  steps/nnet/decode.sh --nj 30 --cmd "$train_cmd" $graph $data $decode_dir || exit 1
fi
