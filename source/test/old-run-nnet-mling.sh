#!/bin/bash

. path.sh
. cmd.sh

# begin options
prepare_feat=false
test_make_fbank_feat=false
test_doali=false
test_pretrain=false
test_train=false
make_lang_dep_nnet=false
test_decode=false
link_data=false
train_fmllr=false

# end options

. parse_options.sh


function Options {
  cmdName=`echo $0 | perl -pe 's/^.*\///g;'`
  cat<<END

$cmdName [options]:
prepare_feat                  # value, $prepare_feat
test_make_fbank_feat          # valie, $test_make_fbank_feat
test_doali                    # vlaue, $test_doali
test_pretrain                 # value, $test_pretrain
test_train                    # value, $test_train
make_lang_dep_nnet            # value, $make_lang_dep_nnet
test_decode                   # value, $test_decode
link_data                     # value, $link_data
train_fmllr                   # value, $train_fmllr

END
}

Options;

langnames="viet:cant"
langs="llp2/data/lang:../cant101/llp2/data/lang"
alidatas="llp2/data/train/plp_pitch:../cant101/llp2/data/train/plp_pitch"
sdirs="llp2/exp/tri4a:../cant101/llp2/exp/tri4a"
dir=llp2/exp/mling.feat.test
fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
if $prepare_feat; then
  fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
  source/run-nnet-mling.sh  --PrepareData true   \
  --DoAli true \
  --MakefMLLRFeat true \
  --MakeFBankFeat true --fbankCmd "$fbankCmd" \
  "$langnames" "$langs" "$alidatas" "$sdirs" $dir
fi
langnames="cant:pash:taga:turk"
langs="../cant101/flp2/data/lang:../pash104/flp2/data/lang:../taga106/flp2/data/lang:../turk105/flp2/data/lang"
alidatas="../cant101/flp2/data/train/plp_pitch:../pash104/flp2/data/train/plp_pitch:../taga106/flp2/data/train/plp_pitch:../turk105/flp2/data/train/plp_pitch"
sdirs="../cant101/flp2/exp/tri4a:../pash104/flp2/exp/tri7b:../taga106/flp2/exp/tri7b:../turk105/flp2/exp/tri7b"
dir=llp2/exp/mling.fmllr
if $train_fmllr; then
  hid_dim=2048
  nn_depth=5
  splice=5
  disabled=
  preTrnCmd="steps/nnet/pretrain_dbn.sh  --copy_feats_tmproot $dir --hid-dim $hid_dim --nn-depth $nn_depth --apply-cmvn true --norm-vars true  --splice $splice"
  train_scheduler="steps/nnet/train_scheduler_mling.sh"
  trnCmd="steps/nnet/train_mling.sh  --copy-feats false --train-scheduler $train_scheduler"

  source/run-nnet-mling.sh  ${disabled:+--PrepareData true}   \
  ${disabled:+--DoAli true} \
  ${disabled:+--MakefMLLRFeat true} --use_fmllr_feat true \
  ${disabled:+--PreTrain true --preTrnCmd "$preTrnCmd"} \
  --TrainNnet true --trnCmd "$trnCmd" \
  "$langnames" "$langs" "$alidatas" "$sdirs" $dir

fi
if $test_make_fbank_feat; then
  echo "$0: test_make_fbank_feat started @ `hostname` `date`"
  featdir=/media/kiwi_hhx502_usb/kws15/monoling/viet107/llp/exp/feat.fbank
  dataid=fbank22_pitch
  fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
  source/run-nnet-mling.sh --featdir $featdir --dataid $dataid --fbankCmd "$fbankCmd" --MakeFBankFeat true   \
  "$langnames" "$langs" "$trndatas" "$alidatas" "$sdirs" $dir

fi

if $test_doali; then
  echo "$0: test_doali started @ `hostname` `date`"
  source/run-nnet-mling.sh --DoAli true  \
  "$langnames" "$langs" "$trndatas" "$alidatas" "$sdirs" $dir
 
fi
sdata=llp/exp/mling.test
dir=llp/exp/mling.trap
if $link_data; then
  echo "$0: link_data started @ `date`"
  sdata_abs=$(cd $sdata; pwd)
  [ -d $dir ] || mkdir -p $dir
  (cd $dir; 
    for x in ali  fbank22_pitch  trndata  trnlabel; do
      [ -h $x ] && unlink $x
      ln -s $sdata_abs/$x
    done
  )
fi
dataid=fbank22_pitch
hid_dim=1024
nn_depth=7
if $test_pretrain; then
  echo "$0: test_pretrain started @ `date`"
  splice=10
  delta_order=2
  preTrnCmd="isteps/nnet/pretrain_dbn_trap.sh  --copy_feats_tmproot $dir --hid-dim $hid_dim --nn-depth $nn_depth   --apply-cmvn true --norm-vars true   --feat-type trap --splice $splice --delta-order $delta_order"
  source/run-nnet-mling.sh --dataid $dataid  --PreTrain true --preTrnCmd "$preTrnCmd" --usefbankdata true \
  "$langnames" "$langs" "$trndatas" "$alidatas" "$sdirs" $dir
fi

learn_rate=0.008
dnn_dir=$dir/nnet
pretrain_dir=$dir/pretrain_dbnfbank22_pitch
feature_transform=$pretrain_dir/final.feature_transform
dbn=$pretrain_dir/$nn_depth.dbn
lang=llp/data/lang
alidirs="llp/exp/mling.test/ali/viet:llp/exp/mling.test/ali/cant"
train=./llp/exp/mling.test/$dataid/train
validating=./llp/exp/mling.test/$dataid/validating
train_label="ark:gzip -cd llp/exp/mling.test/trnlabel/train/ali2post.gz|"
validating_label="ark:gzip -cd llp/exp/mling.test/trnlabel/validating/ali2post.gz|"
dnn_dir=$dir/nnet

train_scheduler="steps/nnet/train_scheduler_mling.sh"
trnCmd="steps/nnet/train_mling.sh --hid-dim $hid_dim  --copy-feats false --train-scheduler $train_scheduler"
if $test_train; then
  echo "$0: test_train started @ `date`"
  $trnCmd \
  --feature-transform $feature_transform \
  --dbn $dbn \
  --hid-layers 0  \
  --learn-rate $learn_rate \
  $train $validating  "$train_label" \
  "$validating_label" $lang "$alidirs"  $dnn_dir || exit 1;
fi

nnet_dir=$dir/nnet_viet
if $make_lang_dep_nnet; then
  echo "$0: make_lang_dep_nnet started @ `date`"
  lang=llp/data/lang
  lang_nnet=$dir/nnet/viet/final.nnet
  lang_alidir=$dir/ali/viet/train
  alidir=$lang_alidir
  mling_nnet=$dir/nnet/final.nnet
  [ -d $nnet_dir/log ] || mkdir -p $nnet_dir/log
  labels_tr_pdf="ark:ali-to-pdf $alidir/final.mdl \"ark:gunzip -c $alidir/ali.*.gz |\" ark:- |" # for analyze-counts.
  analyze-counts --verbose=1 --binary=false "$labels_tr_pdf" $nnet_dir/ali_train_pdf.counts 2>$nnet_dir/log/analyze_counts_pdf.log || exit 1
  copy-transition-model --binary=false $alidir/final.mdl $nnet_dir/final.mdl || exit 1
  cp $alidir/tree $nnet_dir/tree || exit 1
  labels_tr_phn="ark:ali-to-phones --per-frame=true $alidir/final.mdl \"ark:gunzip -c $alidir/ali.*.gz |\" ark:- |"
  analyze-counts --verbose=1 --symbol-table=$lang/phones.txt "$labels_tr_phn" /dev/null 2>$nnet_dir/log/analyze_counts_phones.log || exit 1
  nnet-concat "nnet-copy --remove-last-layers=2 $mling_nnet - |" \
  $lang_nnet $nnet_dir/final.nnet 2> $nnet_dir/log/concat_nnet.log || exit 1 
  [ ! -f $nnet_dir/final.feature_transform ] && cp -rL $(dirname $mling_nnet)/final.feature_transform  $nnet_dir || exit 1
  cp $(dirname $mling_nnet || { echo "$0: ERROR, mling_nnet $mling_nnet is not ready !"; exit 1; } )/{delta_order,norm_vars} $nnet_dir/  2>/dev/null
fi

if $test_decode; then
  echo "$0: test_decode started @ `date`"
  data=llp/data/fbank22_pitch/dev
  sdir=llp/exp/tri4a
  graph=$sdir/graph
  decode_dir=$nnet_dir/decode_dev
  steps/nnet/decode.sh --nj 30 --cmd "$train_cmd" $graph $data $decode_dir || exit 1
fi
