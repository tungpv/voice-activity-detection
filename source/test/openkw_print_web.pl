#!/usr/bin/perl 
use strict;
use warnings;
use open qw(:std :utf8);
use XML::Simple;
use Data::Dumper;

print STDERR "\n$0 ", join(" ", @ARGV), "\n";

print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  s/^[\s]+//g;
  s/[\-_\.!,;"~\/?:\$#@%^&\(\\)\|]/ /g;
  next if (/^$/);
  next if length($_) == 0;
  s/[\s]+/ /g;
  print lc $_, "\n";
}
print STDERR "$0: stdin ended\n";
