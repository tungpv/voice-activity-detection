#!/usr/bin/perl
use warnings;
use strict;
use open qw(:std :utf8);
sub NormalWordRatio {
  my ($wordArray) = @_;
  my $nTotal = scalar @$wordArray;
  my $nSil = 0;
  for(my $i = 0; $i < $nTotal; $i++) {
    my $sWord = $$wordArray[$i];
    if($sWord =~ m/^</) {
      $nSil ++;
    }
  }
  return ($nTotal - $nSil) / $nTotal;
}
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/\s+/);
  if(@A < 2) {
    print STDERR "empty: $_\n";
    next;
  }
  shift @A;
  if (NormalWordRatio(\@A) <= 0.5) {
    print STDERR "low NormalWordRatio: $_\n";
    next;
  }
  print "$_\n";
}
print STDERR "$0: stdin ended\n";
