#!/usr/bin/perl
use strict;
use warnings;
use open qw(:std :utf8);
use XML::Simple;
use Data::Dumper;

print STDERR "\n$0 ", join(" ", @ARGV), "\n";

if (scalar @ARGV != 1) {
  print STDERR "\n\nExample: cat x.txt | $0 map > mapped.txt\n\n";
  exit 1
}
my ($sMapFile) = @ARGV;
sub LoadMap {
  my ($sFile, $vocab) = @_;
  open(File, "$sFile") or 
  die "$0: LoadMap: ERROR, file $sFile cannot open\n";
  while(<File>) {
    chomp;
    my @A = split(/[\t\s]/);
    next if @A < 1;
    my $word = shift @A;
    $$vocab{$word} = join(" ", @A);   
  }
}
my %vocab = ();
LoadMap($sMapFile, \%vocab);
print STDERR "$0: stdin expected\n";
while(<STDIN>) {
  chomp;
  my @A = split(/[\s\t]/);
  next if @A < 1;
  my $s = "";
  for(my $i=0; $i< @A; $i++) {
    my $word = $A[$i];
    # $word =~ s/[_\-]/ /g;
    if(exists $vocab{$word}) {
      $s .= "$vocab{$word} ";
    } else {
      $s .= "$word ";
    }
  }
  $s =~ s/\s+/ /g;
  $s =~ s/\s+$//g;
  print $s, "\n";
}
print STDERR "$0: stdin ended\n";
