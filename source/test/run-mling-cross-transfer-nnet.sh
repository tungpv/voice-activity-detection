#!/bin/bash

. path.sh
. cmd.sh

# begin options
make_train_fbank=false
cross_mling_transfer=false
cross_mling_transfer_test=false
make_dev_fbank=false
monoling_dnn_training=false
monoling_test=false

# end options

. parse_options.sh

function Options {
  cmdName=`echo $0 | perl -pe 's/^.*\///g;'`
  cat<<END

$cmdName [options]:
make_train_fbank              # value, $make_train_fbank
cross_mling_transfer          # value, $cross_mling_transfer
cross_mling_transfer_test     # value, $cross_mling_transfer_test
make_dev_fbank                # value, $make_dev_fbank
monoling_dnn_training         # value, $monoling_dnn_training
monoling_test                 # value, $monoling_test

END
}

Options;

rdata=llp2/data
rexp=llp2/exp
trndata=$rdata/train/plp_pitch
sdir=$rexp/tri4a
dir=$rexp/nnet.mling.cross.transfer
lang=$rdata/lang
fbankCmd="steps/make_fbank_pitch.sh --fbank-config conf/fbank22.conf"
if $make_train_fbank; then
  echo "$0: make_train_fbank started @ `date`"
  featdir=$dir/fbank_pitch25
  source/run-nnet.sh --dataid fbank_pitch25  --featdir $featdir \
  --PrepareData true --MakeFBankFeat true --fbankCmd "$fbankCmd" \
  --sdir $sdir --DoAli true \
  $lang $trndata $trndata $dir || exit 1
fi

cross_nnet_dir=$dir;
cross_learn_rate=0.008
cross_hid_layers=0
hid_dim=2048
mling_nnet_dir=../viet107/llp/exp/mling.flp4/nnet
cross_learn_rate=0.008
cross_hid_layers=0
cross_hid_dim=2048
if $cross_mling_transfer; then
  echo "$0: cross_mling_transfer started @ `date`"
  train_feat=$(cat $dir/train_feat) || { echo "$0: cross_mling_transfer: ERROR, file train_feat expected"; exit 1; }
  validating_feat=$(cat $dir/validating_feat)|| { echo "$0: cross_mling_transfer: ERROR, file validating_feat expected"; exit 1; }
  train_label=$(cat $dir/train_label)|| { echo "$0: cross_mling_transfer: ERROR, file train_label expected"; exit 1; }
  validating_label=$(cat $dir/validating_label) || { echo "$0: cross_mling_transfer: ERROR, file validating_label expected"; exit 1; } 
  mling_nnet=$mling_nnet_dir/final.nnet
  mling_feature_transform=$mling_nnet_dir/final.feature_transform
  feature_transform=$mling_nnet_dir/mling.feature_transform
  nnet-concat  $mling_feature_transform  "nnet-copy --remove-last-layers=2 $mling_nnet -|" $feature_transform
  steps/nnet/train.sh --learn-rate $cross_learn_rate --feature-transform $feature_transform \
  --hid-layers $cross_hid_layers --hid-dim $cross_hid_dim \
  $train_feat $validating_feat $lang $train_label $validating_label $cross_nnet_dir || exit 1

fi
cross_dev=$rdata/dev/fbank_pitch25
if $make_dev_fbank; then
  echo "$0:make_dev_fbank started @ `date`"
  sdata=$rdata/dev; data=$cross_dev
  [ -d $data ] || mkdir -p $data
  cp $sdata/*  $data
  feat=$dir/fbank_pitch25/dev
  $fbankCmd --cmd "$train_cmd" --nj 20 $data $feat/log $feat/data || exit 1
  steps/compute_cmvn_stats.sh $data  $feat/log  $feat/data || exit 1
  utils/fix_data_dir.sh $data
 
fi
if $cross_mling_transfer_test; then
  echo "$0: test_decode started @ `date`"
  data=$cross_dev
  graph=$sdir/graph
  decode_dir=$cross_nnet_dir/decode_dev
  steps/nnet/decode.sh --nj 40 --cmd "$train_cmd" $graph $data $decode_dir || exit 1

fi
monoling_hid_dim=2048
monoling_hid_layers=5
monoling_splice=5
delta_order=2
monoling_dir=$rexp/dnn.trap.$monoling_hid_dim
if $monoling_dnn_training; then
  echo "$0: monoling_dnn_training started @ `date`"
  data_source_dir=$dir
  [ -d $monoling_dir ] || mkdir -p $monoling_dir
  cp $data_source_dir/{train_feat,validating_feat,train_label,validating_label} $monoling_dir || \
  { echo "$0: monoling_dnn_training: ERROR, copy data source $data_source_dir failed"; exit 1; }
  pretrain_opts="--splice $monoling_splice --delta-order $delta_order --apply-cmvn true --norm-vars true  --feat-type trap"
  pretrain_opts="$pretrain_opts --nn-depth $monoling_hid_layers  --hid-dim $monoling_hid_dim"
  preTrnCmd="isteps/nnet/pretrain_dbn_trap.sh  --copy_feats_tmproot $monoling_dir $pretrain_opts"
  source/run-nnet.sh  --PreTrain true --preTrnCmd "$preTrnCmd" --TrainNnet true \
  $lang x y  $monoling_dir || exit 1
fi
if $monoling_test; then
  echo "$0: monoling_test started @ `date`"
  data=$cross_dev
  graph=$sdir/graph
  decode_dir=$monoling_dir/nnet/decode_dev
  steps/nnet/decode.sh --nj 40 --cmd "$train_cmd" $graph $data $decode_dir || exit 1

fi
