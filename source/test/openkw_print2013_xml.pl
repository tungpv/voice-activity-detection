#!/usr/bin/perl 
use strict;
use warnings;
use open qw(:std :utf8);
use XML::Simple;
use Data::Dumper;

print STDERR "\n$0 ", join(" ", @ARGV), "\n";

print STDERR "$0: stdin expected\n";
my $bBOS = 0;
my $sUtt = "";
while(<STDIN>) {
  chomp;
  if(/<s/) {
    $bBOS = 1; next;
  }
  if ($bBOS == 1) {
    next if(/<time/);
    if(/<\/s/) {
      $sUtt =~ s/[\.!,;"~\/?:\$#@%^&\(\\)\|]/ /g;
      $sUtt =~ s/[\s]+/ /g;
      $sUtt =~ s/^[\s]+//g;
      print lc $sUtt, "\n";
      $bBOS = 0;
      $sUtt = "";
      next;
    }
    if (/<w/) {
      s/<w\s+[^\<]+>//g;
      s/<\/w.*$//g;
      s/^[\s+]//g;
      s/[\s+]$//g;
      $sUtt .= "$_ ";
    }
  }
}
print STDERR "$0: stdin ended\n";
