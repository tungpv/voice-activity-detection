#!/bin/bash

. path.sh
. cmd.sh

# begin options
cmd="run.pl"
nj=20
ubm_train=false
ubm_num_gauss=600
sgmm2mle_train=false
sgmm2mle_test=false
sgmm2mle_test_data=
sgmm2mle_test_dataname=
sgmm2mle_test_transform=
sgmm2_num_leave=2000
sgmm2_num_gauss=7000
index=5
lmname=
# end options

. parse_options.sh || exit 1

function PrintOptions {
  cat <<END

$0 [options]:
cmd                              # value, "$cmd"
nj                               # value, $nj
ubm_train                	 # value, $ubm_train
ubm_num_gauss            	 # value, $ubm_num_gauss
sgmm2mle_train     	         # value, $sgmm2mle_train
sgmm2mle_test           	 # value, $sgmm2mle_test
sgmm2mle_test_data     		 # value, "$sgmm2mle_test_data"
sgmm2mle_test_dataname  	 # value, "$sgmm2mle_test_dataname"
sgmm2mle_test_transform 	 # value, "$sgmm2mle_test_transform"
sgmm_num_leave          	 # value, $sgmm2_num_leave
sgmm_num_gauss           	 # value, $sgmm2_num_gauss
index                    	 # value, $index
lmname                   	 # value, "$lmname"

END
}

PrintOptions;
if [ $# -ne 4 ]; then
  echo 
  echo "$0: [options] <data> <lang> <ali> <tgt_dir>"
  echo  && exit 1
fi

data=$1
lang=$2
ali=$3
tgt_dir=$4

ubmdir=$tgt_dir/ubm$index
if $ubm_train; then
  echo "$0: ubm_train started @ `date`"
  steps/train_ubm.sh  --cmd "$cmd" $ubm_num_gauss  $data  $lang $ali $ubmdir || exit 1
fi

if $sgmm2mle_train; then
  echo "$0: sgmm2mle_train started @ `date`" 
  steps/train_sgmm2.sh  --cmd "$cmd" --nj $nj $sgmm2_num_leave $sgmm2_num_gauss \
    $data $lang  $ali $ubmdir/final.ubm $tgt_dir/sgmm2$index || exit 1
  echo "$0: sgmm2 training in $tgt_dir/sgmm2$index done @ `date`"
fi

if $sgmm2mle_test; then
  echo "$0: sgmm2mle_test started @ `date`"
  [ ! -z $sgmm2mle_test_data ] || \
  { echo "$0: sgmm2mle_test: ERROR, test data is not specified"; exit 1; }
  sdir=$tgt_dir/sgmm2$index
  graph=$sdir/graph${lmname:+.$lmname}
  data=$sgmm2mle_test_data
  decode_dir=$sdir/decode_dev
  [ ! -z $sgmm2mle_test_dataname ] &&\
  decode_dir=$sdir/decode_$sgmm2mle_test_dataname
  mkgraph.sh $lang $sdir $graph
  steps/decode_sgmm2.sh --cmd "$cmd" --nj $nj ${sgmm2mle_test_transform:+--transform-dir $sgmm2mle_test_transform}\
  $graph $data $decode_dir || exit 1
  echo "$0: sgmm2mle_test: decode_sgmm2 done @ `date`"
fi
