#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl;
nj=10;
step=0
substep=0
prepare_data=false
prepare_fea=false
train_gmm=false
lang=data/llp/lang; lang_test=${lang}_test
samples_per_iter=4000000;  
max_mem=500000000;
state=2500;gauss=20000
pnorm_input_dim=3000; pnorm_output_dim=500; 
nonspeech_words=$lang/nonspeech_words.txt

# end tunable variable
bn_am_cf=conf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";

datastr="train dev10h" ;

id=llp_sil;
noise_words=data/local/dict/noise_words.txt
train=/media/lychee_w1/hhx502/kws14/nist/IARPA-babel204b-v1.1b/BABEL_OP1_204/conversational/sub-train/transcript_roman 
if $prepare_data; then
  dir=data/local/$id/train
  if [ $step  -le 1 ]; then
    echo -e "\n## prepare text `date`\n"
    mkdir -p $dir 2>/dev/null
    iutils/prepare_seg_text.pl  "ls $train/*.txt"  >  $dir/raw.txt  
    echo -e "\n## done check $dir/raw.txt `date`\n"
  fi
  if [ $step -le 5 ]; then
    paste <(cut -d " " -f 1 $dir/raw.txt )  <(cut -d " " -f 2- $dir/raw.txt | uconv -f utf8 -t utf8 -x "Any-Lower" ) > $dir/raw_lower.txt
    cat $dir/raw_lower.txt | \
    awk '{for(i=4; i<=NF; i++){ print $i; }}' | sort -u > $dir/raw_lower.wlist
    cat  $dir/raw_lower.txt | \
    sed 's#<no-speech>#[SIL]#g'| \
    sed -e 's/[\*\~]//g' |  \
    sed 's#(())#<unk>#g' | \
    sed 's#<breath>#[BREATH]#g' | \
    sed 's#<click>#[NOISE]#g' | \
    sed 's#<cough>#[COUGH]#g' | \
    sed 's#<dtmf>#[NOISE]#g' | \
    sed 's#<female-to-male>##g' | \
    sed 's#<male-to-female>##g' | \
    sed 's#<foreign>##g' | \
    sed 's#<int>#[NOISE]#g' | \
    sed 's#<laugh>#[LAUGH]#g' | \
    sed 's#<lipsmack>#[NOISE]#g' | \
    sed 's#<overlap>##g' | \
    sed 's#<prompt>#[NOISE]#g' |\
    sed 's#<ring>#[NOISE]#g' | \
    sed 's#<sta>#[NOISE]#g' | \
    awk '{lab=$1; $1=""; gsub(/_/," ", $0);print lab, $0; }' | \
    awk '{lab=$1; $1 = ""; for(i=1;i<=NF;i++){ gsub(/^-/,"",$i); gsub(/-$/,"",$i); }  print lab, $0; }' > $dir/raw_lower_step01.txt
    cat $dir/raw_lower_step01.txt | \
    awk '{for(i=4; i<=NF; i++){ print $i; }}' | sort -u > $dir/raw_lower_step01.wlist
    echo -e "\n## check $dir/raw_lower_step01.txt && $dir/raw_lower_step01.wlist  `date`\n"  ; 
  fi
nist_lex=nist/IARPA-babel204b-v1.1b/BABEL_OP1_204/conversational/patched/reference_materials/lexicon.sub-train.txt
   if [ $step -le 10 ]; then
     dir=data/local/$id/dict;
     mkdir -p $dir 2>/dev/null
     cat $nist_lex | \
     perl -ne 'chomp; 
       m/(\S+)\s+(\S+)\s+(.*)/ or die "illegal entry $_";
       print STDERR $1, "\n";
       print "$2\t$3\n"; 
     '   2>$dir/tamil.wlist  > $dir/raw_lex.txt
     paste <(cut -f 1 $dir/raw_lex.txt | uconv -f utf8 -t utf8 -x "Any-Lower" )  <(cut -f 2- $dir/raw_lex.txt) > $dir/raw_lex_lower.txt
     echo -e "\n## check oov case first time\n"
     cat data/local/$id/train/raw_lower_step01.wlist | \
     perl -e '  $lex = shift @ARGV;
       open LEX, "<$lex" or die "lexicon $lex cannot open\n";
       while (<LEX>) {
         chomp;
         m/(\S+)\s+(.*)/;  $vocab{$1} = 0;
       }
       close LEX;
       while (<STDIN>) {
        chomp; 
        # print STDERR "$_\n";
        if (not exists $vocab{$_}) {
          print "$_\n";
        } 
       }  
     ' $dir/raw_lex_lower.txt |sort -u  > $dir/raw_oov_step01.txt
     echo  -e "\n## check $dir for dict preparation `date`\n" ;
   fi 
   if [ $step -le 15 ]; then
     echo -e "\n## filter training text `date`\n"
     dir=data/local/$id/train;
     cat $dir/raw_lower_step01.txt |\
     perl -e '  $noise = shift @ARGV;
       open N, "<$noise" or die "noise file $noise cannot open\n";
       while (<N>) {
        chomp;
        m/(\S+)/;
        $vocab{$1} = 0;
       }
       close N;  
       while (<STDIN>) {
         chomp;
         @A = split /\s+/;
         if (scalar @A == 3) {
           print STDERR $_,"\n";  next;
         }
         $deserted=1;
         for($i = 3; $i < scalar @A; $i ++) {
           $w = $A[$i];
           if(not exists $vocab{$w}) { 
             $deserted = 0; last;
           }
         }
         if ($deserted == 1) {
           print STDERR $_, "\n"; next;
         }
         print $_ , "\n";
       } '  $noise_words  > $dir/raw_lower_step02.txt 2>$dir/raw_deserted_step02.txt
     echo -e "\n##step15: check $dir/raw_lower_step02.txt and $dir/raw_deserted_step02.txt\n" 
   fi 
   if [ $step -le 20 ]; then
     echo -e "\n## prepare kaldi data\n"
     dir=data/local/$id/train/kaldi;
     mkdir -p $dir 2>/dev/null
     cat $dir/../raw_lower_step02.txt | \
     perl -e '
       ($seg, $txt) = @ARGV;
       open SEG, ">$seg" or die "segment file $seg cannot open\n";
       open TXT, ">$txt" or die "text file $txt cannot open\n";
       $unit=100000;
       while (<STDIN>) {
         chomp;
         m/(\S+)\s+(\S+)\s+(\S+)\s+(.*)/ or next;
         my ($segName, $start, $end, $trans) = ($1, $2, $3, $4);
         $lab = sprintf "%s_%010d_%010d", $segName, $start*$unit, $end*$unit;
         print TXT "$lab\t$trans\n";
         print SEG "$lab\t$segName\t$start\t$end\n"; 
       }
       close SEG;
       close TXT;
     ' $dir/segments $dir/text 
      cat $dir/segments | \
      perl -ne '
        m/(\S+)\s+(\S+)\s+(.*)/;
        print "$1\t$2\n";
      '  > $dir/utt2spk
      utils/utt2spk_to_spk2utt.pl <$dir/utt2spk >$dir/spk2utt
      echo -e "\ncheck file $dir/utt2spk and $dir/spk2utt\n"; 
   fi
   pd=/media/lychee_w1/hhx502/kws14/nist/IARPA-babel204b-v1.1b;
   wavdir=$pd/BABEL_OP1_204/conversational/training/audio;
   sox=/usr/bin/soxi;
   sph2pipe=/opt/kaldi3550/trunk/tools/sph2pipe_v2.5/sph2pipe;
   if [ $step -le 25 ]; then
     echo -e "\n## prepare wave data `date`\n"
     dir=data/local/$id/train/kaldi; segments=$dir/segments
     ls $wavdir/* | egrep "\.wav$|\.sph$" | \
     perl -e '
      ($sph2pipe, $seg)  = @ARGV;
      open SEG, "<$seg" or die "segment file $seg cannot open\n";
      while (<SEG>) {
        chomp;
        m/(\S+)\s+(\S+)\s+(.*)/;
        $vocab{$2} = 0;
      }
      close SEG;
      while (<STDIN>) {
        chomp;
        $lab = $_; $lab =~ s/.*\///g;  $lab =~ s/\.wav$//g; 
        $lab =~ s/\.sph$//g;
        next if (not exists $vocab{$lab});
        # print $lab, "\n";
        if (/\.sph/) {
          print $lab, " $sph2pipe  -f wav -p -c 1 $_|\n";
        } elsif (/\.wav/) {
         print $lab, " $_\n";
        } else {
          die "unidentified files $_\n";
        }
      } '   $sph2pipe $segments |sort -u > $dir/wav.scp
     echo -e "\n## check file $dir/wav.scp\n" 
   fi
   if [ $step -le 30 ]; then
     sraw_lex=data/local/$id/dict/raw_lex_lower.txt
     dir=`dirname $sraw_lex`
     echo -e "\n## prepare word dict `date`\n"
     cat $sraw_lex | \
     perl -ne '
       chomp;
       @A = split /\t/;
       $nA = scalar @A;
       if($nA == 2) {
         print "$A[0]\t$A[1]\n";
       } elsif ($nA > 2) {
         $w = shift @A;
         for($i = 0; $i < $nA -1; $i++) {
           $pron = $A[$i];
           print "$w\t$pron\n";
         }
       }
     '   | \
     sed -e 's#\.# #g' | \
     sed -e 's:\#: :g' | \
     awk '{gsub(/ +/," ",$0);print; }' | \
     sort -u > $dir/lexicon01.txt
     paste <(cut -f 1 $dir/lexicon01.txt) <(cut -f 2- $dir/lexicon01.txt| sed -e 's/4/F/g') \
     >$dir/lexicon02.txt 
     echo -e "<unk>\tSPN\n[BREATH]\tBRE\n[COUGH]\tCOU\n[LAUGH]\tLAU\n[NOISE]\tNSN" | \
     cat - $dir/lexicon02.txt > $dir/lexicon03.txt
     echo -e "\n## check $dir\n"
     cat $dir/lexicon03.txt | \
     cut -f 2- | awk '{for(i=1;i<=NF;i++){print $i; }}' | sort -u > $dir/phones.txt
     echo -e "\n## check phone set $dir/phones.txt\n"
   fi
   if [ $step -le 35 ]; then
     echo -e "\n## prepare lex `date`\n"
     dir=data/local/$id/dict;
     cat data/local/llp/dict/silence_phones.txt | \
     grep -v 'MUS' > $dir/silence_phones.txt    
     echo SIL > $dir/optional_silence.txt
     cat $dir/phones.txt | grep -v -f $dir/silence_phones.txt \
     > $dir/nonsilence_phones.txt
     cat $dir/silence_phones.txt| awk '{printf("%s ", $1);} END{printf "\n";}' > $dir/extra_questions.txt || exit 1;
cat $dir/nonsilence_phones.txt | perl -e 'while(<>){ foreach $p (split(" ", $_)) {
  $p =~ m:^([^\d]+)(\d*)$: || die "Bad phone $_"; $q{$2} .= "$p "; } } foreach $l (values %q) {print "$l\n";}' \
 >> $dir/extra_questions.txt || exit 1;
    (echo  '[SIL] SIL') | \
    cat - $dir/lexicon03.txt > $dir/lexicon.txt 
     echo -e "\n## lex done, see $dir/lexicon.txt `date`\n"
   fi
   if [ $step -le 40 ]; then
     utils/prepare_lang.sh data/local/$id/dict "<unk>" data/local/$id/lang_tmp data/$id/lang || exit 1;
   fi
   if [ $step -le 41 ]; then
     echo -e "\n## step 41 prepare dev data started `date`\n"
     iprepare/dev_prepare.sh --prepare-data true --step 1 || exit 1
     echo -e "\n## step41 prepare dev data ended `date`\n"
   fi
   if [ $step -le 45 ]; then
     echo -e "\n## format data `date`\n"
     data=data/$id;
     for x in data/local/$id/train data/local/dev10h; do
       name=`basename $x`; mkdir -p $data/$name 2>/dev/null
       cp $x/kaldi/* $data/$name;
     done
     echo -e "\n## ended with data formating `date`\n"
   fi
  sdir=data/local/$id/lm; mkdir -p $sdir 2>/dev/null
  if [ $step -le 50 ]; then
    echo -e "\n## prepare language model `date`\n"
    cat data/local/$id/dict/lexicon.txt | \
    awk '{print $1;}'|sort -u > $sdir/wlist
    echo -e "\n## prepare lm text\n"
    cat data/$id/train/text | \
    iprepare/make_lm_text.pl > $sdir/lm.txt 
    lm=$sdir/lm_tr.arpa; wlist=$sdir/wlist; text=$sdir/lm.txt
    ngram-count -vocab $wlist  \
    -order 3  -text $text  -lm $lm \
    -interpolate1 -interpolate2 -interpolate3 \
    -kndiscount1  -kndiscount2  -kndiscount3
    
    gzip -c $lm > $sdir/lm_tr.arpa.gz

    echo -e "\n## done `date`\n"
  fi
  if [ $step -le 55 ]; then
    echo -e "\n## evaluate lm\n"
    lm=$sdir/lm_tr.arpa
    cat data/$id/dev10h/text | \
    awk '{$1=""; print "<s>",$0," </s>";}' > $sdir/lm_dev.txt
    ngram -lm  $lm -ppl $sdir/lm_dev.txt  -debug 3  > $sdir/lm_dev_perplexity.log;
    echo -e "\n## done\n"
  fi
  if [ $step -le 60 ]; then
    for x in train dev10h; do
      utils/fix_data_dir.sh data/$id/$x
    done  
  fi

  dir=data/$id/lang_test;
  tmpdir=data/local/$id/lm_tmp; mkdir -p $tmpdir 2>/dev/null
  lmdir=data/local/$id/lm;
  if [ $step -le 65 ]; then
    echo -e "\n## prepare grammar\n"
    mkdir -p $dir 2>/dev/null
    for f in phones.txt words.txt phones.txt L.fst L_disambig.fst \
      phones/; do
       cp -r $lang/$f $dir
     done
      gunzip -c $lmdir/lm_tr.arpa.gz | \
      utils/find_arpa_oovs.pl $dir/words.txt  > $tmpdir/oovs_tr.txt
    echo -e "\n## done\n"
  fi
  if [ $step -le 70 ]; then
      gunzip -c $lmdir/lm_tr.arpa.gz | \
      grep -v '<s> <s>' | \
      grep -v '</s> <s>' | \
      grep -v '</s> </s>' | \
      arpa2fst - | fstprint | \
      utils/remove_oovs.pl $tmpdir/oovs_tr.txt | \
      utils/eps2disambig.pl | \
      utils/s2eps.pl | fstcompile --isymbols=$dir/words.txt \
      --osymbols=$dir/words.txt  \
      --keep_isymbols=false --keep_osymbols=false | \
      fstrmepsilon > $dir/G.fst
      fstisstochastic $dir/G.fst
      echo -e "\n## step70: lm to G fst done `date`\n"
  fi
##   
fi
if $prepare_fea; then
  echo -e "\n## prepare_fea started `date`\n"
  fid=plp ;nj=20
  if [ $step -le 1 ]; then
    for x in train dev10h; do
      sdata=data/$id/$x;data=data/feature/$id/$fid/$x; fp=exp/feature/$id/$fid;
      fea=$fp/$x/_data; logdir=$fp/$x/_log
      mkdir -p $data 2>/dev/null; cp $sdata/* $data
      echo -e "\n## cure wav.scp under folder $data\n"
      cat $sdata/wav.scp | \
      awk '{if(/\.sph$/){print $0,"|";}else print;}' > $data/wav.scp
      steps/make_plp_pitch.sh --nj $nj $data $logdir $fea || exit 1
      steps/compute_cmvn_stats.sh $data $logdir $fea ||exit 1;
      utils/fix_data_dir.sh $data
    done
  fi
  echo -e "\n## prepare_fea done `date`\n"
fi

if $train_gmm; then
  dp=data/feature/$id/plp; train=$dp/train; dev=$dp/dev10h;
  dir=exp/$id/plp_pitch
  
  if [ $step -le 1 ]; then
    isteps/train_am_simple.sh --max-stage 6 --over-train $train \
     --over-dev $dev --over-lang $lang --over-lang-test $lang_test \
     --over-expdir $dir $bn_am_cf || exit 1 
  fi
fi
