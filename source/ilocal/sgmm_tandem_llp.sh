#!/bin/bash 

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "cuda_path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl;
cmd="queue.pl  -q all.q@erl-avocado -q all.q@erl-banana  -q all.q@erl-ciku -q all.q@erl-durian"
nj=10;
step=0
substep=0
data_prepare=false

lang=data/llp/lang; lang_test=${lang}_test
samples_per_iter=4000000;  
max_mem=500000000;
state=2500;gauss=20000
pnorm_input_dim=3000; pnorm_output_dim=500; 
nonspeech_words=$lang/nonspeech_words.txt
ubm_num=600;

# end tunable variable
bn_am_cf=conf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";

data=data/llp/feature/tandem/train; ali=exp/llp/tandem/tri3sat_2500/ali_train;
ubm_dir=exp/llp/tandem/ubm_${ubm_num}; 
if [ $step -le 1 ]; then
 echo -e "\n## $0 ubm started `hostname` `date` \n"
 isteps/train_ubm.sh  --has-delta false --cmd "$cmd" \
 $ubm_num $data $lang $ali $ubm_dir || exit 1;    
 echo -e "\n## done `date`\n"
fi
sdir=exp/llp/tandem/sgmm20a
if [ $step -le 2 ]; then
  echo -e "\n## $0 sgmm2 started `hostname` `date`\n"
  isteps/train_sgmm2.sh --has-delta false --cmd "$cmd" \
  2500 10000 $data $lang $ali $ubm_dir/final.ubm $sdir || exit 1;
  echo -e "\n## $0 done `date`\n"
fi
graph=$sdir/graph;
if [ $step -le 3 ]; then
  echo -e "\n## $0 making graph `hostname` `date`\n"
  utils/mkgraph.sh $lang_test $sdir $graph  || exit 1;
  echo -e "\n## done `date` \n"
fi
transform_dir=$ali/../decode_dev10h; dev=data/llp/feature/tandem/dev10h;
if [ $step -le 3 ]; then
  echo -e "\n## $0 decode started `hostname` `date`\n"
  isteps/decode_sgmm2.sh --nj 30 --cmd "$cmd" --use-delta false --transform-dir $transform_dir \
  $graph $dev $sdir/decode_dev10h || exit 1
  echo -e "\n## $0 done `date`\n"
fi
if [ $step -le 4 ]; then
  echo -e "\n## $0 one_pass decoding `hostname` `date`\n"
  isteps/decode_sgmm2_onepass.sh --nj 30 --cmd "$cmd" --use-delta false --one-pass true  \
  --transform-dir $transform_dir \
  $graph $dev $sdir/decode_dev10h_onepass || exit 1
  echo -e "\n## $0 done `date`\n"
fi
