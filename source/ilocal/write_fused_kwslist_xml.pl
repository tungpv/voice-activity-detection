#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;
# refer to utils/write_kwslist.pl
#
# Function for sorting
sub KwslistOutputSort {
  if ($a->[0] ne $b->[0]) {
    if ($a->[0] =~ m/[0-9]+$/ && $b->[0] =~ m/[0-9]+$/) {
      ($a->[0] =~ /([0-9]*)$/)[0] <=> ($b->[0] =~ /([0-9]*)$/)[0]
    } else {
      $a->[0] cmp $b->[0];
    }
  } elsif ($a->[5] ne $b->[5]) {
    $b->[5] <=> $a->[5];
  } else {
    $a->[1] cmp $b->[1];
  }
}

# Function for printing Kwslist.xml
sub PrintKwslist {
  my ($info, $KWS) = @_;

  my $kwslist = "";

  # Start printing
  $kwslist .= "<kwslist kwlist_filename=\"$info->[0]\" language=\"$info->[1]\" system_id=\"$info->[2]\">\n";
  my $prev_kw = "";
  foreach my $kwentry (@{$KWS}) {
    if ($prev_kw ne $kwentry->[0]) {
      if ($prev_kw ne "") {$kwslist .= "  </detected_kwlist>\n";}
      $kwslist .= "  <detected_kwlist search_time=\"1\" kwid=\"$kwentry->[0]\" oov_count=\"0\">\n";
      $prev_kw = $kwentry->[0];
    }
    $kwslist .= "    <kw file=\"$kwentry->[1]\" channel=\"$kwentry->[2]\" tbeg=\"$kwentry->[3]\" dur=\"$kwentry->[4]\" score=\"$kwentry->[5]\" decision=\"$kwentry->[6]\"";
    if (defined($kwentry->[7])) {$kwslist .= " threshold=\"$kwentry->[7]\"";}
    if (defined($kwentry->[8])) {$kwslist .= " raw_score=\"$kwentry->[8]\"";}
    $kwslist .= "/>\n";
  }
  $kwslist .= "  </detected_kwlist>\n";
  $kwslist .= "</kwslist>\n";

  return $kwslist;
}
my $p_tar = 0.0741;
my $posterior_ratio ="false";
GetOptions('p_tar=f' => \$p_tar,
           'posterior-ratio=s' => \$posterior_ratio);
my $numArgs = scalar @ARGV;
if($numArgs != 4) {
  die "\n\nUsage: $0 <threshold> <weight> <ecf> <score_list.txt>\n\n";
}

my @info = ("", "english", "");

my ($cutoff, $weight_fname, $ecf_fname, $score_list) = @ARGV;

open(F, "$weight_fname") or die "file $weight_fname cannot open\n";
my $wstr = <F>;
close F;

my @alpha = split(/\s+/,$wstr);
my $beta = pop @alpha;

my %ecf_vocab = ();
open (F, "$ecf_fname") or die "file $ecf_fname cannot open\n";
while (<F>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $ecf_vocab{$1} = $2;
}
close F;

open(F, "$score_list") or die "file $score_list canot open\n";
my  @aKWS = ();
my $bias = log($p_tar/(1-$p_tar));
while (<F>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(.*)/ or next;
  my ($kwid, $utt, $scorestr) = ($1, $2, $3);
  die "unknown $utt\n" if not exists $ecf_vocab{$utt};
  my $time_info = $ecf_vocab{$utt};
  my @aTime = split(/\s+/, $time_info);
  my @aScore = split (/\s+/,$scorestr);
  die "dimension mismatched $_\n" if scalar @aScore != scalar @alpha;
  my $x = $beta;
  if ($posterior_ratio eq "true") {
    # print STDERR "logit normalization is on ...\n";
    for(my $i = 0; $i< @aScore; $i++) {
      my $s = $aScore[$i];
      if($s <= 0) {
        $s = 1.0e-6;
      }
      $s = log($s/(1 - $s));
      $s += $bias;
      $x += $s * $alpha[$i];
    }
  } else {
    for(my $i = 0; $i< @aScore; $i++) {
      $x += $aScore[$i] * $alpha[$i];
    }
  }
  my $decision = "NO";
  if ($x >= $cutoff) {
    $decision = "YES";
  }
  my $score = sprintf("%.6f", $x);
  push(@aKWS, [$kwid,$utt, 1, $aTime[0], $aTime[1],$score, $decision ]);
}
close F;
# Output sorting
my @atmp = sort KwslistOutputSort @aKWS;
# Printing
my $kwslist = PrintKwslist(\@info, \@atmp);
print $kwslist;
