#!/bin/bash
# Copyright Johns Hopkins University (Author: Daniel Povey) 2012.  Apache 2.0.

# begin configuration section.
cmd=run.pl
stage=0
min_lmwt=9
max_lmwt=20
latbase=lat
word_ins_penalty=0;
ignore_stm=false;
mbr=
sclite=/opt/kaldi_updated/trunk/tools/sctk-2.4.5/bin/sclite
symtab=
mdldir=
model=
#end configuration section.

[ -f ./path.sh ] && . ./path.sh
. parse_options.sh || exit 1;

if [ $# -ne 3 ]; then
  echo "Usage: local/score_sclite.sh [--cmd (run.pl|queue.pl...)] <data-dir> <lang-dir|graph-dir> <decode-dir>"
  echo " Options:"
  echo "    --cmd (run.pl|queue.pl...)      # specify how to run the sub-processes."
  echo "    --stage (0|1|2)                 # start scoring script from part-way through."
  echo "    --latbase <base>                # base file name of the lattices, if not 'lat'"
  echo "    --mbr \"--decode-mbr=true\"     # do MBR decoding"
  echo "    --min_lmwt <int>                # minumum LM-weight for lattice rescoring "
  echo "    --max_lmwt <int>                # maximum LM-weight for lattice rescoring "
  exit 1;
fi

data=$1
lang=$2 # Note: may be graph directory not lang directory, but has the necessary stuff copied.
dir=$3
if [ -z $model ]; then
  model=$dir/../final.mdl # assume model one level up from decoding dir.
  if [ ! -z $mdldir ]; then
    model=$mdldir/final.mdl;
  fi
fi

for f in $lang/words.txt \
     $model $dir/${latbase}.1.gz; do
  [ ! -f $f ] && echo "$0: expecting file $f to exist" && exit 1;
done
symtab=$lang/words.txt;
name=`basename $data`; # e.g. eval2000


if [ "$latbase" != "lat" ]; then
  infix="${latbase}_"
else
  infix=""
fi
function die {
  echo -e "\nERROR: $1\n"; exit 1;
}
mkdir -p $dir/scoring/log
if  $ignore_stm; then
  [ -e $data/text ] ||  die "$data/text file expected";

  cat $data/text | sed 's:<NOISE>::g' | \
  sed 's:<SPOKEN_NOISE>::g'| \
  iutils/clean_text.pl > $dir/scoring/test_filt.txt
  $cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring/log/best_path.LMWT.log \
  lattice-scale --inv-acoustic-scale=LMWT "ark:gunzip -c $dir/lat.*.gz|" ark:- \| \
  lattice-add-penalty --word-ins-penalty=$word_ins_penalty ark:- ark:- \| \
  lattice-best-path --word-symbol-table=$symtab \
    ark:- ark,t:$dir/scoring/LMWT_map.tra || exit 1;

  # Note: the double level of quoting for the sed command
  $cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring/log/score.LMWT.log \
  cat $dir/scoring/LMWT_map.tra \| \
  utils/int2sym.pl -f 2- $symtab \| sed 's:\<UNK\>::g' \| \
  iutils/clean_text.pl \| \
  compute-wer --text --mode=present \
  ark:$dir/scoring/test_filt.txt  ark,p:- ">&" $dir/wer_map_LMWT || exit 1;
  
  exit 0;

fi

if [ ! -e $data/stm ]; then
  cat $data/text | sed 's:<NOISE>::g' | \
  sed 's:<SPOKEN_NOISE>::g'| \
  iutils/clean_text.pl > $dir/scoring/test_filt.txt

  $cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring/log/best_path.LMWT.log \
  lattice-scale --inv-acoustic-scale=LMWT "ark:gunzip -c $dir/lat.*.gz|" ark:- \| \
  lattice-add-penalty --word-ins-penalty=$word_ins_penalty ark:- ark:- \| \
  lattice-best-path --word-symbol-table=$symtab \
    ark:- ark,t:$dir/scoring/LMWT.tra || exit 1;

  # Note: the double level of quoting for the sed command
  $cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring/log/score.LMWT.log \
  cat $dir/scoring/LMWT.tra \| \
  utils/int2sym.pl -f 2- $symtab \| sed 's:\<UNK\>::g' \| \
  iutils/clean_text.pl \| \
  compute-wer --text --mode=present \
  ark:$dir/scoring/test_filt.txt  ark,p:- ">&" $dir/wer_LMWT || exit 1;
  
  exit 0;
fi
#    lattice-1best --lm-scale=LMWT "ark:gunzip -c $dir/${latbase}.*.gz|" ark:- \| \
#    lattice-align-words $lang/phones/word_boundary.int $model ark:- ark:- \| \
#    nbest-to-ctm ark:- - \| \
if [ $stage -le 0 ]; then
  $cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring/log/get_${infix}ctm.LMWT.log \
    mkdir -p $dir/score_${infix}LMWT/ '&&' \
    lattice-align-words $lang/phones/word_boundary.int $model "ark:gunzip -c $dir/${latbase}.*.gz|" ark:- \| \
    lattice-to-ctm-conf --lm-scale=LMWT ark:- - \| \
    utils/int2sym.pl -f 5 $lang/words.txt  \| \
    utils/convert_ctm.pl $data/segments $data/reco2file_and_channel \| \
    slocal/clean_ctm.pl \| \
    sort +0 -1 +1 -2 +2nb -3 \| \
    slocal/fill_blanks_ctm.pl $data/stm '>' $dir/score_${infix}LMWT/$name.ctm || exit 1;
fi

if [ $stage -le 2 ]; then  
  if [ -s $data/stm ]; then
    $cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring/log/${infix}score.LMWT.log \
      $sclite -D -F -f 0 -e utf-8 -r $data/stm stm -h $dir/score_${infix}LMWT/${name}.ctm ctm ">" $dir/wer_${infix}LMWT || exit 1;
  else
    echo "WARNING skipping sclite scoring due to empty stm file!"
  fi
fi

