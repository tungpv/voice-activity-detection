#!/usr/bin/env perl
use strict;
use warnings;
use XML::Simple;
use Data::Dumper;

my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  die "\n\nUsage: $0 <tlist.xml> <kwslist.xml>\n\n";
}
my ($tlist_xml, $kwslist_xml) = @ARGV;

my $tlist_data = XMLin($tlist_xml);
## print Dumper($tlist_data);
my %vocab = ();
my $pA = $tlist_data->{term};
for (my $i = 0; $i < scalar @$pA; $i++) {
  my $pE = $$pA[$i];
  $vocab{$pE->{termid}} = 0;
}
my $kwslist_data = XMLin($kwslist_xml);
my $duplicate_num = 0; 
foreach my $kwentry (@{$kwslist_data->{detected_termlist}}) {
  my $kwid = $kwentry->{termid};
  if (not exists $vocab{$kwid}) {
  $kwslist_data->{detected_termlist} = [ grep {$_->{termid} ne $kwid } @{$kwslist_data->{detected_termlist} } ];
  } elsif(ref($kwentry->{term}) eq 'ARRAY') {
    my @array = @{$kwentry->{term}};
    my @newarray = ();
    push @newarray, $array[0];
    my %file_vocab = ();
    $file_vocab{$array[0]->{file}} ++;
    for(my $i = 1; $i < scalar @array; $i ++) {
      my $fname = $array[$i]->{file};
      if (not exists $file_vocab{$fname}) {
        $file_vocab{$fname} ++;
        push @newarray, $array[$i];
      } else {
        $duplicate_num ++;
      }
    }
    $kwentry->{term} = \@newarray;
  }
}
print STDERR "$duplicate_num duplications are removed\n";
my $xml = XMLout($kwslist_data, RootName=>"stdlist", NoSort=>1);
print $xml;
