#!/bin/bash

# Copyright 2012-2013  Brno University of Technology (Author: Karel Vesely)
# Apache 2.0

# In this recipe we build DNN in four stages:
# 1) Data preparations : the fMLLR features are stored to disk
# 2) RBM pre-training : in this unsupervised stage we train stack of RBMs, a good starting point for Cross-entropy trainig
# 3) Frame-level cross-entropy training : in this stage the objective is to classify frames correctly.
# 4) Sequence-discriminative training : in this stage the objective is to classify the whole sequence correctly,
#     the idea is similar to the 'Discriminative training' in context of GMM-HMMs.


. ./cmd.sh ## You'll want to change cmd.sh to something that will work on your system.
           ## This relates to the queue.

. ./path.sh ## Source the tools/utils (import the queue.pl)


###
### We save the fMLLR features, so we can train on them easily
###

function doit {
  return 0;
}
function passit {
  return 1;
}
function die {
  echo -e "\nERROR: $1 ...\n"; exit 1;
}
function StopHere {
  echo -e "\nSuccessfully done till now !\n"; exit 0;
}


#false && \
{


gmmdir=exp/tri4a
datadir=data/local/fmllr-tri4a
# * eval2000 * #
if passit; then

  dir=$datadir/dev
  logfile=log/LOG.dev.fmllr.feat
  [ -e $logfile ] || rm -f $logfile
  sbatch -p speech -n20 -o $logfile \
  steps/make_fmllr_feats.sh --nj 20 --cmd "$train_cmd" \
  --transform-dir exp/tri4a/decode_dev \
  $dir data/dev  $gmmdir $dir/_log $dir/_data || \
  die "making fmllr feats failed on dev data ..."

fi


# * train_dev * #
dir=$datadir/train_dev
# We need alignments. We will use train_dev as held-out set 
# for frame-level cross-entropy training.
if passit; then
  steps/align_fmllr.sh --nj 20 --cmd "$train_cmd" \
  data/train_dev data/lang $gmmdir  exp/tri4a_ali_train_dev \
  || die "force alignment failed for train_dev data ..."
fi

# We need fMLLR transforms. If we compute fMLLRs by decoding, 
# it is not cheating and the train_dev number can be compared to other systems.
if passit; then
  logfile=log/LOG.train_dev.decode
  sbatch -p speech -n20 -o $logfile \
  steps/decode_fmllr.sh --nj 20 --cmd "$decode_cmd" \
  --config conf/decode.config  $gmmdir/graph \
  data/train_dev $gmmdir/decode_train_dev \
  || die "decoding failed for train_dev data"
fi


if passit; then
  # Save the fMLLR features
  dir=$datadir/train_dev
  logfile=log/LOG.train_dev.sav
  sbatch -p speech -n20 -o $logfile \
  steps/make_fmllr_feats.sh --nj 20 --cmd "$train_cmd" \
  --transform-dir $gmmdir/decode_train_dev \
  $dir data/train_dev $gmmdir $dir/_log $dir/_data \
  || die "making fmllr features failed for train_dev data"
fi

if passit; then
  logfile=log/LOG.train_dnn.fmllr.align
  sbatch -p speech -n20 -o $logfile \
  steps/align_fmllr.sh --nj 20 --cmd "$train_cmd" \
  data/train_dnn  data/lang $gmmdir  exp/tri4a_ali_train_dnn \
  || die "align_fmllr failed on train_dnn data ..."
fi
if passit; then
  # * train_nodup * #
  dir=$datadir/train_dnn
  # Save the fMLLR features
  logfile=log/LOG.train_dnn.fmllr.save
  sbatch -p speech -n20 -o $logfile \
  steps/make_fmllr_feats.sh --nj 20 --cmd "$train_cmd" \
   --transform-dir exp/tri4a_ali_train_dnn \
   $dir data/train_dnn  $gmmdir $dir/_log $dir/_data || exit 1
fi
}


###
### Let's pre-train the stack of RBMs
###

#false && \
if passit; then
{ # Pre-train the DBN
. cuda_path.sh || die "cuda_path.sh expected ..."
. cuda_cmd.sh || die "cuda_cmd.sh expected ..."
echo -e "\nI am here !\n"
dir=exp/tri4a_pretrain-dbn
logfile=log/LOG.pretrain.dnn
[ -e $logfile ] || rm -f $logfile
# (tail --pid=$$ -F $dir/_pretrain_dbn.log)&
sbatch -p speech -o $logfile --mem-per-cpu=10000MB -w "squid[7-9]" \
$cuda_cmd $dir/_pretrain_dbn.log \
 steps/pretrain_dbn.sh $datadir/train_dnn $dir  
 # || die "pretraining failed ..."
}

fi



###
### Train the DNN, while optimizing frame-level cross-entropy.
### This will take some time.
###

#false && \
{ # Train the MLP

dir=exp/tri4a_pretrain-dbn_dnn
sdir=exp/tri4a_pretrain-dbn
ali=exp/tri4a_ali
feature_transform=$sdir/final.feature_transform
dbn=$sdir/4.dbn
# (tail --pid=$$ -F $dir/_train_nnet.log)& 

logdir=data/local/log/train_dnn
[ -d $logdir ] || mkdir -p $logdir
logfile=$logdir/train.dnn.log
[ ! -e $logfile ] || rm -f $logfile

if passit; then
  . cuda_path.sh || die "cuda_path.sh expected ..."
  . cuda_cmd.sh || die "cuda_cmd.sh expected ..."

 # . env.sh
 # run-command -attr 1gpus \

  sbatch -p speech -o $logfile  -w "squid7" \
  $cuda_cmd $dir/_train_dnn.log \
  steps/train_nnet.sh --feature-transform $feature_transform --dbn $dbn --hid-layers 0 --learn-rate 0.008 \
  $datadir/train_dnn $datadir/train_dev data/lang ${ali}_train_dnn ${ali}_train_dev $dir || exit 1;
fi


# decode (reuse HCLG graph)
if passit; then
  steps/decode_nnet.sh --nj 20 --cmd "$decode_cmd" --config conf/decode_dnn.config --acwt 0.0833 \
  exp/tri4b/graph_sw1_fsh_tgpr data-fmllr-tri4b/train_dev $dir/decode_train_dev_sw1_fsh_tgpr || exit 1;
fi
# decode
if doit; then
  . env.sh
  . path.sh
  . cmd.sh
  logdir=data/local/log/nohup
  [ -d $logdir ] || mkdir -p $logdir
  logfile=$logdir/decode.dnn.log
  [ ! -e $logfile ] || rm -f $logfile
 
  sbatch -p speech -o $logfile -n20  \
  steps/decode_nnet.sh --nj 20 --cmd "$decode_cmd" --parallel-opts "" --config conf/decode_dnn.config --acwt 0.0833 \
  exp/tri4a/graph  data/local/fmllr-tri4a/dev  $dir/decode_dev || exit 1;
fi

StopHere;

# rescore eval2000 with trigram sw1_fsh
steps/lmrescore.sh --mode 3 --cmd "$decodebig_cmd" data/lang_sw1_fsh_tgpr data/lang_sw1_fsh_tg data/eval2000 \
  $dir/decode_eval2000_sw1_fsh_tgpr $dir/decode_eval2000_sw1_fsh_tg.3 || exit 1 
}



###
### Finally train the DNN using sMBR criterion.
### We do Stochastic-GD with per-utterance updates. 
###
### To get faster convergence, we will re-generate 
### the lattices after 1st epoch of sMBR.
###

dir=exp/tri4b_pretrain-dbn_dnn_smbr
srcdir=exp/tri4b_pretrain-dbn_dnn
acwt=0.08333

# First we need to generate lattices and alignments:
#false && \
{
steps/align_nnet.sh --nj 250 --cmd "$train_cmd" \
  data-fmllr-tri4b/train_nodup data/lang $srcdir ${srcdir}_ali_all || exit 1;
steps/make_denlats_nnet.sh --nj 250 --cmd "$decode_cmd" --config conf/decode_dnn.config --acwt $acwt \
  data-fmllr-tri4b/train_nodup data/lang $srcdir ${srcdir}_denlats_all  || exit 1;
}
# Now we re-train the hybrid by single iteration of sMBR 
#false && \
{
steps/train_nnet_mpe.sh --cmd "$cuda_cmd" --num-iters 1 --acwt $acwt --do-smbr true \
  data-fmllr-tri4b/train_nodup data/lang $srcdir \
  ${srcdir}_ali_all \
  ${srcdir}_denlats_all \
  $dir || exit 1
}
# Decode
#false && \
{
for ITER in 1; do
  # decode eval2000 with pruned trigram sw1_fsh_tgpr
  steps/decode_nnet.sh --nj 30 --cmd "$decode_cmd" --config conf/decode_dnn.config \
    --nnet $dir/${ITER}.nnet --acwt $acwt \
    exp/tri4b/graph_sw1_fsh_tgpr data-fmllr-tri4b/eval2000 $dir/decode_eval2000_sw1_fsh_tgpr_it${ITER} || exit 1
  # rescore eval2000 with trigram sw1_fsh
  steps/lmrescore.sh --mode 3 --cmd "$decodebig_cmd" data/lang_sw1_fsh_tgpr data/lang_sw1_fsh_tg data/eval2000 \
    $dir/decode_eval2000_sw1_fsh_tgpr_it${ITER} $dir/decode_eval2000_sw1_fsh_tg.3_it${ITER} || exit 1 
done 
}


###
### Re-generate lattices and run several more iterations of sMBR
###

dir=exp/tri4b_pretrain-dbn_dnn_smbr_iter1-lats
srcdir=exp/tri4b_pretrain-dbn_dnn_smbr
acwt=0.08333

# First we need to generate lattices and alignments:
#false && \
{
steps/align_nnet.sh --nj 250 --cmd "$train_cmd" \
  data-fmllr-tri4b/train_nodup data/lang $srcdir ${srcdir}_ali_all || exit 1;
steps/make_denlats_nnet.sh --nj 250 --cmd "$decode_cmd" --config conf/decode_dnn.config --acwt $acwt \
  data-fmllr-tri4b/train_nodup data/lang $srcdir ${srcdir}_denlats_all  || exit 1;
}
# Now we re-train the hybrid by several iterations of sMBR 
#false && \
{
steps/train_nnet_mpe.sh --cmd "$cuda_cmd" --num-iters 2 --acwt $acwt --do-smbr true \
  data-fmllr-tri4b/train_nodup data/lang $srcdir \
  ${srcdir}_ali_all \
  ${srcdir}_denlats_all \
  $dir || exit 1
}
# Decode
#false && \
{
for ITER in 1 2; do
  # decode eval2000 with pruned trigram sw1_fsh_tgpr
  steps/decode_nnet.sh --nj 30 --cmd "$decode_cmd" --config conf/decode_dnn.config \
    --nnet $dir/${ITER}.nnet --acwt $acwt \
    exp/tri4b/graph_sw1_fsh_tgpr data-fmllr-tri4b/eval2000 $dir/decode_eval2000_sw1_fsh_tgpr_it${ITER} || exit 1
  # rescore eval2000 with trigram sw1_fsh
  steps/lmrescore.sh --mode 3 --cmd "$decodebig_cmd" data/lang_sw1_fsh_tgpr data/lang_sw1_fsh_tg data/eval2000 \
    $dir/decode_eval2000_sw1_fsh_tgpr_it${ITER} $dir/decode_eval2000_sw1_fsh_tg.3_it${ITER} || exit 1 
done 
}



# Getting results [see RESULTS file]
# for x in exp/*/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh; done
