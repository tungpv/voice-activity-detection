#!/bin/bash

train_pnorm_gpu_vx() {
  func=$FUNCNAME; 
  echo -e "\n## $func $@\n"
  # begin options
  cmd=run.pl
  nj=4
  cmvn_opts=
  feat_type="user"
  num_utts_subset=300
  samples_per_iter=4000000
  num_jobs_nnet=1;
  dnn_init_learning_rate=0.01
  dnn_final_learning_rate=0.001
  num_threads=1
  dnn_input_dim=3000
  dnn_output_dim=500
   
  # end options
  . utils/parse_options.sh
  data=$1; shift; lang=$1; shift; alidir=$1; shift;  dir=$1; shift
  
  [ -d $dir ] || mkdir -p $dir
  # Get list of validation utterances. 
  if [ ! -f $dir/valid_uttlist ]; then
    awk '{print $1}' $data/utt2spk | utils/shuffle_list.pl | head -$num_utts_subset \
    > $dir/valid_uttlist
  fi
  if [ ! -f $dir/train_subset_uttlist ]; then
    awk '{print $1}' $data/utt2spk | utils/filter_scp.pl --exclude $dir/valid_uttlist | \
     head -$num_utts_subset > $dir/train_subset_uttlist
  fi

  nj=$(cat $alidir/num_jobs); sdata=$data/split$nj; [ -d $sdata ] || utils/split_data.sh $data $nj
  all_feats="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- |"
  feats="ark,s,cs:utils/filter_scp.pl --exclude $dir/valid_uttlist $sdata/JOB/feats.scp | apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:- ark:- |"
  valid_feats="ark,s,cs:utils/filter_scp.pl $dir/valid_uttlist $data/feats.scp | apply-cmvn $cmvn_opts --utt2spk=ark:$data/utt2spk scp:$data/cmvn.scp scp:- ark:- |"
  train_subset_feats="ark,s,cs:utils/filter_scp.pl $dir/train_subset_uttlist $data/feats.scp | apply-cmvn $cmvn_opts --utt2spk=ark:$data/utt2spk scp:$data/cmvn.scp scp:- ark:- |"

  echo "$all_feats" > $dir/all_feats
  echo "$feats" > $dir/feats
  echo "$valid_feats" > $dir/valid_feats
  echo "$train_subset_feats" > $dir/train_subset_feats
  
  if [ ! -f $dir/num_frames ]; then
    num_frames=`feat-to-len scp:$data/feats.scp  ark,t:- | awk '{x += $2;} END{print x;}'` || exit 1;
    echo $num_frames > $dir/num_frames
  fi
  if [ ! -f $dir/final.mdl ]; then
    isteps/nnet2x/train_pnorm.sh --feat-type $feat_type \
    --initial-learning-rate $dnn_init_learning_rate \
    --final-learning-rate $dnn_final_learning_rate \
    --samples-per-iter $samples_per_iter \
    --num-jobs-nnet $num_jobs_nnet --num-threads $num_threads \
    --num-hidden-layers $dnn_num_hidden_layers \
    --pnorm-input-dim $dnn_input_dim \
    --pnorm-output-dim $dnn_output_dim \
    --cmd "$cmd" \
    $data  $lang $ali $dir || exit 1
 fi
}
align_si() {
  func=$FUNCNAME
  echo -e "\n## $0 $@\n"
  boost_sil=1.5
  cmd=run.pl
  nj=4;
  . parse_options.sh
  data=$1; lang=$2; sdir=$3;  alidir=$4;
  if [ ! -f $alidir/.ali_done ]; then
    steps/align_si.sh --boost-silence $boost_sil --nj $nj --cmd "$cmd" \
    $data $lang $exp/tri1 $alidir || exit 1
    touch $alidir/.ali_done
  fi
}
align_fmllr() {
  func=$FUNCNAME
  echo -e "\n## $0 $@\n"
  boost_sil=1.5
  cmd=run.pl
  nj=4
  . parse_options.sh
  if [ ! -f  $alidir/.ali_done ]; then
    steps/align_fmllr.sh \
    --boost-silence $boost_sil --nj $train_nj --cmd "$train_cmd" \
    $train $lang $exp/tri5 $exp/tri5_ali || exit 1
  fi
}
ali_to_phone_text() {
  func=$FUNCNAME;
  echo -e "\n## $func $@\n"
  # begin options
  # end options
  . utils/parse_options.sh
  if [ $# -ne 4 ]; then
    echo -e "\n## Usage: $func lang sdir alidir dir\n"; exit 1
  fi
  lang=$1; shift
  sdir=$1; shift; 
  alidir=$1; shift;
  dir=$1; shift

  if [ ! -f $alidir/ali.1.gz ]; then
    echo -e "\n## $func ali.1.gz is expected\n"; exit 1
  fi 
  if [ ! -f $sdir/final.mdl ]; then
    ecoh -e "\n## $func final.mdl expected in $sdir\n"; exit 1
  fi
  [ -d $dir ] || mkdir -p $dir 
  ali-to-phones  --write-lengths  $sdir/final.mdl  "ark:gzip -cd $alidir/ali.*.gz|" ark,t:- | \
  perl -ne '@A=split(/;/); $lab = shift @A;  $lab =~ m/(\S+)\s+(\S+)\s+(\S+)/ or next; 
            $lab ="$1 $2";  $s = ""; for($i=0; $i<@A; $i++) { $pstr = $A[$i]; if ($pstr =~ m/(\S+)\s+(\S+)/){ $s .=" $1";  }   } print "$lab$s\n";  ' | utils/int2sym.pl -f 2- $lang/phones.txt  | \
  perl -ne '@A=split(/\s+/); $lab =shift @A; $s =join " ", @A;
    $s =~ s/_[BIES]//g; print "$lab $s\n"; ' > $dir/text.phone || exit 1
  echo -e "\n## $func done !\n"
}

train_pnorm_gpu() {
  func=$FUNCNAME; 
  echo -e "\n## $func $@\n"
  # begin options
  cmd=run.pl
  nj=4
  cmvn_opts=
  feat_type="user"
  num_utts_subset=300
  samples_per_iter=4000000
  num_jobs_nnet=1;
  init_learning_rate=0.01
  final_learning_rate=0.001
  num_threads=1
  dnn_input_dim=3000
  dnn_output_dim=500
  num_hidden_layers=3
  transform_dir=
  # end options
  . utils/parse_options.sh
  data=$1; shift; lang=$1; shift; alidir=$1; shift;  dir=$1; shift
  
  [ -d $dir ] || mkdir -p $dir
  if [ ! -f $dir/final.mdl ]; then
    steps/nnet2/train_pnorm.sh  --initial-learning-rate $init_learning_rate \
    --final-learning-rate $final_learning_rate \
    ${transform_dir:+ --transform-dir $transform_dir} \
    --samples-per-iter $samples_per_iter \
    --num-jobs-nnet $num_jobs_nnet --num-threads $num_threads \
    --num-hidden-layers $num_hidden_layers \
    --pnorm-input-dim $dnn_input_dim \
    --pnorm-output-dim $dnn_output_dim \
    --cmd "$cmd" \
    $data  $lang $ali $dir || exit 1
 fi
 echo -e "\n## $func Done `date` !\n"
}

nnet_train() {
  func=$FUNCNAME
  echo -e "\n## $0 $@\n"
  cmd=run.pl
  nj=4;
  alidata=
  fmllr=false
  ndev_hlen=1
  nn_depth=7
  hidden_dim=1204
  learn_rate=0.08
  delta_order=
  ldadir=
  pretrain="steps/nnet/pretrain_dbn.sh"
  train="steps/nnet/train.sh"

  . utils/parse_options.sh
  if [ $# -ne 4 ]; then
    echo -e "\n## $func [options] data lang gmmdir dir \n"; exit 1
  fi
  data=$1; shift; lang=$1; shift; gmmdir=$1; shift; dir=$1; shift
  
  ndev=$data/ndev;
  ndevali=$gmmdir/nali/ndev;
  if [ -z $alidata ]; then
    isteps/um/mksubset_dir.sh --hlen $ndev_hlen  $data $ndev || exit 1
    nj=`wc -l < $ndev/spk2utt`; if [ $nj -gt 30 ]; then nj=30; fi
    if [ ! -f $ndevali/.ali_done ]; then
      if $fmllr; then
        steps/align_fmllr.sh --nj $nj --cmd "$cmd" $ndev $lang $gmmdir $ndevali || exit 1
      else
        steps/align_si.sh --nj $nj --cmd "$cmd" $ndev $lang $gmmdir $ndevali || exit 1
      fi
      touch $ndevali/.ali_done
    fi
  else
    sdata=$data; data=$alidata;
    ndev1=$data/ndev;
    isteps/um/mksubset_dir.sh --hlen $ndev_hlen  $data $ndev1 || exit 1
    nj=`wc -l < $ndev1/spk2utt`; if [ $nj -gt 30 ]; then nj=30; fi
    if [ ! -f $ndevali/.ali_done ]; then
      if $fmllr; then
        steps/align_fmllr.sh --nj $nj --cmd "$cmd" $ndev1 $lang $gmmdir $ndevali || exit 1
      else
        steps/align_si.sh --nj $nj --cmd "$cmd" $ndev1 $lang $gmmdir $ndevali || exit 1
      fi
      touch $ndevali/.ali_done
    fi
    data=$sdata; 
    isteps/um/mksubset_dir.sh --hlen $ndev_hlen  $data $ndev || exit 1
  fi
  ntrain=$data/ntrain;
  ntrainali=$gmmdir/nali/ntrain;
  if [ -z $alidata ]; then
    isteps/um/mksubset_dir.sh --exclude $data/ndev/segments  $data  $ntrain || exit 1
    nj=`wc -l < $ndev/spk2utt`; if [ $nj -gt 30 ]; then nj=30; fi
    if [ ! -f $ntrainali/.ali_done ]; then
      if $fmllr; then
        steps/align_fmllr.sh --nj $nj --cmd "$cmd" $ntrain $lang $gmmdir $ntrainali || exit 1
      else
        steps/align_si.sh --nj $nj --cmd "$cmd" $ntrain $lang $gmmdir $ntrainali || exit 1
      fi
      touch $ntrainali/.ali_done
    fi
  else
    sdata=$data; data=$alidata; ntrain1=$data/ntrain;
    isteps/um/mksubset_dir.sh --exclude $data/ndev/segments  $data  $ntrain1 || exit 1
    nj=`wc -l < $ntrain1/spk2utt`; if [ $nj -gt 30 ]; then nj=30; fi
    if [ ! -f $ntrainali/.ali_done ]; then
      if $fmllr; then
        steps/align_fmllr.sh --nj $nj --cmd "$cmd" $ntrain1 $lang $gmmdir $ntrainali || exit 1
      else
        steps/align_si.sh --nj $nj --cmd "$cmd" $ntrain1 $lang $gmmdir $ntrainali || exit 1
      fi
      touch $ntrainali/.ali_done
    fi
    data=$sdata; 
    isteps/um/mksubset_dir.sh --exclude $data/ndev/segments  $data $ntrain || exit 1
  fi
  pretrain_dir=$dir/pretrain_dbn
  if [ ! -f $pretrain_dir/._pretrain_done ]; then
    echo -e "\n## $func: pretrain started `date`\n"
    $cmd $pretrain_dir/pretrain_dbn.log \
    $pretrain --nn-depth $nn_depth \
    --hid-dim $hidden_dim \
    ${delta_order:+ --delta-order $delta_order} \
    ${ldadir:+ --ldadir $ldadir} \
    $ntrain  $pretrain_dir || exit 1;
    touch $pretrain_dir/._pretrain_done
    echo -e "\n## $func: pretraining done `date`\n"
  fi
  dnn_dir=$dir/dnn_xent
  feature_transform=$pretrain_dir/final.feature_transform;
  dbn=$pretrain_dir/$nn_depth.dbn;
  if [ ! -f $dnn_dir/.dnn_done ]; then
    echo -e  "\n## $func: dnn training started `date`\n";
    $cmd $dnn_dir/_train_nnet.log $train \
    --feature-transform $feature_transform \
    ${ldadir:+ --ldadir $ldadir} \
    ${delta_order:+ --delta-order $delta_order} \
    --dbn $dbn \
    --hid-layers 0  \
    --learn-rate $learn_rate \
    $ntrain $ndev $lang $ntrainali $ndevali $dnn_dir || exit 1; 
    touch $dnn_dir/.dnn_done
    echo -e "\n## $func: dnn training ended `date`\n"
  fi

  return;
} 



