#!/bin/bash
split_data() {
  data=$1; shift; nj=$1;
  sdata=$data/split$nj; [ -d $sdata ] || utils/split_data.sh $data $nj || exit 1
  echo -n $sdata;
}
function JobNum {
  data=$1; max=$2; func=$FUNCNAME
  if [ ! -f $data/spk2utt ]; then
    echo -e "\n## $func spk2utt file is not ready in $data\n" 1>&2; exit 1
  fi
  if [ $max -le 0 ]; then
    echo -e "\n## $func max($max) is illegal\n" 1>&2 ; exit 1
  fi
  nj=`wc -l < $data/spk2utt`; 
  echo -e "\nmaximum_nj=$nj\n" 1>&2 ;
  if [ $nj -gt $max ]; then nj=$max; fi
  echo -n $nj;
}

make_tandem_feature() {
  func=$FUNCNAME; 
  echo -e "\n## $func $@\n"
  # begin options
  cmd=run.pl
  nj=4
  ldadir=
  transform_dir=
  cmvn_opts=
  # end options
  . utils/parse_options.sh
  if [ $# -ne 4 ]; then
    echo -e "\n## usage example: $func [options] data1 data2 data feature\n"; exit 1
  fi
  data1=$1; shift;   data2=$1; shift;  data=$1; shift; feature=$1; shift

  echo -e  "\n## $func assemble feats1\n";
  if [[ -z $cmvn_opts && ! -z $ldadir && -f $ldadir/cmvn_opts  ]]; then
     cmvn_opts=$(cat $ldadir/cmvn_opts);
  fi
  sdata1=$(split_data $data1 $nj )
  feats1="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata1/JOB/utt2spk scp:$sdata1/JOB/cmvn.scp scp:$sdata1/JOB/feats.scp ark:- |"
  if [[ ! -z $ldadir && -f $ldadir/final.mat ]]; then
    feats1="$feats1 splice-feats ark:- ark:- | transform-feats $ldadir/final.mat ark:- ark:- |"
  fi
  if [[ ! -z $transform_dir && -f $transform_dir/trans.1 ]]; then
    if [ $nj -ne `cat $transform_dir/num_jobs` ]; then 
      echo -e "\n## ERROR $func $nj mismatched with `cat  $transform_dir/num_jobs`\n"; exit 1;
    fi
   feats1="$feats1 transform-feats --utt2spk=ark:$sdata1/JOB/utt2spk ark,s,cs:$transform_dir/trans.JOB ark:- ark:- |"
  fi
  echo -e "\n## $func assemble feats2\n"
  sdata2=$(split_data $data2 $nj)
  feats2="ark,s,cs:apply-cmvn  --utt2spk=ark:$sdata2/JOB/utt2spk scp:$sdata2/JOB/cmvn.scp scp:$sdata2/JOB/feats.scp ark:- |"
  
  feats="ark,s,cs:paste-feats '$feats1' '$feats2' ark:- |"
  if [ ! -f $data/.tandem_done ]; then
    [ -d $data ] || mkdir -p $data; dataname=`basename $data`
    cp $data2/* $data; rm $data/{feats.scp,cmvn.scp}
    logdir=$feature/_log; featdir=$feature/_data;
    [ -d $featdir ] || mkdir -p $featdir
    $cmd JOB=1:$nj $logdir/make_fmllr_feats.JOB.log \
    copy-feats "$feats" \
    ark,scp:$featdir/feats_tandem_$dataname.JOB.ark,$featdir/feats_tandem_$dataname.JOB.scp || exit 1;
    for n in $(seq 1 $nj); do
      cat $featdir/feats_tandem_$dataname.$n.scp 
    done > $data/feats.scp
    steps/compute_cmvn_stats.sh $data  $logdir  $featdir || exit 1
    utils/fix_data_dir.sh $data || exit 1;
    touch $data/.tandem_done
  fi
  echo "$feats1" > $data/feats1
  echo "$feats2" > $data/feats2
  echo "$feats" > $data/feats 
  echo -e "\n## $func done !\n";
}
make_bn_feats() {
  func=$FUNCNAME; 
  echo -e "\n## $func $@\n"
  # begin options
  cmd=run.pl
  nj=4
  cmvn_opts=
  bnfid=
  # end options
  . utils/parse_options.sh
  if [ $# -ne 3 ]; then
    echo -e "\n## usage example: $func [options] sdata nndir featdir\n"
  fi
  sdata=$1; shift
  nndir=$1; shift
  featdir=$1; shift

  datadir=$(dirname $sdata); x=$(basename $sdata)  
  data=$datadir/fbank_pitch/$x; feat=$featdir/fbank_pitch/$x;
  if [ ! -f $data/.fbank_done ]; then
    echo -e "\n## $func making fbank_pitch feature\n"
    [ -d $data ] || mkdir -p $data; cp $sdata/* $data 
    steps/make_fbank_pitch.sh  --nj $nj --cmd "$cmd" $data $feat/_log $feat/_data || exit 1
    steps/compute_cmvn_stats.sh $data $feat/_log $feat/_cmvn || exit 1
    touch $data/.fbank_done
  fi
  sdata=$data; data=$datadir/bnf_$bnfid/$x; feat=$featdir/bnf_$bnfid/$x;
  if [ ! -f $data/.bnf_done ]; then
    echo -e "\n## $func making bnf feature\n"
    [ -d $data ] || mkdir -p $data; cp $sdata/* $data 
    steps/nnet/make_bn_feats.sh --nj $nj --cmd "$cmd" $data  $sdata $nndir $feat/_log $feat/_data || exit 1
    steps/compute_cmvn_stats.sh $data $feat/_log $feat/_cmvn || exit 1
    utils/fix_data_dir.sh $data || exit 1
    touch $data/.bnf_done
  fi
  echo -e "\n$func making bnf in $data done \n";
}
