#!/bin/bash

function make_graph {
  func=$FUNCNAME; 
  lang=$1; sdir=$2; graph=$3;
  if [ ! -f $graph/.graph_done ]; then
    echo -e "\n## $func making graph in $sdir\n";
    utils/mkgraph.sh  $lang $sdir $graph || exit 1
    touch $graph/.graph_done
  fi
} 

function prepare_acoustic_data {
  func=$FUNCNAME;
  echo -e "\n## $func $@\n"
  dict=$1; sdata=$2; data=$3;
  if [ ! -f $data/.prepare_done ]; then
    [ -d $data ] || mkdir -p $data; cp $sdata/* $data
    cat $sdata/text | ilocal/magor/word_seq_to_morp_seq.pl $dict  "|cat - "| \
    perl -pe 's/^[\s]+//; s/[\s]+/ /;' >  $data/text  || exit 1
    touch $data/.prepare_done
  fi
} 

function prepare_acoustic_data_for_partial {
  func=$FUNCNAME;
  echo -e "\n## function is $func\n"
  if [ $# -ne 4 ]; then
    echo -e "\n## Usage: $func <shorted_word_to_word.dict> <word_to_morph.idct> <sdata> <data>\n"
    exit 1
  fi
  shorted_word_to_word_dict=$1; word_to_morph_dict=$2; sdata=$3; data=$4;
  if [ ! -f $data/.prepare_done ]; then
    [ -d $data ] || mkdir -p $data; cp $sdata/* $data
    cat $sdata/text | \
    ilocal/magor/word_seq_to_morp_seq.pl $shorted_word_to_word_dict "|cat -" | \
    ilocal/magor/word_seq_to_morp_seq.pl "cat $word_to_morph_dict | ilocal/magor/filter_this.pl $shorted_word_to_word_dict|" "| cat -" | perl -pe 's/^[\s]+//; s/[\s]+/ /;' >  $data/text || exit 1 
    touch $data/.prepare_done
  fi
}

function decode_sat {
  func=$FUNCNAME
  echo -e "\n## $func $@\n"
  cmd=run.pl;
  nj=4;
  beam=10;   
  latbeam=6;  
  skip_scoring=false;  
  sdir=  

  . parse_options.sh
  if [ $# -ne 4 ]; then
    echo -e "\nUsage:$func [options] data lang graph dir\n"; exit 1
  fi
  data=$1; shift
  lang=$1; shift
  graph=$1; shift
  dir=$1; shift
  [ -z $sdir ] && sdir=$(dirname $dir)
  make_graph $lang $sdir $graph || exit 1;
  if [ ! -f $dir/.decode_done ]; then
    echo -e "\n## $func  decoding started `date`\n"
    steps/decode_fmllr_extra.sh --skip-scoring $skip_scoring \
    --beam $beam  --lattice-beam $latbeam   --nj $nj --cmd "$cmd"  $graph $data  $dir  || exit 1
    echo -e "\n## $func decoding ended `date`\n"
    touch $dir/.decode_done
 fi
}

function sgmm2_latgen {
   func=$FUNCNAME;
   echo -e "\n## $func $@\n"
   cmvn_opts="--norm-vars=false"
   transform_dir=
   cmd=run.pl
   max_mem=$[100*5000000];
   max_active=7000;
   lattice_beam=9.0;
   beam=13.0
   acwt=0.1
   splice_opts=
  . parse_options.sh
  if [ $# -ne 5 ]; then
     echo -e "\n## $func [options] prev_decodedir data lang graph dir\n"; exit 1
  fi
  prev_decodedir=$1; shift
  data=$1; shift
  lang=$1; shift
  graph=$1; shift
  dir=$1;  shift 
  nj=`cat $prev_decodedir/num_jobs`; 
  if [ -z $nj ]; then
    echo -e "\n## $func num_jobs file expected in $prev_decodedir\n"; exit 1
  fi
  sdir=`dirname $dir`
  if [ -f $sdir/final.mat ]; then feat_type=lda; else feat_type=delta; fi
  sdata=$data/split$nj; 
  if [ ! -d $sdata ]; then
    echo -e "\n## $func sdata $sdata is not ready \n"; exit 1
  fi
  case $feat_type in
    delta) feats="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- | add-deltas ark:- ark:- |";;
    lda) feats="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- | splice-feats $splice_opts ark:- ark:- | transform-feats $sdir/final.mat ark:- ark:- |"
    ;;
    *) echo "$0: invalid feature type $feat_type" && exit 1;
  esac
  if [ ! -z $transform_dir ]; then
    feats="$feats transform-feats --utt2spk=ark:$sdata/JOB/utt2spk ark,s,cs:$transform_dir/trans.JOB ark:- ark:- |"
  fi
  gselect_opt="--gselect=ark,s,cs:gunzip -c $prev_decodedir/gselect.JOB.gz|";
  spkvecs_opt="--utt2spk=ark:$sdata/JOB/utt2spk --spk-vecs=ark:$prev_decodedir/vecs.JOB";

  if [ ! -f $dir/.decode_done ]; then
    echo -e "\n## sgmm2 decoding started `date`\n";
    $cmd JOB=1:$nj $dir/log/decode.JOB.log \
    sgmm2-latgen-faster --max-mem=$max_mem \
    --max-active=$max_active --beam=$beam --lattice-beam=$lattice_beam \
    --acoustic-scale=$acwt  --allow-partial=true \
    --word-symbol-table=$graph/words.txt "$gselect_opt" "$spkvecs_opt"  $sdir/final.mdl \
    $graph/HCLG.fst "$feats" "ark:|gzip -c > $dir/lat.JOB.gz" || exit 1;
    touch $dir/.decode_done
    echo -e "\n## sgmm2 decoding ended `date`\n"
fi

}

function decode_sigmoid_nnet {
  func=$FUNCNAME;
  echo -e "\n## $func $@\n";
   nj=30;
   cmd=run.pl
   acwt=0.1
   transform_dir=
   beam=13.0
   latbeam=6.0
   fmllr=true
   gmmdir=
   
   max_mem=$[100*5000000];
    
  . parse_options.sh
  if [ $# -ne 3 ]; then
    echo -e "\n## $func [options] data graph dir\n"; exit 1
  fi
  data=$1; shift
  graph=$1; shift
  dir=$1; shift
  if ! $fmllr; then
    echo -e "\n## $0 making fmllr feature \n"
    if [[ -z $gmmdir  ]]; then
      echo -e "\n## $func gmmdir  should be specified\n"; exit 1
    fi
    fmllrdata=$(dir=`dirname $data`; echo $dir/fmllr)
    fmllrfeat=$(dir=`head -1 $data/feats.scp|awk '{print $2;}'`; dir1=`dirname $dir`; dir2=`dirname $dir1`; echo $dir2/fmllr)
    fmllr_nj=`cat $transform_dir/num_jobs`
    if [ ! -f $fmllrdata/.fmllr_done ]; then
      steps/make_fmllr_feats.sh  --cmd "$cmd" --nj $fmllr_nj \
      ${transform_dir:+  --transform-dir $transform_dir} \
      $fmllrdata $data $gmmdir $fmllrfeat/_log $fmllrfeat/_data || exit 1
      touch $fmllrdata/.fmllr_done
    fi
    data=$fmllrdata
  fi
  if [ ! -f $dir/.decode_done ]; then
    isteps/decode_nnet.sh --nj $nj  --cmd  "$cmd" \
    --max-mem $max_mem --beam $beam --latbeam $latbeam  --acwt $acwt \
    $graph  $data $dir || exit 1
    touch $dir/.decode_done
  fi

}
function decode_pnorm_v1 {
  func=$FUNCNAME;
  echo -e "\n## $func $@\n"
  nj=30;
  cmd=run.pl
  acwt=0.1
  transform_dir=
  beam=13.0
  latbeam=6.0
  # max_mem=$[100*5000000];
  skip_scoring=false  
  . parse_options.sh
  if [ $# -ne 3 ]; then
    echo -e "\n## $func [options] data graph dir\n"; exit 1
  fi
  data=$1; shift
  graph=$1; shift
  dir=$1; shift
  [ ! -z $transform_dir ] && nj=`cat $transform_dir/num_jobs`;
  if [ ! -f $dir/.decode_done ]; then
    echo -e "\n## $func: decode started `date`\n"
    steps/nnet2/decode.sh --cmd "$cmd" --nj $nj --skip-scoring $skip_scoring  \
    ${transform_dir:+ --transform-dir $transform_dir} --beam $beam --lat-beam $latbeam \
    $graph $data  $dir || exit 1
    touch $dir/.decode_done
    echo -e "\n## $func: done `date`\n"
  fi
}

function decode_sgmm2 {
  func=$FUNCNAME; 
  cmd=run.pl; nj=4;
  beam=10;    latbeam=6;   skip_scoring=false;   sdir=  
  transform_dir=
  . parse_options.sh
  if [ $# -ne 4 ]; then
    echo -e "\nUsage:$func [options] data lang graph dir\n"; exit 1
  fi
  data=$1; shift
  lang=$1; shift
  graph=$1; shift
  dir=$1; shift
  [ -z $sdir ] && sdir=$(dirname $dir)
  make_graph $lang $sdir $graph
  if [ ! -f $dir/.decode_done ]; then
    steps/decode_sgmm2.sh  --skip-scoring $skip_scoring --beam $beam --lattice-beam $latbeam \
    --cmd "$cmd" --nj $nj --transform-dir $transform_dir  $graph $data $dir || exit 1 
    touch $dir/.decode_done
  fi
}

decode_pnorm_vx () {
  func=$FUNCNAME;
  echo -e "\n## $func $@\n"
  cmd=run.pl; nj=4;
  minimize=true
  beam=10
  latbeam=6.0
  acwt=0.1
  skip_scoring=false
  transform_dir=
  feat_type=
  cmvn_opts=
  feats=
  . parse_options.sh

  data=$1; shift
  graph=$1; shift
  dir=$1; shift
  sdata=$data/split$nj; [ -d $sdata ] || split_data.sh $data $nj
  [ -d $dir ] || mkdir -p $dir
  echo $nj > $dir/num_jobs
  if [[ $feat_type == "user"  ]]; then
    if [ -z $feats ]; then
      feats="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- |"
    fi
    echo "$feats" > $dir/feats
  fi
 

  steps/nnet2/decode.sh ${feat_type:+ --feat-type $feat_type}  \
  --minimize $minimize --cmd "$cmd" --nj $nj \
  --beam $beam --lat-beam $latbeam \
  --skip-scoring $skip_scoring  \
  ${transform_dir:+ --transform-dir $transform_dir} \
  $graph $data  $dir || exit 1
}

decode_nnet() {
  func=$FUNCNAME
  echo -e "\n## $func $@\n" 
  nj=4
  cmd=run.pl
  ldadir=
  transform_dir=
  skip_scoring=false
  max_mem=$[5000000*100]
  beam=13
  latbeam=7
  acwt=0.1
 
  . parse_options.sh
  if [ $# -ne 3 ]; then
    echo -e "\n## usage: $func graph data dir\n"; exit 1
  fi
  graph=$1; shift; data=$1; shift; dir=$1;shift

  if [ ! -f $dir/.done ]; then
    steps/nnet/decode.sh --nj $nj --skip-scoring $skip_scoring \
    ${ldadir:+ --ldadir $ldadir}  --cmd  "$cmd" \
    --max-mem $max_mem --beam $beam --latbeam $latbeam  --acwt $acwt \
    $graph  $data $dir || exit 1
    touch $dir/.done
  fi

}

decode_si() {
  func=$FUNCNAME;
  echo -e "\n## $func $@\n"
  lang=
  skip_scoring=false
  acwt=0.1
  beam=13.0
  latbeam=6.0
  cmd=run.pl
  nj=4
  . parse_options.sh
  if [ $# -ne 3 ]; then
     echo -e "\n## usage: $func graph data dir \n"; exit 1
  fi  
  graph=$1; shift; data=$1; shift; dir=$1; shift
  if [[ ! -f $graph/HCLG.fst ]]; then
    if [ -z $lang ]; then  echo -e "\n## $func lang expected\n"; exit 1;   fi
    utils/mkgraph.sh  $lang $sdir $graph || exit 1
  fi
  if [ ! -f $dir/.decode_done ]; then
    steps/decode.sh --skip-scoring $skip_scoring \
    --beam $beam  --latbeam $latbeam --acwt $acwt \
    --nj $nj --cmd "$cmd"  $graph $data  $dir  || exit 1
   touch $dir/.decode_done
  fi
}

score_wer(){
  func=$FUNCNAME
  echo -e "\n## $func $@\n"
  cmd=run.pl
  min_lmwt=4
  max_lmwt=15
  wdpen=0;
  set -o  pipefail
  . parse_options.sh
  if [ $# -ne 3 ]; then
    echo -e "\n## $func data lang decode_dir\n"; exit 1
  fi
  data=$1; shift;  lang=$1;shift ; dir=$1; shift; 
  symtab=$lang/words.txt
  wdpendir=$dir/lat_has_wdpen
  nj=$(cat $dir/num_jobs)
  echo -e "\n## $func wdpen=$wdpen\n"
  $cmd JOB=1:$nj $wdpendir/log/JOB.log \
  lattice-add-penalty --word-ins-penalty=$wdpen "ark:gzip -cd $dir/lat.JOB.gz|" \
  "ark:|gzip -c > $wdpendir/lat.JOB.gz" || exit 1

  $cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring/log/LMWT.log \
  lattice-best-path --lm-scale=LMWT --word-symbol-table=$symtab \
  "ark:gunzip -c $wdpendir/lat.*.gz|" ark,t:- \| \
   utils/int2sym.pl -f 2- $symtab \| \
   iutils/remove_brackets.pl \| \
   compute-wer --text --mode=present \
   ark:$dir/scoring/test_filt.txt  ark,p:-  ">&" $dir/wer_LMWT || exit 1
   grep WER $dir/wer_* | sort -k2n
}
