#!/bin/bash

if [ ! -f $data/train_sub3/.done ]; then
  echo ---------------------------------------------------------------------
  echo "Subsetting monophone training data in data/train_sub[123] on" `date`
  echo ---------------------------------------------------------------------
  numutt=`cat $train/feats.scp | wc -l`;
  utils/subset_data_dir.sh $train  5000 $data/train_sub1
  if [ $numutt -gt 10000 ] ; then
    utils/subset_data_dir.sh $train 10000 $data/train_sub2
  else
    (cd $data; ln -s $train train_sub2 )
  fi
  if [ $numutt -gt 20000 ] ; then
    utils/subset_data_dir.sh $train 20000 $data/train_sub3
  else
    (cd $data; sdir=`pwd`; trainName=`basename $train`; abs_train=$sdir/$trainName;  ln -s $abs_train train_sub3 )
  fi

  touch $data/train_sub3/.done
fi

if [ ! -f $exp/mono/.done ]; then
  echo ---------------------------------------------------------------------
  echo "Starting (small) monophone training in exp/mono on" `date`
  echo ---------------------------------------------------------------------
  steps/train_mono.sh \
    --boost-silence $boost_sil --nj 8 --cmd "$train_cmd" \
    $data/train_sub1 $lang $exp/mono
  touch $exp/mono/.done
fi

if [ ! -f $exp/tri1/.done ]; then
  echo ---------------------------------------------------------------------
  echo "Starting (small) triphone training in exp/tri1 on" `date`
  echo ---------------------------------------------------------------------
  steps/align_si.sh \
    --boost-silence $boost_sil --nj 12 --cmd "$train_cmd" \
    $data/train_sub2 $lang $exp/mono $exp/mono_ali_sub2 || exit 1
  steps/train_deltas.sh \
    --boost-silence $boost_sil --cmd "$train_cmd" $numLeavesTri1 $numGaussTri1 \
    $data/train_sub2 $lang $exp/mono_ali_sub2 $exp/tri1 || exit 1
  touch $exp/tri1/.done
fi


echo ---------------------------------------------------------------------
echo "Starting (medium) triphone training in exp/tri2 on" `date`
echo ---------------------------------------------------------------------
if [ ! -f $exp/tri2/.done ]; then
  steps/align_si.sh \
    --boost-silence $boost_sil --nj 24 --cmd "$train_cmd" \
    $data/train_sub3 $lang $exp/tri1 $exp/tri1_ali_sub3 || exit 1
  steps/train_deltas.sh \
    --boost-silence $boost_sil --cmd "$train_cmd" $numLeavesTri2 $numGaussTri2 \
    $data/train_sub3 $lang $exp/tri1_ali_sub3 $exp/tri2 || exit 1
  touch $exp/tri2/.done
fi

echo ---------------------------------------------------------------------
echo "Starting (full) triphone training in exp/tri3 on" `date`
echo ---------------------------------------------------------------------
if [ ! -f $exp/tri3/.done ]; then
  steps/align_si.sh \
    --boost-silence $boost_sil --nj $train_nj --cmd "$train_cmd" \
    $train $lang $exp/tri2 $exp/tri2_ali || exit 1
  steps/train_deltas.sh \
    --boost-silence $boost_sil --cmd "$train_cmd" \
    $numLeavesTri3 $numGaussTri3 $train $lang $exp/tri2_ali $exp/tri3 || exit 1
  touch $exp/tri3/.done
fi

echo ---------------------------------------------------------------------
echo "Starting (lda_mllt) triphone training in exp/tri4 on" `date`
echo ---------------------------------------------------------------------
if [ ! -f $exp/tri4/.done ]; then
  steps/align_si.sh \
    --boost-silence $boost_sil --nj $train_nj --cmd "$train_cmd" \
    $train $lang $exp/tri3 $exp/tri3_ali || exit 1
  steps/train_lda_mllt.sh \
    --boost-silence $boost_sil --cmd "$train_cmd" \
    $numLeavesMLLT $numGaussMLLT $train $lang $exp/tri3_ali $exp/tri4 || exit 1
  touch $exp/tri4/.done
fi

echo ---------------------------------------------------------------------
echo "Starting (SAT) triphone training in exp/tri5 on" `date`
echo ---------------------------------------------------------------------

if [ ! -f $exp/tri5/.done ]; then
  steps/align_si.sh \
    --boost-silence $boost_sil --nj $train_nj --cmd "$train_cmd" \
    $train $lang $exp/tri4 $exp/tri4_ali || exit 1
  steps/train_sat.sh \
    --boost-silence $boost_sil --cmd "$train_cmd" \
    $numLeavesSAT $numGaussSAT $train $lang $exp/tri4_ali $exp/tri5 || exit 1
  touch $exp/tri5/.done
fi


################################################################################
# Ready to start SGMM training
################################################################################

if [ ! -f $exp/tri5_ali/.done ]; then
  echo ---------------------------------------------------------------------
  echo "Starting exp/tri5_ali on" `date`
  echo ---------------------------------------------------------------------
  steps/align_fmllr.sh \
    --boost-silence $boost_sil --nj $train_nj --cmd "$train_cmd" \
    $train $lang $exp/tri5 $exp/tri5_ali || exit 1
  touch $exp/tri5_ali/.done
fi

if $tri5_only ; then
  echo "Exiting after stage TRI5, as requested. "
  echo "Everything went fine. Done"
  exit 0;
fi

if [ ! -f $exp/ubm5/.done ]; then
  echo ---------------------------------------------------------------------
  echo "Starting exp/ubm5 on" `date`
  echo ---------------------------------------------------------------------
  steps/train_ubm.sh \
    --cmd "$train_cmd" $numGaussUBM \
    $train $lang $exp/tri5_ali $exp/ubm5  || exit 1
  touch $exp/ubm5/.done
fi

if [ ! -f $exp/sgmm5/.done ]; then
  echo ---------------------------------------------------------------------
  echo "Starting exp/sgmm5 on" `date`
  echo ---------------------------------------------------------------------
  steps/train_sgmm2.sh \
    --cmd "$train_cmd" $numLeavesSGMM $numGaussSGMM \
    $train $lang $exp/tri5_ali $exp/ubm5/final.ubm $exp/sgmm5  || exit 1
  #steps/train_sgmm2_group.sh \
  #  --cmd "$train_cmd" "${sgmm_group_extra_opts[@]-}" $numLeavesSGMM $numGaussSGMM \
  #  data/train data/lang exp/tri5_ali exp/ubm5/final.ubm exp/sgmm5
  touch $exp/sgmm5/.done
fi

################################################################################
# Ready to start discriminative SGMM training
################################################################################

if [ ! -f $exp/sgmm5_ali/.done ]; then
  echo ---------------------------------------------------------------------
  echo "Starting exp/sgmm5_ali on" `date`
  echo ---------------------------------------------------------------------
  steps/align_sgmm2.sh \
    --nj $train_nj --cmd "$train_cmd" --transform-dir $exp/tri5_ali \
    --use-graphs true --use-gselect true \
    $train $lang $exp/sgmm5 $exp/sgmm5_ali  || exit 1
  touch $exp/sgmm5_ali/.done
fi


if [ ! -f $exp/sgmm5_denlats/.done ]; then
  echo ---------------------------------------------------------------------
  echo "Starting exp/sgmm5_denlats on" `date`
  echo ---------------------------------------------------------------------
  steps/make_denlats_sgmm2.sh \
    --nj $train_nj "${sgmm_denlats_extra_opts[@]}" \
    --beam 10.0 --lattice-beam 6 --cmd "$decode_cmd" --transform-dir $exp/tri5_ali \
    $train $lang $exp/sgmm5_ali $exp/sgmm5_denlats || exit 1
  touch $exp/sgmm5_denlats/.done
fi

if [ ! -f $exp/sgmm5_mmi_b0.1/.done ]; then
  echo ---------------------------------------------------------------------
  echo "Starting exp/sgmm5_mmi_b0.1 on" `date`
  echo ---------------------------------------------------------------------
  steps/train_mmi_sgmm2.sh \
    --cmd "$train_cmd" \
    --drop-frames true --transform-dir $exp/tri5_ali --boost 0.1 \
    $train $lang $exp/sgmm5_ali $exp/sgmm5_denlats \
    $exp/sgmm5_mmi_b0.1  || exit 1
  touch $exp/sgmm5_mmi_b0.1/.done
fi

echo ---------------------------------------------------------------------
echo "Finished successfully on" `date`
echo ---------------------------------------------------------------------
