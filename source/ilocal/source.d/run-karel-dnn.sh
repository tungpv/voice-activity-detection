#!/bin/bash

set -e
set -u 
# begin user option

# end user option
train_ndev=$fmllrdata/train_ndev
train_dali=$gmmdir/ali/train_ndev;
if  [ ! -f $fmllrdata/train_ndev/.done ]; then
  echo -e "\n## $0: make $fmllrdata/train_ndev data `date`\n";
  if [ ! -f $train/segments ]; then 
    echo -e "\n ## ERROR $0:segments file is not in $train/nn/ntrain \n"; exit 1
  fi
  ndev=$train/train_nn/train_ndev; feat=$expdir/feat/train_ndev;
  isteps/um/mksubset_dir.sh --fullspk true --hlen $ndev_hlen  $train $ndev || exit 1
  rm $ndev/cmvn.scp 
  steps/compute_cmvn_stats.sh $ndev $feat/_log $feat/_cmvn || exit 1
  nj=`wc -l < $ndev/spk2utt`; if [ $nj -gt 60 ]; then nj=60; fi
   
  steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" $ndev $lang $gmmdir $gmmdir/ali/train_ndev || exit 1

  # Save the fMLLR features
  steps/make_fmllr_feats.sh --nj $nj --cmd "$train_cmd" --transform-dir $gmmdir/ali/train_ndev  \
  $fmllrdata/train_ndev $ndev $gmmdir $fmllrfeat/train_ndev/_log $fmllrfeat/train_ndev/_data || exit 1

  touch $fmllrdata/train_ndev/.done
  echo -e "\n## $0: making $fmllrdata/train_ndev done `date`\n";
fi

train_ntrain=$fmllrdata/train_ntrain;
train_tali=$gmmdir/ali/train_ntrain;
if [ ! -f $fmllrdata/train_ntrain/.done ]; then
  echo -e "\n## $0: making train_ntrain data `date`\n"
  ntrain=$train/train_nn/train_ntrain; feat=$expdir/feat/train_ntrain;
  isteps/um/mksubset_dir.sh --exclude $train/train_nn/train_ndev/segments  \
  $train $ntrain || exit 1
  rm $ntrain/cmvn.scp
  steps/compute_cmvn_stats.sh $ntrain $feat/_log $feat/_cmvn || exit 1
  nj=`wc -l < $ntrain/spk2utt`; if [ $nj -gt 60 ]; then nj=60; fi
   
  steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" $ntrain $lang $gmmdir $gmmdir/ali/train_ntrain || exit 1
 
  # Save the fMLLR features
  steps/make_fmllr_feats.sh --nj $nj --cmd "$train_cmd" --transform-dir $gmmdir/ali/train_ntrain  \
  $fmllrdata/train_ntrain $ntrain $gmmdir $fmllrfeat/train_ntrain/_log $fmllrfeat/train_ntrain/_data || exit 1

  touch $fmllrdata/train_ntrain/.done
  echo -e "\n## $0: done `date`\n"
fi
fmllrdev=$fmllrdata/$devname
if [ ! -f $fmllrdev/.done ]; then
  nj=`cat $dev_transform_dir/num_jobs`
  if [ -z $nj ]; then echo -e "\n## ERROR $0: num_jobs is not in $dev_transform_dir\n"; fi
  echo -e "\n## $0: making $fmllrdev data `date`\n"
  steps/make_fmllr_feats.sh  --cmd "$train_cmd" --nj $nj \
  --transform-dir $dev_transform_dir  \
  $fmllrdev $dev $gmmdir $fmllrdev/_log $fmllrdev/_data || exit 1

  touch $fmllrdev/.done
  echo -e "\n## $0: making $fmllrdev data done `date`\n" 
fi

pretrain_dir=$expdir/pretrain_dbn
if [ ! -f $pretrain_dir/._pretrain_done ]; then
  echo -e "\n## $0: pretrain started `date`\n"
  $cmd $pretrain_dir/pretrain_dbn.log \
  isteps/pretrain_dbn.sh --temp-prefix $expdir \
  --nn-depth $nn_depth \
  --hid-dim $hidden_dim \
  --rbm-iter 1 \
  $train_ntrain  $pretrain_dir || exit 1;
  touch $pretrain_dir/._pretrain_done
  echo -e "\n## $0: pretraining done `date`\n"
fi


dnn_dir=$expdir/dnn_xent
feature_transform=$pretrain_dir/final.feature_transform;
dbn=$pretrain_dir/$nn_depth.dbn;
if [ ! -f $dnn_dir/.dnn_done ]; then
  echo -e  "\n## $0: dnn training started `date`\n";
  $cmd $dnn_dir/_train_nnet.log \
  isteps/tandem/train_nnet.sh \
  --temp-prefix $expdir \
  --clean-temp $clean_temp \
  --feature-transform $feature_transform \
  --dbn $dbn \
  --hid-layers 0  \
  --learn-rate $learn_rate \
  --bunch-size $bunch_size \
  --cache-size $cache_size \
  $train_ntrain $train_ndev $lang $train_tali $train_dali $dnn_dir || exit 1; 
  touch $dnn_dir/.dnn_done
  echo -e "\n## $0: dnn training ended `date`\n"
fi

if [ ! -f $dnn_dir/decode_${devname}/.decode_done ]; then
  nj=`wc -l < $fmllrdata/$devname/spk2utt`; if [ $nj -gt 30 ]; then nj=30; fi
  echo -e "\n## $0: decoding started `date`\n"
  isteps/decode_nnet.sh --nj $nj \
  --cmd "$decode_cmd" \
  --max-mem $max_mem \
  --latbeam 6.0 \
  --acwt $acwt \
  $graphdir  $fmllrdata/$devname $dnn_dir/decode_${devname} || exit 1;
  touch $dnn_dir/decode_${devname}/.decode_done
  echo -e "\n## $0: decoding ended `date`\n"
fi


if $smbr; then
  echo -e "\n## $0: for smbr training `date`\n"
else
  echo -e "\n## $0: cross-entropy dnn training ended `date`\n";
  exit 0
fi

train_smbr=$fmllrdata/train;
if  [ ! -f $fmllrdata/train/.done ]; then
  echo -e "\n## $0: make $fmllrdata/train data `date`\n";
  nj=`wc -l < $train/spk2utt`; if [ $nj -gt 60 ]; then nj=60; fi
  steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" $train $lang $gmmdir $gmmdir/ali/train || exit 1

  # Save the fMLLR features
  steps/make_fmllr_feats.sh --nj $nj --cmd "$train_cmd" --transform-dir $gmmdir/ali/train  \
  $fmllrdata/train $train $gmmdir $fmllrfeat/train/_log $fmllrfeat/train/_data || exit 1

  touch $fmllrdata/train/.done
  echo -e "\n## $0: making $fmllrdata/train done `date`\n";
fi

alidir=$dnn_dir/ali/train_smbr;
if [ ! -f $alidir/.done ]; then
  echo -e "\n## $0: making alignment for data $fmllrdata/train `date`\n"
  nj=`wc -l < $train/spk2utt`; if [ $nj -gt 60 ]; then nj=60; fi
  steps/nnet/align.sh --nj $nj  --cmd "$train_cmd" \
  $train_smbr  $lang $dnn_dir  $alidir || exit 1 ;
  touch $alidir/.done 
  echo -e "\n## $0: done `date`\n"
fi

latdir=$dnn_dir/denlats;
if [ ! -f $latdir/.denlat_done ]; then
  echo -e "\n## $0: making denlat started `date`\n"
  nj=`cat $dnn_dir/ali/train_smbr/num_jobs`; 
  if [ -z $nj ]; then echo -e "\n## $0: num_jobs expected in denlat generation\n"; fi
  isteps/make_denlats_nnet.sh --nj $nj  --cmd "$train_cmd" --lattice-beam 6.0 --max-mem $max_mem \
  --acwt $acwt $train_smbr $lang $dnn_dir $latdir  || exit 1 ;
  touch $latdir/.denlat_done
  echo -e "\n## $0: done `date`\n"
fi

dnn_smbr=$expdir/dnn_smbr;
if [ ! -f $dnn_smbr/.train_done ]; then
  echo -e "\n## $0: sMBR training started `date`\n"
  isteps/train_nnet_mpe.sh --cmd "$train_cmd" --num-iters 4 --acwt $acwt \
  --do-smbr true \
  $train_smbr $lang $dnn_dir \
  $alidir  $latdir  $dnn_smbr || exit 1;
  touch $dnn_smbr/.train_done
  echo -e "\n## $0: done `date`\n"
fi

# decode
echo -e "\n## $0: decoding started `date`\n"
nj=`wc -l < $fmllrdev/spk2utt`; if [ $nj -gt 30 ]; then nj=30; fi
for iter in 1 2 3 4; do
  dir=$dnn_smbr/decode_${devname}_$iter;
  if [ ! -f $dir/.done ]; then
    isteps/decode_nnet.sh --nj $nj  --cmd  "$train_cmd" \
    --max-mem $max_mem --latbeam 6.0 --acwt $acwt \
    --nnet $dnn_smbr/${iter}.nnet $graphdir \
    $fmllrdev $dir || exit 1
    touch $dir/.done
  fi
done
echo -e "\n## $0: decoding ended `date`\n"

echo -e "\n## $0: All is done `date`\n";
