#!/bin/bash

set -o  pipefail

[ -d $dir ] || mkdir -p $dir 
sdata1=$data1/split$nj;



echo -e "\n## assemble lda feature for $data1\n";
tfeats1="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$data1/utt2spk scp:$data1/cmvn.scp scp:$data1/feats.scp ark:- |splice-feats ark:- ark:- | transform-feats $alidir1/final.mat ark:- ark:- |"
feats1="ark,s,cs:utils/filter_scp.pl --exclude $dir/valid_uttlist $sdata1/JOB/feats.scp |apply-cmvn $cmvn_opts --utt2spk=ark:$sdata1/JOB/utt2spk scp:$sdata1/JOB/cmvn.scp scp:$sdata1/JOB/feats.scp ark:- | splice-feats ark:- ark:- | transform-feats $alidir1/final.mat ark:- ark:- |"
valid_feats1="ark,s,cs:utils/filter_scp.pl $dir/valid_uttlist $data1/feats.scp | apply-cmvn $cmvn_opts --utt2spk=ark:$data1/utt2spk scp:$data1/cmvn.scp scp:- ark:- |splice-feats ark:- ark:- | transform-feats $alidir1/final.mat ark:- ark:- |"
train_subset_feats1="ark,s,cs:utils/filter_scp.pl $dir/train_subset_uttlist $data1/feats.scp | apply-cmvn $cmvn_opts --utt2spk=ark:$data1/utt2spk scp:$data1/cmvn.scp scp:- ark:- |splice-feats ark:- ark:- | transform-feats $alidir1/final.mat ark:- ark:- |"

if  [ ! -z $transform_dir ]; then
  echo -e "\n## assemble fmllr feature for $data1\n"
  tfeats1="$tfeats1 transform-feats --utt2spk=ark:$data1/utt2spk \\\"ark:cat $transform_dir/trans.*|\\\" ark:- ark:- |"
  feats1="$feats1 transform-feats --utt2spk=ark:$sdata1/JOB/utt2spk ark,s,cs:$alidir1/trans.JOB ark:- ark:- |"
  valid_feats1="$valid_feats1 transform-feats --utt2spk=ark:$data1/utt2spk \\\"ark:cat $transform_dir/trans.*|\\\" ark:- ark:- |"
  train_subset_feats1="$train_subset_feats1 transform-feats --utt2spk=ark:$data1/utt2spk \\\"ark:cat $transform_dir/trans.*|\\\" ark:- ark:- |"
fi

feats1="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata1/JOB/utt2spk scp:$sdata1/JOB/cmvn.scp scp:$sdata1/JOB/feats.scp ark:- | splice-feats ark:- ark:- | transform-feats $alidir1/final.mat ark:- ark:- |"
if  [ ! -z $transform_dir ]; then
  echo -e "\n## assemble fmllr feature for $data1\n"
  feats1="$feats1 transform-feats --utt2spk=ark:$sdata1/JOB/utt2spk ark,s,cs:$alidir1/trans.JOB ark:- ark:- |"
fi




echo -e "\n## assemble feature for $data2\n"
sdata2=$data2/split$nj; [ -d $sdata2 ] || split_data.sh $data2 $nj;
tfeats2="ark,s,cs:apply-cmvn --norm-vars=$norm_vars --utt2spk=ark:$data2/utt2spk scp:$data2/cmvn.scp scp:$data2/feats.scp  ark:- |";
feats2="ark,s,cs:utils/filter_scp.pl --exclude $dir/valid_uttlist $sdata2/JOB/feats.scp | apply-cmvn $cmvn_opts --utt2spk=ark:$sdata2/JOB/utt2spk scp:$sdata2/JOB/cmvn.scp scp:- ark:- |"
valid_feats2="ark,s,cs:utils/filter_scp.pl $dir/valid_uttlist $data2/feats.scp | ark,s,cs:apply-cmvn --norm-vars=$norm_vars --utt2spk=ark:$data2/utt2spk scp:$data2/cmvn.scp scp:-  ark:- |"
train_subset_feats2="ark,s,cs:utils/filter_scp.pl $dir/train_subset_uttlist $data2/feats.scp | apply-cmvn $cmvn_opts --utt2spk=ark:$data2/utt2spk scp:$data2/cmvn.scp scp:- ark:- |"

alidir=$alidir2

tfeats="ark,s,cs:paste-feats '$tfeats1' '$tfeats2' ark:- |"
feats="ark,s,cs:paste-feats '$feats1' '$feats2' ark:- |"
valid_feats="ark,s,cs:paste-feats '$valid_feats1' '$valid_feats2' ark:- |"
train_subset_feats="ark,s,cs:paste-feats '$train_subset_feats1' '$train_subset_feats2' ark:- |"

feats2="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata2/JOB/utt2spk scp:$sdata2/JOB/cmvn.scp scp:$sdata2/JOB/feats.scp ark:- |"

feats="ark,s,cs:paste-feats '$feats1' '$feats2' ark:- |"
echo  "$feats" > $dir/tandem_feats

if [ ! -f $data/.tandem_done ]; then
  [ -d $data ] || mkdir -p $data; name=`basename $data`
  cp $data2/* $data; rm $data/{feats.scp,cmvn.scp}
  logdir=$feature/_log; featdir=$feature/_data;
  [ -d $featdir ] || mkdir -p $featdir

  $cmd JOB=1:$nj $logdir/make_fmllr_feats.JOB.log \
  copy-feats "$feats" \
  ark,scp:$featdir/feats_tandem_$name.JOB.ark,$featdir/feats_tandem_$name.JOB.scp || exit 1;

  for n in $(seq 1 $nj); do
    cat $featdir/feats_tandem_$name.$n.scp 
  done > $data/feats.scp
  steps/compute_cmvn_stats.sh $data  $logdir  $featdir || exit 1
  utils/fix_data_dir.sh $data || exit 1;
  touch $data/.tandem_done
fi

# Get list of validation utterances. 
if [ ! -f $dir/valid_uttlist ]; then
  awk '{print $1}' $data/utt2spk | utils/shuffle_list.pl | head -$num_utts_subset \
    > $dir/valid_uttlist
fi

if [ ! -f $dir/train_subset_uttlist ]; then
  awk '{print $1}' $data/utt2spk | utils/filter_scp.pl --exclude $dir/valid_uttlist | \
     head -$num_utts_subset > $dir/train_subset_uttlist
fi
nj=$(cat $alidir/num_jobs);  sdata=$data/split$nj; [ -d $sdata ] || utils/split_data.sh $data $nj

all_feats="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- |"
feats="ark,s,cs:utils/filter_scp.pl --exclude $dir/valid_uttlist $sdata/JOB/feats.scp | apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:- ark:- |"
valid_feats="ark,s,cs:utils/filter_scp.pl $dir/valid_uttlist $data/feats.scp | apply-cmvn $cmvn_opts --utt2spk=ark:$data/utt2spk scp:$data/cmvn.scp scp:- ark:- |"
train_subset_feats="ark,s,cs:utils/filter_scp.pl $dir/train_subset_uttlist $data/feats.scp | apply-cmvn $cmvn_opts --utt2spk=ark:$data/utt2spk scp:$data/cmvn.scp scp:- ark:- |"

echo "$all_feats" > $dir/all_feats
echo "$feats" > $dir/feats
echo "$valid_feats" > $dir/valid_feats
echo "$train_subset_feats" > $dir/train_subset_feats

if [ ! -f $dir/num_frames ]; then
  num_frames=`feat-to-len scp:$data/feats.scp  ark,t:- | awk '{x += $2;} END{print x;}'` || exit 1;
  echo $num_frames > $dir/num_frames
fi

if [ ! -f $dir/lda.mat ]; then
  isteps/nnet2x/get_lda.sh --feat-type "user"  --cmd "$cmd" $data $lang $alidir $dir || exit 1;
fi

isteps/nnet2x/get_egs.sh  --samples-per-iter $samples_per_iter --num-jobs-nnet $num_jobs_nnet \
--cmd "$cmd" --feat-type "user"   $data $lang $alidir $dir || exit 1;

