#!/bin/bash

set -e
[ ! -z $beam ] || beam=13.0

echo -e "\n## making fmllr data `date`\n";
if [ ! -f $fmllrdata/.done ]; then
  nj=`cat $transform_dir/num_jobs`
  if [ -z $nj ]; then echo -e "\n## ERROR $0: num_jobs is not in $transform_dir\n"; fi
  echo -e "\n## $0: making $fmllrdata  `date`\n"
  steps/make_fmllr_feats.sh  --cmd "$decode_cmd" --nj $nj \
  --transform-dir $transform_dir  $fmllrdata $data $gmmdir $fmllrfeat/_log $fmllrfeat/_data || exit 1

  touch $fmllrdata/.done
  echo -e "\n## $0: making $fmllrdata data done `date`\n" 
fi

echo -e "\n## decoding started `date`\n";
if [ ! -f $dir/.done ]; then
  nj=`wc -l < $fmllrdata/spk2utt`; [ $nj -le 30 ] || nj=30;
  isteps/decode_nnet.sh --nj $nj  --cmd  "$decode_cmd" \
  --max-mem $max_mem --beam $beam --latbeam $latbeam  --acwt $acwt \
  $graphdir  $fmllrdata $dir || exit 1
  touch $dir/.done
fi
echo -e "\n## decoding ended `date`\n"

