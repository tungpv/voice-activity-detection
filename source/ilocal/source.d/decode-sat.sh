#!/bin/bash

set -u           #Fail on an undefined variable

if [ ! -f $dir/.done ]; then
  if [ ! -f $graph/HCLG.fst ]; then
    echo  "-----------------------------------"
    echo "making graph `date`";
    echo  "-----------------------------------"
    utils/mkgraph.sh  $lang $sdir $graph || exit 1
    echo "making graph ended `date`";
  fi
  echo -e "\n## decode_fmllr_extra started `date`\n"
  steps/decode_fmllr_extra.sh --skip-scoring $skip_scoring \
  --beam $beam  --lattice-beam $latbeam  \
  --nj $nj --cmd "$decode_cmd"  $graph $dev  $dir  || exit 1
  echo -e "\n## ended `date`\n"
  touch $dir/.done
fi

