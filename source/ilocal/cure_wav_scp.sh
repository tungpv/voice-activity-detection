#!/bin/bash


wavscp=$1;

dir=`dirname $wavscp`
(cd $dir; [  -f ./.wav.scp ] || cp ./wav.scp ./.wav.scp; )
if [ ! -f $dir/.wav.scp ]; then
  echo -e "\n## ERROR: no $dir/.wav.scp generated\n"
  exit 1
fi
scp=$dir/.wav.scp
cat $scp | \
perl -ne '
  chomp;
  if(/down/) {
    @A = split / /;
    print $A[0], " $A[2]\n";
  } else {
    print $_, "\n";
  }
' > $wavscp

echo -e "\ncheck $wavscp\n"
