#!/usr/bin/env perl
use strict;
use warnings;

my $numArgs =scalar @ARGV;
print STDERR   $0, " ", join " ", @ARGV, "\n"; 
if ($numArgs != 4) {
  die "\n\nUsage: $0 <minLen> <subLen> <factor> <query_length.txt>\n\n";
}

my ($minLen, $subLen, $factor, $qlenFname) = @ARGV;

open(F, "$qlenFname") or die "file $qlenFname cannot open\n";
my %vocab = ();
my %countv = ();
while (<F>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  my ($kwid, $len) = ($1, $2);
  if ($len >= $minLen) {
    $countv{$kwid} ++;
    $vocab{$kwid} += $len;
  }
}
close F;
my %thVocab = ();
foreach my $kwid ( keys %vocab) {
  my $n = $countv{$kwid};
  my $totLen = $vocab{$kwid};
  my $avgLen = $totLen / $n;
  $thVocab{$kwid} = $factor**$avgLen;
}
%vocab = ();
%countv = ();
my %countv1 = ();
open (F, "$qlenFname") or die "file $qlenFname cannot open 2\n";
while (<F>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  my ($kwid, $len) = ($1, $2);
  if ($len >= $minLen) {
    $countv1{$kwid} ++;
    if ($countv1{$kwid} <= $thVocab{$kwid}  ) {
      $countv{$kwid} ++;
      $vocab{$kwid} += $len;
      if ($len > $subLen) { 
        my $n = $len - $subLen + 1;
        $countv{$kwid} += $n;
        $vocab{$kwid} += $subLen*$n;
      }
    }
  }
}
close F;
foreach my $kwid (keys %vocab) {
  my $n = $countv{$kwid};
  my $totLen = $vocab{$kwid};
  my $avgLen = $totLen / $n;
  print "$kwid\t$avgLen\n";
}
