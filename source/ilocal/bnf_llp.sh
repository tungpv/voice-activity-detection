#!/bin/bash 

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "cuda_path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl;
nj=10;
step=0
substep=0
data_prepare=false
bnf_tanh=false
bnf_dnn=false
tandem_llp_pitch=false
mbr_sys_comb=false
do_sst=false
utt_conf_sst=false
do_dnn_sst=false
llp_smbr_train=false
semi_sec_iter=false
decode_unsupervised=false
lang=data/llp/lang; lang_test=${lang}_test
samples_per_iter=4000000;  
max_mem=500000000;
state=2500;gauss=25000
nonspeech_words=$lang/nonspeech_words.txt

# end tunable variable
bn_am_cf=conf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";

datastr="train dev10h" ;
if $data_prepare; then
  if [ $step -le 1 ]; then
    for x in $datastr; do
      echo -e "\n## plp feature extraction for data $x `date`\n"
      sdata=data/llp/$x; data=data/llp/feature/plp/$x; featdir=exp/llp/feature/plp/$x;
      [  ! -d $sdata ] && echo -e "##ERROR src data $sdata is not ready" && exit 1;
      [ -d $data ] || mkdir -p $data;
      cp $sdata/* $data; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
      steps/make_plp.sh --cmd "$cmd" --nj $nj --plp-config conf/plp.conf \
      $data  $featdir/_log $featdir/_data ||exit 1;
      steps/compute_cmvn_stats.sh $data $featdir/_log $featdir/_data ||exit 1;
    done 
    echo -e "\n## changing path succeeded `date`\n" && exit 0
  fi 
  if [ $step -le 3 ]; then
    for x in $datastr; do
      echo -e "\n## fbank feature extraction for data $x `date`\n"
      sdata=data/llp/$x; data=data/llp/feature/fbank/$x; featdir=exp/llp/feature/fbank/$x;
      [ -d $data ] || mkdir -p $data;
      cp $sdata/* $data; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
      steps/make_fbank.sh   --cmd "$cmd" --nj $nj --fbank-config conf/fbank.conf \
      $data  $featdir/_log $featdir/_data ||exit 1;
      steps/compute_cmvn_stats.sh $data $featdir/_log $featdir/_data || exit 1
    done
    echo -e "\ndone with fbank feature `date`\n" &&exit 0
  fi
  if [ $step -le 5 ]; then
    for x in $datastr; do
      echo -e "\n## kaldi pitch feature extraction for data $x `date` \n"
      sdata=data/llp/$x; data=data/llp/feature/pitch/$x; featdir=exp/llp/feature/pitch/$x;
      [ -d $data ] || mkdir -p $data;
      cp $sdata/* $data; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
      steps/make_pitch_kaldi.sh --cmd "$cmd" --nj $nj --pitch-config conf/plp.conf \
      $data $featdir/_log $featdir/_data || exit 1
    done
  fi
  if [ $step -le 7 ]; then
    for x in $datastr; do
      echo -e "\n## making plp+pitch for data $x `date`\n"
      plp=data/llp/feature/plp/$x; pitch=data/llp/feature/pitch/$x;
      data=data/llp/feature/plp_pitch/$x;
      featdir=exp/llp/feature/plp_pitch/$x;
      steps/append_feats.sh $plp $pitch  $data $featdir/_log $featdir/_data || exit 1
      steps/compute_cmvn_stats.sh $data $featdir/_log $featdir/_data ||exit 1;
    done
    echo -e "\n## making plp+pitch feature done `date`\n"
  fi
  if [ $step -le 9 ]; then
    for x in $datastr; do
      echo -e "\n## making fbank+pitch for dat $x `date`\n"
      fbank=data/llp/feature/fbank/$x; pitch=data/llp/feature/pitch/$x;
      data=data/llp/feature/fbk_pitch/$x;
      featdir=exp/llp/feature/fbk_pitch/$x;
      steps/append_feats.sh $fbank $pitch  $data $featdir/_log $featdir/_data || exit 1
      steps/compute_cmvn_stats.sh $data $featdir/_log $featdir/_data ||exit 1;
    done
    echo -e "\n## making fbank+pitch feature done `date`\n"
  fi
  echo -e "\ndata preparation done `date`\n" ; exit 0
fi
# 

if [ $step -le 1 ]; then
  echo -e "\n$0: prepare data to train BN NN (target) `hostname``date`\n"
  
  sdir=data/llp/feature/plp_pitch/train;  hlen=1.0; datadir=$sdir/../sbn;
  dir=$datadir/nvali; featdir=exp/llp/feature/plp_pitch; feat=$featdir/sbn/nvali
  
  isteps/um/mksubset_dir.sh --hlen $hlen  $sdir $dir || exit 1
  rm $dir/cmvn.scp;
  steps/compute_cmvn_stats.sh $dir  $feat/_log $feat/_cmvn || exit 1
  seg_nvali=$dir/segments; dir=$datadir/ntrain; feat=$featdir/sbn/ntrain;
  isteps/um/mksubset_dir.sh --exclude $seg_nvali $sdir $dir || exit 1
  rm $dir/cmvn.scp;
  steps/compute_cmvn_stats.sh $dir  $feat/_log $feat/_cmvn || exit 1
  echo -e "\n$0: done `hostname` `date`\n"
fi

if [ $step -le 3 ]; then
  echo -e "\n$0: prepare training BN NN data (input) `hostname` `date`\n"
  seg_nvali=data/llp/feature/plp_pitch/sbn/nvali/segments
  sdir=data/llp/feature/fbk_pitch/train; datadir=$sdir/../sbn;
  dir=$datadir/nvali; featdir=exp/llp/feature/fbk_pitch; feat=$featdir/sbn/nvali;
  
  isteps/um/mksubset_dir.sh --include $seg_nvali  $sdir $dir || exit 1
  rm $dir/cmvn.scp;
  steps/compute_cmvn_stats.sh $dir  $feat/_log $feat/_cmvn || exit 1
  dir=$datadir/ntrain; feat=$featdir/sbn/ntrain;
  isteps/um/mksubset_dir.sh --exclude $seg_nvali $sdir $dir || exit 1
  rm $dir/cmvn.scp;
  steps/compute_cmvn_stats.sh $dir  $feat/_log $feat/_cmvn || exit 1
  echo -e "\n$0: done `hostname` `date`\n"
fi

if [ $step -le 5 ]; then
  echo -e "\n## $0: making alignmen started `hostname` `date`\n"
  step=$substep; step=$[step+500]; datap=data/llp/feature/plp_pitch/sbn
  alip=exp/llp/plp_pitch/tri3sat_2500/llp; nj=10;
  if [ $step -le 501 ]; then
    for x in ntrain nvali; do
      data=$datap/$x; ali=$alip/$x; sdir=$alip/..;
      isteps/align_fmllr.sh --nj $nj  $data  $lang $sdir $ali || exit 1
    done
  fi
  transform_dir=$alip
  if [ $step -le 503 ]; then
    alip=exp/llp/plp_pitch/pnorm_learn0.001/llp; sdir=$alip/..; 
    for x in ntrain nvali; do
      data=$datap/$x; ali=$alip/$x; 
      steps/nnet2/align.sh --nj $nj --transform-dir $transform_dir/$x $data $lang \
      $sdir $ali || exit 1
    done
  fi
  echo -e "\n## $0: done `hostname` `date`\n"
fi
if [ $step -le 6 ]; then
  echo -e "\n## $0: alignment conversion `hostname` `date`\n"
  mdl=exp/llp/plp_pitch/mono0a; smdl=exp/llp/plp_pitch/pnorm_learn0.001;
  for x in ntrain nvali; do
    sali=$smdl/llp/$x; ali=$mdl/llp/$x;
    isteps/convert_ali.sh  $smdl $mdl $sali $ali || exit 1
  done
  echo -e "\n## $0: done `hostname` `date`\n"
fi
if [ $step -le 7 ]; then
  echo -e "\n## $0: training sbn started `hostname` `date`\n"
  dp1=data/llp/feature/fbk_pitch/sbn; ap1=exp/llp/plp_pitch/mono0a/llp;
  train=$dp1/ntrain; vali=$dp1/nvali;
  tali=$ap1/ntrain; cali=$ap1/nvali;  ep1=exp/llp/plp_pitch/bn/llp
  isteps/train_sbn.sh --clean-temp true \
  $train $vali $tali $cali $lang $ep1/s1 || exit 1
  echo -e "\n## $0: ended `hostname` `date`\n" ; exit 0
fi

dp2=data/llp/feature/bnllp;
if [ $step -le 8 ]; then
  echo -e "\n## $0: making bnf feature `hostname` `date`\n"
  dp1=data/llp/feature/fbk_pitch; 
  final_bn=exp/llp/plp_pitch/bn/llp/s1/bn2
  dp3=exp/llp/feature/bnllp;
  for x in train dev10h; do
    echo -e "..bnf for $x"
    sdata=$dp1/$x; data=$dp2/$x; feadir=$dp3/$x;
    isteps/imake_bn_feats.sh --nj 10 $data $sdata $final_bn $feadir/_log $feadir/_data || exit 1
  done
  echo -e "\n## $0: done `hostname` `date`\n"
fi
sdir=exp/llp/plp_pitch/bn/llp/s1; sdir_booked=$sdir;
if [ $step -le 9  ]; then
  echo -e "\n## $0: make bnf gmm-hmm system and evaluate `hostname` `date`\n"
  train=$dp2/train; dev=$dp2/dev10h;
  isteps/train_am_simple.sh --max-stage 6 --over-train $train --over-dev $dev \
  --over-lang $lang --over-lang-test $lang_test \
  --over-expdir $sdir $bn_am_cf || exit 1 
  echo -e "\n## $0: done `hostname` `date`\n"
fi
pnorm_input_dim=3000; pnorm_output_dim=500; learn1=0.0006; learn2=0.00006; layers=3;
ali=$sdir/tri3sat_2500/ali_train; train=$dp2/train; sdir=$sdir/pnorm_learn${learn1}_${learn2}_${layers};
ali_booked=$ali; 
if [ $step -le 10 ]; then
  echo -e "\n## $0: bnf pnorm dnn `hostname` `date`\n"
  echo -e "\npnorm_input_dim=$pnorm_input_dim, pnorm_output_dim=$pnorm_output_dim, learn1=$learn1, learn2=$learn2, layers=$layers\n"
  isteps/nnet2/train_pnorm.sh --initial-learning-rate $learn1 --final-learning-rate $learn2 \
  --start-x 0 --stage -100 --num-threads 8 --num-jobs-nnet 4  --num-hidden-layers $layers  \
  --samples-per-iter $samples_per_iter --pnorm-input-dim $pnorm_input_dim \
  --pnorm-output-dim $pnorm_output_dim --cmd run.pl --parallel-opts " " \
  --io-opts " " $train $lang $ali $sdir || exit 1
  echo -e "\n## $0: done `hostname` `date`\n"
fi 
data=$train; transform_dir=$ali; nj=`cat $transform_dir/num_jobs`;  ali=$sdir/ali_train; 
if [ $step -le 11 ]; then
  echo -e "\n##$0: making alignment `hostname` `date`\n"
  steps/nnet2/align.sh --nj $nj --transform-dir $transform_dir $data $lang $sdir $ali || exit 1
  echo -e "\n##$0: done `hostname` `date`\n"
fi
transform_dir=$sdir/../tri3sat_2500/decode_dev10h; graph=$transform_dir/../graph;
dir=$sdir/decode_dev10h; dev=$dp2/dev10h;
if [ $step -le 12 ]; then
  echo -e "\n## $0: pnorm decoding `hostname` `date`\n"
  isteps/nnet2/decode.sh --nj 30  --transform-dir $transform_dir --beam 10 --lat-beam 6.0 \
  --max-mem $max_mem $graph $dev  $dir || exit 1
  echo -e "\n## $0: done `hostname` `date`\n" ; exit 0
fi
 learn1=0.001; learn2=0.0001; sdir=$sdir_booked/tanh0a_cpu${learn1}; 
if [ $step -le 13 ]; then
  echo -e "\n## $0: bnf+tanh-dnn `hostname` `date`\n"
  steps/nnet2/train_tanh.sh --initial-learning-rate $learn1 --final-learning-rate $learn2 \
  --num-jobs-nnet 4 --num-threads 8 --samples-per-iter $samples_per_iter --num-hidden-layers 7 --hidden-layer-dim 1000  \
  --cmd run.pl --parallel-opts " " --io-opts " "  $train $lang $ali_booked $sdir || exit 1 
  echo -e "\n## $0 "done"\n"
fi

if [ $step -le 14 ]; then
  echo -e "\n## $0: evaluate tanh-dnn `hostname` `date`\n"
  for x in tanh0a_cpu tanh0a_cpu$learn1; do
    sdir=$sdir_booked/$x; dir=$sdir/decode_dev10h;
    isteps/nnet2/decode.sh --nj 30  --transform-dir $transform_dir --beam 10 --lat-beam 6.0 \
    --max-mem $max_mem $graph $dev  $dir || exit 1
  done
  echo -e "\n## done `date`\n"
fi
