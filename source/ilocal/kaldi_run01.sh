#!/bin/bash

# refer to egs/swbd/s5

function doit {
  return 0;
}
function passit {
  return 1;
}
function die {
  echo -e "\nERROR: $1 ...\n"; exit 1;
}
function StopHere {
  echo -e "\nSuccessfully done till now !\n"; exit 0;
}
function StopHere01 {
 echo -e "\n$1\n"; exit 0;
}
. path.sh || die "path.sh expected"
. cmd.sh || die "cmd.sh expected"
# train monophone system
if passit; then 
  steps/train_mono.sh --nj 20 --cmd "$train_cmd" \
  data/train data/lang exp/mono0a || exit 1;
fi
# 
if passit; then
  steps/align_si.sh --nj 20 --cmd "$train_cmd" \
  data/train data/lang exp/mono0a exp/mono0a_ali \
  || die "steps/align_si.sh failed";

  steps/train_deltas.sh --cmd "$train_cmd" \
    3500 50000 data/train data/lang exp/mono0a_ali exp/tri1 \
  || die "steps/train_deltas.sh failed";

fi
# do evaluation 
if passit; then
 
  utils/mkgraph.sh data/lang_test exp/tri1 exp/tri1/graph
  steps/decode.sh --nj 30 --cmd "$decode_cmd" \
   --config conf/decode.config \
   exp/tri1/graph data/dev  exp/tri1/decode_dev
fi

# do alignment and do lda_mllt training
if passit; then
  steps/align_si.sh --nj 50   --cmd "$train_cmd" \
   data/train data/lang exp/tri1 exp/tri1_ali \
  || die "steps/align_si.sh at tri1 failed";
  # Train tri3a, which is LDA+MLLT, on 30k_nodup data.
  steps/train_lda_mllt.sh --cmd "$train_cmd" \
   --splice-opts "--left-context=3 --right-context=3" \
   3500 50000 data/train \
    data/lang exp/tri1_ali exp/tri3a \
   || die "steps/train_lda_mllt.sh failed";
fi
# do evaluation on the lda+mllt models
if passit; then
  utils/mkgraph.sh data/lang_test \
  exp/tri3a exp/tri3a/graph || \
  die "utils/mkgraph.sh failed at exp/tri3a" 
  steps/decode.sh --nj 30 --cmd "$decode_cmd" \
  --config conf/decode.config \
   exp/tri3a/graph data/dev  exp/tri3a/decode_dev \
   || die "steps/decode.sh failed at exp/tri3a";

fi
# train fmllr system
if passit; then
  # From now, we start building a more serious system (with SAT), and we'll
  # do the alignment with fMLLR.

  steps/align_fmllr.sh --nj 50 --cmd "$train_cmd" \
  data/train  data/lang exp/tri3a exp/tri3a_ali \
  || die "steps/align_fmllr.sh failed at exp/tri3a_ali";

  steps/train_sat.sh  --cmd "$train_cmd" \
  4000 100000 data/train data/lang exp/tri3a_ali \
  exp/tri4a || \
  die "steps/train_sat.sh failed at training exp/tri4a";

fi
# evaluate the fmllr system
if passit; then
  utils/mkgraph.sh data/lang_test exp/tri4a exp/tri4a/graph
   sbatch -p speech  -N 2 --ntasks-per-node=10  --mem-per-cpu=5000MB -o log/LOG.tri4a.decode.dev \
   steps/decode_fmllr.sh --nj 20 --cmd "$decode_cmd" \
   --config conf/decode.config \
   exp/tri4a/graph data/dev  exp/tri4a/decode_dev || \
   die "decode_fmllr.sh failed for dev data "
   sbatch -p speech  -N 2 --ntasks-per-node=10  --mem-per-cpu=5000MB -o log/LOG.tri4a.decode.train_dev \
   steps/decode_fmllr.sh --nj 20 --cmd "$decode_cmd" \
   --config conf/decode.config \
   exp/tri4a/graph data/train_dev \
   exp/tri4a/decode_train_dev || \
   die "decode_fmllr.sh failed for train_dev data"

fi
# train deep neural network
if passit; then
  steps/align_fmllr.sh --nj 20 --cmd "$train_cmd" \
  data/train_nn data/lang exp/tri4a exp/tri4a_ali_train_nn

fi

if doit ;then
  # srun -p speech  -N 1 --ntasks-per-node=16 ulocal/run_nnet_cpu.sh
  idx=1
  while [ -e log/LOG.run_nnet_cpu.$idx  ]; do
    idx=$[idx+1]
  done 
  logfile=log/LOG.run_nnet_cpu.$idx
  # sbatch -p speech -o $logfile -N 4 --ntasks-per-node=4 --mem-per-cpu=20000MB \
  # sbatch -p speech  -N 4 --ntasks-per-node=4   -o $logfile  --mem-per-cpu=20000MB \
  sbatch -p speech  -n20  -o $logfile  --mem-per-cpu=10000MB \
  ulocal/run_nnet_cpu.sh 
fi
