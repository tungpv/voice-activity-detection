#!/bin/bash

# refer to egs/swbd/s5

function doit {
  return 0;
}
function passit {
  return 1;
}
function die {
  echo -e "\nERROR: $1 ...\n"; exit 1;
}
function StopHere {
  echo -e "\nSuccessfully done till now !\n"; exit 0;
}
function StopHere01 {
 echo -e "\n$1\n"; exit 0;
}

devnum=4000

sdir=data/train

total=`wc -l < $sdir/text`

echo -e "\ntotal_num=$total\n"
dir1=data/train_dev
dir2=data/train_nn

netnum=$[total-devnum]
echo -e "\nneural_network_num=$netnum\n"

utils/subset_data_dir.sh --first $sdir $devnum $dir1
utils/subset_data_dir.sh --last $sdir  $netnum $dir2

