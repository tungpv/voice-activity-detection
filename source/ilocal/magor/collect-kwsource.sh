#!/bin/bash

echo -e "\n## LOG: $0 $@\n"

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}


if [ $# -ne 3 ]; then
  echo -e "\n\n Usage:$0 <data> <indusdb_dir> <kwsource_dir> \n\n"
  exit 1
fi

data=$1; shift
indusdb=$1; shift;
kwsource=$1; shift;

[ -f $data/segments ] || die "segments file is not there $data"
[ -d $indusdb ] || die "indusbd source folder is not ready";

fpath=$(cd $indusdb; pwd)

mkdir -p $kwsource ; rm $kwsource/* 2>/dev/null

if [[ ! -f $kwsource/ecf.xml || ! -f $kwsource/duration ]]; then
  ( cd  $kwsource; target=`ls $fpath/*ecf.xml`; ln -s $target ecf.xml)
  [ -f $kwsource/ecf.xml ] || die  "making ecf.xml file failed"
  
  head -1 $kwsource/ecf.xml | \
  grep -o -E "duration=\"[0-9]*[    \.]*[0-9]*\"" |\
  perl -e 'while($m=<>) {$m=~s/.*\"([0-9.]+)\".*/\1/; print $m;}' \
  > $kwsource/duration

fi


if [ ! -f $kwsource/rttm ]; then
  (cd $kwsource; target=`ls $fpath/*.rttm`; [ ! -z $target ] && ln -s $target rttm )
  [ -f  $kwsource/rttm ] || echo "## WARNING: making rttm file failed"; 
fi

fpath=$(cd $data; pwd)
if [[ ! -f $kwsource/segments || ! -f $kwsource/utter_id ]]; then
  (cd $kwsource; ln -s $fpath/segments)   
  if [[ ! -f $data/utter_map || ! -f $data/utter_id ]]; then
    isteps/kws/make_utter_id.pl $data/segments >$data/utter_id
    cat $data/segments | awk '{print $1" "$2}' | \
    sort | uniq > $data/utter_map;
  fi
  (cd $kwsource; ln -s $fpath/utter_id; ln -s $fpath/utter_map)
fi

