#!/usr/bin/env perl
use strict;
use warnings;

my $numArgs = scalar @ARGV;
print STDERR "## LOG: $0 ", join " ", @ARGV, "\n";
if ($numArgs != 2) {
  die "\n\nUsage example:\n cat text | $0 words.txt 2  >oovs.txt \n\n";
}

my ($fname, $from) = @ARGV;
$from = 1 if $from < 1;
open (F, "$fname") or die "file $fname cannot open\n";
my %vocab = ();
while (<F>) {
  chomp;
  m/(\S+)\s+(.*)/ or next; my ($w) = ($1);
  $vocab{$w} = 0;
}
print STDERR "stdin expected\n\n";
while (<STDIN>) {
  chomp;
  my @A = split (/\s+/, $_);
  for(my $i = $from -1 ; $i < @A; $i ++) {
    my $w = $A[$i];
    if(not exists $vocab{$w}) {
      print $w, "\n";
    }
  }

}
print STDERR "stdin ended\n\n";
close F;
