#!/bin/bash

# ilocal/morph/text_segment.sh --from 2 model text dir
# ilocal/morph/text_segment.sh --from 2 test/morph/morph.mdl data/train/text test/segtext 

echo -e "\n## $0 $@\n"

function die {
  echo -e "\n## ERROR: $1\n";
  exit 1
}

. path.sh


# --
from=1;  # from where to begin
word_to_morph=;
# --

. utils/parse_options.sh

if [ $# -ne 3 ]; then
 echo -e "\n\nUsing morph model to segment text, which"
 echo "put into specified folder"
 echo -e "\nUsage: $0 [options] <morph_model> <text> <dir>"
 echo -e "--from N# from the Nth field\n\n";
 exit 1
fi

mdl=$1; shift;
text=$1; shift;
dir=$1; shift

for x in $mdl $text; do
  [ -e $x ] || die "file $x is not there";
done

sdir=$dir;

dir=`mktemp -d -p $sdir `
trap "rm -rf $dir " EXIT

[ -d $dir ] || die "making folder $dir is failed";

echo -e "\n## create word list from text \n";
cat $text | \
awk  -v from=$from '{for(i=from; i<=NF;i++){print $i}}' | \
sort -u > $dir/wlist.txt

wlists=`wc -l < $dir/wlist.txt`;
if [[ $wlists -eq 0 ]]; then
  die "$dir/wlist.txt is empty";
fi
echo -e "\nwlists=$wlists\n"

morfessor-segment -l $mdl -o $dir/morph.txt $dir/wlist.txt
if [[ `wc -l < $dir/wlist.txt` -ne `wc -l < $dir/morph.txt` ]]; then
  die "word and morph numbers are mismatched, check $dir/wlist.txt and $dir/morph.txt";
fi

paste <(cat $dir/wlist.txt) <(cat $dir/morph.txt| sed -e 's:_: :g') | sort -u > $sdir/word_morph.txt
[ -e $sdir/word_morph.txt ] || die "file $sdir/word_morph.txt is not there";

if [ ! -z $word_to_morph ]; then
  mv $sdir/word_morph.txt $sdir/$word_to_morph
fi


