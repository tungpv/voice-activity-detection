#!/usr/bin/env perl 
use warnings;
use strict;
my $numArg = scalar @ARGV ;
if ($numArg != 1) {
  die "\n\nUsage example cat dict1 | $0 dict2 > dict1-dict.txt \n\n";
}

my ($fname) = @ARGV;
my %vocab = ();
open (F, "$fname") or die "file $fname cannot open\n";
while(<F>) {
  chomp;
  m/(\S+)\s+(.*)/ or next; 
  $vocab {$1} = 0;
}
close F;

print STDERR "\nstdin expected\n";
while (<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  next if exists $vocab{$1};
  print $_, "\n";
}
print STDERR "\nstdin ended\n";
