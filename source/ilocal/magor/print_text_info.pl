#!/usr/bin/env perl
use warnings;
use strict;

my $argNum = scalar @ARGV;

if ($argNum != 3) {
  die "\n\nUsage:$0 <text> <words> <new_text> \n\n";
}

my ($text, $words, $newtext) = @ARGV;


my %vocab = ();
open (V, "$words") or die "words file $words opens error";
while (<V>) {
  chomp;
  m/(\S+)/ or next;
  my $word = $1; 
  $vocab{$word} = 0;
}
close V;
open (NT, "$newtext") or die "newtext file $newtext cannot open";
open (T, "$text") or die "source text file $text opens error";
my %tVocab = ();
while (<T>) {
  chomp;
  my @A = split (' ', $_);  
  for (my $idx = 0; $idx < @A; $idx ++) {
    my $word = $A[$idx];
    $tVocab{$word} ++;
  }
}
close T;
my ($total, $unit) = (0, 1024*1024);
foreach my $word (keys %tVocab) {
  if (not exists $vocab{$word}) {
    print NT $word, " $tVocab{$word}\n";
  }
  $total += $tVocab{$word};
}
printf "total words: %.4fM\n", $total/$unit;
close NT;
