#!/usr/bin/env perl

use warnings;
use strict;
if (@ARGV != 2) {
  die "\n\nUsage example:\n cat word.txt | $0 <word_morph.txt> <morph.txt>\n\n";
}
my ($dfn, $tfn) = @ARGV;

open(F, "$dfn") or die "file $dfn cannot open\n";
my %vocab = (); 
while (<F>) {
  chomp;
  my @A = split (/\s+/, $_); 
  my $w = shift @A;  my $str = join " ", @A;
  $vocab{$w} = $str;
}
close F;

open (F, "$tfn") or die "file $tfn cannot open\n";
print STDERR "stdin expected \n";
while (<STDIN>) {
  chomp;
  my @A = split (/\s+/, $_);  
  my $s = "";
  for(my $i = 0; $i < @A; $i++) {
    my $w = $A[$i];
    if (exists $vocab{$w}) {
      $w = $vocab{$w};
    }
    $s .= " $w";
  }
  print F "$s\n";
}
print STDERR "stdin ended\n";
close F;
