#!/usr/bin/env perl
use strict;
use warnings;
use Math::Random::Secure qw(irand rand);

print STDERR "\n\n## LOG: $0 ", join " ", @ARGV, "\n\n";

if (@ARGV != 5) {
  die  "\n\nUsage Example: cat text | $0 words.txt nonspeech.txt 0.05(oov_rate)  3(ngram) 249(number)\n\n";
}

my ($words, $nonsp, $oov_rate, $ngram, $ncount) = @ARGV;

if ($ngram <1 or $ncount < 1) {
  die "ngram or ncount is illegal ($ngram, $ncount), both should be over zero\n";
}

sub MakePattern {
  my ($nspace) = @_;
  my $s = "\\S+";
  if($nspace > 0) {
    for(my $i=0; $i < $nspace; $i++) {
      $s .= "\\s"; $s .="\\S+";
    }
  }
  return $s;
}
sub LoadWords {
  my ($vocab, $fname) = @_;
  open F, "$fname" or die "file $fname cannot open\n";
  while (<F>) {
    chomp;
    m/(\S+)\s+(\S+)/ or next;
    $$vocab{$1} = 0;  
  }
  close F;
}
sub LoadNon {
  my ($vocab, $fname) = @_;
  open F, "$fname" or die "file $fname cannot open\n";
  while (<F>) {
    chomp;
    m/(\S+)/ or next;
    $$vocab{$1} = 0;
  }
  close F;
}

sub HasNon {
  my ($vocab, $s) = @_;
  my @A = split(/\s+/, $s);
  for (my $i =0 ; $i < @A; $i++) {
    my $w = $A[$i];
    next if length($w) == 0;
    return 1 if exists $$vocab{$w};
  }
  return 0;
}

sub HasOOV {
  my ($vocab, $s) = @_;
  my @A = split(/\s+/, $s);
  for (my $i =0 ; $i < @A; $i++) {
    my $w = $A[$i];
    next if length($w) == 0;
    return 1 if not exists $$vocab{$w};
  }
  return 0;
}
sub CheckTerm {
  my ($term) = @_;
  my @A = split (/\s+/,$term);
  for (my $i = 0; $i < @A; $i ++) {
    my $w = $A[$i];
    return 1 if length $w < 2;
  }
  return 0;
}
sub FindTerm {
  my($sVocab, $vocab, $upper, $lower) = @_;
  my $nonEmpty = 0;
  for (my $x = $lower; $x <= $upper; $x ++) {
    $nonEmpty ++ if exists $$sVocab{$x};
  }
  return 1 if $nonEmpty == 0;
  my %v = (); my $total = 0; my %v2 = ();
  while (1) {
    my $range = $upper - $lower + 1;
    my $x = $lower + irand($range);
    if (exists $$sVocab{$x}) {
      my $pA = $$sVocab{$x};  
      my $len = scalar @$pA;
      my $x2 = irand($len);  
      my $w = $$pA[$x2];
      next if CheckTerm ($w) == 1;
      if (not exists $$vocab{$w}) {
        $$vocab{$w} = $x;
        return 0;
      }
      $v{$x}{$x2} = 0;
      my $size = keys %{$v{$x}};
      if ( $size == $len) {
        $total ++;
        return 1 if ($total == $nonEmpty);
      }
    }
  }
}
sub ReverseVocab {
  my ($sVocab, $vocab) = @_;
  foreach my $key (keys %$sVocab) {
    my $val = $$sVocab{$key};
    if (exists $$vocab{$val}) {
      my $pA = $$vocab{$val};
      push @$pA, $key;
    } else {
      my @A = ();
      $$vocab{$val} = \@A;
      my $pA = $$vocab{$val};  
      push @$pA, $key;
    } 
  }
}
sub BinSel {
  my ($sVocab, $vocab, $total) = @_;
  my ($max, $min) = (0,0);
  my $S = 0; my $S2 = 0; my $N  = 0;
  foreach my $term (keys %$sVocab) {
    $N ++; 
    my $x = $$sVocab{$term};
    $min = $x if $x < $min;
    $max = $x if $x > $max;
  }
  my %revVocab = ();
  ReverseVocab($sVocab, \%revVocab);
  my @A = ();
  if ($max > 10) {
    @A = ([$max, 10, 0.2], [10, 2, 0.7], [2, 1, 0.1] );
  } else {
    @A = ([$max, 1, 1.0]);
  }
  for (my $i =0; $i < @A; $i++) {
    my ($upper, $lower, $factor) = ($A[$i][0], $A[$i][1], $A[$i][2]);
    my $x = $total*$factor; $x = int($x);
    $x = 1 if $x < 1; 
    print STDERR "upper=$A[$i][0], lower=$lower, number=$x\n";
    my $y = 0;
    while ($y < $x) {
      my $failed = FindTerm (\%revVocab, $vocab, $upper, $lower);
      last if $failed == 1;
      $y ++;
    }
    print STDERR "expected=$x, saturated=$y\n";
  }
  ## printf STDERR ("%d %d %.2f %.2f\n", $min, $max, $mean, $sigma);
  
}
my %wordVocab = ();
my %ngramVocab = ();
my %oovNgramVocab = ();
my %nonVocab = ();
LoadWords(\%wordVocab, $words);
LoadNon(\%nonVocab, $nonsp);
print STDERR "stdin expected \n";
my $lineCount = 0;
my $pattern = MakePattern ($ngram -1);
while (<STDIN>) {
  chomp; 
  $lineCount ++;   # print STDERR "line=$lineCount, $_\n";
  my @A =split(/\s+/, $_);
  my $s = join " ", @A; $s =~ s/^[\s]+//g; $s =~ s/[\s]+$//g;
  while ($s =~m/($pattern)/) {
    my $nstr = $1;
    $s =~ s/^\S+//;
    $s =~ s/^\s//;
    next if HasNon(\%nonVocab, $nstr) == 1;
    if ( HasOOV(\%wordVocab, $nstr) == 1) {
      $oovNgramVocab{$nstr} ++;
      $ngramVocab{$nstr} ++; 
    } else {
      $ngramVocab{$nstr} ++;
    }
  }
}
print STDERR "stdin ended\n";
my %selVocab = ();
BinSel (\%ngramVocab, \%selVocab, $ncount);
my $oovCount = $ncount*$oov_rate;
if ($oovCount > 5) {
  BinSel (\%oovNgramVocab, \%selVocab, $oovCount);
}
foreach my $key (keys %selVocab) {
  print "$key\t$selVocab{$key}\n";
}


