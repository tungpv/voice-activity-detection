#!/usr/bin/env perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 5) {
  die "\n\nUsage example:\n $0 audio_length(s) lang version_string source_type segments \n\n";
}

my($len, $lang, $version, $source_type, $segments) = @ARGV;

open(F, "$segments") or die "file $segments cannot open\n";
print "<ecf source_signal_duration=\"$len\" language=\"$lang\" version=\"$version\">\n";
my $total = 0;
while (<F>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/ or next;
  my ($excerpt, $fname, $start, $end) = ($1, $2, $3, $4); 
  my $dur = $end - $start;
  print "<excerpt audio_filename=\"$fname\" channel=\"1\" tbeg=\"$start\" dur=\"$dur\" source_type=\"$source_type\"/>\n";
  $total += $dur;
}
close F;
if (abs($total -$len)> 1.0) {
  die "actual total length=$total, your specified =$len, inconsistent !\n";
}
print "</ecf>";

