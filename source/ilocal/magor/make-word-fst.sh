#!/bin/bash

# desgin: ilocal/kws/make_roman_kwlist.sh word-lexicon.txt morph-lexicon.txt morph.mdl  source-tamil-kwlist.xml  target-dir

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}


. path.sh || die "path.sh expected";
echo -e "\nLOG: $0 $@\n";
echo -e "\nLOG: runing on `hostname` `date`\n"

. parse_options.sh || die "parse_options.sh expected";

set -u
set -e
set -o pipefail

if [ $# -ne 3 ]; then
  echo -e "\n\n making wkeywords.fsts from kwlist.xml";
  echo -e "Usage Example:\n"
  echo -e "$0 <kwxml> <lang> <dir>\n\n";
  exit 1
fi

kwxml=$1; shift;
lang=$1; shift;
dir=$1; shift;

sdir=$dir; dir=$dir/temp; 
[ -d $dir ] || mkdir -p $dir 

if [ ! -f $sdir/keywords.fsts ]; then
  isteps/kws/make_kwlist_from_xml.pl $kwxml > $dir/keywords.txt
  
  cat $dir/keywords.txt  | \
  sym2int.pl --map-oov 0 -f 2- $lang/words.txt > $dir/keywords.int 2> $dir/keywords.log
  
  cat $dir/keywords.int | \
    awk '{if (/ 0 /||/ 0$/){}else{print ;}}' > $dir/keywords_no_oov.int
  transcripts-to-fsts ark:$dir/keywords_no_oov.int ark,t:$sdir/keywords.fsts || exit 1
fi

