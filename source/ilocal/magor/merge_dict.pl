#!/usr/bin/env perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\n\nUsage example: cat merged.dict | $0  main.dict > bigger.dict\n\n";
}

my ($fname) = @ARGV;
my %vocab = ();
open(F, "$fname")  or die "file $fname cannot open\n";
while (<F>) {
  chomp;
  s/^[\s]+//g;
  my @A = split (/\s+/, $_);
  my $w = shift @A; 
  next if length $w == 0;
  my $s = join " ", @A;  $s =~ s/^[\s]+//g;
  print "$w\t",$s, "\n";
  $vocab{$w} = 0;
}
close F;

print STDERR "stdin expected\n";
while (<STDIN>) {
  chomp;
  s/^[\s]+//g;
  my @A = split (/\s+/, $_);
  my $w = shift @A;
  next if length $w == 0;
  my $s = join " ", @A; $s =~ s/^[\s]+//g;
  next if exists $vocab{$w};
  print "$w\t", $s, "\n";
}
print STDERR "stdin ended\n";
