#!/bin/bash
echo -e "\n## LOG: $0 $@\n" >&2
if [ $# -ne 1 ]; then
  echo -e "\n\nUsage : $0 <wavlist>\n\n" >&2;  exit 1
fi

wavlist=$1; shift

if [ ! -f $wavlist ]; then
  echo -e "\nfile $wavlist is not there\n"; exit 1
fi

for f in `cat $wavlist`; do
  shntool len $f | \
    awk -v f=`echo $f| sed 's#.*\/##g'| sed 's#\.wav##g'` 'BEGIN{count=0;}
    {if($1 ~ /:/ && count==0 ){ split ($1, A, ":");  
     printf "%s %s\t%.2f\t%.2f\n",f, f, A[1], A[2]; count ++;  } }' ;
done 
