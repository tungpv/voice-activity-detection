#!/usr/bin/env perl
use warnings;
use strict;

my $numArg = scalar @ARGV;
if ($numArg != 2) {
  die "\n\nUsage example: $0 nonspeech ctm > rttm\n\n";
}
my ($nonspeech, $ctm) = @ARGV;
open (F, "$nonspeech") or die "file $nonspeech cannot open\n";
my %vocab = ();
while (<F>) {
  chomp; 
  /(\S+)/ or next;
  $vocab{$1} = 0;
}
close F;

open (F, "$ctm") or die "file $ctm cannot open\n";
while (<F>) {
  chomp;
  /(.*)\s+(\S+)/ or next; 
  my ($part1, $w) = ($1, $2);
  if (not exists $vocab{$w}) {
    print "LEXEME $_ lex <NA>  <NA>\n";
  } else {
    print "NON-LEX $part1 <no-speech> <NA> <NA> <NA>\n";
  }
}
close F;
