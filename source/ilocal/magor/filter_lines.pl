#!/usr/bin/env perl
use strict;
use warnings;

my $oov = "false";
my $remove_last_field="false";
my $remove_first_field="false";
for(my $i = 0; $i < @ARGV ; $i++) {
  my $argstr = $ARGV[$i];
  my $hasSwitch = 0;
  if ($argstr =~ /^--/) {
    $argstr =~ s/^--//g; $argstr =~ s/-/_/g;
    $hasSwitch = 1;
  }
  if ($argstr eq "oov") {
    splice @ARGV, $i, 1;  $i --;
    $oov = "true"; next;
  }
  if ($argstr eq "remove_last_field") {
    splice @ARGV, $i, 1; $i --; 
    $remove_last_field = "true"; next;
  }
  if ($argstr eq "remove_first_field") {
    splice @ARGV, $i ,1; $i --;
    $remove_first_field = "true"; next;
  }
  if ($hasSwitch == 1) {
    die "$0: ERROR unidentified options $argstr\n";
  }
}
if (@ARGV < 1) {
  die "\n\nUsage:\n $0 [options] words.txt\n\n";
}
my ($words) = @ARGV;
open (F, "$words") or die "file $words cannot open\n";
my %vocab = ();
while (<F>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $vocab{$1} ++;
} 
close F;
print STDERR "$0: stdin expected\n";
while (<STDIN>) {
  chomp;
  my @A = split (/\s+/);
  if ($remove_first_field eq "true") {
    shift @A;
  }
  if ($remove_last_field eq "true") {
    pop @A;
  }
  my $hasOOV = 0;
  for(my $i = 0; $i <@A; $i++) {
    my $w = $A[$i];
    if (not exists $vocab{$w}) {
      $hasOOV = 1; 
      if ($oov eq "true") {
        print "$_\n"; $i = scalar @A;   last;
      }
    }
  }
  if ($hasOOV == 0 && $oov eq "false" ) {
    print "$_\n";
  }    
}
print STDERR "$0: stdin ended\n";
