#!/bin/bash

. path.sh
. cmd.sh

echo -e "\n## LOG: $0 $@\n" 

if [ $# -ne 2 ]; then
  echo -e "\nUsage: $0 <sdir> <dir>\n"; exit 1
fi

sdir=$1; shift;
dir=$1; shift;
mkdir -p $dir
cat $sdir/segments | perl -ne 'if(/0\.00\t0\.00$/){print $_;} ' > $dir/bad_segments
for x in wav.scp text utt2spk segments; do
  f=$sdir/$x;
  [ -f $f ] && cat $f | \
  perl -e '$file = shift @ARGV; open (F, "<$file") or die "file $file cannot open\n";
  while(<F>) {chomp; m/(\S+)\s+(.*)/ or next; $vocab{$1} = 0; } close F; 
  while (<STDIN>){chomp; m/(\S+)\s+(.*)/ or next; if(not exists $vocab{$1}){print $_,"\n";}} ' $dir/bad_segments |sort > $dir/$x;
done

utils/utt2spk_to_spk2utt.pl < $dir/utt2spk > $dir/spk2utt
