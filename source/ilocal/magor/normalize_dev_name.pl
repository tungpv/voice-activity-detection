#!/usr/bin/env perl 
use warnings;
use strict;
if (@ARGV != 1 ) {
  die "\nfile name expected\n\n";
}
my ($path) = @ARGV;
my @A1 =  split ("/", $path);
my $prefix = sprintf "%s_%s","magor", "tdp"; 

my $name = $A1[@A1-1];
$name=~ s/[-]//g; 
$name =~ s/\.wav//g;
my @A = split ("_",$name);

my $len = scalar @A;

my $s = $A[$len -1];

if ($s !~ /(\d+)/ ) {
   $A[$len -1] = $A[$len -2];
   $A[$len -2] = $s;
   $s = $A[$len -1];
}
if ($s !~ /(\d+)/) {
  die "\nunidentified string $path\n";
}
$s = \$A[$len-1];

$$s = sprintf "%05d.wav", $$s+1;
if (length($A[$len-2])<2) {
  $A[$len-3] .=  $A[$len-2];
  splice @A, $len -2, 1; $len --;
}
$s = \$A[$len-2];
while (length ($$s) < 7) {
  $$s = sprintf "0%s", $$s; 
}
my $newName = join "_", @A;
$newName = sprintf "%s_%s", $prefix, $newName;

print lc $newName;
