#!/bin/bash

echo -e "\nLOG: $0 $@\n"

. path.sh
. cmd.sh

# begin user options
# end user options
set -e           #Exit on non-zero return code from any command
set -o pipefail  #Exit if any of the commands in the pipeline will 
                 #return non-zero return code

. parse_options.sh

if [ $# -ne 4 ]; then
  echo -e "\n\nUsage: $0 wav.scp text  previous_prefix current_prefix dest_dir\n\n"
  exit 1
fi

sdir=$1; shift;
prev_prefix=$1; shift;
prefix=$1; shift;
dir=$1; shift

mkdir -p $dir;
if [ ! -f $dir/utt2spk ]; then
  if [ ! -f $sdir/utt2spk ]; then
    echo -e "\n## ERROR: utt2spk is not in $sdir\n"; exit 1
  fi
  cat $sdir/utt2spk | \
  perl -e '($spkmap, $uttmap, $utt2spk) = @ARGV;
    $prefix = "magor_trn";  $count = 0; $uttcount = 0;
    open(S, ">$spkmap") or die "file $spkmap cannot open\n"; 
    open(U, ">$uttmap") or die "file $uttmap cannot open\n";
    open (UTT2SPK, "$utt2spk") or die "file $utt2spk cannot open";
    while(<STDIN>) { chomp; m/(\S+)\s+(\S+)/ or next; ($utt, $spk) = ($1, $2);
      $newspk = $vocab{$spk};   $uttcount = $uttVocab{$newspk};
      if( not exists $vocab{$spk}) {
        $count ++;    $newspk = sprintf "%s%05d", $prefix, $count;
        $vocab{$spk} = $newspk;    $uttVocab{$newspk} = 0; $uttcount = 0;
      }
      $uttVocab{$newspk} ++;    $uttcount ++; 
      $newutt = sprintf "%s_%07d", $newspk, $uttcount;
      print S "$spk\t$newspk\n";
      print U "$utt\t$newutt\n"; 
      print UTT2SPK "$newutt\t$newspk\n";
    }
    close S; close U; close UTT2SPK;
  '  $dir/spkmap.txt $dir/uttmap.txt "|sort -u > $dir/utt2spk"  || exit 1
   utils/utt2spk_to_spk2utt.pl < $dir/utt2spk |sort > $dir/spk2utt
fi

tgtdir=$dir;
if [ ! -f $dir/audio/.done ]; then
  dir=$tgtdir/audio; mkdir -p $dir
  echo -e "\n## copy audo `date`\n";
  for f in `cat $sdir/wav.scp| sed  "s#$prev_prefix#$prefix#g"| awk '{print $2;}' `; do
    if [ ! -e $f ]; then
      echo -e "\n## WARNING: $f is not there\n"
      continue;
    fi
    newfile=$dir/$( grep -w `basename $f| sed 's#.*\/##g'|sed 's#\.wav##g'` $tgtdir/uttmap.txt | awk '{printf "%s.wav", $2}')
    echo -e "\ncopy $f $newfile\n"; 
    if [ ! -f $newfile ]; then  cp $f $newfile || exit 1; fi
  done
  echo -e "\n## copying audo done `date`\n";
  touch $dir/.done
fi
dir=$tgtdir;
echo -e "\n## LOG: prepareing wav.scp & segments\n"
if [ ! -f $dir/segments ]; then
  if [ ! -f $dir/wavlist ]; then
    adir=$(cd $dir/audio; pwd);
    find $adir -name "*.wav"  | sort > $dir/wavlist;
  fi
  cat $dir/wavlist | \
  perl -ne 'chomp; $name = $_; $name =~ s/.*\///g; $name =~ s/\.wav//g; print "$name $_\n";' | \
  sort > $dir/wav.scp
  echo -e "\n## making segments file\n";
  ilocal/magor/make_segments.sh $dir/wavlist > $dir/segments || exit 1
fi

if [ ! -f $dir/text ]; then
  echo -e "\n## making text file in $dir\n"
  cat $sdir/text | \
  perl -e '($file) = @ARGV; open (F, "$file") or die "file $file cannot open\n";
    while (<F>) {
      chomp;  m/(\S+)\s+(\S+)/ or next;  $vocab{$1} = $2;
    }
    close F;
    while(<STDIN>) {
      chomp; m/(\S+)\s+(.*)/ or next; ($name, $text) = ($1, $2);
      if (exists $vocab{$name}) {
        print "$vocab{$name} ", $text, "\n";
      }else { print STDERR "WARNING: $name, $_ is oov\n"; }
    }
  ' $dir/uttmap.txt  | \
  sed 's#<NOISE>#<noise>#g' | \
  sed 's#<SPOKEN_NOISE>#<v-noise>#g' | \
  sed 's#<UNK>#<unk>#g' |sort > $dir/text
fi

echo -e "\n## $0 done `date` !\n";
