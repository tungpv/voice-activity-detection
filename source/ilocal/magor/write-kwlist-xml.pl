#!/usr/bin/env perl
use strict;
use warnings;

print STDERR "\n\n## LOG: $0 ", join " ", @ARGV, "\n\n";

my $numArg = scalar @ARGV;

die "\n\nUsage Example: cat sel.txt | $0 stem ecf language version_string > kwlist.xml\n\n" if $numArg != 4;

my ($stem, $ecf, $lang, $vstr) = @ARGV;

print STDERR "stdin expected \n";
print "<kwlist ecf_filename=\"$ecf\" language=\"$lang\" encoding=\"UTF-8\" compareNormalize=\"\" version=\"$vstr\">\n";
my $count = 0;
while (<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my @A = split (/\s+/, $_);
  pop @A; 
  my $s = join " ", @A; $s =~ s/^[\s]//g; $s =~ s/[\s]$//g;
  $count ++;
  printf "  <kw kwid=\"%s-%05d\">\n", $stem, $count;
  printf "%4c<kwtext>$s</kwtext>\n",0x20, $s;
  print "  </kw>\n";
}
print "</kwlist>";
print STDERR "stdin ended \n";


