#!/bin/bash
echo -e "\n## LOG: $0 $@\n" >&2 

if [ $# -ne 1 ]; then
  echo -e "\n$0 <filename_map.txt>\n" >&2; exit 1
fi

mapfile=$1 ;shift;
if [ ! -f $mapfile ]; then
  echo -e "\n## ERROR: $0 file $mapfile is not there \n" >&2 ; exit 1
fi
cat $mapfile | \
  perl -ne  ' chomp; m/(\S+)\s+(\S+)/ or next; $fName = $2; 
  @A = split ("_", $fName); shift @A; shift @A; pop @A; 
  $s = join "_", @A; $s =~ s/_$//g;
  print $fName, " ", $s, "\n";' | sort -u 

