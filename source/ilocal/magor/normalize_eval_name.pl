#!/usr/bin/env perl 
use warnings;
use strict;
if (@ARGV != 1 ) {
  die "\nfile name expected\n\n";
}
my ($path) = @ARGV;
my @A1 =  split ("/", $path);

my $name = $A1[@A1-1];

$name =~ s/\.wav//g;
$name=~ s/[-+.]/_/g; 
my @A = split ("_",$name);
my $len = scalar @A;

my $s = \$A[$len-1];

$$s = sprintf "%s.wav", $$s;
my $newName = join "_", @A;
print lc $newName;
