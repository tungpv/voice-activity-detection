#!/bin/bash

# desgin: ilocal/kws/make_roman_kwlist.sh word-lexicon.txt morph-lexicon.txt morph.mdl  source-tamil-kwlist.xml  target-dir

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}


. path.sh || die "path.sh expected";
echo -e "\nLOG: $0 $@\n";
echo -e "\nLOG: runing on `hostname` `date`\n"

wordkw=true
morphkw=true
mixkw=true

. parse_options.sh || die "parse_options.sh expected";

set -u
set -e
set -o pipefail

if [ $# -ne 5 ]; then
  echo -e "\n\n making wkeywords.fsts from kwlist.xml";
  echo -e "Usage Example:\n"
  echo -e "$0 <short_listed_words.txt> <morph_model> <kwxml> <lang> <dir>\n\n";
  exit 1
fi
shrink_words=$1; shift
morph_model=$1; shift
kwxml=$1; shift;
lang=$1; shift;
dir=$1; shift;

sdir=$dir; dir=$dir/temp; 
[ -d $dir ] || mkdir -p $dir 

for f in $shrink_words $morph_model $kwxml; do
  if [ ! -f $f ]; then
    echo -e "\n## file $f does not exist\n"; exit 1
  fi
done

if [ ! -f $dir/keywords.txt ]; then
  isteps/kws/make_kwlist_from_xml.pl $kwxml > $dir/keywords.txt
fi 
keywords_int=$dir/keywords.int
if [[ $wordkw == "true" && ! -f $dir/wkeywords.int ]]; then 
  echo -e "\n## $0: making wkeywords.int `date`\n"
  cat $dir/keywords.txt  | \
  sym2int.pl --map-oov 0 -f 2- $lang/words.txt > $dir/wkeywords.int 2> $dir/wkeywords.log
  cat $dir/wkeywords.int > $keywords_int
fi  
if [[ $mixkw == "true"  &&  ! -f $dir/hkeywords.int ]]; then
   echo -e "\n## $0: making hkeywords.int `date`\n"
   cat $dir/keywords.txt | \
   perl -ne "if(/\'/){s/\'/\s/g;} print $_;" | \
   ilocal/magor/dump_oovs.pl $shrink_words 2 > $dir/oovs.txt
   if [ `wc -l < $dir/oovs.txt` -gt 0 ]; then
     ilocal/magor/word-to-morph.sh --word-to-morph oovs.word-to-morph.txt  $morph_model  $dir/oovs.txt $dir  
   fi
   cat $dir/keywords.txt | \
   perl -ne "if(/\'/){s/\'/\s/g;} print $_;" | \
   ilocal/magor/text_transcribe.pl  "cat $dir/oovs.word-to-morph.txt $shrink_words|" 2 | \
   sym2int.pl --map-oov 0 -f 2- $lang/words.txt > $dir/hkeywords.int 2> $dir/hkeywords.log
   cat $dir/hkeywords.int >>$keywords_int
fi
if  [[ $morphkw == "true" && ! -f $dir/mkeywords.int   ]]; then
  echo -e "\n## $0: making mkeywords.int `date`\n"
  cat $dir/keywords.txt | \
  perl -ne "if(/\'/){s/\'/\s/g;} print $_;"  > $dir/keywords01.txt
  ilocal/magor/text_segment.sh --from 2 --output mkeywords.txt  $morph_model  $dir/keywords01.txt $dir || exit 1
  cat $dir/mkeywords.txt | \
  sym2int.pl --map-oov 0 -f 2- $lang/words.txt > $dir/mkeywords.int 2> $dir/mkeywords.log
  cat $dir/mkeywords.int >> $keywords_int
fi

if [ ! -f $sdir/keywords.int ]; then
  cat $keywords_int| \
  perl -ne 'chomp; @A = split(/\s+/,$_); $kwid = shift @A; $s = join " ", @A; print "$kwid $s\n"; ' | \
  sort -u > $sdir/keywords.int
fi
  
cat $sdir/keywords.int | \
awk '{if (/ 0 /||/ 0$/){}else{print ;}}' > $sdir/keywords_no_oov.int
transcripts-to-fsts ark:$sdir/keywords_no_oov.int ark,t:$sdir/keywords.fsts || exit 1

