#!/usr/bin/env perl
use strict;
use warnings;


print STDERR "\n## LOG : $0 ", join " ", @ARGV, "\n";

die "\n\nUsage Example: cat text | $0 words.txt 2 > text1 \n\n"
if @ARGV != 2;

my ($words, $from) = @ARGV;
$from = 1 if $from < 1;
open (F, "$words") or die "file $words cannot open\n";
my %vocab = ();
while (<F>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($w, $pron) = ($1, $2);
  $vocab{$w} = $pron;
}
close F;

print STDERR "stdin expected\n";
while (<STDIN>) {
  chomp;
  my @A = split (/\s+/, $_);
  my $s = "";
  for(my $i = 0; $i < @A; $i++) {
    my $w = $A[$i];
    if ($i < $from - 1) {
      $s .= " $w";
      next;
    }
    $w = $vocab{$w} if exists $vocab{$w};
    $s .= " $w";
  }
  $s =~ s/^[\s]+//g;
  print "$s\n";
}
print STDERR "stdin ended\n";
