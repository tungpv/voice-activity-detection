#!/usr/bin/env perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;
print "$0 ", join " ", @ARGV, "\n";
die "\n## Example cat text | $0 sd_segments sd_utt2spk\n"  if $numArgs != 4 ;

my ($segments, $utt2spk, $new_utt2spk, $segments_map) = @ARGV;

open (F, "$utt2spk") or die "file $utt2spk cannot open\n";
my %vocab01 = ();
my %vocab02 =();
my %vocab03 =();
my %tmpvocab = ();
while (<F>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  my ($seg, $spk) = ($1, $2);
  my $fname = $seg;
  $fname =~ s/(\S+_\S+)_[\d]+_.*/$1/g;
  $spk =~ s/\S+_\S+_s(.*)/$1/g;
  $spk = sprintf ("%05d",$spk+1);
  $vocab03{$seg} = $spk;
  if ( not exists $vocab01{$fname}{$spk}) {
    if (not exists $tmpvocab{$fname}) {
      $tmpvocab{$fname} = 0;
    } else {
      $tmpvocab{$fname} ++;
    }
    my $index = $tmpvocab{$fname};
    $vocab01{$fname}{$spk} = $index;
    $vocab02{$fname}{$index} = $spk;
  }
}
if (0) {
foreach my $fname (keys %vocab01) {
  my $pv = $vocab01{$fname};
  if ($fname eq "malaych3tv1_20140501") {
    foreach my $spk (keys %$pv){
      print "$fname, $spk, $$pv{$spk}\n";
    }
  }
}
}
close F;
my %vocab = ();
open (F, "$segments") or die "file $segments cannot open\n";
while (<F>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/ or next;
  my ($seg, $fname, $start, $end) = ($1, $2, $3, $4);
  my @C = ($start, $end);
  my $spk = $vocab03{$seg};
  die "$_ has no spk info\n"  if not defined $spk;
  my $index = $vocab01{$fname}{$spk};
  die "$_ has no spk index\n" if not defined $index;
  if (not exists $vocab{$fname}{$index}) {
    my @A = ();
    $vocab{$fname}{$index} = \@A;
    my $ap = $vocab{$fname}{$index};
    push @$ap, \@C;
  } else {
    my $ap = $vocab{$fname}{$index};
    push @$ap, \@C;
  }
}

if (0) {
foreach my $fname (keys %vocab) {
  next if $fname ne "malaych3tv1_20140501";
  my $ap = $vocab{$fname};
  foreach my $index (keys %$ap) {
    my $ap2 = $$ap{$index};
    for (my $i = 0; $i < @$ap2; $i ++) {
      my $ap3 = $$ap2[$i];
      print "($index, $$ap3[0], $$ap3[1]) ";
    }
    print "\n";
  }
}
exit 1;
}
close F;
sub Overlap {
  my ($ap, $start, $end) = @_;
  my ($start1, $end1) = ($$ap[0], $$ap[1]);
  if ($end <= $start1 || $start >= $end1) {
    return 0;
  }
  my $s = $start;
  $s = $start1 if ($start1 > $start);
  my $e = $end;
  $e = $end1 if ($end1 < $end);
  return $e - $s;
}
sub FindSpkIndex {
  my ($fname, $start, $end, $vocab) = @_;
  die "file name $fname does not exist\n" if not exists $$vocab{$fname};
  my $ap = $$vocab{$fname};
  my $find_index = -1; my $max_overlap = ($end - $start)/2;
  foreach my $index ( keys %$ap) {
    my $ap2 = $$ap{$index};
    my $overlap = 0;
    for(my $j = 0; $j < @$ap2; $j ++) {
      my $ap3 = $$ap2[$j];
      $overlap += Overlap($ap3, $start, $end);
      # print "file=$fname, start1=$$ap3[0], end1=$$ap3[1], start=$start, end=$end, overlap=$overlap\n";
     #  print "overlap=$overlap, max_overlap=$max_overlap\n" if $overlap > 0 ;
    }
    if ($overlap > $max_overlap) {
      $find_index = $index;   $max_overlap = $overlap;
    }
  } 
   die  "no overlap occurred !\n" if $find_index == -1;
  return $find_index;
}


print STDERR "stdin expected\n";
open (OF1, ">$new_utt2spk") or die "file $new_utt2spk cannot open\n";
open (OF2, ">$segments_map") or die "file $segments_map cannot open\n";
while (<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($seg, $text) = ($1, $2);
  my $fname = $seg ; $fname =~ s/(\S+_\S+)_[\d]+_.*/$1/g;
  my @A = split ("_", $seg); my $n = @A -1;
  my ($start, $end) = ($A[$n-1]/1000, $A[$n]/1000 );
  # die "$fname,start=$start, end=$end, $_\n";
  my $index = FindSpkIndex ($fname, $start, $end, \%vocab);
  my $spk = $vocab02{$fname}{$index};
  die "spk $spk is not found \n" if not defined $spk;
  my $spk1 = sprintf ("%s_%s", $fname, $spk);
  my $seg1 = sprintf ("%s_%07d_%07d", $spk1, $A[$n-1]/10, $A[$n]/10);
  print OF1 "$seg1 $spk1\n";
  print OF2 "$seg $seg1\n";
}
print STDERR "stdin ended\n";
close OF1; close OF2;
