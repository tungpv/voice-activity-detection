#!/bin/bash

echo -e "\n## LOG: $0 $@\n" >&2 ;

if [ $# -ne 2 ]; then
  echo -e "\n\nUsage: $0 src_utt2spk utt_name_map\n\n" >&2 ; exit 1
fi

utt2spk=$1; shift;
mapfile=$1; shift;

for f in $utt2spk $mapfile; do
  if [ ! -f $f ]; then
    echo -e "\n$0 file $f is not there\n" >&2; exit 1
  fi
done

cat $utt2spk | \
perl -e '($fname) =@ARGV; open (F, "<$fname") or die "file $fname cannot open\n"; 
  while(<F>) {
    chomp;  m/(\S+)\s+(\S+)/ or next;  $vocab{$1} = $2;
  } close F; 
  while (<STDIN>) {
    chomp;
    m/(\S+)\s+(\S+)/ or next;  ($uname, $spkname) = ($1, $2);
    $spkname =~ s/[.+-]//g; $spkname = lc $spkname;
    if (exists $vocab{$uname}) {
     print $vocab{$uname}, " ", $spkname, "\n";
    } else {
      print STDERR "## WARNING: oov $_\n";
    } 
  }
  ' $mapfile
