#!/usr/bin/env perl
use strict;
use warnings;

my $argNum = scalar @ARGV;

die "\n\nUsage Example: cat sel.txt | $0 1-3:3-10:10-  words.txt \n\n" if $argNum != 2;

my ($binstr, $words) = @ARGV;

sub Str2Bin {
  my ($str, $array) = @_;
  my @A = split (/:/, $str);
  for(my $i = 0; $i < @A; $i ++) {
    my $s = $A[$i]; my ($a, $b);
    if ($s =~ /(.*)-(.*)/) {
      ($a, $b) = ($1, $2);
    } elsif ($s =~ /(.*)-$/) {
      ($a, $b) = ($1, "INF");
    } elsif ($s =~ /^-(.*)/) {
      ($a, $b) = ("INF", $1);
    } else {
      die "unidentified string $str, $s\n";
    }
    my @ta = ();
    push @ta, $a; push @ta, $b;
    push @$array, \@ta;
  }
}

sub Insert {
  my ($key, $val, $vocab) = @_;
  if (exists $$vocab{$key}) {
    my $pA = $$vocab{$key};
    push @$pA, $val;
    return; 
  }
  my @A = ();
  $$vocab{$key} = \@A;
  my $pA = $$vocab{$key};
  push @$pA, $val;
  return; 
}

sub FillBin {
  my ($str, $array, $vocab) = @_;
  my @A = split (/\s+/, $str);
  my $count = pop @A; 
  my $s = join " ", @A; $s =~ s/^\s//g; $s =~ s/\s$//g;
  for(my $i = 0; $i< scalar @$array; $i++) {
     my $pA = $$array[$i];
    if ($$pA[0] =~ /^[0-9]/) {
      if ($$pA[1] =~ /^[0-9]/) {
        if ( $$pA[0] <= $count && $count < $$pA[1]) {
          my $key = sprintf "%d %d", $$pA[0], $$pA[1];
          Insert ($key, $s, $vocab);
        }
      } else {
        if ($count >=$$pA[0]) {
          my $key = sprintf "%d INF", $$pA[0];
          Insert ($key, $s, $vocab);
        }
      }
    } else {
      if ($count < $$pA[1]) {
        my $key = sprintf "INF %d", $$pA[1];
        Insert ($key, $s, $vocab);
      }
    }
  }
}

sub LoadDict {
  my ($fName, $vocab) = @_;
  open (F, "$fName") or die "file $fName cannot open\n";
  while (<F>) {
    chomp;
    m/(\S+)\s+(.*)/ or next;
    $$vocab{$1} ++;
  }
  close F;
}
# ---------- main entrance function -----
my @A = ();
Str2Bin($binstr, \@A);
my %dict = ();
LoadDict($words, \%dict);

print STDERR "stdin expected\n";
my %vocab = ();
while (<STDIN>) {
  chomp;
  FillBin ($_, \@A, \%vocab);
}
print STDERR "stdin ended\n";
foreach my $key (keys %vocab) {
  my $pVal = $vocab{$key};
  my $total = scalar @$pVal;
  my $oovNum = 0; 
  for (my $i = 0; $i < $total; $i ++ ) {
    my $s = $$pVal[$i];
    my @A = split (/\s/, $s);
    for (my $j = 0; $j < @A; $j ++ ) {
      my $w = $A[$j];
      if (not exists $dict{$w}) {
        $oovNum ++; last;
      }
    }  
  }
  print "$key\t$total $oovNum\n";
}

