#!/usr/bin/env perl 
use warnings;
use strict;
if (@ARGV != 2 ) {
  die "\nfile name expected\n\n";
}
my ($path,$index) = @ARGV;
my @A1 =  split ("/", $path);
my $s1 = \$A1[$index+1];
if ($$s1 eq "UM_broadcast") {
  $$s1 = "uma01";
} elsif ($$s1 eq "MalaysiaVarious") {
  $$s1 = "uma03";
} elsif ($$s1 eq "UM_broadcast2") {
  $$s1 = "uma02";
} elsif ($$s1 eq "Suria"){
  $$s1 = "sur01"
} else {
  die "unidentified tv station $path: $$s1\n";
}
 $s1 = \$A1[$index];
if ($$s1 eq "tdp") {
  $$s1 = "tdp01";
}elsif ($$s1 eq "tdp2") {
  $$s1 = "tdp02";
} elsif ($$s1 eq "magor") {
  $$s1 = "mag01";
} else {
  die "unidentified project name $path: $$s1\n";
}
my $prefix = sprintf "%s_%s",$A1[$index],$A1[$index+1]; 

my $name = $A1[@A1-1];
$name=~ s/[-]//g; 
$name =~ s/\.wav//g;
my @A = split ("_",$name);

my $len = scalar @A;

my $s = $A[$len -1];

if ($s !~ /(\d+)/ ) {
   $A[$len -1] = $A[$len -2];
   $A[$len -2] = $s;
   $s = $A[$len -1];
}
if ($s !~ /(\d+)/) {
  die "\nunidentified string $path\n";
}
$s = \$A[$len-1];

$$s = sprintf "%05d.wav", $$s+1;
if (length($A[$len-2])<2) {
  $A[$len-3] .=  $A[$len-2];
  splice @A, $len -2, 1; $len --;
}
$s = \$A[$len-2];
while (length ($$s) < 7) {
  $$s = sprintf "0%s", $$s; 
}
my $newName = join "_", @A;
$newName = sprintf "%s_%s", $prefix, $newName;

print lc $newName;
