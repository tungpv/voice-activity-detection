#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl;
nj=10;
step=0
substep=0
prepare_data=false
lang=data/llp/lang; lang_test=${lang}_test
samples_per_iter=4000000;  
max_mem=500000000;
state=2500;gauss=20000
pnorm_input_dim=3000; pnorm_output_dim=500; 
nonspeech_words=$lang/nonspeech_words.txt

# end tunable variable
bn_am_cf=conf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";

alidir=exp/llp/plp_pitch/bn/llp/s1/tri3sat_2500/semi/cutoff_0.4_0.5/bnf/ntrain;
nj=`cat $alidir/num_jobs`
data=data/feature/semi/cutoff_0.4_0.5/bnf/ntrain;
split_data.sh $data $nj || exit 1;
sdata=$data/split$nj
mkdir -p $dir/log 2>/dev/null
oov=`cat $lang/oov.txt`
model=$alidir/final.mdl
set e;
set -o pipefail;
if [ $step -le 0 ]; then
  echo -e "\n## ali to ctm file `hostname` `date` \n"
  dir=$alidir
  $cmd JOB=1:$nj $dir/log/get_ctm.JOB.log \
    linear-to-nbest "ark:gunzip -c $dir/ali.JOB.gz|" \
     "ark:utils/sym2int.pl --map-oov $oov -f 2- $lang/words.txt < $sdata/JOB/text |" \
     '' '' ark:- \| \
    lattice-align-words $lang/phones/word_boundary.int $model ark:- ark:- \| \
    nbest-to-ctm ark:- - \| \
    utils/int2sym.pl -f 5 $lang/words.txt \| \
    gzip -c '>' $dir/ctm.JOB.gz ||exit 1
   echo -e "\n## done `date`\n"
fi

