#!/bin/bash

. path.sh
. cmd.sh

echo -e "\n$0 $@\n"

# begin options
cmd=run.pl
max_jobs=30
valid_data_portion=0.1                      # validation data portion in frame percent
fmllr_feats=false                           # indicates whether fmllr feature is needed
cmd_ali="steps/align_fmllr.sh"              # one of kaldi alignment scripts
cmd_feats="steps/nnet/make_fmllr_feats.sh"  # kaldi script to generate fmll feature 
cmd_train="steps/nnet/train.sh"             # nnet training tool, by Karel
nn_depth=7
hid_dim=1024
learn_rate=0.008
rbm_iter=1
datadir=                                    # where temporary data, such as fmllr data , saved
alidir=                                     # alignment dir 
featdir=                                    # where temporary feature, such as fmllr, saved
# end options

. parse_options.sh

if [ $# -ne 4 ]; then
  echo  "Usage: $0 <data-train> <lang-dir> <src-dir> <exp-dir>"
  echo  "e.g.: $0 data/train  data/lang exp/tri3a exp/tri3a_nnet"
  echo ""
fi

data=$1
lang=$2
srcdir=$3
dir=$4

JobNum(){ data=$1; max=$2; nj=`wc -l < $data/spk2utt`; [ $nj -le $max ] || nj=$max; echo -n $nj;  }

for f in $data/feats.scp $srcdir/final.mdl; do
  [ ! -f $f ] && echo "no such a file $f" && exit 1
done

num_frames=`feat-to-len scp:$data/feats.scp ark,t:- | awk '{x += $2;} END{print x;}'` || exit 1;
validate_frames=`perl -e "print int $num_frames*$valid_data_portion;"`
if [[ !  $validate_frames -lt $num_frames || ! $validate_frames -gt 0 ]]; then
  echo "validate_frames is unavailable "; exit 1
fi

[ -d $dir ] || mkdir -p $dir

[ ! -z $datadir ] || datadir=`mktemp -d -p $data`
train=$datadir/train
valid=$datadir/validate
[ ! -d $alidir ] || alidir=$srcdir/ali
ali_train=$alidir/train
ali_valid=$alidir/validate

iutils/subset_spklist.pl  $validate_frames  $data/spk2utt \
"feat-to-len scp:$data/feats.scp ark,t:- |" > $dir/validate_spklist || exit 1
cut -d " " -f1 $data/spk2utt | grep -v -f $dir/validate_spklist > $dir/train_spklist

utils/subset_data_dir.sh --spk-list $dir/validate_spklist $data $valid || exit 1
utils/subset_data_dir.sh --spk-list $dir/train_spklist $data $train || exit 1

nj1=`JobNum $valid $max_jobs`
if [ ! -f $ali_valid/.done ]; then
  $cmd_ali  --nj $nj1 --cmd "$cmd"   $valid  $lang $srcdir  $ali_valid || exit 1
  touch $ali_valid/.done
fi
if [ -f $ali_valid/trans.1 ]; then
  cmd_feats_valid="$cmd_feats --nj $nj1 --transform-dir $ali_valid"
else
  cmd_feats_valid="$cmd_feats --nj $nj1"
fi
nj2=`JobNum $train $max_jobs`
if [ ! -f $ali_train/.done ]; then
  $cmd_ali  --nj $nj2  --cmd "$cmd"   $train     $lang $srcdir  $ali_train || exit 1
  touch $ali_train/.done
fi
if [ -f $ali_train/trans.1 ]; then
  cmd_feats_train="$cmd_feats --nj $nj2 --transform-dir $ali_train"
else
  cmd_feats_train="$cmd_feats --nj $nj2"
fi

if $fmllr_feats; then
  [ ! -z $featdir ] || featdir=$sdir/feature
  fmllr_train=$datadir/fmllr/train
  fmllr_feat=$featdir/fmllr/train
  if [ ! -f $fmllr_train/.done ]; then
    $cmd_feats_train --cmd "$cmd"  \
    $fmllr_train $train $srcdir $fmllr_feat/_log $fmllr_feat/_data || exit 1
    touch $fmllr_train/.done
  fi
  fmllr_valid=$datadir/fmllr/validate
  fmllr_feat=$featdir/fmllr/validate
  if [ ! -f $fmllr_valid/.done ]; then
    $cmd_feats_valid --cmd "$cmd" \
    $fmllr_valid $valid $srcdir $fmllr_feat/_log  $fmllr_feat/_data || exit 1
    touch $fmllr_valid/.done
  fi
  train=$fmllr_train
  valid=$fmllr_valid
fi

pretrain_dir=$dir/pretrain_dbn
if [ ! -f $pretrain_dir/._pretrain_done ]; then
  echo  "$0: pretraining started `date`"
  $cmd $pretrain_dir/pretrain_dbn.log \
  steps/nnet/pretrain_dbn.sh   --nn-depth $nn_depth \
  --hid-dim $hid_dim \
  --rbm-iter 1 \
  $train  $pretrain_dir || exit 1;
  touch $pretrain_dir/._pretrain_done
fi
dnn_dir=$dir/dnn
feature_transform=$pretrain_dir/final.feature_transform
dbn=$pretrain_dir/$nn_depth.dbn
if [ ! -f $dnn_dir/.dnn_done ]; then
  echo "$0: dnn training started `date`"
  $cmd $dnn_dir/_train_nnet.log \
  $cmd_train  --feature-transform $feature_transform \
  --dbn $dbn \
  --hid-layers 0  \
  --learn-rate $learn_rate \
  $train $valid $lang $ali_train $ali_valid $dnn_dir || exit 1; 
  touch $dnn_dir/.dnn_done
fi

