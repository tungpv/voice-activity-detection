#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Log {
  echo -e "\n`date`:$1 @ `hostname`\n";
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
doit=false;
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if [ $stage -le 1 ]; then
  Log "decode the data";
  data=data/107/llp/bn/semi/semi0.5;
  dname=`basename $data`;
  sdir=exp/bn_semi0.5/tri3sat;
  graph=$sdir/graph;
  nj=30;
  decode_cmd=run.pl;
  steps/decode_fmllr.sh --nj $nj \
  --cmd "$decode_cmd" \
  $graph $data  $sdir/decode_$dname || exit 1;
fi
# get utterance posterior
if [ $stage -le 2 ]; then
  Log "get utterance confidence list";
  lang=data/107/llp/lang_dialect_final;
  latdir=exp/bn_semi0.5/tri3sat/decode_semi0.5;
  mdldir=$latdir/..;
  dir=$latdir/post;
  log=log/utt_conf.log
  nj=`cat $latdir/num_jobs`;
  silence_opts="--silence-label=1";
  sbatch -p speech -n$nj -o $log \
  isteps/um/utt_conf.sh --make-utt-conf true \
  --confdir "$latdir/uconf_rem_nsp" \
  --non-speech "$lang/non_speech.txt" \
  --cmd "$train_cmd" \
  --silence-opts "$silence_opts" $latdir $lang \
  $mdldir $dir
fi
# select utterance with higher confidence score
if [ $stage -le 3 ]; then
  Log "select utterances with higher confidence score";
  seg=data/107/llp/bn/semi/semi0.5/segments;
  prefix=exp/bn_semi0.5/tri3sat/decode_semi0.5;
  uconf="gzip -cdf $prefix/uconf_rem_nsp/uconf.*.gz|";
  dir=$prefix/selected
  [ -e $dir/selected.log ] && rm $dir/selected.log
  [[ -d $dir ]] || mkdir -p $dir;
  for c in `seq 0.1 0.1 0.9`; do
    selSeg=$dir/segments_${c};
    iutils/um/conf_utt_select.pl --thresh $c \
    --selected $selSeg "$uconf" $seg  2>>$dir/selected.log
  done
  # 
fi

# make kaldi training data
if [ $stage -le 4 ]; then
  Log "format data";
  prefix=exp/bn_semi0.5/tri3sat/decode_semi0.5; 
  sup_seg=data/107/llp/bn/train_semi0.5/segments;
  semi_seg=$prefix/selected/segments_0.5;
  unsup_segments=$prefix/selected/unsup_segments
  unsup_uconf=$prefix/selected/unsup_uconf.txt
  merged_uconf=$prefix/selected/merged_uconf.txt
  dir=data/107/llp/bn/semi/trans_updated_semi0.5
  thresh=0.5
  uconf="gzip -cdf $prefix/uconf_rem_nsp/uconf.*.gz|";
  iutils/um/make_unsup_uconf.pl "$uconf" $sup_seg $thresh \
  $unsup_uconf
  iutils/um/make_unsup_seg.pl $semi_seg $sup_seg $unsup_segments
  [ -e $merged_uconf ] && rm $merged_uconf
  iutils/um/make_merged_uconf.pl $sup_seg $unsup_uconf \
  |sort > $merged_uconf
  # merge the data to train sgmm or whatever
  lang=data/107/llp/lang_dialect_final;
  fprefix=data/107/llp/bn;
  data=$fprefix/train_semi0.5;
  udata=$fprefix/semi/semi0.5;
  asr_trans=$prefix/scoring/20.tra;
  isteps/um/merge_unsup_data.sh --segments $unsup_segments \
  --lang $lang \
  $asr_trans $udata $data $dir
  [ -e $dir/uconf.txt ] || cp $merged_uconf $dir/uconf.txt
    
fi
