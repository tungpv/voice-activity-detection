#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
doit=false;
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if [ $stage -le 1 ] ; then
  echo -e "copy data";
  sdir=language/107_FLP/data/srs_bn_0413/train;
  dir=data/107/llp/flp_train;
  [ -d $dir ] || mkdir -p $dir;
  for x in reco2file_and_channel  spk2utt  text \
     segments  utt2spk; do
    [ -e $dir/$x ] ||cp $sdir/$x $dir
  done
 # copy wav.scp separately.
 [ -e $dir/wav.scp ] || \
 cp swordfish/107/data/train/wav.scp  $dir
 sdir=$dir;
 dir=data/107/llp/flp_train_nn;
 exclude=data/107/llp/fbank/train_dev/segments
 isteps/um/mksubset_dir.sh --exclude $exclude \
  $sdir  $dir 10000;
fi
# make bottleneck feature
if [ $stage -le 2 ]; then
  echo -e "\nprepare bn feature\n";
  nj=20;
  train_cmd=run.pl;
  sdata=data/107/llp/flp_train_nn;
  xname=`basename $sdata`;
  dir=data/107/llp/fbank/$xname;
  mkdir -p $dir;
  cp $sdata/*  $dir;

  prefix=data/107/llp/bn;
  nnetdir=exp/fbank/bn2;
  for x in $xname; do
    # make fbank feature first
    sdata=data/107/llp/fbank/$x;
    steps/make_fbank.sh --cmd "$train_cmd" --nj $nj \
    $sdata $sdata/_log $sdata/_data || exit 1;
    steps/compute_cmvn_stats.sh $sdata $sdata/_log $sdata/_data
    
    data=$prefix/$x;
    steps/make_bn_feats.sh --cmd "$train_cmd" \
    --nj $nj $data $sdata $nnetdir $data/_log \
    $data/_data || exit 1;
    # compute CMVN of the BN-features
    steps/compute_cmvn_stats.sh $data $data/_log \
    $data/_data || exit 1;
  done

fi
# forced alignment on
# the flp_train_nn data
if [ $stage -le 3 ]; then
  echo -e "\n foced alignment at `date`\n";
  nj=10;
  prefix=data/107/llp/fbank;
  mdldir=exp/fbank/tri3sat;
  lang=data/107/llp/lang_dialect_final;
  for x in flp_train_nn; do
    alidir=${mdldir}_ali_$x;
    steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
    $prefix/$x $lang $mdldir $alidir || exit 1; 
  done
fi
# train bottleneck neural network
if [ $stage -le 4 ]; then
  echo -e "\ntrain bottleneck neural network\n";
  conf=configs/bn1.config;
  lang=data/107/llp/lang_dialect_final;
  mdldir=exp/fbank/tri3sat;
  train_nn=data/107/llp/fbank/flp_train_nn;
  xname=`basename $train_nn`;
  train_dev=data/107/llp/fbank/train_dev;
  bn1dir=exp/fbank/bn1_$xname;
  bn2dir=exp/fbank/bn2_$xname;
  isteps/tandem/train_bn1.sh --configs $conf \
  --stage 1 \
  --fmllr-align true \
  $lang $mdldir $train_nn $train_dev $bn1dir $bn2dir
fi
# re-generate the bottleneck feature
# only on the supervised part
if [ $stage -le 5 ] ; then
  echo -e "\nregenerate the bn feature \n";
  nj=20;
  train_cmd=run.pl;
  prefix=data/107/llp/bn;
  xname=flp_train_nn;
  nnetdir=exp/fbank/bn2_$xname;
  for x in train dev; do
    sdata=data/107/llp/fbank/$x;
    data=$prefix/${x}_$xname;
    isteps/make_bn_feats.sh --cmd "$train_cmd" \
    --nj $nj $data $sdata $nnetdir $data/_log \
    $data/_data || exit 1;
    # compute CMVN of the BN-features
    steps/compute_cmvn_stats.sh $data $data/_log \
    $data/_data || exit 1;
  done
fi
# retrain the system using
# the updated bottleneck feature
if [ $stage -le 6 ] ;then
  echo -e "\nretrain the system with updated bn feature\n";
  nj=30;
  stage=1;
  xname=flp_train_nn;
  config=configs/bn_$xname.config;
  log=./log/train_bn_${xname}_regular.log;
  sbatch -p speech -n$nj -o $log \
  isteps/tandem/train_regular_am.sh \
  --stage $stage \
   $config
fi

