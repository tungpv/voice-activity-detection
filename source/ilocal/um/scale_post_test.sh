#!/bin/bash

. path.sh
data=data/107/llp/bn/semi/trans_updated_semi0.5;
dir=exp/bn_semi0.5/tri3sat/ali_trans_updated_semi0.5;
post_scale=$data/uconf.txt;
ali-to-post "ark:gunzip -c $dir/ali.1.gz|" ark,t:- | \
scale-post ark:-  ark:$post_scale ark,t:- | head
