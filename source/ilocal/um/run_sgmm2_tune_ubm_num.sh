#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\ninput: $0 $@\n";

# begin user-defined varialbe
ubmixtures=400;
pdfs=2800;
sstates=3500;
nj=20;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

data=$1; shift;
alidir=$1; shift;
ubmdir=$1; shift;
dir=$1; shift;

steps/train_ubm.sh --cmd "$train_cmd" \
$ubmixtures  $data  data/lang $alidir $ubmdir || exit 1;

steps/train_sgmm2.sh --cmd "$train_cmd" \
$pdfs $sstates  $data  data/lang $alidir \
$ubmdir/final.ubm $dir || exit 1;

utils/mkgraph.sh data/lang_test $dir  $dir/graph

steps/decode_sgmm2.sh --nj $nj  --cmd "$decode_cmd" \
--transform-dir  exp/tri3b/10h/decode_dev3h \
$dir/graph data/dev3h  $dir/decode_dev3h



