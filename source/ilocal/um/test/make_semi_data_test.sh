#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Log {
  echo -e "\n`date`:$1 @ `hostname`\n";
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
cmd=run.pl;
doit=false;
mixdata=false;     # if mixdata is true, then it means udata in the following
                   # is composed of supervised and unsupervised data simultaneously
cutoff=0;     # threshold to select decode utterance
mdldir=;
utext=;
uconf=;
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

lang=data/107/llp/lang_dialect_final;
sdata=data/107/llp/bn/train_semi0.5;

if [ $stage -le 1 ]; then
  udata=data/107/llp/bn/semi/semi0.5;
  ulat_dir=exp/bn_semi0.5/tri3sat/decode_semi0.5;
  uasr_trans=exp/bn_semi0.5/tri3sat/decode_semi0.5/scoring/20.tra;
  cutoff=0;
  inv_ascale=20;
  semidir=semi_test$cutoff;
  [ -d $semidir/log ] || mkdir -p $semidir/log;
  sbatch -p speech -n30 -o $semidir/log/semi_mixdata.log \
  isteps/um/make_semi_data.sh --cmd "$train_cmd" \
  --mixdata true \
  --inv-ascale $inv_ascale \
  --cutoff $cutoff \
  --stage 1 \
  $lang $udata $ulat_dir $sdata $uasr_trans $semidir
fi

# make BN feature
if [ $stage -le 2 ]; then
  echo -e "\nmaking BN feature `hostname` `date`\n";
  
fi
