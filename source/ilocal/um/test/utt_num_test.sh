#!/bin/bash
include=
hlen=
sdir=data/107/llp/bn/semi014sgmm2
upost_list=$sdir/uconf.txt
exclude=data/107/llp/fbank/train_dev/segments;
upost_thresh=0.4
dir=subtmp
[ -d $dir ] || mkdir $dir; rm $dir/* 2>/dev/null

doit=false;

if $doit; then
  iutils/um/utt_num.pl ${exclude:+ --exclude $exclude}  \
  ${include:+ --include $include} \
  ${upost_list:+ --upost-list $upost_list} \
  ${upost_thresh:+ --upost-thresh $upost_thresh} \
  --segment $sdir/segments ${hlen:+ --hlen $hlen} \
  --utt2spk $sdir/utt2spk --destdir $dir
fi
if $doit; then
  isteps/um/mksubset_dir.sh \
  ${exclude:+ --exclude $exclude} \
  ${upost_list:+ --upost-list $upost_list} \
  ${upost_thresh:+ --upost-thresh $upost_thresh} \
  $sdir $dir
fi
# 
sdir=data/107/llp/bn/semi014sgmm2/fbank
isteps/um/mksubset_dir.sh \
  ${exclude:+ --exclude $exclude} \
  ${upost_list:+ --upost-list $upost_list} \
  ${upost_thresh:+ --upost-thresh $upost_thresh} \
  $sdir $dir

