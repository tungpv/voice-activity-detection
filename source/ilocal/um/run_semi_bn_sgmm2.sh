#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}

function Wait {
  [[ $# -eq 1 ]] || \
  die "waiting partner expected";
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";

echo -e "\n$0 $@\n";

# begin user-defined variable
doit=false;
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if [ $# -lt 1 ]; then
  echo -e "\n$0 config_file\n"; exit 1;
fi
config=$1; shift;

[[ -e $config ]] || \
die "config file is not ready";

. $config;

[ -d $semidata/log ] || mkdir -p $semidata/log;

# make semi data
if [ $stage -le 1 ]; then 
  sbatch -p speech -n30 -o $semidata/log/semi.log \
  isteps/um/make_semi_data.sh --cmd "$train_cmd" \
  --inv-ascale $inv_ascale \
  --cutoff $cutoff \
  --stage 1 \
  $lang $udata $ulatdir $sdata $stt_label_udata $semidata
  Wait $semidata/.done
fi

# prepare bn feature
if [ $stage -le 3 ]; then
  echo -e "\nprepare bn feature\n";
  [ -d $semidir ] || mkdir -p $semidir;
  sbatch -p speech -o $semidir/sbatch.log -n$nj \
  isteps/imake_bn_feats.sh \
  --nj $nj \
  --fbfeature true \
  $semidir $semidata $bnnetdir $semidir/_log $semidir/_data
  Wait $semidir/.done;
  [ -e $scale_post ] || die "scale_post $scale_post is not generated";
fi
# make alignment data 
if [ $stage -le 5 ]; then
  echo -e "\nmake alignment\n";
  steps/align_fmllr.sh --nj $nj \
  --cmd "$train_cmd" $semidir $lang $alimdldir $alidir
fi
# 
if [ $stage -le 7 ]; then
  echo -e "\ntrain ubm `date`\n";
  steps/train_ubm.sh --cmd "$train_cmd" \
  $ubm_num  $semidir $lang $alidir $ubmdir 
fi
# train subspace gmm
if [ $stage -le 9 ]; then
  echo -e "\ntrain subspace gmm `date`\n";
  isteps/um/train_sgmm2.sh --cmd "$train_cmd" \
  --scale-post $scale_post \
  $nstate $nsubstate $semidir $lang $alidir \
  $ubmdir/final.ubm $sgmm2dir  || exit 1;
fi
#  evaluation on development data
if [ $stage  -le 11 ] ;then
  echo -e "\nsgmm2 decode\n";
  if [ ! -e $sgmm2dir/graph/HCLG.fst ]; then
    utils/mkgraph.sh $test_lang $sgmm2dir $sgmm2dir/graph;
  fi
  train_cmd=run.pl;
  isteps/i2decode_sgmm2.sh --nj $nj \
  --cmd $train_cmd \
  ${transform_dir:+ --transform-dir $transform_dir} \
  $sgmm2dir/graph $devdata $sgmm2dir/decode_${devname}_nosfmllr
fi
