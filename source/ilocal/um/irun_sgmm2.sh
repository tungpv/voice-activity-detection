#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\ninput: $0 $@\n";

# begin user-defined varialbe
nj=20;
tune_ubm_num=false;
tune_pdf_num=false;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if $tune_ubm_num; then
  x=10h
  ubm_array="250 300 350 400 450 500 550 600";
  data=data/train_10h
  alidir=exp/tri3b/10h/train_10h_ali
  for ubm in $ubm_array; do
    echo "tune ubm $ubm";
    prefix=exp/sgmm2
    ubmdir=$prefix/$x/ubm_$ubm
    dir=$prefix/$x/sgmm_tune_ubm_$ubm
    logdir=log/$x;
    [[ -d $logdir ]] || mkdir -p $logdir
    logfile=$logdir/tune_ubm_$ubm.log
    sbatch -p speech -n$nj -o $logfile \
    ilocal/um/run_sgmm2_tune_ubm_num.sh  --ubmixtures $ubm \
    $data $alidir $ubmdir $dir 
  done
fi
# tune pdf number 
if $tune_pdf_num; then
  x=10h; ubm=600
  pdf_array="3500 4200 4900 5600 7000 7700";
  data=data/train_10h
  alidir=exp/tri3b/10h/train_10h_ali
  prefix=exp/sgmm2/$x
  ubmdir=$prefix/ubm_$ubm
  for pdf in $pdf_array; do
    sstates=`echo $pdf | awk '{ x=$0; y=x*1.4; d =int(y/1000);  y = d*1000; print y; }'`
    dir=$prefix/sgmm_tune_pdf_$ubm_$pdf
    logdir=log/$x;
    [[ -d $logdir ]] || mkdir -p $logdir
    echo "pdf=$pdf, sstates=$sstates";
    logfile=$logdir/tune_pdf_$pdf.log
    sbatch -p speech -n$nj -o $logfile \
    ilocal/um/run_sgmm2_tune_pdf_num.sh  --ubmixtures $ubm \
    --pdfs $pdf --sstates $sstates \
    $data $alidir $ubmdir $dir
  done
fi
