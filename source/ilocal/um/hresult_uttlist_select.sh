#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\ninput: $@\n";

# begin user-defined varialbe
min_thresh=1.0;
doit=false;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if [ $# -ne 4 ]; then
  echo -e "\n\n";
  echo "Usage:\n hresult_uttlist_select.sh supdata data source-data dir";
  echo -e "\n\n";
  exit 1;
fi

supdata=$1; shift;
data=$1; shift;
sdata=$1; shift;
dir=$1; shift;

[ -e $data/text ] || die "text file in folder $data is not ready ";
[ -e $sdata/text  ] || die "text file in folder $sdata is not ready ";
mlfdir=$dir/htk_mlf
mkdir -p $mlfdir

iutils/um/kaldi_text_to_mlf.pl $data/text "rec" $mlfdir/rec.mlf 
iutils/um/kaldi_text_to_mlf.pl $sdata/text "lab" $mlfdir/ref.mlf
[ -e $mlfdir/listfn ] && rm -f $mlfdir/listfn
echo > $mlfdir/listfn <<EOF
a
b
c
d
EOF

if $doit; then
  [ ! -e $mlfdir/results.txt ] || rm -f $mlfdir/results.txt
  HResults -f -I $mlfdir/ref.mlf  $mlfdir/listfn $mlfdir/rec.mlf > $mlfdir/results.txt

  iutils/um/filter_hresults.pl $mlfdir/results.txt  $mlfdir/results_reorg.txt
fi

[ -e $data/segments ] || die "segments file $segments does not exist";
accs=(20 30 35 40 50 60 70 80 90) ;
words=(1 2 3 4);

num1=${#accs[@]};
num2=${#words[@]};
i=0; 

# utterance list dir
udir=$dir/utt_subset
mkdir -p $udir 2>/dev/null
tinfo=$udir/time_len.txt;
[ ! -e $tinfo ] || rm -f $tinfo
while [ $i -lt $num1 ]; do
  acc=${accs[$i]};
  j=0;
  while [ $j -lt $num2 ]; do
    wnum=${words[$j]};
    echo -e "select utterance with acc=$acc and length=$wnum";
    cdir=$udir/${acc}_${wnum}
    mkdir -p $cdir 2>/dev/null
    cseg=$cdir/segments1
    iutils/um/cond_utt_stats.pl $acc $wnum $mlfdir/results_reorg.txt $data/segments $cseg >> $tinfo
    echo utterance selected:`tail -1 $tinfo`
    [ -e $cseg ] || die "no output segments";
    hour_len=`tail -1 $tinfo | awk '{ print $NF; }'`
    if  [ `echo $hour_len  '>'  $min_thresh| bc -l` -gt 0 ]  ; then
      echo -e "\n\nhour_len=$hour_len\n\n";
      iutils/um/subset_data_dir.sh --uttlist $cseg $data $cdir || \
      die "subset_data_dir.sh failed, check folder $data and $cdir";
      mdir=$dir/merge/${acc}_${wnum}
      iutils/um/merge_data_dir.sh $supdata $cdir $mdir
    else
      # we remove the whole folder, which means we
      # do not merge smaller auto-labeled data with the supervised data
      rm -rf $cdir
    fi
    j=$[$j+1];
  done
  i=$[$i+1];
done

