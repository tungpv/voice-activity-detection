#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n$0 $@\n";
# begin user-defined variable
doit=false;
stage=0;
nj=30;
scale_post=;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

# if [[  $# -ne 4 ]]; then
#   echo -e "\nUsage:$0 config \n";
#   echo -e "data alidir exp_dir\n"; exit -1;
# fi
conf=$1; shift;
. $conf || die "config file $conf does not exist";

echo @`hostname` on `date`;

# data=$1; shift;
# alidir=$1; shift;
# exp_dir=$1; shift;

if [ $stage -le 1 ]; then
  if [ ! -z $scale_post ] ;then
    isteps/um/train_sgmm2.sh --cmd "$train_cmd" \
    --scale-post $scale_post \
    $nstate $nsubstate $data $lang $alidir \
    $ubmdir/final.ubm $exp_dir  || exit 1;
  else
    steps/train_sgmm2.sh --cmd "$train_cmd" \
    $nstate $nsubstate $data $lang $alidir \
    $ubmdir/final.ubm $exp_dir  || exit 1;
  fi
fi

# evaluate the trained models
if [ $stage -le 2 ] ;then
  lattice_beam=8.0;
  train_cmd=run.pl;
  if [ -e $exp_dir/final.mdl ]; then 
    if [ ! -e $exp_dir/graph/HCLG.fst ]; then
      utils/mkgraph.sh $lang_test $exp_dir $exp_dir/graph;
    fi
    steps/decode_sgmm2.sh --nj $nj \
    --cmd "$train_cmd" --transform-dir $satdecdir \
    --lattice-beam $lattice_beam \
    $exp_dir/graph $dev_data $exp_dir/decode_${devname}_latbeam$lattice_beam;
  fi
fi
