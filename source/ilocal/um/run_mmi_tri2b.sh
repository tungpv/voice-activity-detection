#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\ninput: $@\n";

# begin user-defined varialbe

nj=20
boost=0.5
doit=false;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

data=$1; shift;
dataname=`basename $data`
mdldir=$1; shift;
alidir=$1; shift;
denlatdir=$1; shift;
dir=$1; shift;

if $doit; then
# Train and test MMI (and boosted MMI) on tri2b system.
steps/make_denlats.sh --nj $nj --cmd "$train_cmd" \
  $data data/lang $mdldir $denlatdir || exit 1;

# train the basic MMI system.
steps/train_mmi.sh --cmd "$train_cmd" --boost $boost \
  $data  data/lang $alidir \
  $denlatdir  $dir  || exit 1;
fi

for iter in 4; do
  steps/decode_si.sh --nj $nj --cmd "$decode_cmd" --iter $iter \
    $mdldir/graph data/dev3h $dir/decode_dev3h_it$iter 
done


