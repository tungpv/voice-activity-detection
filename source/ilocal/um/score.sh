#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. ./cmd.sh ## You'll want to change cmd.sh to something that will work on your system.
           ## This relates to the queue.

. ./path.sh ## Source the tools/utils (import the queue.pl)

# begin user-defined variable
cmd=slocal/slurm.pl
scoring_opts=

# end user-defined variable


. parse_options.sh || die "parse_options. sh expected";

data=$1; shift;
graphdir=$1; shift;
dir=$1; shift;
. path.sh 
local/score.sh --cmd "$cmd" $scoring_opts $data $graphdir $dir
