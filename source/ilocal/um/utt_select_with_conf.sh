#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\ninput: $@\n";
# begin user-defined variable
cmd=run.pl;
doit=false;
min_utt_len=2;  # minimum word number should be over 2
min_hour_len=4.0; # selected data set should be over this time-length value
# end  user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if [[ $# -ne 3 ]]; then
  echo -e "\n\nCalculate utterance confidence based on";
  echo "lattice, then use a threshold over the"; 
  echo "confidence score to select";
  echo "utterance list, which will be merged into the";
  echo -e "source data to do unsupervised training.\n";
  echo "Example: utt_select_with_conf.sh overall-ASR-labeled data folder \\";
  echo -e  "     corresponding-lattice-folder objective-folder \n\n";
  exit -1;
fi

data=$1; shift;
[[ -e $data/text && -e $data/segments ]] || \
die "text or segments file not seen in data $data";
sdir=$1; shift;
dir=$1; shift;

nj=`cat $sdir/num_jobs`|| die "num_jobs expected";

cdir=$sdir/utt_conf
logdir=$cdir/log
mkdir -p $logdir 2>/dev/null

if $doit; then
  $cmd JOB=1:$nj $cdir/log/utt_conf.JOB.log  \
      lattice-best-path-post --inv-acoustic-scale=13 "ark:gzip -cdf $sdir/lat.JOB.gz|" \
        "ark,t:|gzip -c >$cdir/uttconf.JOB.gz" || exit 1;
  Exit;
fi
seldir=$dir/select;
[[ -d $seldir ]] || mkdir -p  $seldir;
[[ -e $seldir/time_len.txt ]] && rm -f $seldir/time_len.txt
[[ -e $cdir/uttconf.txt ]] && rm -f $cdir/uttconf.txt 
gzip -cd $cdir/uttconf.*.gz > $cdir/uttconf.txt

xseq=(0.01 0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9);
idx=0;
xseq_len=${#xseq[@]};
while [[ $idx -lt $xseq_len ]]  ; do
  x=${xseq[$idx]}; 
  xdir=$seldir/conf_$x
  [ -d $xdir ] || mkdir -p $xdir
  iutils/um/select_utt_with_ulen_conf.pl  $x  $min_utt_len $cdir/uttconf.txt $data/segments $data/text  $xdir/seg  >> $seldir/time_len.txt
 hlen=`tail -1 $seldir/time_len.txt |awk '{print $NF;}'`
 if [ ! -z $hlen ]; then
   if [[ `echo $hlen '>=' $min_hour_len |bc -l` -gt 0 ]]; then
     iutils/um/subset_data_dir.sh --uttlist $xdir/seg $data $xdir
   else
     rm -rf $xdir 2>/dev/null    
   fi
 fi
 idx=$(( $idx + 1 ));
done
