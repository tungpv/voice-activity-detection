#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\ninput: $@\n";

# begin user-defined varialbe

nj=20
doit=false;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

state=$1; shift;
gaussian=$1; shift;
data=$1; shift;
dataname=`basename $data`
lang=$1; shift;
ali=$1; shift;
dir=$1; shift;

if $doit; then
steps/train_sat.sh --cmd "$train_cmd" \
  $state  $gaussian  $data  data/lang $ali  $dir || exit 1;

utils/mkgraph.sh data/lang_test  $dir  $dir/graph || exit 1;

steps/decode_fmllr.sh --nj $nj --cmd "$decode_cmd" \
$dir/graph  data/dev3h $dir/decode_dev3h || exit 1;
fi

steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
  $data data/lang $dir $dir/${dataname}_ali || exit 1;


