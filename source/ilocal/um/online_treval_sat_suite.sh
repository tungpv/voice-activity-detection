#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\ninput: $@\n";

# begin user-defined varialbe
leaves=4000
gaussians=48000
nj=20
doit=false;
prior_align=false;
post_align=false;
# end user-defined variable
. parse_options.sh || die "parse_options.sh expected";
if [[ $# -ne 5 ]]; then
  
  echo -e "\n  $0 $@\n";
  echo -e "\n should be $0 data mdldir alidir testdata dir \n";
  exit -1;
fi
data=$1; shift; dname=`basename $data`
mdldir=$1; shift;
alidir=$1; shift;
testdata=$1; shift;tname=`basename $testdata`;
dir=$1; shift;

# do alignment
if $prior_align; then
  [ -e $mdldir/final.mdl ] || die "final.mdl is not ready in folder $mdldir";
  steps/align_si.sh  --nj $nj --cmd "$train_cmd" \
  $data data/lang $mdldir $mdldir/${dname}_ali$nj  || exit 1;

 # steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
 #  $data data/lang $mdldir  $mdldir/${dname}_ali$nj || die "steps/align_fmllr.sh failed";
  alidir=$mdldir/${dname}_ali$nj;
fi
# training
steps/train_sat.sh --cmd "$train_cmd" \
  $leaves  $gaussians  $data  data/lang $alidir  $dir || \
die "steps/train_sat.sh failed";

utils/mkgraph.sh data/lang_test  $dir  $dir/graph || \
die "making graph failed";

# evaluation

steps/decode_fmllr.sh --nj $nj --cmd "$decode_cmd" \
$dir/graph  $testdata  $dir/decode_$tname || \
die "steps/decode_fmllr.sh failed";

# re-align the data again
if $post_align; then
  steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
  $data data/lang $dir $dir/${dname}_ali || \
  die "final alignment failed";
fi


