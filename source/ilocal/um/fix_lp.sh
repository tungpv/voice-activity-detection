#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
doit=false;
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

dir=$1; shift;

cd $dir;
# mkdir .back;
# remove them, and we will 
# redo it again.
for x in feats.scp cmvn.scp; do
  [ -e $x ] && rm $x;
done
[ -e wav.scp ] && rm wav.scp;
cat wav_backup.scp | \
awk '{
  if($1 in vocab) {
    f1 = vocab[$1];
    f2 = $0;
    if (f1 ~ /LLP/) {
      vocab[$1] = $0;
    }
  } else {
    vocab[$1] = $0;
  }
} END {
  for (x in vocab) {
    print vocab[x];
  }
}' | sort -u > wav.scp

