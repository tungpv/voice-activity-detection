#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
doit=false;
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if [ $stage -le 1 ] ;then
  echo -e "\ndata preparation\n";
  exclude=data/107/llp/train/segments
  sdir=language/107_FLP/data/srs_bn_0413/train
  dir=data/107/llp/utrain
  isteps/um/mksubset_dir.sh --exclude $exclude \
  $sdir  $dir 10000;
  for x in feats.scp cmvn.scp; do
    [ -e $dir/$x ] && rm $dir/$x;
  done
  wav=./swordfish/107/data/train/wav.scp
  cp $wav $dir/
fi

# prepare bottleneck feature
if [ $stage -le 2 ]; then
  echo -e "\nprepare bottleneck feature\n";
  nj=20;
  train_cmd=run.pl;
  sdata=data/107/llp/utrain;
  mkdir -p data/107/llp/fbank/utrain;
  cp $sdata/*  data/107/llp/fbank/utrain;

  prefix=data/107/llp/bn;
  nnetdir=exp/fbank/bn2;
  for x in utrain; do
    # make fbank feature first
    sdata=data/107/llp/fbank/$x;
    steps/make_fbank.sh --cmd "$train_cmd" --nj $nj \
    $sdata $sdata/_log $sdata/_data || exit 1;
    steps/compute_cmvn_stats.sh $sdata $sdata/_log $sdata/_data
    
    data=$prefix/$x;
    steps/make_bn_feats.sh --cmd "$train_cmd" \
    --nj $nj $data $sdata $nnetdir $data/_log \
    $data/_data || exit 1;
    # compute CMVN of the BN-features
    steps/compute_cmvn_stats.sh $data $data/_log \
    $data/_data || exit 1;
  done

fi
# decode the unlabeled data
if [ $stage -le 3 ];  then
  data=data/107/llp/bn/utrain;
  dname=`basename $data`;
  sdir=exp/bn/tri3sat;
  graph=$sdir/graph;
  nj=30;
  decode_cmd=run.pl;
  steps/decode_fmllr.sh --nj $nj \
  --cmd "$decode_cmd" \
  $graph $data  $sdir/decode_$dname || exit 1;
fi
# calculate utterance posterior
if [ $stage -le 4 ]; then
  echo -e "\ncalculate posterior\n";
  lang=data/107/llp/lang_dialect_final;
  latdir=exp/bn/tri3sat/decode_utrain;
  mdldir=$latdir/..;
  dir=$latdir/post;
  log=log/utt_conf.log
  nj=`cat $latdir/num_jobs`;
  silence_opts="--silence-label=1";
  sbatch -p speech -n$nj -o $log \
  isteps/um/utt_conf.sh --make-utt-conf true \
  --confdir "$latdir/uconf_rem_nsp" \
  --non-speech "$lang/non_speech.txt" \
  --cmd "$train_cmd" \
  --silence-opts "$silence_opts" $latdir $lang \
  $mdldir $dir
fi
# collect utterance list
if [ $stage -le 5 ]; then
  echo -e "\ncollect utterance with confidence score `date`\n";
  
  seg=data/107/llp/bn/utrain/segments;
  prefix=exp/bn/tri3sat/decode_utrain;
  uconf="gzip -cdf $prefix/uconf_rem_nsp/uconf.*.gz|";
  trans=$prefix/scoring/20.tra;
  dir=$prefix/selected
  [[ -d $dir ]] || mkdir -p $dir;
  for c in `seq 0.1 0.1 0.9`; do
    selSeg=$dir/segments_${c};
    iutils/um/conf_utt_select.pl --thresh $c \
    --selected $selSeg "$uconf" $seg  2>>$dir/selected.log
  done
fi

# merge data
if [ $stage -le 6 ]; then
  echo -e "\nmerge data\n";
  lang=data/107/llp/lang_dialect_final;
  prefix=data/107/llp/fbank;
  data=$prefix/train_nn;
  udata=$prefix/utrain;
  dprefix=exp/bn/tri3sat/decode_utrain;
  asr_trans=$dprefix/scoring/20.tra;
  c=0.5;
  dir=$prefix/semi/semi$c;
  seg=$dprefix/selected/segments_$c;
  isteps/um/merge_unsup_data.sh --segments $seg \
  --lang $lang \
  $asr_trans $udata $data $dir

fi
# remove some duplicated items
if [ $stage -le 7 ]; then
  echo -e "\n fix data\n";
  prefix=data/107/llp/fbank;
  data=$prefix/semi/semi0.5;
  ilocal/um/fix_lp.sh $data
  nj=20;
  for x in semi/semi0.5; do
    dir=$prefix/$x;
    sbatch -p speech -n$nj -o log/`basename $x`.log \ 
    steps/make_fbank.sh --cmd "$train_cmd" --nj $nj \
    $dir $dir/_log $dir/_data || exit 1;
    steps/compute_cmvn_stats.sh $dir $dir/_log $dir/_data
  done

fi
# update the neural network
if [ $stage -le 8 ]; then
  echo -e "\nupdate the bottleneck neural network @`hostname`, `date` \n";
  conf=configs/bn1.config;
  lang=data/107/llp/lang_dialect_final;
  mdldir=exp/fbank/tri3sat;
  train_nn=data/107/llp/fbank/semi/semi0.5;
  train_dev=data/107/llp/fbank/train_dev;
  bn1dir=exp/fbank/bn1_semi0.5;
  bn2dir=exp/fbank/bn2_semi0.5;
  isteps/tandem/train_bn1.sh --configs $conf \
  --stage 2 \
  --fmllr-align true \
  $lang $mdldir $train_nn $train_dev $bn1dir $bn2dir
fi
# re-generate the bottleneck feature
# only on the supervised part
if [ $stage -le 9 ] ; then
  echo -e "\nregenerate the bn feature \n";
  nj=20;
  train_cmd=run.pl;
  prefix=data/107/llp/bn;
  nnetdir=exp/fbank/bn2_semi0.5;
  for x in train dev; do
    sdata=data/107/llp/fbank/$x;
    data=$prefix/${x}_semi0.5;
    isteps/make_bn_feats.sh --cmd "$train_cmd" \
    --nj $nj $data $sdata $nnetdir $data/_log \
    $data/_data || exit 1;
    # compute CMVN of the BN-features
    steps/compute_cmvn_stats.sh $data $data/_log \
    $data/_data || exit 1;
  done
fi
# retrain the system using
# the updated bottleneck feature
if [ $stage -le 10 ] ;then
  echo -e "\nretrain the system with semi bn feature\n";
  nj=30;
  stage=7;
  config=configs/bn_semi0.5.config;
  log=./log/train_bn_semi0.5_regular.log;
  sbatch -p speech -n$nj -o $log \
  isteps/tandem/train_regular_am.sh \
  --stage $stage \
   $config
fi

