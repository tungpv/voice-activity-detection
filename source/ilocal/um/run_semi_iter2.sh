#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  echo -e "\n$1 on `hostname` @ `date`\n";
}
#
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  [[ $# -eq 1 ]] || \
  die "waiting partner expected";
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
doit=false;
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if [ $stage -le 1 ]; then
  echo -e "\nmake bn feature for unsupervised data\n";
  sdata=data/107/llp/utrain;
  data=data/107/llp/bn/utrain_semi0.5;
  nnetdir=exp/fbank/bn2_semi0.5;
  isteps/imake_bn_feats.sh --fbfeature true \
  $data $sdata $nnetdir $data/_log \
  $data/_data || die "failed to make bn features";
fi
# decode the data
if [ $stage -le 2 ]; then
  echo -e "\nfmllr decode `date`\n";
  nj=30;
  cmd=run.pl;
  acwt=0.05;
  lattice_beam=8.0;
  data=data/107/llp/bn/utrain_semi0.5;
  exp_dir=exp/bn_semi0.5/tri3sat;
  dir=$exp_dir/decode_`basename $data`;
  graph=$exp_dir/graph;
  if [ ! -e $graph/HCLG.fst ]; then
    die "decoding graph is not ready in $graph";
  fi
  steps/decode_fmllr.sh --nj $nj \
  --acwt $acwt \
  --stage 4 \
  --cmd $cmd  $graph $data $dir;
fi
# get semi data to update the model
if [ $stage -le 3 ] ; then
  echo -e "\nget semi data\n";
  udata=data/107/llp/utrain;
  ulat_dir=exp/bn_semi0.5/tri3sat/decode_utrain_semi0.5;
  uasr_trans=$ulat_dir/scoring/20.tra;
  lang=data/107/llp/lang_dialect_final;
  sdata=data/107/llp/train;
  cutoff=0;
  inv_ascale=20;
  semidir=data/107/llp/semi01;
  [ -d $semidir/log ] || mkdir -p $semidir/log;
  sbatch -p speech -n30 -o $semidir/log/semi.log \
  isteps/um/make_semi_data.sh --cmd "$train_cmd" \
  --inv-ascale $inv_ascale \
  --cutoff $cutoff \
  --stage 1 \
  $lang $udata $ulat_dir $sdata $uasr_trans $semidir
fi
# prepare bn feature
if [ $stage -le 4 ]; then
  echo -e "\nprepare bn feature\n";
  nj=30;
  data=data/107/llp/bn/semi014sgmm2;
  sdata=data/107/llp/semi01;
  nnetdir=exp/fbank/bn2_semi0.5;
  sbatch -p speech -o log/bn_semi014sgmm2.log -n$nj \
  isteps/imake_bn_feats.sh \
  --nj $nj \
  --fbfeature true \
  $data $sdata $nnetdir $data/_log $data/_data
fi
# make alignment for sgmm training
data=data/107/llp/bn/semi014sgmm2;
xname=`basename $data`;
lang=data/107/llp/lang_dialect_final;
mdldir=exp/bn_semi0.5/tri3sat;
alidir=$mdldir/ali_${xname}
ubmdir=exp/bn_semi0.5/ubm7;
sgmm2dir=exp/bn_semi0.5/sgmm2_7;
ubm_num=600;

nj=30;
cmd=$train_cmd;
if [ $stage -le 5 ]; then
  echo -e "\nprepare fmllr alignment\n";
  steps/align_fmllr.sh --nj $nj \
  --cmd "$cmd" $data $lang $mdldir $alidir
fi
# train ubm 

ubmdir=exp/bn_semi0.5/ubm$ubm_num;
if [ $stage -le 6 ]; then
  echo -e "\ntrain ubm `date`\n";
  train_cmd=run.pl;
  steps/train_ubm.sh --cmd "$train_cmd" \
  $ubm_num  $data $lang $alidir $ubmdir 
fi
# run sgmm training
if [ $stage -le 7 ]; then
  echo -e "\nsgmm2 work `date`\n";
  nstate=4500; nsubstate=36000;
  scale_post=data/107/llp/bn/semi014sgmm2/uconf.txt;
  sgmm2dir=exp/bn_semi0.5/sgmm2_${nstate}_$nsubstate
  if $doit; then
    sgmm2dir=exp/bn_semi0.5/sgmm2_${nstate}_$nsubstate
    isteps/um/train_sgmm2.sh --cmd "run.pl" \
    --scale-post $scale_post \
    $nstate $nsubstate $data $lang $alidir \
    $ubmdir/final.ubm $sgmm2dir  || exit 1;
  fi  
   # decode
  if [ -e $sgmm2dir/final.mdl ]; then 
    testdata=data/107/llp/bn/dev_semi0.5;
    testname=`basename $testdata`;
    lang_test=data/107/llp/lang_dialect_final_test;
    if [ ! -e $sgmm2dir/graph/HCLG.fst ]; then
      utils/mkgraph.sh $lang_test $sgmm2dir $sgmm2dir/graph;
    fi
    cmd=run.pl;
    satdecdir=exp/bn_semi0.5/tri3sat/decode_dev_semi0.5;
    sbatch -p speech -n$[$nj*2] -o log/msgmm2.log \ 
    steps/decode_sgmm2.sh --nj $nj \
    --acwt 0.1 \
    --lattice-beam 8.0 \
    --num-threads 2 \
    --cmd "$cmd" --transform-dir $satdecdir \
    $sgmm2dir/graph $testdata $sgmm2dir/decode_$testname;
  fi
  echo -e "done with sgmm2 eval";
  exit 0;
fi
# select sub set data
train_bn_data=data/107/llp/fbank/semi/semi${thresh}_it2
if [ $stage -le 8 ]; then
  echo -e "\nprepare bn training data\n";
  sdir=data/107/llp/bn/semi014sgmm2/fbank;
  uconf=$sdir/uconf.txt;
  thresh=0.4;
  exclude=data/107/llp/fbank/train_dev/segments;
  isteps/um/mksubset_dir.sh --exclude $exclude \
  --upost-list $uconf \
  --upost-thresh $thresh \
  $sdir $train_bn_data
  
  sdir=data/107/llp/bn/semi014sgmm2;
  uconf=$sdir/uconf.txt;
  dir=data/107/llp/bn/semi/semi0.4_it2;
  isteps/um/mksubset_dir.sh --exclude $exclude \
  --upost-list $uconf \
  --upost-thresh $thresh \
  $sdir $dir
  dir=data/107/llp/bn/semi/semi0.4_it2_nndev;
  
  isteps/um/mksubset_dir.sh --include $exclude \
  $sdir $dir

fi
 # do forced alignment on the training data
if [ $stage -le 9  ]; then
  echo -e "\nforced alignment on the training data\n";
  nj=30;
  cmd=run.pl;  
  lang=data/107/llp/lang_dialect_final;
  mdldir=exp/bn_semi0.5/tri3sat;
  data1=data/107/llp/bn/semi/semi0.4_it2;
  data2=data/107/llp/bn/semi/semi0.4_it2_nndev;
  for data in $data2; do
    nj=10;
    xname=`basename $data`
    alidir=$mdldir/ali_${xname};
    steps/align_fmllr.sh --nj $nj \
    --cmd "$cmd" $data $lang $mdldir $alidir
  done 
fi
# update BN  neural network
if [ $stage -le 10 ]; then
  echo -e "\n bottleneck neural network training\n";
  isteps/train_hierbn.sh configs/bn/hierbns.sh 
fi
# update bottleneck feature
if [ $stage -le 11 ]; then
  echo -e "\nmake BN feature\n";
  nj=30;
  nnetdir=exp/bn_semi0.4_it2/bn2;
  sdata_array=(data/107/llp/train data/107/llp/dev);
  data_array=(data/107/llp/bn/iter2/train data/107/llp/bn/iter2/dev);
  for i in 0 1; do
    sdata=${sdata_array[$i]};
    data=${data_array[$i]};
    sbatch -p speech -o log/bn_iter2.log -n$nj \
    isteps/imake_bn_feats.sh \
    --nj $nj \
    --fbfeature true \
   $data $sdata $nnetdir $data/_log $data/_data
  done 
fi
# update acoustice model
if [ $stage -le 12 ]; then
  echo -e "\nretraing acoustic model\n";
  config=configs/bn/ac_bn_iter2.sh;
  nj=30;
  log=./log/ac_bn_iter2.log;
  sbatch -p speech -n$nj -o $log \
  isteps/tandem/train_regular_am.sh \
   $config
fi
# update feature for the unsupervised data
if [ $stage -le 19 ]; then
  echo -e "\nbn feature for unsupervised data\n";
  nj=30;
  cmd=run.pl;
  sgmmdir=exp/ac_bn_iter2/sgmm4;
  nnetdir=exp/bn_semi0.4_it2/bn2;
  sdata=data/107/llp/utrain;
  data=data/107/llp/bn/iter2/utrain;
  dataname=`basename $data`;
  satdir=exp/ac_bn_iter2/tri3sat;
  transform_dir=$satdir/decode_$dataname;

  if $doit; then
    sbatch -p speech -o log/utrain_bn_it2.log -n$nj \
    isteps/imake_bn_feats.sh \
    --nj $nj \
    --fbfeature true  \
    $data $sdata $nnetdir $data/_log $data/_data
  fi
  # use sgmm2 model to  redecode the unlabeled data
  if $doit; then
  Wait $data/.done;
  
  isteps/decode_fmllr.sh --nj $nj \
  --cmd $cmd \
  $satdir/graph $data $transform_dir
  Wait $transform_dir/.done;
  fi
  echo -e "\nsgmm2 decods unlabeled data\n";

  isteps/idecode_sgmm2.sh --nj $nj \
  --cmd $cmd \
  --stage 6 \
  --transform-dir $transform_dir \
  --use-fmllr true \
  $sgmmdir/graph $data $sgmmdir/decode_$dataname
fi
# one-pass  sgmm decode test
# to generate denser lattice, hopfully
if [ $stage -le 17 ]; then
  echo -e "\nsgmm2 denser lattice generation\n";
  nj=30;
  cmd=run.pl;
  inverse_acwt=15;
  acwt=`echo "1/$inverse_acwt"|bc -l | awk '{printf "%.4f", $1;}'`
  data=data/107/llp/bn/iter2/dev;
  transform_dir=exp/ac_bn_iter2/tri3sat/decode_dev;
  mdldir=exp/ac_bn_iter2/sgmm4
  vecs_dir=$mdldir/decode_dev
  gselect_dir=$mdldir/decode_dev
  dir=$mdldir/decode_dev_sgmm2_single;
  isteps/decode_sgmm2_single_pass.sh --nj $nj \
  --cmd $cmd \
  --acwt $acwt \
  --transform-dir $transform_dir \
  --vecs-dir $vecs_dir \
  --gselect-dir $gselect_dir \
  $mdldir/graph $data $dir
fi
#  
# to generate denser lattice, hopfully
if [ $stage -le 21 ]; then
  echo -e "\nsgmm2 denser lattice generation\n";
  nj=30;
  cmd=run.pl;
  data=data/107/llp/bn/iter2/dev;
  transform_dir=exp/ac_bn_iter2/tri3sat/decode_dev;
  mdldir=exp/ac_bn_iter2/sgmm4
  dir=$mdldir/decode_dev_sgmm2_i2;
  isteps/i2decode_sgmm2.sh --nj $nj \
  --cmd $cmd \
  --transform-dir $transform_dir \
  --use-fmllr true \
  $mdldir/graph $data $dir
fi
# for comparison
if [ $stage -le 23 ]; then
  LOG "sgmm2 decode started";
  nj=30;
  cmd=run.pl;
  data=data/107/llp/bn/dev;
  transform_dir=exp/bn/tri3sat/decode_dev;
  mdldir=exp/bn/sgmm4
  dir=$mdldir/decode_dev_sgmm2_i2;
  isteps/i2decode_sgmm2.sh --nj $nj \
  --cmd $cmd \
  --transform-dir $transform_dir \
  --use-fmllr true \
  $mdldir/graph $data $dir
  LOG "sgmm2 decode ended";
fi
