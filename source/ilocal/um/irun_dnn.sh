#!/bin/bash

# Copyright 2012-2013  Brno University of Technology (Author: Karel Vesely)
# Apache 2.0

# In this recipe we build DNN in four stages:
# 1) Data preparations : the fMLLR features are stored to disk
# 2) RBM pre-training : in this unsupervised stage we train stack of RBMs, a good starting point for Cross-entropy trainig
# 3) Frame-level cross-entropy training : in this stage the objective is to classify frames correctly.
# 4) Sequence-criterion training : in this stage the objective is to classify the whole sequence correctly,
#     the idea is similar to the 'Discriminative training' in context of GMM-HMMs.


function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}



. ./cmd.sh ## You'll want to change cmd.sh to something that will work on your system.
           ## This relates to the queue.

. ./path.sh ## Source the tools/utils (import the queue.pl)
# begin user-defined variable
 prepare_data=false;
 make_fmllr_feature=false;
 rbm_pretrain=false;
 dnn_train=false;
 dnn_decode=false;
 nn_depth=4;
# end user-defined variable
. parse_options.sh || die "parse_options. sh expected";

# make development set
if $prepare_data; then
  echo -e "\n making deep neural network development data\n";
  nj=20; x=10h;
  sdir=../107/data/train
  exclude=data/train_10h/segments;
  dir=data/dnn_dev2h;
  hour_len=2;
  [[ -d $dir ]] || \
  isteps/um/mksubset_dir.sh --exclude $exclude $sdir $dir $hour_len
  prefix=data/local/data/dnn
  data=$dir
  dname=`basename $data`;
  dir=$prefix/data_fmllr/$dname
  gmmdir=exp/tri3b/10h
  logdir=log/$x;
  [[ -d $logdir ]] || mkdir -p $logdir
  logfile=$logdir/make_fmllr_feature_${dname}.log
  sbatch -p speech -n$nj -o $logfile \
  isteps/um/make_fmllr_feats.sh --nj $nj  \
  --cmd "$train_cmd" --prior-align true \
  $dir $data $gmmdir $dir/_log $dir/_data || exit 1
fi
# make fMLLR features
if $make_fmllr_feature; then
  nj=20
  x=10h
  gmmdir=exp/tri3b/10h
  prefix=data/local/data/dnn
  sets=(train_10h dev3h);
  tdir=(exp/tri3b/10h/train_10h_ali  exp/tri3b/10h/decode_dev3h);
  for idx in 0 1; do
    dname=${sets[$idx]};
    trans_dir=${tdir[$idx]};
    dir=$prefix/data_fmllr/$dname
    data=data/$dname
    logdir=log/$x;
    [[ -d $logdir ]] || mkdir -p $logdir
    logfile=$logdir/make_fmllr_feature_${dname}.log
    sbatch -p speech -n$nj -o $logfile \
    steps/make_fmllr_feats.sh --nj $nj  --cmd "$train_cmd" \
    --transform-dir $trans_dir \
   $dir $data $gmmdir $dir/_log $dir/_data || exit 1
  done
fi
# pretraining
pretrain_dir=exp/dnn/$x/pretrain_$nn_depth;
if $rbm_pretrain; then
  x=10h
  . cuda_path.sh || die  "cuda_path.sh expected";
  . cuda_cmd.sh  || die  "cuda_cmd.sh expected";
  prefix=data/local/data/dnn/data_fmllr
  data=$prefix/train_10h
  dir=$pretrain_dir;
  logdir=log/$x;
  [[ -d $logdir ]] || mkdir -p $logdir
 #  logfile=$logdir/rbm_pretrain.log
 #  sbatch -p speech  -w squid7 -o $logfile \
  isteps/um/pretrain_dbn.sh --rbm-iter 1  --nn-depth $nn_depth \
  --hid-dim 1024   $data $dir
fi
# dnn training
if $dnn_train; then
  x=10h;
  . cuda_path.sh || die  "cuda_path.sh expected";
  . cuda_cmd.sh  || die  "cuda_cmd.sh expected";

  prefix=data/local/data/dnn/data_fmllr
  data=$prefix/train_10h
  pretrain=$pretrain_dir;
  fpretrain=`(cd $pretrain; pwd)`;
  train_alidir=exp/tri3b/10h/train_10h_ali
  cv_alidir=exp/tri3b/10h/dnn_dev2h_ali
  dbn=$fpretrain/$nn_depth.dbn
  feature_transform=$fpretrain/final.feature_transform
  dir=exp/dnn/$x/pret_dnn_$nn_depth;
  logdir=log/$x;
  logfile=$logdir/dnn_.log
  # sbatch -p speech -gres=gpu:1  -w squid7 -o $logfile \
  steps/train_nnet.sh --feature-transform $feature_transform --dbn $dbn \
  --hid-layers 0 --learn-rate 0.008 \
  $data $prefix/dnn_dev2h data/lang $train_alidir $cv_alidir $dir || exit 1;
#
fi
#
if $dnn_decode; then
  x=10h;
  nj=30;
  # decode with 'big-dictionary' (reuse HCLG graph)
  # . path.sh || die  "cuda_path.sh expected";
  # . cmd.sh  || die  "cuda_cmd.sh expected";

  data=data/local/data/dnn/data_fmllr/dev3h
  dname=`basename $data`;
  dir=exp/dnn/$x/pretrain_dbn-dnn;
  logdir=log/decode; [[ -d $logdir ]] || mkdir -p $logdir
  logfile=$logdir/dnn.log;

  sbatch -p speech -o $logfile \
  steps/decode_nnet.sh --nj $nj --cmd "$decode_cmd" \
  --acwt 0.10 --config conf/decode_dnn.config \
  exp/tri3b/10h/graph $data  $dir/decode_${dname} || exit 1;
fi


