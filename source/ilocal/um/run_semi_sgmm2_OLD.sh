#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Log {
  echo -e "\n`date`:$1 @ `hostname`\n";
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
doit=false;
stage=0;
do_semi_fa=false;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

# make bottleneck feature
if [ $stage -le 1 ]; then
  echo -e "make bn feature at `hostname`@ `date`"
  cmd=run.pl
  data=data/107/llp/fbank/semi/semi0.5
  xname=`basename $data`;
  dir=data/107/llp/bn/semi/$xname;
  nnetdir=exp/fbank/bn2_semi0.5;
  nj=20;
  isteps/make_bn_feats.sh --cmd "$cmd" \
  --nj $nj $dir $data $nnetdir $dir/_log \
  $dir/_data || exit 1;
  # compute CMVN of the BN-features
  steps/compute_cmvn_stats.sh $dir $dir/_log \
  $dir/_data || exit 1;

fi
# get subset data, which
# should be the supervised part.
if [ $stage -le 2 ]; then
  Log "extract the supervised part";
  sdir=data/107/llp/bn/semi/semi0.5;
  seg=data/107/llp/bn/train/segments
  dir=$sdir/_sub;
  ./isteps/um/mksubset_dir.sh --include $seg $sdir $dir
fi
# do forced alignment or 
# decoding on the semi data
if [ $stage -le 3 ]; then
  # redecode the data with
  # the updated model
  ilocal/um/auto_label_data.sh --stage 4;
  # cure cmvn duplicated case
  dir=data/107/llp/bn/semi/trans_updated_semi0.5
  cat $dir/cmvn.scp | awk '{ if ($1 in vocab){}else {vocab[$1]=0; print;}}' > $dir/tmp.cmvn
  mv $dir/tmp.cmvn $dir/cmvn.scp
fi
# do forced alignment
if [ $stage -le 4 ]; then
  Log "do forced alignment";
  nj=30;
  cmd=run.pl;
  data=data/107/llp/bn/semi/trans_updated_semi0.5;
  xname=`basename $data`;
  lang=data/107/llp/lang_dialect_final;
  mdldir=exp/bn_semi0.5/tri3sat;
  alidir=$mdldir/ali_${xname}
  steps/align_fmllr.sh --nj $nj \
  --cmd "$cmd" $data $lang $mdldir $alidir
fi
# train subspace gmm, first ubm
if [ $stage -le 5 ]; then
  Log "train ubm";
  train_cmd=run.pl;
  ubm_num=600;
  data=data/107/llp/bn/semi/trans_updated_semi0.5;
  mdldir=exp/bn_semi0.5/tri3sat;
  xname=`basename $data`;
  alidir=$mdldir/ali_${xname}
  lang=data/107/llp/lang_dialect_final;
  exp_dir=exp/bn_semi0.5/ubm5;
  steps/train_ubm.sh --cmd "$train_cmd" \
  $ubm_num  $data $lang $alidir $exp_dir 
fi
#
if [ $stage -le 6 ]; then
  Log "train subspace gmm";
  cmd=run.pl;
  nj=30;
  nstate=7000;
  nsubstate=90000;
  data=data/107/llp/bn/semi/trans_updated_semi0.5;
  lang=data/107/llp/lang_dialect_final;
  eprefix=exp/bn_semi0.5; 
  mdldir=$eprefix/tri3sat;
  xname=`basename $data`;
  alidir=$mdldir/ali_${xname}
  ubmdir=$eprefix/ubm5;
  exp_dir=$eprefix/sgmm2_5b;
  scale_post=$data/uconf.txt;
  # if $doit; then
    isteps/um/train_sgmm2.sh --cmd "$cmd" \
    --scale-post $scale_post \
    $nstate $nsubstate $data $lang $alidir \
    $ubmdir/final.ubm $exp_dir  || exit 1;
  # fi
  # decode
  if [ -e $exp_dir/final.mdl ]; then 
    testdata=data/107/llp/bn/dev_semi0.5;
    testname=`basename $testdata`;
    lang_test=data/107/llp/lang_dialect_final_test;
    if [ ! -e $exp_dir/graph/HCLG.fst ]; then
      utils/mkgraph.sh $lang_test $exp_dir $exp_dir/graph;
    fi
    satdecdir=exp/bn_semi0.5/tri3sat/decode_dev_semi0.5;
    steps/decode_sgmm2.sh --nj $nj \
    --cmd "$cmd" --transform-dir $satdecdir \
    $exp_dir/graph $testdata $exp_dir/decode_$testname;
  fi
fi
# test various methods to train
# sgmm
if [ $stage -le 0 ]; then
  Log "train_sgmm2";
  nj=30;
  config=configs/run_sgmm2_conf.sh
  data=data/107/llp/bn/semi/trans_updated_semi0.5;
  eprefix=exp/bn_semi0.5;
  alidir=$eprefix/tri3sat/ali_trans_updated_semi0.5;
  exp_dir=$eprefix/sgmm2_5_no_scale_post;
  sbatch -p speech -n$nj -o log/run_sgmm2_no_scale_post.log \
  ilocal/um/run_sgmm2.sh --stage 2 $config \
  $data $alidir $exp_dir
  #
  data=data/107/llp/bn/train_semi0.5;
  alidir=$eprefix/tri3sat_ali_train_semi0.5  
  exp_dir=$eprefix/sgmm2_5_sup;
  sbatch -p speech -n$nj -o log/run_sgmm2_5_sup.log \
  ilocal/um/run_sgmm2.sh --stage 2 $config \
  $data $alidir $exp_dir
fi
