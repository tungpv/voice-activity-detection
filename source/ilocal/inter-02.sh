#!/bin/bash
function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "cuda_path.sh expected";
. cmd.sh || die "cmd.sh expected";

# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
step=0
substep=0
prepare_data=false
train_plppp=false
bnf_llp=false
train_sgmm=false
bnf_tanh=false
tandem_llp_pitch=false
mbr_sys_comb=false
decode_unsupervised=false
lang=data/lang_dialect_final; lang_test=${lang}_test

# end tunable variable
bn_am_cf=conf/am_regular.sh
echo -e "\n$0 $@\n";
. parse_options.sh || die "parse_options.sh expected";
dp=data/feature/inter02;
array_src=data/{train,utrain,dev10h,dev3h}
plp=$dp/plp/{train,utrain,dev10h,dev3h}
fbank=$dp/fbank/{train,utrain,dev10h,dev3h}
pitch=$dp/pitch/{train,utrain,dev10h,dev3h}
fbank40=$dp/block/fbank40/{train,utrain,dev10h,dev3h}

if $prepare_data; then
  
  if [ $step -le 1 ]; then
    for x in train dev10h; do
      src=data/$x; data1=$dp/$x/plp_pitch;
      [ ! -d $src ] && echo "src folder $src does not exist" && exit 1
      [ -d $data1 ] || mkdir -p $data1
      cp $src/* $data1; rm $data1/{feats.scp,cmvn.scp} 2>/dev/null
      steps/make_plp_pitch.sh --nj 10 $data1  $data1/_log $data1/_data || exit 1
      data2=$dp/$x/ffv;
      cp $src/* $data2; rm $data2/{feats.scp,cmvn.scp} 2>/dev/null
      local/make_ffv.sh --nj 10 $data2 $data2/_log $data2/_data || exit 1;
      data=$dp/$x/plp_pitch_ffv;
      steps/append_feats.sh --nj 10 $data1 $data2 $data $data/_log $data/_data || exit 1
    done
  fi
fi
