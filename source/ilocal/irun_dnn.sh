#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Log {
  echo -e "\n\n`date` $1\n\n";
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";

# begin user-defined variable
stage=0;
max_stage=8;
nj=10;
cmd=run.pl;
acwt=0.0833;
bunch_size=256;
cache_size=$[16384*16];
hidden_dim=2048;
nn_depth=6;
learn_rate=0.008;
gmmdir=;
graphdir=;
make_fmllr_feat=false;
align_fmllr=false;
over_train_ali=;
over_vali_ali=;
feat_prefix=`pwd`;
prior_pretrain_dir=;
dev_usable=false;
dev_data=;
transform_dir=
temp_prefix=`pwd`;
clean_temp=false;
# end user-defined variable

echo -e "\n $0 $@\n";
. parse_options.sh || \
die "parse_options.sh expected";
#
if [ $# -ne 6 ] ;then
  echo -e "\nWrapper to train dnn";
  echo -e "\n$0 [options] compulsories\n";
  exit 1;
fi

train_data=$1; shift;
vali_data=$1; shift;
lang=$1; shift;
train_ali=$1; shift;
vali_ali=$1; shift;
expdir=$1; shift;
has_dev=`eval [ ! -z $transform_dir ] && [ ! -z $dev_data ] && [ ! -z $graphdir ] || $dev_usable`;
# check
if $dev_usable; then
  echo "dev_usable is true";
  $has_dev && echo "has_dev is true";
fi
# fmllr feature preparation, if necessay
if [ $stage -le 1 ]; then
  if $make_fmllr_feat; then
    Log "step1: fmllr feature preparation";   
    [ -e $gmmdir/final.mdl ] || die "gmmdir is not ready";
    array_ali=($train_ali $vali_ali); idx=0;
    array_nj=(30 11); 
    array_data=($train_data $vali_data);
    for x in $train_data $vali_data; do
      ali=${array_ali[$idx]}; 
      data=${array_data[$idx]}; 
      njx=${array_nj[$idx]};
      dataname=`basename $data`;
      idx=$[$idx+1];
      if $align_fmllr; then
        steps/align_fmllr.sh --nj $njx --cmd "$cmd" \
        $data $lang  $gmmdir  $ali || \
        die "@step1:steps/align_fmllr.sh for $ali";
      fi
      # make fmllr feature
      dir=$feat_prefix/$dataname;
      steps/make_fmllr_feats.sh --nj $njx --cmd "$cmd" \
      --transform-dir $ali $dir  $x $gmmdir $dir/_log \
      $dir/_data \
      || die "@step1:steps/make_fmllr_feats.sh for $dataname";
    done
  fi
fi
#
[ ! -z $over_train_ali ] && train_ali=$over_train_ali;
[ ! -z $over_vali_ali ] && vali_ali=$over_vali_ali; 
# for dev data if any
if  [ $stage -le 2 ]; then
  if  $has_dev && ! $dev_usable ; then
    echo -e "update fmllr feature for updated neural network";
    dir=$feat_prefix/`basename $dev_data`;
    steps/make_fmllr_feats.sh --nj $nj --cmd "$cmd" \
    --transform-dir $transform_dir $dir $dev_data \
    $gmmdir $dir/_log $dir/_data \
    || die "steps/make_fmllr_feats.sh for $dev_data";
    # dev_data=$dir;
  fi
fi
$has_dev && ! $dev_usable && dev_data=$feat_prefix/`basename $dev_data`;
# 
if $make_fmllr_feat; then
  echo -e "\n\nbefore:\ntrain_data=$train_data\nvali_data=$vali_data\n\n";
  train_data=$feat_prefix/`basename $train_data`;
  vali_data=$feat_prefix/`basename $vali_data`;
  # dev_data=$feat_prefix/`basename $dev_data`;
  echo -e "\n\nafter:\ntrain_data=$train_data\nvali_data=$vali_data\n\n";
fi
devname=`basename $dev_data`;
# pretraining
pretrain_dir=$expdir/_pretrain_dbn;
cuda_cmd=run.pl;
if [ $stage -le 3 ]; then
  if  [ ! -z $prior_pretrain_dir ]; then
    echo -e "user served prtraining: $prior_pretrain_dir";
    pretrain_dir=$prior_pretrain_dir;
  else
    Log "pretraining";
    $cuda_cmd $pretrain_dir/pretrain_dbn.log \
    isteps/pretrain_dbn.sh --temp-prefix $temp_prefix \
    --nn-depth $nn_depth \
    --hid-dim $hidden_dim \
    --rbm-iter 1 \
    $train_data $pretrain_dir \
    || die "@step3:isteps/pretrain_dbn.sh";
  fi
fi

dnn_dir=$expdir/dnn_xent
feature_transform=$pretrain_dir/final.feature_transform;
dbn=$pretrain_dir/$nn_depth.dbn;
if [ $stage -le 5 ]; then
  Log "deep neural network training";
  $cuda_cmd $dnn_dir/_train_nnet.log \
  isteps/tandem/train_nnet.sh \
  --temp-prefix $temp_prefix \
  --clean-temp $clean_temp \
  --feature-transform $feature_transform \
  --dbn $dbn \
  --hid-layers 0  \
  --learn-rate $learn_rate \
  --bunch-size $bunch_size \
  --cache-size $cache_size \
  $train_data $vali_data $lang $train_ali $vali_ali $dnn_dir \
  || die "@step5: isteps/tandem/train_nnet.sh"; 
fi
decode_cmd=slocal/slurm.pl;
# evaluation for the first time
if [ $stage -le 8 ]; then
  if $has_dev; then
    Log "xent:decode";
    # sbatch -p speech -n$nj -o $dnn_dir/sbatch_decode.log \
    isteps/decode_nnet.sh --nj $nj \
    --cmd "$decode_cmd" \
    --max-mem $[50000000*100] \
    --latbeam 5.0 \
    --acwt $acwt \
    $graphdir  $dev_data $dnn_dir/decode_${devname} || \
    die "@step8: steps/decode_nnet.sh";
    [[ $max_stage -le 8 ]] && exit 0;
  fi
fi
# sequence sMBR neural network training
smbr_dnn_dir=$expdir/dnn_smbr
srcdir=$dnn_dir;
trainname=`basename $train_data`;
alidir=$srcdir/${trainname}_ali;
dendir=$srcdir/${trainname}_denlats;
if [ $stage -le 9 ]; then
  Log "sMBR training";
  
  steps/align_nnet.sh --nj $nj  --cmd "run.pl" \
  $train_data $lang $srcdir  $alidir \
  || die "@step9:steps/align_nnet.sh" ;
fi
if [ $stage -le 10 ]; then
  isteps/make_denlats_nnet.sh --nj $nj \
  --cmd "run.pl" \
  --lattice-beam 4.0 \
  --max-mem $[50000000*60] \
  --config conf/decode_dnn.config --acwt $acwt \
  $train_data $lang $srcdir $dendir \
  || die "@step10: steps/make_denlats_nnet.sh" ;
fi
# 
if [ $stage -le 11 ]; then
  # training
  isteps/train_nnet_mpe.sh --cmd "run.pl" \
  --num-iters 1 \
  --acwt $acwt \
  --do-smbr true \
  $train_data $lang $dnn_dir \
  $alidir \
  $dendir \
  $smbr_dnn_dir || \
  die "@step11:isteps/train_nnet_mpe.sh";
fi
# decode
if [ $stage -le 14 ]; then
  if $has_dev; then
    Log "sMBR1:sMBR decoding";
    # sbatch -p speech -n$nj -o $smbr_dnn_dir/sbatch_smbr1.log \
    isteps/decode_nnet.sh --nj $nj \
    --cmd  run.pl \
    --max-mem $[50000000*60] \
    --latbeam 5.0 \
    --acwt $acwt \
    --nnet $smbr_dnn_dir/1.nnet \
    $graphdir  $dev_data $smbr_dnn_dir/decode_${devname}_1 || \
    die "@step14: isteps/decode_nnet.sh";
  fi
  [[ $max_stage -le 14 ]] && Log "Ended for ilocal/irun_dnn.sh" && exit 0;
fi

# regenerate lattice and retrain smbr dnn
smbr_dnn_dir2=$expdir/dnn_smbr2;
cmd=run.pl;
srcdir=$smbr_dnn_dir;
trainname=`basename $train_data`;
alidir=$srcdir/${trainname}_ali;
dendir=$srcdir/${trainname}_denlats;
if [ $stage -le 15 ]; then
  Log "prepare further smbr training";
  steps/align_nnet.sh --nj $nj  --cmd $cmd \
  $train_data $lang $srcdir  $alidir \
  || die "@step15:steps/align_nnet.sh" ;
fi
if [ $stage -le 16 ]; then
  Log "make den lattice";
  steps/make_denlats_nnet.sh --nj $nj \
  --cmd $cmd \
  --max-mem $[50000000*60] \
  --lattice-beam 4.0 \
   --acwt $acwt \
  $train_data $lang $srcdir $dendir \
  || die "@step16: steps/make_denlats_nnet.sh" ;
fi 

if [ $stage -le 17 ]; then
  isteps/train_nnet_mpe.sh --cmd "$cmd" \
  --num-iters 2 --acwt $acwt --do-smbr true \
  $train_data $lang $srcdir \
  $alidir \
  $dendir \
  $smbr_dnn_dir2 \
  || die "@step17:isteps/train_nnet_mpe.sh";
fi

# eval on dev data, if any
if [ $stage -le  19 ]; then
  if $has_dev; then
    for iter in 1 2; do
      sbatch -p speech -n$nj -o $smbr_dnn_dir2/sbatch_$iter.log \
      isteps/decode_nnet.sh --nj $nj \
      --cmd run.pl \
      --max-mem $[50000000*60] \
      --latbeam 6.0 \
      --acwt $acwt \
      --nnet $smbr_dnn_dir2/${iter}.nnet \
      $graphdir  $dev_data $smbr_dnn_dir2/decode_${devname}_${iter} || \
      die "@step19: steps/decode_nnet.sh for $iter";
    done
  fi
fi
echo -e "\ndone for running dnn\n";
