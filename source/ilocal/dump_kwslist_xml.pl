#!/usr/bin/env perl
use warnings;
use strict;
use Getopt::Long;
use XML::Simple;
use Data::Dumper;

sub DetectInsertion {
  my ($component_num, $component_index, $kwid, $file, $score, $decision, $vocab, $max_vocab) = @_;
  die "component_index is larger than specified component number" if ($component_index >= $component_num);  
  if(not exists $$max_vocab{$component_index}{$kwid}{$file}{$decision}){
    if(not exists $$vocab{$kwid}{$file} ) {
      my @array = ();  
      $$vocab{$kwid}{$file} = \@array;
      my $aRef = $$vocab{$kwid}{$file};
      for(my $i = 0; $i < $component_num; $i++) {
        push @$aRef, 0;
      }
      $$aRef[$component_index] = $score;
    } else {
      my $aRef = $$vocab{$kwid}{$file};
      @$aRef[$component_index] = $score;   
    }
    $$max_vocab{$component_index}{$kwid}{$file}{$decision} = $score;
  } else {
    my $prev_score = $$max_vocab{$component_index}{$kwid}{$file}{$decision};
    if ($score > $prev_score) {
      $$max_vocab{$component_index}{$kwid}{$file}{$decision} = $score;
      my $aRef = $$vocab{$kwid}{$file};
      $$aRef[$component_index] = $score;
    }  
  }  
}
# keyword specific z-normalization
sub ZNormalize {
  my($dataRef) = @_;
  my %vocab = ();
  foreach my $kent (@{$$dataRef->{detected_kwlist}}) {
    my $kwid = $kent->{kwid};
    if(ref $kent->{kw} eq 'ARRAY') {
      my $aRef = $kent->{kw}; 
      my ($mu, $sigma) = (0, 0);
      my $N = scalar @$aRef;
      for(my $i = 0; $i < $N; $i ++) {
        my $vRef = $$aRef[$i];  my $x = $vRef->{score};
        $mu += $x;  $sigma +=$x*$x;
      }
      # this might cause problems, if all scores are zero
      $mu /= $N; $sigma = $sigma/$N - $mu*$mu; $sigma = sqrt($sigma);
      for(my $i = 0; $i < $N; $i ++) {
        my $vRef = $$aRef[$i];
        $vRef->{score} = ($vRef->{score} - $mu)/ $sigma;
      }
    } else {
      my $hRef = $kent->{kw};
      # die "duplicated error !" if exists $vocab{$kwid};
      # $vocab{$kwid} ++;
      $hRef->{score} = 0;
    } 
  }
}
# sum-to-one normalization
sub STONormalize {
  my ($datap) = @_;
  foreach my $kent (@{$$datap->{detected_kwlist}}) {
    my $kwid = $kent->{kwid};
    if(ref $kent->{kw} eq 'ARRAY') {
      my $ap = $kent->{kw};
      my $sum = 0;
      for(my $i = 0; $i < @$ap; $i ++) {
        my $vp = $$ap[$i]; my $x = $vp->{score};
        $sum += $x;
      }
      if ($sum != 0) {
        for(my $i = 0; $i < @$ap; $i++) {
          my $vp = $$ap[$i]; my $x = $vp->{score};
          $vp->{score} = sprintf ("%.6f", $x/$sum);
        }
      }
    } else {
      my $hp = $kent->{kw};  $hp->{score} = 1.0;
    }
  }
}
# keyword specific normalization
sub KSTNormalize {
  my ($datap, $duration, $beta) = @_;
  foreach my $kent (@{$$datap->{detected_kwlist}}) {
    my $kwid = $kent->{kwid};
    if(ref $kent->{kw} eq 'ARRAY') {
      my $ap = $kent->{kw};
      my ($ntrue) =(0); my $N = scalar @$ap;
      for(my $i = 0; $i < $N; $i ++) {
        my $vp = $$ap[$i]; my $x = $vp->{score};
        $ntrue += $x;
      }
      my $thresh = $ntrue/($duration/$beta +($beta-1)/$beta*$ntrue);
      for(my $i = 0; $i < scalar @$ap; $i ++) {
        my $vp = $$ap[$i]; my $x = $vp->{score};
        my $num = (1-$thresh)*$x;
        my $den = (1-$thresh)*$x + (1-$x)*$thresh;
        if ($den != 0) {
          $vp->{score} = sprintf ("%.6f", $num/$den);
        } else {
          $vp->{score} = sprintf ("%.6f", $x);
        }
      }
    } else {
      my $hp = $kent->{kw};
      my $x = $hp->{score}; my $ntrue = $x;
      my $thresh = $ntrue/($duration/$beta +($beta-1)/$beta*$ntrue);
      my $num = (1-$thresh)*$x;
      my $den = (1-$thresh)*$x + (1-$x)*$thresh;
      if ($den != 0) {
        $hp->{score} = sprintf ("%.6f", $num/$den);
      } else {
        $hp->{score} = sprintf ("%.6f", $x);
      }
    }
  }
}

# |-------------- main function entrance ----------------|
# |                                                      |
# |------------------------------------------------------|
#
my $z_norm = "false";
my $sto_norm="false";
my $kst_norm="false";
my $beta = 66.6;
my $duration = 66.6; 
my $language = "english";
my $query_length_file="";

GetOptions( 'language=s' => \$language,
  'z-norm=s' => \$z_norm,
  'sto-norm=s' => \$sto_norm,
  'kst-norm=s' => \$kst_norm,
  'duration=f' => \$duration,
  'beta=f' => \$beta,
  'query-length-file=s' => \$query_length_file);
my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  die "\nUsage: $0 <kwslist_xml_filelist> <component_num>\n";
}
my($xml_fname, $component_num) = @ARGV;
open(F, "$xml_fname") or die "file cannot open\n";
my %vocab = (); my %max_vocab=();
my $component_index = 0;
while (<F>) {
  chomp;
  my $fname = $_;
  print STDERR "$0: processing file $fname ...\n";
  my $data = XMLin($fname);
  if ($z_norm eq "true") {
    print STDERR "$0: z-norm  $fname ...\n";
    ZNormalize(\$data);
  }
  if ($sto_norm eq 'true') {
    print STDERR "$0: sum-to-one norm $fname ....\n";
    STONormalize(\$data);
  }
  if ($kst_norm eq 'true') {
    print STDERR "$0: kst norm $fname enabled with duration=$duration, beta=$beta ...\n";
    KSTNormalize(\$data, $duration, $beta); 
  }
  ## print Dumper($data);
  foreach my $kent (@{$data->{detected_kwlist}}) {
    my $kwid = $kent->{kwid};
    if(ref $kent->{kw} eq 'ARRAY') {
      my $aRef = $kent->{kw};
      for(my $i = 0; $i< @$aRef; $i++) {
        my $vRef = $$aRef[$i];
        my($file, $score, $decision) = ($vRef->{file}, $vRef->{score}, $vRef->{decision});
        DetectInsertion($component_num, $component_index, $kwid, $file, $score, $decision, \%vocab, \%max_vocab);
      }
    } else {
      my $hRef = $kent->{kw};
      my ($file, $score, $decision) = ($hRef->{file}, $hRef->{score}, $hRef->{decision});
      DetectInsertion($component_num, $component_index, $kwid, $file, $score, $decision, \%vocab, \%max_vocab);
    }
  }
  $component_index ++;
}
close F;
my %query_vocab = ();
my $has_query_length = 0;
if (length($query_length_file) > 0) {
  open (F, "$query_length_file") or die "file $query_length_file cannot open\n";
  $has_query_length = 1;
  while(<F>) {
    chomp;
    m/(\S+)\s+(\S+)/ or next;
    $query_vocab{$1} = $2;
  }
  close F;
}
foreach my $kwid (keys %vocab ) {
  my $hRef = $vocab{$kwid};
  foreach my $fname (keys %$hRef){
    my $aRef = $vocab{$kwid}{$fname};
    if ($has_query_length) {
      my  $query_length = 0;
      $query_length = $query_vocab{$kwid} if exists $query_vocab{$kwid}; 
      print "$kwid $fname ", join " ", @$aRef," $query_length", "\n";
    } else {
      print "$kwid $fname ", join " ", @$aRef, "\n";
    }
  } 
}
