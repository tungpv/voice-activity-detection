#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
doit=false;
prepare_data=false;
prepare_fbank=false;
regular_train=false;
bn_regular_train=false;
plp_regular_train=false;
mfcc_regular_train=false;
forced_alignment1=false;
train_bnn1=false;
test_bnn1=false;

compose_bn1=false;

train_bnn2=false;
prepare_bn_feature=false;
prepare_plp_feature=false;
prepare_mfc_feature=false;

train_tandem=false;

run_bn1=false;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if $prepare_data; then
  echo -e "\nprepare data is in progress\n";
  prefix=data/107/llp;
  src_dir=language/107/llp/data/srs_bn_0420;
  for d in train dev; do
    sdir=$src_dir/$d;
    dir=$prefix/$d;
    [[ -d $dir ]] || mkdir -p $dir;
    for f in reco2file_and_channel \
           segments  spk2utt text  utt2spk stm ; do
      [[ -e $sdir/$f ]] || echo -e "WARNING: $sdir/$f does not exist.";
      if [[ -e $sdir/$f && ! -e $dir/$f ]]; then 
        cp $sdir/$f $dir/$f;
      fi
    done
     wav=swordfish/107_LLP/data/$d/wav.scp
     [[ -e $wav ]] || die "wave file $wav does not exist";
     cp $wav $dir;
  done
  # copy lang and lang test folder
  src_dir=swordfish/107_LLP/data
  for lang in lang_dialect_final{,_test}; do
    dir=data/107/llp;
    [[ -d $dir/$lang ]] || \
    cp -r $src_dir/$lang  data/107/llp/
  done
fi
# prepare filter bank feature
if $prepare_fbank; then
  prefix=data/107/llp/fbank;
  nj=20;
  for x in train dev; do
    dir=$prefix/$x;
    sbatch -P speech -n$nj -o $dir/$x.log \ 
    steps/make_fbank.sh --cmd "$train_cmd" --nj $nj \
    $dir $dir/_log $dir/_data || exit 1;
    steps/compute_cmvn_stats.sh $dir $dir/_log $dir/_data
  done
fi

# train filter bank based acoustic system
if $regular_train; then
  nj=30;
  stage=0;
  config=configs/am_conf;
  log=./log/train_regular_am.log;
  sbatch -p speech -n$nj -o $log \
  isteps/tandem/train_regular_am.sh \
  --stage $stage \
   $config
fi
#
if $bn_regular_train; then
  nj=30;
  stage=8;
  config=configs/bn.config;
  log=./log/train_bn_regular.log;
  sbatch -p speech -n$nj -o $log \
  isteps/tandem/train_regular_am.sh \
  --stage $stage \
   $config
fi
#
if $plp_regular_train; then
  nj=30;
  stage=0;
  config=configs/plp.config;
  log=./log/train_plp_regular.log;
  sbatch -p speech -n$nj -o $log \
  isteps/tandem/train_regular_am.sh \
  --stage $stage \
   $config

fi 
#
if $mfcc_regular_train; then
  nj=30;
  stage=7;
  config=configs/mfcc.config;
  log=./log/train_mfcc_regular.log;
  # sbatch -p speech -n$nj -o $log \
  isteps/tandem/train_regular_am.sh \
  --stage $stage \
   $config

fi 

# do alignment
if $forced_alignment1; then
  nj=10;
  prefix=data/107/llp/fbank;
  mdldir=exp/fbank/tri3sat;
  lang=data/107/llp/lang_dialect_final;
  for x in train_nn train_dev; do
    alidir=${mdldir}_ali_$x;
    steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
    $prefix/$x $lang $mdldir $alidir || exit 1; 
  done
fi
# train bottleneck neural network
if $train_bnn1; then
  echo -e "\nbottleneck 1 training\n";
  cuda_cmd=run.pl;
  prefix=data/107/llp/fbank;
  base=exp/fbank/tri3sat;
  lang=data/107/llp/lang_dialect_final;
  train_data=$prefix/train_nn;
  train_ali=${base}_ali_train_nn;
  dev_data=$prefix/train_dev;
  dev_ali=${base}_ali_train_dev;
  dir=exp/fbank/bn1;
  
  temp_prefix=`pwd`;
  clean_temp=false;
  bunch_size=256;
  cache_size=$[16384*16];
  $cuda_cmd $dir/_train_nnet.log \
  isteps/tandem/train_nnet.sh --hid-layers 2 \
  --temp-prefix $temp_prefix \
  --clean-temp $clean_temp \
  --hid-dim 1024 --bn-dim 80 --feat-type traps \
  --splice 5 --traps-dct-basis 6  --learn-rate 0.008 \
  --bunch-size $bunch_size \
  --cache-size $cache_size \
  $train_data $dev_data $lang $train_ali $dev_ali $dir || exit 1;

fi
# test if the trained bnn works fine
if $test_bnn1; then
  echo -e "\ntest bnn1\n"; 
  conf=conf/decode_dnn.config;
  data=data/107/llp/fbank/dev;
  mdldir=exp/fbank/tri3sat;
  graphdir=$mdldir/graph;
  prev_dec=$mdldir/decode_dev;
  nj=`cat $prev_dec/num_jobs`;
  dir=exp/fbank/bn1
  
  sbatch -p speech -n$nj -o log/decode_bn1.log \
  isteps/decode_nnet.sh --nj $nj \
  --cmd "$decode_cmd" --acwt 0.10 \
  --config $conf \
  $graphdir \
  $data $dir/decode_dev || exit 1;
  
fi
# compose feature transform with bn1
if $compose_bn1; then
  echo -e "\ncompose bn1\n";
  dir=exp/fbank/bn1;
  feature_transform=$dir/final.feature_transform.bn1;
  nnet-concat $dir/final.feature_transform \
  "nnet-copy --remove-last-layers=4 --binary=false $dir/final.nnet - |" \
      "utils/nnet/gen_splice.py --fea-dim=80 --splice=2 --splice-step=5 |" \
      $feature_transform
 
fi
# train bnn2 based on bnn1;
if $train_bnn2; then
  echo -e "\nbnn2 training on `hostname`@ `date`\n";
  dir=exp/fbank/bn2;
  cuda_cmd=run.pl;
  prefix=data/107/llp/fbank;
  base=exp/fbank/tri3sat;
  lang=data/107/llp/lang_dialect_final;
  train_data=$prefix/train_nn;
  train_ali=${base}_ali_train_nn;
  dev_data=$prefix/train_dev;
  dev_ali=${base}_ali_train_dev;
  bn1dir=exp/fbank/bn1;
  feature_transform=$bn1dir/final.feature_transform.bn1;
  temp_prefix=`pwd`;
  clean_temp=true;
  bunch_size=256;
  cache_size=$[16384*16];
  
  $cuda_cmd $dir/_train_nnet.log \
  isteps/tandem/train_nnet.sh --hid-layers 2 \
  --temp-prefix $temp_prefix \
  --clean-temp $clean_temp \
  --hid-dim 1024 --bn-dim 30 --feature-transform $feature_transform \
  --learn-rate 0.008 \
  --bunch-size $bunch_size \
  --cache-size $cache_size \
  $train_data $dev_data $lang $train_ali $dev_ali $dir || exit 1;

fi

# prepare bottleneck feature
if $prepare_bn_feature; then
  echo -e "\nprepare bottleneck feature\n";
  nj=20;
  prefix=data/107/llp/bn2;
  nnetdir=exp/fbank/bn2_mono0a;
  for x in train dev; do
    sdata=data/107/llp/fbank/$x;
    data=$prefix/$x;
    steps/make_bn_feats.sh --cmd "$train_cmd" \
    --nj $nj $data $sdata $nnetdir $data/_log \
    $data/_data || exit 1;
    # compute CMVN of the BN-features
    steps/compute_cmvn_stats.sh $data $data/_log \
    $data/_data || exit 1;
  done

fi
# prepare plp feature
if $prepare_plp_feature; then
  echo -e "\nprepare plp feature\n";
  nj=20;
  prefix=data/107/llp/plp;
  config=conf/plp.conf;
  for x in train dev; do
    sdata=data/107/llp/$x;
    data=$prefix/$x;   
    isteps/make_plp.sh --cmd "$train_cmd" \
    --nj $nj \
    --plp-config $config \
    $sdata $data/_log $data/_data || exit 1;
    steps/compute_cmvn_stats.sh $data $data/_log \
    $data/_data || exit 1;
  done
fi
# make mfcc feature
if $prepare_mfc_feature; then
  echo -e "\nprepare mfcc feature\n";
  nj=20;
  prefix=data/107/llp/mfcc;
  config=conf/mfcc.conf;
  for x in train dev; do
    sdata=data/107/llp/$x;
    data=$prefix/$x;   
    isteps/make_mfcc.sh --cmd "$train_cmd" \
    --nj $nj \
    --mfcc-config $config \
    $sdata $data/_log $data/_data || exit 1;
    steps/compute_cmvn_stats.sh $data $data/_log \
    $data/_data || exit 1;
  done
fi
# train tandem system
if $train_tandem; then
  echo -e "\ntrain tandem systems in sequence mode\n";
  config=configs/tandem.config.mfcc_bn2;
  log=log/run_tandem_mfcc_bn2.log;
  nj=30;
  sbatch -p speech -n$nj -o $log \
  ilocal/tandem/run_tandem.sh \
  --stage 1 \
  $config
 fi
# train bottleneck neural network 
if $run_bn1; then
  echo -e "start to traing bottleneck neural network 1";
  conf=configs/bn1.config;
  lang=data/107/llp/lang_dialect_final;
  mdldir=exp/fbank/mono0a;
  train_nn=data/107/llp/fbank/train_nn;
  train_dev=data/107/llp/fbank/train_dev;
  bn1dir=exp/fbank/bn1_mono0a;
  bn2dir=exp/fbank/bn2_mono0a;
  # nj=`grep train_nsub $conf | awk -F "=" '{gsub(/;/,"",$2);print $2;}'`
  # sbatch -p speech -n$nj -o log/bn1.log \
  isteps/tandem/train_bn1.sh --configs $conf \
  --stage 3 \
  $lang $mdldir $train_nn $train_dev $bn1dir $bn2dir
fi

