#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
stage=0;
# end user-defined variable
echo -e "\n$0 $@\n";
. parse_options.sh || die "parse_options.sh expected";

if [[ $# -ne 1 ]]; then
  echo -e "\n$0 <config_file>\n"; exit -1;
fi

conf=$1; shift;
[[ -e $conf ]] || \
die "config file $conf does not exist";
. $conf;

mono_ali=$exp_dir/mono_ali;
if [[ $stage -le 1 ]] ;then
  echo -e "\ntrain mono-phone\n";
  steps/tandem/train_mono.sh --nj $train_nsub \
   --cmd "$train_cmd" \
  $data1_train $data2_train  $lang  $exp_dir/mono0a \
  || exit 1;
  
  steps/tandem/align_si.sh --nj $train_nsub \
  --cmd "$train_cmd" \
  $data1_train $data2_train  $lang  $exp_dir/mono0a \
  $mono_ali || exit 1;
fi

tri1_ali=$exp_dir/tri1_ali;
if [[ $stage -le 2 ]]; then
  echo -e "\ntrain delta triphone\n";
  steps/tandem/train_deltas.sh --cmd "$train_cmd" \
  $nstate $ngauss $data1_train $data2_train  $lang \
  $mono_ali $exp_dir/tri1 || exit 1;
 
  steps/tandem/align_si.sh --nj $train_nsub \
  --cmd "$train_cmd" \
  $data1_train $data2_train  $lang \
  $exp_dir/tri1 $tri1_ali || exit 1;
fi

# lda+mllt tri2;
tri2_ali=$exp_dir/tri2_ali;
if [[ $stage -le 3 ]]; then
  echo -e "\ntrain lda+mllt triphone\n";
  steps/tandem/train_lda_mllt.sh --cmd "$train_cmd" \
  --splice-opts "$lda_ctx_opt" \
  $nstate $ngauss $data1_train $data2_train  $lang \
  $tri1_ali $exp_dir/tri2 || exit 1;
  
  steps/tandem/align_si.sh --nj $train_nsub \
  --cmd "$train_cmd" \
  $data1_train $data2_train  $lang \
  $exp_dir/tri2 $tri2_ali || exit 1;
fi
# sat tri3;
tri3_ali=$exp_dir/tri3_ali;
if [[ $stage -le 4 ]]; then
  echo -e "\ntrain lda+mllt sat triphone\n";
  [[ -e $exp_dir/tri3/final.mdl ]] || \
  steps/tandem/train_sat.sh  --cmd "$train_cmd" \
  $nstate $ngauss $data1_train $data2_train  $lang \
  $tri2_ali $exp_dir/tri3 || exit 1;
  
  steps/tandem/align_fmllr.sh --nj $train_nsub \
  --cmd "$train_cmd" \
  $data1_train $data2_train  $lang \
  $exp_dir/tri3 $tri3_ali;

fi

# evaluate the sat system with 
# unsupervised fmllr training recipe
if [[ $stage -le 5 ]]; then
  echo -e "\nevaluate sat system\n";
  sdir=$exp_dir/tri3;
  graphdir=$sdir/graph;
  dir=$sdir/decode_$devname;
  [[ -e $graphdir/HCLG.fst ]] || \
  utils/mkgraph.sh $lang_test \
  $sdir $graphdir;
  
  steps/tandem/decode_fmllr.sh --nj $train_nsub \
  --cmd "$decode_cmd" --config $tandem_decode_conf \
  $graphdir $data1_dev $data2_dev  $dir || exit 1;
fi
# sgmm
if [[ $stage -le 6 ]]; then
  echo -e "\nsubspace gmm\n";
  if [[ ! -e $exp_dir/ubm4/final.ubm ]]; then
    steps/tandem/train_ubm.sh --cmd "$train_cmd" \
    $ubm_num $data1_train $data2_train $lang \
    $tri3_ali $exp_dir/ubm4 || exit 1; 
  fi
  steps/tandem/train_sgmm2.sh --cmd "$train_cmd" \
  $pdf_num $substate_num \
  $data1_train $data2_train \
  $lang $tri3_ali \
  $exp_dir/ubm4/final.ubm  $exp_dir/sgmm4 || exit 1;

fi
# sgmm mmi
# disable this !
if [[ $stage -le 0 ]]; then
  echo -e "\nsgmm mmi:`date` \n";
  sgmm_ali=$exp_dir/sgmm4_ali;
  steps/tandem/align_sgmm2.sh --nj $train_nsub \
  --cmd "$train_cmd" --use-graphs true --use-gselect true \
  --transform-dir $tri3_ali \
  $data1_train $data2_train $lang \
  $exp_dir/sgmm4 $sgmm_ali || exit 1;
  # make den lattice
  steps/tandem/make_denlats_sgmm2.sh --nj $num_train \
  --acwt 0.0833 --max-active 7000 \
  --max-mem $[100*1000000] --cmd "$decode_cmd" \
  --transform-dir $tri3_ali \
  $data1_train $data2_train $lang \
  $exp_dir/sgmm4_{ali,denlats} || exit 1;

   echo `date` Starting SGMM MMI training
   steps/tandem/train_mmi_sgmm2.sh --cmd "$decode_cmd" \
   --boost $mmi_boost \
   --transform-dir $tri3_ali \
   $data1_train $data2_train $lang \
   $exp_dir/sgmm4_{ali,denlats,mmi_b$mmi_boost} || exit 1;
   echo  -e "\n`date` finished sgmm mmi training\n";
fi

# evaluate sgmmi
if [[ $stage -le 8 ]]; then
  echo `date` sgmm decode
  sdir=$exp_dir/sgmm4;
  graphdir=$sdir/graph;
  [[ -e $graphdir/HCLG.fst ]] || \
  utils/mkgraph.sh $lang_test \
  $sdir $graphdir;

  steps/tandem/decode_sgmm2.sh   --cmd "$train_cmd" \
  --nj $train_nsub \
  --config $sgmm_decode_conf \
  --transform-dir $exp_dir/tri3/decode_$devname \
  $graphdir $data1_dev $data2_dev \
  $exp_dir/sgmm4/decode_$devname || exit 1;
  
fi

