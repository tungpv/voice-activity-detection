#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
doit=false;
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if [ $# -ne 1 ]; then
  echo -e "\n$0 [options] <config>\n";
  exit 1;
fi 

config=$1; shift;

. $config || die "configure file $config does not exist";

if [ $stage -le 1 ]; then
  log=log/tandem/decode_sgmm2.log;
  # sbatch -p speech -n$nj -w kajiki4 --mem-per-cpu 5000MB -o $log \
  isteps/tandem/decode_sgmm2.sh --nj $nj \
  --acwt $acwt \
  --cmd run.pl \
  --stage 6 \
  --transform-dir $transform_dir \
  $graphdir \
  $data1 $data2 $decodedir $srcdir || \
  die "isteps/tandem/decode_sgmm2.sh";
fi

