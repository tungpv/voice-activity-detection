#!/bin/bash

# BABEL decoding script
# koried, 3/25/2013
# Revised 5/8/2013:  Better handling of stored config and resumability

function error_exit { echo ${1:-"unknown error"};  exit 1; }

. cmd.sh

# required variables, defaults
prefix=`pwd`;
data=
exp_dir=exp
prevent_redecode=false;
# recognizer 1:  mdl and lang dir, decoding config
rec1_mdl=tri4a
rec1_lang=data/lang_test
rec1_conf=conf/decode.config.tri4a

# recognizer 2:  mdl and lang dir, decoding config
rec2_mdl=sgmm5a
rec2_lang=data/lang_test
rec2_conf=conf/decode.config.sgmm5a

# parallelization (needs to match each step, b/c of the speaker infos)
num_decode=20

# resume options
stage=1
identifier=
first_pass_slf=false
no_second_pass=false

# local script variables (will not be stored)
prevent_overwrite=true

# you may want to use a template configuration...
. utils/parse_options.sh || exit 1;

if [ $# -lt 2 ]; then
  echo "usage: $0 features1 features2"
  echo " e.g. $0 --data data/dev1 --rec1-lang data/syll_lang --rec2-lang data/syll_lang --exp-dir exp_llp --num-decode 10 mfcc bn"
  exit 1;
fi

feats1=$1
feats2=$2
testdata="$prefix/data/$feats1/dev $prefix/data/$feats2/dev";
# we use a unique identifier for each decoding run
if [ "$identifier" == "" ]; then
  # none set, so this will be a new run, so make sure it is stage 1
  [ $stage == 1 ] || error_exit "$0: You requested a new decoding run, but stage $stage != 1"

  # write out config to temporary file (required to generate the ID)
  conf_temp=conf/decode_temp;
  [[ -d $conf_temp ]] || mkdir -p $conf_temp
  cat > $conf_temp/$$.config << EOF
# decoding run started: `date --rfc-3339=ns` as job $SLURM_JOB_ID
data=$data
exp_dir=$exp_dir
rec1_mdl=$rec1_mdl
rec1_lang=$rec1_lang
rec1_conf=$rec1_conf
rec2_mdl=$rec2_mdl
rec2_lang=$rec2_lang
rec2_conf=$rec2_conf
num_decode=$num_decode
EOF

  echo -n "Generating new identifier..."
  identifier=`md5sum $conf_temp/$$.config | awk '{print $1}'`
  echo "  $identifier"
  [ -n "$identifier" ] || error_exit "Could not generate MD5 identifier;  exitting."

  mv $conf_temp/$$.config $conf_temp/$identifier.config
else 
  # we have an idenfifier, so read the config file!
  [ -f $conf_temp/$identifier.config ] || error_exit "$0: no such file $conf_temp/$identifier.config -- Sure you're pointing to the right ID?"
  . $conf_temp/$identifier.config

  # document that we resumed the decoding run
  echo "# decoding resumed at `date --rfc-3339=ns` as job $SLURM_JOB_ID at stage $stage" >> conf/$identifier.config
fi

# a few tests
for i in $prefix/data/{$feats1,$feats2}/$data/feats.scp \
  $rec1_conf $rec2_conf {$rec1_lang,$rec2_lang}/{G,L}.fst; do
  [ -f $i ] || error_exit "$0: no such file $i"
done


# the graph dir is based on the lang_dir we're using
graph_dir1=`basename $rec1_lang`
graph_dir2=`basename $rec2_lang`    
decode_dir1=$rec1_mdl/decode_$identifier
decode_dir2=$rec2_mdl/decode_$identifier

# stage 1:  rec_dir1 (typically tri4a), speaker adaptive decoding
if [ $stage -le 1 ]; then
  echo `date` "Entering stage $stage"

  # prevent redecoding?
  if $prevent_redecode ; then
    [ -e $decode_dir1 ] && error_exit "Decoding directory $decode_dir1 already exists;  if you're resuming a decoding run, set --prevent-redecode false"
  fi

  # make decoding dir
  mkdir -p $decode_dir1

  # verify we have the model and the graph
  [ -f $rec1_mdl/final.mdl ] || error_exit "No such file $rec1_mdl/final.mdl"

  if [ ! -f $rec1_mdl/graph_$graph_dir1/HCLG.fst ]; then
    echo "Building decoding graph for $rec1_mdl ~ $rec1_lang"
    $decode_cmd /dev/stderr utils/mkgraph.sh $rec1_lang $rec1_mdl $rec1_mdl/graph_$graph_dir1 || exit 1;
  fi

  # decode...  
  touch $decode_dir1/.begin
  steps/tandem/decode_fmllr.sh --nj $num_decode --cmd "$decode_cmd" --config $rec1_conf \
    $rec1_mdl/graph_$graph_dir1 $testdata $decode_dir1
  touch $decode_dir2/.end

  if $first_pass_slf; then
    # generate SLF lattices for first pass
    echo `date` Generating SLF lattices
    steps/tandem/mk_aslf_lda_mllt.sh --transform-dir $decode_dir1 \
      --cmd "$decode_cmd" $rec1_mdl/graph_$graph_dir1 $testdata $decode_dir1 $decode_dir1/slf
  fi

  stage=$[$stage + 1]
fi

if "$no_second_pass"; then 
  exit 0;
fi

if [ $stage -le 2 ]; then
  echo `date` "Entering stage $stage"

  # prevent redecoding?
  if $prevent_redecode; then
    [ -e $decode_dir2 ] && error_exit "Decoding directory $decode_dir2 already exists;  if you're resuming a decoding run, set --prevent-redecode false"
  fi

  # make decoding dir
  mkdir -p $decode_dir2

  # verify we have the model and the graph
  [ -f $rec2_mdl/final.mdl ] || ( echo "No such file $rec2_mdl/final.mdl";  exit 1; )

  if [ ! -f $rec2_mdl/graph_$graph_dir2/HCLG.fst ]; then
    echo "Building decoding graph for $rec2_mdl ~ $rec2_lang"
    $decode_cmd /dev/stderr utils/mkgraph.sh $rec2_lang $rec2_mdl $rec2_mdl/graph_$graph_dir2 || exit 1;
  fi

  # decode...
  touch $decode_dir2/.begin
  steps/tandem/decode_sgmm2.sh --nj $num_decode --cmd "$decode_cmd" --config $rec2_conf --transform-dir $decode_dir1 \
    $rec2_mdl/graph_$graph_dir2 $testdata $decode_dir2
  touch $decode_dir2/.end

  stage=$[$stage + 1]
fi

if [ $stage -le 3 ]; then
  # generate SLF lattices for second pass
  echo `date` Generating SLF lattices
  steps/tandem/mk_aslf_sgmm2.sh --transform-dir $decode_dir1 \
    --cmd "$decode_cmd" $rec2_mdl/graph_$graph_dir2 $testdata $decode_dir2 $decode_dir2/slf
fi

