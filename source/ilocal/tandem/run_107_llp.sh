#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
prepare_data=false;
tandem_train=false;
# end user-defined variable

. parse_options.sh || die "parse_options. sh expected";

if [[ $# -ne 1 ]]; then
  die "Usage: $0 <config-file>";
fi
conf=$1; shift;
. $conf || die "configure $conf is not ready ?";

if $prepare_data; then
  destdir=language/107/llp/data
  prefix=swordfish/107_LLP;
  for dataname in srs_mfccpp_0420 srs_bn_0420; do
    sdir=swordfish/107_LLP/$dataname/data
    [[ -d $sdir ]] || die "source dir $sdir does not exist";
    for x in dev train; do
      dir=$destdir/$dataname/$x
      isteps/tandem/copy_kaldi_data.sh --prefix $prefix $sdir/$x $dir
    done
  done
  # copy lang folder
  for lname in lang_dialect_final lang_dialect_final_test; do
    lang=$prefix/data/$lname;
    [[ -d $lang ]] || die "lang $lang expected";
    cp -r $lang $destdir;
  done
fi
# begin tandem training
if $tandem_train; then
  sbatch -p speech -n$nj -o log/ifish/train_am_llp_tandem.log \
  ilocal/tandem/train_am_tandem.sh \
  --nj $nj \
  --lang-dir $lang \
  --exp-dir $exp_dir19 \
  $train19_1 $train19_2 ||
fi
