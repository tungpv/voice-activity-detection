#!/bin/bash

# reproduce Korbinian's results

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
doit=false;
importhtk_feature=false;
copy_feats=false;
copy_lang=false;
tandem_train=false;
tandem_decode=false;

# end user-defined variable

. parse_options.sh || die "parse_options. sh expected";

if [[ $# -ne 1 ]]; then
  die "Usage: $0 <config-file>";
fi
conf1=$1; shift;
. $conf1 || die "configure $conf1 is not ready ?";

# get full path
conf=$(cd `dirname $conf1`; x=`pwd`; fname=`basename $conf1`; echo $x/$fname);

for x in log conf local; do
  tdir=$TARGETDIR/$x;
  [[ -d $tdir ]] || mkdir -p $tdir
done
# make me confused
(\
 cd $TARGETDIR;
# link a couple of directories we'll need
resource=($iprefix/slocal  \
          $cpu_kaldi/egs/wsj/s5/steps \
          $cpu_kaldi/egs/wsj/s5/utils );
resNum=${#resource[@]};
for x in `seq 0 $[$resNum-1]`;do
  res=${resource[$x]};
  local_res=`pwd`/`basename $res`
  [[ -e $local_res ]] || ln -s $res;
done
# copy other source files
for x in cmd.sh path.sh; do
  file=$TEMPLATES/$x;
  [[ -e $file ]] || cp $file . ;
done
for x in mfcc.conf decode.config.tri4a decode.config.sgmm5a; do
  file=$TEMPLATES/$x;
  [[ -e conf/$x ]] || cp $file conf/$x;  
done

# link a scoring script;

if $dict_charbased; then
  scoring=charbased;
else
  scoring=regular;
fi
[[ -e local/score.sh ]] || \
ln -s $TEMPLATES/score-${scoring}.sh local/score.sh;)

# Get the dictionary ($srs_lexicon)
if $doit; then
  slocal/import_lex.sh $conf $srs_lexicon data/local/dict
fi
# we don't have any extra questions for now
touch data/local/dict/extra_questions.txt
# this should be disabled
if $doit; then
  utils/prepare_lang.sh \
  --share-silence-phones true \
  --make-individual-sil-models true \
  data/local/dict "<unk>" data/local/lang data/lang
fi
# prepare the data/lang_test
if $doit; then
  slocal/prepare_lang_test.sh data/lang $lm_arpa_file data/lang_test
fi
# now import the language pack
if $doit; then
  slocal/import_lp.sh $conf
fi
# import SRS features to kaldi format;  make sure to store those in $MODELSDIR
for x in data exp; do
  dir=$MODELSDIR/srs_mfccpp/$x;
  [[ -d $dir ]] || mkdir -p $dir;
  dir=$MODELSDIR/srs_bn/$x;
  [[ -d $dir ]] || mkdir -p $dir;
done
# since I am not familiar with what Korbinian has done,
# I am skipping it over right now, and I will
# just copy the feature
if $importhtk_feature; then
  echo -e "\nimport htk feature to make kaldi feature\n";
  sbatch slocal/importhtk_train.sh \
  --compute-cmvn true --fr 0:14 $srs_feat_train data/train/text srs_mfccpp train 
  sbatch slocal/importhtk_train.sh \
  --compute-cmvn true --fr 45:74 $srs_feat_train data/train/text srs_bn train 

  sbatch slocal/importhtk_dev.sh  \
  --compute-cmvn true --fr 0:14 $srs_feat_dev $BABEL_STM srs_mfccpp dev
  sbatch slocal/importhtk_dev.sh \
  --compute-cmvn true --fr 45:74 $srs_feat_dev $BABEL_STM srs_bn dev

  sbatch slocal/importhtk_dev.sh \
  --compute-cmvn true --fr 0:14 $srs_feat_eval ../templates/empty-stm srs_mfccpp eval
  sbatch slocal/importhtk_dev.sh \
  --compute-cmvn true --fr 45:74 $srs_feat_eval ../templates/empty-stm srs_bn eval

fi
# this is very confusing
if $copy_feats; then
  pdir=$iprefix/data/local
  for x in srs_mfccpp_0413 srs_bn_0413; do
    for y in train train_20k_short train_4k_short dev ; do
      echo -e "\ncopy $x/$y\n";
      sdir=$iprefix/swordfish/107/$x/data/$y;
      [[  -d $sdir ]] || die "source $sdir does not exist";
      dir=$pdir/$x/$y
      [[ -d $dir ]] || mkdir -p $dir;
      for f in cmvn.scp reco2file_and_channel  spk2utt text \
      feats.scp  segments  utt2spk stm; do
        [[ -e $sdir/$f ]] && cp $sdir/$f $dir/$f;
        [[ -e $dir/feats.scp ]] && sed -i "s: srs_bn_0413/: $iprefix/swordfish/107/srs_bn_0413/:" $dir/feats.scp;
        [[ -e $dir/feats.scp ]] && sed -i "s: srs_mfccpp_0413/: $iprefix/swordfish/107/srs_mfccpp_0413/:" $dir/feats.scp;
      done
    done
    [[ -d data/$x ]] ||cp -r $pdir/$x data/$x
  done 
fi
# copy data, I must ask korbinian for 
# this part
if $copy_lang; then
  pdir=$iprefix/swordfish/107/data
  dir=data
  for lang in lang_dialect_final lang_dialect_final_test; do
    [[ -d $pdir/$lang ]] || die "source lang $lang does not exist";
    [[ -d $dir/$lang ]] || cp -r $pdir/$lang $dir/
  done
fi
# begin tandem training
if $tandem_train; then
  prefix=language/107_FLP
  num_train=20;
  sbatch -p speech -n $num_train -o log/train_am.tandem.log \
  ilocal/tandem/train_am_tandem.sh \
  --num-train $num_train \
  --prefix $prefix \
  --lang-dir $prefix/data/lang_dialect_final \
  --exp-dir $prefix/exp/tandem/srs_bn_0413 \
  --stage 6 --do-sgmm true \
  $conf srs_{mfccpp,bn}_0413
fi
# decode
if $tandem_decode; then
  prefix=language/107_FLP;
  data2=srs_bn_0413;
  testdata=dev;
  mprefix=$prefix/exp/tandem/$data2;
  lang_test=$prefix/data/lang_dialect_final_test;
  nj=20;
  sbatch -p speech -n $nj -o log/decode.tandem.dev.log \
  ilocal/tandem/decode.sh \
  --num-decode $nj \
  --prefix $prefix \
  --data $testdata \
  --rec1-mdl $mprefix/tri4a \
  --rec1-lang $lang_test \
  --rec1-conf templates/decode.config.tri4a \
  --rec2-mdl $mprefix/sgmm5a \
  --rec2-lang $lang_test \
  --rec2-conf templates/decode.config.sgmm5a \
  srs_{mfccpp,bn}_0413
fi
