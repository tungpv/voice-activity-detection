#!/bin/bash 

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "cuda_path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl;
nj=10;
step=0
substep=0
data_prepare=false
bnf_tanh=false
bnf_dnn=false
tandem_llp_pitch=false
mbr_sys_comb=false
do_sst=false
utt_conf_sst=false
do_dnn_sst=false
llp_smbr_train=false
semi_sec_iter=false
decode_unsupervised=false
lang=data/llp/lang; lang_test=${lang}_test
samples_per_iter=4000000;  
max_mem=500000000;
state=2500;gauss=20000
pnorm_input_dim=3000; pnorm_output_dim=500; 
nonspeech_words=$lang/nonspeech_words.txt

# end tunable variable
bn_am_cf=conf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";

datastr="train dev10h" ;
if $data_prepare; then
  if [ $step -le 1 ]; then
    for x in $datastr; do
      echo -e "\n## plp feature extraction for data $x `date`\n"
      sdata=data/llp/$x; data=data/llp/feature/plp/$x; featdir=exp/llp/feature/plp/$x;
      [  ! -d $sdata ] && echo -e "##ERROR src data $sdata is not ready" && exit 1;
      [ -d $data ] || mkdir -p $data;
      cp $sdata/* $data; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
      steps/make_plp.sh --cmd "$cmd" --nj $nj --plp-config conf/plp.conf \
      $data  $featdir/_log $featdir/_data ||exit 1;
      steps/compute_cmvn_stats.sh $data $featdir/_log $featdir/_data ||exit 1;
    done 
    echo -e "\n## changing path succeeded `date`\n" && exit 0
  fi 
  if [ $step -le 3 ]; then
    for x in $datastr; do
      echo -e "\n## fbank feature extraction for data $x `date`\n"
      sdata=data/llp/$x; data=data/llp/feature/fbank/$x; featdir=exp/llp/feature/fbank/$x;
      [ -d $data ] || mkdir -p $data;
      cp $sdata/* $data; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
      steps/make_fbank.sh   --cmd "$cmd" --nj $nj --fbank-config conf/fbank.conf \
      $data  $featdir/_log $featdir/_data ||exit 1;
      steps/compute_cmvn_stats.sh $data $featdir/_log $featdir/_data || exit 1
    done
    echo -e "\ndone with fbank feature `date`\n" &&exit 0
  fi
  if [ $step -le 5 ]; then
    for x in $datastr; do
      echo -e "\n## kaldi pitch feature extraction for data $x `date` \n"
      sdata=data/llp/$x; data=data/llp/feature/pitch/$x; featdir=exp/llp/feature/pitch/$x;
      [ -d $data ] || mkdir -p $data;
      cp $sdata/* $data; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
      steps/make_pitch_kaldi.sh --cmd "$cmd" --nj $nj --pitch-config conf/plp.conf \
      $data $featdir/_log $featdir/_data || exit 1
    done
  fi
  if [ $step -le 7 ]; then
    for x in $datastr; do
      echo -e "\n## making plp+pitch for data $x `date`\n"
      plp=data/llp/feature/plp/$x; pitch=data/llp/feature/pitch/$x;
      data=data/llp/feature/plp_pitch/$x;
      featdir=exp/llp/feature/plp_pitch/$x;
      steps/append_feats.sh $plp $pitch  $data $featdir/_log $featdir/_data || exit 1
      steps/compute_cmvn_stats.sh $data $featdir/_log $featdir/_data ||exit 1;
    done
    echo -e "\n## making plp+pitch feature done `date`\n"
  fi
  if [ $step -le 9 ]; then
    for x in $datastr; do
      echo -e "\n## making fbank+pitch for dat $x `date`\n"
      fbank=data/llp/feature/fbank/$x; pitch=data/llp/feature/pitch/$x;
      data=data/llp/feature/fbk_pitch/$x;
      featdir=exp/llp/feature/fbk_pitch/$x;
      steps/append_feats.sh $fbank $pitch  $data $featdir/_log $featdir/_data || exit 1
      steps/compute_cmvn_stats.sh $data $featdir/_log $featdir/_data ||exit 1;
    done
    echo -e "\n## making fbank+pitch feature done `date`\n"
  fi
  echo -e "\ndata preparation done `date`\n" ; exit 0
fi
#
datadir=data/llp/feature/tandem; 
if [ $step -le 1 ]; then
  echo -e "\n## $0 making tandem feature `hostname` `date`\n"
  plp=data/llp/feature/plp; bnf=data/llp/feature/bnllp;
  feadir=exp/llp/feature/tandem;
  for x in train dev10h; do
    tandem="$plp/$x $bnf/$x"; data=$datadir/$x; fea=$feadir/$x;
    isteps/tandem/make_fmllr_feats.sh  --cmd "$train_cmd" $data $tandem dummy $feadir/_log $feadir/_data ||exit 1;
    steps/compute_cmvn_stats.sh $data $feadir/_log $feadir/_data || exit 1; 
  done 
  echo -e "\n## done  `date`\n"
fi 
ali=exp/llp/plp_pitch/bn/llp/s1/tri3sat_2500/ali_train; train=$datadir/train;
dev=$datadir/dev10h; sdir=exp/llp/tandem/tri3sat_${state}; sdir_booked=$sdir;
if [ $step -le 2 ]; then
  echo -e "\n## $0 train tandem SAT system `hostname` `date`\n"
  isteps/train_sat2.sh  --cmd "$train_cmd" $state $gauss $train  $lang  $ali $sdir||exit 1 
  echo -e "\n## $0 done `date`\n"
fi
ali=$sdir/ali_train; graph=$sdir/graph; ali_booked=$ali;
if [ $step -le 3 ]; then
  echo -e "\n## $0 align `hostname` `date`\n"
  isteps/align_fmllr.sh --nj 20 --cmd "$train_cmd" --delta-order 0  $train $lang $sdir $ali || exit 1
  echo -e "\n## done `date`\n"
fi 
dec_dir=$sdir/decode_dev10h;
if [ $step -le 4 ]; then
  echo -e "\n## $0 decode `hostname` `date`\n"
  utils/mkgraph.sh $lang_test $sdir $graph 
  isteps/decode_fmllr2.sh --nj 30 --cmd "$train_cmd" --delta-order 0 $graph $dev  $dec_dir ||exit 1
  echo -e "\n## done `date`\n"
fi
learn1=0.0005; learn2=0.00005; layers=3;
array_learn=("0.0005 0.00005" "0.0006 0.00006" "0.0007 0.00007");
if [ $step -le 5 ]; then
  echo -e "\n## $0 train tandem pnorm-dnn `hostname` `date`\n"
  step=$substep; step=$[step+500];
  for idx in 0 1 2; do
    learn_pair="${array_learn[$idx]}"
    learn1=`echo "$learn_pair" | awk '{print $1;}'`; learn2=`echo "$learn_pair"| awk '{print $2;}'`
    sdir=exp/llp/tandem/pnorm_learn${learn1}_${learn2}_leayers${layers}
    echo -e "\npnorm_input_dim=$pnorm_input_dim, pnorm_output_dim=$pnorm_output_dim, learn1=$learn1, learn2=$learn2, layers=$layers\n"
    if [ $step -le 501 ]; then
      echo -e "\n## $0 train pnorm layers=$layers `hostname``date`\n"
      if [ ! -e $sdir/final.mdl ]; then
        isteps/nnet2/train_pnorm.sh --initial-learning-rate $learn1 \
        --final-learning-rate $learn2 \
        --feat-type raw \
        --start-x 0 --stage -100 --num-threads 8 \
        --num-jobs-nnet 4  --num-hidden-layers $layers  \
        --samples-per-iter $samples_per_iter --pnorm-input-dim $pnorm_input_dim \
        --pnorm-output-dim $pnorm_output_dim --cmd run.pl --parallel-opts " " \
        --io-opts " " $train $lang $ali_booked $sdir || exit 1
      fi
      echo -e "\n## $0 ended `date`\n"
    fi
    data=$train; transform_dir=$ali_booked; nj=`cat $ali_booked/num_jobs`;
    ali=$sdir/ali_train;
  # a if [ $step -le 502 ]; then
  #    echo -e "\n##$0 making alignment`hostname` `date`\n"
  #    isteps/nnet2/align.sh --nj $nj --transform-dir $transform_dir \
  #    $data $lang $sdir $ali || exit 1
  #    echo -e "\n## done `date`\n"
  #  fi 
    transform_dir=exp/llp/tandem/tri3sat_2500/decode_dev10h;
    graph=$transform_dir/../graph;
    nj=`cat $transform_dir/num_jobs`; dev=data/llp/feature/tandem/dev10h;
    if [ $step -le 503 ]; then
      echo -e "\n## decoding `hostname` `date`\n"
      dir=$sdir/decode_dev10h;
      isteps/nnet2/decode.sh --nj $nj --feat-type raw \
      --transform-dir1 $transform_dir --beam 10 --lat-beam 6.0 \
      --max-mem $max_mem $graph $dev  $dir || exit 1

      echo -e "\n## done\n"
    fi
    echo -e "\n## done `date`\n"
  done
  echo -e "\n## `date`\n"
fi
