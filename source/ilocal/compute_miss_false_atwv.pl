#!/usr/bin/env perl
use strict;
use warnings;

my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  print STDERR "\n\n$0 ", join " ", @ARGV, "\n\n";
  die "\n\nUsage: $0 <kwid_list.txt> <bsum.txt>\n\n";
}

my ($kwlist, $bsum_fname) = @ARGV;

open(F, "$kwlist") or die "file $kwlist cannot open\n";
my %vocab = ();
while (<F>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  $vocab{$1} = $2;
}
close F;
open (F, "$bsum_fname") or die "file $bsum_fname cannot open\n";
my $start = 0;
my $N = 0;
my @array = (0, 0, 0);
my $aLen = 0;
while (<F>) {
  chomp;
  if( m/^Keyword/) {
    $start ++;
    s/^Keyword\s+//g;
  }
  if (/^Summary/) {
    $start = 0;
  }
  if ($start > 0) {
    s/\s+//g;
    my @A = split (/[\|]/);
    my $len = scalar @A; 
    next if ($len < 9);
    my ($kwid, $atwv, $pFA, $pMiss) = ($A[0], $A[$len-3], $A[$len-2], $A[$len-1]);
    if (exists $vocab{$kwid}) {
      $N ++;
      $aLen += $vocab{$kwid}; 
      $array[0] += $atwv; $array[1] += $pFA; $array[2] += $pMiss;
    }
  }
}
close F;
if ($N > 0) {
  my $s = sprintf ("%.1f %.6f %.6f %.6f",$aLen/$N, $array[0]/$N, $array[1]/$N, $array[2]/$N);
  print $s, "\n";
} 
