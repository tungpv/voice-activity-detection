#!/bin/bash

. path.sh
. cmd.sh
echo "LOG: $0 $@"
# begin options
cmd="slurm.pl --nodelist=node01"
nj=30
dataname=train
identifier=a
factor=
datadir=
totgauss=2500
states=6000
pdfs=75000
lda_mat_opts=
x_hour_train=

# end options

. parse_options.sh

cat <<END

$0 [options]:

cmd                    # value,   $cmd
nj                     # value,   $nj
dataname               # value,   $dataname
identifier             # value,   "$identifier"
factor                 # value,   $factor
datadir                # value,   $datadir
totgauss               # value,   $totgauss
states                 # value,   $states
pdfs                   # value,   $pdfs
lda_mat_opts           # value,   "$lda_mat_opts"
x_hour_train           # value,   "$x_hour_train"

END

if [ $# -ne 3 ]; then
  echo ""
  echo "Usage: $0 [options] <lang> <data> <dir>"
  echo ""
  exit 1
fi

lang=$1
data=$2
dir=$3

x=$dataname
id=$identifier
if [ ! -z "$lda_mat_opts" ]; then
  echo "$0: from lda+mllt training started @ `date`"
  exp=$dir
  index=5
  id=b
  use_lda_mat=$(echo "$lda_mat_opts"| perl -e '$s=<>; if($s=~ m/--use-lda-mat\s+(\S+)/){print $1;}else{exit 1;}') || \
  { echo "$0: use_lda_mat expected in $lda_mat_opts"; exit 1; }
  ali=$(echo "$lda_mat_opts"| perl -e '$s=<>; if ($s=~ m/--ali\s+(\S+)/){print $1;}else{exit 1;}') || \
  { echo "$0: ali expected in $lda_mat_opts"; exit 1; }
  dir=$exp/tri${index}$id;
  if [ ! -f $dir/final.mdl ]; then
    steps/train_lda_mllt.sh  --cmd "$cmd" --use-lda-mat $use_lda_mat --mllt-iters ''  $states $pdfs $data  $lang $ali $dir || exit 1
    index=$[index+1]
  fi
  ali=$dir/ali_$x;
  if [ ! -f $ali/ali_done ]; then
    steps/align_si.sh   --nj $nj --cmd "$cmd" $data  $lang $dir  $ali || exit 1
    touch $ali/ali_done
  fi
  dir=$exp/tri${index}$id; 
  if [ ! -f $dir/final.mdl ]; then
    steps/train_sat.sh  --cmd "$cmd" $states $pdfs $data  $lang $ali $dir || exit 1
    index=$[index+1]
  fi
  ali=$dir/ali_$x;
  if [ ! -f $ali/ali_done ]; then
    steps/align_fmllr.sh --cmd "$cmd" --nj $nj  $data $lang $dir $ali || exit 1
    touch $ali/ali_done
  fi
  dir=$exp/tri${index}$id; 
  if [ ! -f $dir/final.mdl ]; then
    steps/train_sat.sh  --cmd "$cmd" $states $pdfs $data  $lang $ali $dir || exit 1
  fi
  ali=$dir/ali_$x;
  if [ ! -f $ali/ali_done ]; then
    steps/align_fmllr.sh --cmd "$cmd" --nj $nj  $data $lang $dir $ali || exit 1
    touch $ali/ali_done
  fi
  echo "$0: from lda+mllt training finished @ `date`" && exit 0
fi

if [ ! -z $x_hour_train ]; then
  echo "$0: x_hour_train enabled"
  tot_hour=$(cat $data/segments | awk '{x+=$4-$3;}END{print x/3600;}') || \
  { echo "$0: ERROR, segments expected in $data"; exit 1; }
  [ $(echo "$tot_hour > 0 && $tot_hour > $x_hour_train" |bc -l) -gt 0 ] || \
  { echo "$0: ERROR, total hour is $tot_hour, require hour is $x_hour_train"; exit 1; } 
  tgt_factor=$(perl -e "print $x_hour_train/$tot_hour;")
  sdata=$data
  data=$sdata$x_hour_train
  [ -d $data ] || mkdir -p $data
  iutils/mksubset_spklist.pl $tgt_factor $sdata/spk2utt "feat-to-len scp:$sdata/feats.scp ark,t:- |"  >$data/spklist || exit 1
  utils/subset_data_dir.sh --spk-list $data/spklist $sdata $data || exit 1
  utils/fix_data_dir.sh $data
fi

if [ ! -z $factor ]; then
  echo "$0: use $facter of data to start-up"
  if [ $(echo "$factor <0|| $factor > 1" |bc -l) -eq 1 ]; then
    echo "$0: ERROR, fractional number $factor is illegal"
    exit 1
  fi
  if [ -z $datadir ]; then
    echo "$0: ERROR, datadir should be specified"
    exit 1
  fi
  subdata=$datadir/${x}.sub.$factor
  echo "$0: subdata=$subdata"
  if [ ! -f $data/feats.scp ]; then
    echo "$0: ERROR, file $data/feats.scp expected"
    exit 1
  fi
  [ -d $subdata ] || mkdir -p $subdata
  if [ ! -f $subdata/feats.scp ]; then
    iutils/mksubset_spklist.pl $factor $data/spk2utt "feat-to-len scp:$data/feats.scp ark,t:- |"  >$subdata/spklist || exit 1
    utils/subset_data_dir.sh --spk-list $subdata/spklist $data $subdata || exit 1
    utils/fix_data_dir.sh $subdata
  fi
  rx=$x
  rdata=$data
  data=$subdata
  x=${x}.sub.$factor
fi
exp=$dir
index=0
dir=$exp/mono${index}$id
if [ ! -f $dir/final.mdl ];then
  steps/train_mono.sh  --totgauss $totgauss \
  --nj $nj  --cmd "$cmd"  $data  $lang $dir || exit 1
  index=$[index+1]
fi
ali=$dir/ali_$x;
if [ ! -f $ali/ali_done ]; then
  steps/align_si.sh   --nj $nj --cmd "$cmd" $data  $lang $dir  $ali || exit 1
  touch $ali/ali_done
fi

if [ ! -z $rdata ]; then
  rstates=$states
  rpdfs=$pdfs
  states=2500
  pdfs=40000
fi

dir=$exp/tri${index}$id
if [ ! -f $dir/final.mdl ]; then
    steps/train_deltas.sh  --cmd "$cmd" $states $pdfs  $data  $lang $ali $dir || exit 1
  index=$[index+1]
fi 

ali=$dir/ali_$x
if [ ! -z $rdata ]; then
  ali=$dir/ali_$rx
  data=$rdata
  x=$rx
  states=$rstates
  pdfs=$rpdfs
fi
if [ ! -f $ali/ali_done ]; then
  steps/align_si.sh   --nj $nj --cmd "$cmd" $data  $lang $dir  $ali || exit 1
  touch $ali/ali_done
fi

dir=$exp/tri${index}$id;
if [ ! -f $dir/final.mdl ]; then
  steps/train_lda_mllt.sh  --cmd "$cmd" $states $pdfs $data  $lang $ali $dir || exit 1
  index=$[index+1]
fi
ali=$dir/ali_$x;
if [ ! -f $ali/ali_done ]; then
  steps/align_si.sh   --nj $nj --cmd "$cmd" $data  $lang $dir  $ali || exit 1
  touch $ali/ali_done
fi

dir=$exp/tri${index}$id; 
if [ ! -f $dir/final.mdl ]; then
  steps/train_sat.sh  --cmd "$cmd" $states $pdfs $data  $lang $ali $dir || exit 1
  index=$[index+1]
fi
ali=$dir/ali_$x;
if [ ! -f $ali/ali_done ]; then
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj  $data $lang $dir $ali || exit 1
  touch $ali/ali_done
fi
dir=$exp/tri${index}$id; 
if [ ! -f $dir/final.mdl ]; then
  steps/train_sat.sh  --cmd "$cmd" $states $pdfs $data  $lang $ali $dir || exit 1
fi

ali=$dir/ali_$x;
if [ ! -f $ali/ali_done ]; then
  steps/align_fmllr.sh --cmd "$cmd" --nj $nj  $data $lang $dir $ali || exit 1
  touch $ali/ali_done
fi

echo "$0: finished @`date` !"
