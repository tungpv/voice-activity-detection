#!/bin/bash

if [ ! -f $dir/final.mdl ]; then
  steps/nnet2/train_pnorm.sh \
  --initial-learning-rate $dnn_init_learning_rate \
  --final-learning-rate $dnn_final_learning_rate \
  --samples-per-iter $samples_per_iter \
  --num-jobs-nnet $num_jobs_nnet --num-threads $num_threads \
  --num-hidden-layers $dnn_num_hidden_layers \
  --pnorm-input-dim $dnn_input_dim \
  --pnorm-output-dim $dnn_output_dim \
  --cmd "$train_cmd" \
  $train  $lang $ali $dir || exit 1
fi
if [ -z $dev ]; then
  echo -e "\n## : dev $dev is not defined \n"; exit 0
fi
devname=`basename $dev`; decode=$dir/decode_$devname;
if [ ! -f $decode/.done ]; then
  mkdir -p $decode
  if [ ! -z $transform_dir ]; then nj=$(cat $transform_dir/num_jobs); fi
  steps/nnet2/decode.sh \
  --minimize $minimize --cmd "$decode_cmd" --nj $nj \
  ${beam:+ --beam $beam} ${lat_beam:+ --lat-beam $lat_beam} \
  ${skip_scoring:+ --skip-scoring $skip_scoring}  \
  ${transform_dir:+ --transform-dir $transform_dir} \
  $graph $dev  $decode | tee $decode/decode.log || exit 1

  touch $decode/.done
fi

