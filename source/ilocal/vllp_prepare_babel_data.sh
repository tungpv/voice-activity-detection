#!/bin/bash

. path.sh
. cmd.sh

# begin options
flp=false
stmfile=
nj=10
cmd="run.pl"
romanized=
user_lexicon_file=
dev_data_dir=
featdir=
# end options

. parse_options.sh


if [ $# -ne 4 ];then
  echo ""
  echo "Usage: $0 [options] <lang> <source-data-dir> <data-dir> <expdir>"
  echo ""
  exit 1
fi


lang=$1
sdata=$2
data=$3
exp=$4  

function PrintArg {

cat <<END

$(basename $0) [options/args]:
flp                 # value, $flp
stmfile             # value, "$stmfile"
nj                  # value, $nj
cmd                 # value, "$cmd"
romanized           # value, $romanized
user_lexicon_file   # value, "$user_lexicon_file"
dev_data_dir        # value, "$dev_data_dir"
featdir             # value, "$featdir"
lang                # value, "$lang"
sdata               # value, "$sdata"
data                # value, "$data"
exp                 # value, "$exp"

END
}

PrintArg;

# set -e           #Exit on non-zero return code from any command
set -o pipefail  #Exit if any of the commands in the pipeline will 
 
. $lang || exit 1

. conf/common_vars.sh || exit 1

train_data_dir=$sdata
transdir=$train_data_dir
# if ! $flp; then
#   transdir=$sdata/conversational/sub-train
# fi
trainlist=$sdata/train.list
find $train_data_dir/transcription/ -name "*.txt" | \
  perl -pe 's/.*\///g;s/\.txt//g; $_="$_";' > $trainlist

if [ ! -f $data/raw_train_data/prepare_done ]; then
  local/make_corpus_subset.sh "$train_data_dir" "$trainlist" $data/raw_train_data
  touch $data/raw_train_data/prepare_done
fi
[ -d $data/local ] || mkdir -p $data/local
if [ -z $user_lexicon_file ]; then
  if $flp; then
    lexicon_file=$sdata/conversational/reference_materials/lexicon.txt
  else
    lexicon_file=$sdata/conversational/reference_materials/lexicon.sub-train.txt
  fi
else
  lexicon_file=$user_lexicon_file
fi
if [[ ! -f $data/local/lexicon.txt ]]; then
  local/vllp_make_lexicon_subset.sh $transdir/transcription $lexicon_file $data/local/filtered_lexicon.txt
  local/prepare_lexicon.pl ${romanized:+--romanized} --phonemap "$phoneme_mapping"  \
    $lexiconFlags   $data/local/filtered_lexicon.txt $data/local
fi

[ -d $data/lang ] || mkdir -p $data/lang
if [[ ! -f $data/lang/L.fst  ]]; then
  utils/prepare_lang.sh \
  --share-silence-phones true \
    $data/local $oovSymbol $data/local/tmp.lang $data/lang
fi

if [[ ! -f $data/train/wav.scp  ]]; then
  mkdir -p $data/train
  local/vllp_prepare_acoustic_training_data.pl \
    --vocab $data/local/lexicon.txt --fragmentMarkers \-\*\~ \
    `pwd`/$data/raw_train_data  $data/train > $data/train/skipped_utts.log
fi

echo "$0: prepare dev data"
if [ ! -z $dev_data_dir ]; then
  echo "$0: making dev_data"
  devlist=$dev_data_dir/dev.list
  if [ ! -f $devlist ]; then
    find $dev_data_dir/transcription/ -name "*.txt" | \
    perl -pe 's/.*\///g;s/\.txt//g; $_="$_";' > $devlist
  fi
  if [ ! -f $data/raw_dev_data/prepare_done ]; then
    local/make_corpus_subset.sh "$dev_data_dir" "$devlist" \
    $data/raw_dev_data || exit 1
    touch $data/raw_dev_data/prepare_done
  fi
  if [[ ! -f $data/dev/wav.scp ]]; then
    mkdir -p $data/dev
    local/vllp_prepare_acoustic_training_data.pl \
    --fragmentMarkers \-\*\~ \
    `pwd`/$data/raw_dev_data $data/dev > $data/dev/skipped_utts.log || exit 1
  fi
fi
# We will simply override the default G.fst by the G.fst generated using SRILM
if [ ! -f $data/srilm/lm.gz  ]  && [ -f $data/dev/text ];  then
  local/train_lms_srilm.sh --dev-text $data/dev/text \
    --train-text $data/train/text $data $data/srilm 
fi

if [[ ! -f $data/lang/G.fst ]]; then
  local/arpa2G.sh $data/srilm/lm.gz $data/lang $data/lang
fi

if [ -z $stmfile ] && [ ! -f $data/dev/stm ]; then
  local/prepare_stm.pl --fragmentMarkers \-\*\~ $data/dev || exit 1
else 
  [ ! -f $data/dev/stm ] && local/vllp_augment_original_stm.pl $stmfile $data/dev
fi

echo "$0: make plp feature for $data1"
for x in train dev; do
  sdata1=$data/$x; data1=$sdata1/plp_pitch 
  [ -z $featdir ] && feat=$exp/feat/$x/plp_pitch;
  [ ! -z $featdir ] && feat=$featdir/$x/plp_pitch
  if [ ! -f $data1/plp_pitch_done ]; then
    [ -d $data1 ] || mkdir -p $data1;     cp $sdata1/* $data1/
    utils/fix_data_dir.sh $data1
    echo "$0: make plp feature for $data1"
    steps/make_plp_pitch.sh --cmd "$cmd" --nj $nj $data1 $feat/log  $feat/data || exit 1
    utils/fix_data_dir.sh $data1
    steps/compute_cmvn_stats.sh $data1 $feat/log $feat/data || exit 1
    utils/fix_data_dir.sh $data1
    touch $data1/plp_pitch_done
  fi
done

# echo "$0: stop_here_now !" && exit 1
