#!/usr/bin/perl
use strict;
use warnings;
use utf8;
print STDERR "LOG: $0 ", join (" ", @ARGV), "\n";
sub Usage {
  my $cmdName = $0;
  $cmdName =~ s/^.*\///g;
  my $sUsage=<<END;

Usage: $cmdName train1  train2 ... language1 language2

END
  print $sUsage;
  exit 1;
}

my $numArgs = scalar @ARGV;
Usage unless $numArgs%2==0 && $numArgs > 1;
my $fileNum = $numArgs / 2;
my %vocab = ();
my %vocab2 = ();
for(my $i = 0; $i < $fileNum; $i++) {
  my ($sFeatFile, $sLangName) = ($ARGV[$i], $ARGV[$i+$fileNum]);
  $sFeatFile .= "/feats.scp";
  open(File, "$sFeatFile") or die "$0: ERROR, file $sFeatFile cannot open\n";
  while(<File>) {
    chomp;
    m/(\S+)\s+(.*)/ or next; 
    my $uttName = $1;
    my $s = sprintf "$uttName $sLangName";
    if(not exists $vocab2{$uttName}) {
      $vocab2{$uttName}= $s;
    } else {
      $vocab{$uttName} ++;
    }
  }
  close File;
}
foreach my $uttName (keys %vocab2) {
  next if(exists $vocab{$uttName});
  print $vocab2{$uttName}, "\n"; 
}

my $dupNum = scalar (keys %vocab) * 2;
print STDERR "$0: $dupNum utterances are removed\n";
