#!/usr/bin/env perl
use strict;
use warnings;

scalar @ARGV ==1 or
die "\n\nUsage: cat wtext | $0 word_morph.txt > morph_text.txt\n\n";

my ($lex) = @ARGV;

my %vocab = ();

open L, "$lex" or die "file $lex cannot open\n";
while (<L>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $vocab{$1} = $2;
}
close L;

print STDERR "$0 stdin expected\n";
while (<STDIN>) {
  chomp;
  my @A = split (' ', $_);
  next if (@A <=0);
  my $s = "";
  for(my $i = 0; $i < @A; $i ++) {
    my $word = $A[$i];
    if (exists $vocab{$word}) {
      $s .= " $vocab{$word}";
    } else {
      $s .=" $word";
    }
  }
  @A = split (' ', $s);
  print join ' ', @A, "\n";
}
print STDERR "$0 stdin ended\n";
