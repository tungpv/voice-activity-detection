#!/bin/bash

echo -e "\n## LOG: $0 $@\n"

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl;
nj=10;
step=0
substep=0
beam=15;
lat_beam=10.0
make_trans=true
transform_dir=
max_mem=500000000;
skip_scoring=true
# end tunable variable
bn_am_cf=iconf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";

graph=$1; shift
data=$1; shift
transform_dir=$1; shift
dir=$1; shift;

echo -e "\n## $0 decoding started `date`\n"


if [ ! -f $transform_dir/trans.1 ]; then
  echo -e "\n## making transform started `date`\n"
  isteps/decode_fmllr2.sh  --make-trans $make_trans \
  --nj $nj --cmd "$cmd" --delta-order 0 \
  $graph  $data  $transform_dir ||exit 1
  echo -e "\n## making transform ended `date`\n"
fi

if [ ! -f $transform_dir/trans.1 ];then
  die "no trans file in $transform_dir folder"
fi
nj=`cat $transform_dir/num_jobs`
if [ ! -f $dir/lat.1.gz ]; then
  echo -e "\ntandem decoding started `date`\n"
  isteps/nnet2/decode.sh --cmd "$cmd" --nj $nj \
  --skip-scoring $skip_scoring \
  --feat-type raw --transform-dir1 $transform_dir \
  --beam $beam --lat-beam $lat_beam \
  --max-mem $max_mem $graph $data  $dir || exit 1

  echo -e "\ntandem decoding ended `date`\n"
fi

echo -e "\n## $0 decoding ended `date`\n"
