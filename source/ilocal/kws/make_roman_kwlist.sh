#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}

# desgin: ilocal/kws/make_roman_kwlist.sh word-lexicon.txt morph-lexicon.txt morph.mdl  source-tamil-kwlist.xml  target-dir

. path.sh || die "path.sh expected";
echo -e "\nLOG: $0 $@\n";
echo -e "\nLOG: runing on `hostname` `date`\n"

phones=

. parse_options.sh || die "parse_options.sh expected";

set -u
## set -e
set -o pipefail

if [ $# -ne 6 ]; then
  echo -e "\n\n dump roman kwlist.txt from tamil kwlist.xml file"
  echo -e "Example:\n"
  echo -e "$0 <wlexicon.txt> <mlexicon.txt> <noprons(for morph)> <morph_model> <tamil_kwlist.xml> <target_dir>\n\n"
  exit 0
fi

wlex=$1; shift
mlex=$1; shift
noprons=$1; shift
mdl=$1; shift
kwxml=$1;  shift
dir=$1; shift;


for x in $wlex $mlex $noprons $mdl $kwxml; do
  [  -e $x ] || die "file $x is not there"
done

sdir=$dir; dir=$sdir/temp; mkdir -p $dir

[ -e $dir/tamil_keywords.txt ] || \
isteps/kws/make_kwlist_from_xml.pl $kwxml > $dir/tamil_keywords.txt

[ -e $dir/tamil_keywords.txt ] || die "tamil_keywords.txt expected"

if [ ! -f $dir/roman_keywords_01.txt ]; then
  paste <(cut -f 1 $dir/tamil_keywords.txt) \
    <( cut -f 2- $dir/tamil_keywords.txt| sed -e 's:[-_]: :g' | \
    perl -e '
      while(<STDIN>) {
        ($conv) = @ARGV;
        chomp; @A = split / /;
        @B=();
        for($i=0; $i< scalar @A ; $i++) {
          $w1 = $A[$i];
          open CONV, "$conv $w1|";
          $w = <CONV>; 
          close CONV;
          push @B, $w;
        }
        print join " ", @B, "\n";
      }' tamil2roman
    ) > $dir/roman_keywords_01.txt 
fi

echo -e "\n## $0 be_\n"
[ -e $dir/roman_keywords_01.txt ] || die "$dir/roman_keywords_01.txt expected";

(cat  $dir/roman_keywords_01.txt | grep "ERROR"   > $dir/tamil2roman_failure.txt )

echo -e "\n## check file $dir/roman_keywords_01.txt\n"

invW=$dir/roman_invw_keywords_01.txt;
oovW=$dir/roman_oovw_keywords_01.txt; 
if [[ ! -f $invW || ! -f $oovW ]]; then
  echo -e "\n## for word case\n"
  iutils/kws/dump_keywords.pl $wlex $dir/roman_keywords_01.txt $invW $oovW ||\
  die "dumping word keyword error";
fi

if [ ! -z $phones ]; then
  [ -e $phones ] || die "you specify an empty phone files" 
  if [ ! -f $sdir/phone_mapping.fst ]; then
    echo -e "\n## making phone_mapping.txt file\n"
    cat $phones | \
    grep -v '<eps>'|\
    grep -v '#' | \
    awk '{s=$1; gsub(/_[SBIE]/,"",s); print $1, "\t",s; }' > $sdir/phone_mapping.txt
    cat $phones | \
    iutils/kws/make_phone_map.pl  $sdir/phone_mapping.txt $sdir/mapped_phones.txt 
    echo -e "\n## making phone mapping transducer `date`\n"
    cat $sdir/phone_mapping.txt |\
    utils/make_lexicon_fst.pl  - |\
    fstcompile --isymbols=$sdir/mapped_phones.txt \
    --osymbols=$phones - |\
    fstinvert | fstarcsort --sort_type=olabel > $sdir/phone_mapping.fst
  fi
fi

invM=$dir/roman_invm_keywords_01.txt;
oovM=$dir/roman_oovm_keywords_01.txt; 
if [[ ! -f $invM || ! -f $oovM ]]; then
  echo -e "\n## for morph case\n"
  if [ ! -f $sdir/morph/text ]; then
    ilocal/morph/text_segment.sh --from 2  $mdl $dir/roman_keywords_01.txt $sdir/morph 
  fi
  [  -e $sdir/morph/text ] || die "text_segment.sh failed to generate $sdir/morph/text"
  
  cat $sdir/morph/text | \
  ilocal/morph/filter_noprons.pl $noprons > $sdir/morph/text_noprons_removed
 
  iutils/kws/dump_keywords.pl $mlex $sdir/morph/text_noprons_removed $invM $oovM || \
  die "dumping morph keyword error"
fi

echo -e "\ngenerate two word lists, one for word system, and the other for morph system\n"
cat $invW $oovW $invM $oovM | sort > $sdir/wkeywords.txt
cat $invM $oovM | sort > $sdir/mkeywords.txt


