#!/usr/bin/env perl
use strict;
use warnings;

scalar @ARGV == 1 or
die "\n\nUsage: $0 cat wlex.txt | $0 mlex.txt > xlex.txt\n\n";
my ($mlex) = @ARGV;

open L, "<$mlex" or die "file $mlex cannot open";
my %vocab = ();
while (<L>) {
  chomp;
  my @A = split(' ', $_);
  next if (scalar @A < 2);
  my $word = shift @A;
  my $pron = join ' ', @A;
  $vocab{$word} = $pron;
}
close L;

print STDERR "$0 stdin expected\n";
while (<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my ($word) = ($1);
  next if (exists $vocab{$word});
  $vocab{$word} = $word;
}
print STDERR "$0 stdin ended\n";

foreach my $word (keys %vocab) {
  print $word, "\t", $vocab{$word}, "\n"
}
