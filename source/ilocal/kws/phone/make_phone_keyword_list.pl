#!/usr/bin/env perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 4) {
  die "\n\nUsage: $0 <word_lexicon.txt> <word_keywords.txt>  <inv_phone_keywords.txt>  <oov_word_keywords.txt>\n\n";
}
my ($wlex, $word_kwfname, $inv_phone_kwfname, $oov_word_kwfname) = @ARGV;

sub LoadLexicon {
  my ($fName, $vocab) = @_;
  open F, "$fName" or die "file $fName cannot open";
  while (<F>) {
    chomp;
    s/ +/ /g;
    my @a = split /\s+/;
    my $word = shift @a;
    if ( not exists $$vocab{$word}) {
      my @array = ();
      $$vocab{$word} = \@array;
      my $pArray = $$vocab{$word};
      push @$pArray, \@a;
    } else {
      my $pArray = $$vocab{$word};
      push @$pArray, \@a;
    }
  }
  close F;
}

sub OutputKW {
  my ($kwId, $oArray, $FILE) = @_;
  my $size = scalar @$oArray; 
  if($size <1) {
    print STDERR "$kwId\n"; return;
  }
  print $FILE "$kwId"; 
  for(my $idx = 0 ; $idx < scalar @$oArray; $idx ++) {
    my $pArray = $$oArray[$idx];
    for (my $j = 0; $j < scalar @$pArray; $j ++ ) {
      print $FILE " $$pArray[$j]";
   }
  }
  print $FILE "\n";
}

sub WriteOutPhoneKW {
  my ($kwId, $pronArray, $FILE) = @_;
  my $aLen = scalar @$pronArray;
  
  my (@bArray, @fArray) = (); 
  for (my $idx = 0; $idx < $aLen; $idx ++) {
    my @a = ();
    push @bArray, \@a;
  }
  my ($i, $j) = (0,0);
  while ($i < $aLen && $i >= 0) {
    my $pArray = $$pronArray[$i];
    my $bLen = scalar @$pArray;
      
    my $p2A = $$pArray[$j];
    my @iArray = ();
    for (my $idx = 0; $idx < scalar @$p2A; $idx ++) {
      push @iArray, $$p2A[$idx];
    }
    push @fArray, \@iArray;
    my $bStack = $bArray[$i];
    push @$bStack, $j;
    if ($i == $aLen -1) { # sweep finished
      OutputKW($kwId, \@fArray, $FILE);
      pop @fArray;
      $i --;
    }
   
    $i ++  if $i < $aLen -1;
    $bStack = $bArray[$i];
    if (scalar @$bStack == 0) {
      $j = 0;
    } else {
      $j = pop @$bStack;
      $pArray = $$pronArray[$i];
      $bLen = scalar @$pArray;
      while ($j == $bLen -1) {
        $i --;
        last if ($i < 0);
        pop @fArray;
        $bStack = $bArray[$i];
        $j = pop @$bStack;
        $pArray = $$pronArray[$i];
        $bLen = scalar @$pArray;
      }
      last if ($i < 0);
      $j ++;
    }
  }   
}
## ------ main entrance ------
my (%phoneVocab, %lexVocab);
LoadLexicon($wlex, \%lexVocab);

open (F, "$word_kwfname") or die "$0: file $word_kwfname cannot open\n";

open (Fout1, ">$inv_phone_kwfname") or 
die "$0: file $inv_phone_kwfname cannot be created\n";
open (Fout2, ">$oov_word_kwfname") or
die "$0: file $oov_word_kwfname cannot be created\n";

while (<F>) {
  chomp;
  s/ +/ /g;
  my @a = split /\s+/;
  my $kwId = shift @a;
  my $num = scalar @a;
  my @newArray = (); my $oov = 0;
  for (my $idx = 0; $idx < $num; $idx ++) {
    my $word = $a[$idx ];
    if (not exists $lexVocab{$word}) {
      print Fout2 "$_\n"; $oov = 1; $idx = $num; next;
    }
    my $pArray = $lexVocab{$word};
    push @newArray, $pArray;
  }
  next if ($oov == 1);
  if (scalar @newArray <1) {
    print STDERR "$_: no pron\n"; next;
  }
  WriteOutPhoneKW($kwId, \@newArray, \*Fout1);
}

close (F);
close (Fout1);
close (Fout2);
