#!/bin/bash

echo -e "\n## LOG: $0 $@\n"

. path.sh || exit 1
. cmd.sh || exit 1

cmd=run.pl
silence_opt=
min_lmwt=8
max_lmwt=12
frame_dur=0.01
beta=999.9
duptime=0.6
skip_optimization=false
max_states=150000;
max_silence_frames=50
word_ins_penalty=0;
ntrue_scale=1.0;
phone_map=
silphone=
silphone_opt=
prune_beam=6.0;
prune_lmwt=12;
strict=true
skip_scoring=false
id=
. parse_options.sh

if [ $# -ne 6 ]; then
  echo -e "\n\nUsage:\n"
  echo -e "$0 [optionts] lang kwsource kwlist keywords model_dir latdir"
  exit 1
fi

lang=$1; shift
kwsource=$1; shift;
kwlist=$1; shift
keywords=$1; shift
sdir=$1; shift;
latdir=$1; shift

if [[ ! -e $keywords  || ! -e $kwlist ]]; then
  echo -e "\n## ERROR: keywords $keywords is not there\n"
  exit 1
fi
# each time we should do this
cp $kwlist $kwsource/kwlist.xml
cp $keywords $kwsource/keywords.fsts

if [ ! -z $id ]; then
  id=_$id
fi
indexdir=$latdir/index$prune_beam;
model_flags="--model $sdir/final.mdl" 
if [ ! -f $latdir/num_jobs ]; then
  nj=`ls $latdir/lat.*.gz | wc -l`; echo $nj > $latdir/num_jobs;
fi

echo -e "\n## $0 making index `hostname` `date`\n"
for lmwt in `seq $min_lmwt $max_lmwt`; do
  dir=$indexdir/$lmwt;
  [ -d $dir ] || mkdir -p $dir;
  acwt=`perl -e "print (1.0/$lmwt)"`;
  logdir=$dir/log; 
  nj=`cat $latdir/num_jobs`; echo $nj >  $dir/num_jobs
  if [ ! -f $dir/.make_index.done ]; then
    ilocal/kws/phone/make_index.sh $silence_opt \
    --cmd "$cmd" --acwt $acwt --prune-beam $prune_beam  $model_flags\
    --skip-optimization $skip_optimization --max-states $max_states \
    --word-ins-penalty $word_ins_penalty --max-silence-frames $max_silence_frames\
    $kwsource  $lang $latdir $dir  || exit 1
    touch $dir/.make_index.done
  fi
  kwsdir=$dir/kws$id;
  sdir=$kwsource;  
  if [ ! -f $kwsdir/.search_index.done ]; then
    steps/search_index.sh --cmd "$cmd" \
    --indices-dir $dir $sdir $kwsdir 
    touch $kwsdir/.search_index.done
  fi
done

if [ ! -f $indexdir/$max_lmwt/kws$id/kwslist.xml ]; then
  $cmd LMWT=$min_lmwt:$max_lmwt $indexdir/write_normalized$id.LMWT.log \
  set -e ';' set -o pipefail ';' \
  cat $indexdir/LMWT/kws$id/result.* \| \
  utils/write_kwslist.pl  --Ntrue-scale=$ntrue_scale --beta=$beta \
  --flen=$frame_dur --duration=`cat $kwsource/duration` \
  --segments=$kwsource/segments --normalize=true \
  --duptime=$duptime --remove-dup=true\
  --map-utter=$kwsource/utter_map --digits=6 \
  - $indexdir/LMWT/kws$id/kwslist.xml || exit 1

  $cmd LMWT=$min_lmwt:$max_lmwt $indexdir/write_normalized$id.LMWT.log \
  set -e ';' set -o pipefail ';' \
  cat $indexdir/LMWT/kws$id/result.* \| \
  utils/write_kwslist.pl  --Ntrue-scale=$ntrue_scale --beta=$beta \
  --flen=$frame_dur --duration=`cat $kwsource/duration` \
  --segments=$kwsource/segments --normalize=false \
  --duptime=$duptime --remove-dup=true\
  --map-utter=$kwsource/utter_map --digits=6 \
  - $indexdir/LMWT/kws$id/kwslist_nonorm.xml || exit 1

fi
if $skip_scoring; then
  echo -e "\n## skip scoring `date`\n";
  exit 0
fi
if [ ! -f $indexdir/$max_lmwt/kws$id/sum.txt ]; then
  echo -e "\n## $0 ATWV evaluation `hostname` `date` \n"
  run.pl LMWT=$min_lmwt:$max_lmwt \
  $indexdir/LMWT/kws$id/log/scoring.LMWT.log \
  ilocal/kws/phone/ntu_kws_score.sh  $kwsource \
  ${indexdir}/LMWT/kws$id || exit 1;
  echo -e "\n## $0 done `date`\n"
fi

echo -e "\n## done `date`\n"

