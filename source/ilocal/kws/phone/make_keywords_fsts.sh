#!/bin/bash

. path.sh
. cmd.sh

# begin option
g2ptool=g2p.py
g2pmodel=
min_length=5
max_length=100
sub_seq_len=6
proxy_opts=
# end option

. parse_options.sh

if [ $# -ne 4 ]; then
  echo "Usage: $0 [options] <keywordlist.xml> <lang> <word-lexicon> <dir>"
  echo "e.g. $0 keywordlist.xml data/phone/lang data/word/local/lexicon.txt dir"
  echo ""
  echo "Main options"
  echo "--g2pmodel <g2p_model>                   # g2p model"
  exit 1
fi

kwxml=$1
lang=$2
wlex=$3
dir=$4

[ -d $dir ] || mkdir -p $dir
if [ ! -f $dir/word_keywords.txt ]; then
  isteps/kws/make_kwlist_from_xml.pl $kwxml | \
  perl -pe 'chomp; @A = split(/\s+/); $kwid =shift @A; push @B, $kwid;
    for($i=0; $i<@A; $i++){$w =$A[$i]; $w =~ s/[\-_]/ /g; push @B, $w;} 
    $_ = join " ", @B, "\n"; @B=();' > $dir/word_keywords.txt
fi
if [[ ! -f $dir/inv_phone_keywords.txt || ! -f $dir/oov_word_keywords.txt ]]; then
  ilocal/kws/phone/make_phone_keyword_list.pl   $wlex  $dir/word_keywords.txt  \
  $dir/inv_phone_keywords.txt  $dir/oov_word_keywords.txt
  
fi
target_kw_txt=$dir/inv_phone_keywords.txt

if [ ! -z $g2pmodel ]; then
  echo "$0: g2p to label oov words"
  ilocal/kws/phone/dump_oov_words.sh --from 2 $wlex $dir/oov_word_keywords.txt $dir/oov_words.txt || exit 1 
  if [ ! -f $dir/oov_words_g2p_lex.txt ]; then
    ilocal/kws/phone/tamil_g2p.sh $g2pmodel $dir/oov_words.txt  $dir/g2p || exit 1
    if [ ! -f $dir/g2p/oov_words.lex ]; then
      echo "$0: file $dir/g2p/oov_words.lex expected"; exit 1
    fi
    (cd $dir; ln -s ./g2p/oov_words.lex oov_words_g2p_lex.txt)
  fi
  ilocal/kws/phone/make_phone_keyword_list.pl "cat $wlex $dir/oov_words_g2p_lex.txt|" \
  $dir/oov_word_keywords.txt $dir/g2p_phone_keywords.txt   $dir/oov_g2p_word_keywords.txt
  oov2_num=`wc -l < $dir/oov_g2p_word_keywords.txt`
  if [ $oov2_num -gt 0 ]; then
    echo "$0: $oov_num of words are failed to transcribe with g2p tool, check $dir/oov_g2p_word_keywords.txt"
    head -10 $dir/oov_g2p_word_keywords.txt
  fi
  cat $dir/{inv,g2p}_phone_keywords.txt |sort -u  | \
  perl -e '($minlen, $maxlen, $fname1, $fname2) = @ARGV;
    open(F1, ">$fname1") or die "file $fname1 cannot open\n";
    open(F2, ">$fname2") or die "file $fname2 cannot open\n";
    while(<STDIN>) { chomp; @A = split(/\s+/); $len=scalar @A; 
      if($len<=$minlen || $len>=$maxlen) { print F2 "$_\n";  } else {
        print F1 "$_\n";
      }
    } close F1; close F2;
 ' $min_length $max_length   $dir/phone_keywords.txt  $dir/shorter_phone_keywords.txt
  target_kw_txt=$dir/phone_keywords.txt
fi

if [ ! -z "$proxy_opts" ] ; then
  echo "$0: [phone_confusion] is utilized"
  temp=$dir/phone_confusion
  [ -d $temp ] || mkdir $temp
  perl -pe 'chomp; $_="$_\t$_\n";' <(awk '{print $1;}' $lang/words.txt | \
  grep -E -v '<eps>|^#')  > $temp/L1.lex;
  [ -f $temp/L2.lex ] || (cd $temp; ln -s ./L1.lex L2.lex)
  [ -f $temp/words.txt ] || ( lang_path=$(cd $lang; pwd); cd $temp; ln -s $lang_path/words.txt words.txt)
  [ -f $temp/keywords.txt ] || ( cd $temp; ln -s ../`basename $target_kw_txt` keywords.txt )
  if [ ! -f $temp/split/.done ]; then
    local/generate_proxy_keywords.sh $proxy_opts $temp || exit 1
    touch $temp/split/.done
  fi
  cat $temp/split/keywords.*.txt |\
  awk '{$2=""; print; }' | \
  perl -ne "s/ +/ /g; @A=split(/\s+/); if(@A >$sub_seq_len){print;} " | \
  utils/int2sym.pl -f 2-  $lang/words.txt | \
  cat - $target_kw_txt | \
  perl -e 'while(<STDIN>){chomp; @A=split(/\s+/); $kwid=shift @A;
    $str=join(" ", @A); if(exists $vocab{$str}) { next; } else { $vocab{$str} ++; print "$_\n";  } 
  }' | sort  > $dir/confusion_phone_keywords.txt
  target_kw_txt=$dir/confusion_phone_keywords.txt
fi

if [ $sub_seq_len -ge 5 ]; then
  echo "$0: [partial_search] is utilized"
  cat $target_kw_txt | \
  perl -e '($sub_seq_len) = @ARGV; while(<STDIN>){ chomp;
    @A = split (/\s+/);  $kwid = shift @A; 
    print "$_\n";
    $str = join " ", @A;  $vocab{$str}++;
    if(@A > $sub_seq_len) { 
      for($i=0; $i<= @A -$sub_seq_len; $i++) {
        for($j=$i; $j<$i+$sub_seq_len; $j++) { push @B, $A[$j]; }
        $str = join " ", @B;
        if (exists $vocab{$str}) {
          @B = (); next;
        }
        $vocab{$str} ++; 
        print $kwid, " ", $str, "\n";
        @B = ();
      }  
    } }' $sub_seq_len |sort -u > $dir/partial_phone_keywords.txt
  target_kw_txt=$dir/partial_phone_keywords.txt
fi

if [ ! -f $dir/keywords.fsts ]; then
  cat $target_kw_txt | \
  sym2int.pl --map-oov 0 -f 2- $lang/words.txt \
  > $dir/keywords.int 2> $dir/keywords.log
  cat $dir/keywords.int | \
  awk '{if (/ 0 /||/ 0$/){}else{print ;}}'  > $dir/keywords_no_oov.int
  transcripts-to-fsts ark:$dir/keywords_no_oov.int ark,t:$dir/keywords.fsts || exit 1
fi
