#!/bin/bash

# begin option
from=2
# end option

. utils/parse_options.sh

if [ $# -ne 3 ]; then
  echo "Usage: $0 [options]  <lexicon.txt> <keyword.txt> <oov_words.txt>"
  echo "e.g. $0 data/local/lexicon.txt keywords.txt oov_words.txt"
  echo ""
  echo "Main options"
  echo "--from <field-index>             # word position index from which to check (default 2)"
  echo ""
  exit 1
fi
lex=$1
text=$2
oov=$3
for x in $lex $text; do
  if [ ! -f $x ]; then
    echo "$0: file $x expected"
    exit 1
  fi
done

cut -d " " -f $from- $text | \
perl -e '($fname) = @ARGV; open (F, "$fname") or die "file $fname cannot open\n";
  while (<F>) {chomp; m/(\S+)\s+(.*)/ or next; $vocab{$1} ++; } close F;
  while (<STDIN>) {chomp; @A =split(/\s+/); for($i=0;$i<@A;$i++){$w=$A[$i];
    if (not exists $vocab{$w}) {print $w,"\n";} }  } 
 '  $lex |sort -u > $oov  || exit 1

