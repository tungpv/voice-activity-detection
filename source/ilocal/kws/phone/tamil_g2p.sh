#!/bin/bash

# this script labels tamil word with pronunciation using 
# tamil-roman g2p model in batch mode. That is, given a 
# tamil word list, a g2p lexicon is generated.

# Example:  ilocal/kws/tamil_g2p.sh g2p_model tamil_word_list.txt dir


if [ $# -ne 3 ]; then
  echo "Usage: $0 <g2p_model> <tamil_word_list.txt> <dir>"
  exit 1
fi

model=$1
twordlist=$2
dir=$3

if ! which tamil2roman >/dev/null; then
  echo "$0: tamil2roman not found"
  exit 1
fi
if ! which g2p.py >/dev/null; then
  echo "$0: g2p.py not found"
  exit 1
fi
[ -d $dir ] || mkdir -p $dir

echo "$0: tamil to roman conversion"
cat $twordlist | \
perl -e '
  ($conv) = @ARGV;
  while (<STDIN>) {
    chomp; $tw=$_;  @A = split(/[\-_\*]/);   @R =();
    for($i=0; $i<@A; $i++) {
      $t =$A[$i];
      next if length $t == 0;
      open F, "$conv $t|";
      $r = <F>;
      push @R, $r;
    }
    $r = join " ", @R;
    if ($r =~ /ERROR/) {
      print STDERR "$tw $r\n";
      next;
    }
    print "$tw\t$r\n";
  }
' tamil2roman  > $dir/tamil_roman.txt
echo "$0: generate roman word list"
cut -f 2- $dir/tamil_roman.txt | \
awk '{for(i=1;i<=NF;i++){ print $i; }}' | \
sort -u  > $dir/roman.txt

echo   "g2p.py --model=$model --apply=$dir/roman.txt > $dir/roman.lex"
g2p.py --model=$model --apply=$dir/roman.txt > $dir/roman.lex

fname=$(basename $twordlist|perl -pe 's/\.[^\.]+/\.lex/g;')
cat $dir/tamil_roman.txt | \
perl -e ' ($dict) = @ARGV; open L, "$dict" or die "file $dict cannot open\n";
  while(<L>) { chomp;  m/(\S+)\s+(.*)/ or next; $vocab{$1} = $2;} close L;
  while (<STDIN>) {chomp; m/(\S+)\s+(.*)/ or next; ($tw, $rws) = ($1, $2);
    @A = split (/\s+/, $rws);
    for($i=0; $i< @A; $i++) {
      $rw = $A[$i];
      die "$rw is an oov\n" if not exists $vocab{$rw};
      push @B, $vocab{$rw};
    }
    $pron = join " ", @B;  @B = ();
    print "$tw\t$pron\n";
  }
' $dir/roman.lex > $dir/$fname


echo "$0: g2p pronunciation labelling ($dir/$fname) done"
