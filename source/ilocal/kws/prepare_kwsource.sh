#!/bin/bash

echo -e "\n## LOG: $0 $@\n"

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}

. path.sh

segments=

. parse_options.sh

if [ $# -ne 3 ]; then
  echo -e "\n\n Usage:$0 <data> <indusdb_dir> <kwsource_dir> \n\n"
  exit 1
fi

data=$1; shift
indusdb=$1; shift;
kwsource=$1; shift;
if [ -z $segments ]; then
  [ -f $data/segments ] || die "segments file is not there $data"
fi
[ -d $indusdb ] || die "indusbd source folder is not ready";

fpath=$(cd $indusdb; pwd)

mkdir -p $kwsource ; # rm $kwsource/* 2>/dev/null

if [[ ! -f $kwsource/ecf.xml || ! -f $kwsource/duration ]]; then
  ( cd  $kwsource; target=`ls $fpath/*.ecf.xml 2>/dev/null`; [ ! -z $target ] && ln -s $target ecf.xml)
  if [ ! -f $kwsource/ecf.xml ]; then
    ( cd  $kwsource; target=`ls $fpath/ecf.xml 2>/dev/null `; [ -f $target ] && ln -s $target ecf.xml)
  fi
  [ -f $kwsource/ecf.xml ] || die  "making ecf.xml file failed"
  
  head -1 $kwsource/ecf.xml | \
  grep -o -E "duration=\"[0-9]*[    \.]*[0-9]*\"" |\
  perl -e 'while($m=<>) {$m=~s/.*\"([0-9.]+)\".*/\1/; print $m/2;}' \
  > $kwsource/duration

fi


if [ ! -f $kwsource/rttm ]; then
  (cd $kwsource; target=`ls $fpath/*.rttm 2>/dev/null | grep mit `; [ ! -z $target ] && ln -s $target rttm )
  if [ ! -f $kwsource/rttm ]; then
    (cd $kwsource; target=`ls $fpath/rttm 2>/dev/null` ; [ ! -z $target ] && ln -s $target rttm )
  fi
  [ -f  $kwsource/rttm ] || echo "## WARNING: making rttm file failed"; 
fi

fpath=$(cd $data; pwd)
if [[ ! -f $kwsource/segments || ! -f $kwsource/utter_id ]]; then
  if [ -z $segments ]; then
    (cd $kwsource; ln -s $fpath/segments)   
  else
    if [ ! -f $segments ]; then
      echo -e "\n## ERROR: $0:segments file expected\n"; exit 1
    fi
    seg_full_path=$( dir=`dirname $segments`; cd $dir; pwd  ); 
    ( cd $kwsource; ln -s $seg_full_path/segments )
  fi
  if [[ ! -f $kwsource/utter_map || ! -f $kwsource/utter_id ]]; then
    isteps/kws/make_utter_id.pl $kwsource/segments >$kwsource/utter_id
    cat $kwsource/segments | awk '{print $1" "$2}' | \
    sort | uniq > $kwsource/utter_map;
  fi
fi

