#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}

# desgin: ilocal/kws/make_roman_kwlist.sh word-lexicon.txt morph-lexicon.txt morph.mdl  source-tamil-kwlist.xml  target-dir

. path.sh || die "path.sh expected";
echo -e "\nLOG: $0 $@\n";
echo -e "\nLOG: runing on `hostname` `date`\n"

phones=
g2p_model=
morph_model=
mlex=
noprons=
mlang=
xlang=
tamil_keywords=true

. parse_options.sh || die "parse_options.sh expected";

set -u
## set -e
set -o pipefail

if [ $# -ne 4 ]; then
  echo -e "\n\n dump roman kwlist.txt from tamil kwlist.xml file"
  echo -e "Example:\n"
  echo -e "$0 <tamil_kwlist.xml> <word_lang> <word_lexicon> <target_dir>\n\n"
  exit 0
fi

kwxml=$1;  shift
lang=$1; shift;
wlex=$1; shift
dir=$1; shift;

sdir=$dir; dir=$sdir/temp; mkdir -p $dir
if $tamil_keywords; then

[ -e $dir/tamil_keywords.txt ] || \
isteps/kws/make_kwlist_from_xml.pl $kwxml > $dir/tamil_keywords.txt

[ -e $dir/tamil_keywords.txt ] || die "tamil_keywords.txt expected"

if [ ! -f $dir/roman_keywords_01.txt ]; then
  paste <(cut -f 1 $dir/tamil_keywords.txt) \
    <( cut -f 2- $dir/tamil_keywords.txt| sed -e 's:[-_]: :g' | \
    perl -e '
      while(<STDIN>) {
        ($conv) = @ARGV;
        chomp; @A = split / /;
        @B=();
        for($i=0; $i< scalar @A ; $i++) {
          $w1 = $A[$i];
          open CONV, "$conv $w1|";
          $w = <CONV>; 
          close CONV;
          push @B, $w;
        }
        print join " ", @B, "\n";
      }' tamil2roman
    ) > $dir/roman_keywords_01.txt 
fi
else
  isteps/kws/make_kwlist_from_xml.pl $kwxml > $dir/roman_keywords_01.txt
fi

[ -e $dir/roman_keywords_01.txt ] || die "$dir/roman_keywords_01.txt expected";

(cat  $dir/roman_keywords_01.txt | grep "ERROR"   > $dir/tamil2roman_failure.txt )

if [ ! -f $sdir/wkeywords.txt ]; then
  echo -e "\nmaking word keywords.int `date`\n"
  invW=$dir/roman_invw_keywords_01.txt;
  oovW=$dir/roman_oovw_keywords_01.txt; 

  if [[ ! -f $invW || ! -f $oovW ]]; then
    echo -e "\n## for word case\n"
    iutils/kws/dump_keywords.pl $wlex $dir/roman_keywords_01.txt $invW $oovW ||\
    die "dumping word keyword error";
    echo -e "\ndone \n"
  fi

  cat $invW $oovW | sort > $sdir/wkeywords.txt
fi

# making mkeywords.fsts, optionally
if [ ! -z $morph_model ]; then
  echo -e "\nyou are intending to making mkeywords.fsts\n"
  for x in $morph_model $mlex  $noprons; do
    [ -f $x ] || die "file $x expected to make mkeywords.fsts";
  done
  [ -f $mlang/words.txt ] || die "morph lang words.txt expected "

  if [ ! -f $sdir/mkeywords.fsts ]; then
    echo -e "\n## making mkeywords.fsts started `date`\n"
    invM=$dir/roman_invm_keywords_01.txt;
    oovM=$dir/roman_oovm_keywords_01.txt; 
    if [[ ! -f $invM || ! -f $oovM ]]; then
       echo -e "\n## for morph case\n"
       if [ ! -f $sdir/morph/text ]; then
          ilocal/morph/text_segment.sh --from 2  $morph_model $dir/roman_keywords_01.txt $sdir/morph 
       fi
       [  -e $sdir/morph/text ] || die "text_segment.sh failed to generate $sdir/morph/text"
  
       cat $sdir/morph/text | \
       ilocal/morph/filter_noprons.pl $noprons > $sdir/morph/text_noprons_removed
 
       iutils/kws/dump_keywords.pl $mlex $sdir/morph/text_noprons_removed $invM $oovM || \
       die "dumping morph keyword error"
    fi
    # here we add all morph word to word keyword list
    cat $invM $oovM >> $sdir/wkeywords.txt;
    # take xkeywords.txt into consideration
    cat $invM $oovM >> $sdir/xkeywords.txt
    cat $invM $oovM | sort > $sdir/mkeywords.txt
    echo -e "\n## for morph  from symbol to int\n"
    cat $sdir/mkeywords.txt $sdir/wkeywords.txt  | \
    sym2int.pl --map-oov 0 -f 2- $mlang/words.txt > $sdir/mkeywords.int 2> $sdir/mkeywords.log
    cat $sdir/mkeywords.int | \
    awk '{if (/ 0 /||/ 0$/){}else{print ;}}' > $sdir/mkeywords_no_oov.int
    . ilocal/bn/path.sh
    transcripts-to-fsts ark:$sdir/mkeywords_no_oov.int ark,t:$sdir/mkeywords.fsts || \
    die "making word keyword fsts failed"

    echo -e "\n## making mkeywords.fsts ended `date`\n"
  fi
fi

# make wkeywords.fsts, now
if [ ! -f $sdir/wkeywords.fsts ]; then
  echo -e "\n## for $sdir/wkeywords.fsts\n"
 
  cat $sdir/wkeywords.txt | \
  sym2int.pl --map-oov 0 -f 2- $lang/words.txt > $sdir/wkeywords.int 2> $sdir/wkeywords.log

  cat $sdir/wkeywords.int | \
  awk '{if (/ 0 /||/ 0$/){}else{print ;}}' > $sdir/wkeywords_no_oov.int
  . ilocal/bn/path.sh
  transcripts-to-fsts ark:$sdir/wkeywords_no_oov.int ark,t:$sdir/wkeywords.fsts || \
  die "making word keyword fsts failed"
 
  echo -e "\n## making word keywords.fsts done `date`\n"
fi

if [[ ! -z $xlang && ! -z $morph_model ]]; then
  echo -e "\n## xlang enabled and xkeywords.txt will be generated `date`\n"
  if [ ! -f $xlang/local/word_morph.txt ]; then
    die "word_morph.txt expected from  $xlang, xlang/local ? `date`";
  fi  
  iutils/kws/dump_keywords.pl $xlang/local/word_morph.txt \
  $dir/roman_keywords_01.txt $dir/roman_invx_keywords_01.txt \
  $dir/roman_oovx_keywords_01.txt || \
  die "dumping keyword list error in xlang case";
    if [ ! -f $sdir/xkeywords.fsts ]; then
      if [[ $(wc -l < $dir/roman_oovx_keywords_01.txt ) -gt 0 ]]; then

        if [ ! -f $sdir/xmorph/text ]; then
          ilocal/morph/text_segment.sh --from 2  $morph_model \
          $dir/roman_oovx_keywords_01.txt $sdir/xmorph  || \
          die "tex_segment.sh failed in xlang case";
        fi
        # remove no-pron-word
        cat $sdir/xmorph/text | \
        ilocal/morph/filter_noprons.pl $noprons \
        > $sdir/xmorph/text_noprons_removed
        # merge inv and oov from xmorph
        cat $dir/roman_invx_keywords_01.txt $sdir/xmorph/text_noprons_removed \
        >$sdir/xkeywords_01.txt
      else 
        cat $dir/roman_invx_keywords_01.txt  >$sdir/xkeywords_01.txt 
      fi
      # add wkeywords.txt
      cat $sdir/wkeywords.txt >> $sdir/xkeywords_01.txt
      # add mkeywords.txt
      cat $sdir/mkeywords.txt >> $sdir/xkeywords_01.txt
      # remove duplicated items
      cat $sdir/xkeywords_01.txt | \
      ilocal/kws/xlex_remove_dup.pl |sort > $sdir/xkeywords.txt
      cat $sdir/xkeywords.txt | \
      sym2int.pl --map-oov 0 -f 2- $xlang/lang/words.txt \
      > $sdir/xkeywords.int 2> $sdir/xkeywords.log
      cat $sdir/xkeywords.int | \
      awk '{if (/ 0 /||/ 0$/){}else{print ;}}' > $sdir/xkeywords_no_oov.int
      transcripts-to-fsts ark:$sdir/xkeywords_no_oov.int ark,t:$sdir/xkeywords.fsts || \
      die "making super hybrid keyword fsts failed"
      echo -e "\n## making xkeywords.fsts succeeded\n"
   fi    
fi

if [ ! -z $phones ]; then
  [ -e $phones ] || die "phone list $phones is not there";
  [ ! -z $g2p_model ] || die "g2p model must be specified in phone case";
  . ilocal/bn/path.sh
  if [ ! -f $sdir/pkeywords.fsts ]; then
    echo -e "\n## making pkeywords.fsts started `date`\n"
    if [ ! -f $sdir/phone_mapping.fst ]; then
      echo -e "\n## making phone_mapping.txt file\n"
      cat $phones | \
      grep -v '<eps>'|\
      grep -v '#' | \
      awk '{s=$1; gsub(/_[SBIE]/,"",s); print $1, " ",s; }'| \
      awk '{printf "%s\t%s\n", $1, $2; }'| sort -u > $sdir/phone_mapping.txt
      cat $phones | \
      iutils/kws/make_phone_map.pl  $sdir/phone_mapping.txt $sdir/mapped_phones.txt 
      cat $sdir/mapped_phones.txt | \
      awk '{printf "%s\t%s\n", $1, $2; }' | sort -u > $sdir/.mphones.txt;
      mv $sdir/.mphones.txt $sdir/mapped_phones.txt
      echo -e "\n## making phone mapping transducer `date`\n"
      cat $sdir/phone_mapping.txt |\
      utils/make_lexicon_fst.pl  - |\
      fstcompile --isymbols=$sdir/mapped_phones.txt \
      --osymbols=$phones - |\
      fstinvert | fstarcsort --sort_type=olabel > $sdir/phone_mapping.fst
    fi
    cat $sdir/wkeywords.txt | \
    iutils/kws/show_oov.pl 2 $wlex | sort -u >  $sdir/oov_wlist.txt
    
    g2p.py --model=$g2p_model --apply=$sdir/oov_wlist.txt \
    --word-to-phoneme=$sdir/oov_wlist.g2p.lex  > $sdir/g2p.log 2>&1 

    [ -f $sdir/oov_wlist.g2p.lex ] || \
    die "g2p failed to create file $sdir/oov_wlist.g2p.lex "
 
    echo -e "\ncheck g2p log: $sdir/g2p.log\n"

    n=`wc -l < $sdir/oov_wlist.g2p.lex`;
    [ $n -gt 0 ] || die "no word labeled, in $sdir/oov_wlist.g2p.lex";
    echo -e "\n## checking $sdir/oov_wlist.g2p.lex file\n"
    cat $sdir/wkeywords.txt | \
    iutils/kws/make_phone_tkeywords.pl "cat $sdir/oov_wlist.g2p.lex $wlex|" \
    $sdir/mapped_phones.txt  > $sdir/pkeywords.int 2>$sdir/pkeywords.error.log
    cat $sdir/pkeywords.int | awk '{if(NF<=1){print "ERROR:" ; exit 1; }}'
    echo -e "\n## check $sdir/pkeywords.int and $sdir/pkeywords.error.log\n"
    transcripts-to-fsts ark:$sdir/pkeywords.int \
    ark,t:$sdir/pkeywords.fsts
    echo -e "\n## pkeywords.fsts done `date`\n"
  fi
fi

exit 0
