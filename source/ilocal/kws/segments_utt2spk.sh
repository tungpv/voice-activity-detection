#!/bin/bash

. path.sh


if [ $# -ne 3 ]; then
  echo -e "\n\nUsage: $0 segments utt2spk spk2utt\n\n";
  exit 1;
fi

seg=$1; shift
utt2spk=$1; shift
spk2utt=$1; shift

if [ ! -f $seg ] ; then
  echo -e "\nsegments $seg is not there\n";
  exit 1
fi

cat $seg | \
perl -ne 'chomp; @A =split (" ", $_); 
$s = $A[0]; @B = split('_', $s);
$spk = join '_', $B[0], $B[1];
print "$s\t$spk\n";
 ' > $utt2spk;
utils/utt2spk_to_spk2utt.pl  < $utt2spk | sort -u > $spk2utt

exit 0

