#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "cuda_path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl
step=0
substep=0
prepare_data=false
train_plppp=false
lang=data/llp/lang; lang_test=${lang}_test
samples_per_iter=4000000;  
max_mem=500000000;
state=2500;gauss=25000
nonspeech_words=$lang/nonspeech_words.txt

# end tunable variable
bn_am_cf=conf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";
phone_dir=data/llp/local/kws/phone;
kwlistdir=data/llp/local/kws/dev10h;
if $prepare_data; then
  echo -e "\n## prepare_data started `hostname` `date`\n"
  dir=$phone_dir; 
  mkdir -p $dir 2>/dev/null
  if [ $step -le  1 ]; then
    echo -e "\n## prepare phone mapping file `hostname` `date`\n"
    cat $lang/phones.txt | \
    iutils/kws/make_phone_map.pl  $dir/mapped_lex.txt $dir/mapped_phones.txt 
    echo -e "\n## done `date`\n"
  fi
  if [ $step -le 3 ]; then
    echo -e "\n## making transducer `hostname` `date`\n"
    cat $phone_dir/mapped_lex.txt |\
    utils/make_lexicon_fst.pl  - |\
    fstcompile --isymbols=$phone_dir/mapped_phones.txt \
    --osymbols=$lang/phones.txt - |\
    fstinvert | fstarcsort --sort_type=olabel > $phone_dir/mapped_lex.fst
    echo -e "\n## done `date`\n"
  fi
  words=$lang/words.txt; wordLex=data/llp/local/dict/lexicon.txt;
  mapped_phoneTxt=$phone_dir/mapped_phones.txt;
  if [ $step -le 5 ]; then
    echo -e "\n## phone mapping `hostname` `date`\n"
    cat $kwlistdir/keywords.int | \
    iutils/kws/make_phone_keywords.pl $words $wordLex $mapped_phoneTxt \
    > $kwlistdir/phone_keywords.int  || exit 1
    echo -e "\n## done `date`\n"
  fi
  sdir=$kwlistdir;
  if [ $step -le 7 ]; then
    echo -e "\n## making phone mapping fst `hostname` `date`\n"
    transcripts-to-fsts ark:$sdir/phone_keywords.int \
    ark,t:$sdir/phone_keywords.fsts
    # Map utterance
    echo -e "\n## done `date`\n"
  fi
  echo -e "\n## prepare_data done `hostname` `date` \n"
  exit 0
fi

sdir=exp/llp/tandem/pnorm_learn0.0006_0.00006_leayers3; latdir=$sdir/decode_dev10h
dir=$latdir/plat; platdir=$dir;
nj=`cat $latdir/num_jobs`
if [ $step -le 1 ]; then
  echo -e "\n## $0 word to phone lattice conversion `hostname` `date`\n"
  $cmd  JOB=1:$nj $dir/log/lat.JOB.log \
  lattice-align-phones --replace-output-symbols=true $sdir/final.mdl "ark:gzip -cdf $latdir/lat.JOB.gz|" "ark:|gzip -c >$dir/lats.JOB.gz" || exit 1
  echo -e "\n## $0 step01 done `date`\n"
fi

mplatdir=$platdir/mplat; dir=$mplatdir; mapFST=$phone_dir/mapped_lex.fst
if [ $step -le 3 ]; then
  echo -e "\n## $0 phone mapping `hostname` `date`\n"
  $cmd  JOB=1:$nj $dir/log/lat.JOB.log \
  lattice-compose "ark:gzip -cdf $platdir/lats.JOB.gz|"  $mapFST "ark:|gzip -c >$dir/lats.JOB.gz" || exit 1
  echo -e "\n## $0 step03 done `date`\n"
fi
min_lmwt=13; max_lmwt=15; 
mdldir=exp/llp/tandem/pnorm_learn0.0006_0.00006_leayers3
latdir=$mdldir/decode_dev10h/plat/mplat
cp $latdir/../../num_jobs $latdir 2>/dev/null
silence_opt="--silence-word 1";
model_flags="--model $mdldir/final.mdl"
latbeam=6.0
indexdir=$latdir/index$latbeam; sdir=data/llp/local/kws/dev10h;
skip_optimization=false;
max_states=150000;
max_silence_frames=50
word_ins_penalty=0;

if [ $step -le 5 ]; then
  echo -e "\n## $0 making index `hostname` `date`\n"
  for lmwt in `seq $min_lmwt $max_lmwt`; do
    dir=$indexdir/$lmwt;
    [ -d $dir ] || mkdir -p $dir;
    cp $sdir/utter_id $dir/
    acwt=`perl -e "print (1.0/$lmwt)"`;
    isteps/make_index_phone.sh $silence_opt --cmd "$cmd" --acwt $acwt $model_flags\
    --latbeam $latbeam \
    --skip-optimization $skip_optimization --max-states $max_states \
    --word-ins-penalty $word_ins_penalty --max-silence-frames $max_silence_frames\
    $sdir $lang $latdir $dir  || exit 1
  done
  echo -e "\n## indexing ended `hostname`  `date`\n"
fi
keywords=$sdir/phone_keywords.fsts
if [ $step -le 7 ]; then
  echo -e "\n## $0 search index `hostname ` `date`\n"
  for lmwt in `seq $min_lmwt $max_lmwt`; do
    indir=$indexdir/$lmwt;
    kwsdir=$indir/kws;
    isteps/search_index_phone.sh --cmd "$cmd"  --keywords $keywords --indices-dir $indir $sdir $kwsdir || exit 1
  done
  echo -e "\n## done `date`\n"
fi

ntrue_scale=1.0;
duptime=0.6;
kwsource=$sdir/..;
ecf_xml=$kwsource/babel107b-v0.7_conv-dev.ecf.xml
if [ $step -le 9 ]; then
  echo -e "\n## write out kws results `hostname` `date` \n"
  duration=`head -1 $ecf_xml |\
  grep -o -E "duration=\"[0-9]*[    \.]*[0-9]*\"" |\
  perl -e 'while($m=<>) {$m=~s/.*\"([0-9.]+)\".*/\1/; print $m/2;}'`
  echo -e "\nduation=$duration\n"
  
  $cmd LMWT=$min_lmwt:$max_lmwt $indexdir/LMWT/kws/write_normalized.log \
  set -e ';' set -o pipefail ';' \
  cat $indexdir/LMWT/kws/result.* \| \
  utils/write_kwslist.pl  --Ntrue-scale=$ntrue_scale --flen=0.01 --duration=$duration \
  --segments=$sdir/segments --normalize=true --duptime=$duptime --remove-dup=true\
  --map-utter=$sdir/utter_map --digits=3 \
  - $indexdir/LMWT/kws/kwslist.xml || exit 1
  echo -e "\n## done `date`\n"
fi

if [ $step -le  10 ]; then
  echo -e "\n## $0 ATWV evaluation `hostname` `date` \n"
  $cmd LMWT=$min_lmwt:$max_lmwt \
  $indexdir/LMWT/kws/log/scoring.LMWT.log \
  ilocal/kws/ntu_kws_score.sh  $kwsource \
  ${indexdir}/LMWT/kws || exit 1;
  echo -e "\n## $0 done `date`\n"
fi
ep=exp/kws/merge;
ecf=$kwsource/ecf.xml; rttm=$kwsource/rttm; kwlist=$kwsource/kwlist.xml
if [ $step -le  11 ]; then
  echo -e "\n## $0 merge btween word and phone systems started `hostname` `date`\n"
  mkws=exp/llp/tandem/pnorm_learn0.0006_0.00006_leayers3/decode_dev10h/index/13/kws/kwslist.xml;
  akws=exp/llp/tandem/pnorm_learn0.0006_0.00006_leayers3/decode_dev10h/plat/mplat/index6.0/13/kws/kwslist.xml
  for prune in 0.5 0.6 0.7 0.8 0.9; do
    dir=$ep/prune$prune; [ -d $dir ] || mkdir -p $dir 
    iutils/kws/merge_kwslist_xml_aux_yes02.pl $mkws $akws $duptime $prune 2> $dir/LOG.merge \
    >$dir/kwslist.xml;
    KWSEval.pl -e $ecf -r $rttm -t $kwlist -s $dir/kwslist.xml \
    -c -o -b -d -f $dir/
  done
  echo -e "\n## done\n"
fi
