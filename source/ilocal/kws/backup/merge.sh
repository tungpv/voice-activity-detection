#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
function remove_dups {
  [ $# -ge 1 ] || die "remove_dups: file expected";
  file=$1;
  [ -e $file ] || die "remove_dups: file $file does not exist";
  dir=`dirname $file`;
  temp=$dir/t001;
  rm $temp 2>/dev/null;
  cat $file | \
  awk '{if ($1 in vocab){}else {print ; vocab[$1]++;}}' > $temp;
  mv $temp $file;
}
#
. path.sh || die "cuda_path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
doit=false;
eprefix=exp/tune_am_bn_semi;
bn_prefix=exp/tune_bn_semi;
stage=0;
sub_stage=0;
disable_stage="19";
max_stage=1347;
nj=30;
dprefix=data/107/llp/ifish;
dprefix_semi=$dprefix/semi_tune;
ulatdir=exp/am_bn_from_dnn_ali/tri3sat/decode_utrain;
utrans=$ulatdir/scoring/20.tra;
inv_ascale=20;
prepare_data=false;
lang=data/107/llp/lang_dialect_final;
bn_cf=configs/bn/semi01.sh;
bn_am_cf=configs/am_bn/am_regular.sh;
nn_hidden_dim=1024;
# end user-defined variable
. env.sh || die "env.sh expected";
echo -e "\n$0 $@\n";
. parse_options.sh || die "parse_options.sh expected";

kwsdir=i2r/c3i2r01
output_prefix=$kwsdir/merge
array_auxes=("Ffv.xml sgmm.xml" "FFV.xml sgmm.xml" "FFV.xml Ffv.xml");
array_prune=("0.5 0.6 0.7 0.8 0.9" "0.5 0.6 0.7 0.8 0.9" "0.5 0.6 0.7 0.8 0.9");
idx=0;

ecf=../isword/IndusDB.latest/babel107b-v0.7_conv-dev.ecf.xml;
rttm=../isword/IndusDB.latest/babel107b-v0.7_conv-dev/babel107b-v0.7_conv-dev.mitllfa3.rttm;
kwlist=../isword/IndusDB.latest/babel107b-v0.7_conv-dev.kwlist2.xml;
function mybase {
  str=$1;
  echo $str | awk '{gsub(/\.[^\.]+$/,"",$0); print;}';
}
gaptime=0.6;
for kws in  FFV.xml Ffv.xml sgmm.xml; do
  echo "kws_xml=$kws";
  mbase=`mybase $kws`;
  mkws=$kwsdir/$kws;
  array_aux=${array_auxes[$idx]};
  echo "array_aux=$array_aux";
  axml1=`echo $array_aux|awk '{print $1;}'`
  abase1=`mybase $axml1`;
  akws1=$kwsdir/$axml1;
  axml2=`echo $array_aux|awk '{print $2;}'`
  abase2=`mybase $axml2`;
  akws2=$kwsdir/$axml2;
  my_total=0;
  for x in $mkws $akws1 $akws2; do
    [ -e $x ] || die "xml file $x does not exist";
  done
  prune=${array_prune[$idx]};
  my_prefix=$output_prefix/${mbase}_${abase1}_${abase2};
  echo "my_prefix=$my_prefix";
  idx=$[idx+1];
  for p1 in $prune; do
    for p2 in $prune; do
      dir=$my_prefix/${p1}_${p2};
      [ -d $dir ] || mkdir -p $dir;
      echo "dir=$dir";
      echo -e "\n!!! merge kwslist in folder $dir ...\n";
      i2r/ntu/merge_kwslist_xml_aux_yes02.pl $mkws $akws1 $gaptime $p1 2> $dir/LOG.merge \
      | i2r/ntu/merge_kwslist_xml_aux_yes02.pl - $akws2 $gaptime $p2 2>>$dir/LOG.merge \
      > $dir/kwslist.xml;
      echo -e "\n!!!start to evaluate\n";
      KWSEval -e $ecf -r $rttm -t $kwlist -s $dir/kwslist.xml \
      -c -o -b -d -f $dir/
      my_total=$[my_total+1];
      echo -e "Done in $dir ($my_total)!";
    done
  done
done
