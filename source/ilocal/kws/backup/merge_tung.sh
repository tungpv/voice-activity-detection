#!/bin/bash

ecf_dev=../isword/IndusDB.latest/babel107b-v0.7_conv-dev.ecf.xml;
ecf_eval=../isword/IndusDB.latest/babel107b-v0.7_conv-eval.ecf.xml;
rttm1=../isword/IndusDB.latest/babel107b-v0.7_conv-dev/babel107b-v0.7_conv-dev.mitllfa3.rttm;
rttm2=../isword/IndusDB.latest/babel107b-v0.7_conv-dev/babel107b-v0.7_conv-dev.rttm;
kwlist2=../isword/IndusDB.latest/babel107b-v0.7_conv-dev.kwlist2.xml;
kwlist3=../isword/IndusDB.latest/babel107b-v0.7_conv-dev/babel107b-v0.7_conv-dev.annot.kwlist3.xml;
kwsdir=i2r/c3i2r01;
array_kwlist=($kwlist2 $kwlist2 $kwlist2);
array_ecf=($ecf_dev $ecf_dev $ecf_dev);
array_rttm=($rttm1 $rttm1 $rttm1);

ecf=../isword/IndusDB.latest/babel107b-v0.7_conv-evalpart1.ecf.xml;
ecf=../isword/IndusDB.latest/babel107b-v0.7_conv-evalpart1/babel107b-v0.7_conv-evalpart1.scoring.ecf.xml;
kwlist=../isword/IndusDB.latest/babel107b-v0.7_conv-evalpart1/babel107b-v0.7_conv-evalpart1.annot.kwlist3.xml;
rttm=../isword/IndusDB.latest/babel107b-v0.7_conv-evalpart1/babel107b-v0.7_conv-evalpart1.mitllfa3.rttm;

. env.sh;

function MyBase {
  mypath=$1;shift;
  x=`basename $mypath`;
  echo $x| awk '{gsub (/\.[^\.]*$/,"",$0); printf "%s",$0;}';
}

kwsdir=i2r/tung/scoring_files;
idx=0; 
for kws in `ls i2r/tung/scoring_files/*.xml` ; do
  fname=`MyBase $kws`;
  echo "process file $fname";
  outputdir=$kwsdir/eval_$fname;
  [ -d $outputdir ] || mkdir -p $outputdir;
  echo -e "\n!!! check kws xml file ...\n";
  cat $kws | grep KW107 | awk '{print $3;}'| \
  awk '{ gsub(/kwid=/,"",$0);gsub(/\"/,"",$0); print;}' | \
  sort -u | wc -l;
  echo -e "\n!!! process file $f ...\n";
  
  KWSEval -e $ecf -r $rttm -t $kwlist -s $kws -c -o -b -d -f $outputdir/
  # rm $kwsdir/t001;
done 
