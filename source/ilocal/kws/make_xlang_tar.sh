#!/bin/bash

echo  -e "\n## LOGIN: $0 $@\n"

echo -e "\n## DESIGN local/kws/make_xlang.sh [options]  wlex mlex noprons wtext mtext morph.mdl dir\n"
echo -e  "\n## EXAMPLE1: ./ilocal/kws/make_xlang_tar.sh --kwlist \"test/keywords/ntu3/xkeywords.txt test/keywords/bbn2/xkeywords.txt test/keywords/ibm1/xkeywords.txt test/keywords/ibm2/xkeywords.txt\"   data/local/lexicon.txt data/g2pflp0c/local/lexicon.txt data/g2p0fa/local/noprons.txt data/train/text data/g2pflp0c/train/text data/g2p0fa/local/morph/morph.mdl test/xlang.k2\n"
echo -e "\n## EXAMPLE2: ilocal/kws/make_xlang_tar.sh --keep-filler true --kwlist \"test/keywords/ntu3/xkeywords.txt test/keywords/bbn2/xkeywords.txt test/keywords/ibm1/xkeywords.txt test/keywords/ibm2/xkeywords.txt\"   data/local/lexicon.txt data/g2pflp0c/local/lexicon.txt data/g2p0fa/local/noprons.txt data/train/text data/g2pflp0c/train/text data/g2p0fa/local/morph/morph.mdl test/xlang.kf2 \n"
function die {
  echo -e "\nERROR:$1\n"; exit -1;
}



. path.sh
. cmd.sh 

# begin options
oovSymbol="<unk>"
word_count=2
lm_order=2
kwlist=;
kw_scale=0.2;
keep_filler=false
# end options

. parse_options.sh || die "parse_options.sh expected";

set -u
## set -e
set -o pipefail

if [ $# -ne 7 ]; then
  echo -e "\n\nUsage:\n"
  echo -e "$0 [options] wlex mlex noprons  wtext mtext  morph.mdl dir"
  echo -e "\noptions:\n"
  echo -e "--kwlist kwlists     # make language model interpolation from keyword lists, default  empty"
  echo -e "--kw-scale kw_sacle  # scaling factor for keyword list language model interpolation [default 0.2]\n\n";
  exit 1
fi

wlex=$1; shift
mlex=$1; shift
noprons=$1; shift
wtext=$1; shift
mtext=$1; shift
morph_mdl=$1; shift
dir=$1; shift
for x in $wlex $mlex $noprons $wtext $mtext $morph_mdl; do
  [ -e $x ] || die "file $x is not there"
done

local=$dir/local; 
lang=$dir/lang;

mkdir -p $dir/{local,lang}
cat $noprons > $local/noprons.txt

if [ ! -f $local/lexicon.txt ]; then
  ilocal/kws/xlex.pl $wlex $mlex  "|sort>$local/lexicon.txt"
  if [[ ! -f $local/lexicon.txt || `wc -l < $local/lexicon.txt` -eq 0 ]]; then
    die "lexicon.txt is not made in $local";
  fi
fi

if [ ! -f $local/nonsilence_phones.txt ]; then
  cat $local/lexicon.txt | \
  grep -v '<' | \
  awk '{for(i=2;i<=NF;i++){print $i;}}' | sort -u > $local/nonsilence_phones.txt
fi

if [ ! -f $local/silence_phones.txt ]; then
  grep '<' $local/lexicon.txt| \
  grep -v 'hes' | cut -f 2- > $local/silence_phones.txt
fi

echo "SIL" > $local/optional_silence.txt

cat $local/silence_phones.txt | awk '{printf "%s ",$1;}END{printf "\n";}' > $local/extra_questions.txt
cat $local/nonsilence_phones.txt | awk '{printf "%s ",$1;}END{printf "\n";}' >> $local/extra_questions.txt
 
if [ ! -f $lang/L.fst ]; then
  utils/prepare_lang.sh \
  --share-silence-phones true \
  $local $oovSymbol $local/tmp.lang $lang
fi

dir=$local/tmp.lm;
if [ ! -f $local/tmp.lm/text ]; then
  dir=$local/tmp.lm; mkdir -p $dir
  cat $wtext | \
  awk '{for(i=2; i<=NF; i++){print $i;}}'| \
  perl -e 'while(<>){chomp; $vocab{$_}++; } foreach $w (keys %vocab){print "$w $vocab{$w}\n";} '\
  | sort -k2n > $dir/word_count.txt
  cat $dir/word_count.txt | awk '{if($2 <=2){print $1;} }' \
  | grep -v '<' > $dir/oov_wlist.txt
  # 
  cat $wtext | awk '{for (i=2; i <= NF; i++){ print $i;}}' |\
  ilocal/kws/xlex_dumpoov.pl $wlex >> $dir/oov_wlist.txt
  
  morfessor-segment -l $morph_mdl -o $dir/oov_morph.txt $dir/oov_wlist.txt
  if [[ `wc -l < $dir/oov_wlist.txt` -ne `wc -l < $dir/oov_morph.txt` ]]; then
    die "word and morph numbers are mismatched, check $dir/oov_wlist.txt and $dir/oov_morph.txt";
  fi
  paste <(cat $dir/oov_wlist.txt) <(cat $dir/oov_morph.txt| sed -e 's:_: :g') | sort -u > $dir/word_morph.txt
  [ -e $dir/word_morph.txt ] || die "file $dir/word_morph.txt is not there";
  cat $dir/word_morph.txt | \
       ilocal/morph/filter_noprons.pl $local/noprons.txt \
       > $dir/oov_word_morph.norm.txt
  cat $local/lexicon.txt | \
  ilocal/kws/xlex_merge.pl  $dir/oov_word_morph.norm.txt > $local/word_morph.txt || exit 1
  cat $wtext | \
  ilocal/kws/xlex_trans.pl $local/word_morph.txt > $dir/text
  grep '<' $local/lexicon.txt| awk '{print $1;}'  | \
  sort -u | awk '{ printf "'s#%s##g'\n", $1; }' > $local/filler_words.sed.txt
  echo -e "\n## filler_words:\n"
  cat $local/filler_words.sed.txt
  echo -e "\n## check folder $dir/text\n"
fi

if [ ! -f $dir/lm.gz ];then
  sort $lang/words.txt | \
  awk '{print $1}' | grep -v '\#0' | grep -v '<eps>' > $dir/vocab

  if $keep_filler; then
    cat $dir/text | cut -f2- -d' ' | \
    sed -f $local/filler_words.sed.txt > $dir/xtrain.txt
    cat $wtext | \
    sed -f $local/filler_words.sed.txt > $dir/wtrain.txt
    cat $mtext | \
    sed -f $local/filler_words.sed.txt > $dir/mtrain.txt
  else
    cat $dir/text | cut -f2- -d' ' > $dir/xtrain.txt
    cat $wtext > $dir/wtrain.txt
    cat $mtext > $dir/mtrain.txt
  fi

  i=1;
  for x in wtrain.txt xtrain.txt mtrain.txt; do
    train=$dir/$x;
    if [[ $lm_order -eq 2 ]]; then   
      ngram-count -order $lm_order -lm $dir/${lm_order}gram.$i.gz -gt1min 0 -gt2min 1 -text $train\
      -vocab $dir/vocab  -sort
    fi
    if [[ $lm_order -eq 3 ]]; then
      ngram-count -lm $dir/${lm_order}gram.$i.gz -gt1min 0 -gt2min 1 -gt2min 2 -text $train\
      -vocab $dir/vocab  -sort
    fi
    i=$[i+1];
  done
  ngram -order $lm_order -lm $dir/${lm_order}gram.1.gz -mix-lm $dir/${lm_order}gram.2.gz -lambda 0.3 \
  -mix-lm2 $dir/${lm_order}gram.3.gz -mix-lambda2 0.3  -write-lm $dir/lm.gz
fi

if [ ! -z "$kwlist" ]; then
  echo   kwlist="$kwlist";
  cat $kwlist | \
  awk '{$1=""; gsub (/^ /, "",$0); gsub(/ $/, "",$0); gsub (/ +/," ",$0);  print;}' |\
  sort > $dir/ktrain.txt;
  ngram-count -order 2 -lm $dir/2gram.kwlist.gz -gt1min 0 -gt2min 1 -text $dir/ktrain.txt \
  -vocab $dir/vocab -sort
  ngram -order $lm_order -lm $dir/lm.gz -mix-lm $dir/2gram.kwlist.gz -lambda $kw_scale -write-lm $dir/lm.fake.gz
fi

if [ ! -f $lang/G.fst ]; then
  if [ -z "$kwlist" ]; then
    if $keep_filler; then
      local/arpa2G.sh $dir/lm.gz $lang $lang
    else
      ilocal/kws/arpa2G.sh $dir/lm.gz $lang $lang
    fi
  else
    if $keep_filler; then
      local/arpa2G.sh $dir/lm.fake.gz $lang $lang
    else
      ilocal/kws/arpa2G.sh $dir/lm.fake.gz $lang $lang
    fi
  fi
fi
