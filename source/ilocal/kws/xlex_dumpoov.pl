#!/usr/bin/env perl

use strict;
use warnings;

my ($lex) = @ARGV;

my %vocab =();
open L, "<$lex" or die "file $lex cannot open\n";
while (<L>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $vocab{$1} ++;
}
close L;

print STDERR "$0 stdin expected\n";
my %oov = ();
while(<STDIN>) {
  chomp;
  if(not exists $vocab{$_}) {
    $oov{$_} ++;
  }
}
print STDERR "$0 stdin ended\n";

foreach my $words (keys %oov) {
  print "$words\n";
}
