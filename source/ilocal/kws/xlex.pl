#!/usr/bin/env perl
use warnings;
use strict;

scalar @ARGV ==3 or 
die "\n\nUsage: $0 wlex mlex xlex\n\n";

my ($wlex, $mlex, $xlex) = @ARGV;


sub LoadVocab {
  my ($vocab, $lex) = @_;
  open L, "<$lex" or die "file $lex cannot open";
  while (<L>) {
    chomp;
    my @A = split (' ', $_);
    next if (scalar @A < 2);
    my $word =shift @A;
    my $pron = join ' ', @A;
    if (exists $$vocab{$word}) {
      my $pv = $$vocab{$word};
      $$pv{$pron} = 0;
    } else {
      my %v = ();
      $$vocab{$word} = \%v;
      my $pv = $$vocab{$word};
      $$pv{$pron} = 0;
    }
  }
  close L;
}

sub MergeVocab {
  my ($vocab, $lex) = @_;
  open L, "<$lex" or die "file $lex cannot open";
  while (<L>) {
    chomp;
    my @A = split (' ', $_);
    next if (scalar @A < 2);
    my $word =shift @A;
    my $pron = join ' ', @A;
    next if(exists $$vocab{$word});
    my %v = ();
    $$vocab{$word} = \%v;
    my $pv = $$vocab{$word} ;
    $$pv{$pron} = 0;
  }
  close L;
}

sub DumpVocab {
  my ($vocab, $lex) = @_;
  open L, "$lex" or die "file $lex cannot open";
  foreach my $w (keys %$vocab) {
    my $pv = $$vocab{$w};
    for my $pron (keys %$pv) {
      print L "$w\t$pron\n";
    }
  }
  close L;
}

my %vocab = ();
LoadVocab(\%vocab, $wlex);
MergeVocab(\%vocab, $mlex);
DumpVocab(\%vocab, $xlex);
