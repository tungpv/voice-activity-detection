#!/bin/bash

echo -e "\n## LOG: $0 $@\n"

if [ $# -ne 2 ]; then
  echo -e "\n\nUsage:\n $0 <decoding_log_dir> <dir>\n\n"
  exit 1
fi

decode_dir=$1;
dir=$1; 

if [ ! -d $decode_dir ]; then
  echo "ERROR: decode dir $decode_dir does not exist";
  exit 1
fi
mkdir -p $dir

for x in `ls $decode_dir/decode.*`; do 
  if  ! grep -q 'status 0' $x; then 
    echo $x; 
  fi; 
done
