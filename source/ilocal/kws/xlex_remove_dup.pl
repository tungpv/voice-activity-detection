#!/usr/bin/env perl
use strict;
use warnings;

print STDERR "$0 stdin expected\n";
my %vocab =();
while (<STDIN>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  my @A = split (' ',$_);
  my $instance = join(' ', @A);
  $vocab{$instance} ++;
}
foreach my $inst (keys %vocab) {
  my @A = split (' ', $inst);
  my $kwid = shift @A;
  my $pron = join (' ', @A);
  print "$kwid\t$pron\n";
}
print STDERR  "$0 stdin ended\n";
