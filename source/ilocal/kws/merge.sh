#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}


. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n$0 $@\n";
echo -e "\n$0 run on `hostname` @ `date` \n";

# begin options
prune=0.9
duptime=0.6
# end options

. parse_options.sh || die "parse_options.sh expected";
# design: $0 kwsource dir  kwslist1 kwslist2 ...
if [ $# -lt 4 ]; then
  echo -e "\n\nUsage:"
  echo "$0 [options] kwsource dir kwslist1 kwslist2 ..."
  echo -e "\n\n"
  exit 1
fi
kwsource=$1; shift
dir=$1; shift
kws=$1; shift

for x in $kws; do
  [ -f $x ] || die "file $x is not there";
done


array_kws=($@)
array_prune=(`echo $prune|sed 's#:# #g'`)
prune_size=${#array_prune[@]};
prune=`echo $prune| sed 's#:#_#g'`
size=${#array_kws[@]}
[ $prune_size -eq $size ] || die "prune size is inequal to component size "
dir=$dir/$prune; mkdir -p $dir;

j=2;
for i in `seq 0 $[size-1]`; do
  kws1=${array_kws[$i]}; prune=${array_prune[$i]}
  echo -e "\n## merge $kws1 `hostname`  `date`\n"
  iutils/kws/merge_kwslist_xml_aux_yes02.pl $kws $kws1 $duptime $prune 2> $dir/LOG.$i.merge \
  >$dir/kwslist.$j.xml 
  kws=$dir/kwslist.$j.xml
  j=$[$j+1]
done


KWSEval -e $kwsource/ecf.xml -r $kwsource/rttm -t $kwsource/kwlist.xml \
-s $kws  -c -o -b -d -f $dir/
