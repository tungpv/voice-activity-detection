#!/bin/bash

function die {
  echo -e "\nERROR: $1 ...\n"; exit 1;
}

. path.sh || die "path.sh expected ..." ## Source the tools/utils (import the queue.pl)
. cmd.sh || die  "cmd.sh expected ..."

# Begin configuration section
make_index=false
prep_data=false
make_searchfst=false
search_index=false
norm_search=false
kws_score=false

min_lmwt=10
max_lmwt=10
duptime=0.6
skip_optimization=false
max_states=150000
word_ins_penalty=0
silence_word=
silence_opt=
ntrue_scale=1.0
acwt=1.0
# End configuration section

. parse_options.sh || die "parse_options.sh expected ..."
if $prep_data; then
  echo -e "\ndata preparation ...\n"
  silword=../swordfish/templates/silwords.asc
  [ -e $silword ] || die "silword file $silword does not exist ..."
  words=data/lang/words.txt
  [ -e $words ] || die "words.txt file $words does not exist ..."
  cat $silword| \
  awk '{print $1;}' | \
  isteps/kws/print_silword.pl $words > data/lang/phones/silwords.int
 
  # echo $silence_words
fi
segments=data/dev/segments
[ -e $segments ] || die "segments file $segments does not exist ..."
decodedir=exp/nnet6b/decode_dev
kwsdatadir=$decodedir/index
model=exp/nnet6b/final.mdl
[ -e $model ] ||die "$model does not exist ..."
model_flags="--model $model"
lang=data/lang
datadir=data/dev

if $make_index ; then
  echo -e "\nmaking index is on progress ...\n"
 
  silence_words=`cat data/lang/phones/silwords.int`
  [ ! -z $silence_words ] || die "silence_words not found ..."
  silence_opt="--silence-word 1"
  [ -d $kwsdatadir ] || mkdir -p $kwsdatadir
  # make utter id
  if [ ! -e $kwsdatadir/utter_id  ]; then
    isteps/kws/make_utter_id.pl $segments >$kwsdatadir/utter_id
  fi

  for lmwt in `seq $min_lmwt $max_lmwt`; do
    idir=$kwsdatadir/$lmwt
    [ -d $idir ] || mkdir -p $idir
    acwt=`echo "1/$lmwt"|bc -l |sed "s/^./0./g"`
    logdir=data/local/kws/log/sbatch
    [ -d $logdir ]|| mkdir -p $logdir
    logfile=$logdir/make_index_${lmwt}.log
    sbatch -p speech -o $logfile --mem-per-cpu=10000MB \
    steps/make_index.sh $silence_opt --cmd "$decode_cmd" \
     --acwt $acwt $model_flags\
     --skip-optimization $skip_optimization --max-states $max_states \
     --word-ins-penalty $word_ins_penalty \
     $kwsdatadir $lang $decodedir $idir  || exit 1

  done    
fi
# make search fst using keyword list
kwssrcdir=IndusDB.latest/babel107b-v0.7_conv-dev
sfstdir=data/local/kws/kwlist3
if $make_searchfst; then
  echo -e "\nmake search fst from keyword list\n"
  keywords=$kwssrcdir/babel107b-v0.7_conv-dev.annot.kwlist3.xml
  [ -e $keywords ] || die "xml file $keywords does not exist ...";
  dir=$sfstdir
  [ -d $dir ] || mkdir -p $dir
  [ -e $dir/keywords.txt ] || \
  isteps/kws/make_kwlist_from_xml.pl $keywords >$dir/keywords.txt
  cat $dir/keywords.txt | \
  awk '{$0 = tolower($0);$1 = toupper($1) ; print; }' > $dir/keywords_lower.txt 
  words=data/lang/words.txt
  [ -e $words ] || die "words.txt file $words is not seen ...";
  logdir=$dir/log
  [ -d $logdir ] || mkdir -p $logdir
  logfile=$logdir/oov_word.log
  rm -f $logfile 2>/dev/null
  cat $dir/keywords_lower.txt | \
  sym2int.pl --map-oov 0 -f 2- $words > $dir/keywords_all.int 2> $logfile
  cat $dir/keywords_all.int | \
  awk '{if (/ 0 /||/ 0$/){}else{print ;}}' > $dir/keywords.int
  cat $dir/keywords_all.int | \
  awk '{if (/ 0 /|| / 0$/){print $1;}}' | \
  ilocal/kws/subset_kwslist.pl $keywords > $dir/kwlist_oov.xml
  transcripts-to-fsts ark:$dir/keywords.int ark,t:$dir/keywords.fsts
  # Map utterance
  cat $datadir/segments | awk '{print $1" "$2}' | sort | uniq > $kwsdatadir/utter_map;
  echo -e "\nmaking search fst done !\n"
fi
# search index
if $search_index; then
  sfst=$sfstdir/keywords.fsts
  [ -e $sfst ] || die "search fst is not ready ...";
  abs_sfstdir=`(cd $sfstdir; pwd)`
  echo -e  "\nabs_sfstdir=$abs_sfstdir\n"
  [ -d $kwsdatadir ] || die "kwsdatadir $kwsdatadir is not ready ..."
  rm -f $kwsdatadir/keywords.fsts 2>/dev/null
  ln -s $abs_sfstdir/keywords.fsts $kwsdatadir/keywords.fsts

  for lmwt in `seq $min_lmwt $max_lmwt`; do
    idir=$kwsdatadir/$lmwt
    [ -e $idir/index.1.gz ] || die "index should be created before searching it ...";
    kwsoutput=$idir/kws
    [ -d $kwsoutput ] || mkdir -p $kwsoutput
    logdir=data/local/kws/log/sbatch
    [ -d $logdir ] || mkdir -p $logdir
    logfile=$logdir/search_index.log;   rm -f $logfile 2>/dev/null

    sbatch -p speech -o $logfile --mem-per-cpu=10000MB \
    kws/steps/search_index.sh --cmd "$decode_cmd" --indices-dir $idir \
    $kwsdatadir $kwsoutput  || \
    die "search index failed ..."
  done
fi 
# normalize results
ecf_xml=$kwssrcdir/babel107b-v0.7_conv-dev.scoring.ecf.xml

if $norm_search;  then

  echo -e "\nWriting normalized results\n"
  # get the full duration of the test set
  [ -e $ecf_xml ] || die "ecf_xml file $ecf_xml does not exist ..."
  duration=`head -1 $ecf_xml |\
  grep -o -E "duration=\"[0-9]*[    \.]*[0-9]*\"" |\
  perl -e 'while($m=<>) {$m=~s/.*\"([0-9.]+)\".*/\1/; print $m/2;}'`
  echo -e "\nduation=$duration\n"
  $decode_cmd LMWT=$min_lmwt:$max_lmwt \
  $kwsdatadir/LMWT/kws/log/write_normalized.LMWT.log \
  set -e ';' set -o pipefail ';'\
  cat $kwsdatadir/LMWT/kws/result.* \| \
  iutils/write_kwslist.pl --Ntrue-scale=$ntrue_scale --flen=0.01 --duration=$duration \
        --segments=$datadir/segments --normalize=true \
        --map-utter=$kwsdatadir/utter_map --digits=3 \
        - - \| \
   ilocal/filter_kwslist.pl $duptime '>'\
   ${kwsdatadir}/LMWT/kws/kwslist.xml || exit 1

fi
# do evaluation
if $kws_score; then
  echo -e "\nScoring KWS results\n"
  . env.sh || die "env.sh expected ..."

  abs_kwsrcdir=`(cd $kwssrcdir;cd ..; pwd)`
  ( cd $kwsdatadir; rm -f ecf.xml kwlist.xml rttm  ) 2>/dev/null
  [ -e $kwsdatadir/ecf.xml ] || \
  ln -s $abs_kwsrcdir/babel107b-v0.7_conv-dev.ecf.xml $kwsdatadir/ecf.xml
  [ -e $kwsdatadir/rttm ] || \
  ln -s $abs_kwsrcdir/babel107b-v0.7_conv-dev.kwlist3.xml $kwsdatadir/kwlist.xml
  [ -e $kwsdatadir/rttm  ] || \
  ln -s $abs_kwsrcdir/babel107b-v0.7_conv-dev/babel107b-v0.7_conv-dev.rttm $kwsdatadir/rttm

  $decode_cmd LMWT=$min_lmwt:$max_lmwt $kwsdatadir/LMWT/kws/log/scoring.LMWT.log \
  ilocal/kws/kws_score.sh  $kwsdatadir ${kwsdatadir}/LMWT/kws || exit 1;
fi 
