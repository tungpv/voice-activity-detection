#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n$0 $@\n";
echo -e "\n$0 run on `hostname` @ `date` \n";
# begin user-defined variable
doit=false;
nj=30;
cmd=run.pl;
step=1;
min_lmwt=8;
max_lmwt=15;
duptime=0.6;
skip_optimization=false;
max_states=150000;
max_silence_frames=50
word_ins_penalty=0;
ntrue_scale=1.0;
frame_dur=0.01;
acwt=1.0;
#
over_data=
over_mdldir=
over_latdir=
lang=data/llp/lang; lang_test=${lang}_test
#
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

set -u
set -e
set -o pipefail


testdata=data/llp/feature/plp_pitch/dev10h;
segments=$testdata/segments;
# put off-line source here
kwsource=data/llp/local/kws; sdir=$kwsource/dev10h;
[ -d $sdir ] || mkdir -p $sdir

if [ $step -le -1 ];then
  echo -e "\n## $0 prepare data \n"  
  for x in `find  $kwsource  -type f | egrep 'ecf|rttm|xml' |grep -v oov |awk '{printf " %s", $0;}'`; do
   dir=`dirname $x`;  base=`basename $x`;
   abdir=$(cd $dir; pwd);
   if echo $x| grep ecf ; then
     unlink $abdir/ecf.xml
     ln -s $abdir/$base $abdir/ecf.xml
   else 
     if echo $x | grep rttm; then
       unlink $abdir/rttm
       ln -s $abdir/$base $abdir/rttm;
     else
       unlink $abdir/kwlist.xml
       ln -s $abdir/$base $abdir/kwlist.xml
     fi
   fi  
  done
  exit 0;
fi

if [ $step -le 1 ]; then
  echo -e "\n## $0 making utt id `hostname` `date` \n"
  if [ ! -e $segments ]; then
    cat $testdata/wav.scp | \
     perl -e 'while (<STDIN>){ chomp; 
       @a = split /\s+/; $num = scalar @a; 
       $fname = $a[$num-1]; $fname =~ s/\.sph\|//g; 
       $fname =~ s/.*\///g; $time = $a[3]; 
       $time =~ s/:/ /; $segname = $a[0];
       print "$segname $fname $time\n" ;}' > $segments 
  fi
  [ -e $sdir/segments ] || cp $segments $sdir/segments;
  [ -e $sdir/utter_di ] ||  isteps/kws/make_utter_id.pl $segments >$sdir/utter_id
  echo -e "\n## done `date`\n"
fi
min_lmwt=13; max_lmwt=15; 
latdir=exp/llp/tandem/pnorm_learn0.0006_0.00006_leayers3/decode_dev10h
indexdir=$latdir/index;
silence_opt="--silence-word 1";
model_flags="--model $latdir/../final.mdl"
if [ $step -le 3 ]; then
  echo -e "\n## $0 making index `hostname` `date`\n"
  for lmwt in `seq $min_lmwt $max_lmwt`; do
    dir=$indexdir/$lmwt;
    [ -d $dir ] || mkdir -p $dir;
    cp $sdir/utter_id $dir/
    acwt=`perl -e "print (1.0/$lmwt)"`;
    logdir=$dir/log; 
    [ -e $dir/.done ] && continue;
    steps/make_index.sh $silence_opt --cmd "$cmd" --acwt $acwt $model_flags\
    --skip-optimization $skip_optimization --max-states $max_states \
    --word-ins-penalty $word_ins_penalty --max-silence-frames $max_silence_frames\
    $sdir $lang $latdir $dir  || exit 1
    touch $dir/.done;
  done
  echo -e "\n## $0 done `hostname` `date`\n"
fi
keywords_xml=$kwsource/babel107b-v0.7_conv-eval.kwlist3.xml
if [ $step -le 4 ]; then
  echo -e "\n## $0 prepare search fst `hostname` `date`\n"
  if [ ! -e $sdir/keywords.fsts ]; then
    isteps/kws/make_kwlist_from_xml.pl $keywords_xml \
    > $sdir/keywords.txt
    # the following in perl
    # is dangerous for using "use bytes" in perl !!!
    cat $sdir/keywords.txt | \
    perl -ne 'use bytes; $line = lc $_;  @a = split /\s+/, $line;  $a[0] = uc $a[0];
    $a[1] =~ s/-/ /g; 
    print join " ", @a, "\n"; ' > $sdir/keywords_lower.txt
    logfile=$sdir/oow_word.log 
    cat $sdir/keywords_lower.txt | \
    sym2int.pl --map-oov 0 -f 2- $lang/words.txt \
    > $sdir/keywords_all.int 2> $logfile
    cat $sdir/keywords_all.int | \
    awk '{if (/ 0 /||/ 0$/){}else{print ;}}' > $sdir/keywords.int
    cat $sdir/keywords_all.int | \
    awk '{if (/ 0 /|| / 0$/){print $1;}}' | \
    ilocal/kws/subset_kwslist.pl $keywords_xml > $sdir/kwlist_oov.xml
    # 
    transcripts-to-fsts ark:$sdir/keywords.int \
    ark,t:$sdir/keywords.fsts
    # Map utterance
    cat $segments | awk '{print $1" "$2}' | \
    sort | uniq > $sdir/utter_map;

  fi
  echo -e "\n## $0 done `date`\n"
fi

if [ $step -le 5 ]; then
  echo -e "\n## $0 search index `hostname``date`\n"
  [ -e $sdir/keywords.fsts ] || die "\n## ERROR $0 keywords.fsts is not ready\n";
  for lmwt in `seq $min_lmwt $max_lmwt`; do
    indir=$indexdir/$lmwt;
    kwsdir=$indir/kws;
    steps/search_index.sh --cmd "$cmd" --indices-dir $indir $sdir $kwsdir || exit 1

  done
  echo -e "\n## $0 done `date`\n"
fi
ecf_xml=$kwsource/babel107b-v0.7_conv-dev.ecf.xml
if [ $step -le 6 ]; then
  echo -e "\n## $0 write out kws `hostname` `date`\n" 
  duration=`head -1 $ecf_xml |\
  grep -o -E "duration=\"[0-9]*[    \.]*[0-9]*\"" |\
  perl -e 'while($m=<>) {$m=~s/.*\"([0-9.]+)\".*/\1/; print $m/2;}'`
  echo -e "\nduation=$duration\n"
  
  $cmd LMWT=$min_lmwt:$max_lmwt $indexdir/LMWT/kws/write_normalized.log \
  set -e ';' set -o pipefail ';' \
  cat $indexdir/LMWT/kws/result.* \| \
  utils/write_kwslist.pl  --Ntrue-scale=$ntrue_scale --flen=0.01 --duration=$duration \
  --segments=$sdir/segments --normalize=true --duptime=$duptime --remove-dup=true\
  --map-utter=$sdir/utter_map --digits=3 \
  - $indexdir/LMWT/kws/kwslist.xml || exit 1

  echo -e "\n## $0 done `date`\n"
fi

if [ $step -le 7 ]; then
  echo -e "\n## $0 ATWV evaluation `hostname` `date` \n"
  $cmd LMWT=$min_lmwt:$max_lmwt \
  $indexdir/LMWT/kws/log/scoring.LMWT.log \
  ilocal/kws/ntu_kws_score.sh  $kwsource \
  ${indexdir}/LMWT/kws || exit 1;
  echo -e "\n## $0 done `date`\n"
fi
