#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n$0 $@\n";
echo -e "\n$0 run on `hostname` @ `date` \n";
# begin user-defined variable
doit=false;
nj=30;
min_lmwt=0.1;
max_lmwt=0.9;
inc_lmwt=0.1;
duptime=0.6;
skip_optimization=false;
max_states=150000;
word_ins_penalty=0;
ntrue_scale=1.0;
frame_dur=0.01;
acwt=12;
#
override_data=
override_mdldir=
override_lat_dir=

#
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if [ $# -ne 1 ]; then
  echo -e "\n\nUsage:\n$0 <config-file>\n"; exit -1;
fi

config=$1; shift;
. $config || die "config file $config expected";

acwt1=`echo "1/$acwt"|bc -l |awk '{printf "%.4f",$1;}'`;
# data preparation 
if [ $stage -le 1 ]; then
  echo -e "\nsilwords_int file $silwords_int preparation\n";
  [ -e $silwords_int ] || \
  cat $silwords | \
  awk '{print $1; }' | \
  isteps/kws/print_silword.pl $lang/words.txt \
  > $silwords_int
fi
# make index from lattice
if [ $stage -le 1 ]; then
  echo -e "\nmake search index from lattice\n";
  # make utter id
  if [ ! -e $kwsdatadir/utter_id  ]; then
    isteps/kws/make_utter_id.pl $segments >$kwsdatadir/utter_id
  fi
  #

  for lmwt in `seq $min_lmwt $inc_lmwt $max_lmwt`; do
    index_dir=$kwsdatadir/acwt${acwt1}_lmwt$lmwt;
    [ -d $index_dir ] || mkdir -p $index_dir;
    logdir=log/kws;
    [ -d $logdir ] || mkdir -p $logdir;
    logfile=$logdir/index_acwt${acwt1}_lmwt${lmwt}.log;
    sbatch -p speech -n $nj -o $logfile \
    isteps/make_index.sh $silence_opt \
    --cmd "$decode_cmd" \
    --acwt $acwt1 \
    --lmwt $lmwt \
    --skip-optimization $skip_optimization \
    --max-states $max_states \
    --word-ins-penalty $word_ins_penalty \
    $kwsdatadir $lang $lat_dir $index_dir || exit 1;
  done 
fi
# prepare search fst using keyword list file
if [ $stage -le 2 ] ;then
  echo -e "\nsearch fst preparation\n";
  rm -rf $kwdir/* 2>/dev/null
  # 
  isteps/kws/make_kwlist_from_xml.pl $keywords_xml \
  > $kwdir/keywords.txt
  #
  cat $kwdir/keywords.txt | \
  awk '{$0 = tolower($0);$1 = toupper($1) ; print; }' \
  > $kwdir/keywords_lower.txt 
  #
  logfile=$kwdir/oow_word.log 
  cat $kwdir/keywords_lower.txt | \
  sym2int.pl --map-oov 0 -f 2- $lang/words.txt \
   > $kwdir/keywords_all.int 2> $logfile
  #
  cat $kwdir/keywords_all.int | \
  awk '{if (/ 0 /||/ 0$/){}else{print ;}}' > $kwdir/keywords.int
  cat $kwdir/keywords_all.int | \
  awk '{if (/ 0 /|| / 0$/){print $1;}}' | \
  ilocal/kws/subset_kwslist.pl $keywords_xml > $kwdir/kwlist_oov.xml
  # 
  transcripts-to-fsts ark:$kwdir/keywords.int \
  ark,t:$kwdir/keywords.fsts
  # Map utterance
  cat $segments | awk '{print $1" "$2}' | \
  sort | uniq > $kwsdatadir/utter_map;
  echo -e "\nmaking search fst done !\n";

fi
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
# search index
if [ $stage -le 4 ] ;then
  echo -e "\nbegin to search index `date`\n";
  [ -e $kwdir/keywords.fsts ] || \
  die "searching fst is not ready ...";
  abs_kwdir=`(cd $kwdir; pwd)`;
  [ -e $kwsdatadir/keywords.fsts ] || \
  ln -s $abs_kwdir/keywords.fsts $kwsdatadir/keywords.fsts;
 
  for lmwt in `seq $min_lmwt $inc_lmwt  $max_lmwt`; do
    index_dir=$kwsdatadir/acwt${acwt1}_lmwt$lmwt;
    Wait $index_dir/.done;
    kwsoutput=$index_dir/kws;
    [ -d $kwsoutput ] || mkdir -p $kwsoutput;
    logdir=log/kws;
    logfile=$logdir/search_index_acwt${acwt1}_lmwt$lmwt.log;
    sbatch -p speech -n$nj -o $logfile \
    isteps/search_index.sh --cmd "$decode_cmd" \
    --indices-dir $index_dir \
    $kwsdatadir $kwsoutput || \
    die "failed in searching source index ...";
  done  
  echo -e "\n searching index ending at `date`\n";
fi

# wait till all necessary are done
for lmwt in `seq $min_lmwt $inc_lmwt $max_lmwt`; do
  index_dir=$kwsdatadir/acwt${acwt1}_lmwt$lmwt;
  kwsoutput=$index_dir/kws;
  Wait $kwsoutput/.done;
done

# do simple result normalization
if [ $stage -le 5 ]; then
  echo -e "\nnormalize searched results `date` \n"
  duration=`head -1 $ecf_xml |\
  grep -o -E "duration=\"[0-9]*[    \.]*[0-9]*\"" |\
  perl -e 'while($m=<>) {$m=~s/.*\"([0-9.]+)\".*/\1/; print $m/2;}'`
  echo -e "\nduation=$duration\n"
  for lmwt in `seq $min_lmwt $inc_lmwt $max_lmwt`; do
    kwsdir=$kwsdatadir/acwt${acwt1}_lmwt${lmwt}/kws;
    log=$kwsdir/log/write_normalized.${lmwt}.log 
rm $kwsdir/batch.sh 2>/dev/null
cat > $kwsdir/batch.sh <<END
#!/bin/bash
     set -e ;  set -o pipefail ; \
    cat $kwsdir/result.* | \
    iutils/write_kwslist.pl \
    --Ntrue-scale=$ntrue_scale \
    --flen=$frame_dur \
    --duration=$duration \
    --segments=$segments \
    --normalize=true \
    --map-utter=$kwsdatadir/utter_map \
    --digits=3  - - | \
    ilocal/filter_kwslist.pl $duptime > \
    $kwsdir/kwslist.xml  || exit 1
END
    chmod +x $kwsdir/batch.sh;
    sbatch -p speech -o $log -n1 \
    $kwsdir/batch.sh
  done
  echo -e "\ndone with search results normalization `date`\n";
fi

# wait till all necessary are done
for lmwt in `seq $min_lmwt $inc_lmwt $max_lmwt`; do
  kwsdir=$kwsdatadir/acwt${acwt1}_lmwt${lmwt}/kws;
  Wait $kwsdir/kwslist.xml;
done

# get ATWV evaluation
if [ $stage -le 6 ]; then
  echo -e "\nGet ATWV for KWS\n";
  decode_cmd=run.pl;
  # ICSI particular environment
  . env.sh || die "env.sh expected ...";

  # remove previous links, in case which are dead links
  ( cd $kwsdatadir; rm ecf.xml kwlist.xml rttm ) 2>/dev/null;
  ln -s $ecf_xml  $kwsdatadir/ecf.xml
  ln -s $keywords_xml  $kwsdatadir/kwlist.xml
  ln -s $rttm  $kwsdatadir/rttm
  # do evaluation
  for lmwt in `seq $min_lmwt $inc_lmwt $max_lmwt`; do
    kwsdir=$kwsdatadir/acwt${acwt1}_lmwt$lmwt/kws;
    log=$kwsdir/scoring.$lmwt.log;
    sbatch -p speech -n1 -o $log \
    ilocal/kws/kws_score.sh  $kwsdatadir \
    $kwsdir || exit 1;  
  done
fi

echo -e "\nDone on `hostname` at `date` \n";
exit 0;
