#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}


. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n$0 $@\n";
echo -e "\n$0 run on `hostname` @ `date` \n";

# begin options
prune=0.9
duptime=0.6
# end options

. parse_options.sh || die "parse_options.sh expected";
# design: $0 kwsource dir  kwslist1 kwslist2 ...
if [ $# -lt 2 ]; then
  echo -e "\n\nUsage:"
  echo "$0 [options]  dir kwslist1 kwslist2 ..."
  echo -e "\n\n"
  exit 1
fi
dir=$1; shift
kws=$1; shift

for x in $kws; do
  [ -f $x ] || die "file $x is not there";
done


array_kws=($kws)
array_prune=(`echo $prune|sed 's#:# #g'`)
prune_size=${#array_prune[@]};
prune_size=$[prune_size+1]
size=${#array_kws[@]}
[[ $prune_size -eq $size ]] || die "prune size is inequal to component size "
mkdir -p $dir;

j=2;
kws=${array_kws[0]};
echo -e "\n## mergeing started `date`\n"
for i in `seq 1 $[size-1]`; do
  kws1=${array_kws[$i]}; k=$[i-1]; prune=${array_prune[$k]}
  echo -e "\n## merge $kws1 `hostname`  `date`\n"
  echo -e "\n## kws=$kws, kws1=$kws1\n"
  iutils/kws/merge_kwslist_xml_aux_yes02.pl $kws $kws1 $duptime $prune 2> $dir/LOG.$i.merge \
  >$dir/kwslist.$j.xml 
  kws=$dir/kwslist.$j.xml
  j=$[j+1]
done

echo -e "\n## merging done `date`\n"; exit 0

KWSEval -e $kwsource/ecf.xml -r $kwsource/rttm -t $kwsource/kwlist.xml \
-s $kws  -c -o -b -d -f $dir/
