#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Log {
  echo -e "\n`date`:$1 @ `hostname`\n";
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
doit=false;
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if [ $stage -le 1 ]; then
  srcdata=../w1/107/data/dev;
  data=data/107/llp/bn/dev11_semi0.5
  nnetdir=exp/fbank/bn2_semi0.5;
  isteps/imake_bn_feats.sh --fbfeature true \
  $data $srcdata $nnetdir $data/_log \
  $data/_data || die "failed to make bn features";
fi
# decode the data
if [ $stage -le 2 ]; then
  echo -e  "\nfmllr decode `date` \n";
  nj=30;
  cmd=run.pl;
  acwt=0.05;
  lattice_beam=8.0;
  data=data/107/llp/bn/dev11_semi0.5;
  exp_dir=exp/bn_semi0.5/tri3sat;
  dir=$exp_dir/decode_`basename $data`;
  graph=$exp_dir/graph;
  if [ ! -e $graph/HCLG.fst ]; then
    die "decoding graph is not ready in $graph";
  fi
  if $doit; then
    steps/decode_fmllr.sh --nj $nj \
    --acwt $acwt \
    --cmd $cmd  $graph $data $dir;
  fi
  # sgmm2 decode
  echo  -e "\nsgmm2 decode `date`\n";
  transform_dir=$dir;
  exp_dir=exp/bn_semi0.5/sgmm2_5;
  dir=$exp_dir/decode_`basename $data`_latbeam8_fmllr;
  graph=$exp_dir/graph;
  if [ ! -e $graph/HCLG.fst ]; then
    die "decoding graph is not ready in $graph";
  fi
  [ -e $transform_dir/trans.1 ] || \
  die "transform is not ready in $transform_dir";
  steps/decode_sgmm2.sh --nj $nj \
  --acwt $acwt \
  --lattice-beam $lattice_beam \
  --use-fmllr true \
  --cmd "$cmd" \
  $graph $data $dir
fi
# decode the development data to
# generate even deeper lattice
if [ $stage -le 3 ] ;then
  echo -e "generate denser lattice";
  nj=30;
  cmd=run.pl;
  acwt=`echo "1/20"|bc -l | awk '{printf "%.4f",$1;}'`;
  acwt=0.1;
  lattice_beam=8.0;
  data=data/107/llp/bn/dev11_semi0.5;
  transform_dir=exp/bn_semi0.5/tri3sat/decode_dev11_semi0.5;
  exp_dir=exp/bn_semi0.5/sgmm2_5;
  dir=$exp_dir/decode_`basename $data`_latbeam${lattice_beam}_acwt$acwt;
  graph=$exp_dir/graph;
  
  [ -e $transform_dir/trans.1 ] || \
  die "transform is not ready in $transform_dir";
  isteps/decode_sgmm2.sh --nj $nj \
  --acwt $acwt \
  --lattice-beam $lattice_beam \
  --transform-dir $transform_dir \
  --cmd "$cmd" \
  $graph $data $dir

fi
