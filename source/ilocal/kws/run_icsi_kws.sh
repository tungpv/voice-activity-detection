#!/bin/bash


function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Wait {
  if [ $# -ne 1 ]; then
    die "waiting objective expected";
  fi
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n$0 $@\n";
echo -e "\n$0 run on `hostname` @ `date` \n";
# begin user-defined variable
doit=false;
nj=30;
#

#
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

if [ $# -ne 1 ]; then
  echo -e "\n\nUsage:\n$0 <config-file>\n"; exit -1;
fi

config=$1; shift;
[ -e $config ] || \
die "config file $config does not exist ";
. $config;
#
 # make hescii words
if [ $stage -le 1 ]; then
  echo -e "\nmake hescii dictionary\n";
  iutils/hescii-words.py < $words > $hescii_words
fi
#
if [ $stage -le 2 ]; then
  echo -e "\nkaldi lattice to htk lattice format conversion\n";
  nj=`cat $latdir/num_jobs`
  [ -d $slfdir/log ] || mkdir -p $slfdir/log 2>/dev/null 
  rm $slfdir/.done 2>/dev/null
  sbatch  -p speech -o $slfdir/log/fst2slf.log -n$nj --mem-per-cpu=3000MB \
  isteps/mk_aslf_sgmm2.sh ${transform_dir:+ --transform-dir $transform_dir} \
   --cmd "$train_cmd" \
   --use-fmllr true \
  $graphdir $data \
  $latdir  $slfdir || exit 1;
fi
# keyword spotting work
if [ $stage -le 3 ]; then
  Wait $slfdir/.done;
  echo -e "\nicsi kws\n";
  . env.sh || die "env.sh expected";
  [ -d $dir_srs_go ] || mkdir -p $dir_srs_go
  log=$dir_srs_go/scoring.log;
  [ -e $log ] && rm $log;
  srs-go -dir $dir_srs_go -template $template_dir \
  -config $kws_config kws_input_lattice_directory $slfdir \
  kws_kwslist_output_path  $dir_srs_go/kwslist.xml \
  kws_scoring_directory $dir_srs_go \
  kws_ecf_file $ecf \
  kws_scoring_ecf_file $ecf_scoring \
  kws_rttm_file $rttm
  
fi
