#!/usr/bin/perl -w

# Copyright 2012  Johns Hopkins University
# Apache 2.0.
#
use strict;
use warnings;
use lib "/n/shokuji/da/haihuaxu/.cpan/build/XML-Simple-2.20-uhlKAo/lib";
use lib "/n/shokuji/da/haihuaxu/.cpan/build";

use XML::Simple;
use Data::Dumper;

binmode STDOUT, ":utf8";
my %keywords = () ;
while (<STDIN>) {
  chomp;
  my $line = $_;
  # push @keywords, $line;
  $keywords{$line} = 0;
  # print "line=$line\n";
}

#print "Will retain ",  scalar(@keywords), " keywords\n";

my $data = XMLin($ARGV[0], ForceArray => 1);

#print Dumper($data->{kw});
my @filtered_kws = ();

foreach my $kwentry (@{$data->{kw}}) {
  # print $kwentry->{kwid}, "\n";
  if ( exists $keywords{$kwentry->{kwid}} ) {
    push @filtered_kws, $kwentry;
  }
}
$data->{kw} = \@filtered_kws;
my $xml = XMLout($data, RootName=> "kwlist", KeyAttr=>'');
print $xml; 
exit 0
