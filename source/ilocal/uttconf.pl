#!/usr/bin/perl -w

sub LoadSilVocab {
  my ($eFName, $vocab) = @_;
  open FILE, "$eFName";
  while (<FILE>) {
    chomp;
    next if /^$/;
    m/(\S+)/ || next;
    $$vocab{$1} = 0;
  }
  close FILE;
}
#
sub LogAdd {
  my ($x, $y) = @_;
  my $minLogDiff = -23.0258;
  my $diff;
  if ($x < $y) {
   $diff = $x - $y;
    $x = $y;
  } else {
    $diff = $y - $x;
  }
  if ($diff >= $minLogDiff) {
    my $z = exp ($diff);
    return $x + log(1.0 + $z);
  } 
  return $x;
}
# 
sub InsertNewWord {
  my ($vocab, $wordId, $logPost) = @_;
  if (exists $$vocab{$wordId}) {
     my $x = $$vocab{$wordId};
     my $y = $logPost;
     $y = LogAdd($y, $x);
     $$vocab{$wordId} = $y;
     return;
  }
  $$vocab{$wordId} = $logPost;
}
# decompose word posterior string from a lattice
# into two arrays, one is word array with time
# boundary info, and the other is accumulated
# time-frame word posterior.
sub ReorganizeWordPost {
  my ($latWordPostStr, $timeWordArray, $timePostArray, $frameMaxPostArray) = @_;
  my @tmpArray = split /;/, $$latWordPostStr;
  return if scalar @tmpArray < 1;
  for (my $idx = 0; $idx < scalar @tmpArray ; $idx ++) {
    my $wStr = $tmpArray[$idx];
    my @a1 = split / /, $wStr;
    scalar @a1 == 4 || die "entry length is illegal ", scalar @a1, "\n";
    my ($wordId, $post) = ($a1[0], $a1[3]);
    my @a2 = @a1; pop @a2;
    push @$timeWordArray, \@a2; # build time word array
    for (my $j = $a1[1]; $j < $a1[2]; $j ++) {
      my $vocab_ref;
      if (! defined $$timePostArray[$j]) {
        my %vocab = ();
        $$timePostArray[$j] = \%vocab;
      } 
      $vocab_ref = $$timePostArray[$j];
      InsertNewWord ($vocab_ref, $wordId, $post);
      ## keep  max posterior for each  frame
      if ($j >= scalar @$frameMaxPostArray) {
        push (@$frameMaxPostArray, $post); 
      } else {
        if ($post > @$frameMaxPostArray[$j]) {
          $$frameMaxPostArray[$j] = $post;
        }
      }
    }
  }
}
#
sub GetWordMaxPost {
  my ($timePostArray, $wordRefStr) = @_;
  my ($wordId, $sTime, $eTime) = @$wordRefStr;
  my $max = -1.0e10;
  for (my $idx = $sTime; $idx < $eTime; $idx ++) {
    my $vocabRef = $$timePostArray[$idx];
    defined $vocabRef || die "GetWordMaxPost: empty index in timePostArray";
    exists $$vocabRef{$wordId} || die "GetWordMaxPost: 
    no word id $wordId in timePostArray at frame $idx";
    my $x = $$vocabRef{$wordId};
    if ($x > $max) {
      $max = $x;
    }
  }
  return exp ($max);
}
# ------------- main entrance point -----
my $argNum = scalar @ARGV;
my $idx = 0;
my $eFName = "";
my $framePost ="";
while ($idx < $argNum) {
  my $swStr = $ARGV[0];
  if ($swStr =~ /--silence/) {
    shift @ARGV; $idx ++;
    $eFName = $ARGV[0]; shift @ARGV; $idx ++;
    next;
  }
  if ($swStr =~ /--frame-post/) {
    shift @ARGV; $idx ++;
    $framePost=$ARGV[0]; shift @ARGV; $idx ++;
    next;
  }
  last;
}
# print scalar @ARGV, "\n";
scalar @ARGV == 2 || die "\n\nUsage:\n",
"utt_conf.pl silence-word-list(int) utt-conf-file\n\n";
my ($swords, $uttConfFName) = @ARGV;
my %silVocab = ();
LoadSilVocab($swords, \%silVocab);
open OFILE, "$uttConfFName" or die "file $uttConfFName cannot open\n";

print STDERR "stdin expected\n";
while (<STDIN>) {
  chomp;
  next if /^$/;
  m/(\S+)\s+(.*)/|| next;
  my ($labName, $str) = ($1, $2);
  next if $str =~ /^$/;
  my @timeWordArray = ();
  my @timePostArray = ();
  my @frameMaxPostArray = ();

  # print "str=$str\n";
  ReorganizeWordPost (\$str, \@timeWordArray, \@timePostArray, \@frameMaxPostArray);
  # calculate each word maximum posterior 
  # and accumulate and average
  my $totFrames = 0;
  my $totPost = 0;   my %wordTimeVocab = (); my $latDur = 0; 
  my $silWordDur = 0;
  for(my $idx = 0; $idx < scalar @timeWordArray; $idx ++) {
    my $aRef = $timeWordArray[$idx];
    my $wordId = $$aRef[0];  my $curDur = $$aRef[2] - $$aRef[1];
    $latDur += $curDur;
    if (exists $silVocab{$wordId}) {
      $silWordDur += $curDur;
    }
    next if exists $silVocab{$wordId}; # we won't count silence word in.
    my ($sTime, $eTime) = ($$aRef[1], $$aRef[2]);
    my $max_post = GetWordMaxPost (\@timePostArray, $aRef);
    $totFrames += $eTime - $sTime;
    $totPost += ($eTime - $sTime)*$max_post;
  }
  my $conf = 0;
  if ($totFrames != 0) {
    $conf = $totPost / $totFrames;
  }
  my $conf_str = sprintf "%.4f", $conf;
  print OFILE "$labName $conf_str\n";
}
print STDERR "stdin done\n";
close OFILE;
