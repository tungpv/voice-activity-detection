#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl;
nj=10;
step=0
substep=0
prepare_data=false
tandem=false
semibnf=false
cutoff=0.4    # utterance confidence threshold to select utterance
inv_acwt=12;
exclude=;
lang=data/llp/lang; lang_test=${lang}_test
samples_per_iter=4000000;  
max_mem=500000000;
state=2500;gauss=20000
pnorm_input_dim=3000; pnorm_output_dim=500; 
nonspeech_words=$lang/nonspeech_words.txt

# end tunable variable
bn_am_cf=conf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";

echo -e "\ncutoff=$cutoff, inv_acwt=$inv_acwt\n"

if [ $# -ne 5 ]; then
  echo -e "\n\nExample: \n";
  echo " ilocal/semi/make_utrain.sh --cutoff 0.4 --inv-acwt 9 data  lang mdldir latdir dir";
  echo -e "\n\n"
  exit 1
fi

data=$1; shift
lang=$1; shift;
sdir=$1; shift;
latdir=$1; shift;

dir=$1; shift

[ -f $data/segments ] || die "segments $data/segments expected";
silence_opt=`cat $lang/silence_opt`;
[ -f $latdir/lat.1.gz ] || die "lat.1.gz expected in folder $latdir"
 wordpost=$dir/wordpost; uttpost=$dir/uttpost;
 nonspeech_words=$lang/nonspeech_words.int; 
[ -f $nonspeech_words ] || \
die "nonspeech_words $nonspeech_words is not there";

if [[ ! -f $wordpost/post.1.gz || ! -f $uttpost/uconf.1.gz ]]; then
  echo -e "\n## making word post files and utterance post files `date`\n"
  isteps/um/utt_conf.sh --cmd "$cmd" --inv-ascale $inv_acwt  \
  --silence-opts "$silence_opt" --non-speech $nonspeech_words \
  $latdir $lang $sdir $wordpost $uttpost || exit 1
  echo -e "\n## done `date`\n"
fi
echo -e "\n## $0 check wordpost $wordpost and uttpost $uttpost folder\n"

selected=$dir/cutoff$cutoff;
mkdir -p $selected/tmp 2>/dev/null
if [ -z $exclude ]; then
  echo -e "\n## direct copy\n"
  cp $data/segments $selected/tmp 
else
  echo -e "\n## filtering with $exclude file\n"
  cat $data/segments | \
  perl -e '
    ($seg) = @ARGV;
    %vocab = ();
    open S, "<$seg" or die "seg file $seg cannot open";
    while(<S>) {
      chomp;
      m/(\S+)\s+(.*)/ or next;
      $vocab{$1} = 0;
    }
    close S;
    while(<STDIN>) {
      chomp;
      m/(\S+)\s+(.*)/ or next;
      next if (exists $vocab{$1});
      print "$_\n";
    }
  ' $exclude/segments   > $selected/tmp/segments
fi
echo -e "\n## check segments $selected/tmp/segments, it should not contain supervised files `date`\n"

uttconf_list="gzip -cdf $uttpost/uconf.*.gz|";
if [ ! -f $selected/segments ]; then
  iutils/um/conf_utt_select.pl --thresh $cutoff \
  --selected $selected/segments "$uttconf_list" $selected/tmp/segments  \
        >$selected/uttconfs 2>> $selected/log
fi
if [[ `wc -l < $selected/segments` -eq 0 ]]; then
  die "no segments file created in $selected folder";
fi
min_lmwt=$[inv_acwt-1]; 
max_lmwt=$[inv_acwt+1];
itran=$dir/$inv_acwt.tra
if [ ! -f $itran ]; then
  echo -e "\n## making best path out of lattice `date`\n"
  $cmd LMWT=$min_lmwt:$max_lmwt $dir/log/best_path.LMWT.log \
  lattice-best-path --lm-scale=LMWT --word-symbol-table=$lang/words.txt \
  "ark:gunzip -c $latdir/lat.*.gz|" ark,t:$dir/LMWT.tra || exit 1;
  echo -e "## done `date`"
fi
if [ ! -f $itran ]; then
  die "best path expected in $dir";
fi
echo -e "\n## making best path $itran  succeeded `date`\n"


udir=$selected/utrain;
mkdir -p $udir/tmp 2>/dev/null
cp $data/* $udir/tmp;
rm $udir/tmp/{text,segments,cmvn.scp} 


nonspeech_words=$lang/nonspeech_words.txt;
if [ ! -f $udir/tmp/text ]; then
  cat $itran | \
  utils/int2sym.pl -f 2- $lang/words.txt | \
  iutils/filter_utt.pl $nonspeech_words  2>$udir/tmp/filter_text.log  | \
  perl -e ' 
  ($seg, $nseg, $ntxt) = @ARGV; 
  open SEG, "<$seg"; 
  open NS, ">$nseg" or die "file $nseg cannot open";
  open NT, ">$ntxt" or die "file $ntxt cannot open";  
  while (<SEG>) {
    chomp;
    m/(\S+)\s+(.*)/;
    $vocab{$1} = $_;
  }
  while (<STDIN>) {
    chomp;
    m/(\S+)\s+(.*)/;
    next if not exists $vocab{$1};
    print NT "$_\n";
    print NS $vocab{$1}, "\n";
  }  close NS; close NT;
  close SEG;'  $selected/segments  $udir/tmp/segments  $udir/tmp/text ;
  filtered=`wc -l <$udir/tmp/filter_text.log`
  echo -e "\n##filtered=$filtered in folder $dir_booked\n" 
fi
[ -f $udir/tmp/text ] || die "text file in $udir/tmp expected";

echo -e "\n## making utrain data `date`\n"
isteps/um/mksubset_dir.sh  --include $udir/tmp/segments $udir/tmp $udir || exit 1
if [ ! -f $udir/text ]; then
  die "\n## text file is not generated in $udir\n";
fi
echo -e "#\n## $0 check folder $udir\n#"

