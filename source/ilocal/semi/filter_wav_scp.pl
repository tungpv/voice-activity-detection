#!/usr/bin/env perl 
use warnings;
use strict;

my ($srcFile, $sphCmd, $segFile) = @ARGV;

my %vocab = ();

print STDERR "\n $0 stdin expected\n";
while (<STDIN>) {
  chomp;
  $vocab{$_} = 0;
}
print STDERR "\n$0 stdin ended\n";

open FILE, "<$srcFile" or
die "source file $srcFile cannot open\n";
open SEG, "$segFile" or 
die "$0 seg file $segFile cannot open\n";
while (<FILE>) {
  chomp;
  my @a = split /\s+/;
  my $size = scalar @a;
  my $lineStr = $a[$size -1];
  next if exists $vocab{$a[0]};
  my $time = $a[3]; 
  $time =~ s/:/ /g;
  $lineStr =~ s/^.*\///g; 
  $lineStr =~ s/\.sph\|$//g; 
  print SEG "$a[0] $lineStr $time\n";
  $a[0] = $lineStr; $a[1] = $sphCmd; $a[2] = ""; $a[3] = "";
  $a[4]="-p -f wav"; $a[5] = "";
  $a[6] = "";
  $lineStr = join " ", @a;
  $lineStr =~ s/ +/ /g;
  print join " ", @a, "\n";
}

close FILE;
close SEG;
