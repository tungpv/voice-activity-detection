#!/bin/bash


echo -e "\nUSER: $0 $@\n"
if [ $# -ne 3 ]; then
  echo -e "\n$0 <flp_data> <llp_data> <utrain>\n"; exit 1;
fi

flp_data=$1; shift; 
llp_data=$1; shift; 
dir=$1; shift;
. path.sh  

for x in $flp_data $llp_data; do
  if [ ! -d $x ]; then
    echo "folder $x does not exist" && exit 1;
  fi
done
mkdir -p  $dir 2>/dev/null
sphCmd="/opt/kaldi_updated/trunk/tools/sph2pipe_v2.5/sph2pipe"
cat $llp_data/segments | awk '{print $1;}' | \
ilocal/semi/filter_wav_scp.pl  $flp_data/wav.scp $sphCmd  "|sort -u > $dir/segments"  |sort -u >$dir/wav.scp;


perl -e  '($seg, $txt) = @ARGV; open SEG, "<$seg" or die "seg cannot open"; while (<SEG>) {@a =split /\s+/; $vocab{$a[0]} =0;  } open TXT, "<$txt"; while(<TXT>) { @a1 = split /\s+/; if (exists $vocab{$a1[0]}) {print $_;}  }  close TXT; close SEG;'  $dir/segments $flp_data/text | sort -u > $dir/text

cat $dir/segments | \
perl -ne '@a = split /\s+/; print "$a[0] $a[1]\n";' |sort -u  > $dir/utt2spk;

cat $dir/utt2spk |\
perl -e 'while (<>){ chomp; @a =split /\s+/; if (exists $vocab{$a[1]}) { $vocab{$a[1]} .= " $a[0]"; }else { $vocab{$a[1]} = $a[0];   } } foreach $spk ( keys %vocab){ print $spk," ", $vocab{$spk}, "\n"; } '| sort -u  >  $dir/spk2utt

echo -e "\n$0 done `date`\n" && exit 0
