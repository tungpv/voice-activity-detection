#!/bin/bash

. path.sh
. cmd.sh

# begin options
see_network=false
# end options

. parse_options.sh

function Options {
  cmd=`echo $0| perl -pe 's/.*\///g;'`
  cat <<END

$cmd [Options]:

see_network         # value, $see_network

END
}

Options;

if $see_network; then
  source/see_network.sh 
fi
