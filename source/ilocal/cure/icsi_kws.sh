#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit 1;
}

ecf=/u/drspeech/projects/swordfish/IndusDB/IndusDB.latest/babel107b-v0.7_conv-dev.ecf.xml
index="bzip2 -cd SRS-GO/data/scratch_ttmp/indexdir/split.lat.index.bz2|"
for x in $ecf; do
  [ -e $x ] || die "$x does not ready";
done

iutils/cure/icsi_ecf_oov_check.pl $ecf "$index"
