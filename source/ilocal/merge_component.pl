#!/usr/bin/env perl
use warnings;
use strict;

my $numArgs = scalar @ARGV;

if ($numArgs != 1) {
  die "\n\nExample: cat x.txt | $0 y.txt > z.txt\n\n";
}

my ($fname) = @ARGV;
open (F, "$fname") or die "file $fname cannot open\n";
my %vocab = ();
my $vector_dim = 0;
my @aTemp = ();
while(<F>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(.*)/ or next;
  $vocab{$1}{$2} =$3;
  if ($vector_dim == 0) {
    my @a = split (/\s+/,$3);
    $vector_dim = scalar @a;
    for(my $i = 0; $i < $vector_dim; $i++) {
      push @aTemp, 0;
    }
  }
}
close F;
print STDERR "$0: stdin expected\n";

while(<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(.*)/ or next;
  if(not exists $vocab{$1}{$2}) {
    print $_, " ", join " ", @aTemp, "\n";
  }else {
    my $s = $vocab{$1}{$2};
    print $_, " ",$s, "\n";
  }
}
print STDERR "$0: stdin ended\n";
