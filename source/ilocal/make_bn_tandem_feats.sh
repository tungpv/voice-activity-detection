#!/bin/bash

# make mfcc + plp + lda+mllt(bn) fused feature

# Begin configuration section.  

nj=4
cmd=run.pl
lda_mllt=
norm_vars=false
select=
# End configuration section.
echo "$0 $@"  # print the commande line for logging

. path.sh || exit 1
. parse_options.sh || exit 1

[ $# -lt 5 ] && echo "tandem data argument is not correct" && exit 1;

data=$1; shift;
x=$[$#-1];
logdir=${!x};   # echo "logdir=$logdir";
x=$[$#]
feadir=${!x};   # echo "feadir=$feadir"; exit 0;
comp_num=$[$#-2];
idx=0;
for ((idx=0; idx < $comp_num; idx++)) do
  array_data[$idx]=$1; shift;
done
mkdir -p $data $logdir $feadir
# echo "data folder is $data"
idx=0
if [ ! -z $select ]; then
  for x in `echo $select | sed 's:#: :g'`; do
    array_select[$idx]=$x
    idx=$[idx+1]
  done
fi
# splice the mfcc+plp feature first
num=$[comp_num-1];
feats="ark,s,cs:paste-feats"
for (( idx=0; idx < num ; idx++)) do
  sdir=${array_data[$idx]}
  sdata=$sdir/split$nj;
  [[ -d $sdata && $sdir/feats.scp -ot $sdata ]] || split_data.sh $sdir $nj ||exit 1;
  echo "$0: $sdata splitted";
  basefeats="ark,s,cs:apply-cmvn --norm-vars=$norm_vars --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- |"
  if [ ! -z $select ]; then
    curstr=${array_select[$idx]}
    array_str=(`echo $curstr|sed 's#:# #g'`)
    array_size=${#array_str[@]}
    [ $array_size -ne 3 ] && echo "selection string size is misconfigured" && exit 1
    start=${array_str[0]}; end=${array_str[1]}; delta_order=${array_str[2]}
    echo "start=$start, end=$end, delta_order=$delta_order" 
    if [[ $start -ge 0 && $end -gt $start ]]; then
      echo "feature selection from $[start+1] to $[end+1] for data $sdir";
      basefeats="$basefeats select-feats ${start}-${end} ark:- ark:- |"
    fi
    if [ $delta_order -gt 0 ]; then
      echo "feature selection: delta order is $delta_order"
      basefeats="$basefeats add-deltas --delta-order=$delta_order ark:- ark:- |"
    fi 
  fi
  feats="$feats '$basefeats'"
done
# echo "tuning is on work" && exit 1;

# now BN feature
srcdir=${array_data[$idx]};
sdata=$srcdir/split$nj;
[[ -d $sdata && $srcdir/feats.scp -ot $sdata ]] || split_data.sh $srcdir $nj ||exit 1;
bnfeats="ark,s,cs:apply-cmvn --norm-vars=$norm_vars --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- |" 

if [ ! -z $lda_mllt ]; then
  sdir=$lda_mllt
  splice_opts=`cat $sdir/splice_opts`
  bnfeats="$bnfeats splice-feats $splice_opts ark:- ark:- | transform-feats $sdir/final.mat ark:- ark:- |"
fi
feats="$feats '$bnfeats' ark:- |"

echo "data folder is $data";
rm $data/paste_feats 2>/dev/null 
echo "$feats" >  $data/paste_feats

#prepare the dir
cp $srcdir/* $data; rm $data/{feats.scp,cmvn.scp};

# make $bnfeadir an absolute pathname.
# echo "feadir=$feadir"; exit 0;
feadir=`perl -e '($dir,$pwd)= @ARGV; if($dir!~m:^/:) { $dir = "$pwd/$dir"; } print $dir; ' $feadir ${PWD}`

name=`basename $data`

#forward the feats
$cmd JOB=1:$nj $logdir/make_bn_tandem_feats.JOB.log \
  copy-feats "$feats" ark,scp:$feadir/$name.JOB.ark,$feadir/$name.JOB.scp || exit 1;
   
#merge the feats to single SCP
rm $data/feats.scp
for n in $(seq 1 $nj); do
  cat $feadir/$name.$n.scp 
done > $data/feats.scp

echo "Ended"
exit 0

