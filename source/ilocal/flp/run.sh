#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl;
nj=10;
step=0
substep=0
prepare_data=false
lang=data/flp/lang; lang_test=${lang}_test
samples_per_iter=4000000;  
max_mem=500000000;
state=2500;gauss=20000
pnorm_input_dim=3000; pnorm_output_dim=500; 
nonspeech_words=$lang/nonspeech_words.txt

# end tunable variable
bn_am_cf=conf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";

datastr="train dev10h" ;

flp_train=/media/lychee_w1/hhx502/kws14/nist/IARPA-babel204b-v1.1b/BABEL_OP1_204/conversational/training/transcript_roman 
if $prepare_data; then
  dir=data/local/flp/train
  if [ $step  -le 1 ]; then
    echo -e "\n## prepare text `date`\n"
    mkdir -p $dir 2>/dev/null
    iutils/prepare_seg_text.pl  "ls $flp_train/*.txt"  >  $dir/raw.txt  
    echo -e "\n## done check $dir/raw.txt `date`\n"
  fi
  if [ $step -le 5 ]; then
    paste <(cut -d " " -f 1 $dir/raw.txt )  <(cut -d " " -f 2- $dir/raw.txt | uconv -f utf8 -t utf8 -x "Any-Lower" ) > $dir/raw_lower.txt
    cat $dir/raw_lower.txt | \
    awk '{for(i=4; i<=NF; i++){ print $i; }}' | sort -u > $dir/raw_lower.wlist
    cat  $dir/raw_lower.txt | \
    sed 's#<no-speech>##g'| \
    sed -e 's/[\*\~]//g' |  \
    sed 's#(())#<unk>#g' | \
    sed 's#<breath>#[BREATH]#g' | \
    sed 's#<click>#[NOISE]#g' | \
    sed 's#<cough>#[COUGH]#g' | \
    sed 's#<dtmf>#[NOISE]#g' | \
    sed 's#<female-to-male>##g' | \
    sed 's#<male-to-female>##g' | \
    sed 's#<foreign>##g' | \
    sed 's#<int>#[NOISE]#g' | \
    sed 's#<laugh>#[LAUGH]#g' | \
    sed 's#<lipsmack>#[NOISE]#g' | \
    sed 's#<overlap>##g' | \
    sed 's#<prompt>#[NOISE]#g' |\
    sed 's#<ring>#[NOISE]#g' | \
    sed 's#<sta>#[NOISE]#g' | \
    awk '{lab=$1; $1=""; gsub(/_/," ", $0);print lab, $0; }' | \
    awk '{lab=$1; $1 = ""; for(i=1;i<=NF;i++){ gsub(/^-/,"",$i); gsub(/-$/,"",$i); }  print lab, $0; }' > $dir/raw_lower_step01.txt
    cat $dir/raw_lower_step01.txt | \
    awk '{for(i=4; i<=NF; i++){ print $i; }}' | sort -u > $dir/raw_lower_step01.wlist
    echo -e "\n## check $dir/raw_lower_step01.txt && $dir/raw_lower_step01.wlist  `date`\n" 
  fi
nist_lex=nist/IARPA-babel204b-v1.1b/BABEL_OP1_204/conversational/patched/reference_materials/lexicon.txt
   if [ $step -le 10 ]; then
     dir=data/local/flp/dict;
     mkdir -p $dir 2>/dev/null
     cat $nist_lex | \
     perl -ne 'chomp; 
       m/(\S+)\s+(\S+)\s+(.*)/ or die "illegal entry $_";
       print STDERR $1, "\n";
       print "$2\t$3\n"; 
     '   2>$dir/tamil.wlist  > $dir/raw_lex.txt
     paste <(cut -f 1 $dir/raw_lex.txt | uconv -f utf8 -t utf8 -x "Any-Lower" )  <(cut -f 2- $dir/raw_lex.txt) > $dir/raw_lex_lower.txt
     echo -e "\n## check oov case first time\n"
     cat data/local/flp/train/raw_lower_step01.wlist | \
     perl -e '  $lex = shift @ARGV;
       open LEX, "<$lex" or die "lexicon $lex cannot open\n";
       while (<LEX>) {
         chomp;
         m/(\S+)\s+(.*)/;  $vocab{$1} = 0;
       }
       close LEX;
       while (<STDIN>) {
        chomp; 
        # print STDERR "$_\n";
        if (not exists $vocab{$_}) {
          print "$_\n";
        } 
       }  
     ' $dir/raw_lex_lower.txt |sort -u  > $dir/raw_oov_step01.txt
     echo  -e "\n## check $dir for dict preparation `date`\n"
   fi 
   if [ $step -le 15 ]; then
     echo -e "\n## filter training text `date`\n"
     dir=data/local/flp/train;
     cat $dir/raw_lower_step01.txt |\
     perl -e '  $noise = shift @ARGV;
       open N, "<$noise" or die "noise file $noise cannot open\n";
       while (<N>) {
        chomp;
        m/(\S+)/;
        $vocab{$1} = 0;
       }
       close N;  
       while (<STDIN>) {
         chomp;
         @A = split /\s+/;
         if (scalar @A == 3) {
           print STDERR $_,"\n";  next;
         }
         $deserted=1;
         for($i = 3; $i < scalar @A; $i ++) {
           $w = $A[$i];
           if(not exists $vocab{$w}) { 
             $deserted = 0; last;
           }
         }
         if ($deserted == 1) {
           print STDERR $_, "\n"; next;
         }
         print $_ , "\n";
       } '  $dir/../dict/noise_words.txt  > $dir/raw_lower_step02.txt 2>$dir/raw_deserted_step02.txt
     echo -e "\n## check $dir/raw_lower_step02.txt and $dir/raw_deserted_step02.txt\n"
   fi 
   if [ $step -le 20 ]; then
     echo -e "\n## prepare kaldi data\n"
     dir=data/local/flp/train/kaldi;
     mkdir -p $dir 2>/dev/null
     cat $dir/../raw_lower_step02.txt | \
     perl -e '
       ($seg, $txt) = @ARGV;
       open SEG, ">$seg" or die "segment file $seg cannot open\n";
       open TXT, ">$txt" or die "text file $txt cannot open\n";
       $unit=100000;
       while (<STDIN>) {
         chomp;
         m/(\S+)\s+(\S+)\s+(\S+)\s+(.*)/ or next;
         my ($segName, $start, $end, $trans) = ($1, $2, $3, $4);
         $lab = sprintf "%s_%010d_%010d", $segName, $start*$unit, $end*$unit;
         print TXT "$lab\t$trans\n";
         print SEG "$lab\t$segName\t$start\t$end\n"; 
       }
       close SEG;
       close TXT;
     ' $dir/segments $dir/text 
      cat $dir/segments | \
      perl -ne '
        m/(\S+)\s+(\S+)\s+(.*)/;
        print "$1\t$2\n";
      '  > $dir/utt2spk
      utils/utt2spk_to_spk2utt.pl <$dir/utt2spk >$dir/spk2utt
      echo -e "\ncheck file $dir/utt2spk and $dir/spk2utt\n";
   fi
   pd=/media/lychee_w1/hhx502/kws14/nist/IARPA-babel204b-v1.1b;
   wavdir=$pd/BABEL_OP1_204/conversational/training/audio;
   sox=/usr/bin/soxi;
   sph2pipe=/opt/kaldi3550/trunk/tools/sph2pipe_v2.5/sph2pipe;
   if [ $step -le 25 ]; then
     echo -e "\n## prepare wave data `date`\n"
     dir=data/local/flp/train/kaldi;
     ls $wavdir/* | egrep "\.wav$|\.sph$" | \
     perl -e '
      ($sph2pipe)  = @ARGV;
      while (<STDIN>) {
        chomp;
        $lab = $_; $lab =~ s/.*\///g;  $lab =~ s/\.wav$//g; 
        $lab =~ s/\.sph$//g;
        # print $lab, "\n";
        if (/\.sph/) {
         print $lab, " $sph2pipe  -f wav -p -c 1 $_|\n";
        } elsif (/\.wav/) {
         print $lab, " $_\n";
        } else {
          die "unidentified files $_\n";
        }
      }
     '   $sph2pipe |sort -u > $dir/wav.scp
     echo -e "\n## check file $dir/wav.scp\n"
   fi
   if [ $step -le 30 ]; then
     sraw_lex=data/local/flp/dict/raw_lex_lower.txt
     dir=`dirname $sraw_lex`
     echo -e "\n## prepare word dict `date`\n"
     cat $sraw_lex | \
     perl -ne '
       chomp;
       @A = split /\t/;
       $nA = scalar @A;
       if($nA == 2) {
         print "$A[0]\t$A[1]\n";
       } elsif ($nA > 2) {
         $w = shift @A;
         for($i = 0; $i < $nA -1; $i++) {
           $pron = $A[$i];
           print "$w\t$pron\n";
         }
       }
     '   | \
     sed -e 's#\.# #g' | \
     sed -e 's:#: :g' | \
     sed  -e 's/ +/ /g' | \
      sort -u > $dir/lexicon01.txt
     echo -e "<unk>\tSPN\n[BREATH]\tBRE\n[COUGH]\tCOU\n[LAUGH]\tLAU\n[NOISE]\tNSN" | \
     cat - $dir/lexicon01.txt > $dir/lexicon.txt
     echo -e "\n## check $dir\n"
     cat $dir/lexicon.txt | \
     cut -f 2- | awk '{for(i=1;i<=NF;i++){print $i; }}' | sort -u > $dir/phones.txt
     echo -e "\n## checking $dir/phones.txt `date`\n"
   fi
##   
fi
