#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl;
nj=10;
step=0
substep=0
prepare_data=false
lang=data/llp/lang; lang_test=${lang}_test
samples_per_iter=4000000;  
max_mem=500000000;
state=2500;gauss=20000
pnorm_input_dim=3000; pnorm_output_dim=500; 
nonspeech_words=$lang/nonspeech_words.txt

# end tunable variable
bn_am_cf=conf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";

datastr="train dev10h" ;

if $prepare_data; then
  echo -e "\n## prepare_data started `hostname` `date`\n"
  utrain=data/utrain;
  if [ $step -le 1 ]; then
    echo -e "\n## $0 making utrain data `hostname` `date` \n"
    flp_data=/data4/kws13/c1/data/train; llp_data=data/llp/train; 
    ilocal/semi/prepare_semi_data.sh $flp_data $llp_data $utrain || exit 1; 
    echo -e "\n## $0 done\n"
  fi
  sdata=$utrain; fea=exp/feature/utrain; dp=data/feature/utrain;
  cmd=run.pl; nj=30;
  if [ $step -le 3 ]; then
    echo -e "\n## $0 plp feature `hostname` `date`\n";
    data=$dp/plp; featdir=$fea/plp;
    mkdir -p $data 2>/dev/null
    cp $sdata/* $data; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
    steps/make_plp.sh --cmd "$cmd" --nj $nj --plp-config conf/plp.conf \
    $data  $featdir/_log $featdir/_data ||exit 1;
    steps/compute_cmvn_stats.sh $data $featdir/_log $featdir/_data ||exit 1;
    utils/fix_data_dir.sh $data
    echo -e "\n## done `date`\n"
  fi 
  if [ $step -le 4 ]; then
    echo -e "\n## $0 fbank feature `hostname` `date`\n"
    data=$dp/fbank; featdir=$fea/fbank;
    mkdir -p $data 2>/dev/null
    cp $sdata/* $data; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
    steps/make_fbank.sh   --cmd "$cmd" --nj $nj --fbank-config conf/fbank.conf \
    $data  $featdir/_log $featdir/_data ||exit 1;
    steps/compute_cmvn_stats.sh $data $featdir/_log $featdir/_data || exit 1
    utils/fix_data_dir.sh $data
    echo -e "\n## done `date`\n"
  fi
  if [ $step -le 5 ]; then
    echo -e "\n## $0 pitch feature `hostname` `date`\n"
    data=$dp/pitch; featdir=$fea/pitch;
    mkdir -p $data 2>/dev/null;
    cp $sdata/* $data; rm $data/{feats.scp,cmvn.scp} 2>/dev/null
    steps/make_pitch_kaldi.sh --cmd "$cmd" --nj $nj --pitch-config conf/plp.conf \
    $data $featdir/_log $featdir/_data || exit 1
    utils/fix_data_dir.sh $data
    echo -e "\n## done `date`\n"
  fi
  if [ $step -le 6 ]; then
    echo -e "\n## making plp+pitch `hostname` `date` \n"
    plp=$dp/plp; pitch=$dp/pitch;
    data=$dp/plp_pitch; featdir=$fea/plp_pitch;
    steps/append_feats.sh $plp $pitch  $data $featdir/_log $featdir/_data || exit 1
    steps/compute_cmvn_stats.sh $data $featdir/_log $featdir/_data ||exit 1;
    echo -e "\n## done `date`\n"
  fi
  if [ $step -le 7 ]; then
    echo -e "\n## making fbank+pitch `hostname` `date` \n"
    fbank=$dp/fbank; pitch=$dp/pitch;
    data=$dp/fbk_pitch; featdir=$fea/fbk_pitch;
    steps/append_feats.sh $fbank $pitch  $data $featdir/_log $featdir/_data || exit 1
    steps/compute_cmvn_stats.sh $data $featdir/_log $featdir/_data ||exit 1;
    echo -e "\n## done `date`\n"
  fi
  bnf=data/feature/utrain/bnf; 
  if [ $step -le 8 ]; then
    echo -e "\n## making bnf feature `hostname` `date`\n"
    final_bn=exp/llp/plp_pitch/bn/llp/s1/bn2;
    sdata=data/feature/utrain/fbk_pitch; data=$bnf; 
    feadir=exp/feature/utrain/bnf;
    isteps/imake_bn_feats.sh --nj 30 $data $sdata $final_bn $feadir/_log $feadir/_data || exit 1
    echo -e "\n## done `date`\n";
  fi
  dev=$dp/tandem;
  if [ $step -le 9 ]; then
    echo -e "\n## $0 making tandem feature `hostname` `date`\n"
    plp=$dp/plp; fea=exp/feature/utrain/tandem;
    tandem="$plp $bnf"; data=$dp/tandem; 
    isteps/tandem/make_fmllr_feats.sh  $data $tandem dummy $fea/_log $fea/_data ||exit 1;
    steps/compute_cmvn_stats.sh $data $fea/_log $fea/_data || exit 1; 
    echo -e "\n## done `date`\n"
  fi
  dec_nj=40; cmd=run.pl; sdir=exp/llp/tandem/tri3sat_2500;
  graphdir=$sdir/graph;  dec_dir=$sdir/decode_utrain;
  if [ $step -le 10 ]; then
    echo -e "\n## $0 decoding `hostname` `date`\n"
    isteps/decode_fmllr2.sh --nj $dec_nj  --delta-order 0 \
    $graphdir $dev  $dec_dir || exit 1;
    echo -e "\n## done `date`\n"
  fi
  inv_acwt=20; nonspeech_words=$lang/nonspeech_words.int;
  mdl=exp/llp/tandem/tri3sat_2500; latdir=$mdl/decode_utrain;
  wpdir=$latdir/latfb/word_post; ucdir=$latdir/latfb/utt_conf;
  if [ $step -le 11 ]; then
    echo -e "\n## $0 confidence score `hostname` `date`\n"
    silence_opts="--silence-label=`cat $nonspeech_words|grep \!SIL |awk '{print $2;}'`"
    isteps/um/utt_conf.sh --inv-ascale $inv_acwt  --silence-opts "$silence_opts" --non-speech $nonspeech_words \
      $latdir $lang $mdl $wpdir $ucdir || exit 1
    echo -e "\n## done `date`\n"
  fi
  cutoff=0.4; uttconf_list="gzip -cdf $ucdir/uconf.*.gz|";
  src_segments=data/feature/utrain/plp/segments; dir=$latdir/latfb/cutoff$cutoff
  dir_booked=$dir;
  if [ $step -le 12 ]; then
    echo -e "\n## select unsupervised data `hostname` `date`\n"
    mkdir -p $dir 2>/dev/null
    iutils/um/conf_utt_select.pl --thresh $cutoff \
    --selected $dir/segments "$uttconf_list" $src_segments  \
        >$dir/uttconfs 2>> $dir/log
    echo -e "\n## done `date`\n"
  fi
  sr_trans_int=exp/llp/tandem/tri3sat_2500/decode_utrain/scoring/$inv_acwt.tra;
  unsup_txt=$dir_booked/text;
  if [ $step -le 13 ]; then
    echo -e "\n## make unsup data `hostname` `date`\n"
    echo -e "\nasr_trans_int=$sr_trans_int\n"
    nonspeech_words=$lang/nonspeech_words.txt;
    cat $sr_trans_int |utils/int2sym.pl -f 2- $lang/words.txt | \
    iutils/filter_utt.pl $nonspeech_words  2>$dir_booked/filter_text.log  | \
    perl -e ' ($seg) = @ARGV; 
      open SEG, "<$seg";  
      while (<SEG>) {
        m/(\S+)\s+(.*)/;
        $vocab{$1} = 0;
      }
      while (<STDIN>) {
        m/(\S+)\s+(.*)/;
        next if not exists $vocab{$1};
        print "$_";
      } 
      close SEG;'  $dir_booked/segments > $unsup_txt ;
    filtered=`wc -l <$dir_booked/filter_text.log`
    echo -e "\n##filtered=$filtered in folder $dir_booked\n" && exit 0 ;
    echo -e "\n## done `date`";
  fi
  unsup_dir=$dir_booked/unsup; 
  semidir=data/feature/semi/cutoff$cutoff/fbk_pitch;
  if [ $step -le 14 ]; then
    echo -e "\n## making unsupervised fb_pitch data `hostname` `date`\n"
    sdir=data/feature/utrain/fbk_pitch; 
    echo -e "\n## get fb_pitch unsupervised part with $dir_booked/text \n"
    isteps/um/make_udata_dir.sh $dir_booked/text $sdir $unsup_dir || exit 1
    sdir=data/llp/feature/fbk_pitch/sbn/ntrain;
    echo -e "\n## merge with $sdir\n"
    iutils/um/merge_data_dir.sh $unsup_dir $sdir $semidir || exit 1

    echo -e "\n## $0 NO unsupervised data is selected for fb_pitch data \n"
    (cd $semidir; rm  ./*);
    cp -r $sdir/*   $semidir  

    # redo cmvn
    rm $semidir/cmvn.scp 2>/dev/null
    echo -e "\n## redo cmvn \n"
    steps/compute_cmvn_stats.sh $semidir $semidir/_log $semidir/_cmvn || exit 1
    echo -e "\n## done `date`\n";
  fi
  ali_semidir=data/feature/semi/cutoff$cutoff/bnf;
  if [ $step -le 15 ]; then
    echo -e "\n## prepare semi bnf to make alignment `hostname` `date` \n"
    usrc=data/feature/utrain/bnf; ssrc=data/llp/feature/bnllp/train;
    seltxt=data/feature/semi/cutoff$cutoff/fbk_pitch/text;
    for sdata in $usrc $ssrc; do
      dir=$sdata/.sel ; rm $dir/* 2>/dev/null
      iutils/um/subset_data_dir.sh --uttlist $seltxt $sdata $dir || exit 1
    done

    dir=$ali_semidir;
    # iutils/um/merge_data_dir.sh $usrc/.sel $ssrc/.sel  $dir/.semitemp || exit 1
    # isteps/um/make_udata_dir.sh $seltxt $dir/.semitemp $dir/ntrain || exit 1
    rm -rf $dir/.semitemp 2>/dev/null
    
    echo -e "\n## $0 No unsupervised data is selected\n"
    (mkdir -p $dir/ntrain 2>/dev/null; cd $dir/ntrain; rm ./*)
    text=data/llp/feature/fbk_pitch/sbn/ntrain/text;
    isteps/um/make_udata_dir.sh $text  $ssrc $dir/ntrain || exit 1
    
    echo -e "\n## redo cmvn for semi-bnf data \n"
    dir=$ali_semidir/ntrain;  feat_prefix=exp/feature/semi/cutoff$cutoff/bnf;
    rm $dir/cmvn.scp 2>/dev/null; fea=$feat_prefix/ntrain;
    steps/compute_cmvn_stats.sh $dir $fea/_log $fea/_cmvn || exit 1
    echo -e "\n## making cross validation data\n"
    seltxt=data/llp/feature/fbk_pitch/sbn/nvali/text;
    sdata=$ssrc; data=$ali_semidir/nvali; fea=$feat_prefix/nvali;
    iutils/um/subset_data_dir.sh --uttlist $seltxt $sdata $data || exit 1
    rm $data/cmvn.scp 2>/dev/null;
    steps/compute_cmvn_stats.sh $data $fea/_log $fea/_cmvn || exit 1
    echo -e "\n## done\n"
  fi
  alidir=exp/llp/plp_pitch/mono0a/semi/cutoff$cutoff/bnf;
  if [ $step -le 16 ]; then
    echo -e "\n## making alignment `hostname` `date` \n"
    smdl=exp/llp/plp_pitch/bn/llp/s1/tri3sat_2500; ap=$smdl/semi/cutoff$cutoff/bnf;
    rm -rf $ap 2>/dev/null
    mdl=exp/llp/plp_pitch/mono0a
    for x in ntrain nvali; do
      data=$ali_semidir/$x; ali=$ap/$x;
      steps/align_fmllr.sh --nj 10  $data $lang $smdl $ali || exit 1
      echo -e "\n## convert alignment `date`\n"
      sali=$ali; ali=$alidir/$x;
      isteps/convert_ali.sh  $smdl $mdl $sali $ali || exit 1
    done   
    echo -e "\n## done `date`\n"
  fi

  bndir=exp/semi/cutoff$cutoff;
  if [ $step -le 17 ]; then
    echo -e "\n## train sbn `hostname` `date`\n"
    train=data/feature/semi/cutoff$cutoff/fbk_pitch; vali=data/llp/feature/fbk_pitch/sbn/nvali;
    alip=exp/llp/plp_pitch/mono0a/semi/cutoff$cutoff/bnf;
    tali=$alip/ntrain; cali=$alip/nvali;
    isteps/train_sbn.sh  $train $vali $tali $cali $lang $bndir || exit 1
    echo -e "\ndone `date`\n"
  fi
  sdp=data/llp/feature/fbk_pitch; tdp=data/feature/semi/cutoff$cutoff/bn;
  tfp=exp/feature/semi/cutoff$cutoff/bn;
  if [ $step -le 18 ]; then
    echo -e "\n## $0 make bn feature`hostname` `date`\n"
    for x in train dev10h; do
      sdata=$sdp/$x; data=$tdp/$x; fea=$tfp/$x;
      isteps/imake_bn_feats.sh --nj 10 $data $sdata $bndir/bn2 $fea/_log $fea/_data || exit 1
    done
    echo -e "\n## done `date`\n"
  fi
  train=$tdp/train; dev=$tdp/dev10h; sdir=exp/semi/cutoff$cutoff/bn;
  if [ $step -le 19 ]; then
    echo -e "\n## $0 train first bn gmm-hmm system `hostname` `date`\n"
    isteps/train_am_simple.sh --max-stage 6 --over-train $train --over-dev $dev --over-lang $lang --over-lang-test $lang_test \
      --over-expdir $sdir $bn_am_cf || exit 1   
    echo -e "\n## $0 done`date`\n"
  fi
  echo -e "\n## prepare_data done `hostname` `date`\n"; exit 0
fi


array_cutoff=("0.4 0.5" "0.5 0.6" "0.6 0.7" "0.7 0.8" "0.8 1.5");
inv_acwt=20; nonspeech_words=$lang/nonspeech_words.int;
mdl=exp/llp/tandem/tri3sat_2500; latdir=$mdl/decode_utrain;
wpdir=$latdir/latfb/word_post; ucdir=$latdir/latfb/utt_conf;
uttconf_list="gzip -cdf $ucdir/uconf.*.gz|";
for cIndex in `seq 0 4`; do
  cutoffs=${array_cutoff[$cIndex]}
  locut=`echo "$cutoffs"|awk '{print $1;}'`;
  hicut=`echo "$cutoffs"| awk '{print $2;}'`;
  id=cutoff_${locut}_${hicut};
  echo -e  "\nlocut=$locut, hicut=$hicut\n";
  src_segments=data/feature/utrain/plp/segments; dir=$latdir/latfb/$id
  dir_booked=$dir;
  if [ $step -le 12 ]; then
    echo -e "\n## select unsupervised data `hostname` `date`\n"
    mkdir -p $dir 2>/dev/null
    iutils/um/conf_utt_select.pl --thresh $locut --hithresh $hicut \
    --selected $dir/segments "$uttconf_list" $src_segments  \
        >$dir/uttconfs 2>> $dir/log
    cat $dir/segments | awk '{x+=$4-$3;}END{print "hour=",x/3600;}'
    echo -e "\n## done in $dir `date`\n"
  fi
  
  sr_trans_int=exp/llp/tandem/tri3sat_2500/decode_utrain/scoring/$inv_acwt.tra;
  unsup_txt=$dir_booked/text;
  if [ $step -le 13 ]; then
    nonspeech_words=$lang/nonspeech_words.txt;
    cat $sr_trans_int |utils/int2sym.pl -f 2- $lang/words.txt | \
    iutils/filter_utt.pl $nonspeech_words  2>$dir_booked/filter_text.log  | \
    perl -e ' ($seg) = @ARGV; 
      open SEG, "<$seg";  
      while (<SEG>) {
        m/(\S+)\s+(.*)/;
        $vocab{$1} = 0;
      }
      while (<STDIN>) {
        m/(\S+)\s+(.*)/;
        next if not exists $vocab{$1};
        print "$_";
      } 
      close SEG;'  $dir_booked/segments > $unsup_txt ;
    filtered=`wc -l <$dir_booked/filter_text.log`
  fi
  unsup_dir=$dir_booked/unsup; 
  semidir=data/feature/semi/$id/fbk_pitch;
  if [ $step -le 14 ]; then
    echo -e "\n## making unsupervised fb_pitch data `hostname` `date`\n"
    sdir=data/feature/utrain/fbk_pitch; 
    echo -e "\n## get fb_pitch unsupervised part with $dir_booked/text \n"
    rm $unsup_dir/* 2>/dev/null
    isteps/um/make_udata_dir.sh $dir_booked/text $sdir $unsup_dir || exit 1
    sdir=data/llp/feature/fbk_pitch/sbn/ntrain;
    echo -e "\n## merge with $sdir\n"
    iutils/um/merge_data_dir.sh $unsup_dir $sdir $semidir || exit 1

    # redo cmvn
    rm $semidir/cmvn.scp 2>/dev/null
    echo -e "\n## redo cmvn \n"
    steps/compute_cmvn_stats.sh $semidir $semidir/_log $semidir/_cmvn || exit 1
    echo -e "\n## done `date`\n";
  fi
  ali_semidir=data/feature/semi/$id/bnf;
  if [ $step -le 15 ]; then
    echo -e "\n## prepare semi bnf to make alignment `hostname` `date` \n"
    usrc=data/feature/utrain/bnf; ssrc=data/llp/feature/bnllp/train;
    seltxt=data/feature/semi/$id/fbk_pitch/text;
    for sdata in $usrc $ssrc; do
      dir=$sdata/.sel ; rm $dir/* 2>/dev/null
      iutils/um/subset_data_dir.sh --uttlist $seltxt $sdata $dir || exit 1
    done

    dir=$ali_semidir;
    iutils/um/merge_data_dir.sh $usrc/.sel $ssrc/.sel  $dir/.semitemp || exit 1
    isteps/um/make_udata_dir.sh $seltxt $dir/.semitemp $dir/ntrain || exit 1
    rm -rf $dir/.semitemp 2>/dev/null
    
    echo -e "\n## redo cmvn for semi-bnf data \n"
    dir=$ali_semidir/ntrain;  feat_prefix=exp/feature/semi/$id/bnf;
    rm $dir/cmvn.scp 2>/dev/null; fea=$feat_prefix/ntrain;
    steps/compute_cmvn_stats.sh $dir $fea/_log $fea/_cmvn || exit 1
    echo -e "\n## making cross validation data\n"
    seltxt=data/llp/feature/fbk_pitch/sbn/nvali/text;
    sdata=$ssrc; data=$ali_semidir/nvali; fea=$feat_prefix/nvali;
    iutils/um/subset_data_dir.sh --uttlist $seltxt $sdata $data || exit 1
    rm $data/cmvn.scp 2>/dev/null;
    steps/compute_cmvn_stats.sh $data $fea/_log $fea/_cmvn || exit 1
    echo -e "\n## done\n"
  fi
  alidir=exp/llp/plp_pitch/mono0a/semi/$id/bnf;
  if [ $step -le 16 ]; then
    echo -e "\n## making alignment `hostname` `date` \n"
    smdl=exp/llp/plp_pitch/bn/llp/s1/tri3sat_2500; ap=$smdl/semi/$id/bnf;
    rm -rf $ap 2>/dev/null
    mdl=exp/llp/plp_pitch/mono0a
    for x in ntrain nvali; do
      data=$ali_semidir/$x; ali=$ap/$x;
      steps/align_fmllr.sh --nj 10  $data $lang $smdl $ali || exit 1
      echo -e "\n## convert alignment `date`\n"
      sali=$ali; ali=$alidir/$x;
      isteps/convert_ali.sh  $smdl $mdl $sali $ali || exit 1
    done   
    echo -e "\n## done `date`\n"
  fi
  
  bndir=exp/semi/$id;
  if [ $step -le 17 ]; then
    echo -e "\n## train sbn `hostname` `date`\n"
    train=data/feature/semi/$id/fbk_pitch; vali=data/llp/feature/fbk_pitch/sbn/nvali;
    alip=exp/llp/plp_pitch/mono0a/semi/$id/bnf;
    tali=$alip/ntrain; cali=$alip/nvali;
    isteps/train_sbn.sh  $train $vali $tali $cali $lang $bndir || exit 1
    echo -e "\ndone `date`\n"
  fi
  sdp=data/llp/feature/fbk_pitch; tdp=data/feature/semi/$id/bn;
  tfp=exp/feature/semi/$id/bn;
  if [ $step -le 18 ]; then
    echo -e "\n## $0 make bn feature`hostname` `date`\n"
    for x in train dev10h; do
      sdata=$sdp/$x; data=$tdp/$x; fea=$tfp/$x;
      isteps/imake_bn_feats.sh --nj 10 $data $sdata $bndir/bn2 $fea/_log $fea/_data || exit 1
    done
    echo -e "\n## done `date`\n"
  fi
  train=$tdp/train; dev=$tdp/dev10h; sdir=exp/semi/$id/bn;
  if [ $step -le 19 ]; then
    echo -e "\n## $0 train first bn gmm-hmm system `hostname` `date`\n"
    isteps/train_am_simple.sh --max-stage 6 --over-train $train --over-dev $dev --over-lang $lang --over-lang-test $lang_test \
      --over-expdir $sdir $bn_am_cf || exit 1   
    echo -e "\n## $0 done`date`\n"
  fi
  echo -e "\n## done `date`\n"
done
exit 0;

