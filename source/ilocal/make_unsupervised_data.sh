#!/bin/bash

# Begin configuration section.
cmd=run.pl
lmwt=10
word_lattice=true
max_expand=20
utt_conf_cutoff=0
# End configuration section.

echo "$0 $@" 
. path.sh
. parse_options.sh
if [ $# != 4 ]; then
  echo "Usage: $0 [options] <lang|graph-dir> <data-dir>  <decode-dir> <dir>"
  echo "e.g.: $0 exp/tri4a/graph data/unlabeled exp/tri4a/decode_unlabeled data/asr_labeled"
  exit 1
fi

lang=$1
data=$2
decode_dir=$3
dir=$4

symtab=$lang/words.txt
silence_words=$lang/silence_words.int
for f in $symtab $decode_dir/lat.1.gz ; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1
done
if [ ! -f $decode_dir/$lmwt.txt ]; then
  lattice-best-path --lm-scale=$lmwt --word-symbol-table=$symtab \
  "ark:gunzip -c $decode_dir/lat.*.gz|" \
  "ark,t:| ilocal/filter_utt.pl $silence_words|utils/int2sym.pl -f 2- $symtab > $decode_dir/$lmwt.txt"  2>$decode_dir/$lmwt.log  || exit 1;
fi
awk '{print $1}' $decode_dir/$lmwt.txt > $decode_dir/uttlist.txt
uttlist=$decode_dir/uttlist.txt
if [ $(echo $utt_conf_cutoff '>' 0 | bc -l) -gt 0 ] ; then
  nj=$(cat $decode_dir/num_jobs)
  sdir=$(dirname $decode_dir)
  model=$sdir/final.mdl
  word_boundary=$lang/phones/word_boundary.int
  for f in $model $silence_words; do
    [ ! -f $f ] && echo "no such file $f " && exit 1     
  done
  if $word_lattice; then
    f=$word_boundary
    [ ! -f $f ] && echo "no such file $f " && exit 1     
  fi

  silence_opts=`cat $lang/silence_opts` || exit 1
  uttconfdir=$decode_dir/uttconf 
  if [ ! -f $uttconfdir/.done ]; then
    if $word_lattice; then
      $cmd JOB=1:$nj $uttconfdir/log/uconf.JOB.log \
      lattice-align-words "$silence_opts" --max-expand=$max_expand \
      $word_boundary $model  "ark:gzip -cdf $decode_dir/lat.JOB.gz|" ark:- \| \
      lattice-to-word-post --inv-acoustic-scale=$lmwt ark:-  \
      "|ilocal/uttconf.pl $silence_words  \"|gzip -c > $uttconfdir/uconf.JOB.gz\""  || exit 1
    else
      $cmd JOB=1:$nj $uttconfdir/log/uconf.JOB.log \
       lattice-to-word-post --inv-acoustic-scale=$lmwt "ark:gzip -cdf $decode_dir/lat.JOB.gz|"  \
      "|ilocal/uttconf.pl $silence_words  \"|gzip -c > $uttconfdir/uconf.JOB.gz\""  || exit 1
    fi
    touch $uttconfdir/.done
  fi
  gzip -cd $uttconfdir/uconf.*.gz | \
  awk -v cutoff=$utt_conf_cutoff '{if($2>=cutoff){print $1; } }'|sort -u > $uttconfdir/uttlist.txt
  uttlist=$uttconfdir/uttlist.txt
fi
uttnum=`wc -l <$uttlist`
[ -z $uttnum ] && uttnum=0
echo "$0: utterances($uttnum) are selected"
if [[  $uttnum  -gt 2000 ]]; then
  ilocal/subset_data_dir.sh --uttlist $uttlist $data $uttnum $dir || exit 1 
  rm $dir/text
  cp $decode_dir/$lmwt.txt $dir/text
  fix_data_dir.sh $dir 
else
  echo "$0: no utterance selected";
  exit 1
fi
