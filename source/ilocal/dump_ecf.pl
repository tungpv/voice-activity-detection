#!/usr/bin/env perl
use strict;
use warnings;

use Getopt::Long;
use XML::Simple;
use Data::Dumper;

my $numArgs = scalar @ARGV;
if ($numArgs != 1) {
  die "\n\nUsage: $0 <ecf.xml>\n\n";
}
my ($ecf_fname) = @ARGV;
my $data = XMLin($ecf_fname);
my $excerpts = $data->{excerpt};
for(my $i =0; $i < @$excerpts; $i ++) {
  my $hRef = $$excerpts[$i];
  my $audio_filename=$hRef->{audio_filename};
  $audio_filename =~ s/.*\///g; $audio_filename =~ s/\.wav//g;
  print "$audio_filename $hRef->{tbeg} $hRef->{dur}\n";
}
