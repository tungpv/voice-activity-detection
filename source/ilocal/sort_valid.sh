#!/bin/bash

 grep LOG -A 1 ./exp/nnet6b/log/compute_prob_valid.*.log | grep -v LOG | awk -F '--' '{gsub(/.*_valid\./,"",$1); gsub(/\.log/,"",$1); print $1, $2; }' | sort -k1,2  -n

