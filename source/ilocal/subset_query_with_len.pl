#!/usr/bin/env perl
use strict;
use warnings;

my $numArgs = scalar @ARGV;
if ($numArgs != 2) {
  die "\n\nUsage: $0 <query_length.txt> <range-string>\n\n";
}
my ($qlen_fname, $range_str) = @ARGV;

sub GetRange {
  my ($parray, $range_str) = @_;
  $range_str =~ s/\s//g;
  if ($range_str =~ m/^-(\S+)/) {
    push (@$parray, -1.0e10);
    push (@$parray, $1);
  } elsif ($range_str =~ m/(\S+)-$/) {
    push (@$parray, $1);
    push (@$parray, 1.0e10);
  }elsif ($range_str =~ m/(\S+)-(\S+)/) {
    push (@$parray, $1);
    push (@$parray, $2);
  } else {
    die "unparsable string $range_str\n";
  }
}

my @rangeA;
GetRange(\@rangeA, $range_str);

open (F, "$qlen_fname") or die "file $qlen_fname cannot open\n";
while (<F>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  my ($kwid, $alen) = ($1, $2);
  if ($alen < $rangeA[1] && $alen >= $rangeA[0]) {
    print "$kwid $alen\n";
  }
}
close F;

