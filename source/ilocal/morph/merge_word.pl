#!/usr/bin/env perl
use warnings;
use strict;

my ($lfactor, $from, $lex, $text) = @ARGV;

my %vocab = ();
open( L, "<$lex" ) or die "lexicon $lex cannot open\n";
while(<L>) {
  chomp;
  print $_, "\n";
  m/(\S+)\s+(.*)/ or next;
  my $w = $1;
  if(exists $vocab{$w}) {
    $vocab{$w} += $lfactor;
  } else {
    $vocab{$w} = $lfactor;
  }
}
close L;

open T, "$text" or die "text file $text cannot open";
while (<T>) {
  chomp;
  my @A = split / /;
  # print join " ", @A, "\n";
  for(my $i=$from -1; $i < scalar @A; $i++) {
    my $w = $A[$i];
    if(exists $vocab{$w}) {
      $vocab{$w} ++;
    } else {
      $vocab{$w} = 1;
    }
  }
}
close T;
