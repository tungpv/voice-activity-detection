#!/bin/bash

# ilocal/morph/text_segment.sh --from 2 model text dir
# ilocal/morph/text_segment.sh --from 2 test/morph/morph.mdl data/train/text test/segtext 

echo -e "\n## $0 $@\n"

function die {
  echo -e "\n## ERROR: $1\n";
  exit 1
}

. path.sh


# --
from=1;  # from where to begin
nbest=1;
# --

. utils/parse_options.sh

if [ $# -ne 5 ]; then
  echo -e "\n\nUsing training lexicon and text to"
  echo -e "make morph lexicon. Usage:\n\n"
  echo -e "$0 [options] morph_model g2p_model lex text dir\n\n"
 exit 1
fi

mmdl=$1; shift;
gmdl=$1; shift;
lex=$1; shift
text=$1; shift
dir=$1; shift;

for x in $mmdl $gmdl $lex $text; do
  [ -e $x ] || die "file $x is not there";
done

sdir=$dir;
dir=$dir/g2p;

mkdir -p $dir 2>/dev/null

[ -d $dir ] || die "making folder $dir is failed";

echo -e "\n## create word list from text \n";

wlist=$dir/wlist.txt; 
if [ ! -f $wlist ] ; then
  cut -f 1 $lex | \
  cat - <(cat $text | awk -v from=$from '{for(i=from; i<=NF; i++){print $i;}}') | \
  grep -v "<" | sort -u > $wlist
fi

wlists=`wc -l < $wlist`;
if [[ $wlists -eq 0 ]]; then
  die "$wlist is empty";
fi
echo -e "\nwlists=$wlists\n"

if [ ! -f $dir/lexicon.txt ] ;then
  ilocal/g2p/transcribe_wordlist.sh --nbest $nbest $mmdl \
  $gmdl  $wlist $dir || die "transcribe_wordlist.sh failed"
fi
[[ -f $dir/lexicon.txt  && -f $dir/noprons.txt  ]] || \
die "lexicon.txt and noprons.txt expected in $dir folder";
    
cut -f 2- $dir/lexicon.txt | \
awk '{for(i=1;i<=NF;i++){print $i;}}' | sort -u > $sdir/nonsilence_phones.txt
     
grep '<' $lex | \
grep -v 'hes' | cut -f 2- > $sdir/silence_phones.txt

grep 'SIL' $lex | cut -f 2- > $sdir/optional_silence.txt

cat $sdir/silence_phones.txt | \
awk '{printf "%s ",$1;}END{printf "\n";}' >$sdir/extra_questions.txt
cat $sdir/nonsilence_phones.txt | \
awk '{printf "%s ",$1;}END{printf "\n";}' >> $sdir/extra_questions.txt
     
cut -f 1 <( perl -ne 'if(/^[\`\-\+]\t/){print $_;}' $dir/lexicon.txt ) | \
cat - $dir/noprons.txt > $sdir/noprons.txt

grep '<' $lex | \
cat - <(perl -ne 'if(!/^[\`\-\+]\t/) {print $_; }' $dir/lexicon.txt ) \
| sort -u > $sdir/lexicon.txt

echo -e "\n## check lexicon file $sdir/lexicon.txt\n"


