#!/bin/bash


echo -e "\n## $0 $@\n"

function die {
  echo -e "\n## ERROR: $1\n";
  exit 1
}

# ilocal/morph/train_morph.sh --lfactor 5 --from 2 data/local/lexicon.txt  data/train/text  test/morph
. path.sh



# --
lfactor=1;   # lexicon scaling factor
from=1;  # from where to begin
allone=false
# --

. utils/parse_options.sh

if [ $# -ne 3 ]; then
  echo -e "\n\nUsing morfessor to train morph model"
  echo -e "\nUsage: $0 [options] <lexicon> <kaldi_text> <dir>"
  echo "--lfactor # lexicon scaling factor"
  echo -e "--from # from where to begin \n\n"
  exit 1
fi

lex=$1; shift;
text=$1; shift;
dir=$1; shift;

for x in $lex $text ;do
  [ -f $x ] || die "file $x is not there";
done
[ $lfactor -gt 0 ] || die "lfactor should be over zero";
sdir=$dir;
dir=$dir/mtemp;
mkdir -p $dir 2>/dev/null
[ -d $dir ] || die "making folder $dir is failed";

cat $lex | \
perl -e '
  ($lfactor, $from, $text) = @ARGV;
  %vocab = ();
  while (<STDIN>) {
    chomp;
    s/\t/ /g;
    m/(\S+)\s+(.*)/ or next;
    $w = $1;
    if (exists $vocab{$w}) {
      $vocab{$w} += $lfactor;
    } else {
      $vocab{$w} = $lfactor;
    }
  }
  open(F, "<$text" ) or die "file $text cannot open\n";
  while (<F>) {
    chomp;
    @A = split / /;
    for ($i =$from -1; $i < scalar @A; $i ++) {
      $w = $A[$i];
      if (exists $vocab{$w}) {
        $vocab{$w} ++;
      } else {
        $vocab{$w} = 1;
      }
    }
  }
  close F;
  foreach $w (keys %vocab) {
    print "$vocab{$w}\t$w\n";
  }
' $lfactor $from $text | \
grep -v '<' |sort -k1n > $dir/train.txt 
if $allone; then
  cat $dir/train.txt | awk '{print 1,"\t", $2;}' > $dir/.train.txt
  cp $dir/.train.txt $dir/allone_train.txt
else 
  mv $dir/train.txt $dir/.train.txt 
fi

cat $dir/.train.txt | \
ilocal/morph/remove_dash.pl  > $dir/train.txt 
morfessor-train -s $sdir/morph.mdl $dir/train.txt \
--logfile=$dir/morfessor-train.log

[ -e $sdir/morph.mdl ] || die "morfessor-train failed";

echo -e "\n## check $sdir/morph.mdl\n"

echo -e "\n\n";
