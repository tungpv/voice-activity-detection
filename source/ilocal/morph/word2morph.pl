#!/usr/bin/env perl
use strict;
use warnings;

my($dict) = @ARGV;
my %vocab = ();

open D, "<$dict"  or die "file $dict cannot open\n";
while (<D>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $vocab{$1} = $2;
}
close D;

while (<STDIN>) {
  chomp;
  my @A = split / /;
  my $s = "";
  for(my $i = 0;  $i < scalar @A; $i ++ ) {
    my $w = $A[$i];
    if (exists $vocab{$w}) {
      $s .= "$vocab{$w} ";
    } else {
      $s .= "$w ";
    }
  }
  $s =~ s/ $//g;
  if (length($s) > 0) {
    print "$s\n";
  }
}
