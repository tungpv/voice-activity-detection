#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl;
nj=10;
step=0
substep=0
prepare_data=false
lang=data/llp/lang; lang_test=${lang}_test
samples_per_iter=4000000;  
max_mem=500000000;
state=2500;gauss=20000
pnorm_input_dim=3000; pnorm_output_dim=500; 
nonspeech_words=$lang/nonspeech_words.txt

# end tunable variable
bn_am_cf=conf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";



if $prepare_data; then
  dir=data/llp/local/morph
  if [ $step -le 1 ]; then
    cat /data4/wsj_dan2/s5/data/test_dev93/text | \
    cut -d " " -f 2-  > $dir/train.txt
  fi
  if [ $step -le 3 ]; then
    echo -e "\n## morph training `date`\n"
    morfessor-train -s $dir/morph.mdl $dir/train.txt --logfile=$dir/train.log
    echo -e "\n## morph ended `date`\n"
  fi
  if [ $step -le 5 ]; then
    echo -e "\n## prepare test data\n"
    cat /data4/wsj_dan2/s5/data/train/text | \
    awk '{for(i=1;i<=NF;i++){if (length($i) > 9)print $i; } }' | sort -u > $dir/test.txt  
  fi
  if [ $step -le 7 ]; then
    morfessor-segment -l $dir/morph.mdl -o $dir/morph_segment.txt $dir/test.txt 
  fi
  if [ $step -le 9 ]; then
    cat $dir/test.txt | awk '{printf "%s ",$1;}' > $dir/test_line.txt
  fi
  if [ $step -le 10 ]; then
    echo -e "\n## segmentation started `date`\n"
    morfessor-segment -l $dir/morph.mdl -o $dir/morph_segment_line.txt $dir/test_line.txt 
    echo -e "\ndone `date`\n"
  fi
  echo -e "\n## prepare_data ended `date`\n"
fi
