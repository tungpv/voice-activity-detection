#!/usr/bin/env perl
use strict;
use warnings;

print STDERR "stdin expected\n";
my %vocab = ();
while (<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)/ or next;
  my ($count, $word) = ($1, $2);
  my @A = split /_/, $word;
  for(my $i = 0; $i < scalar @A; $i++) {
    my $w = $A[$i];
    if(not exists $vocab{$w}) {
      $vocab{$w} = $count;
    } else {
      $vocab{$w} += $count;
    }
  }
}

foreach my $w (keys %vocab) {
  print $vocab{$w}, "\t", $w, "\n";
}

print STDERR "stdin ended\n";
