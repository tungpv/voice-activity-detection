#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "path.sh expected";
. g2p_path.sh || die "g2p_path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl;
nj=10;
step=0
substep=0
prepare_data=false
g2p=false
morph=false
align4lex=false
lang=data/llp/lang; lang_test=${lang}_test
samples_per_iter=4000000;  
max_mem=500000000;
state=2500;gauss=20000
pnorm_input_dim=3000; pnorm_output_dim=500; 
nonspeech_words=$lang/nonspeech_words.txt

# end tunable variable
bn_am_cf=conf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";



if $prepare_data; then
  dir=data/llp/local/morph
  if [ $step -le 1 ]; then
    cat /data4/wsj_dan2/s5/data/test_dev93/text | \
    cut -d " " -f 2-  > $dir/train.txt
  fi
  if [ $step -le 3 ]; then
    echo -e "\n## morph training `date`\n"
    morfessor-train -s $dir/morph.mdl $dir/train.txt --logfile=$dir/train.log
    echo -e "\n## morph ended `date`\n"
  fi
  if [ $step -le 5 ]; then
    echo -e "\n## prepare test data\n"
    cat /data4/wsj_dan2/s5/data/train/text | \
    awk '{for(i=1;i<=NF;i++){if (length($i) > 9)print $i; } }' | sort -u > $dir/test.txt  
  fi
  if [ $step -le 7 ]; then
    morfessor-segment -l $dir/morph.mdl -o $dir/morph_segment.txt $dir/test.txt 
  fi
  if [ $step -le 9 ]; then
    cat $dir/test.txt | awk '{printf "%s ",$1;}' > $dir/test_line.txt
  fi
  if [ $step -le 10 ]; then
    echo -e "\n## segmentation started `date`\n"
    morfessor-segment -l $dir/morph.mdl -o $dir/morph_segment_line.txt $dir/test_line.txt 
    echo -e "\ndone `date`\n"
  fi
  echo -e "\n## prepare_data ended `date`\n"
fi

if $g2p; then
  sdict=/data4/wsj_dan2/s5/data/local/dict/lexicon.txt
  sdir=data/llp/local/morph
  if [ $step -le 1 ]; then
    cat $sdict | \
    perl -e '$txt = shift @ARGV; open TXT, "<$txt"; 
           while(<TXT>){ chomp; @a = split /\s+/;
             for ($i = 0; $i < scalar @a; $i++) { $w = $a[$i]; $vocab{$w} = 0; }
           }  close TXT; 
           while (<STDIN>) {
             chomp; s/\t/ /g; m/(\S+)\s+(.*)/; ($w, $pstr) = ($1, $2);
             print "$w\t$pstr\n" if (exists $vocab{$w});
           } '  $sdir/train.txt > $sdir/train.lex
  fi
  
  if [ $step -le 5 ]; then
    dir=$sdir
    phonetisaurus-align  --seq2_del=false --input=$dir/train.lex --ofile=$dir/train_lex.corpus
  fi
  
  if [ $step -le -10 ]; then
    dir=$sdir;
    ngram-count -order 7 -kn-modify-counts-at-end -gt1min 0 -gt2min 0\
    -gt3min 0 -gt4min 0 -gt5min 0 -gt6min 0 -gt7min 0 -ukndiscount \
    -ukndiscount1 -ukndiscount2 -ukndiscount3 -ukndiscount4 \
    -ukndiscount5 -ukndiscount6 -ukndiscount7 -text $dir/train_lex.corpus \
    -lm $dir/train_lex.arpa
  fi
  if [ $step -le 10 ]; then
    dir=$sdir;
    ngram-count -order 3 -text $dir/train_lex.corpus -lm $dir/train_lex.arpa2
  fi
  if [ $step -le 15 ]; then
    dir=$sdir;
    phonetisaurus-arpa2fst --input=$dir/train_lex.arpa2 --prefix="$dir/train_lex"
  fi

  if [ $step -le 20 ]; then
    dir=$sdir
    phonetisaurus-g2p --model=$dir/train_lex.fst --input=$dir/test.txt --isfile --words \
    > $dir/test_initial.lex 2>$dir/phonetisaurus-g2p.log 
    echo -e "\n## check log $dir/phonetisaurus-g2p.log\n"
    cat $dir/test_initial.lex | \
    awk '{$2=""; print $0}' | \
    awk '{printf("%s\t", $1); for(i=2;i<NF;i++){printf("%s ", $i)}; printf("\n"); }' \
    > $dir/test.lex
    echo -e "\n## check transcribed lexicon $dir/test.lex\n"
  fi
  
##
fi

if $morph; then

  sdir=data/llp/local/morph
  if [ $step -le 1 ]; then
    dir=$sdir;
    morfessor-train -s $dir/morph.mdl $dir/train.txt --logfile=$dir/morfessor-train.log
  fi
  if [ $step -le 5 ]; then
    dir=$sdir
    cat $dir/train.txt | \
    awk '{for(i=1; i<=NF; i++){ print $i; }}' | \
    sort -u > $dir/train_wordlist.txt
    echo -e "\n## check $dir/train_wordlist.txt\n"
  fi
  if [ $step -le 15 ]; then
    dir=$sdir
    morfessor-segment -l $dir/morph.mdl -o $dir/train_morph_segment.txt $dir/train_wordlist.txt 
    echo -e "\n## check $dir/train_morph_segment.txt\n"
  fi
  if [ $step -le 20 ]; then
    dir=$sdir
    n1=`wc -l < $dir/train_wordlist.txt`; n2=`wc -l < $dir/train_morph_segment.txt`
    [ $n1 -eq $n2 ] || die "$n1 <> $n2"
    paste $dir/train_wordlist.txt $dir/train_morph_segment.txt > $dir/morph_lex.txt
    echo -e "\nmorph_lex.txt file $dir/morph_lex.txt generated \n"
  fi
  if [ $step -le 25 ]; then
    dir=$sdir
    cat $dir/morph_lex.txt | \
    awk '{for(i=2; i<=NF; i++){print $i;}}' | sort -u > $dir/morph_wordlist.txt
    echo -e "\n## morph_wordlist file $dir/morph_wordlist.txt created\n"
  fi
  if [ $step -le 30 ]; then
    dir=$sdir
    phonetisaurus-g2p --model=$dir/train_lex.fst --input=$dir/morph_wordlist.txt --isfile --words \
    > $dir/morph_initial.lex 2>$dir/phonetisaurus-g2p.morph.log 
    echo -e "\n## check log $dir/phonetisaurus-g2p.log\n"

  fi  

  echo -e "\nmorph ended `date`\n"
##
fi

if $align4lex; then
  oov=`cat $lang/oov.int`; data=data/llp/train; 
  ali=exp/llp/plp_pitch/mono0a_ali; sdir=$ali;
  dir=exp/llp/laign_test;
  smooth_count=1.0; max_one=true
  if [ $step -le 1 ]; then
    [ -d $dir ] || mkdir -p $dir 2>/dev/null
    linear-to-nbest "ark:gzip -cdf $ali/ali.1.gz|" \
   "ark:utils/sym2int.pl --map-oov $oov -f 2- $lang/words.txt $data/text |" '' '' ark:- | \
    lattice-align-words $lang/phones/word_boundary.int $sdir/final.mdl ark:- ark:- | \
    lattice-to-phone-lattice --replace-words=false $sdir/final.mdl ark:- ark,t:-  | \
    awk '{ if (NF == 4) { word_phones = sprintf("%s %s", $3, $4); count[word_phones]++; } } 
        END { for(key in count) { print count[key], key; } }' | \
          sed s:0,0,:: | awk '{print $2, $1, $3;}' | sed 's/_/ /g' | \
          utils/int2sym.pl -f 3- $lang/phones.txt  | \
          sed -E 's/_I( |$)/ /g' |  sed -E 's/_E( |$)/ /g' | sed -E 's/_B( |$)/ /g' | sed -E 's/_S( |$)/ /g' | \
          utils/int2sym.pl -f 1 $lang/words.txt |sort -u > $dir/lexicon_counts.txt 2>&1 | tee $dir/get_fsts.log
  echo -e "\n align4lex ended `date`\n"
  fi
  nj=`cat $ali/num_jobs`; srcdir=$ali;
  if [ $step -le 5 ]; then
    ( ( for n in `seq $nj`; do gunzip -c $srcdir/ali.$n.gz; done ) | \
    linear-to-nbest ark:- "ark:utils/sym2int.pl --map-oov $oov -f 2- $lang/words.txt $data/text |" '' '' ark:- | \
    lattice-align-words $lang/phones/word_boundary.int $srcdir/final.mdl ark:- ark:- | \
    lattice-to-phone-lattice --replace-words=false $srcdir/final.mdl ark:- ark,t:- | \
    awk '{ if (NF == 4) { word_phones = sprintf("%s %s", $3, $4); count[word_phones]++; } } 
        END { for(key in count) { print count[key], key; } }' | \
          sed s:0,0,:: | awk '{print $2, $1, $3;}' | sed 's/_/ /g' | \
          utils/int2sym.pl -f 3- $lang/phones.txt  | \
          sed -E 's/_I( |$)/ /g' |  sed -E 's/_E( |$)/ /g' | sed -E 's/_B( |$)/ /g' | sed -E 's/_S( |$)/ /g' | \
          utils/int2sym.pl -f 1 $lang/words.txt > $dir/lexicon_counts.txt
    ) 2>&1 | tee $dir/log/get_fsts.log
  fi
  old_lexicon=data/llp/local/dict/lexicon.txt
  new_lexicon=$dir/lexicon_p.txt
  if [ $step -le 10 ]; then
    grep -v -w '^<eps>' $dir/lexicon_counts.txt | \
  perl -e ' ($old_lexicon, $smooth_count, $max_one) = @ARGV;
    ($smooth_count >= 0) || die "Invalid smooth_count $smooth_count";
    ($max_one eq "true" || $max_one eq "false") || die "Invalid max_one variable $max_one";
    open(O, "<$old_lexicon")||die "Opening old-lexicon file $old_lexicon"; 
    while(<O>) {
      $_ =~ m/(\S+)\s+(.+)/ || die "Bad old-lexicon line $_";
      $word = $1;
      $orig_pron = $2;
      # Remember the mapping from canonical prons to original prons: in the case of
      # syllable based systems we want to remember the locations of tabs in
      # the original lexicon.
      $pron = join(" ", split(" ", $orig_pron));
      $orig_pron{$word,$pron} = $orig_pron;
      $count{$word,$pron} += $smooth_count;
      $tot_count{$word} += $smooth_count;
    }
    while (<STDIN>) {
      $_ =~ m/(\S+)\s+(\S+)\s+(.+)/ || die "Bad new-lexicon line $_";
      $word = $1;
      $this_count = $2;
      $pron = join(" ", split(" ", $3));
      $count{$word,$pron} += $this_count;
      $tot_count{$word} += $this_count;
    }
    if ($max_one eq "true") {  # replace $tot_count{$word} with max count
       # of any pron.
      %tot_count = {}; # set to empty assoc array.
      foreach $key (keys %count) {
        ($word, $pron) = split($; , $key); # $; is separator for strings that index assoc. arrays.
        $this_count = $count{$key};
        if (!defined $tot_count{$word} || $this_count > $tot_count{$word}) {
          $tot_count{$word} = $this_count;
        }
      }
    }
    foreach $key (keys %count) {
       ($word, $pron) = split($; , $key); # $; is separator for strings that index assoc. arrays.
       $this_orig_pron = $orig_pron{$key};
       if (!defined $this_orig_pron) { die "Word $word and pron $pron did not appear in original lexicon."; }
       if (!defined $tot_count{$word}) { die "Tot-count not defined for word $word."; }
       $prob = $count{$key} / $tot_count{$word};
       print "$word\t$prob\t$this_orig_pron\n";  # Output happens here.
    } '  $old_lexicon $smooth_count $max_one |sort -u > $new_lexicon || exit 1;

  fi
  if [ $step -le 15 ]; then
   perl -e  '
     ($olex, $lexc) = @ARGV;
     open (LC, "<$lexc") or die "\nlexicon with count file $lexc cannot open\n";
     while (<LC>) {
       chomp;
       s/\t/\s/g;
       m/(\S+)\s+(\S+)\s+(.+)/;
       ($word, $count, $pron) = ($1, $2, $3);
       $vocab{$word} = 0;
       print "$word\t$pron\n";
     }
     close LC;
     open (OL, "<$olex") or die "\nold lexicon $olex cannot open\n";
     while(<OL>) {
       chomp;
       m/(\S+)\s+(.+)/ or die "bad old lexicon line $_\n";
       ($word, $pron) = ($1, $2);
       if (not exists $vocab{$word}) {
         print STDERR $_, "\n";
         print $_, "\n";
       }
     }
     close OL;

   '  $old_lexicon $dir/lexicon_counts.txt |sort -u  > $dir/lexicon_nopr.txt
  fi
 ##
fi
