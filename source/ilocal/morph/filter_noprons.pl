#!/usr/bin/env perl
use warnings;
use strict;

my ($noprons) = @ARGV;

my %vocab = ();

open N, "<$noprons" or die "file $noprons cannot open\n";
while (<N>) {
  chomp;
  m/(\S+)/ or next;
  print STDERR "word=$1\n";
  $vocab{$1} = 0;
}
close N;

while (<STDIN>){
  chomp;
  my @A = split / /; my $s = "";
  for(my $i = 0; $i < scalar @A; $i++) {
    my $w = $A[$i];
    if(not exists $vocab{$w}) {
      $s .= "$w ";
    } 
  }
  $s =~ s/ $//g;
  print "$s\n";
}

