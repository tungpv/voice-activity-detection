#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  log_string=$1; shift;
  echo -e "\n$log_string `hostname`@ `date` \n";
}
sleep_dur=3;
total_sleep=$[9*3600];
function Wait {
  f=$1; shift;
  x=0; 
  while [ ! -e $f ]; do
    sleep $sleep_dur;  x=$[x+$sleep_dur];
    if [ $x -gt $total_sleep ]; then
      die "$f is not ready";
    fi 
  done 
}
#
. path.sh || die "cuda_path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER SPECIFIED: $0 $@\n";
# begin tunable variable
# for f  in `ls /usr/local/proceeds/2013/speech/Interspeech2013/IS2013/PDF/AUTHOR/*.PDF`; do  if pdftotext  $f - | head | grep 'Korbinian';then echo $f; fi; done
cmd=run.pl;
nj=10;
step=0
substep=0
prepare_data=false
train_g2p=false
lang=data/llp/lang; lang_test=${lang}_test
samples_per_iter=4000000;  
max_mem=500000000;
state=2500;gauss=20000
pnorm_input_dim=3000; pnorm_output_dim=500; 
nonspeech_words=$lang/nonspeech_words.txt

# end tunable variable
bn_am_cf=conf/am_regular.sh
. parse_options.sh || die "parse_options.sh expected";

datastr="train dev10h" ;

if $prepare_data; then
  echo -e "\n##`hostname` `date`\n"
  slex=data/llp/local/dict/lexicon.txt;
  g2pdir=data/llp/local/g2p; dir=$g2pdir/lex; lex=$dir/train.lex; 
  if [ $step -le 1 ] ;then
    echo -e "\n## $0 prepare training lexicon`hostname` `date`\n"
    mkdir -p $dir 2>/dev/null
    cat $slex | awk '{if(NR>14){print;}}' > $lex;
    echo -e "\n## $0 done `date`\n"
  fi
  if [ $step -le 3 ]; then
    echo -e "\n## $0 train g2p model `hostname` `date`\n"
    lpstr="1,2,1,2"
    dir=$g2pdir
    dev=5%
    [ -e $lex ] || die "\nERROR: $0 lexicon file $lex does not exist \n";
    ilocal/g2p/train_simple_dev2.sh $lex $dir $lpstr $dev
    echo -e "\n## done `date`\n"
  fi
  echo -e "\n## prepare_data done `hostname` `date`\n"; 
fi
cid=g2pllp01;
if $train_g2p; then
  lpstr="0,1,0,1"

  dir=data/$cid/local/aachenG2P;
  if [ ! -f $dir/train.lex ]; then
    mkidr -p $dir 2>/dev/null
    cat data/local/lexicon.txt |\
    grep -v '<' > $dir/train.lex || exit 1
  fi
  train_lex=$dir/train.lex;
  dev_rate=5%
  if [ ! -f $dir/model-6 ]; then
    ilocal/g2p/train_simple_dev2.sh $train_lex $dir $lpstr $dev_rate
  fi
fi


