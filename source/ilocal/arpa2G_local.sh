#!/bin/bash

. path.sh
. cmd.sh 

echo
echo "$0 $@"
echo

# begin options

# end options

. parse_options.sh || exit 1

if [ $# -ne 3 ]; then
  echo
  echo "Usage: $0 <arpa-lm-file> <lang-dir> <dest-dir>"
  echo
  exit 1
fi

lmfile=$1
langdir=$2
destdir=$3

[ -d $destdir ] || mkdir -p $destdir 

[ -f $lmfile ] || \
{ echo "$0: lmfile $lmfile does not exist"; exit 1; }

