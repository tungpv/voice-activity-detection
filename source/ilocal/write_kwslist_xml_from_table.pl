#!/usr/bin/env perl
use strict;
use warnings;

use Getopt::Long;

# Function for sorting
sub KwslistOutputSort {
  if ($a->[0] ne $b->[0]) {
    if ($a->[0] =~ m/[0-9]+$/ && $b->[0] =~ m/[0-9]+$/) {
      ($a->[0] =~ /([0-9]*)$/)[0] <=> ($b->[0] =~ /([0-9]*)$/)[0]
    } else {
      $a->[0] cmp $b->[0];
    }
  } elsif ($a->[5] ne $b->[5]) {
    $b->[5] <=> $a->[5];
  } else {
    $a->[1] cmp $b->[1];
  }
}

# Function for printing Kwslist.xml
sub PrintKwslist {
  my ($info, $KWS, $write_term) = @_;

  my $kwslist = "";

  # Start printing
  if ($write_term eq "true") {
    $kwslist .= "<stdlist termlist_filename=\"$info->[0]\" language=\"$info->[1]\" system_id=\"$info->[2]\">\n";
  }else {
    $kwslist .= "<kwslist kwlist_filename=\"$info->[0]\" language=\"$info->[1]\" system_id=\"$info->[2]\">\n";
  }
  my $prev_kw = "";
  foreach my $kwentry (@{$KWS}) {
    if ($prev_kw ne $kwentry->[0]) {
      if ($prev_kw ne "") {
        if ($write_term eq "true") {
          $kwslist .= "  </detected_termlist>\n";
        } else {
          $kwslist .= "  </detected_kwlist>\n";
        }
      }
      if ($write_term eq "true") {
        $kwslist .= "  <detected_termlist search_time=\"1\" termid=\"$kwentry->[0]\" oov_count=\"0\">\n";
      } else {
        $kwslist .= "  <detected_kwlist search_time=\"1\" kwid=\"$kwentry->[0]\" oov_count=\"0\">\n";
      }
      $prev_kw = $kwentry->[0];
    }
    if ($write_term eq "true") {
      $kwslist .= "    <term file=\"$kwentry->[1]\" channel=\"$kwentry->[2]\" tbeg=\"$kwentry->[3]\" dur=\"$kwentry->[4]\" score=\"$kwentry->[5]\" decision=\"$kwentry->[6]\"";
    } else {
      $kwslist .= "    <kw file=\"$kwentry->[1]\" channel=\"$kwentry->[2]\" tbeg=\"$kwentry->[3]\" dur=\"$kwentry->[4]\" score=\"$kwentry->[5]\" decision=\"$kwentry->[6]\"";
    }
    if (defined($kwentry->[7])) {$kwslist .= " threshold=\"$kwentry->[7]\"";}
    if (defined($kwentry->[8])) {$kwslist .= " raw_score=\"$kwentry->[8]\"";}
    $kwslist .= "/>\n";
  }
  if ($write_term eq "true") {
    $kwslist .= "  </detected_termlist>\n";
    $kwslist .= "</stdlist>\n";
  } else {
    $kwslist .= "  </detected_kwlist>\n";
    $kwslist .= "</kwslist>\n";
  }
  return $kwslist;
}


my $thresh = 2.5;
my $language = "unknown";
my $write_term = "false";
my $remove_duplicate = "false";

GetOptions( 'thresh=f' => \$thresh,
 'language=s' => \$language,
 'write-term=s' => \$write_term,
 'remove-duplicate=s' =>\$remove_duplicate);

my @info = ("", $language, "");
my $numArgs = scalar @ARGV;
print STDERR  "\n\nLOG:", join " ", @ARGV, "\n\n";
if ($numArgs != 1) {
  die "\n\nUsage: $0 [options] <ecf.txt> <score-table.txt>\n\n";
}
my ($ecfFName) = @ARGV;
my %ecf_vocab = ();
open(F, "$ecfFName") or die "file $ecfFName cannot open\n";
while(<F>) {
  chomp;
  m/(\S+)\s+(.*)/ or next;
  $ecf_vocab{$1} = $2;
}
close F;
print STDERR "$0: stdin expected ...\n";
my @aKWS = ();
my %vocab = ();
while(<STDIN>) {
  chomp;
  m/(\S+)\s+(\S+)\s+(.*)/ or next;
  my ($kwid, $utt, $score) = ($1, $2, $3);
  if ($remove_duplicate eq "true") {
    next if exists $vocab{$kwid}{$utt};
    $vocab{$kwid}{$utt} ++;
  }
  die "unknown $utt\n" if not exists $ecf_vocab{$utt};
  my $time_str = $ecf_vocab{$utt};
  my @aTime = split (/\s+/, $time_str);
  my $decision = "NO";
  if($score >= $thresh) {
    $decision = "YES";
  }
  $score = sprintf("%.6f", $score);
  push(@aKWS, [$kwid, $utt, 1, $aTime[0], $aTime[1], $score, $decision]);
}

print STDERR "$0: stdin ended ...\n";

# Output sorting
my @atmp = sort KwslistOutputSort @aKWS;
# Printing
my $kwslist = PrintKwslist(\@info, \@atmp, $write_term);
print $kwslist;
