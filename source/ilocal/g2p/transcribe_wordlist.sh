#!/bin/bash

echo -e "\n## $0 $@\n"

function die {
  echo -e "\n## ERROR: $1\n";
  exit 1
}

#local/g2p/transcribe_wordlist.sh test/morph/morph.mdl test/g2p/g2p5.fst test/g2plex/wlist.txt test/g2plex 
. path.sh

# --
use_aachen=true
nbest=1
# --
. utils/parse_options.sh

if [ $# -ne 4 ]; then
  echo -e "\n\nGiven a wordlist, we first decompose words in it into"
  echo "morph sequences from which morph list is generated. Then we"
  echo "transcribe morph using g2p model"
  echo -e "\nUsage: $0 [options] morph.mdl g2p.mdl wordlist dir";
  echo -e "--nbest N # N pronunciation is given\n\n"
  exit 1
fi

morph=$1; shift
g2pmdl=$1; shift;
wlist=$1; shift;
dir=$1; shift;

for x in $morph $g2pmdl $wlist; do
  [ -f $x ] || die "file $x is not there";
done

[ $nbest -gt 0 ] || die "$nbest should be over zero";

sdir=$dir;
dir=$dir/tmp
mkdir -p $dir 2>/dev/null
[ -d $dir ] || die "making folder $dir is failed";

cat $wlist | grep -v '<' > $dir/wlist.txt

morfessor-segment -l $morph -o $dir/morph.txt $dir/wlist.txt
if [[ `wc -l < $dir/wlist.txt` -ne `wc -l < $dir/morph.txt` ]]; then
  die "word and morph numbers are mismatched, check $dir/wlist.txt and $dir/morph.txt";
fi

paste <(cat $dir/wlist.txt) <(cat $dir/morph.txt| sed -e 's:_: :g') | sort -u > $dir/word_morph.txt
[ -e $dir/word_morph.txt ] || die "file $dir/word_morph.txt is not there";

cat $dir/morph.txt | \
sed -e 's:_::g' | \
awk '{for(i=1;i<=NF;i++){print $i;}}' | sort -u > $dir/mlist.txt

if [[ ! -e $dir/mlist.txt || `wc -l < $dir/mlist.txt` -le 0 ]]; then
  die "morph list is not normally generated";
fi

if $use_aachen; then
  g2p.py --model=$g2pmdl --apply=$dir/mlist.txt  --word-to-phoneme=$dir/morph.lex > $dir/g2p.log 2>&1
  echo -e "\n## check $dir/g2p.log !!!\n"
  if [[ `wc -l < $dir/morph.lex` -eq 0 ]]; then
    die "making morph.lex by g2p.py failed";
  fi
else
  phonetisaurus-g2p --nbest=$nbest --model=$g2pmdl --input=$dir/mlist.txt \
  --isfile --words 2>$dir/phonetisaurus-g2p.log | \
  sort -u > $dir/raw_morph.lex
  [ -f $dir/raw_morph.lex ] || die "$dir/raw_morph.txt is not there"
  cat $dir/raw_morph.lex | \
  awk '{$2=""; print $0}' | \
  awk '{printf("%s\t", $1); for(i=2;i<NF;i++){printf("%s ", $i)}; printf("\n"); }' \
  |sort -u > $dir/morph.lex
  
fi
cat $dir/morph.lex | \
awk '{if (NF==1){print $1;}}' > $sdir/noprons.txt
echo -e "\n## check morph list $sdir/noprons.txt \n";
cat $dir/morph.lex | \
awk '{if(NF>1){printf "%s\t",$1;for(i=2; i<=NF; i++){printf "%s ", $i; } printf "\n"; }}' > $sdir/lexicon.txt

echo -e "\n## check g2p lexicon file $sdir/lexicon.txt\n"

