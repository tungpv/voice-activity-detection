#!/bin/bash

run_name=$0;
function die {
  echo -e "\nERROR:$run_name:$0\n"; exit 1;
}

# begin options
step=1; 
#end options

. g2p_path.sh || exit 1;
. utils/parse_options.sh || exit 1


source_dict=/data4/kws13/c1/data/local/dict/lexicon.txt;

sdir=exp/g2p/align
if [ $step -le 1 ]; then
  echo -e "\n## organize dict\n"
  dir=$sdir;
  mkdir -p $dir 2>/dev/null
  cat $source_dict | \
  ilocal/g2p/decompose_word.py  $dir/vet_lexicon.train
  echo -e "\n## done\n"; 
fi

if [ $step -le 2 ]; then
  echo -e "\n## run alignment `date`\n"
  phonetisaurus-align --input=$sdir/vet_lexicon.train --ofile=$sdir/vet_lexicon.corpus --s1_char_delim="#"
  echo -e "\n## done `date`\n" 
  exit 0
fi

if [ $step -le -3 ]; then
  align=/opt/tools/phonetisaurus-0.7.8/is2013-conversion/models/g014b2b.train; corpus=exp/g2p/align/g014b2b.corpus
  phonetisaurus-align --input=$align --ofile=$corpus
fi

dir=$sdir

if [ $step -le 5 ]; then
   ngram-count -kn-modify-counts-at-end -order 7  -gt1min 0 -gt2min 0 \
          -gt3min 0 -gt4min 0 -gt5min 0 -gt6min 0 -gt7min 0 -ukndiscount \
          -ukndiscount1 -ukndiscount2 -ukndiscount3 -ukndiscount4 \
          -ukndiscount5 -ukndiscount6 -ukndiscount7 -text $dir/vet_lexicon.corpus -lm $dir/lexicon.arpa

fi

if [ $step -le 7 ]; then
  phonetisaurus-arpa2fst --input=$dir/lexicon.arpa --prefix="$dir"
fi

if [ $step -le 9 ]; then
   cat exp/g2p/wordlist.txt | while read w; do
      phonetisaurus-g2p --model=exp/g2p/align.fst --input="$w"  --words
    done

fi
