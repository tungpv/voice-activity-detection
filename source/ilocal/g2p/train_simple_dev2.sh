#!/bin/bash

function die {
  echo -e "\n## ERROR: $1\n"; 
  exit 1
}
. path.sh

trigram_only=false
echo "$0: $@"
. utils/parse_options.sh

train_lex=$1; shift;
dir=$1; shift;
lpstr=$1;shift
dev=$1; shift;

log=$dir/log
[ -d $log ] || mkdir -p $log

if [ ! -f $dir/model-1 ]; then
  echo -e "\nunigram is started `date`\n"
 echo  g2p.py  --encoding UTF-8 --train $train_lex \
  --devel $dev  -s $lpstr --write-model $dir/model-1
  g2p.py  --encoding UTF-8 --train $train_lex \
  --devel $dev  -s $lpstr --write-model $dir/model-1   > $log/log.1 2>&1 
fi

if [ ! -f $dir/model-1 ]; then
  die "$dir/model-1 is not there";
fi

if [ ! -f $dir/model-2 ]; then
  echo -e "\n2gram is started `date`\n"
  g2p.py  --model $dir/model-1 --ramp-up --train $train_lex \
  --devel $dev  -s $lpstr --write-model $dir/model-2  > \
  $log/log.2 2>&1
fi

if [ ! -f $dir/model-2 ]; then
  die "$dir/model-2 is not there";
fi

if [ ! -f $dir/model-3 ]; then
  echo -e "\n3gram is started `date`\n"
  g2p.py  --model $dir/model-2 --ramp-up --train $train_lex \
  --devel $dev  -s $lpstr --write-model $dir/model-3 > \
  $log/log.3 2>&1

  if $trigram_only;then
    echo "$0: 3gram g2p model training done" && exit 0
  fi
fi

if [ ! -f $dir/model-3 ]; then
  die "$dir/model-3 is not there";
fi

if [ ! -f $dir/model-4 ]; then
  echo -e "\n4gram is started `date`\n"
  g2p.py  --model $dir/model-3 --ramp-up --train $train_lex \
  --devel $dev  -s $lpstr --write-model $dir/model-4 > \
  $log/log.4 2>&1
fi

if [ ! -f $dir/model-4 ]; then
  die "$dir/model-4 is not there";
fi



if [ ! -f $dir/model-5 ]; then
  echo -e "\n5gram is started `date`\n"
  g2p.py  --model $dir/model-4 --ramp-up --train $train_lex \
  --devel $dev  -s $lpstr --write-model $dir/model-5 > \
  $log/log.5 2>&1
fi

if [ ! -f $dir/model-5 ]; then
  die "$dir/model-5 is not there";
fi

if [ ! -f $dir/model-6 ]; then
  echo -e "\n5gram is started `date`\n"
  g2p.py  --model $dir/model-5 --ramp-up --train $train_lex \
  --devel $dev  -s $lpstr --write-model $dir/model-6 > \
  $log/log.6 2>&1
fi

if [ ! -f $dir/model-6 ]; then
  die "$dir/model-6 is not there";
fi

echo -e "\n## check $dir/model-6\n"

