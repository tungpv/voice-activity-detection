#!/bin/bash


function die {
  echo -e "\n## ERROR: $1\n"; exit 1
}

echo -e "\n## $0 $@\n";
if [ $# -ne 3 ]; then
  echo -e "\nusing kaldi lexicon.txt to train";
  echo "g2p model using phonetisaurus toolkit"
  echo -e "\n$0 <train_lexicon.txt> <dev_lexicon> <dir>\n\n"
  exit 1
fi

. utils/parse_options.sh
. path.sh

slex=$1; shift
devlex=$1; shift
dir=$1; shift

[ -f $slex ] || die "training lexicon $slex does not exist";
[ -f $devlex ] || die "dev lexicon $devlex does not exist";

tmpdir=$dir/tmp;
mkdir -p $tmpdir 2>/dev/null

[ -d $tmpdir ] || die "making $tmp folder failed";

paste <(grep -v '<' $slex | cut -f 1) <(egrep -v '<|\[' $slex|cut -f 2- | awk '{gsub(/\t/," ",$0); print; }')  > $tmpdir/train.lex

[ -e $tmpdir/train.lex ] || die "$tmpdir/train.lex expected";

if [ ! -f $tmpdir/train.corpus ]; then
  phonetisaurus-align --seq2_del=false \
  --input=$tmpdir/train.lex --ofile=$tmpdir/train.corpus 
fi

echo -e "\n## check $tmpdir/train.corpus\n";
textopt="-text $tmpdir/train.corpus"
gtmin="-gt1min 0 -gt2min 1 -gt3min 2"
kndc="-kndiscount1 -kndiscount2 -kndiscount3"

echo "## trigram"
ngram-count -order 3 -kn-modify-counts-at-end \
 -lm $tmpdir/lm3.arpa -sort $textopt  $kndc

gtmin="$gtmin -gt4min 2"
kndc="$kndc -kndiscount4"
echo "## 4gram"
ngram-count -order 4 -kn-modify-counts-at-end \
$gtmin $kndc $textopt  -lm $tmpdir/lm4.arpa -sort 

gtmin="$gtmin -gt5min 2"
kndc="$kndc -kndiscount5"

echo "## 5gram"
ngram-count -order 5 -kn-modify-counts-at-end \
$gtmin $kndc $textopt -lm $tmpdir/lm5.arpa -sort

gtmin="$gtmin -gt6min 2"
kndc="$kndc -kndiscount6"

echo "## 6gram"
ngram-count -order 6  -kn-modify-counts-at-end \
$gtmin $kndc $textopt -lm $tmpdir/lm6.arpa -sort

gtmin="$gtmin -gt7min 2"
kndc="$kndc -kndiscount7"

echo "## 7gram"
ngram-count -order 7  -kn-modify-counts-at-end \
$gtmin $kndc $textopt -lm $tmpdir/lm7.arpa -sort

cat $devlex | \
awk '{print $1; }' > $tmpdir/dev.wlist

for x in 3 4 5 6 7; do
  echo -e "\n## train g2p$x model and evaluate it\n"
  arpalm=$tmpdir/lm$x.arpa;
  [ -f $arpalm ] || die "arpa lm $arpalm expected"
  phonetisaurus-arpa2fst --input=$arpalm --prefix="$dir/g2p$x" 
  [ -e $dir/g2p$x.fst ] || die "fst $dir/g2p$x.fst expected";
  phonetisaurus-g2p --model=$dir/g2p$x.fst --input=$tmpdir/dev.wlist \
  --isfile --words 2>$tmpdir/g2p$x.log | \
  sort -u > $tmpdir/raw_dev$x.lex
  cat $tmpdir/raw_dev$x.lex | \
  awk '{$2=""; print $0}' | \
  awk '{printf("%s\t", $1); for(i=2;i<NF;i++){printf("%s ", $i)}; printf("\n"); }' \
  |sort -u > $tmpdir/dev$x.lex
  
  compute-wer --text --mode=present ark:$devlex ark:$tmpdir/dev$x.lex \
  > $tmpdir/per_$x || die "comput-wer failed";
done


