#!/usr/bin/env python
import sys
import codecs

if len(sys.argv) != 2:
	print '\n\nUsage:', sys.argv[0], '<output>\n\n'
	sys.exit(1)
output = sys.argv[1]
fp = codecs.open(output, "w", "utf-8")
for line in sys.stdin:
	myList = line.split()
        s1 = myList[0]
	w1 = unicode(s1, 'utf-8')
        w =  unicode('#','utf-8'). join(w1.lower())
        s2 = myList [1:]
        s3 = ' '.join (s2)
        s = unicode(s3, 'utf-8')
        wordPhone = w + '\t'+ s + '\n'
	fp.write(wordPhone)
fp.close()
