#!/bin/bash

# Copyright 2012  Johns Hopkins University (Author: Daniel Povey)
# Apache 2.0
# Decoding of fMMI or fMPE models (feature-space discriminative training).
# If transform-dir supplied, expects e.g. fMLLR transforms in that dir.

# Begin configuration section.  
stage=1
iter=final
nj=4
cmd=run.pl
maxactive=7000
beam=13.0
latbeam=6.0
acwt=0.083333 # note: only really affects pruning (scoring is on lattices).
post_acwt=$acwt;
ngselect=2; # Just use the 2 top Gaussians for fMMI/fMPE.  Should match train.
transform_dir=
num_threads=1 # if >1, will use gmm-latgen-faster-parallel
parallel_opts=  # If you supply num-threads, you should supply this too.
scoring_opts=
delta_order=2
# for fmllr
fmllr=false;
silence_weight=0.01
fmllr_update_type=full
max_arcs=-1

# End configuration section.

echo "$0 $@"  # Print the command line for logging

[ -f ./path.sh ] && . ./path.sh; # source the path.
. parse_options.sh || exit 1;

if [ $# != 3 ]; then
   echo "Usage: steps/decode_fmmi.sh [options] <graph-dir> <data-dir> <decode-dir>"
   echo "... where <decode-dir> is assumed to be a sub-directory of the directory"
   echo " where the model is."
   echo "e.g.: steps/decode_fmmi.sh exp/mono/graph_tgpr data/test_dev93 exp/mono/decode_dev93_tgpr"
   echo ""
   echo "This script works on CMN + (delta+delta-delta | LDA+MLLT) features; it works out"
   echo "what type of features you used (assuming it's one of these two)"
   echo "You can also use fMLLR features-- you have to supply --transform-dir option."
   echo ""
   echo "main options (for others, see top of script file)"
   echo "  --config <config-file>                           # config containing options"
   echo "  --nj <nj>                                        # number of parallel jobs"
   echo "  --iter <iter>                                    # Iteration of model to test."
   echo "  --cmd (utils/run.pl|utils/queue.pl <queue opts>) # how to run jobs."
   echo "  --acwt <float>                                   # acoustic scale used for lattice generation "
   echo "  --transform-dir <transform-dir>                  # where to find fMLLR transforms."
   echo "  --scoring-opts <string>                          # options to local/score.sh"
   echo "                                                   # speaker-adapted decoding"
   echo "  --num-threads <n>                                # number of threads to use, default 1."
   echo "  --parallel-opts <opts>                           # e.g. '-pe smp 4' if you supply --num-threads 4"
   exit 1;
fi


graphdir=$1
data=$2
dir=$3
srcdir=`dirname $dir`; # The model directory is one level up from decoding directory.
sdata=$data/split$nj;
splice_opts=`cat $srcdir/splice_opts 2>/dev/null`
thread_string=
[ $num_threads -gt 1 ] && thread_string="-parallel --num-threads=$num_threads" 

mkdir -p $dir/log
[[ -d $sdata && $data/feats.scp -ot $sdata ]] || split_data.sh $data $nj || exit 1;
echo $nj > $dir/num_jobs

model=$srcdir/$iter.mdl
silphonelist=`cat $graphdir/phones/silence.csl` || exit 1;

for f in $sdata/1/feats.scp $sdata/1/cmvn.scp $model $graphdir/HCLG.fst; do
  [ ! -f $f ] && echo "decode_fmmi.sh: no such file $f" && exit 1;
done

if [ -f $srcdir/final.mat ]; then feat_type=lda; else feat_type=delta; fi
echo "decode_fmmi.sh: feature type is $feat_type";

feats="ark,s,cs:apply-cmvn --norm-vars=false --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- |"
case $feat_type in
  delta) 
    if [ $delta_order -gt 0 ]; then
      feats="$feats add-deltas ark:- ark:- |"
    fi
    ;;
  lda) feats="$feats splice-feats $splice_opts ark:- ark:- | transform-feats $srcdir/final.mat ark:- ark:- |";;
  *) echo "Invalid feature type $feat_type" && exit 1;
esac

if [ ! -z "$transform_dir" ]; then # add transforms to features...
  echo "Using fMLLR transforms from $transform_dir"
  [ ! -f $transform_dir/trans.1 ] && echo "Expected $transform_dir/trans.1 to exist."
  [ "`cat $transform_dir/num_jobs`" -ne $nj ] && \
     echo "Mismatch in number of jobs with $transform_dir";
  feats="$feats transform-feats --utt2spk=ark:$sdata/JOB/utt2spk ark:$transform_dir/trans.JOB ark:- ark:- |"
fi

fmpe_dir=$dir;
if $fmllr; then
  fmpe_dir=${dir}.fmpe;
fi
fmpefeats="$feats fmpe-apply-transform $srcdir/$iter.fmpe ark:- 'ark,s,cs:gunzip -c $fmpe_dir/gselect.JOB.gz|' ark:- |" 

function do_fmpe_decode {
  dir=$1; stage=$2;

  if [ $stage -le 1 ]; then
    # Get Gaussian selection info.
    $cmd JOB=1:$nj $dir/log/gselect.JOB.log \
    gmm-gselect --n=$ngselect $srcdir/$iter.fmpe "$feats" \
    "ark:|gzip -c >$dir/gselect.JOB.gz" || exit 1;
  fi
  
  if [ $stage -le 2 ]; then
    $cmd $parallel_opts JOB=1:$nj $dir/log/decode.JOB.log \
    gmm-latgen-faster$thread_string --max-active=$maxactive --beam=$beam --lattice-beam=$latbeam \
    --acoustic-scale=$acwt --allow-partial=true --word-symbol-table=$graphdir/words.txt \
    $model $graphdir/HCLG.fst "$fmpefeats" "ark:|gzip -c > $dir/lat.JOB.gz" || exit 1;
  fi

  if [ $stage -le 3 ]; then
    [ ! -x local/score.sh ] && \
    echo "Not scoring because local/score.sh does not exist or not executable." && exit 1;
    local/score.sh $scoring_opts --cmd "$cmd" $data $graphdir $dir
  fi
}

if  $fmllr; then
  if [ $stage -le 1 ]; then
    do_fmpe_decode $fmpe_dir 1 ;
  fi
  # use fmpe lattice to estimate first-pass fMMR transforms
  dir=${dir}.fmllr;
  pass1feats="$fmpefeats transform-feats --utt2spk=ark:$sdata/JOB/utt2spk ark:$dir/pre_trans.JOB ark:- ark:- |"
  if [ $stage -le 2 ]; then
    echo "$0: getting first-pass fMLLR transforms."
    $cmd JOB=1:$nj $dir/log/fmllr_pass1.JOB.log \
    gunzip -c $fmpe_dir/lat.JOB.gz \| \
    lattice-to-post --acoustic-scale=$post_acwt ark:- ark:- \| \
    weight-silence-post $silence_weight $silphonelist $model ark:- ark:- \| \
    gmm-post-to-gpost $model "$fmpefeats" ark:- ark:- \| \
    gmm-est-fmllr-gpost --fmllr-update-type=$fmllr_update_type \
    --spk2utt=ark:$sdata/JOB/spk2utt $model "$fmpefeats" ark,s,cs:- \
    ark:$dir/pre_trans.JOB || exit 1; 
  fi
  # do the main lattice generation pass.
  if  [ $stage -le 3 ]; then
    echo "$0: doing main lattice generation phase"
    $cmd $parallel_opts JOB=1:$nj $dir/log/decode.JOB.log \
    gmm-latgen-faster$thread_string --max-active=$maxactive --beam=$beam --lattice-beam=$latbeam \
    --acoustic-scale=$acwt --max-arcs=$max_arcs \
    --determinize-lattice=false --allow-partial=true --word-symbol-table=$graphdir/words.txt \
    $model $graphdir/HCLG.fst "$pass1feats" "ark:|gzip -c > $dir/lat.tmp.JOB.gz" \
    || exit 1;
  fi
  # do a second pass;
  if [ $stage -le 5 ]; then
    echo "$0: estimating fMLLR transforms a second time."
    $cmd JOB=1:$nj $dir/log/fmllr_pass2.JOB.log \
    lattice-determinize-pruned --acoustic-scale=$acwt --beam=4.0 \
    "ark:gunzip -c $dir/lat.tmp.JOB.gz|" ark:- \| \
    lattice-to-post --acoustic-scale=$post_acwt ark:- ark:- \| \
    weight-silence-post $silence_weight $silphonelist $model ark:- ark:- \| \
    gmm-est-fmllr --fmllr-update-type=$fmllr_update_type \
    --spk2utt=ark:$sdata/JOB/spk2utt $model "$pass1feats" \
    ark,s,cs:- ark:$dir/trans_tmp.JOB '&&' \
    compose-transforms --b-is-affine=true ark:$dir/trans_tmp.JOB ark:$dir/pre_trans.JOB \
    ark:$dir/trans.JOB  || exit 1;
  fi  
  #
  feats="$fmpefeats transform-feats --utt2spk=ark:$sdata/JOB/utt2spk ark:$dir/trans.JOB ark:- ark:- |"

  if [ $stage -le 7 ]; then
    echo "$0: doing a final pass of acoustic rescoring."
    $cmd $parallel_opts JOB=1:$nj $dir/log/acoustic_rescore.JOB.log \
    gmm-rescore-lattice $model "ark:gunzip -c $dir/lat.tmp.JOB.gz|" "$feats" ark:- \| \
    lattice-determinize-pruned$thread_string --acoustic-scale=$acwt --beam=$latbeam ark:- \
    "ark:|gzip -c > $dir/lat.JOB.gz" '&&' rm $dir/lat.tmp.JOB.gz || exit 1;
  fi
  do_fmpe_decode $dir 3;  # one enable scoring function :-)

  rm $dir/{trans_tmp,pre_trans}.*;
else
  do_fmpe_decode $dir 0;

fi
exit 0;
