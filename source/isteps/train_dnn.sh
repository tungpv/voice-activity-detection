#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";

# begin user-defined variable
nj=10;
stage=0;
mdldir=;
train_ali=;
dev_ali=;
align_needed=false;
fmllr_align=false;
# end user-defined variable
echo -e "\n $0 $@\n";
. parse_options.sh || \
die "parse_options.sh expected";
#
if [[ $# -lt 1 ]]; then
  echo -e "\nwrapper to train dnn and do evaluation\n";
  echo -e "$0 [options] <config-file>\n\n";
   exit 1;
fi 
echo -e "\n started `date`\n";
configs=$1; shift;
. $configs || die "config file expected";

# do forced alignment
if $align_needed; then
  echo -e "\nforced alignment started at `hostname` `date`\n";
  # check 
  [[ ! -z $modeldir4ali && -e $modeldir4ali/final.mdl ]] \
  || die "final.mdl is not ready in folder $modeldir4ali";
  mdldir=$modeldir4ali;
  idx=0;
  array_dir=($train_ali $dev_ali);
  for x in $traindata4ali $devdata4ali; do
     alidir=${array_dir[$idx]}; idx=$[$idx+1];
    [[ -d $x ]] || \
    die "data $x is not ready ?";
    if $fmllr_align; then
      if [ ! -e $alidir/ali.1.gz ]; then
        steps/align_fmllr.sh --nj $nj \
        --cmd "$train_cmd" $x $lang $mdldir $alidir \
        ||  die "forced alignment failed";
      fi
    else
      steps/align_si.sh --nj $nj \
      --cmd "$train_cmd" $x $lang $mdldir $alidir \
      ||  die "forced alignment failed";   
    fi
  done
fi

# check
for x in $train_ali $dev_ali; do
 [[ -e $x/ali.1.gz ]] || die "ali $x is not well ready";
done


if [[ $stage -le 2 ]] ;then
  echo -e "\ntrain bottleneck neural network\n";
  $cuda_cmd $bn1dir/_train_nnet.log \
  isteps/tandem/train_nnet.sh --hid-layers $hid_layers \
  --temp-prefix $temp_prefix \
  --clean-temp $clean_temp \
  --hid-dim $hid_dim --bn-dim $bn1_output_dim --feat-type traps \
  --splice $bn1_input_ctx_width \
  --traps-dct-basis $traps_dct_basis \
  --learn-rate 0.008 \
  --bunch-size $bunch_size \
  --cache-size $cache_size \
  $train_data $dev_data $lang $train_ali $dev_ali $bn1dir \
  || die "training bn failed";

fi
# compose feature transform with bn1
feature_transform=$bn1dir/final.feature_transform.bn1;
if [ $stage -le 3 ]; then
  echo -e "\ncomposed bn1 to as a feature transform to bn2\n";
  [ -e $bn1dir/final.nnet ] || \
  die "bn1 $bn1dir/final.nnet does not exist";
  nnet-concat $bn1dir/final.feature_transform \
  "nnet-copy --remove-last-layers=$remove_last_layers --binary=false $bn1dir/final.nnet - |" \
      "utils/nnet/gen_splice.py --fea-dim=$bn1_output_dim --splice=$bn1_output_ctx_width --splice-step=$bn1_output_splice_step |" \
      $feature_transform

fi
# check to proceed
[ -e $feature_transform ] || \
die "feature_transform $feature_transform does not exist";
# train bottleneck neural network 2 as merger
if [ $stage -le 4 ] ;then
  echo -e "\nbn2 training started\n";
  $cuda_cmd $bn2dir/_train_nnet.log \
  isteps/tandem/train_nnet.sh --hid-layers $hid_layers \
  --temp-prefix $temp_prefix \
  --clean-temp $clean_temp \
  --hid-dim $hid_dim --bn-dim $bn2_output_dim \
   --feature-transform $feature_transform \
  --learn-rate 0.008 \
  --bunch-size $bunch_size \
  --cache-size $cache_size \
  $train_data $dev_data $lang $train_ali $dev_ali $bn2dir || exit 1;
fi

echo -e "\nhierbn finished `hostname` `date`\n";
exit 0;
