#!/usr/bin/perl -w
use strict;
use XML::Simple;
use Data::Dumper;
binmode STDOUT, ":utf8";
# ------ main entrance -----
scalar @ARGV > 0 || die "\nkwlist.xml file expected\n";
my $kxml = shift @ARGV;

my $data = XMLin($kxml);
# print Dumper ($data->{kw});
foreach my $kwentry (@{$data->{kw}}) {
  print "$kwentry->{kwid}\t$kwentry->{kwtext}\n";
}
