#!/usr/bin/perl -w
use strict;

@ARGV > 0 || die "words.txt file expected ...";
my $words = shift @ARGV;

# print "\nwords file is $words\n" ;

sub load_words {
  my ($vocab, $wfile) = @_;
  open (my $file, "<", $wfile) || die "$wfile cannot open ...";
  while (<$file>) {
    chomp;
    m/(\S+)\s+(\d+)/|| next;
    my ($wstr, $index) = ($1, $2);
    $$vocab{$wstr} = $index;
  }
  close ($file); 
}

# -------- main entrance ----
my %vocab = ();
&load_words (\%vocab, $words);
my $int_str = "";
while (<STDIN>) {
  chomp;
  m/(\S+)/ || next;
  my $silword = $1;
  # print "$silword\n";
  if (not exists $vocab{$silword}) {
    print STDERR "WARNING: $silword does not exist ...\n";
    next;
  }
  my $index = $vocab{$silword};
  $int_str .= "$index:";
}
$int_str =~ s/:$//;
print "$int_str\n";

