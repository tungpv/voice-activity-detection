#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}

. path.sh || die "path.sh expected";
echo -e "\n$0 $@\n";
# begin user-defined variable
prefix=; 
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";


if [[ $# -ne 2 ]]; then
  echo -e "\n\nUsage:\n$0 <source_dir> # kaldi format data";
  echo -e "         <dest_dir> # kaldi format data\n\n";
  exit -1;
fi

sdir=$1; shift;
dir=$1; shift;

[[ ! -d $dir ]] && mkdir -p $dir;

for f in cmvn.scp reco2file_and_channel  spk2utt text \
      feats.scp  segments  utt2spk stm; do
  [[ -e $sdir/$f ]] && cp $sdir/$f $dir/$f;
done
# change the  path  so we can locate it in new place
if [[ ! -z $prefix  && -e $dir/feats.scp ]]; then
  perl -e '($fname, $prefix) = @ARGV; open FILE, "$fname";
  while (<FILE>) {
    chomp;
    m:(\S+)\s+(\S+):|| next;
    ($lab, $pos) = ($1, $2);
    print "$lab $prefix/$pos\n";
  }
 ' $dir/feats.scp $prefix  > tmp.feats.scp;
 mv tmp.feats.scp $dir/feats.scp;
fi

