#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";

# begin user-defined variable
nj=10;
configs=;
stage=0;
mdldir=;
train_ali=;
dev_ali=;
align_needed=false;
fmllr_align=false;
# end user-defined variable
echo -e "\n $0 $@\n";
. parse_options.sh || \
die "parse_options.sh expected";
if [[ -z $configs || ! -e $configs ]] ;then
  die "config file must be specified and exist";
fi
. $configs;
#
if [[ $# -ne 5 ]]; then
  echo -e "\nwrapper script used to train bottleneck feature";
  echo -e "\n\nUsage: $0 \n";
  echo -e "\t <lang-dir>  # kaldi lang dir";
 # echo -e "\t <model-dir> # acoustic models used to";
  echo -e "\t\t # align the training and dev. data";
  echo -e "\t <train-data> # training data";
  echo -e "\t <dev-data> # dev data";
  echo -e "\t <neural network dir1> # folder1  where trained nerual";
  echo -e "\t\t # network1 is in";
  echo -e "\t <neural network dir2> # folder2 for neural network 2";
  echo -e "\n\n";
fi 

lang=$1; shift;
train_data=$1; shift;
dev_data=$1; shift;
dir=$1; shift;
dir2=$1; shift;
# do forced alignment
if $align_needed; then
  echo -e "\nforced alignment started at `date`\n";
  # check 
  [[ ! -z $mdldir && -e $mdldir/final.mdl ]] \
  || die "final.mdl is not ready in folder $mdldir";
  for x in $train_data $dev_data; do
    [[ -d $x ]] || \
    die "data $x is not ready ?";
    xname=`basename $x`;
    alidir=$mdldir/ali_$xname;
    if $fmllr_align; then
      if [ ! -e $alidir/ali.1.gz ]; then
        steps/align_fmllr.sh --nj $nj \
        --cmd "$train_cmd" $x $lang $mdldir $alidir \
        ||  die "forced alignment failed";
      fi
    else
      steps/align_si.sh --nj $nj \
      --cmd "$train_cmd" $x $lang $mdldir $alidir \
      ||  die "forced alignment failed";   
    fi
  done
fi
# train bottleneck neural network
[[ -z $train_ali ]] || \
train_ali=$mdldir/ali_`basename $train_data`;
[[ -z $dev_ali ]] || \
dev_ali=$mdldir/ali_`basename $dev_data`;
# check
for x in $train_ali $dev_ali; do
 [[ -d $x ]] || die "ali $x does not exist";
done


if [[ $stage -le 2 ]] ;then
  echo -e "\ntrain bottleneck neural network\n";
  $cuda_cmd $dir/_train_nnet.log \
  isteps/tandem/train_nnet.sh --hid-layers $hid_layers \
  --temp-prefix $temp_prefix \
  --clean-temp $clean_temp \
  --hid-dim $hid_dim --bn-dim $bn1_output_dim --feat-type traps \
  --splice $bn1_input_ctx_width \
  --traps-dct-basis $traps_dct_basis \
  --learn-rate 0.008 \
  --bunch-size $bunch_size \
  --cache-size $cache_size \
  $train_data $dev_data $lang $train_ali $dev_ali $dir \
  || die "training bn failed";

fi
# compose feature transform with bn1
feature_transform=$dir/final.feature_transform.bn1;
if [ $stage -le 3 ]; then
  echo -e "\ncomposed bn1 to as a feature transform to bn2\n";
  [ -e $dir/final.nnet ] || \
  die "bn1 $dir/final.nnet does not exist";
  nnet-concat $dir/final.feature_transform \
  "nnet-copy --remove-last-layers=$remove_last_layers --binary=false $dir/final.nnet - |" \
      "utils/nnet/gen_splice.py --fea-dim=$bn1_output_dim --splice=$bn1_output_ctx_width --splice-step=$bn1_output_splice_step |" \
      $feature_transform

fi
# check to proceed
[ -e $feature_transform ] || \
die "feature_transform $feature_transform does not exist";
# train bottleneck neural network 2 as merger
bn2dir=$dir2;
if [ $stage -le 4 ] ;then
  echo -e "\nbn2 training started\n";
  $cuda_cmd $bn2dir/_train_nnet.log \
  isteps/tandem/train_nnet.sh --hid-layers $hid_layers \
  --temp-prefix $temp_prefix \
  --clean-temp $clean_temp \
  --hid-dim $hid_dim --bn-dim $bn2_output_dim \
   --feature-transform $feature_transform \
  --learn-rate 0.008 \
  --bunch-size $bunch_size \
  --cache-size $cache_size \
  $train_data $dev_data $lang $train_ali $dev_ali $bn2dir || exit 1;
fi
