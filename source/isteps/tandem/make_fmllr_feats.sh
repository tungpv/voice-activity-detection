#!/bin/bash

# Copyright 2012  Karel Vesely
#                 Johns Hopkins University (Author: Daniel Povey),
#                 
# Apache 2.0.

# This script is for use in neural network training and testing; it dumps
# (LDA+MLLT or splice+delta) + fMLLR features in a similar format to
# conventional raw MFCC features. 

# Begin configuration section.  
nj=4
cmd=run.pl
transform_dir=
norm_vars=false
delta_order=2;
delta_order2=0
feats1_lda_mllt=;
feats2_lda_mllt=;

feats1_transform_dir=;
feats2_transform_dir=;

# End configuration section.

echo "$0 $@"  # Print the command line for logging

[ -f ./path.sh ] && . ./path.sh; # source the path.
. parse_options.sh || exit 1;

if [ $# != 6 ]; then
   echo "Usage: $0 [options] <tgt-data-dir> <src-data-dir1> <src-data-dir2> <gmm-dir> <log-dir> <fea-dir>"
   echo "e.g.: $0 data-fmllr/train data/train exp/tri5a exp/make_fmllr_feats/log plp/processed/"
   echo ""
   echo "This script works on CMN + (delta+delta-delta | LDA+MLLT) features; it works out"
   echo "what type of features you used (assuming it's one of these two)"
   echo "You can also use fMLLR features-- you have to supply --transform-dir option."
   echo ""
   echo "main options (for others, see top of script file)"
   echo "  --config <config-file>                           # config containing options"
   echo "  --nj <nj>                                        # number of parallel jobs"
   echo "  --cmd (utils/run.pl|utils/queue.pl <queue opts>) # how to run jobs."
   echo "  --transform-dir <transform-dir>                  # where to find fMLLR transforms."
   exit 1;
fi
# NOTE: this script only works for the following two cases:
#  1) delta (plppp) + bn
#  2) lda+mllt(plpp) + bn
# we do this based on our preliminary observations that
# using some transformation on BN feature does not help.
# If you want it to do your case some adaptations have to be done
# Haihua

data=$1 ; shift
srcdata1=$1;
srcdata2=$2
gmmdir=$3
logdir=$4
feadir=$5



#srcdir=$1 -> gmmdir
#data=$2 -> srcdata
#dir=$3 -> ruzne
#tgtdata=$4 -> feadir
# nj=`cat $gmmdir/num_jobs`
sdata1=$srcdata1/split$nj;
sdata2=$srcdata2/split$nj;
splice_opts=`cat $gmmdir/splice_opts 2>/dev/null`

mkdir -p $data $logdir $feadir
[[ -d $sdata1 && $srcdata1/feats.scp -ot $sdata1 ]] || split_data.sh $srcdata1 $nj || exit 1;
[[ -d $sdata2 && $srcdata2/feats.scp -ot $sdata2 ]] || split_data.sh $srcdata2 $nj || exit 1;

for sdata in $sdata1 $data2; do
  for f in $sdata/1/feats.scp $sdata/1/cmvn.scp; do
    [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
  done
done

# if [ -f $gmmdir/final.mat ]; then feat_type=lda; else feat_type=delta; fi
# echo "$0: feature type is $feat_type";


feats1="ark,s,cs:apply-cmvn --norm-vars=$norm_vars --utt2spk=ark:$sdata1/JOB/utt2spk scp:$sdata1/JOB/cmvn.scp scp:$sdata1/JOB/feats.scp ark:- |";
feats2="ark,s,cs:apply-cmvn --norm-vars=$norm_vars --utt2spk=ark:$sdata2/JOB/utt2spk scp:$sdata2/JOB/cmvn.scp scp:$sdata2/JOB/feats.scp ark:- |";

# for feature one
if [ ! -z $feats1_lda_mllt ]; then
  echo  "$0: the first is lda feature";
  sdir=$feats1_lda_mllt;
  splice_opts=`cat $sdir/splice_opts`;
  feats1="$feats1 splice-feats $splice_opts ark:- ark:- | transform-feats $sdir/final.mat ark:- ark:- |"; 
else
  if [ $delta_order -gt 0 ]; then
    echo "$0: the first is delta feature";
    feats1="$feats1 add-deltas --delta-order=$delta_order ark:- ark:- |";
  fi
fi

# for feature two
if [ ! -z $feats2_lda_mllt ]; then
  echo "$0: the second is lda feature";
  sdir=$feats2_lda_mllt;
  splice_opts=`cat $sdir/splice_opts`;
  feats2="$feats2 splice-feats $splice_opts ark:- ark:- | transform-feats $sdir/final.mat ark:- ark:- |";
else
  if [ $delta_order2 -gt 0 ]; then
    echo "$0: the second is delta feature";
    feats2="$feats2 add-deltas --delta-order=$delta_order2 ark:- ark:- |";
  fi
fi
# for transform dir
if [ ! -z $feats1_transform_dir ]; then
  echo -e "\n## feats1_transform_dir is set\n";
  sdir=$feats1_transform_dir;
  snj=`cat $sdir/num_jobs`;
  [ $snj -ne $nj ] && echo "num_jobs for feats1 $snj is mismatched with $nj" && exit 1;
  feats1="$feats1 transform-feats --utt2spk=ark:$sdata1/JOB/utt2spk ark,s,cs:$sdir/trans.JOB ark:- ark:- |";
fi
if [ ! -z $feats2_transform_dir ]; then
  echo -e "\n## feats2_transform_dir is set\n";
  sdir=$feats2_transform_dir;
  snj=`cat $sdir/num_jobs`;
  [ $snj -ne $nj ]&& echo "num_jobs for feats2 $snj is mismatched with $nj" && exit 1;
  feats2="$feats2 transform-feats --utt2spk=ark:$sdata2/JOB/utt2spk ark,s,cs:$sdir/trans.JOB  ark:- ark:- |";
fi

echo -e "\n## feats1=$feats1\n"
echo -e "\n## feats2=$feats2\n"
# assemble tandem features
feats="ark,s,cs:paste-feats '$feats1' '$feats2' ark:- |";


#prepare the dir
cp $srcdata2/* $data; rm $data/{feats.scp,cmvn.scp} 2>/dev/null;

# make $bnfeadir an absolute pathname.
feadir=`perl -e '($dir,$pwd)= @ARGV; if($dir!~m:^/:) { $dir = "$pwd/$dir"; } print $dir; ' $feadir ${PWD}`

name=`basename $data`

#forward the feats
$cmd JOB=1:$nj $logdir/make_fmllr_feats.JOB.log \
  copy-feats "$feats" \
  ark,scp:$feadir/feats_fmllr_$name.JOB.ark,$feadir/feats_fmllr_$name.JOB.scp || exit 1;
   
#merge the feats to single SCP
for n in $(seq 1 $nj); do
  cat $feadir/feats_fmllr_$name.$n.scp 
done > $data/feats.scp

echo "$0 finished... $srcdata2 -> $data ($gmmdir)"
utils/fix_data_dir.sh $data || exit 1;
exit 0;
