#!/bin/bash

# koried, 2/13/2012

# Tandem acoustic model training template;  uses settings from config file.
# Models will be stored in features2/ directory (assuming that feature1 is
# the "constant" for all tandem systems


function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  loginfo=$1; shift;
  echo -e "@ `hostname` $loginfo `date` ";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
stage=0;
nj=20;
exp_dir=exp/tandem;
lang_dir=data/lang;
lang_test_dir=;
dev_data=;
devname=;
#
# training parameters;  mono-phone not configured
tp_s2_ns=2500
tp_s2_nd=25000   # tri/deltas, part_train2

tp_s3_ns=2500
tp_s3_nd=25000   # tri/deltas, part_train3

tp_so="--left-context=3 --right-context=3"
tp_s4_ns=2500
tp_s4_nd=25000   # tri/lda+mllt, full

tp_s5_ns=2500
tp_s5_nd=25000   # tri/lda+mllt+sat, full

tp_s6_ns=2500
tp_s6_nd=25000   # sgmm2/lda+mllt+sat, full
tp_s6_ubm=400
dim1=30;  

# end user-defined variable

. parse_options.sh || die "parse_options. sh expected";

if [ $# -ne 2 ]; then
   echo -e "\nUsage: $0 [options]  feat1 feat2\n";
   exit 1;
fi

feats1=$1; shift;
feats2=$1; shift;
train_data="$feats1 $feats2";
train_cmd=run.pl;
if [ $stage -le 1 ] ;then
  LOG "mono phone system training"; 
  steps/tandem/train_mono.sh --nj $nj --cmd "$train_cmd" \
    $train_data $lang_dir $exp_dir/mono0a || \
  die "step1:train_mono.sh"; 
fi
#
if [ $stage -le 3 ]; then
   LOG "triphone system training";
   steps/tandem/align_si.sh --nj $nj --cmd "$train_cmd" \
    $train_data $lang_dir \
    $exp_dir/mono0a $exp_dir/mono0a_ali || \
    die "@step3:steps/tandem/align_si.sh";

    steps/tandem/train_deltas.sh --cmd "$train_cmd" \
    $tp_s2_ns $tp_s2_nd $train_data \
    $lang_dir $exp_dir/mono0a_ali $exp_dir/tri1 || \
    die "@step3:steps/tandem/train_deltas.sh";
fi
# 
if [ $stage -le 5 ]; then
  LOG "triphone with lda+mllt";
  steps/tandem/align_si.sh --nj $nj --cmd "$train_cmd" \
  $train_data $lang_dir $exp_dir/tri1 $exp_dir/tri1_ali || \
  die "@step5:steps/tandem/align_si.sh";
  steps/tandem/train_lda_mllt.sh --cmd "$train_cmd" \
  --splice-opts "$tp_so" \
  --dim1  $dim1 \
  $tp_s4_ns $tp_s4_nd $train_data \
    $lang_dir $exp_dir/tri1_ali $exp_dir/tri3a || \
 die "@step5:steps/tandem/train_lda_mllt.sh";
fi
#
transform_dir=$exp_dir/tri4a/decode_$devname;
if [ $stage -le 7 ] ;then
  LOG "sat training";
  ali=$exp_dir/tri3a/train_ali;
  steps/tandem/align_si.sh --nj $nj --cmd "$train_cmd" \
    $train_data $lang_dir $exp_dir/tri3a $ali || \
  die "@step7: steps/tandem/align_fmllr.sh";
  mdldir=$exp_dir/tri4a;
  steps/tandem/train_sat.sh --cmd "$train_cmd" \
  $tp_s5_ns $tp_s5_nd $train_data $lang_dir $ali\
  $mdldir || \
  die "@step7: steps/tandem/train_sat.sh";
  if [[ ! -z "$dev_data" ]]; then
    utils/mkgraph.sh $lang_test_dir $mdldir \
    $mdldir/graph || \
    die "@step7: utils/mkgraph.sh";
    steps/tandem/decode_fmllr.sh \
    --nj $nj \
    --acwt 0.1 \
    --cmd "$train_cmd" \
    $mdldir/graph \
    $dev_data \
    $mdldir/decode_$devname || \
    die "@step7:steps/tandem/decode_fmllr.sh";
  fi 
fi

LOG "Done !"; exit 0;

[ ! -e $transform_dir/trans.1 ] && transform_dir=;
# 
if [ $stage -le 9 ]; then
  LOG "sgmm2 training";
  steps/tandem/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
  $train_data $lang_dir \
  $exp_dir/tri4a $exp_dir/tri4a_ali ||\
  die "@step9:steps/tandem/align_fmllr.sh" ;
  if [ ! -f $exp_dir/ubm5a/final.ubm ]; then
    echo `date` Training UBM
    steps/tandem/train_ubm.sh --cmd "$train_cmd" $tp_s6_ubm \
    $train_data $lang_dir \
    $exp_dir/tri4a_ali $exp_dir/ubm5a || \
    die "@step9:steps/tandem/train_ubm.sh";
  fi
  echo `date` Training SGMM system
  mdldir=$exp_dir/sgmm5a;
  steps/tandem/train_sgmm2.sh --cmd "$train_cmd" \
  $tp_s6_ns $tp_s6_nd $train_data $lang_dir \
  $exp_dir/tri4a_ali \
  $exp_dir/ubm5a/final.ubm $mdldir || \
  die "@step9:steps/tandem/train_sgmm2.sh";
fi
#
if [ $stage -le 10 ]; then
  # do evaluation
  mdldir=$exp_dir/sgmm5a;
  if [[ ! -z "$dev_data" ]]; then
    if [ ! -e $mdldir/graph/HCLG.fst ]; then
      utils/mkgraph.sh $lang_test_dir $mdldir \
      $mdldir/graph || \
      die "@step10: utils/mkgraph.sh";
    fi
    isteps/tandem/decode_sgmm2.sh --nj $nj \
    --cmd "$train_cmd" \
    ${transform_dir:+ --transform-dir $transform_dir} \
    $mdldir/graph \
    $dev_data $mdldir/decode_$devname $mdldir || \
    die "@step10:isteps/tandem/decode_sgmm2.sh";
  fi
fi

exit 0;
#
if [ $stage -le 11 ]; then
   echo `date` Aligning training data with SGMM
   steps/tandem/align_sgmm2.sh --nj $nj --cmd "$train_cmd" \
   --use-graphs true --use-gselect true \
   --transform-dir $exp_dir/tri4a_ali \
   $train_data $lang_dir $exp_dir/sgmm5a \
   $exp_dir/sgmm5a_ali || \
   die "@step11:steps/tandem/align_sgmm2.sh";
    
   # Took the beam down to 8 to get acceptable decoding speed.
   echo `date` Making dense lattices
   steps/tandem/make_denlats_sgmm2.sh --nj $nj \
   --beam 13 \
   --lattice-beam 7.0 \
   --max-mem $[20000000*10] \
   --cmd "$decode_cmd" \
   --transform-dir $exp_dir/tri4a_ali \
   $train_data $lang_dir $exp_dir/sgmm5a_{ali,denlats} \
   || die "@step11:steps/tandem/make_denlats_sgmm2.sh";

   echo `date` Starting SGMM MMI training
   steps/tandem/train_mmi_sgmm2.sh --cmd "$decode_cmd" --boost 0.1 \
   --transform-dir $exp_dir/tri4a_ali \
   $train_data $lang_dir $exp_dir/sgmm5a_{ali,denlats,mmi_b0.1} \
   || die "@step11:steps/tandem/train_mmi_sgmm2.sh";
   # we will do model evaluation later
fi

LOG "finished training";
