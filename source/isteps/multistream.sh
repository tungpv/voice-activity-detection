#!/bin/bash

# koried, 2/13/2012

# Tandem acoustic model training template;  uses settings from config file.
# Models will be stored in features2/ directory (assuming that feature1 is
# the "constant" for all tandem systems


function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function LOG {
  loginfo=$1; shift;
  echo -e "@ `hostname` $loginfo `date` ";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
# begin user-defined variable
step=0;
nj=20;
cmd=run.pl;
lda_dim=70;
nstate=2500;
ngauss=25000;
exp_dir=exp/mstream;
lang_dir=data/lang;
lang_test_dir=;
dev_data=;
devname=;
#
# training parameters;  mono-phone not configured
tp_s2_ns=2500
tp_s2_nd=25000   # tri/deltas, part_train2

tp_s3_ns=2500
tp_s3_nd=25000   # tri/deltas, part_train3

splice_opts="--left-context=3 --right-context=3"
tp_s4_ns=2500
tp_s4_nd=25000   # tri/lda+mllt, full

tp_s5_ns=2500
tp_s5_nd=25000   # tri/lda+mllt+sat, full

tp_s6_ns=2500
tp_s6_nd=25000   # sgmm2/lda+mllt+sat, full
tp_s6_ubm=400
dim1=30;  

# end user-defined variable

. parse_options.sh || die "parse_options. sh expected";
stream_num=$#;
[ $stream_num -gt 1 ] || \
die "stream_num $stream_num is not bigger than 1";
lang=$lang_dir;
lang_test=$lang_test_dir;
echo -e "\nstream_num=$stream_num\n";
for idx in `seq 0 $[stream_num-1]`;do
  # echo $1; shift;
  array_stream[$idx]=$1; shift;
done
# re-assemble
train_data=;
for idx in `seq 0 $[stream_num-1]`; do
  train_data="$train_data ${array_stream[$idx]}";
done
echo -e "\ntrain_data=$train_data\n";

if [ $step -le 1 ]; then
  LOG "mono phone system training"; 
  steps/multistream/train_mono.sh --nj $nj --cmd "$cmd" \
    $train_data $lang_dir $exp_dir/mono0a || \
  die "step1:train_mono.sh"; 
 
fi
# mono-phone alignment
if [ $step -le 2 ]; then
  steps/multistream/align_si.sh --boost-silence 1.25 --nj $nj \
  --cmd "$cmd" \
  $train_data $lang $exp_dir/mono0a $exp_dir/mono0a_ali || \
   die "step2:steps/multistream/align_si.sh";
fi
#  train triphone
tri1_ali=$exp_dir/tri1/train_ali;
if [ $step -le 3 ]; then
  echo -e "\n delta triphone training\n";
  steps/multistream/train_deltas.sh --boost-silence 1.25 \
  --cmd "$cmd" \
  $nstate $ngauss \
  $train_data $lang  $exp_dir/mono0a_ali $exp_dir/tri1 \
  || die "@step3: steps/multistream/train_deltas.sh";
fi
# tri1 alignment
if [ $step -le 4 ]; then
  steps/multistream/align_si.sh --nj $nj --cmd "$cmd" \
  $train_data  $lang $exp_dir/tri1  $tri1_ali || exit 1;
fi

# train ldat+mllt
ldamlltmdl=$exp_dir/tri2;
if [ $step -le 5 ]; then
  echo -e "\nlda+mllt training\n";
  steps/multistream/train_lda_mllt.sh --cmd "$cmd" \
   --dim $lda_dim \
   --splice-opts "$splice_opts" \
  $nstate $ngauss $train_data  $lang $tri1_ali $exp_dir/tri2 \
  || die "@step5:steps/multistream/train_lda_mllt.sh";
  
fi
# lda+mllt alignment
tri2_ali=$exp_dir/tri2/train_ali;
if [ $step -le 6 ]; then
  echo -e "\nlda+mllt alignment\n";
  steps/multistream/align_si.sh  --nj $nj --cmd "$cmd" \
  --use-graphs true $train_data \
  $lang $exp_dir/tri2  $tri2_ali  ||\
  die "@step6:steps/multistream/align.sh";
fi
# train sat model
satmdl=$exp_dir/tri3sat;
if [ $step -le 7 ]; then
  echo -e "\n@step7: sat training\n";
  steps/multistream/train_sat.sh --cmd "$cmd" \
  $nstate $ngauss $train_data \
  $lang  $tri2_ali $satmdl ||\
  die "@step7: steps/multistream/train_sat.sh";
fi
# evaluate lda + mllt model
array_dev=($dev_data);
x=${array_dev[0]};
devname=`basename $x`;
if [ $step -le 8 ]; then
  echo -e "\n@step8: evaluate the lda+mllt training\n";
  sdir=$ldamlltmdl;
  # make graph
  if [ ! -e $graphdir/HCLG.fst ] ; then
    utils/mkgraph.sh $lang_test $sdir \
    $sdir/graph ||\
    die "@step8: utils/mkgraph.sh";
  fi
  steps/multistream/decode.sh --nj $nj \
  --cmd "$cmd" \
  $sdir/graph  $dev_data  $sdir/decode_$devname \
  || die "@step8: steps/multistream/decode.sh";
fi
# evaluate sat fmllr model
if [ $step -le 9 ]; then
  echo -e "\n @step9: evaluate the sat+fmllr training";
  sdir=$satmdl;
  # make graph
  if [ ! -e $graphdir/HCLG.fst ] ; then
    utils/mkgraph.sh $lang_test $sdir \
    $sdir/graph ||\
    die "@step9: utils/mkgraph.sh";
  fi
  steps/multistream/decode_fmllr.sh --nj $nj \
  --cmd "$cmd" \
  $sdir/graph  $dev_data  $sdir/decode_$devname \
  || die "@step9: steps/multistream/decode_fmllr.sh";
fi

