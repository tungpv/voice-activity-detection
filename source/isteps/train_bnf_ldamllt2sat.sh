#!/bin/bash

. path.sh || exit 1 
. cmd.sh  || exit 1

echo -e "\n## LOG: $0 $@\n";

# begin options
bnndir=
cmd=run.pl
norm_vars=false
# end options

. parse_options.sh
if [ $# -ne 5 ]; then
   echo -e "\n\nExample: $0 lang_conf lang ali sdata expdir\n\n";
   exit 1
fi
lang_conf=$1; shift
lang=$1; shift
ali=$1; shift
sdata=$1; shift;
expdir=$1; shift;


. $lang_conf ||  exit 1
. conf/common_vars.sh || exit 1
sname=`basename $sdata`
feature=$expdir/feature;
if [ -z $feature ]; then echo -e "\n## ERROR: $feature is not ready\n"; exit 1; fi

fbdata=$sdata/fbank/$sname; fbfeat=$feature/fbank/$sname;
if [ ! -f $fbdata/.fb_done ]; then
  [ -d $fbdata ] || mkdir -p $fbdata
  cp $sdata/* $fbdata; rm $fbdata/{feats.scp,cmvn.scp}
  nj=$(wc -l < $fbdata/spk2utt); [ $nj -gt 30 ] && nj=30;
  echo -e "\n## making fb feature `hostname` `date`\n";
  steps/make_fbank_pitch.sh  --nj $nj --cmd "$cmd"  $fbdata $fbfeat/_log $fbfeat/_data || exit 1
  steps/compute_cmvn_stats.sh $fbdata $fbfeat/_log $fbfeat/_cmvn || exit 1
  touch $fbdata/.fb_done
fi

nj=$(wc -l < $fbdata/spk2utt); [ $nj -gt 30 ] && nj=30;
if [ -z $nj ]; then echo -e "\n## num_jobs is not specified in $fbdata\n"; exit 1; fi

bnfdata=$sdata/sbnf/$sname; bnffeat=$feature/sbnf/$sname
if [ ! -f $bnfdata/.bnf_done ]; then 
  if [ -z $bnndir ]; then echo -e "\n## ERROR: BN-NN folder $bnndir is not ready\n"; exit 1; fi
  echo -e "\n## making bottle-neck feature `date`\n";
  isteps/imake_bn_feats.sh --nj $nj --cmd "$cmd" \
  $bnfdata $fbdata $bnndir  $bnffeat/_log $bnffeat/_data || exit 1
  touch $bnfdata/.bnf_done
fi

train=$bnfdata; exp=$expdir/tri4; 

if [ ! -f $exp/.train_done  ]  ; then
  if [ ! -f $exp/final.mdl ]; then
    echo -e "\n## train_lda_mllt started `date`\n";
    steps/train_lda_mllt.sh --norm-vars $norm_vars --boost-silence $boost_sil --cmd "$cmd" \
    $numLeavesMLLT $numGaussMLLT $train $lang $ali $exp || exit 1
  fi
  nj=$(cat $exp/num_jobs)
  steps/align_si.sh --nj $nj --cmd "$cmd" \
  --use-graphs true $train  $lang $exp  $exp/ali_$sname  || exit 1;
  touch $exp/.train_done
fi

ali=$exp/ali_$sname; exp=$expdir/tri5;
if [ ! -f $exp/.train_done ]; then
  if [ ! -f $exp/final.mdl ]; then
    steps/train_sat.sh  \
    --boost-silence $boost_sil --cmd "$cmd" \
    $numLeavesSAT $numGaussSAT $train $lang $ali $exp || exit 1

  fi
  nj=$( x=`wc -l < $train/spk2utt`; [ $x -gt 30 ] && x=30; echo $x;)
  steps/align_fmllr.sh  --boost-silence $boost_sil --nj $nj --cmd "$cmd" \
    $train $lang $exp $exp/ali_$sname || exit 1
  touch $exp/.train_done
fi
