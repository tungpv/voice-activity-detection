#!/bin/bash

# wrapper to call make_bn_feats.sh in kaldi

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. ilocal/bn/path.sh || die "path.sh expected";
. ilocal/bn/cmd.sh || die "cmd.sh expected";
# begin user-defined variable
doit=false;
stage=0;
nj=80;
fbfeature=false;
cmd="run.pl";
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";

data=$1; shift;
srcdata=$1; shift;
nndir=$1; shift;
logdir=$1; shift;
bnfeadir=$1; shift;

# copy the dataset metadata from srcdata.

if $fbfeature; then
  echo -e "\n Prepare filter-bank feature \n";
  sdata=$data/fbank;
  mkdir -p $sdata;
  cp $srcdata/* $sdata 2>/dev/null;
  ( rm $sdata/feats.scp $sdata/cmvn.scp ) 2>/dev/null;
  
  steps/make_fbank.sh --cmd "$cmd" --nj $nj \
  $sdata $sdata/_log $sdata/_data || exit 1;
  steps/compute_cmvn_stats.sh $sdata $sdata/_log $sdata/_data
  srcdata=$sdata;
fi

rm $data/.done 2>/dev/null;
isteps/make_bn_feats.sh --cmd "$cmd" \
 --nj $nj $data $srcdata $nndir $logdir \
 $bnfeadir || exit 1;
# compute CMVN of the BN-features
steps/compute_cmvn_stats.sh $data $logdir \
$bnfeadir || exit 1;

touch $data/.done;
echo -e "done bn feature extraction on `hostname` @ `date`";
exit 0;

