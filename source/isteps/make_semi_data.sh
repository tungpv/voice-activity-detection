#!/bin/bash

. path.sh
. cmd.sh


echo -e "\n ## Design: isteps/make_semi_data.sh --conf-thresh 0.4 --silence-dict data/local/silence-dict.txt  --manual manual_data lang  asr_data source_dir asr_latdir  semi_data\n"
echo -e "\n## LOG: $0 $@\n"


# begin user options
# end user options
# set -e           #Exit on non-zero return code from any command
set -o pipefail  #Exit if any of the commands in the pipeline will 
                 #return non-zero return code
# begin options
cmd=run.pl
conf_thresh=0.4
silence_words=
lmwt=10
manual=
silence_opts=
max_expand=20
# end options

. parse_options.sh

if [ $# -ne 5 ]; then
  echo -e "\n## Usage: $0 [options] lang data srcdata latdir dir\n"; exit 0
fi

lang=$1; shift
data=$1; shift
srcdir=$1; shift
latdir=$1; shift
dir=$1; shift

nj=`cat $latdir/num_jobs`

if [[ -z $nj || ! -f $latdir/lat.1.gz ]]; then
  echo -e "\n## lattice is expected in latdir $latdir\n"; exit 1
fi

stt=$latdir/stt
wconf=$stt/wconf
uconf=$stt/uconf

for x in $wconf $uconf; do
  [ -d $x ] || mkdir -p $x
done

if [ ! -f $stt/.conf_done ]; then
  word_boundary=$lang/phones/word_boundary.int
  if [ -f $word_boundary ]; then
    model=$srcdir/final.mdl;
    if [[ -z "silence_opts" && -e $lang/silence_label  ]] ;then
      silence_opts="--silence-label=$(cat $lang/silence_label)"
    fi
    $cmd JOB=1:$nj $wconf/log/posts.JOB.log \
    lattice-align-words $silence_opts --max-expand=$max_expand $word_boundary $model  "ark:gzip -cdf $latdir/lat.JOB.gz|" ark:- \| \
    lattice-to-word-post --inv-acoustic-scale=$lmwt ark:- "|gzip -c >$wconf/post.JOB.gz" || exit 1

  else 
    echo -e "\n## word_boundary.int file $word_boundary is not seen, no lattice-align-words operation used\n"
    $cmd JOB=1:$nj $wconf/log/posts.JOB.log \
    lattice-to-word-post --inv-acoustic-scale=$inv_ascale ark:- "|gzip -c >$wconf/post.JOB.gz" || exit 1
  fi
  if [[ -z $silence_words || ! -f $silence_words ]]; then
    echo -e "\n ## silence_word list is not specified or does not exist\n";
  fi
  $cmd JOB=1:$nj $uconf/log/uconf.JOB.log iutils/um/utt_silword_timerate.pl  ${silence_words:+ --silence $silence_words} \
 "gzip -cdf $wconf/post.JOB.gz" "gzip -c > $uconf/uconf.JOB.gz" \
 '>' $uconf/silrate.JOB.txt  || exit 1

  touch $stt/.conf_done 
fi

symtab=$lang/words.txt
if [ ! -f $stt/.onebest_done ]; then
  echo -e "\n## get one best results `date`\n"
  if [ -z $silence_words ]; then
    if [ ! -e $lang/silence_words.int ]; then
      echo -e "\n## no silence_words.int in $lang\n";
      echo > $lang/silence_words.int
    else
      silence_words=$lang/silence_words.int
    fi
  fi
  $cmd LMWT=$lmwt:$lmwt $stt/log/best_path.LMWT.log \
  lattice-best-path --lm-scale=LMWT --word-symbol-table=$symtab \
    "ark:gunzip -c $latdir/lat.*.gz|" "ark,t:|iutils/filter_utt.pl $silence_words 2> $stt/filtered_text.log |utils/int2sym.pl -f 2- $symtab >$stt/LMWT.tra" || exit 1;

  touch $stt/.onebest_done
fi
function  Filtering {
  src=$1; shift;   
  filter=$1; shift;
  dest=$1;
 cat $src | \
 perl -e '$f = shift @ARGV; open F, "$f" or die "file $f cannot open\n"; 
   while (<F>) { chomp; m/(\S+)/ or next; $vocab{$1} ++;  } close F;
   while(<STDIN>) { chomp; m/(\S+)/ or next; if (exists $vocab{$1}){print "$_\n";}  }'  $filter  > $dest
}
uttconf="gzip -cdf $uconf/uconf.*.gz|"; tgtdir=$dir;  
dir=$stt/${dataname}_${conf_thresh};
if [ $(wc -l < $stt/$lmwt.tra) -gt 0 ]; then
  dataname=$(basename $data);  
  isteps/um/mksubset_dir.sh --include $stt/$lmwt.tra  $data $dir || exit 1
  iutils/um/conf_utt_select.pl --thresh $conf_thresh \
  --selected $dir/tmpf  "$uttconf" $dir/segments  \
  >$dir/uttconfs 
  mv $dir/tmpf $dir/segments
  # use unsupervised text
  Filtering $stt/$lmwt.tra $dir/segments $dir/text
  utils/fix_data_dir.sh $dir
  echo -e "\n## selected data have $(cat $dir/segments | awk '{x += $4 -$3;}END{print x/3600;}') hours in $dir\n"
fi

if [[ ! -z $manual  && -f $dir/text  ]]; then
  echo -e "\n## making semi data $tgtdir  `date`\n"
  iutils/um/merge_data_dir.sh $dir $manual $tgtdir || exit 1
fi
