#!/bin/bash

# Copyright 2013  Haihua Xu, Korbinian Riedhammer 

# Convert the lattices from a decoding run to HTK SLF format (to share with
# other sites or applications).

function die {
  echo -e "\nERROR: $1 ...\n"; exit 0;
}

# begin user-defined variable
cmd=slocal/slurm4.pl
nj=4
# end user-defined variable

. path.sh || die "path.sh expected"
. cmd.sh || die "cmd.sh expected"
. parse_options.sh || die "parse_options.sh expected";


latdir=exp/nnet6b/decode_dev
lang=data/lang
data=/data/dev
dir=$latdir/slf_lat

[ -e $lang/words.txt ] || \
die "lang/words.txt file is not ready";
words=$lang/words.txt
[ -e $lang/phones/word_boundary.int  ] || \
die "word_boundary.txt is not ready in lang dir $lang/phones";
word_boundary=$lang/phones/word_boundary.int
[ -e $latdir/lat.1.gz ] || \
die "no lattice file in latdir $latdir";
srcdir=`dirname $latdir`
[ -e $srcdir/final.mdl ] || \
die "no final.mdl in folder $srcdir"
mdl=$srcdir/final.mdl

nj=`cat $latdir/num_jobs`;
[ ! -z $nj ] || die "num_jobs is unseen";
[ -d $dir/log ] || mkdir -p $dir/log


$cmd JOB=1:$nj $dir/log/slf_lat.JOB.log \
lattice-align-words $word_boundary  $mdl \
 "ark:gzip -cdf $latdir/lat.JOB.gz |"  "ark,t:-" \| \
  utils/int2sym.pl -f 3 $words \| \
  utils/convert_slf.pl - $dir

