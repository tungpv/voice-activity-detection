#!/bin/bash

# Copyright 2012  Korbinian Riedhammer 

# Convert the lattices from a decoding run to HTK SLF format (to share with
# other sites or applications).

hescii=true
transform_dir=
nj=4
cmd=run.pl
latbase=lat
# End configuration section

echo "$0 $@"  # Print the command line for logging

[ -f ./path.sh ] && . ./path.sh; # source the path.
. parse_options.sh || exit 1;

if [ $# != 4 ]; then
   echo "Usage: steps/tandem/mk_aslf_sgmm2.sh [options] <graph-dir> <data-dir> <decode-dir> <slf-out-dir>"
   echo " e.g.: steps/tandem/mk_aslf_sgmm2.sh exp/tri2b/graph {mfcc,bottleneck}/data/test_dev93 exp/tri2b/decode_dev93"
   echo "main options (for others, see top of script file)"
   echo "  --config <config-file>                   # config containing options"
   echo "  --nj <nj>                                # number of parallel jobs"
   echo "  --cmd <cmd>                              # Command to run in parallel with"
   echo "  --transform-dir dir                      # path to directory with transforms to use"

   exit 1;
fi


graphdir=$1
data=$2
dir=`echo $3 | sed 's:/$::g'` # remove any trailing slash.
outdir=`echo $4 | sed 's:/$::g'`

srcdir=`dirname $dir`; # Assume model directory one level up from decoding directory.
final_model=$srcdir/final.mdl

mkdir -p $dir/log

sdata=$data/split$nj;
[[ -d $sdata && $data/feats.scp -ot $sdata ]] || split_data.sh $data $nj || exit 1;

echo $nj > $dir/num_jobs

# Some checks.  Note: we don't need $srcdir/tree but we expect
# it should exist, given the current structure of the scripts.
for f in $graphdir/HCLG.fst $data/feats.scp; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

## Some checks, and setting of defaults for variables.
[ "$nj" -ne "`cat $dir/num_jobs`" ] && echo "Mismatch in #jobs with si-dir" && exit 1;
for f in $adapt_model $final_model; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done
##


# Set up features.

splice_opts=`cat $srcdir/splice_opts 2>/dev/null` # frame-splicing options.

if [ -f $srcdir/final.mat ]; then feat_type=lda; else feat_type=delta; fi

case $feat_type in
  delta) 
  	echo "$0: feature type is $feat_type"
  	;;
  lda) 
  	echo "$0: feature type is $feat_type"
    ;;
  *) echo "$0: invalid feature type $feat_type" && exit 1;
esac

# set up feature stream 1;  this are usually spectral features, so we will add
# deltas or splice them
feats="ark,s,cs:apply-cmvn --norm-vars=false --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- |"

if [ "$feat_type" == "delta" ]; then
  feats="$feats add-deltas ark:- ark:- |"
elif [ "$feat_type" == "lda" ]; then
  feats="$feats splice-feats $splice_opts ark:- ark:- | transform-feats $srcdir/final.mat ark:- ark:- |"
fi

# assemble tandem features
sifeats="$feats"

if [ -e $dir/trans.1. ]; then
  echo "Using fMLLR transforms in $dir"
  feats="$sifeats transform-feats --utt2spk=ark:$sdata/JOB/utt2spk ark:$dir/trans.JOB ark:- ark:- |"
elif [ ! -z "$transform_dir" ]; then
  echo "$0: using transforms from $transform_dir"
  [ ! -f $transform_dir/trans.1 ] && echo "$0: no such file $transform_dir/trans.1" && exit 1;
  [ "$nj" -ne "`cat $transform_dir/num_jobs`" ] \
    && echo "$0: #jobs mismatch with transform-dir." && exit 1;
  feats="$sifeats transform-feats --utt2spk=ark:$sdata/JOB/utt2spk ark,s,cs:$transform_dir/trans.JOB ark:- ark:- |"
elif grep 'transform-feats --utt2spk' $srcdir/log/acc.0.1.log 2>/dev/null; then
  echo "$0: **WARNING**: you seem to be using an SGMM system trained with transforms,"
  echo "  but you are not providing the --transform-dir option in test time."
fi


# Rescore the state-level lattices with the final adapted features, and the final model
# (which by default is $srcdir/final.mdl, but which may be specified on the command line,
# useful in case of discriminatively trained systems).
# At this point we prune and determinize the lattices and write them out, ready for 
# language model rescoring.

if [ $hescii ]; then
  if [ -f $graphdir/words.hescii.txt ];  then
    words=$graphdir/words.hescii.txt
  else
    # try to get it from the lang dir
    langdir=`basename $graphdir`
    lwords=data/${langdir#graph_}/words.hescii.txt
    if [ -f $lwords ]; then
      words=$lwords
    else
      echo "WARNING: no such file $words or $lwords, resetting to $graphdir/words.txt"
      words=$graphdir/words.txt
    fi
  fi
else
  words=$graphdir/words.txt
fi

echo "Rescoring lattices, converting to slf"
mkdir -p $outdir
$cmd JOB=1:$nj $dir/log/rescore.slf.JOB.log \
  lattice-align-words $graphdir/phones/word_boundary.int $final_model "ark:gunzip -c $dir/${latbase}.JOB.gz |" ark:- \| \
  sgmm2-rescore-lattice --spk-vecs=ark:$dir/vecs.JOB --utt2spk=ark:$sdata/JOB/utt2spk \
      "--gselect=ark:gunzip -c $dir/gselect.JOB.gz |" $final_model ark:- "$feats" ark,t:- \| \
  utils/int2sym.pl -f 3 $words \| \
  utils/convert_slf.pl - $outdir

exit 0;

