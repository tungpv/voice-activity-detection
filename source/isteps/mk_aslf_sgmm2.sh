#!/bin/bash

# Copyright 2012  Johns Hopkins University (Author: Daniel Povey).  Apache 2.0.

# This script does decoding with an SGMM system, with speaker vectors. 
# If the SGMM system was
# built on top of fMLLR transforms from a conventional system, you should
# provide the --transform-dir option.

# Begin configuration section.
stage=1
transform_dir=    # dir to find fMLLR transforms.
nj=4 # number of decoding jobs.
acwt=0.1  # Just a default value, used for adaptation and beam-pruning..
cmd=run.pl
beam=13.0
gselect=15  # Number of Gaussian-selection indices for SGMMs.  [Note:
            # the first_pass_gselect variable is used for the 1st pass of
            # decoding and can be tighter.
first_pass_gselect=3 # Use a smaller number of Gaussian-selection indices in 
            # the 1st pass of decoding (lattice generation).
max_active=7000
max_arcs=-1

#WARNING: This option is renamed lat_beam (it was renamed to follow the naming 
#         in the other scripts
lattice_beam=6.0 # Beam we use in lattice generation.
vecs_beam=4.0 # Beam we use to prune lattices while getting posteriors for 
    # speaker-vector computation.  Can be quite tight (actually we could
    # probably just do best-path.
use_fmllr=false
fmllr_iters=10
fmllr_min_count=1000
num_threads=1 # if >1, will use gmm-latgen-faster-parallel
parallel_opts=  # If you supply num-threads, you should supply this too.
skip_scoring=false
scoring_opts=
# note: there are no more min-lmwt and max-lmwt options, instead use
# e.g. --scoring-opts "--min-lmwt 1 --max-lmwt 20"
# End configuration section.

echo "$0 $@"  # Print the command line for logging

[ -f ./path.sh ] && . ./path.sh; # source the path.
. parse_options.sh || exit 1;

if [ $# -ne 4 ]; then
  echo "Usage: $0 [options] <graph-dir> <data-dir> <latdir> <slf-dir>";
  exit 1;
fi
graphdir=$1; shift;
data=$1;  shift;
latdir=$1; shift;
dir=$1; shift;

srcdir=`dirname $latdir`
[ ! -e $srcdir/final.mdl ] && \
echo "final.mdl expected in source folder" && exit 1;
final_model=$srcdir/final.mdl;
nj=`cat $latdir/num_jobs`
[[ -z $nj  ]] && echo "num_jobs file expected" && exit 1;

for f in $graphdir/HCLG.fst $data/feats.scp $srcdir/final.mdl; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done
sdata=$data/split$nj;
gselect_opt="--gselect=ark,s,cs:gunzip -c $latdir/gselect.JOB.gz|"

mkdir -p $dir/log
[[ -d $sdata && $data/feats.scp -ot $sdata ]] || split_data.sh $data $nj || exit 1;
echo $nj > $dir/num_jobs
splice_opts=`cat $srcdir/splice_opts 2>/dev/null` # frame-splicing options.

## Set up features.
if [ -f $srcdir/final.mat ]; then feat_type=lda; else feat_type=delta; fi
echo "$0: feature type is $feat_type"

case $feat_type in
  delta) feats="ark,s,cs:apply-cmvn --norm-vars=false --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- | add-deltas ark:- ark:- |";;
  lda) feats="ark,s,cs:apply-cmvn --norm-vars=false --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- | splice-feats $splice_opts ark:- ark:- | transform-feats $srcdir/final.mat ark:- ark:- |"
    ;;
  *) echo "$0: invalid feature type $feat_type" && exit 1;
esac
if [ ! -z "$transform_dir" ]; then
  [ "$nj" -ne "`cat $transform_dir/num_jobs`" ] \
    && echo "$0: #jobs mismatch with transform-dir." && exit 1;
  if [ -f $transform_dir/trans.1 ]; then
    echo "$0: using transforms from $transform_dir"
    feats="$feats transform-feats --utt2spk=ark:$sdata/JOB/utt2spk ark,s,cs:$transform_dir/trans.JOB ark:- ark:- |"
  elif [ -f $transform_dir/raw_trans.1 ]; then
    feats="ark,s,cs:apply-cmvn --norm-vars=false --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- | transform-feats --utt2spk=ark:$sdata/JOB/utt2spk ark,s,cs:$transform_dir/raw_trans.JOB ark:- ark:- | splice-feats $splice_opts ark:- ark:- | transform-feats $srcdir/final.mat ark:- ark:- |"    
  else
    echo "$0: no such file $transform_dir/trans.1 or $transform_dir/raw_trans.1, invalid --transform-dir option?"
    exit 1;
  fi
elif grep 'transform-feats --utt2spk' $srcdir/log/acc.0.1.log 2>/dev/null; then
  echo "$0: **WARNING**: you seem to be using an SGMM system trained with transforms,"
  echo "  but you are not providing the --transform-dir option in test time."
fi
##
if $use_fmllr; then
  feats="$feats transform-feats --utt2spk=ark:$sdata/JOB/utt2spk ark,s,cs:$latdir/trans.JOB ark:- ark:- |"  

fi

 rm $dir/.done 2>/dev/null
# Rescore the state-level lattices with the final adapted features, and the final model
# (which by default is $srcdir/final.mdl, but which may be specified on the command line,
# useful in case of discriminatively trained systems).
# At this point we prune and determinize the lattices and write them out, ready for 
# language model rescoring.

echo "Rescoring lattices, converting to slf"
$cmd JOB=1:$nj $dir/log/rescore.slf.JOB.log \
lattice-align-words $graphdir/phones/word_boundary.int $final_model \
"ark:gunzip -c $latdir/lat.JOB.gz |" ark:- \| \
sgmm2-rescore-lattice --spk-vecs=ark:$latdir/vecs.JOB --utt2spk=ark:$sdata/JOB/utt2spk \
    "$gselect_opt" $final_model ark:- "$feats" ark,t:- \| \
  utils/int2sym.pl -f 3 $graphdir/words.hescii.txt \| \
  utils/convert_slf.pl - $dir

touch $dir/.done 
exit 0;

