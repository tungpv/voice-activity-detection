#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Log {
  echo -e "\n`date`:$1 @ `hostname`\n";
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";

echo -e "\n$0 $@\n";

# begin user-defined variable
cmd=run.pl;
doit=false;
mixdata=false;     # if mixdata is true, then it means udata in the following
                   # is composed of supervised and unsupervised data simultaneously
cutoff=0;     # threshold to select decode utterance
inv_ascale=12;
remove_feats_cmvn=false;
uconfdir=;
utext=;
uconf=;
stage=0;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";
if [ $# -ne 6 ]; then
  echo -e "\n\nUsage:\n $0 [options] \n";
  echo "<lang>       # kaldi lang folder";
  echo "<udata>      # unsupervised data";
  echo "<ulat_dir>   # decoded lattice folder for unsupervised data";
  echo "<sdata>      # supervised data, which will be mixed with udata to update models";
  echo "<uasr_trans> # transcripts by asr for unsupervised data";
  echo "<semidir>    # data  mixture of the supervised and the unsupervised part";
  echo -e "\n\n";
fi

lang=$1; shift;
udata=$1; shift;
ulat_dir=$1; shift;
sdata=$1; shift;
uasr_trans=$1; shift;
semidir=$1; shift;
# dec_logdir=$1; shift;

tmpdir=$semidir/usup;
[ -d $tmpdir ] || \
mkdir -p $tmpdir;
rm $tmpdir/* 2>/dev/null

uconf=$tmpdir/uconf_asr.txt;
rm $uconf 2>/dev/null;
utext=$tmpdir/text_asr;
rm $utext 2>/dev/null;
if [ -z $uconfdir ]; then
  uconfdir=$ulat_dir/uconf;
fi
[ -e $uconfdir/uconf.1.gz ] || \
die "uconf folder is not ready";

# format unsupervised into kaldi text format
[ -e $uasr_trans ] || \
die "uasr_trans $uasr_trans is not ready";

rm $semidir/.done 2>/dev/null
scoredir=`dirname $uasr_trans`;
rm $scoredir/filter_utt.log 2>/dev/null;
nonspeech_words=$lang/non_speech.int
[ -e $nonspeech_words ] || \
die "nonspeech_words file is not ready in lang folder";
cat $uasr_trans | \
iutils/filter_utt.pl $nonspeech_words 2>$scoredir/filter_utt.log | \
utils/int2sym.pl -f 2- $lang/words.txt > $utext;

if [ $stage -le 1 ]; then
  # add utterance confidence info to corresponding data
  gzip -cdf $uconfdir/uconf.*.gz | \
  awk '{printf "%s %.4f\n", $1 ,$2;}' | sort >$uconf; 
fi
# remove the supervised part for the following merge
if  $mixdata ; then
  echo -e "\ndata are mixed\n";
  iutils/um/diff_segments.pl $sdata/segments \
  $udata/segments > $tmpdir/segments_unsup;
  iutils/um/select_invocab.pl $tmpdir/segments_unsup \
  $uconf > $tmpdir/uconf_unsup.txt;
  uconf=$tmpdir/uconf_unsup.txt;
  iutils/um/select_invocab.pl $uconf $utext > $tmpdir/text_unsup;
  utext=$tmpdir/text_unsup;
fi
# if cutoff is set
if [ `echo "$cutoff > 0" | bc -l` -eq 1 ]; then
  echo -e "cutoff=$cutoff";
  iutils/um/cut_uconf.pl $cutoff $uconf > $tmpdir/uconf_$cutoff.txt
  uconf=$tmpdir/uconf_$cutoff.txt;
  iutils/um/select_invocab.pl $uconf $utext > $tmpdir/text_$cutoff.txt
  utext=$tmpdir/text_$cutoff.txt
fi
isteps/um/make_udata_dir.sh $utext  $udata $tmpdir
iutils/um/merge_data_dir.sh $tmpdir $sdata $semidir

# update utterance confidence list
iutils/um/make_semi_uconf.pl $uconf $sdata/segments | \
sort > $semidir/uconf.txt

if $remove_feats_cmvn; then
  rm $semidir/{feats.scp,cmvn.scp} 2>/dev/null
fi 
echo -e "\nDone with semidata preparation @`hostname` `date`!\n";

touch $semidir/.done;
exit 0; 
  
