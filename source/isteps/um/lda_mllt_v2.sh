#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\ninput: $0 $@\n";
# begin user-defined varialbe
nj=20
# end user-defined variable
. parse_options.sh || die "parse_options.sh expected";

state=$1; shift;
gaussian=$1; shift;
data=$1; shift;
ali=$1; shift;
testdata=$1; shift;
dir=$1; shift;
dataname=`basename $data`;
testname=`basename $testdata`;

steps/train_lda_mllt.sh --cmd "$train_cmd" \
     --splice-opts "--left-context=3 --right-context=3" \
     $state $gaussian $data  data/lang $ali  $dir || exit 1;

utils/mkgraph.sh data/lang_test $dir  $dir/graph || exit 1;

steps/decode.sh --nj 40 --cmd "$decode_cmd" \
 $dir/graph $testdata  $dir/decode_${testname} || exit 1;

steps/align_si.sh  --nj 40 --cmd "$train_cmd" \
  --use-graphs true $data data/lang $dir  $dir/${dataname}_ali  || exit 1;


