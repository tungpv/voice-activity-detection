#!/bin/bash


function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\ninput: $0 $@\n";
# begin user-defined varialbe
cmd=slocal/slurm.pl
trans_index=12;
get_utt_acc=false;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";


if [[ $# -ne 3 ]]; then
  echo -e "\n\nUsage:\n";
  echo -e "$0 data score_dir dir\n\n";
  exit -1;
fi

data=$1; shift;
score_dir=$1; shift;
dir=$1; shift;

[[ -d $dir ]] || mkdir -p $dir;
lang=data/lang;

[[ -e $score_dir/${trans_index}.tra ]] || \
die "file ${trans_index}.tra is not ready in folder $score_dir";

cat $score_dir/${trans_index}.tra | \
utils/int2sym.pl -f 2- $lang/words.txt > $dir/${trans_index}.txt
# make mlf
iutils/um/kaldi_text_to_mlf.pl $dir/${trans_index}.txt  "rec" $dir/${trans_index}.mlf

[[ -e $data/text ]] || die "source text in folder $data expected";
iutils/um/kaldi_text_to_mlf.pl $data/text "lab" $dir/ref.mlf
if $get_utt_acc; then
echo > $dir/dummy_listfn <<EOF
a
b
c
d
EOF

if ! which HResults 2>/dev/null; then
  echo -e "\nHResults is expected\n"; exit -1;
fi
  HResults -f -I $dir/ref.mlf $dir/dummy_listfn $dir/${trans_index}.mlf > $dir/results.txt
  iutils/um/filter_hresults.pl $dir/results.txt $dir/results_reorg.txt
fi


