#!/bin/bash


function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "$0 $@";
# begin user-defined varialbe
nj=20
prior_align=false;
post_align=false;
# end user-defined variable
. parse_options.sh || die "parse_options.sh expected";

states=$1; shift;
gaussians=$1; shift;
traindata=$1; shift;
alidir=$1; shift;
testdata=$1; shift;
dir=$1; shift;
exp=$dir;
testname=`basename $testdata`;
trainname=`basename $traindata`;
echo -e "start at `hostname; date`";

if $prior_align; then
   mdldir=`dirname $alidir`
   [[ -e $mdldir/final.mdl ]] || die "mdldir $mdldir has not final.mdl";
   alidir=$mdldir/${trainname}_ali ; #
   steps/align_si.sh --boost-silence 1.25 --nj $nj \
    --cmd "$train_cmd" \
   $traindata  data/lang $mdldir $alidir || exit 1;
fi


[ -e $exp/final.mdl  ] || \
{
  steps/train_deltas.sh --boost-silence 1.25 --cmd "$train_cmd" \
       $states $gaussians  $traindata  data/lang $alidir $exp || exit 1;
}

if $post_align; then
  [[ -e $dir/final.mdl ]] || die "training failed";
  alidir=$dir/${trainname}_ali
  steps/align_si.sh --boost-silence 1.25 --nj $nj \
    --cmd "$train_cmd" \
  $traindata data/lang $dir $alidir
fi

[ -e $exp/graph/HCLG.fst ] || \
{ 
  utils/mkgraph.sh data/lang_test $exp  $exp/graph || exit 1; 
}

[ -e $exp/decode_${testname}/wer_10 ] || \
{
  steps/decode.sh --nj $nj --cmd "$decode_cmd" \
         --config conf/decode.config \
         $exp/graph $testdata  $exp/decode_${testname} 
}

echo -e "ending at `hostname; date`";
