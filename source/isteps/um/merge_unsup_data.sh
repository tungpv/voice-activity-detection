#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\ninput: $0 $@\n";
# begin user-defined varialbe
cmd=slocal/slurm4.pl
clean=false;
segments=;
lang=;
# end user-defined variable
. parse_options.sh || die "parse_options.sh expected";

if [[ $# -ne 4 ]]; then
  echo -e "\n\nUsage:\n";
  echo "$0 --segments <segfile> # segment file provided by utterance selection program, used to select utterances";
  echo "<asr-transcription>     # kaldi decoded one-best transcription (eg. in scoring folder) ";
  echo "<kaldi-unsupervised-data-folder> # unsupervised data folder";
  echo "<baseline-source-data-folder> # baseline source folder, from which baseline system is trained";
  echo "<dest-folder> # folder to be created, where data is merged by baseline and selected data, from which acoustic model";
  echo "              # will be updated"; 
  echo -e "\n\n";
  exit -1;
fi

trans=$1; shift;
udata=$1; shift;
data=$1; shift;
dir=$1; shift;

tmpdir=$dir/unsup;
[[ -d $tmpdir ]] || mkdir -p $tmpdir

#
if [[ ! -z $segments ]]; then
  [[ -e $segments ]] || die "segments file does not exist";
  cat $trans | \
  iutils/um/filter_trans.pl $segments | \
  utils/int2sym.pl -f 2- $lang/words.txt > $tmpdir/text1;
  isteps/um/make_udata_dir.sh $tmpdir/text1 $udata $tmpdir
else
  cat $trans | \
  utils/int2sym.pl -f 2- $lang/words.txt > $tmpdir/text1;
  isteps/um/make_udata_dir.sh $tmpdir/text1 $udata $tmpdir
fi
# merge
[ -d $data ] || die "folder $data does not exist";
iutils/um/merge_data_dir.sh $tmpdir $data $dir
# clean the unsupervised data folder
if $clean; then
  rm -rf $tmpdir
fi
