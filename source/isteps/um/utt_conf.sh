#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}

. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n## USER_INPUT: $0 $@\n";
# begin user-defined varialbe
cmd=run.pl
stage=0;

make_utt_conf=false;
max_expand=20;
inv_ascale=12;
silence_opts=;
confdir=;
non_speech=;
# end user-defined variable
. parse_options.sh || die "parse_options.sh expected";

if [ $# -ne 5 ]; then
  echo -e "\nUsage:\n $0 [options] <latdir> <lang> <mdldir> <wordpost_dir> <uttconf_dir>\n" && exit 1
fi

latdir=$1; shift;
lang=$1; shift;
mdldir=$1; shift;
wordpost_dir=$1; shift;
uttconf_dir=$1; shift;

for x in $lang/phones/word_boundary.int $lang/nonspeech_words.int $mdldir/final.mdl; do
  [ -e $x ] || die "\n source file $x expected\n";
done

nj=`cat $latdir/num_jobs`;
echo -e "\n## number of jobs $nj\n";
model=$mdldir/final.mdl;
word_boundary=$lang/phones/word_boundary.int;

if [ ! -f $wordpost_dir/post.1.gz ]; then
  echo -e "\n## making word post is in progress `date`\n";
  $cmd JOB=1:$nj $wordpost_dir/log/posts.JOB.log \
  lattice-align-words "$silence_opts" --max-expand=$max_expand $word_boundary $model  "ark:gzip -cdf $latdir/lat.JOB.gz|" ark:- \| \
  lattice-to-word-post --inv-acoustic-scale=$inv_ascale ark:- "|gzip -c >$wordpost_dir/post.JOB.gz"

fi
# make utterance confidence 
if [ ! -f $uttconf_dir/uconf.1.gz ]; then
  echo -e "\n## making utterance conf is in progress `date`\n";
  [[ -e $wordpost_dir/post.1.gz ]] || die "source folder $wordpost_dir is not ready ";
  silence_word=$lang/nonspeech_words.int;
  [[ ! -z $non_speech ]] && silence_word=$non_speech;
  echo -e "\nnon_speech word is $silence_word\n"
  $cmd JOB=1:$nj $uttconf_dir/log/uconf.JOB.log iutils/um/utt_silword_timerate.pl  --silence $silence_word \
 "gzip -cdf $wordpost_dir/post.JOB.gz" "gzip -c > $uttconf_dir/uconf.JOB.gz" \
 '>' $wordpost_dir/silrate.JOB.txt  || exit 1
fi

echo -e "\n## Done ! `date`\n"
