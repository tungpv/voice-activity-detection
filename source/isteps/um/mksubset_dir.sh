#!/bin/bash

echo -e "\n##$0 $@\n"
function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Exit {
  echo -e "\n$1\n"; exit 0;
}
function ShowMe {
  echo -e "\n$1\n";
}
#
. path.sh || die "path.sh expected";
# begin user-defined option
exclude=
include=;
hlen=100000000;
upost_list=;
upost_thresh=0;
fullspk=false;
spklist=
# end user-defined option
echo -e "\n$0 $@\n";
. parse_options.sh || die "parse_options.sh expected";
echo -e "\n$0 $@\n";
if [ $# -ne 2 ];then
  ShowMe "Usage:\n$0 --exclude file \n
              --fullspk <false/true> \n
              --exclude <segments> \n
              --spklist <spklist> \n
             --include file \n
             --hlen <hour-length> \n
             --upost-list <upost_list_file> \n
             --upost_thresh <upost_thresh> \n
             <source-data-dir>  <target-data-dir>";
  exit -1;
fi
sdir=$1; shift;
dir=$1; shift;

[ -e $sdir/segments ] || die "$sdir/segments file expected"; 

if [[ `echo $hlen '>' 0 |bc -l` -lt 1 ]]; then
  die "time length should be over zero";
fi

fullspk_opt="--fullspk=0"
if $fullspk; then
  fullspk_opt="--fullspk=1"
fi
function do_select {
  utt2spk=$sdir/utt2spk
  iutils/um/utt_num.pl ${exclude:+ --exclude $exclude}  \
  ${include:+ --include $include} \
  ${upost_list:+ --upost-list $upost_list} \
  $fullspk_opt  \
  --upost-thresh $upost_thresh \
  --segment $sdir/segments --hlen $hlen \
  --utt2spk $utt2spk --destdir $dir \
  || die "utt_num.pl error"; 
}
function utt2spk_from_spklist {
  spklist=$1; shift; utt2spk=$1;
  cat $utt2spk | \
  perl -e '$spklist = shift @ARGV; 
    open F, "$spklist" or die "file $spklist cannot open\n";
    while (<F>) {chomp; m/(\S+)/ or next; $vocab{$1} ++;} close F;
    while (<STDIN>) { chomp; m/(\S+)\s+(\S+)/ or next; 
      if (exists $vocab{$2}) { print $_, "\n"; }  }
 ' $spklist || exit 1
}
mkdir -p $dir
if [ -z $spklist ]; then
  do_select;
else
  utt2spk_from_spklist $spklist $sdir/utt2spk > $dir/utt2spk || exit 1
  if [ $(wc -l < $dir/utt2spk ) -le 0 ];then
    echo -e "\n## ERROR no utterance selected for spklist $(cat $spklist)\n"; exit 1
  fi
fi
# if [ ! -z $exclude ]; then
#   iutils/um/utt_num.pl --exclude $exclude  \
#   --segment $sdir/segments --hlen $hlen \
#   --utt2spk $sdir/utt2spk --destdir $dir || \
#   die "iutils/utt_num.pl failed with exclude option";
# else
#   iutils/um/utt_num.pl --segment $sdir/segments \
#   --hlen $hlen --utt2spk $sdir/utt2spk \
#   --destdir $dir || die "iutils/utt_num.pl failed";
# fi
# from subset_data_dir.sh in KALDI
srcdir=$sdir
destdir=$dir
[ -e $destdir/spk2utt ] || utils/utt2spk_to_spk2utt.pl < $destdir/utt2spk > $destdir/spk2utt
function do_filtering {
  # assumes the utt2spk and spk2utt files already exist.
  [ -f $srcdir/feats.scp ] && utils/filter_scp.pl $destdir/utt2spk <$srcdir/feats.scp >$destdir/feats.scp
  [ -f $srcdir/wav.scp ] && utils/filter_scp.pl $destdir/utt2spk <$srcdir/wav.scp >$destdir/wav.scp
  [ -f $srcdir/text ] && utils/filter_scp.pl $destdir/utt2spk <$srcdir/text >$destdir/text
  [ -f $srcdir/spk2gender ] && utils/filter_scp.pl $destdir/spk2utt <$srcdir/spk2gender >$destdir/spk2gender
  [ -f $srcdir/cmvn.scp ] && utils/filter_scp.pl $destdir/spk2utt <$srcdir/cmvn.scp >$destdir/cmvn.scp
  if [ -f $srcdir/segments ]; then
     utils/filter_scp.pl $destdir/utt2spk <$srcdir/segments >$destdir/segments
     if [ ! -z $spklist ]; then
       shours=$(cat $destdir/segments | awk '{x+=$4-$3;}END{print x/3600;}')
       echo -e "\nselected data are $(echo $shours | perl -pe 's/(^\d\.\d{2}).*/$1/;') hours\n"
     fi
     awk '{print $2;}' $destdir/segments | sort | uniq > $destdir/reco # recordings.
     # The next line would override the command above for wav.scp, which would be incorrect.
     [ -f $srcdir/wav.scp ] && utils/filter_scp.pl $destdir/reco <$srcdir/wav.scp >$destdir/wav.scp
     [ -f $srcdir/reco2file_and_channel ] && \
       utils/filter_scp.pl $destdir/reco <$srcdir/reco2file_and_channel >$destdir/reco2file_and_channel
     
     # Filter the STM file for proper sclite scoring (this will also remove the comments lines)
     [ -f $srcdir/stm ] && utils/filter_scp.pl $destdir/reco < $srcdir/stm > $destdir/stm
     
     rm $destdir/reco
  fi
  srcutts=`cat $srcdir/utt2spk | wc -l`
  destutts=`cat $destdir/utt2spk | wc -l`
  echo "$0: reducing #utt from $srcutts to $destutts"
}

do_filtering;


