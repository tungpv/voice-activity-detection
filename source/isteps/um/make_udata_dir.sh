#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}

if [ $# -ne 3 ]; then
  echo -e "\n\n";
  echo "Usage: $0 <auto-labeled-text> <unlabeled-data-dir> <dir>";
  echo -e "\n\n";
  exit 1;
fi

echo -e "\n$@\n";

text=$1; shift;
sdir=$1; shift;
dir=$1; shift;

export LC_ALL=C

[ -e $text ] || die "auto-labeled-text file $text does not exist"; 
[ -d $dir ] || mkdir -p $dir

iutils/um/subset_data_dir.sh --uttlist $text $sdir $dir
# text file substitution
rm -f $dir/text || die "source text file removal error";
cat $text | sort > $dir/text || die "replacing text file $dir/text failed";

echo -e "\ndata dir $dir is prepared\n";

