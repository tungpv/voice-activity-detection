#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";

# begin user-defined variable
nj=10;
stage=0;
mdldir=;
use_mono=false;
mono=;
over_model=
over_train_data=;
over_vali_data=;
over_train_ali=;
over_trainali_data=;
over_vali_ali=;
over_valiali_data=;
over_prefix=;

align_needed=false;
fmllr_align=false;
# end user-defined variable
echo -e "\n $0 $@\n";
. parse_options.sh || \
die "parse_options.sh expected";
# if [[ -z $configs || ! -e $configs ]] ;then
#   die "config file must be specified and exist";
# fi
#
if [[ $# -lt 1 ]]; then
  echo -e "\nwrapper script used to train bottleneck feature";
  echo -e "\n\nUsage: $0 \n";
  echo -e "\t <lang-dir>  # kaldi lang dir";
 # echo -e "\t <model-dir> # acoustic models used to";
  echo -e "\t\t # align the training and dev. data";
  echo -e "\t <train-data> # training data";
  echo -e "\t <dev-data> # dev data";
  echo -e "\t <neural network dir1> # folder1  where trained nerual";
  echo -e "\t\t # network1 is in";
  echo -e "\t <neural network dir2> # folder2 for neural network 2";
  echo -e "\n\n";
fi 
echo -e "\nhierbn started `date`\n";
configs=$1; shift;
. $configs || die "config file expected";
# overridding variable set in configure file
[ ! -z $over_model ]  && modeldir4ali=$over_model;
[ ! -z $over_train_ali ] && train_ali=$over_train_ali;
[ ! -z $over_vali_ali ] && dev_ali=$over_vali_ali;
[ ! -z $over_trainali_data ] && traindata4ali=$over_trainali_data;
[ ! -z $over_valiali_data ] && devdata4ali=$over_valiali_data;
[ ! -z $over_train_data ] &&  train_data=$over_train_data;
[ ! -z $over_vali_data ] && dev_data=$over_vali_data;

if [ ! -z $over_prefix ]; then
  bn1dir=$over_prefix/bn1;
  bn2dir=$over_prefix/bn2;
fi
#
if $use_mono; then
  echo -e "\n!!! mono-phone model is enabled !\n"
  [ ! -z $mono ] || die "mono model is not provided";
  [ -e $mono/final.mdl ] || die "final model in mono folder does not exist"; 
  array_ali=($train_ali $dev_ali);
  train_ali=`mktemp -d -p $modeldir4ali`;
  dev_ali=`mktemp -d -p $modeldir4ali`;
fi

#
# do forced alignment
if $align_needed; then
  echo -e "\nforced alignment started at `hostname` `date`\n";
  # check 
  [[ ! -z $modeldir4ali && -e $modeldir4ali/final.mdl ]] \
  || die "final.mdl is not ready in folder $modeldir4ali";
  mdldir=$modeldir4ali;
  idx=0;
  array_dir=($train_ali $dev_ali);
  for x in $traindata4ali $devdata4ali; do
     alidir=${array_dir[$idx]}; 
     if $use_mono; then
       mono_alidir=${array_ali[$idx]};
     fi
    [[ -d $x ]] || \
    die "data $x is not ready ?";
    if $fmllr_align; then
      if [ ! -e $alidir/ali.1.gz ]; then
        steps/align_fmllr.sh --nj $nj \
        --cmd "$train_cmd" $x $lang $mdldir $alidir \
        ||  die "forced alignment failed";
      fi
    else
      steps/align_si.sh --nj $nj \
      --cmd "$train_cmd" $x $lang $mdldir $alidir \
      ||  die "forced alignment failed";   
    fi
    # alignment conversion
    if $use_mono; then
      run.pl JOB=1:$nj $mono_alidir/log/convert.JOB.log \
      convert-ali  $mdldir/final.mdl $mono/final.mdl $mono/tree \
     "ark:gunzip -c $alidir/ali.JOB.gz|" "ark:|gzip -c >$mono_alidir/ali.JOB.gz" \
     || die "convert-ali failed";
     cp $mono/{final.mdl,tree} $mono_alidir/  2>/dev/null; 
    fi
    idx=$[$idx+1];
  done
fi
# 
if $use_mono; then
  train_ali=${array_ali[0]};
  dev_ali=${array_ali[1]};
fi

# check
for x in $train_ali $dev_ali; do
 [[ -e $x/ali.1.gz ]] || die "ali $x is not well ready";
done


if [[ $stage -le 2 ]] ;then
  echo -e "\ntrain bottleneck neural network\n";
  $cuda_cmd $bn1dir/_train_nnet.log \
  isteps/tandem/train_nnet.sh --hid-layers $hid_layers \
  --temp-prefix $temp_prefix \
  --clean-temp $clean_temp \
  --hid-dim $hid_dim --bn-dim $bn1_output_dim --feat-type traps \
  --splice $bn1_input_ctx_width \
  --traps-dct-basis $traps_dct_basis \
  --learn-rate 0.008 \
  --bunch-size $bunch_size \
  --cache-size $cache_size \
  $train_data $dev_data $lang $train_ali $dev_ali $bn1dir \
  || die "training bn failed";

fi
# compose feature transform with bn1
feature_transform=$bn1dir/final.feature_transform.bn1;
if [ $stage -le 3 ]; then
  echo -e "\ncomposed bn1 to as a feature transform to bn2\n";
  [ -e $bn1dir/final.nnet ] || \
  die "bn1 $bn1dir/final.nnet does not exist";
  nnet-concat --binary=false $bn1dir/final.feature_transform \
  "nnet-copy --remove-last-layers=$remove_last_layers --binary=false $bn1dir/final.nnet - |" \
      "utils/nnet/gen_splice.py --fea-dim=$bn1_output_dim --splice=$bn1_output_ctx_width --splice-step=$bn1_output_splice_step |" \
      $feature_transform

fi
# check to proceed
[ -e $feature_transform ] || \
die "feature_transform $feature_transform does not exist";
# train bottleneck neural network 2 as merger
if [ $stage -le 4 ] ;then
  echo -e "\nbn2 training started\n";
  $cuda_cmd $bn2dir/_train_nnet.log \
  isteps/tandem/train_nnet.sh --hid-layers $hid_layers \
  --temp-prefix $temp_prefix \
  --clean-temp $clean_temp \
  --hid-dim $hid_dim --bn-dim $bn2_output_dim \
   --feature-transform $feature_transform \
  --learn-rate 0.008 \
  --bunch-size $bunch_size \
  --cache-size $cache_size \
  $train_data $dev_data $lang $train_ali $dev_ali $bn2dir || exit 1;
fi

echo -e "\nhierbn finished `hostname` `date`\n";
exit 0;
