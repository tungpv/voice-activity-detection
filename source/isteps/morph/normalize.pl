#!/usr/bin/env perl
use strict;
use warnings;

sub IsMerge {
  my ($w) = @_;
  if ($w eq "-" or $w eq "+" or $w eq "-+" 
      or $w eq "+-") {
    return 1;
  }
  return 0;
}
print STDERR "## std expected\n";
my @A = (); my @B = ();
while (<STDIN>) {
  chomp;
  @A = split / /;  my $len = scalar @A; my $j = 0;
  next if $len <= 0;
  for (my $i = 0; $i < $len; $i ++) {
     my $w = $A[$i];
    if ($i == 0) {
      push @B, $w;
      $j ++;
    } else { # for nonstart words
      my $w1 = $B[$j-1];
      if (IsMerge($w) == 0) {
        if (IsMerge($w1) == 0) {
           push @B, $w; $j ++;
        } else {
          $B[$j-1] = $B[$j-1] . $w;
        }
      } else { # w is a word to be merged
        $B[$j-1] = $B[$j-1] . $w;  
      }  
    }
  }
  if (scalar @B > 0) {
    print join " ", @B, "\n"; 
    @B = (); $j =0;
  }
}
print STDERR "## std ended\n";
