#!/bin/bash

echo -e "\n## $0 $@\n"
function die {
  echo -e "\n## ERROR: $1\n"; exit 1
}

. path.sh || die "path.sh expected"

#
# example : isteps/morph/morfessor_segment.sh  test/morph.mdl test/tobe.wlist test/tobe.morph.lex  
#
. parse_options.sh || die "parse_options.sh expected";

if [ $# -ne 3 ]; then
  echo -e "\n\n Usage Example: $0 <morph.mdl> <wlist.txt>  <word2morph.txt> \n\n"
  exit 1
fi

mdl=$1; shift;
wlist=$1; shift;
w2m=$1; shift

for x in $mdl $wlist ; do
 [ -f $x ] || die "file $x is not there";
done

mkit=`which morfessor-segment`
if [ -z $mkit ]; then
  die "morfessor-segment tool is not there";
fi
dir=`dirname $w2m`;
logdir=$dir/log_morph
mkdir -p $logdir 2>/dev/null

[ -d $logdir ] || die "makding log dir $logdir is failed";

echo -e "\n## $0 making wordlist, some words will be removed\n"
cat $wlist | \
perl -e '
  $str = shift @ARGV;
  foreach $s (split / /, $str) {
    $vocab01{$s} = 0;
  }
  while (<>) {
    chomp;
    @A = split / /;
    for($i = 0; $i < scalar @A; $i++) {
      $w = $A[$i];
      next if (exists $vocab01{$w});
      $vocab{$w} = 0; 
    }
  }
  foreach $w (keys %vocab) {
    if($w =~ /[<]/) {
      print STDERR $w, "\n";
      next;
    }
    print $w, "\n";
  }
' "+ -"  > $logdir/filtered.wlist 2>$logdir/removed.wlist

echo -e "\n## $0 check $logdir/filtered.wlist and $logdir/removed.wlist\n"

$mkit -l $mdl  -o $logdir/filtered.morph  $logdir/filtered.wlist 

[ -f $logdir/filtered.morph ] || die "morph failed from $mkit";

echo -e "\n## $0 check $logdir/filtered.morph\n"

echo -e "\n## $0 normalize morph started \n"
cat $logdir/filtered.morph | \
isteps/morph/normalize.pl > $logdir/filtered.morph.normalized

if [[ `wc -l <$logdir/filtered.morph.normalized` -ne `wc -l <$logdir/filtered.wlist` ]]; then
  echo -e "\n## $0 word number mismatched \n"; exit 1
fi


paste <(cat $logdir/filtered.wlist) <(cat $logdir/filtered.morph.normalized)|sort  -u > $w2m

cat $logdir/filtered.morph.normalized | \
awk '{for(i=1;i<=NF;i++){print $i;}}'| sort -u > $logdir/filtered_norm.mlist

echo -e "\n## $0 check $w2m and $logdir/filtered_norm.mlist files\n"
