#!/bin/bash

function die {
  echo -e "\nERROR: $1\n"; exit 1;
}
# begin options
cmd=run.pl
# end options
. path.sh || die "path.sh expected"
. cmd.sh || die "cmd.sh expeced"
if [ $# -ne 4 ]; then
   echo -e "\n$0 [options] <src_model_dir> <dest_model_dir> <src_ali_dir> <dest_ali_dir>\n"
   exit 1
fi
. parse_options.sh || die "parse_options.sh expected"
srcmdldir=$1; shift
mdldir=$1; shift
sali=$1; shift
ali=$1; shift

if [ ! -e $sali/ali.1.gz ]; then
  die "source alignment is not ready in folde $sali"
fi
nj=`cat $sali/num_jobs`
if [ -z $nj ]; then
  die "num_jobs file expected in folder $sali"
fi


$cmd JOB=1:$nj $ali/log/convert.JOB.log \
      convert-ali  $srcmdldir/final.mdl $mdldir/final.mdl $mdldir/tree \
     "ark:gunzip -c $sali/ali.JOB.gz|" "ark:|gzip -c >$ali/ali.JOB.gz" \
     || die "convert-ali failed";
     cp $mdldir/{final.mdl,tree} $ali/  2>/dev/null;
     cp $sali/num_jobs $ali/

exit 0
