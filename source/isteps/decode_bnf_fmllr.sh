#!/bin/bash

. path.sh || exit 1 
. cmd.sh  || exit 1

echo -e "\n## LOG: $0 $@\n";

# begin options
cmd=run.pl
bnndir=
feature=
norm_vars=false
max_active=7000
beam=13.0
latbeam=6.0
acwt=0.083333 # note: only really affects pruning (scoring is on lattices).
num_threads=1 # if >1, will use gmm-latgen-faster-parallel

# end options

. parse_options.sh
if [ $# -ne 3 ]; then
   echo -e "\n\nExample: $0 lang_conf lang ali sdata expdir\n\n";
   exit 1
fi
graph=$1; shift
sdata=$1; shift;
dir=$1; shift;

set -u;

sname=`basename $sdata`; fbdata=$sdata/fbank/$sname; fbfeat=$feature/fbank/$sname;
if [ ! -f $fbdata/.fb_done ]; then
  if [ -z $feature ]; then echo -e "\n## ERROR: $feature is not ready\n"; exit 1; fi
  [ -d $fbdata ] || mkdir -p $fbdata
  cp $sdata/* $fbdata; rm $fbdata/{feats.scp,cmvn.scp}
  nj=$(wc -l < $fbdata/spk2utt); [ $nj -gt 30 ] && nj=30;
  echo -e "\n## making fb feature `hostname` `date`\n";
  steps/make_fbank_pitch.sh  --nj $nj --cmd "$cmd"   $fbdata $fbfeat/_log $fbfeat/_data || exit 1
  steps/compute_cmvn_stats.sh $fbdata $fbfeat/_log $fbfeat/_cmvn || exit 1
  touch $fbdata/.fb_done
fi

nj=$(wc -l < $fbdata/spk2utt); [ $nj -gt 30 ] && nj=30;
if [ -z $nj ]; then echo -e "\n## num_jobs is not specified in $fbdata\n"; exit 1; fi
bnfdata=$sdata/sbnf/$sname; bnffeat=$feature/sbnf/$sname
if [ ! -f $bnfdata/.bnf_done ]; then 
  if [ -z $bnndir ]; then echo -e "\n## ERROR: BN-NN folder $bnndir is not specified\n"; exit 1; fi
  echo -e "\n## making bottle-neck feature `date`\n";
  isteps/imake_bn_feats.sh --nj $nj --cmd "$cmd" \
  $bnfdata $fbdata $bnndir  $bnffeat/_log $bnffeat/_data || exit 1
  touch $bnfdata/.bnf_done
fi
data=$bnfdata; 

if [ ! -f $dir/.decode_done ]; then
  steps/decode_fmllr.sh --cmd "$cmd" --nj $nj --beam $beam --lattice-beam $latbeam \
  --acwt $acwt  $graph $data $dir || exit 1
  touch $dir/.decode_done
fi
