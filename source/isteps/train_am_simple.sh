#!/bin/bash


function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
function Log {
  echo -e "\n\n`date` $1\n\n";
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";
echo -e "\n$0 $@\n";
# begin user-defined variable
cmd=run.pl;
doit=false;
max_stage=8;
over_train=
over_dev=
over_expdir=
over_ubm=;
over_nstate=;
over_ngauss=;
over_npdf=;
over_nsubstate=;
stage=0;
sub_stage=1;
over_tri3_ali=false;
norm_vars=false;
dec_nj=30;
topo=;
over_lang=; over_lang_test=;
# end user-defined variable

. parse_options.sh || die "parse_options.sh expected";
# echo -e "\nafter options removal: parameters=$#\n"
if [[  $# -ne 1 ]]; then
  echo -e "\nUsage:$0 config\n"; exit -1;
fi
conf=$1; shift;
. $conf || die "config file $conf does not exist";

[ ! -z $over_lang ] && lang=$over_lang;
[ ! -z $over_lang_test ] && lang_test=$over_lang_test;
[ ! -z $over_train ] && train_data=$over_train && echo "train_data=$train_data" ;
[ ! -z $over_dev ] && dev_data=$over_dev && echo "dev_data=$over_dev";
[ ! -z $over_expdir ] && exp_dir=$over_expdir;
[ ! -z $over_ubm ] && ubm_num=$over_ubm;
[ ! -z $over_npdf ] && npdf=$over_npdf;
[ ! -z $over_nsubstate ] && nsubstate=$over_nsubstate;
[ ! -z $over_nstate ] && nstate=$over_nstate;
[ ! -z $over_ngauss ] && ngauss=$over_ngauss;
if [ -z $topo ] ;then
  topo=$lang/topo;
fi
echo `date`@`hostname`
if [[ $stage -le 1 ]] ;then
  echo -e "\nmonophone training\n";
  isteps/train_mono.sh --boost-silence 1.25 --nj $train_nsub \
  --cmd "$cmd" --topo $topo \
  --norm-vars $norm_vars \
  $train_data  $lang  $exp_dir/mono0a  || exit 1;
  
  isteps/align_si.sh --boost-silence 1.25 --nj $train_nsub \
  --cmd "$cmd" \
  --norm-vars $norm_vars \
  $train_data $lang $exp_dir/mono0a $exp_dir/mono0a_ali || exit 1;
  [ $max_stage -eq 1 ] && echo "finished mono training" && exit 0;
fi
# delta
tri1_ali=$exp_dir/tri1_ali_$trainname;
if [[ $stage -le 2 ]]; then
  echo -e "\ndelta triphone training\n";
  isteps/train_deltas.sh --boost-silence 1.25 \
  --cmd "$cmd" --topo $topo \
  --norm-vars $norm_vars \
  $nstate $ngauss \
  $train_data $lang  $exp_dir/mono0a_ali $exp_dir/tri1 || exit 1;

  isteps/align_si.sh --nj $train_nsub --cmd "$cmd" \
  --norm-vars $norm_vars \
  $train_data  $lang $exp_dir/tri1  $tri1_ali || exit 1;
fi
# lda+mllt
tri2_ali=$exp_dir/tri2b_ali_${trainname};
if [[ $stage -le 3 ]]; then
  isteps/train_lda_mllt.sh --cmd "$cmd" \
  --norm-vars $norm_vars --topo $topo \
   --splice-opts "$lda_ctx_opt" \
  $nstate $ngauss $train_data  $lang $tri1_ali $exp_dir/tri2 || exit 1;
  isteps/align_si.sh  --nj $train_nsub --cmd "$cmd" \
  --norm-vars $norm_vars \
  --use-graphs true $train_data \
  $lang $exp_dir/tri2  $tri2_ali  || exit 1;
fi
# sat
mdldir=; mdldir=$exp_dir/tri3sat_$nstate;
tri3_ali=$mdldir/ali_${trainname};
if  [[ $stage -le 4 ]]; then
  isteps/train_sat.sh --cmd "$cmd" \
  --norm-vars $norm_vars --topo $topo \
  $nstate $ngauss $train_data \
  $lang  $tri2_ali $mdldir || exit 1;
  
  isteps/align_fmllr.sh --nj $train_nsub --cmd "$cmd" \
  --norm-vars $norm_vars \
  $train_data  $lang $mdldir \
  $tri3_ali || exit 1;
fi
# data less than 10 hour should not perform mmi
denlat1=$exp_dir/tri3_denlats_${trainname}
if [[ $stage -le -1 ]] ;then
  steps/make_denlats.sh --nj $train_nsub \
  --sub-split $split_nsub --cmd "$train_cmd" \
  --transform-dir $tri3_ali \
  $train_data  $lang \
  $exp_dir/tri3sat  $denlat1  || exit 1;
  
  steps/train_mmi.sh --cmd "$train_cmd" --boost $mmi_boost \
  $train_data $lang  \
  $tri3_ali  $denlat1 \
  $exp_dir/tri4_mmi_b$mmi_boost  || exit 1;

fi
# evaluate on dev data
devname=`basename $dev_data`
satdecdir=$mdldir/decode_$devname;
if [[ $stage -le 6 ]]; then
  echo -e "\nevaluate the training results\n";
  sdir=$mdldir;
  graphdir=$sdir/graph;
  # make graph
  if [ ! -e $graphdir/HCLG.fst ] ; then
    utils/mkgraph.sh $lang_test $sdir \
    $graphdir|| exit 1;
  fi
  isteps/decode_fmllr.sh --nj $dec_nj \
  --cmd "$cmd" \
  --norm-vars $norm_vars \
  $graphdir $dev_data  $satdecdir || exit 1;
  
  # steps/decode.sh --nj $decode_nsub \
  # --cmd "$decode_cmd" --transform-dir $satdecdir \
  # $graphdir \
  # $dev_data $exp_dir/tri4_mmi_b$mmi_boost/decode_$devname
fi
[ $max_stage -le 6 ] && exit 0;
# sgmm
sgmmdir=$exp_dir/sgmm4_$ubm_num;
if [ $stage -le 7 ]; then
  echo -e "\nsubspace gmm training\n";

  if $over_tri3_ali; then
    name=`basename $train_data`;
    tri3_ali=$exp_dir/tri3sat/ali_$name;
    if [ $sub_stage -le 0 ]; then
      steps/align_fmllr.sh --nj $train_nsub --cmd "$train_cmd" \
      $train_data  $lang $exp_dir/tri3sat \
      $tri3_ali || die "@step7:steps/align_fmllr.sh";
    fi
  fi
  ubm=$exp_dir/ubm4_$ubm_num;
  train_cmd=run.pl;
  [ -e $ubm/final.ubm ] || \
  steps/train_ubm.sh --cmd "$train_cmd" \
  $ubm_num  $train_data $lang $tri3_ali $ubm
  
  steps/train_sgmm2.sh --cmd "$train_cmd" \
  $npdf $nsubstate \
  $train_data  $lang $tri3_ali \
  $ubm/final.ubm $sgmmdir \
  || die "@step7:steps/train_sgmm2.sh";
fi

# evaluate sgmm
if [ $stage -le 8 ] ;then
  echo -e "\nsgmm eval\n";
  sdir=$sgmmdir;
  # train_cmd=run.pl;
  [ -e $sdir/graph/HCLG.fst ] || \
  utils/mkgraph.sh $lang_test $sdir $sdir/graph
  isteps/idecode_sgmm2.sh --nj $decode_nsub \
  --cmd "$train_cmd" --transform-dir $satdecdir \
  --beam 13 \
  --max-active 7000 \
  --lattice-beam2 6.0 \
  $sdir/graph $dev_data $sdir/decode_$devname\
  || die "@step8: isteps/idecode_sgmm2.sh";
  
fi
  [[ $max_stage -le 8 ]] && \
  Log "Ended with isteps/tandem/train_regular_am.sh"&& exit 0;
# sgmm + mmi training
alidir=$sgmmdir/ali_`basename $train_data`;
dendir=$sgmmdir/denlat_`basename $train_data`
sgmm_mmi_dir=$exp_dir/sgmm5_${ubm_num}_mmi_b0.1;
if [ $stage -le 9 ]; then
  Log "start sgmm+mmi training"; 
  # sbatch -p speech -o $sgmmdir/align_sgmm2_sbatch.log -n$train_nsub \
  steps/align_sgmm2.sh --nj $train_nsub \
  --cmd "$train_cmd" --transform-dir $tri3_ali  \
  --use-graphs true --use-gselect true $train_data \
  $lang $sgmmdir  $alidir || \
  die "@step9:steps/align_sgmm2.sh"; 
fi
# use run.pl in icsi, as this might need
# more memory, which should  be killed by the slurm
if [ $stage -le 10 ]; then  
  steps/make_denlats_sgmm2.sh --nj $train_nsub \
  --cmd $train_cmd --transform-dir $tri3_ali \
  --max_mem $[50000000*20] \
  --max_active 6000 \
  $train_data $lang $alidir $dendir || \
  die "@step10:steps/make_denlats_sgmm2.sh"; 
fi
if [ $stage -le 11 ]; then
   
  steps/train_mmi_sgmm2.sh --cmd $train_cmd \
  --transform-dir $tri3_ali --boost 0.1 \
  $train_data $lang $alidir $dendir $sgmm_mmi_dir \
  || die "@step11:steps/train_mmi_sgmm2.sh";

fi
#  evaluate sgmm+mmi

if [ $stage -le 13 ]; then
  Log "evaluate sgmm2+mmi";
  for iter in 1 2 3 4; do
    # sbatch -p speech -n$nj --mem-per-cpu 5000MB  -o $sgmm_mmi_dir/sbatch_$iter.log \
    steps/decode_sgmm2_rescore.sh --cmd "$decode_cmd" --iter $iter \
    --transform-dir $satdecdir \
    $sgmmdir/graph   $dev_data  \
    $sgmmdir/decode_$devname   $sgmm_mmi_dir/decode_${devname}_it$iter || \
    die "@step13:steps/decode_sgmm2_rescore.sh"; 
  done
fi
