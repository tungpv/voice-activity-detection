#!/bin/bash

. path.sh || exit 1 
. cmd.sh  || exit 1

echo -e "\n## LOG: $0 $@\n";

# begin options
cmd=run.pl
hid_layers=2;
hid_dim=1500;
bn1_output_dim=80;
bn1_input_ctx_width=5
traps_dct_basis=6
learn_rate=0.008
remove_last_layers=4
bn1_output_ctx_width=2
bn1_output_splice_step=5
bn2_output_dim=30;
dev_hlen=1.0
fmllr_align=true;
monodir=
id=
train_script="isteps/tandem/train_nnet.sh"
# end options

. parse_options.sh 
if [ $# -ne 4 ]; then
  echo -e "\n\nExample: $0 [options] lang gmmdir data dir"
fi

lang=$1; shift
gmmdir=$1; shift
data=$1; shift
dir=$1;  shift

data4ali=$data/sbn; 
train4ali=$data4ali/train_nn; dev4ali=$data4ali/train_dev;
if [ ! -f $data4ali/.prepare_done ]; then
  echo -e "\n##  $0 prepare train_nn and train_dev data for alignment \n";
  isteps/um/mksubset_dir.sh --fullspk true --hlen $dev_hlen $data $dev4ali || exit 1
  exclude=$dev4ali/segments;
  isteps/um/mksubset_dir.sh --exclude $exclude $data $train4ali || exit 1
  touch $data4ali/.prepare_done
fi

alidir=$gmmdir/sbnali$id; 
train_ali=$alidir/train_nn; dev_ali=$alidir/train_dev;
if [ ! -z $monodir ]; then
  alidir1=$monodir/sbnali$id;
  train_ali=$alidir1/train_nn; dev_ali=$alidir1/train_dev;
fi
for x in train_nn train_dev; do
  data1=$data4ali/$x; ali=$alidir/$x;
  nj=`wc -l <$data1/spk2utt`; [ $nj -gt 30 ] && nj=30;
  echo -e "\n## $0 doing alignment for data $data1\n";

  if [ ! -f $ali/.ali_done ]; then
    if $fmllr_align; then  
      steps/align_fmllr.sh --nj $nj --cmd "$cmd"  $data1  $lang $gmmdir $ali || exit 1 
    else
      steps/align_si.sh --nj $nj --cmd "$cmd"  $data1  $lang $gmmdir $ali || exit 1 
    fi
    touch $ali/.ali_done
  fi
  if [ ! -z $monodir ]; then
    xali=$alidir1/$x;
    if [ ! -f $xali/ali.1.gz ];then
      isteps/convert_ali.sh  $gmmdir $monodir $ali $xali || exit 1
    fi
  fi
done

fbdata=$data/fbank; fbfeat=$dir/feature;
train_data=$fbdata/train_nn; dev_data=$fbdata/train_dev;
if [ ! -f $fbdata/.fbank_done ]; then
  for x in train_nn train_dev; do
    echo -e "\n## $0 making fbank+pitch feature for data $fbdata/$x `date`\n"
    sdata=$data4ali/$x; data1=$fbdata/$x; [ -d $data1 ] || mkdir -p $data1
    cp $sdata/* $data1; rm  $data1/{feats.scp,cmvn.scp}
    feat=$fbfeat/$x;  nj=$(wc -l < $data1/spk2utt); [ $nj -gt 30 ] && nj=30;
    steps/make_fbank_pitch.sh  --nj $nj --cmd "$cmd" \
    $data1 $feat/_log $feat/_data || exit 1
    steps/compute_cmvn_stats.sh $data1 $feat/_log $feat/_cmvn || exit 1
  done
  touch $fbdata/.fbank_done
fi
bn1dir=$dir/bn1; 
if [ ! -f $bn1dir/final.nnet  ] ;then
  echo -e "\ntrain bottleneck neural network #1\n";
  $cmd $bn1dir/_train_nnet.log \
  $train_script --hid-layers $hid_layers \
  --hid-dim $hid_dim --bn-dim $bn1_output_dim --feat-type traps \
  --splice $bn1_input_ctx_width  --traps-dct-basis $traps_dct_basis \
  --learn-rate $learn_rate  $train_data $dev_data $lang $train_ali $dev_ali $bn1dir || exit 1
fi


# compose feature transform with bn1
feature_transform=$bn1dir/final.feature_transform.bn1;
if [ ! -f $feature_transform ]; then
  echo -e "\ncomposed bn#1 as a feature transform to bn#2 `date` \n";
  nnet-concat --binary=false $bn1dir/final.feature_transform \
  "nnet-copy --remove-last-layers=$remove_last_layers --binary=false $bn1dir/final.nnet - |" \
  "utils/nnet/gen_splice.py --fea-dim=$bn1_output_dim --splice=$bn1_output_ctx_width --splice-step=$bn1_output_splice_step |"    $feature_transform || exit 1
fi

bn2dir=$dir/bn2
if [ ! -f $bn2dir/final.nnet  ]; then
  echo -e "\n## bn #2 training started `date` \n"
  $cmd $bn2dir/_train_nnet.log \
  $train_script --hid-layers $hid_layers \
  --hid-dim $hid_dim --bn-dim $bn2_output_dim \
  --feature-transform $feature_transform \
  --learn-rate $learn_rate \
  $train_data $dev_data $lang $train_ali $dev_ali $bn2dir || exit 1;
fi


