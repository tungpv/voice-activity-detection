#!/bin/bash

echo -e "\n## $0 $@\n"
function die {
  echo -e "\n## ERROR: $1\n"; exit 1
}

. path.sh || die "path.sh expected"
. g2p_path_ifc.sh
. g2p_path.sh 
#
nbest=1  
#
. parse_options.sh || die "parse_options.sh expected";

if [ $# -ne 3 ]; then
  echo -e "\n\n Usage Example: $0 <g2p_model> <to_be_transcribed_word_list>  <transcribed.lex> \n\n"
  exit 1
fi
mdl=$1; shift
wlist=$1; shift
lex=$1; shift

for x in $mdl $wlist; do
  [ -f $x ] || die "file $x is not there";
done

g2ptool=`which phonetisaurus-g2p`
if [ -z $g2ptool ]; then
  die "g2ptool is not found";
fi
dir=`dirname $wlist`
logdir=$dir/log; mkdir -p $logdir >/dev/null
[ -d $logdir ] ||die "logdir $logdir is not ready";
name=`basename $wlist`
echo -e "\n## $0 nbest=$nbest\n";
$g2ptool  --model=$mdl \
  --input=$wlist  --isfile --words --nbest=$nbest  \
  2>$logdir/phonetisaurus-g2p-${name}.log | \
  sort -u > $logdir/raw_transcribed.lex
  [ -f $logdir/raw_transcribed.lex ] || die "raw_transcribed file is not made";
  cat $logdir/raw_transcribed.lex | \
  awk '{$2=""; print $0}' | \
  awk '{printf("%s\t", $1); for(i=2;i<NF;i++){printf("%s ", $i)}; printf("\n"); }' \
  |sort -u > $lex 

[  -f $lex ]  || die "failed to create transcribed lexicon file : $lex";

echo -e "\n## check transcribed lexicon file: $lex\n"

exit 0
