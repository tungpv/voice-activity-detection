#!/bin/bash

function die {
  echo -e "\nERROR:$1\n"; exit -1;
}
. path.sh || die "path.sh expected";
. cmd.sh || die "cmd.sh expected";

# begin options
step=0
cmd=run.pl
hid_layers=2
clean_temp=true
hid_dim=1500
bn1_output_dim=80
bn1_input_ctx_width=5
traps_dct_basis=6
learn_rate=0.008
bunch_size=128
cache_size=$[16384*16]
remove_last_layers=4
bn1_output_ctx_width=2
bn1_output_splice_step=5
bn2_output_dim=30;
# end options

echo -e "\n\n$0 $@\n\n"
. parse_options.sh || die "parse_options.sh expected"

if [ $# -ne 6 ]; then
  echo -e "\n\n$0 [options] <train_data> <vali_data> <train_ali> <vali_ali>\\"
  echo "<lang> <dir>\n\n"
  echo "user_input:$@"
fi

train_data=$1; shift
dev_data=$1; shift
train_ali=$1; shift
dev_ali=$1; shift
lang=$1; shift
dir=$1; shift

bn1dir=$dir/bn1
temp_prefix=$dir
if [ $step -le 3 ] ;then
  echo -e "\ntrain bottleneck neural network #1\n";
  $cmd $bn1dir/_train_nnet.log \
  isteps/train_nnet.sh --hid-layers $hid_layers \
  --temp-prefix $temp_prefix \
  --hid-dim $hid_dim --bn-dim $bn1_output_dim --feat-type traps \
  --splice $bn1_input_ctx_width \
  --traps-dct-basis $traps_dct_basis \
  --learn-rate $learn_rate \
  $train_data $dev_data $lang $train_ali $dev_ali $bn1dir \
  || die "training bn failed";

fi

# compose feature transform with bn1
feature_transform=$bn1dir/final.feature_transform.bn1;
if [ $step -le 5 ]; then
  echo -e "\ncomposed bn#1 as a feature transform to bn#2\n";
  [ -e $bn1dir/final.nnet ] || die "bn1 $bn1dir/final.nnet does not exist";
  nnet-concat --binary=false $bn1dir/final.feature_transform \
  "nnet-copy --remove-last-layers=$remove_last_layers --binary=false $bn1dir/final.nnet - |" \
  "utils/nnet/gen_splice.py --fea-dim=$bn1_output_dim --splice=$bn1_output_ctx_width --splice-step=$bn1_output_splice_step |" \
   $feature_transform || exit 1
fi

# check to proceed
[ -e $feature_transform ] || die "feature_transform $feature_transform does not exist after step05"
bn2dir=$dir/bn2
if [ $step -le 7 ]; then
  echo -e "\nbn #2 training started\n"
  $cmd $bn2dir/_train_nnet.log \
  isteps/train_nnet.sh --hid-layers $hid_layers \
  --temp-prefix $temp_prefix \
  --hid-dim $hid_dim --bn-dim $bn2_output_dim \
  --feature-transform $feature_transform \
  --learn-rate $learn_rate \
  $train_data $dev_data $lang $train_ali $dev_ali $bn2dir || exit 1;
fi

echo -e  "\nstacked bn training done `hostname` `date`\n"
exit 0
