// latbin/lattice-to-post.cc

// Copyright 2009-2011   Saarland University
// Author: Arnab Ghoshal

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "fstext/fstext-lib.h"
#include "lat/kaldi-lattice.h"
#include "lat/lattice-functions.h"
#include "lat/kws-functions.h"
namespace kaldi {



}  // kaldi namespace

int main (int argc, char *argv[] ) {
  try {
    typedef kaldi::int32 int32;
    using fst::SymbolTable;
    using fst::VectorFst;
    using fst::StdArc;
    typedef kaldi::CompactLattice::Arc Arc;
    typedef Arc::StateId StateId;

    const char *usage =
       "Calculate a word confidence in lattice with maximum mode,\n"
       "and lattice must be aligned before calling this program.\n"
       "Usage:liattice-to-word-max-post [options] lats-rspecifier wspcifier\n"
       "e.g.: lattice-to-word-max-post --inv-acoustic-scale=10 ark:1.lats 1.post\n";
    kaldi::BaseFloat inv_acoustic_scale = 1.0, lm_scale = 1.0;
    kaldi::ParseOptions po (usage);
    po.Register("inv-acoustic-scale", &inv_acoustic_scale,
                "Scaling factor for acoustic likelihoods");
    po.Register("lm-scale", &lm_scale,
                "Scaling factor for \"graph costs\" (including LM costs)");
    float frame_length = 0.01;
    po.Register("frame-length", &frame_length, "fame duration in terms of second");
    po.Read(argc, argv);
    if (po.NumArgs() != 2) {
      po.PrintUsage();
      exit(1);
    }
    kaldi::BaseFloat acoustic_scale = 1.0;
    if (inv_acoustic_scale == 0.0)
      KALDI_ERR << "Do not use a zero acoustic scale (cannot be inverted)";
    acoustic_scale = 1.0 / inv_acoustic_scale;

    std::string lats_rspecifier = po.GetArg(1),
        posteriors_wspecifier = po.GetArg(2);
    kaldi::SequentialCompactLatticeReader clattice_reader(lats_rspecifier);
    bool binary;  binary = false;
    kaldi::Output ko(posteriors_wspecifier, binary, false);
    
    int32 n_done = 0;
    for(; !clattice_reader.Done(); clattice_reader.Next()) {
      std::string key = clattice_reader.Key();
      kaldi::CompactLattice clat = clattice_reader.Value();
      clattice_reader.FreeCurrent();
      if(acoustic_scale != 1.0 || lm_scale != 1.0)
        fst::ScaleLattice(fst::LatticeScale(lm_scale, acoustic_scale), &clat);
      kaldi::uint64 props = clat.Properties(fst::kFstProperties, false);
      if (!(props & fst::kTopSorted)) {
        if (fst::TopSort(&clat) == false)
          KALDI_ERR << "Cycles detected in lattice.";
      }
      std::vector<int32> state_times;
      kaldi::CompactLatticeStateTimes(clat, &state_times);
      std::vector<double> alphas;
      kaldi::ComputeCompactLatticeAlphas(clat, &alphas);
      double tot_forward_prob = alphas[alphas.size() - 1];
      std::vector<double> betas;
      kaldi::ComputeCompactLatticeBetas(clat, &betas);
      double tot_backward_prob = betas[0];
      if (!kaldi::ApproxEqual(tot_forward_prob, tot_backward_prob, 1.0)) {
        KALDI_ERR << "Total forward probability over lattice = " << tot_forward_prob
              << ", while total backward probability = " << tot_backward_prob;
      }
      // traverse lattice again to get word's posterior
      std::ostringstream ss;
      int32 num_states = clat.NumStates();
      for (StateId s = 0; s < num_states; s++) {
        double this_alpha = alphas[s];
        int32 stime = state_times[s];
        float stime_sec = frame_length * stime;
        char char_stime_sec[1024];
        sprintf(char_stime_sec, "%.2f", stime_sec);
        for (fst::ArcIterator<kaldi::CompactLattice> aiter(clat, s); !aiter.Done(); aiter.Next()) {
          const Arc &arc = aiter.Value();
          int32 etime = state_times[arc.nextstate];
          float etime_sec = frame_length * etime;
          char char_etime_sec[1024];
          sprintf(char_etime_sec, "%.2f", etime_sec);
          if (arc.olabel != 0) {
            double arc_like = - ConvertToCost(arc.weight);
            double lm_score = arc.weight.Weight().Value1()/lm_scale;
            double ac_score = arc.weight.Weight().Value2()/acoustic_scale;
            double this_posterior = this_alpha + arc_like + betas[arc.nextstate] - betas[0];
            this_posterior = exp(this_posterior);
            ss << key << ' ' << arc.olabel << ' ' << stime << ' ' << etime << ' ' 
               << lm_score << ' ' << ac_score << ' ' << this_posterior << "\n";
          }
        }
      }
      ko.Stream() << ss.str();
      n_done ++; 
    }
    ko.Close();
    return (n_done != 0 ? 0 : 1);
  } catch (const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}



