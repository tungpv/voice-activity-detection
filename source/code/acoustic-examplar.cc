#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include <sstream>
#include "mgb-lib.h"
#include "acoustic-examplar.h"

namespace kaldi {
void AcousticUnit::ReadWords(std::string &words) {
  bool binary_in;
  Input ki_words(words, &binary_in); 
  std::string line;
  const char *delim = " \t";
  std::vector<std::string> vec_str;
  while(std::getline(ki_words.Stream(), line)) {
    SplitStringToVector(line, delim, true, &vec_str);
    if(vec_str.size() != 2)
      KALDI_ERR << "bad line " << '"' << line 
                << '"' << " in file " << words;
    int wordId;
    ConvertStringToInteger(vec_str[1], &wordId);
    std::map<std::string, int>::iterator it = map_word2Id_.find(vec_str[0]), 
                                         it_end = map_word2Id_.end();
    if (it != it_end)
      KALDI_ERR << "duplication found: " << '"' << vec_str[0] << '"'
                << " in file " << words;
    map_word2Id_.insert(std::pair<std::string, int>(vec_str[0], wordId));
  }
  ki_words.Close();
}
void AcousticUnit::ReadSegments(std::string &segments) {
  bool binary_in;
  Input ki_segments(segments, &binary_in);
  std::vector<std::string> vec_str;
  const char *delim = " \t";
  int segId = 0;
  std::map<std::string, int> map_segFileToInt;
  typedef std::map<std::string, int>::iterator TypedIterator;
  std::string line;
  while(std::getline(ki_segments.Stream(), line)) {
    SplitStringToVector(line, delim, true, &vec_str);
    if(vec_str.size() != 4)
      KALDI_ERR << "bad line: " << '"' << line << '"'
                << " from file " << segments;
    std::string featFile = vec_str[0];
    std::string segFile = vec_str[1];
    TypedIterator it = map_segFileToInt.find(segFile),
                                         it_end = map_segFileToInt.end();
    if(it == it_end) {
      std::pair<TypedIterator, bool> ret = map_segFileToInt.insert(std::pair<std::string, int>(segFile, segId));
      if(!ret.second) 
        KALDI_ERR << "inserting " << '"' << segFile << '"' << " into map failed";
      it = ret.first;
      segId ++;
    }
    int curSegId = it->second;
    it = map_featFile2SegmentId_.find(featFile);
    it_end = map_featFile2SegmentId_.end();
    if(it != it_end)
      KALDI_ERR << "duplicated file " << '"' << segFile << '"' << " at line: "
                << line;
     map_featFile2SegmentId_.insert(std::pair<std::string, int>(featFile, curSegId));
  }
  ki_segments.Close();
}
void AcousticUnit::InsertAcousticUnit(int wordId, int segId, UnitTimeBoundary &unitBound) {
  std::map<int, std::map<int, std::vector<UnitTimeBoundary> > >::iterator it = map_wrd2boundary_.find(wordId);
  if(it == map_wrd2boundary_.end()) {
    
  }
}
void AcousticUnit::ReadCTMFile(std::string &uttCtmFile) {
  bool binary_in;
  Input ki_ctm(uttCtmFile, &binary_in);
  std::string line;
  std::vector<std::string> vec_str;
  const char *delim = " \t";
  while(std::getline(ki_ctm.Stream(), line)) {
    SplitStringToVector(line, delim, true, &vec_str);
    std::string featFile = vec_str[0];
    std::map<std::string, int>::iterator it = map_featFile2SegmentId_.find(featFile), 
                                 it_end = map_featFile2SegmentId_.end();
    if(it == it_end)
      KALDI_ERR << "File " << '"' << featFile << '"'
                <<  " in ctm file " << uttCtmFile << " not registered in segments";
    int segId = it->second;
    float start;
    ConvertStringToReal(vec_str[2], &start);
    float end;
    ConvertStringToReal(vec_str[3], &end);
    end += start;
    UnitTimeBoundary unitBound(start, end, featFile);
    it = map_word2Id_.find(vec_str[4]);
    if(it == map_word2Id_.end())
      KALDI_ERR << "word " << '"' << vec_str[4] << '"' << " is not in the words.txt file";
    int wordId = it->second;
    InsertAcousticUnit(wordId, segId, unitBound);
  }
  ki_ctm.Close(); 
}
void AcousticUnit::Init(std::string &words, std::string &segments, std::string &uttCtmFile) {
  ReadWords(words);
  ReadSegments(segments);
  ReadCTMFile(uttCtmFile);
}

} // namespace kaldi

