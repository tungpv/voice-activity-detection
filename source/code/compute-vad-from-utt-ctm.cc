#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "matrix/kaldi-matrix.h"

namespace kaldi {
struct CtmWord {
  int32 chanId;
  float start;
  float dur;
  std::string word;
  float conf;
  CtmWord() {}
  CtmWord(int32 channel_id, float start_time, float dur_time, std::string &word_string, float conf_score):
  chanId(channel_id), start(start_time), dur(dur_time), word(word_string), conf(conf_score) {}
  int32 StartFrames (const float frame_shift) {
    return SecToFrames(frame_shift, start);
  }
  int32 EndFrames (const float frame_shift) {
    return SecToFrames(frame_shift, start + dur);
  }
  void ShowMe() {
    std::cerr << "chanId: " << chanId << ", start: " << start
              << ", dur: " << dur << ", word: " << word
              << ", conf: " << conf << "\n";
  }
 private:
  int32 SecToFrames(const float frame_shift, const float second) {
    return static_cast<int32>(second / frame_shift);
  }
};
typedef std::vector<CtmWord> CtmUtt;
class CtmFile {
 public:
  CtmFile() { }
  void Read(std::string &ctm_filename) {
    bool binary_in;   
    Input input_kaldi(ctm_filename, &binary_in);
    std::string line;
    CtmWord value;
    while(std::getline(input_kaldi.Stream(), line)) {
      std::string key;
      SplitCtmLine(line, &key, &value);
      InsertCtmWord(key, value);
    }
    input_kaldi.Close();   
  }
  CtmUtt *GetCtmUtt(const std::string &key) {
    std::map<std::string, CtmUtt>::iterator it = map_utts_.find(key);
    if(it == map_utts_.end()) { 
      return NULL; 
    }
    return &(it->second);
  }
 private:
  void SplitCtmLine(std::string line, std::string *key, CtmWord *word) {
    std::vector<std::string> vec_str;
    const char *delim = " \t";
    SplitStringToVector(line, delim, true, &vec_str);
    if (vec_str.size() != 5 && vec_str.size() != 6)
      KALDI_ERR << "Bad line " << '"' << line << '"' << " in ctm file";
    *key = vec_str[0];
    int32 chan_id;
    ConvertStringToInteger(vec_str[1], &chan_id);
    float sec_start;
    ConvertStringToReal(vec_str[2], &sec_start);
    float sec_dur;
    ConvertStringToReal(vec_str[3], &sec_dur);
    std::string word_label = vec_str[4];
    float conf_score = 0;
    if (vec_str.size() == 6)
      ConvertStringToReal(vec_str[5], &conf_score);
    *word = CtmWord(chan_id, sec_start, sec_dur, word_label, conf_score);
    // word->ShowMe();
  }

  void InsertCtmWord(const std::string &key, const CtmWord &value) {
    std::map<std::string, std::vector<CtmWord> >::iterator it = map_utts_.find(key);
    if(it != map_utts_.end()) {
      CtmUtt &vec_word = it->second;
      vec_word.push_back(value);
    } else {
      CtmUtt ctm_utt;
      ctm_utt.push_back(value);
      map_utts_.insert( std::pair<std::string, CtmUtt>(key, ctm_utt) );
    }
  }
  std::map<std::string, CtmUtt> map_utts_;
};

}  // namespace kaldi

int main(int argc, char *argv[]) {
  try {
    using namespace kaldi;
    using kaldi::int32;
    
    const char *usage = 
        "making speech activity decision (SAD), using ASR utterance based ctm output\n"
        "Usage: compute-vad-from-utt-ctm [options] <utt-ctm> <feats-rspecifier> <vad-wspecifier>\n"
        "e.g.: compute-vad-from-utt-ctm utt.ctm scp:feats.scp ark:vad.ark\n";
    ParseOptions po(usage);
    float frame_shift = 0.01;
    po.Register("frame-shift", &frame_shift, "frame shift in terms of second");
    po.Read(argc, argv);
    
    if(po.NumArgs() != 3) {
      po.PrintUsage();
      exit(-1);
    }
    std::string ctm_filename = po.GetArg(1);
    std::string feat_rspecifier = po.GetArg(2);
    std::string vad_wspecifier = po.GetArg(3);
    CtmFile ctm_file;
    ctm_file.Read(ctm_filename);
    SequentialBaseFloatMatrixReader feat_reader(feat_rspecifier);
    BaseFloatVectorWriter vad_writer(vad_wspecifier);
    int32 num_done = 0, num_err = 0;
    for(; !feat_reader.Done(); feat_reader.Next(), num_done ++) {
      std::string utt = feat_reader.Key();
      Matrix<BaseFloat> feat(feat_reader.Value());
      if (feat.NumRows() == 0) {
        KALDI_WARN << "Empty feature matrix for utterance " << utt;
        num_err ++;
        continue;
      }
      CtmUtt *ctm_utt;
      if( (ctm_utt = ctm_file.GetCtmUtt(utt)) == NULL ) {
        KALDI_WARN << "Empty ctm file for utterance " << utt;
        num_err ++;
        continue;
      }
      if ((*ctm_utt)[ctm_utt->size()-1].EndFrames(frame_shift) > feat.NumRows()) {
        KALDI_ERR << "ctm ending frames: " <<  (*ctm_utt)[ctm_utt->size()-1].EndFrames(frame_shift) << ", " 
                  << "while feature frames: " << feat.NumRows() << ", for file " << '"' << utt << '"';
      }
      Vector<BaseFloat> vad_result(feat.NumRows());
      vad_result.SetZero();
      for(int32 i = 0; i < ctm_utt->size(); ++i) {
        CtmWord ctm_word = (*ctm_utt)[i];
        for(int32 j = ctm_word.StartFrames(frame_shift); j < ctm_word.EndFrames(frame_shift); ++j)
          vad_result(j) = 1;
      }
      vad_writer.Write(utt, vad_result);
    }
    KALDI_LOG << "ctm file based voice activity detection applied; "
              << "processed " << num_done << " utterances successfully; "
              << num_err << " had empty features or no ctm files.";
    return 0;
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
