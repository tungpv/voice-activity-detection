#ifndef MGB_LIB_H_
#define MGB_LIB_H_

#include <string>
#include <iostream>
#include <map>
namespace kaldi {
struct CTMItem {
  std::string spk;
  int32 chanId; 
  float start;
  float dur;
  std::string word;
  float conf;
};
struct SegmentItem {
  std::string segId;
  std::string sFile;
  float start;
  float end;
};
typedef std::vector<CTMItem> vecCTM;
// The following function refers to stackoverflow.com  with keyword 'c++ split string'
void SimpleSplitString(const std::string &s, const char delimiter, std::vector<std::string> *vec_str);
float TimeOverlap(const CTMItem &it1, const CTMItem &it2);
void DumpCTMLine(std::ostream &os, const std::string &key, const CTMItem &value, float *tot_len);
bool FindTimeOverlap(const std::map<std::string, 
                     vecCTM> &map_ctm, const std::string &key, const CTMItem &value, 
                     CTMItem *overlap_ctm,
                     const float overlap_thresh=0.6);
void CheckOrder(const std::map<std::string, vecCTM> &map_ctm);
void SortCTM(std::map<std::string, vecCTM> *map_ctm);
void SplitCTMLine (std::string &line, std::string *key, CTMItem *value);
void ReadCTMFile (std::istream &infile, std::map<std::string, vecCTM> *map_ctm);
void ReadSpkCTMFile (std::istream &infile, std::map<std::string, vecCTM> *map_ctm);
void ReadKaldiSegments(std::istream &infile, std::map<std::string, SegmentItem> *map_seg);

class StrMapInMap{
 public:
  StrMapInMap(std::string &spk2utt) {
    if(spk2utt.empty())
      return;
    Open(spk2utt);
  }
  StrMapInMap(){}
  ~StrMapInMap(){}
  void Open(const std::string &spk2utt);
  bool HasKey(const std::string &key);
  bool HasValue(const std::string &key, const std::string &value);
 private:
  std::map<std::string, std::map<std::string, unsigned short> >	map_; 
  std::map<std::string, std::map<std::string, unsigned short> >::iterator it_, end_;
};

} // namespace kaldi
#endif
