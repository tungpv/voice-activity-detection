#include <iostream>
#include <string>
#include <map>

void GetMaxDupStrLen(const std::string &str) {
  std::cout << "input string=" << str << "\n";
  std::string::const_iterator it = str.begin(),
  			      end = str.end();
  std::map<char, std::string> letter_table;
  int max_len = 1;
  std::string max_dupstr;
  for(; it != end; it++) {
    std::map<char, std::string>::iterator it_letter = letter_table.find(*it);
    if(it_letter != letter_table.end()) {
      // std::cout << "before string=" << it_letter->second << "\n";
      it_letter->second += *it;
      if (str.find(it_letter->second) != std::string::npos) {
        if(it_letter->second.length() > max_len)
          max_len = it_letter->second.length();
          max_dupstr = it_letter->second;
      }
      // std::cout << "after string=" << it_letter->second << "\n";
    } else {
      // std::cout << "here\t" << *it << "\n";
      const char c = *it;
      std::string dup_str = "";
      dup_str += *it;
      letter_table.insert(std::pair<char, std::string>(c, dup_str));
    }
  }
  std::cout << max_dupstr << "\t" << max_len << "\n";
}
int main () {
  using namespace std;
  string str_stdin;
  cin >> str_stdin;
  GetMaxDupStrLen(str_stdin); 
  return 0;
}
