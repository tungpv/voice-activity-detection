#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "hmm/posterior.h"
#include "mgb-lib.h"
#include <string>

namespace kaldi {
typedef std::vector<std::pair<std::string, std::vector<CTMItem> > > CTMVec;
void ReadCTMAsVec(std::istream &is, CTMVec *vec_ctm_vec) {
  std::string line, key = "", key_prev = "";
  std::vector<CTMItem> vec_ctm_item;
  while(std::getline(is, line)) {
    CTMItem value;
    SplitCTMLine(line, &key, &value);
    if(key_prev != "" && key_prev != key) {
      vec_ctm_vec->push_back(std::pair<std::string, std::vector<CTMItem> > (key_prev, vec_ctm_item));
      vec_ctm_item.clear();
    }
    vec_ctm_item.push_back(value);
    key_prev = key;
  }
  if(key != "") {
    vec_ctm_vec->push_back(std::pair<std::string, std::vector<CTMItem> > (key, vec_ctm_item));
  }
}
void SubsetCTMItem (const float conf_thresh, const CTMVec &vec_ctm_vec, float *total_selected, CTMVec *sub_vec) {
  *total_selected = 0;
  for(int32 i = 0; i < vec_ctm_vec.size(); i ++) {
    const std::vector<CTMItem> &vec_ctm_item  = vec_ctm_vec[i].second;
    std::vector<CTMItem> new_vec_ctm_item;
    for(int32 j = 0; j < vec_ctm_item.size(); j ++) {
      if(vec_ctm_item[j].conf >= conf_thresh) {
        new_vec_ctm_item.push_back(vec_ctm_item[j]);
        *total_selected += vec_ctm_item[j].dur;
      }
    }
    if(new_vec_ctm_item.size() > 0 && sub_vec != NULL) {
      sub_vec->push_back(std::pair<std::string, std::vector<CTMItem> >(vec_ctm_vec[i].first, new_vec_ctm_item));
    }
  }
}
void BinarySearchConfThresh(const float inc_step, const float hour_length, CTMVec &vec_ctm_vec, float *conf_thresh) {
  float total_selected = 0;
  const float total_expected = hour_length*3600;
  SubsetCTMItem(0, vec_ctm_vec, &total_selected, NULL);
  if (total_selected <= total_expected) {
    *conf_thresh = 0;
    return;
  }
  float conf_low = 0, conf_high = 1, conf_mid;
  while(conf_low < conf_high) {
    conf_mid = (conf_low + conf_high)/2;
    SubsetCTMItem(conf_mid, vec_ctm_vec, &total_selected, NULL);
    if (total_selected == total_expected) {
      *conf_thresh = conf_mid;
      return;
    }
    if(total_selected > total_expected) {
      if(conf_mid + inc_step > 1.0)
        break;
      conf_low = conf_mid + inc_step;
    } else {
      if (conf_mid - inc_step < 0)
        break;
      conf_high = conf_mid - inc_step;
    }
  }
  *conf_thresh = conf_mid;
}

} // namespace kaldi

int main(int argc, char *argv[]) {
  using namespace kaldi;
  try {
    const char *usage =
    "Extract subset ctm with specified length using confidence score\n"
    "e.g.:\n"
    "subset-ctm-with-post  --hour-length=5 old-ctm new-ctm\n";
    ParseOptions po(usage);
    float hlen = 0;
    po.Register("hour-length", &hlen, "hour-length data to be selected");
    float inc_step = 0.01;
    po.Register("inc-step", &inc_step, "search step length, once failed");
    po.Read(argc, argv);
    if(po.NumArgs() != 2) {
      po.PrintUsage();
      exit(-1);
    }
    std::string ctm_in_filename = po.GetArg(1);
    std::string ctm_out_filename = po.GetArg(2);
    if (hlen == 0) {
      KALDI_WARN << "no data selected with " << hlen << " hours";
      exit(-1);
    }
    bool binary_in; 
    Input ki_ctm(ctm_in_filename, &binary_in);
    CTMVec vec_ctm;
    ReadCTMAsVec(ki_ctm.Stream(), &vec_ctm);
    ki_ctm.Close();
    float conf_thresh, total_selected;
    BinarySearchConfThresh(inc_step, hlen, vec_ctm, &conf_thresh);
    CTMVec sub_vec;
    SubsetCTMItem(conf_thresh, vec_ctm, &total_selected, &sub_vec);
    if (sub_vec.size() > 0) {
      Output ko_ctm(ctm_out_filename, false);
      for(int32 i = 0; i < sub_vec.size(); i++) {
         const std::string &key = sub_vec[i].first;
        for(int32 j = 0; j < sub_vec[i].second.size(); j ++) {
          const CTMItem &value = sub_vec[i].second[j];
          ko_ctm.Stream() << key << " " << value.chanId << " " << value.start << " "
                          << " " << value.dur << " " << value.word << " " << value.conf << "\n";
        }
      }
      ko_ctm.Close();
      KALDI_LOG << "total selected " << total_selected/3600 << " hours, with threshold " << conf_thresh;
    }
    return 0;
  } catch (const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
