#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include <sstream>

#include <util-lib.h>

namespace kaldi {
void StrMapInMap::Open(const std::string &spk2utt) {
  bool binary_in;
  Input ki(spk2utt, &binary_in);
  std::istream &infile = ki.Stream();
  std::string line;
  int32 line_num = 0;
  while(std::getline(infile, line)) {
    line_num ++;
    std::vector<std::string> vec_str;
    const char delim = ' ';
    SplitStringToVector(line, &delim, true, &vec_str);
    if(vec_str.size() < 2)
      KALDI_ERR << "ERROR, bad line " << '"' << line << '"'
                << "at " << line_num; 
    std::string key = vec_str[0];
    std::map<std::string, unsigned short> map_utt;
    std::map<std::string, unsigned short>::iterator it, end;
    unsigned short value = 1;
    for(int32 i = 1; i < vec_str.size(); i++, value ++) {
      std::string key_utt = vec_str[i];
      it = map_utt.find(key_utt); 
      end = map_utt.end();
      if(it == end) {
        map_utt.insert(std::pair<std::string, unsigned int>(key_utt, value));       
      } else {
        KALDI_WARN << "duplicated utt name " << '"'
        << key_utt << '"';
      }
    }
    map_.insert(std::pair<std::string, std::map<std::string, unsigned short> >(key, map_utt));   
  }
  ki.Close();
}
bool StrMapInMap::HasKey(const std::string &key) {
  it_ = map_.find(key);
  end_ = map_.end();
  if (it_ == end_)
    return false;
  return true;
}
bool StrMapInMap::HasValue(const std::string &key, const std::string &value ) {
  if (HasKey(key)) {
    it_ = map_.find(key);
    std::map<std::string, unsigned short> & map_utt = it_->second;
    std::map<std::string, unsigned short>::iterator it, end;
    it = map_utt.find(value);
    end = map_utt.end();
    if (it == end) {
      return false;
    }
    return true; 
  }
  return false;
}

} // namespace kaldi
