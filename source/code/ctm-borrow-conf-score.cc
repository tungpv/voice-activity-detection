#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "mgb-lib.h"
#include <string>

namespace kaldi {

} // namespace kaldi

int main(int argc, char *argv[]) {
  using  namespace kaldi;
  try {
    const char *usage =
    "borrow conf score from decoded ctm for the aligned ctm.\n"
    "e.g.:\n"
    "ctm-borrow-conf-score <ali-ctm> <dec-ctm-with-conf-score> <ali-ctm-with-conf>\n";
    ParseOptions po(usage);
    float overlap_thresh = 0.6;
    po.Register("overlap-thresh", &overlap_thresh, "overlap threshold");
    po.Read(argc, argv);
    if(po.NumArgs() != 3) {
      po.PrintUsage();
      exit (-1);
    } 
    std::string ali_ctm_filename = po.GetArg(1);
    std::string dec_ctm_filename = po.GetArg(2);
    std::string out_ctm_filename = po.GetArg(3);
    bool binary_in;
    Input ki_dec_ctm(dec_ctm_filename, &binary_in);
    std::map<std::string, std::vector<CTMItem> > map_dec_ctm;
    ReadCTMFile(ki_dec_ctm.Stream(), &map_dec_ctm);
    ki_dec_ctm.Close(); 
    Input ki_ali_ctm(ali_ctm_filename, &binary_in);
    Output ko_ctm(out_ctm_filename, false);
    std::string line;
    float tot_len = 0;
    float loss_tot_len = 0;
    float avg_overlap_rate = 0;
    int tot_samples = 0;
    while(std::getline(ki_ali_ctm.Stream(), line)) {
      std::string key;
      CTMItem value, overlap_ctm;
      SplitCTMLine(line, &key, &value);
      if(FindTimeOverlap(map_dec_ctm, key, value, &overlap_ctm, overlap_thresh) == true) {
        value.conf = overlap_ctm.conf;
        avg_overlap_rate += TimeOverlap(value, overlap_ctm) / overlap_ctm.dur; 
        tot_samples ++; 
        DumpCTMLine(ko_ctm.Stream(), key, value, &tot_len);
      } else {
        KALDI_LOG << "an un-overlapped item: " 
                  << key << ' ' << value.start << ' '
                  << value.dur << ' '
                  << value.word << ' '
                  << value.conf;
        loss_tot_len += value.dur;
      }
    }
    char buf[1024];
    sprintf(buf, "%.2f", tot_len/3600);
    char sbuf[1024];
    sprintf(sbuf, "%.4f", avg_overlap_rate/tot_samples);
    KALDI_LOG << "total length: " << buf 
              << " hrs, loss data length: " << loss_tot_len/3600
              << " hrs, average overlap rate: " << sbuf; 
    ki_ali_ctm.Close();
    ko_ctm.Close();
    
    return 0;
  } catch (const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
  
}
