#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "mgb-lib.h"
#include <string>

namespace kaldi {
struct wordInfo {
  int32 stime;
  int32 etime;
  float post;
};
void ClusterUttWord(std::vector<std::vector<std::string> > &vector_utt,
                    std::map<int32, int32> &map_exclude,
                    std::map<int32, std::vector<wordInfo> > *map_word,
                    int32 *stime_utt, int32 *etime_utt) {
  *stime_utt = std::numeric_limits<int32>::max();;
  *etime_utt = 0;
  std::map<int32, int32>::iterator  it_exclude_end = map_exclude.end();
  for(int32 i = 0; i < vector_utt.size(); i ++) {
    std::vector<std::string> vector_word = vector_utt[i];
    int32 wordId;
    ConvertStringToInteger(vector_word[1], &wordId);
    if(map_exclude.find(wordId) != it_exclude_end) // skip over silence word
      continue;
    wordInfo winfo;
    ConvertStringToInteger(vector_word[2], &winfo.stime);
    ConvertStringToInteger(vector_word[3], &winfo.etime);
    ConvertStringToReal(vector_word[4], &winfo.post);
    if(winfo.stime < *stime_utt)
      *stime_utt = winfo.stime;
    if(winfo.etime > *etime_utt)
      *etime_utt = winfo.etime;
    std::map<int32, std::vector<wordInfo> >::iterator it = map_word->find(wordId), it_end = map_word->end(); 
    if(it == it_end) {
      std::vector<wordInfo> vector_winfo;
      vector_winfo.push_back(winfo);
      map_word->insert(std::pair<int32, std::vector<wordInfo> >(wordId, vector_winfo));
    } else {
      (it->second).push_back(winfo);
    }  
  }
}
void GetWordMaxPosterior(const int32 curFrameIndex, std::vector<wordInfo> &vec_wordInfo, float *posterior) {
  *posterior = 0;
  float tot_posterior = 0;
  for(int32 i = 0; i < vec_wordInfo.size(); i ++) {
    wordInfo winfo = vec_wordInfo[i];
    if(winfo.stime <= curFrameIndex && curFrameIndex < winfo.etime) {
      tot_posterior += winfo.post;
    }
  }
  *posterior = tot_posterior;
}
void GetMaxPosterior(const int32 curFrameIndex, std::map<int32, std::vector<wordInfo> > &word_cluster, float *max_post) {
  std::map<int32, std::vector<wordInfo> >::iterator it = word_cluster.begin(), end = word_cluster.end();
  *max_post = 0;
  for(; it != end; it ++) {
    float cur_posterior;
    GetWordMaxPosterior(curFrameIndex, it->second, &cur_posterior);
    if (cur_posterior > *max_post)
      *max_post = cur_posterior;
  }
}
void WriteOutUttPost(std::ostream &os, std::map<int32, int32> &map_exclude, 
                    std::vector<std::vector<std::string> > &vector_utt) {
  if(vector_utt.size() == 0)
    return;
  std::string utt_name = vector_utt[0][0];
  std::map<int32, std::vector<wordInfo> > map_word_cluster;
  float tot_posterior = 0;
  int32 start_frame, end_frame, effective_tot_frame = 0;
  ClusterUttWord(vector_utt, map_exclude, &map_word_cluster, &start_frame, &end_frame);
  for(int32 i = start_frame; i < end_frame; i ++) {
    float max_post = 0;
    GetMaxPosterior(i, map_word_cluster, &max_post);
    if(max_post > 0) {
      tot_posterior += max_post;
      effective_tot_frame ++;
    }
  }
  if(effective_tot_frame != 0)
    tot_posterior /= effective_tot_frame;
  os << utt_name << ' ' << tot_posterior << "\n";
}

}  // namespace kaldi

int main(int argc, char *argv[]) {
  using namespace kaldi;
  try {
    const char *usage =
     "Calculate utterance posterior from word posterior from lattice.\n"
     "e.g.: \n"
     "word-to-utt-post --exclude-words=2:3:4 word-post.txt utt-post.txt\n"
     "the word-post.txt is produced by lattice-to-word-post\n";
    ParseOptions po(usage);
    std::string exclude_words = "";
    po.Register("exclude-words", &exclude_words, "words that are not considered when we compute utterance posterior");
    po.Read(argc, argv);
    if (po.NumArgs() != 2) {
      po.PrintUsage();
      exit (-1);
    }
    std::string word_post_fname =po.GetArg(1);
    std::string utt_post_fname = po.GetArg(2);
    std::map<int32, int32> map_exclude;
    if(exclude_words != "") {
      std::vector<int32> vector_exclude;
      const char *delim = ": ";
      SplitStringToIntegers(exclude_words, delim, true, &vector_exclude);
      for(int32 i = 0; i < vector_exclude.size(); i ++) {
        map_exclude.insert(std::pair<int32, int32>(vector_exclude[i], i));
      }
    }
    bool binary_in;
    Input input_file(word_post_fname, &binary_in);
    Output output_file(utt_post_fname, false);
    std::string line, prev_utt_name = "";
    std::vector<std::vector<std::string> > vector_utt;
    while(std::getline(input_file.Stream(), line)) {
      std::vector<std::string> vector_word;
      const char *delim = "\t ";
      SplitStringToVector(line, delim, true, &vector_word);
      if(vector_word.size() != 5) {
        KALDI_ERR << "bad line: " << "'" <<line << "'" << " with fields="
		  << vector_word.size();
      }
      std::string utt_name = vector_word[0];
      if(prev_utt_name != "" && prev_utt_name != utt_name) {
        WriteOutUttPost(output_file.Stream(), map_exclude, vector_utt);
        vector_utt.resize(0);
      }
      vector_utt.push_back(vector_word);
      prev_utt_name = utt_name;
    }
    WriteOutUttPost(output_file.Stream(), map_exclude, vector_utt);
    input_file.Close();
    output_file.Close();
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
  return 0;
}
