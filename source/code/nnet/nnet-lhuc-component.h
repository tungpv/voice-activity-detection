#ifndef KALDI_NNET_NNET_LHUC_COMPONENT_H_
#define KALDI_NNET_NNET_LHUC_COMPONENT_H_


#include "nnet/nnet-component.h"
#include "nnet/nnet-utils.h"
#include "cudamatrix/cu-math.h"

namespace kaldi {
namespace nnet1 {
class LhucComponent : public UpdatableComponent {
 public:
  LhucComponent(int32 dim_in, int32 dim_out)
  : UpdatableComponent(dim_in, dim_out) {
    update_bias_.Resize(dim_in);
    lhuc_vec_.Resize(dim_in);
    lhuc_scale_vec_.Resize(dim_in);
    lhuc_sigmoid_scale_ = 2.0;
  }
  ~LhucComponent() {
    update_bias_.Resize(0);
    lhuc_vec_.Resize(0);
    lhuc_scale_vec_.Resize(0);
  }
  // initialize the vector buffer to update the lhuc parameter vector
  void SetUpdateBias(const Vector<BaseFloat>& vec) {
    KALDI_ASSERT(vec.Dim() == update_bias_.Dim());
    update_bias_.CopyFromVec(vec);
  }
  void SetSigmoidScale(const BaseFloat scale) {
    lhuc_sigmoid_scale_ = scale;
  }
  // initialize the lhuc parameter
  void SetLhucVector(const Vector<BaseFloat> &vec) {
    KALDI_ASSERT(vec.Dim() == lhuc_vec_.Dim());
    lhuc_vec_.CopyFromVec(vec);
    lhuc_scale_vec_.CopyFromVec(vec);
    Vector<BaseFloat> cpu_vec(vec.Dim()), cpu_vec_sig(vec.Dim());
    lhuc_vec_.CopyToVec(&cpu_vec); 
    cpu_vec_sig.Sigmoid(cpu_vec);
    cpu_vec_sig.Scale(lhuc_sigmoid_scale_);
    lhuc_scale_vec_.CopyFromVec(cpu_vec);
  }
  void LhucPropagate(CuMatrix<BaseFloat> *out) {
    KALDI_ASSERT(out->NumRows() == lhuc_scale_vec_.Dim());
    out->MulColsVec(lhuc_scale_vec_);
  }
  void LhucBackpropagate(CuMatrix<BaseFloat> &in, 
                         CuMatrix<BaseFloat> &out,
                         CuMatrix<BaseFloat> &in_diff,
                         CuMatrix<BaseFloat> *out_diff) {
    KALDI_ASSERT(in.NumCols() == lhuc_vec_.Dim());
    CuMatrix<BaseFloat> lhuc_mat(in);
    for(int32 i = 0; i < in.NumRows(); i++) {
      CuSubMatrix<BaseFloat> submat_one_row(lhuc_mat, i, 1, 0, lhuc_mat.NumCols());
      submat_one_row.CopyRowsFromVec(lhuc_vec_);
    }
    CuMatrix<BaseFloat> lhuc_act_grad(in);
    lhuc_act_grad.Sigmoid(lhuc_mat);
    lhuc_act_grad.Scale(lhuc_sigmoid_scale_);
    CuMatrix<BaseFloat> lhuc_backpropagate_buf(in_diff);
    lhuc_backpropagate_buf.MulElements(in);
    out_diff->SetZero();
    out_diff->DiffSigmoid(lhuc_act_grad, lhuc_backpropagate_buf);
  }
  void LhucUpdate(Component &comp, CuMatrix<BaseFloat> &diff) {
    KALDI_ASSERT(comp.IsUpdatable() == true);
    UpdatableComponent &uc = dynamic_cast<UpdatableComponent &>(comp);
    const BaseFloat lr = uc.GetTrainOptions().learn_rate;
    const BaseFloat mmt =uc.GetTrainOptions().momentum;
    update_bias_.AddRowSumMat(1.0, diff, mmt);
    lhuc_vec_.AddVec(-lr, update_bias_);   
  }
  int32 NumParams() const { return 0;}
  void GetParams(Vector<BaseFloat> * wei_copy) const { return; }
  void Update(const CuMatrixBase<BaseFloat> &input,
                      const CuMatrixBase<BaseFloat> &diff) { return; }
  void SetTrainOptions(const NnetTrainOptions &opts) {
    opts_ = opts;
  }
  
  std::string Info() const { return ""; }
  std::string InfoGradient() const { return ""; }
  Component* Copy() const { return NULL; }
  ComponentType  GetType() const { return kAffineTransform; }
  void PropagateFnc(const CuMatrixBase<BaseFloat> &in,
                            CuMatrixBase<BaseFloat> *out) {}
  void BackpropagateFnc(const CuMatrixBase<BaseFloat> &in,
                                const CuMatrixBase<BaseFloat> &out,
                                const CuMatrixBase<BaseFloat> &out_diff,
                                CuMatrixBase<BaseFloat> *in_diff) {}
  void InitData(std::istream &is) { }
  void ReadData(std::istream &is, bool binary) { }
  void WriteData(std::ostream &os, bool binary) const { }
  const NnetTrainOptions& GetTrainOptions() const { 
    return opts_; 
  }
 protected:
  NnetTrainOptions opts_;
 private:
  BaseFloat lhuc_sigmoid_scale_;
  CuVector<BaseFloat> update_bias_;
  CuVector<BaseFloat> lhuc_vec_;
  CuVector<BaseFloat> lhuc_scale_vec_;
};

}  // namespace nnet1
}  // namespace kaldi
#endif
