#include <utility>
#include <string>
#include <iostream>
#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "cudamatrix/cu-vector.h"
#include "cudamatrix/cu-matrix.h"
#include "cudamatrix/cu-device.h"
#include "mgb-lib.h"
namespace kaldi {
void NormalizeAndOutput(std::ostream &os, 
                        const std::vector<BaseFloat> &scores, 
                        const std::vector<std::vector<std::string> > &vec_search) {
  KALDI_ASSERT(scores.size() == vec_search.size());
  Vector<BaseFloat> vec_score;
  vec_score.Resize(scores.size());
  for(int32 i = 0; i < scores.size(); i ++) {
    vec_score(i) = scores[i];
  }
  int32 sizes = scores.size();
  BaseFloat mean = vec_score.Sum();
  mean /= sizes;
  BaseFloat var_std = vec_score.Norm(2.0);
  var_std = sqrt(var_std*var_std/sizes - mean *mean);
  KALDI_LOG << "test files: " << vec_search.size()
            << ", mean=" << mean << ", stdvar=" << var_std;
  for(int32 i = 0; i < vec_search.size();i ++) {
    std::vector<std::string> vec_item = vec_search[i];
    float start_sec, end_sec, score;
    ConvertStringToReal(vec_item[2], &start_sec);
    ConvertStringToReal(vec_item[3], &end_sec);
    ConvertStringToReal(vec_item[4], &score);
    score = (score - mean) / var_std;
    os << vec_item[0] << ' ' << vec_item[1] << ' '
       << start_sec << ' ' << end_sec << ' '
       << score << "\n";
  }
}

} // namespace kaldi

int main(int argc, char *argv[]) {
  using namespace kaldi;
  try {
    const char *usage =
    "Do mean and variance normalization\n"
    "qnorm-normalize [options] results.txt normalized-results.txt\n";
    
    ParseOptions po(usage);
    po.Read(argc, argv);
    if(po.NumArgs() != 2) {
      po.PrintUsage();
      exit(1);
    }
    std::string filename_input = po.GetArg(1);
    std::string filename_output = po.GetArg(2);
    bool binary_input;
    Input input(filename_input, &binary_input);
    Output output(filename_output, false);
    std::string line;
    const char *delim = " \t";
    std::string prev_query_name = "";
    std::vector<std::string> vec_str;
    std::vector<BaseFloat> vec_score;
    std::vector<std::vector<std::string> > vec_search;
    while(std::getline(input.Stream(), line)) {
      SplitStringToVector(line, delim, true, &vec_str);
      KALDI_ASSERT(vec_str.size() == 5);
      std::string query_name = vec_str[0];
      float score;
      ConvertStringToReal(vec_str[4], &score);
      if(prev_query_name != "" && prev_query_name != query_name) {
        NormalizeAndOutput(output.Stream(), vec_score, vec_search);
        prev_query_name = query_name;
        vec_score.resize(0);
        vec_score.push_back(score);
        vec_search.resize(0);
        vec_search.push_back(vec_str);
      } else {
        prev_query_name = query_name;
        vec_score.push_back(score);
        vec_search.push_back(vec_str);
      } 
    }
    if(!prev_query_name.empty()) {
      NormalizeAndOutput(output.Stream(), vec_score, vec_search);
    }
    input.Close();
    output.Close();
  } catch (std::exception &e) {
    std::cerr << e.what();
    exit(1);
  }
  return 0;  
} 
