#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "hmm/posterior.h"
#include "mgb-lib.h"
#include <string>

namespace kaldi {

void WriteCTMWithUpdatedPost(std::ostream &os,const std::string &key, const Matrix<BaseFloat> &features, 
                             const std::vector<CTMItem> &vec_ctm_item) {
  int32 nItem = vec_ctm_item.size();
  KALDI_ASSERT (features.NumRows() == nItem);
  for(int32 i = 0; i < nItem; i ++) {
    BaseFloat new_post = features(i, 0);
    const CTMItem *ptr_ctm = &vec_ctm_item[i];
    os << key << " " << ptr_ctm->chanId << " "
       << ptr_ctm->start << " " << ptr_ctm->dur << " " << ptr_ctm->word << " " << new_post << "\n";
  }
}

} // namespace kaldi

int main (int argc, char *argv[]) {
  using namespace kaldi;
  try {
    const char *usage = 
    "Rerank ctm post with new posterior probably estimated from neural network\n"
    "e.g.:\n"
    "rerank-ctm-post old-utt-ctm ark:post-feats.ark new-utt-ctm\n";
    ParseOptions po(usage);
    po.Read(argc, argv);
    if(po.NumArgs() != 3) {
      po.PrintUsage();
      exit(-1);
    }
    std::string utt_ctm_rfilename = po.GetArg(1);
    std::string post_rspecifier = po.GetArg(2);
    std::string utt_ctm_wfilename = po.GetArg(3);
    bool binary_in;
    RandomAccessBaseFloatMatrixReader post_reader(post_rspecifier);
    Matrix<BaseFloat> features;
    Input ki_utt_ctm(utt_ctm_rfilename, &binary_in);
    std::string key = "", key_prev = "", line;
    std::vector<CTMItem> vec_ctm_item;
    Output ko_utt_ctm(utt_ctm_wfilename, false);
    while(std::getline(ki_utt_ctm.Stream(), line)) {
      CTMItem value; 
      SplitCTMLine(line, &key, &value);
      if (key_prev != "" && key_prev != key) {
        if (! post_reader.HasKey(key_prev)) {
          KALDI_ERR << "utterance " << '"' << key_prev << '"' << " has no features";
        }
        features = post_reader.Value(key_prev);
        WriteCTMWithUpdatedPost(ko_utt_ctm.Stream(), key_prev, features, vec_ctm_item);
        vec_ctm_item.clear();
      }
      vec_ctm_item.push_back(value);
      key_prev = key;     
    }
    if(key != "") {
      features = post_reader.Value(key);
      WriteCTMWithUpdatedPost(ko_utt_ctm.Stream(), key, features, vec_ctm_item);
    }
    ki_utt_ctm.Close();
    ko_utt_ctm.Close();
    return 0;
  } catch (const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
