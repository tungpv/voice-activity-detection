#include "base/kaldi-common.h"
#include "util/stl-utils.h"


namespace kaldi {
void DumpVector (std::vector<std::pair<int32, int16> > &v, const std::string &strInfo) {
  std::cout << "\n" << strInfo <<std::endl;
  for (size_t i = 0; i < v.size(); i++) {
    std::cout << v[i].first <<", " << v[i].second << std::endl;
  }
}
void TestMergePairVectorSumming() {
  for (int p = 0; p < 100; p++) {
    std::vector<std::pair<int32, int16> > v;
    std::map<int32, int16> m;
    int sz = Rand() % 10;
    for (size_t i = 0; i < sz; i++) {
      int32 key = Rand() % 10;
      int16 val = (Rand() % 5) - 2;
      v.push_back(std::make_pair(key, val));
      if (m.count(key) == 0) m[key] = val;
      else m[key] += val;
    }
    DumpVector(v, "before operation");
    MergePairVectorSumming(&v);
    DumpVector(v, "after operation");
    
    KALDI_ASSERT(IsSorted(v));
    for (size_t i = 0; i < v.size(); i++) {
      KALDI_ASSERT(v[i].second == m[v[i].first]);
      KALDI_ASSERT(v[i].second != 0.0);
      if (i > 0) KALDI_ASSERT(v[i].first > v[i-1].first);
    }
    for (std::map<int32, int16>::const_iterator iter = m.begin();
         iter != m.end(); ++iter) {
      if (iter->second != 0) {
        size_t i;
        for (i = 0; i < v.size(); i++)
          if (v[i].first == iter->first) break;
        KALDI_ASSERT(i != v.size()); // Or we didn't find this
        // key in v.
      }
    }
  }
}
void TestRandInt() {
  int min_value = 0;
  int max_value = 20;
  RandomState state;
  int num = Rand(&state) % 20;
  std::cout << "num=" << num << "\n";
  for(int i = 0; i < 40; ++i) {
    std::cout << RandInt(min_value, max_value) << ", ";
  }
  std::cout << std::endl;
}
}

int main(int argc, char**argv) {
  using namespace kaldi;
  TestMergePairVectorSumming();
  TestRandInt();
}
