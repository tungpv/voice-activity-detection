#ifndef UTIL_LIB_H_
#define UTIL_LIB_H_

#include <string>
#include <iostream>
#include <map>
namespace kaldi {
class StrMapInMap{
 public:
  StrMapInMap(std::string &spk2utt) {
    if(spk2utt.empty())
      return;
    Open(spk2utt);
  }
  StrMapInMap();
  ~StrMapInMap(){}
  void Open(const std::string &spk2utt);
  bool HasKey(const std::string &key);
  bool HasValue(const std::string &key, const std::string &value);
 private:
  std::map<std::string, std::map<std::string, unsigned short> >	map_; 
  std::map<std::string, std::map<std::string, unsigned short> >::iterator it_, end_;
};
} // namespace
#endif
