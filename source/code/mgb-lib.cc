#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include <sstream>
#include "mgb-lib.h"

namespace kaldi {

void SplitSpkCTMLine (std::string &line, std::string *key, CTMItem *value) {
  std::string rest;
  SplitStringOnFirstSpace(line, key, &rest);
  std::string rest2, str_tmp;
  SplitStringOnFirstSpace(rest, &value->spk, &rest2);
  rest =rest2;
  SplitStringOnFirstSpace(rest, &str_tmp, &rest2);
  ConvertStringToInteger(str_tmp,&value->chanId);
  SplitStringOnFirstSpace(rest2, &str_tmp, &rest);
  ConvertStringToReal(str_tmp, &value->start);
  SplitStringOnFirstSpace(rest, &str_tmp, &rest2);
  ConvertStringToReal(str_tmp, &value->dur);
  SplitStringOnFirstSpace(rest2, &value->word, &rest);
  value->conf = 0;
  if(!rest.empty())
    ConvertStringToReal(rest, &value->conf);
}
void ReadSpkCTMFile (std::istream &infile, std::map<std::string, std::vector<CTMItem> > *map_ctm) {
  std::string line;
  while(std::getline(infile, line)) {
    std::string key;
    CTMItem value;
    SplitSpkCTMLine(line, &key, &value);
    std::map<std::string, vecCTM>::iterator it_map = map_ctm->find(key),
                                                  it_map_end = map_ctm->end();
    if(it_map != it_map_end) {
      vecCTM &vec_ctm = it_map->second;
      vec_ctm.push_back(value);
    } else {
      vecCTM vec_ctm;
      vec_ctm.push_back(value);
      map_ctm->insert(std::pair<std::string, vecCTM>(key, vec_ctm));
    }
  }
  SortCTM(map_ctm);
  CheckOrder(*map_ctm);
}

void SplitCTMLine (std::string &line, std::string *key, CTMItem *value) {
  std::vector<std::string> vec_str;
  const char *delim = " \t";
  SplitStringToVector(line, delim, true, &vec_str);
  if(!(vec_str.size() == 5 || vec_str.size() == 6)) 
    KALDI_ERR << "bad ctm line " << '"' << line << '"';
  *key = vec_str[0];
  ConvertStringToInteger(vec_str[1], &value->chanId);
  ConvertStringToReal(vec_str[2], &value->start);
  ConvertStringToReal(vec_str[3], &value->dur);
  value->word = vec_str[4];
  value->conf = 0;
  if(vec_str.size() == 6)
  ConvertStringToReal(vec_str[5], &value->conf);

  if (0) { // to be deleted
    std::string rest;
    SplitStringOnFirstSpace(line, key, &rest);
    std::string rest2, str_tmp;
    SplitStringOnFirstSpace(rest, &str_tmp, &rest2);
    ConvertStringToInteger(str_tmp,&value->chanId);
    SplitStringOnFirstSpace(rest2, &str_tmp, &rest);
    ConvertStringToReal(str_tmp, &value->start);
    SplitStringOnFirstSpace(rest, &str_tmp, &rest2);
    ConvertStringToReal(str_tmp, &value->dur);
    SplitStringOnFirstSpace(rest2, &value->word, &rest);
    value->conf = 0;
    if(!rest.empty())
      ConvertStringToReal(rest, &value->conf);
  }
}
void DumpCTMLine(std::ostream &os, const std::string &key, const CTMItem &value, float *tot_len) {
  os << key << " " << value.spk << " " << value.chanId << " "
              << value.start << " " << value.dur << " " << value.word << " " << value.conf << "\n";
  *tot_len += value.dur;
}

void ReadCTMFile (std::istream &infile, std::map<std::string, std::vector<CTMItem> > *map_ctm) {
  std::string line;
  while(std::getline(infile, line)) {
    std::string key;
    CTMItem value;
    SplitCTMLine(line, &key, &value);
    std::map<std::string, vecCTM>::iterator it_map = map_ctm->find(key),
                                                  it_map_end = map_ctm->end();
    if(it_map != it_map_end) {
      vecCTM &vec_ctm = it_map->second;
      vec_ctm.push_back(value);
    } else {
      vecCTM vec_ctm;
      vec_ctm.push_back(value);
      map_ctm->insert(std::pair<std::string, vecCTM>(key, vec_ctm));
    }
  }
  SortCTM(map_ctm);
  CheckOrder(*map_ctm);
}
struct ctm_sort {
  bool operator()(const CTMItem a, const CTMItem b) {
    return a.start < b.start;
  }
} sort_ctm;
void SortCTM(std::map<std::string, vecCTM> *map_ctm) {
  std::map<std::string, vecCTM>::iterator it = map_ctm->begin(), it_end = map_ctm->end();
  for(;it != it_end; it ++) {
    vecCTM &vec_ctm = it->second;
    std::sort(vec_ctm.begin(), vec_ctm.end(), sort_ctm);
  }
}
void CheckOrder(const std::map<std::string, vecCTM> &map_ctm) {
  std::map<std::string, vecCTM>::const_iterator it = map_ctm.begin(), it_end = map_ctm.end();
  for(;it != it_end; it ++) {
    const vecCTM &vec_ctm = it->second;
    for(size_t i = 0; i < vec_ctm.size() - 1; i++) {
      if(vec_ctm[i].start > vec_ctm[i+1].start)
        KALDI_WARN << "ctm is not sorted";
    }
  }
}
float TimeOverlap(const CTMItem &it1, const CTMItem &it2) {
  float start1 = it1.start, end1 = it1.start + it1.dur;
  float start2 = it2.start, end2 =it2.start + it2.dur;
  if(end2 <= start1 || end1 <= start2)
    return 0;
  float start = start2;
  if(start2 <= start1)
    start = start1;
  float end = end1;
  if(end2 <= end1)
    end = end2;
  return end - start;
}
void MaxLeftOverlap(const vecCTM &vec_ctm, const int32 start, const CTMItem &value, int32 *pos, float *overlap) {
  int32 j = start -1;
  if (0 <= j) {
    float cur_overlap = TimeOverlap(vec_ctm[j], value);
    while(0 <= j && cur_overlap > 0) {
      if (*overlap < cur_overlap) {
        *overlap = cur_overlap;
        *pos = j;
      }
      j --;
      if (0 <= j)
        cur_overlap = TimeOverlap(vec_ctm[j], value);
    }
  }
}
void MaxRightOverlap(const vecCTM &vec_ctm, const int32 start, const CTMItem &value, int32 *pos, float *overlap) {
  int32 j = start + 1;
  if(j < vec_ctm.size()) {
    float cur_overlap = TimeOverlap(vec_ctm[j], value);
    while (j < vec_ctm.size() && cur_overlap > 0) {
      if (*overlap < cur_overlap) {
        *overlap = cur_overlap;
        *pos = j;
      }
      j ++;
      if(j < vec_ctm.size())
        cur_overlap = TimeOverlap(vec_ctm[j], value);
    }
  }
} 
void MaxOverlap(const vecCTM &vec_ctm, const int32 start, const CTMItem &value, int32 *pos, float *overlap) {
  MaxLeftOverlap(vec_ctm, start, value, pos, overlap);
  MaxRightOverlap(vec_ctm, start, value, pos, overlap);
} 
bool FindTimeOverlap(const std::map<std::string, vecCTM> &map_ctm, 
                     const std::string &key, const CTMItem &value, CTMItem *overlap_ctm,
                     const float overlap_thresh) {
  std::map<std::string, vecCTM>::const_iterator it = map_ctm.find(key), it_end = map_ctm.end();
  int32 max_overlap_pos;
  if(it == it_end)
    return false;
  const vecCTM &vec_ctm = it->second;
  int32 low = 0, high = vec_ctm.size() - 1;
  while(low < high) {
    int32 x = (low + high)/2;
    if(value.start == vec_ctm[x].start) {
      float overlap = value.dur;
      if(overlap > vec_ctm[x].dur)
        overlap = vec_ctm[x].dur;
      max_overlap_pos = x;
      MaxOverlap(vec_ctm, x, value, &max_overlap_pos, &overlap);
      *overlap_ctm = vec_ctm[max_overlap_pos];
      if (overlap/value.dur > overlap_thresh) {
        return true;
      }
      return false;  
    }
    if(value.start < vec_ctm[x].start) {
      if (vec_ctm[x].start - value.start < value.dur) {
        // float overlap = value.start + value.dur - vec_ctm[x].start;
        float overlap = TimeOverlap(vec_ctm[x], value);
        max_overlap_pos = x;
        MaxOverlap(vec_ctm, x, value, &max_overlap_pos, &overlap);
        *overlap_ctm = vec_ctm[max_overlap_pos];
        if (overlap/value.dur > overlap_thresh) {
          return true;
        }
        return false;
      }
      high = x - 1;
    } else if (value.start > vec_ctm[x].start) {
      if(value.start - vec_ctm[x].start < vec_ctm[x].dur) {
        // float overlap = vec_ctm[x].start + vec_ctm[x].dur - value.start;
        float overlap = TimeOverlap(vec_ctm[x], value);
        max_overlap_pos = x;
        MaxOverlap(vec_ctm, x, value, &max_overlap_pos, &overlap);
        *overlap_ctm = vec_ctm[max_overlap_pos];
        if(overlap/value.dur > overlap_thresh) {
          return true;
        }
        return false;  
      }
      low = x + 1;
    }  
  }
  float overlap = TimeOverlap(vec_ctm[low], value);
  *overlap_ctm = vec_ctm[low];
  if (overlap/value.dur > overlap_thresh)
    return true;
  return false; 
}
 
// The following function refers to stackoverflow.com  with keyword 'c++ split string'
void SimpleSplitString(const std::string &s, const char delimiter, std::vector<std::string> *vec_str) {
  std::stringstream ss(s);
  std::string elem;
  vec_str->clear();
  while(std::getline(ss, elem, delimiter)) {
    if(!elem.empty())
      vec_str->push_back(elem);
  } 
}
void ReadKaldiSegments(std::istream &infile, std::map<std::string, SegmentItem> *map_seg) {
  std::string line;
  std::vector<std::string> vec_str;
  while(std::getline(infile, line)) {
    const char *delim = " \t";
    SplitStringToVector(line, delim, true, &vec_str);
    if(vec_str.size() != 4)
      KALDI_ERR << "bad segment line " << '"' << line << '"';
    SegmentItem sItem;
    sItem.segId = vec_str[0];
    sItem.sFile = vec_str[1];
    ConvertStringToReal(vec_str[2], &sItem.start);
    ConvertStringToReal(vec_str[3], &sItem.end);
    std::map<std::string, SegmentItem>::iterator it, it_end = map_seg->end();
    it = map_seg->find(sItem.segId);
    if (it != it_end) {
      KALDI_WARN << "duplicated line " << '"' << line << '"';
      continue;
    }
    map_seg->insert(std::pair<std::string, SegmentItem>(vec_str[0], sItem));
  }
}
void StrMapInMap::Open(const std::string &spk2utt) {
  bool binary_in;
  Input ki(spk2utt, &binary_in);
  std::istream &infile = ki.Stream();
  std::string line;
  int32 line_num = 0;
  while(std::getline(infile, line)) {
    line_num ++;
    std::vector<std::string> vec_str;
    const char delim = ' ';
    SplitStringToVector(line, &delim, true, &vec_str);
    if(vec_str.size() < 2)
      KALDI_ERR << "ERROR, bad line " << '"' << line << '"'
                << "at " << line_num; 
    std::string key = vec_str[0];
    std::map<std::string, unsigned short> map_utt;
    std::map<std::string, unsigned short>::iterator it, end;
    unsigned short value = 1;
    for(int32 i = 1; i < vec_str.size(); i++, value ++) {
      std::string key_utt = vec_str[i];
      it = map_utt.find(key_utt); 
      end = map_utt.end();
      if(it == end) {
        map_utt.insert(std::pair<std::string, unsigned int>(key_utt, value));       
      } else {
        KALDI_WARN << "duplicated utt name " << '"'
        << key_utt << '"';
      }
    }
    map_.insert(std::pair<std::string, std::map<std::string, unsigned short> >(key, map_utt));   
  }
  ki.Close();
}
bool StrMapInMap::HasKey(const std::string &key) {
  it_ = map_.find(key);
  end_ = map_.end();
  if (it_ == end_)
    return false;
  return true;
}
bool StrMapInMap::HasValue(const std::string &key, const std::string &value ) {
  if (HasKey(key)) {
    it_ = map_.find(key);
    std::map<std::string, unsigned short> & map_utt = it_->second;
    std::map<std::string, unsigned short>::iterator it, end;
    it = map_utt.find(value);
    end = map_utt.end();
    if (it == end) {
      return false;
    }
    return true; 
  }
  return false;
}


} // namespace kaldi
