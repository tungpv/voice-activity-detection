#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <map>
#include <exception>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <cstring>

namespace Apple {
class RandNumber {
 public:
  RandNumber(const int lower_bound,
             const int upper_bound):lower_bound_(lower_bound),
                                    upper_bound_(upper_bound) {
    srand(time(NULL));
  }
  RandNumber() {
    srand(time(NULL));
    lower_bound_ = 0;
    upper_bound_ = 100;
  }
  int GetNum() {
    return rand()%(upper_bound_ - lower_bound_ + 1) + lower_bound_;
  }
 private:
  int lower_bound_;
  int upper_bound_;    
};
class StringSolution {
 public:
  StringSolution() {
  }
  static void Init() {

  }
 private:
};
void GenerateRandString(RandNumber &randNumber, std::string *str) {
  static char const *strBase = "0123456789"
                         "abcdefghijklmnopqrstuvwxyz"
                         "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                         "`~!@#$%^&*()_-+=\\|?/>.,<\'\"";
  const int nStrLen = strlen(strBase);
  // std::cout << "template string length " << nStrLen << std::endl;
  int nRandomLen = randNumber.GetNum();
  while(nRandomLen == 0) {
    nRandomLen = randNumber.GetNum();
  }
  str->clear();
  for(int i = 0; i < nRandomLen; ++i) {
    str->push_back(strBase[randNumber.GetNum()%nStrLen]);
  }
} 
struct TreeNode {
  int value;
  TreeNode *left;
  TreeNode *right; 
};
struct TreeNode2Parents {
  TreeNode *node;
  char pos;
  int level;
  int index_self;
  int index_parents;
};
class Tree {
 public:
  Tree(const int nLayer, RandNumber *rand_num_ptr) {
    nLayer_ = nLayer;
    if(nLayer_ <= 0) {
      std::cerr << "Illegal Tree layer (" 
                << nLayer_ << ")\n";
      exit(-1); 
    }
    if(rand_num_ptr == NULL) {
      std::cerr << "random number generator is not given" << std::endl;
      exit(-1);
    } 
    rand_num_ptr_ = rand_num_ptr;
    root_ = NULL;
  }
  ~Tree() {
    CutTree();
  }
  void ShowTree(); 
  TreeNode *GenTree();  
  int TreeLevel();
 private:
  bool MakeNextGeneration(const std::vector<TreeNode*> &vec_parents, 
                          std::vector<TreeNode*> *vec_children );
  void PreOrderTraverse(TreeNode *node, std::vector<std::vector<TreeNode2Parents> > *vec_node2parents);
  void RecursivePreOrderTraverse(TreeNode *node, std::vector<TreeNode*> *vecNodes);
  void CutTree(); 
  TreeNode* NewNode(const int value) {
    TreeNode *node = new TreeNode;
    node->value = value;
    node->left = node->right = NULL;
    return node;
  }
  void ShowOneLevelNodes(std::vector<TreeNode*>& vec_node);
  int RecursiveTreeLevel(TreeNode *node) {
    if (node == NULL)
      return 0;
    return(1+ std::max(RecursiveTreeLevel(node->left), RecursiveTreeLevel(node->right)));
  }
  int BFSTreeLevel(TreeNode *node);
  int nLayer_;
  TreeNode *root_;
  std::vector<int> vec_value_;  // for debug
  RandNumber  *rand_num_ptr_;
};
void Tree::ShowTree() {
  std::vector<std::vector<TreeNode2Parents> > vec_node2parents;
  PreOrderTraverse(root_, &vec_node2parents);
  for(int i = 0; i < vec_node2parents.size();  ++i) {
    std::cout << std::endl;
    for(int j = 0; j < vec_node2parents[i].size(); ++j) {
      std::cout << '(' << vec_node2parents[i][j].node->value << ", "
                << "'" << vec_node2parents[i][j].pos << "', "
                << vec_node2parents[i][j].level << ", "
                << vec_node2parents[i][j].index_self << ", "
                << vec_node2parents[i][j].index_parents << ")  ";
    }
  }
  std::cout <<std::endl;
}
int Tree::TreeLevel() {
  int nTreeLevel;
  nTreeLevel = RecursiveTreeLevel(root_);
  int nTreeLevel02;
  nTreeLevel02 = BFSTreeLevel(root_);
  if(nTreeLevel02 != nTreeLevel) {
    std::cerr << "inconsistent tree level obtained, with recursive " << nTreeLevel
              << ", " << nTreeLevel02 << std::endl;
    exit(-1);
  }
  return nTreeLevel;
}
int Tree::BFSTreeLevel(TreeNode *node) {
  if (node == NULL)
    return 0;
  int nLevel = 0;
  std::queue<TreeNode*> queue_temp;
  queue_temp.push(node);
  while(!queue_temp.empty()) {
    nLevel ++;
    int size_curr = queue_temp.size();
    for(int i = 0; i < size_curr; ++i) {
      TreeNode *node_curr = queue_temp.front(); 
      queue_temp.pop();
      if(node_curr->left != NULL)
        queue_temp.push(node_curr->left);
      if(node_curr->right != NULL)
        queue_temp.push(node_curr->right);
    }
  }
  return nLevel; 
}
/// traverse the tree with preorder way, but no recursive call
/// We require each node remembers his own index and his parents' index
void Tree::PreOrderTraverse(TreeNode *node, std::vector<std::vector<TreeNode2Parents> >  *vec_node2parents) {
  
  if(node == NULL)
    return;
  std::queue<TreeNode*> queue_temp;
  std::map<TreeNode*, std::pair<int, int> > map_node2level;
  int level = 0, index = 0;
  map_node2level.insert(std::pair<TreeNode*, std::pair<int, int> >(node, std::pair<int, int>(level, index)));
  TreeNode2Parents node2parents = {node, 'N', 0, 0, -1};
  std::vector<TreeNode2Parents> vec_level_nodes;
  vec_level_nodes.push_back(node2parents);
  vec_node2parents->push_back(vec_level_nodes);
  queue_temp.push(node);
  while(!queue_temp.empty()) {
    TreeNode *node_cur = queue_temp.front();
    std::map<TreeNode*, std::pair<int, int> >::iterator it, it_end = map_node2level.end();
    it = map_node2level.find(node_cur);
    if(it == it_end) {
      std::cerr << "an unregistered node found" << std::endl;
      exit(-1);
    }
    level = (it->second).first, index = (it->second).second;
    queue_temp.pop();
    if(node_cur->left != NULL) {
      queue_temp.push(node_cur->left);
      int new_index = 0;
      if(vec_node2parents->size() > level + 1) {
        new_index = (*vec_node2parents)[level+1].size();
      }
      map_node2level.insert(std::pair<TreeNode*, std::pair<int, int> >(node_cur->left, std::pair<int, int> (level+1, new_index)));
      TreeNode2Parents node2parents = {node_cur->left, 'L', level + 1, new_index, index};
      if(vec_node2parents->size() > level + 1)
        (*vec_node2parents)[level+1].push_back(node2parents);
      else {
        if(vec_node2parents->size() != level+1) {
          std::cerr << "vec_node2parents size (" << vec_node2parents->size() << ')'
                    << "is unequal to level+1 (" << level+1 << ')' << std::endl;
          exit(-1);
        }
        std::vector<TreeNode2Parents> vec_level_nodes;
        vec_level_nodes.push_back(node2parents);
        vec_node2parents->push_back(vec_level_nodes);
      }
    }
    if(node_cur->right != NULL) {
      queue_temp.push(node_cur->right);
      if(vec_node2parents->size() <= level +1) {
        std::cerr << "size of vec_node2parents (" << vec_node2parents->size() << ")" 
                  << " should over " << level + 1;
        exit(-1);
      }
      int new_index = (*vec_node2parents)[level+1].size();
      map_node2level.insert(std::pair<TreeNode*, std::pair<int, int> >(node_cur->right, std::pair<int, int> (level+1, new_index)));
      TreeNode2Parents node2parents = {node_cur->right, 'R', level + 1, new_index, index};
      (*vec_node2parents)[level+1].push_back(node2parents);
    }
  } 
}
void Tree::RecursivePreOrderTraverse(TreeNode *node, std::vector<TreeNode*> *vecNode) {
  if(node == NULL)
    return;
  if (vecNode == NULL) {
    std::cout << node->value << ' ';
  } else {
    vecNode->push_back(node);
  }
  if (node->left)
    RecursivePreOrderTraverse(node->left, vecNode);
  if (node->right)
    RecursivePreOrderTraverse(node->right, vecNode);
}
/// if current layer has zero children generated, we return false, and the generation
/// stopped.
bool Tree::MakeNextGeneration(const std::vector<TreeNode*> &vec_parents,
                              std::vector<TreeNode*> *vec_children) {
  vec_children->resize(0);
  size_t totalChildren = 0;
  for(size_t i = 0; i < vec_parents.size(); ++i) {
    TreeNode *node = vec_parents[i];
    int nChildren = rand_num_ptr_->GetNum()%3; // randomly generate 0,1,2 children
    // std::cerr << nChildren << " Children generated" << std::endl;
    totalChildren += nChildren;
    if(nChildren == 0)
      continue;
    node->left = NewNode(rand_num_ptr_->GetNum());
    // std::cerr << "node->left " << node->left->value << ' ';
    vec_children->push_back(node->left);
    if(nChildren == 1) {
      // std::cerr << std::endl;
      continue;
    }
    node->right = NewNode(rand_num_ptr_->GetNum()); 
    // std::cerr << "node->right " << node->right->value << ' ';
    vec_children->push_back(node->right);  
    // std::cerr << std::endl;
  }
  return totalChildren != 0;
}
void Tree::ShowOneLevelNodes(std::vector<TreeNode*>& vec_node) {
  for(int i = 0; i < vec_node.size(); ++ i) {
    TreeNode *node = vec_node[i];
    std::cerr << node->value << ' ';
  }
  std::cerr << std::endl;
}
TreeNode* Tree::GenTree() {
  if(root_ != NULL)
    return root_;
  int value = rand_num_ptr_->GetNum();
  root_ = NewNode(value);
  if(nLayer_ == 1) 
    return root_;
  std::vector<TreeNode*> vec_parents;
  vec_parents.push_back(root_);
  for(int i = 1; i < nLayer_; ++i) {
    std::vector<TreeNode*>vec_children;
    // ShowOneLevelNodes(vec_parents);
    for(int j = 0; j < vec_parents.size(); ++ j) {
      TreeNode *node = vec_parents[j];
      vec_value_.push_back(node->value);
    }
    if(!MakeNextGeneration(vec_parents,&vec_children))
      break;
    vec_parents = vec_children;
  } 
}
void Tree::CutTree() {
  std::vector<TreeNode*> vecNode;
  RecursivePreOrderTraverse(root_, &vecNode);
  for(size_t i = 0; i < vecNode.size(); ++ i) {
    delete vecNode[i];
  }
  vecNode.resize(0);
}

} // namespace Apple

