#include "apple.h"

int main (int argc, char *argv[]) {
  using namespace Apple;
  try {
    RandNumber rand_number(1, 1000);
    int nTree = rand_number.GetNum()%20 + 1;
    std::cerr << nTree << " tree to be created" << std::endl;
    for(int i = 0; i < nTree ; ++ i) {
      int nLayer = rand_number.GetNum()%10 + 1;
      std::cerr << nLayer << " layer tree generated" << std::endl;
      Tree tree(nLayer, &rand_number);
      tree.GenTree();
      tree.ShowTree();
      int nLevel = tree.TreeLevel();
      std::cout << "Current Tree depth: " << nLevel << std::endl;
    }
    std::string rand_str;
    GenerateRandString(rand_number, &rand_str);
    std::cout << "Current string: " << rand_str << std::endl;      
    return 0;
  } catch (std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
  return 0;
}
