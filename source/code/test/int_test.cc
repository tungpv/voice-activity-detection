#include <iostream>
#include <string>
#include <limits>

int main(int argc, char *argv[]) {
  int imin = std::numeric_limits<int>::min();
  int imax = std::numeric_limits<int>::max();
  std::cout << "int_min: " << imin << std::endl;
  std::cout << "int_max; " << imax << std::endl;
  return 0;
}
