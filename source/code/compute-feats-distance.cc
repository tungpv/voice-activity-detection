#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "cudamatrix/cu-vector.h"
#include "cudamatrix/cu-matrix.h"
#include "cudamatrix/cu-device.h"
#include "mgb-lib.h"
#include <string>

namespace kaldi {
void MakeSubMatrixVector(const Matrix<BaseFloat> &mat_utt,
                         const std::string &utt_name,
                         const float frame_length,
                         const std::string &boundary_name, 
                         RandomAccessBaseFloatMatrixReader &file_reader, 
                         std::vector<Matrix<BaseFloat>* > *vec_mat,
                         std::vector<std::pair<BaseFloat, BaseFloat> > *vec_time_bound) {
  if(!boundary_name.empty() && !file_reader.HasKey(utt_name))
    KALDI_ERR << "ERROR, MakeSubMatrixVector: no boundary info for utterance: " 
                    << "'" << utt_name << "'";
  if(boundary_name.empty()) {
    Matrix<BaseFloat> *mat = new Matrix<BaseFloat>;
    *mat = mat_utt;
    vec_mat->push_back (mat);
    if(vec_time_bound != NULL) {
      BaseFloat sec_start = 0;
      BaseFloat sec_end = static_cast<BaseFloat> (mat_utt.NumRows() * frame_length);
      vec_time_bound->push_back(std::pair<BaseFloat, BaseFloat>(sec_start, sec_end));
    }
    return;
  }
  int32 frame_num = mat_utt.NumRows();
  Matrix<BaseFloat> mat_boundary = file_reader.Value(utt_name);
  KALDI_LOG << utt_name << ": " << mat_boundary.NumRows() << " subsequences";
  for(int32 i = 0; i < mat_boundary.NumRows(); i++) {
    BaseFloat sec_start = mat_boundary(i, 0);
    int32 frame_start = static_cast<int32> (sec_start/frame_length);
    KALDI_ASSERT(0 <= frame_start && frame_start < frame_num);
    BaseFloat sec_end = mat_boundary(i, 1);
    int32 frame_end = static_cast<int32> (sec_end/frame_length);
    if (vec_time_bound != NULL) {
      vec_time_bound->push_back(std::pair<BaseFloat, BaseFloat>(sec_start, sec_end));
    }
    if (!(0 <= frame_end && frame_end < frame_num && frame_start < frame_end)) {
      KALDI_WARN << "frame_start=" << frame_start << ' '
                << "frame_end=" << frame_end << ' ' 
                << "frame_num=" << frame_num;
      frame_end = frame_num;
    }
    SubMatrix<BaseFloat> max_sub(mat_utt, frame_start, frame_end - frame_start, 0, mat_utt.NumCols());
    Matrix<BaseFloat> *mat = new Matrix<BaseFloat>;
    *mat = max_sub;
    vec_mat->push_back(mat);
  }
} 
//   
void FreeSubMatrixVector(std::vector<Matrix<BaseFloat>* > *vec_mat) {
  for(size_t i = 0; i < vec_mat->size(); i++) {
    Matrix<BaseFloat> *mat = (*vec_mat)[i];
    delete mat;
  }
  vec_mat->resize(0);
}
//
void ComputeDist(const CuMatrix<BaseFloat> &mat1, const CuMatrix<BaseFloat> &mat2, 
                 CuMatrix<BaseFloat> *mat_dist) {
  KALDI_ASSERT(mat1.NumCols() == mat2.NumCols());
  mat_dist->Resize(mat1.NumRows(), mat2.NumRows());
  CuMatrix<BaseFloat> mat1_t = mat1;
  mat1_t.Transpose();
  CuMatrix<BaseFloat> mat2_t = mat2;
  mat2_t.Transpose();
  
  mat_dist->AddMatMat(1.0, mat1, kNoTrans, mat2_t, kNoTrans, 0);
  CuMatrix<BaseFloat> mat_scale(mat1.NumRows(), mat2.NumRows());
  for(int32 i = 0; i < mat1.NumRows(); i ++) {
    CuVector<BaseFloat> v1(mat1.NumCols());
    v1.CopyColFromMat(mat1_t, i);
    BaseFloat x = v1.Norm(2);
    for(int32 j = 0; j < mat2.NumRows(); j ++) {
      CuVector<BaseFloat> v2(mat2.NumCols());
      v2.CopyColFromMat(mat2_t, j);
      BaseFloat y = v2.Norm(2);
      mat_scale(i, j) = 1.0/(x * y); 
    }
  }
  mat_dist->MulElements(mat_scale);  // get the cosine distance score
  CuMatrix<BaseFloat> mat_unit(mat1.NumRows(), mat2.NumRows());
  mat_unit.Set(1.0);
  mat_dist->Scale(-1.0);
  mat_dist->AddMat(1.0, mat_unit);  // get the final score 1 - distance_score
}
//
void GetMinCost(const BaseFloat diag_length_penalty, 
                const Matrix<BaseFloat> &mat_dist, BaseFloat *minCost) {
  int32 num_rows = mat_dist.NumRows();
  int32 num_cols = mat_dist.NumCols();
  Matrix<BaseFloat> match_curve_len(num_rows, num_cols);
  Matrix<BaseFloat> total_cost(num_rows, num_cols);
  Matrix<BaseFloat> aver_cost (num_rows, num_cols);
  
  for(int32 i = 0; i < num_cols; i++) {  // first column initialization, horizontal
    total_cost(0, i) = mat_dist(0, i);
    match_curve_len(0, i) = 1.0;
    aver_cost(0, i) = total_cost(0, i);
  }
  for(int32 j = 1; j < num_rows; j ++) { // row initialization  vertical
    total_cost(j, 0) = total_cost(j-1, 0) + mat_dist(j, 0);
    match_curve_len(j, 0) = match_curve_len(j-1, 0) + 1;
    aver_cost(j, 0) = total_cost(j, 0)/match_curve_len(j, 0);  
  }

  for(int32 i = 1; i < num_rows; i ++) {
    for(int32 j = 1; j < num_cols; j ++) {
      BaseFloat cur_dist = mat_dist(i, j);
      BaseFloat cost_h = total_cost(i, j-1) + cur_dist;
      BaseFloat cost_v = total_cost(i-1, j) + cur_dist;
      BaseFloat cost_d = total_cost(i-1, j-1) + cur_dist;
      
      BaseFloat curve_len_h = match_curve_len(i, j-1);
      BaseFloat curve_len_v = match_curve_len(i-1, j);
      BaseFloat curve_len_d = match_curve_len(i-1, j-1);

      BaseFloat aver_cost_h = cost_h / (1 + curve_len_h);
      BaseFloat aver_cost_v = cost_v / (1 + curve_len_v);
      BaseFloat aver_cost_d = cost_d / (diag_length_penalty + curve_len_d);
      if (aver_cost_h < aver_cost_v) {
        if(aver_cost_h < aver_cost_d) {
          total_cost(i, j) = cost_h;
          aver_cost(i, j) = aver_cost_h;
          match_curve_len(i, j) = 1 + curve_len_h;
        } else {
          total_cost(i, j) = cost_d;
          aver_cost(i, j) = aver_cost_d;
          match_curve_len(i, j) = diag_length_penalty + curve_len_d;
        }
      } else if (aver_cost_v < aver_cost_d) {
        total_cost(i, j) = cost_v;
        aver_cost(i, j) = aver_cost_v;
        match_curve_len(i, j) = 1 + curve_len_v;
      } else {
        total_cost(i, j) = cost_d;
        aver_cost(i, j) = aver_cost_d;
        match_curve_len(i, j) = diag_length_penalty + curve_len_d;
      }
    }
  }
  *minCost = aver_cost(num_rows-1, 0);
  for(int32 i = 1; i < num_cols; i ++)  {
    if(aver_cost(num_rows - 1, i) < *minCost)
      *minCost = aver_cost(num_rows - 1, i);
  }
}
//
void ComputeFeatDistances(const BaseFloat diag_length_penalty, 
                          std::vector<Matrix<BaseFloat>* > &vec_mat1, 
                          std::vector<Matrix<BaseFloat>* > &vec_mat2,
                          BaseFloat *minCost, int32 *minIndex) {
  *minCost = 1.0;
  *minIndex = 0;
  for(int32 i = 0; i < vec_mat1.size(); i++) {
    for(int32 j = 0; j < vec_mat2.size(); j++) {
      CuMatrix<BaseFloat> cu_mat_dist;
      ComputeDist(CuMatrix<BaseFloat>(*vec_mat1[i]), CuMatrix<BaseFloat>(*vec_mat2[j]), &cu_mat_dist);
      Matrix<BaseFloat> mat_dist(cu_mat_dist.NumRows(), cu_mat_dist.NumCols());
      cu_mat_dist.CopyToMat(&mat_dist);
      BaseFloat curMinCost;
      GetMinCost(diag_length_penalty, mat_dist, &curMinCost);
      KALDI_ASSERT(curMinCost <= 1.0);
      if(curMinCost < *minCost) {
        *minCost = curMinCost;
        *minIndex = j;   
      }     
    }
  }
  *minCost = 1.0 - *minCost;  // convert cost into score
}

}  // namespace kaldi

int main(int argc, char *argv[]) {
  using namespace kaldi;
  try {
    const char *usage =
    "Compute the minimum distance between two feature streams(matrices) using DTW method.\n"
    "compute-feats-distance [options] scp:feats1.scp scp:feats2.scp dist.txt\n";
    ParseOptions po(usage);
    float frame_length=0.01;
    std::string src2tgt = "";
    std::string src_time_boundary_file = "", tgt_time_boundary_file = "";
    po.Register("frame-length", &frame_length, "frame duration");
    float diag_length_penalty = 2.0;
    po.Register("diag-length-penalty", &diag_length_penalty, "diag length penalty");
    po.Register("src2tgt", &src2tgt, "source to target name mapping file");
    po.Register("src-time-boundary-file", &src_time_boundary_file, "time boundary info for the source files");
    po.Register("tgt-time-boundary-file", &tgt_time_boundary_file, "time boundary info for the target files");
    std::string use_gpu = "no";
    po.Register("use-gpu", &use_gpu, "yes|no|optional, only has effect if compiled with CUDA");

    po.Read(argc, argv);
    if(po.NumArgs() != 3) {
      po.PrintUsage();
      exit(1);
    }
    std::string feature1_rspecifier = po.GetArg(1);
    std::string feature2_rspecifier = po.GetArg(2);
    std::string dist_wfilename = po.GetArg(3);

#if HAVE_CUDA==1
  CuDevice::Instantiate().SelectGpuId(use_gpu);
  CuDevice::Instantiate().DisableCaching();
#endif    
    SequentialBaseFloatMatrixReader feature1_reader(feature1_rspecifier);
    SequentialBaseFloatMatrixReader feature2_reader(feature2_rspecifier);
    std::map<std::string, std::map<std::string, unsigned short> > map_src2tgt;
    StrMapInMap s2t_map;
    if (!src2tgt.empty()) {
      s2t_map.Open(src2tgt);
    }
    RandomAccessBaseFloatMatrixReader boundary1_reader, boundary2_reader;
    if(!src_time_boundary_file.empty()) {
      boundary1_reader.Open(src_time_boundary_file);
    }
    if(!tgt_time_boundary_file.empty()) {
      boundary2_reader.Open(tgt_time_boundary_file);
    }
    bool output_binary =false;
    Output output_kaldi(dist_wfilename, output_binary);
    for(; !feature1_reader.Done(); feature1_reader.Next()) {
      std::string utt1 = feature1_reader.Key();
      Matrix<BaseFloat> mat_utt1 = feature1_reader.Value();
      std::vector<Matrix<BaseFloat>*> vec_mat_utt1;
      MakeSubMatrixVector(mat_utt1, utt1, frame_length, src_time_boundary_file, boundary1_reader, &vec_mat_utt1, NULL); 
      for(; !feature2_reader.Done(); feature2_reader.Next()) {
        std::string utt2 = feature2_reader.Key();
        if(!src2tgt.empty() && !s2t_map.HasValue(utt1, utt2))
          continue;
        Matrix<BaseFloat> mat_utt2 = feature2_reader.Value();
        std::vector<Matrix<BaseFloat>*> vec_mat_utt2;
        std::vector<std::pair<BaseFloat, BaseFloat> > vec_time_bound;
        MakeSubMatrixVector(mat_utt2, utt2, frame_length, tgt_time_boundary_file, boundary2_reader, &vec_mat_utt2, &vec_time_bound);
        BaseFloat minCost;
        int32 minIndex; 
        ComputeFeatDistances(diag_length_penalty, vec_mat_utt1, vec_mat_utt2, &minCost, &minIndex);
        KALDI_ASSERT(minIndex < vec_time_bound.size());
        output_kaldi.Stream() << utt1 << ' ' << utt2 << ' ' << vec_time_bound[minIndex].first 
                              << ' ' << vec_time_bound[minIndex].second << ' '
                              << minCost << "\n";
        FreeSubMatrixVector(&vec_mat_utt2);    
      }
      FreeSubMatrixVector(&vec_mat_utt1);
      feature2_reader.Close();  // is there any rewind function for this ?
      feature2_reader.Open(feature2_rspecifier);
    }
    output_kaldi.Close(); 
  } catch (std::exception &e) {
    std::cerr << e.what();
    return 1;
  }
  return 0;
}


