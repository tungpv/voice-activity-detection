#include "base/kaldi-common.h"
#include "util/common-utils.h"

int main(int argc, char *argv[]) {
  using namespace kaldi;
  try {
    const char *usage = 
      "Calculate frame dropping rate given a frame-level confidence vector for each file.\n"
      "Usage: drop-frame-rate [options] <frame-level-weight-rspecifier> \n"
      "e.g.: \n"
      " drop-frame-rate --conf-thresh=0.5 \"ark:gzip -cd frame-weight.gz|\" \n";
    ParseOptions po(usage);
    double conf_thresh = 0;
    po.Register("conf-thresh", &conf_thresh, "frame-level confidence score threshold");
    float frame_length=0.01;
    po.Register("frame-length", &frame_length, "frame length in terms of second");
    po.Read(argc, argv);
    if(po.NumArgs() != 1) {
      po.PrintUsage();
      exit(1);
    }
    std::string frame_weight_rspecifier = po.GetArg(1);
    SequentialBaseFloatVectorReader weight_reader;
    weight_reader.Open(frame_weight_rspecifier);
    Vector<BaseFloat> weights;
    int32 tot_weights = 0, drop_weights = 0;
    for(; ! weight_reader.Done(); weight_reader.Next()) {
      std::string utt = weight_reader.Key();
      weights = weight_reader.Value();
      int32 feat_num = weights.Dim();
      tot_weights += feat_num;
      for(int32 i = 0; i < feat_num; i++) {
        if(weights(i) < conf_thresh)
          drop_weights ++;
      }
    }
    char cbuffer[100];
    std::string rate_str, tot_str, drop_str;
    if (tot_weights == 0) {
      sprintf(cbuffer, "0");
    } else {
      sprintf(cbuffer,"%.2f%c",static_cast<float>(100*drop_weights)/tot_weights,'%');
    }
    rate_str = cbuffer;
    sprintf(cbuffer, "%.2f", tot_weights*frame_length/3600);
    tot_str = cbuffer;
    sprintf(cbuffer, "%.2f", drop_weights*frame_length/3600);
    drop_str = cbuffer;
    std::cout << "total_hours=" << tot_str << "h, drop_frames=" <<  drop_str
              << "h, drop_rate=" << rate_str << "\n";
    return 0;
  } catch (const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
