#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "hmm/posterior.h"
#include "mgb-lib.h"
#include <string>

namespace kaldi {
void MakeSample(const Vector<BaseFloat> &vec_word_cv, 
                const std::vector<CTMItem> &vec_ctm_item, 
                const std::vector<int32> &vec_label, 
                Matrix<BaseFloat> *features, 
                Posterior *posterior) {
  KALDI_ASSERT(vec_ctm_item.size() == vec_label.size());
  int32 ncol = 6, nrow = vec_label.size();
  features->Resize(nrow, ncol);
  if ( posterior != NULL)
    posterior->resize(nrow);
  for(int32 i = 0; i < vec_ctm_item.size(); i ++) {
    SubVector<BaseFloat> vec_sub(*features, i);
    const CTMItem *ptr_ctm = &vec_ctm_item[i];
    vec_sub(0) = ptr_ctm->conf;                                // word confidence
    vec_sub(1) = vec_word_cv(0) + vec_word_cv(1);              // word length in terms of phoneme
    vec_sub(2) = vec_word_cv(0); vec_sub(3) = vec_word_cv(1);  // consonant and vowel phoneme numbers respectively
    vec_sub(4) = vec_ctm_item[i].dur;                          // word duration
    vec_sub(5) = vec_sub(4) / vec_sub(1);                      // duration per phomeme
    if (posterior != NULL) {
      std::vector<std::pair<int32, BaseFloat> > state_labels;
      state_labels.resize(2);
      state_labels[0] = std::make_pair(0, vec_label[i]);
      state_labels[1] = std::make_pair(1, 1 - vec_label[i]);
      (*posterior)[i] = state_labels;
    }
  }
}

} // namespace kaldi

int main(int argc, char **argv) {
  using namespace kaldi;
  try {
    const char *usage =
    "Extract classifier training sample by aligning ctm file\n"
    "e.g.:\n"
    "make-feat-from-ctm word-to-cv-count utt-ctm scp,ark:feats.scp,feats.ark\n";

    ParseOptions po(usage);
    bool use_conf = false;
    po.Register("use-conf", &use_conf, "use word confidence threshold");
    float conf_thresh = 0;
    po.Register("conf-thresh", &conf_thresh, "word confidence threshold");
    po.Read(argc, argv);
    if(po.NumArgs() != 3) {
      po.PrintUsage();
      exit (-1);
    }
    std::string word_to_cv_count_rfilename = po.GetArg(1);
    std::string utt_ctm_rfilename = po.GetArg(2);
    std::string feat_wspecifier = po.GetArg(3);
    
    bool binary_in;
    RandomAccessBaseFloatVectorReader word_to_cv_count_reader(word_to_cv_count_rfilename);
    
    BaseFloatMatrixWriter kaldi_mat_writer;
    if(! kaldi_mat_writer.Open(feat_wspecifier))
      KALDI_ERR << "Could not initialize ouput with wspecifier "
                << feat_wspecifier;
    Input ki_utt_ctm(utt_ctm_rfilename, &binary_in);
    std::string line, key = "", key_prev = "";
    Matrix<BaseFloat> features;
    Vector<BaseFloat> vec_word_cv;
    std::vector<CTMItem> vec_ctm_item;
    std::vector<int32> vec_label;
    while(std::getline(ki_utt_ctm.Stream(), line)) {
      CTMItem value, overlap_ctm;
      SplitCTMLine(line, &key, &value);
      if (! word_to_cv_count_reader.HasKey(value.word)) {
        KALDI_ERR << "oov word " << '"' << value.word << '"' << " observed"; 
      }
      vec_word_cv = word_to_cv_count_reader.Value(value.word);
      if (key_prev != "" && key_prev != key) {
        vec_label.resize(vec_ctm_item.size()); // dummy
        MakeSample(vec_word_cv, vec_ctm_item, vec_label, &features, NULL);
        kaldi_mat_writer.Write(key_prev, features);
        vec_ctm_item.clear();
      }
      vec_ctm_item.push_back(value);
      key_prev = key;
    }
    if ( key != "") {
      vec_label.resize(vec_ctm_item.size()); // dummy
      MakeSample(vec_word_cv, vec_ctm_item, vec_label, &features, NULL);
      kaldi_mat_writer.Write(key, features);
    }
    ki_utt_ctm.Close();
    return 0;
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  } 
}
