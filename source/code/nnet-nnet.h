// nnet/nnet-nnet.h

// Copyright 2011-2013  Brno University of Technology (Author: Karel Vesely)

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#ifndef KALDI_NNET_NNET_NNET_H_
#define KALDI_NNET_NNET_NNET_H_

#include <iostream>
#include <sstream>
#include <vector>

#include "base/kaldi-common.h"
#include "util/kaldi-io.h"
#include "matrix/matrix-lib.h"
#include "nnet/nnet-trnopts.h"
#include "nnet/nnet-component.h"

namespace kaldi {
namespace nnet1 {

class Nnet {
 public:
  Nnet() : freeze_update_(false), 
           parallel_level_(-1)
           { }
  Nnet(const Nnet& other);  // Copy constructor.
  Nnet &operator = (const Nnet& other); // Assignment operator.

  ~Nnet();

 public:
  /// Perform forward pass through the network
  void Propagate(const CuMatrixBase<BaseFloat> &in, CuMatrix<BaseFloat> *out);
  /// Perform backward pass through the network
  void Backpropagate(const CuMatrixBase<BaseFloat> &out_diff, CuMatrix<BaseFloat> *in_diff);
  /// Perform forward pass through the network, don't keep buffers (use it when not training)
  void Feedforward(const CuMatrixBase<BaseFloat> &in, CuMatrix<BaseFloat> *out);

  /// Dimensionality on network input (input feature dim.)
  int32 InputDim() const;
  /// Dimensionality of network outputs (posteriors | bn-features | etc.)
  int32 OutputDim() const;

  /// Returns number of components-- think of this as similar to # of layers, but
  /// e.g. the nonlinearity and the linear part count as separate components,
  /// so the number of components will be more than the number of layers.
  int32 NumComponents() const { return components_.size(); }

  const Component& GetComponent(int32 c) const;
  Component& GetComponent(int32 c);

  /// Sets the c'th component to "component", taking ownership of the pointer
  /// and deleting the corresponding one that we own.
  void SetComponent(int32 c, Component *component);

  /// Appends this component to the components already in the neural net.
  /// Takes ownership of the pointer
  void AppendComponent(Component *dynamically_allocated_comp);
  /// Append another network to the current one (copy components).
  void AppendNnet(const Nnet& nnet_to_append);

  /// Remove component
  void RemoveComponent(int32 c);
  void RemoveLastComponent() { RemoveComponent(NumComponents()-1); }

  /// Access to forward pass buffers
  const std::vector<CuMatrix<BaseFloat> >& PropagateBuffer() const {
    return propagate_buf_;
  }
  /// Access to backward pass buffers
  const std::vector<CuMatrix<BaseFloat> >& BackpropagateBuffer() const {
    return backpropagate_buf_;
  }

  /// Get the number of parameters in the network
  int32 NumParams() const;
  /// Get the network weights in a supervector
  void GetParams(Vector<BaseFloat>* wei_copy) const;
  /// Get the network weights in a supervector
  void GetWeights(Vector<BaseFloat>* wei_copy) const;
  /// Set the network weights from a supervector
  void SetWeights(const Vector<BaseFloat>& wei_src);
  /// Get the gradient stored in the network
  void GetGradient(Vector<BaseFloat>* grad_copy) const;

  /// Set the dropout rate
  void SetDropoutRetention(BaseFloat r);
  /// Reset streams in LSTM multi-stream training,
  void ResetLstmStreams(const std::vector<int32> &stream_reset_flag);

  /// set sequence length in LSTM multi-stream training
  void SetSeqLengths(const std::vector<int32> &sequence_lengths);

  /// Initialize MLP from config
  void Init(const std::string &config_file);
  /// Read the MLP from file (can add layers to exisiting instance of Nnet)
  void Read(const std::string &file);
  /// Read the MLP from stream (can add layers to exisiting instance of Nnet)
  void Read(std::istream &in, bool binary);
  /// Write MLP to file
  void Write(const std::string &file, bool binary) const;
  /// Write MLP to stream
  void Write(std::ostream &out, bool binary) const;

  /// Create string with human readable description of the nnet
  std::string Info() const;
  /// Create string with per-component gradient statistics
  std::string InfoGradient() const;
  /// Create string with propagation-buffer statistics
  std::string InfoPropagate() const;
  /// Create string with back-propagation-buffer statistics
  std::string InfoBackPropagate() const;
  /// Consistency check.
  void Check() const;
  /// Relese the memory
  void Destroy();

  /// Set training hyper-parameters to the network and its UpdatableComponent(s)
  void SetTrainOptions(const NnetTrainOptions& opts);
  /// Get training hyper-parameters from the network
  const NnetTrainOptions& GetTrainOptions() const {
    return opts_;
  }
  void FreezeUpdate() {
    freeze_update_ = true;
  }
  void SetParallelLevel(const int32 parallel_level) {
    parallel_level_ = parallel_level;
  }
  void ClearParallelOutput() {
    parallel_nnet_output_.clear();
  }
  void CollectParallelOutput(const CuMatrix<BaseFloat> &nnet_output) {
    parallel_nnet_output_.push_back(nnet_output);
  }
 private:
  /// Vector which contains all the components composing the neural network,
  /// the components are for example: AffineTransform, Sigmoid, Softmax
  std::vector<Component*> components_;

  std::vector<CuMatrix<BaseFloat> > propagate_buf_;  ///< buffers for forward pass
  std::vector<CuMatrix<BaseFloat> > backpropagate_buf_;  ///< buffers for backward pass

  /// Option class with hyper-parameters passed to UpdatableComponent(s)
  NnetTrainOptions opts_;
  /// for parallel nnet work
  bool freeze_update_;
  int32 parallel_level_;
  std::vector<*ParallelOneNnet> *vec_nnet_;
};
// parallel nnet
bool valid_string(const std::string &str) {
  if(str.empty())
    return false;
  size_t startpos = str.find_last_not_of(" \t");
  if(startpos == std::string::npos)
    return false;
  return true;
}
class ParallelOneNnet {  // single nnet
 friend class Nnet;
 public:
  ParallelOneNnet(const bool has_utt2spk, const Nnet *nnet,
                  NnetDataRandomizerOptions *rnd_opts ) : has_utt2spk_(has_utt2spk),
                                                              mNnet_(nnet)
                                                              { feature_randomizer_.Init(*rnd_opts);  }
  void InitFeatReader(const std::string &feature_rspecifier) {
    if(!valid_string(feature_rspecifier)) {
      if(has_utt2spk) 
        vec_reader_.Open(feature_rspecifier);
      else
        feature_reader_.Open(feature_rspecifier);
    }
  }
  void InitUtt2SpkReader(const std::string &utt2spk_filename) {
    if(valid_string(utt2spk_filename)) {
      utt2spk_reader_.Open(utt2spk_filename); 
    }
  }
  void InitFeatTransform(const std::string &feature_transform) {
    if(valid_string(feature_transform)) {
      nnet_transf_.Read(feature_transform);
    }
  }
  void InitNnet(const std::string &model_filename) {
    nnet_.Read(model_filename);
    nnet_SetTrainOptions(mNnet_->GetTrainOptions());
  }
  bool HasUtt(const std::string &utt) {
    if(has_utt2spk_) {
      return utt2spk_reader_.HasKey(utt);
    }
    return feature_reader_.HasKey(utt);
  }
  bool AddData(const std::string &utt, const int32 &nRows) {
    Matrix<BaseFloat> mat;
    if(has_utt2spk_) {
      std::string spk = utt2spk_reader_.Value(utt);
      Vector<BaseFloat> vec;
      vec = vec_reader_.Value(spk);
      mat.Resize(nRows, vec.Dim());
      mat.CopyRowsFromVec(vec);
    } else {
      mat = feature_reader_.Value(utt);
      if (mat.NumRows() < nRows)
        return false;
      else if(mat.NumRows > nRows) {
        mat.Resize(nRows, mat.NumCols(), kCoptyData);
      }
    }
    nnet_transf_.Feedforward(CuMatrix<BaseFloat>(mat), &feats_transf_);
    feature_randomizer_.AddData(feats_transf_);
    return true;
  }
  void FeatRandomize(const std::vector<int32> &mask) {
    feature_randomizer_.Randomize(mask);
  }
  void Propagate() {
    KALDI_ASSERT(! feature_randomizer_.Done); // this is monitored by the caller
    const CuMatrixBase<BaseFloat>& nnet_in = feature_randomizer_.Value();
    nnet_.Propagate(nnet_in, &nnet_out_);
    mNnet_->CollectParallelOutput(nnet_out_);
    feature_randomizer_.Next();
  }
 private:
  RandomAccessBaseFloatMatrixReader  feature_reader_;
  RandomAccessBaseFloatVectorReader  vec_reader_;
  bool has_utt2spk_;
  RandomAccessTokenReader utt2spk_reader_;
  MatrixRandomizer feature_randomizer_;
  CuMatrix<BaseFloat> feats_transf_, nnet_out_;
  Nnet nnet_transf_;
  Nnet nnet_;
  Nnet *mNnet_;      // main nnet to which the current subnet attaches
  };

class ParallelNnet {
  friend class Nnet;
 public:
  ParallelNnet(const Nnet *nnet, NnetDataRandomizerOptions *rnd_opts): mNnet_(nnet),
                                                                       rnd_opts_(rnd_opts)                                
                                                                       { }
  ~ParallelNnet() {
    Destroy();
  }
  void SetParallelOpts(ParallelNnetOptions &opts) {
    opts_ = opts;
  }
  void Init() {
    mNnet_->FreezeUpdate();
    KALDI_ASSERT(opts_.parallel_level >= 0);
    mNnet_->SetParallelLevel(opts_.parallel_level);
    for(int32 i = 0; i < opts_.vec_feature.size(); ++i) {
      bool has_utt2spk = false, has_nnet_transf = false;
      if(valid_string(opts_.vec_utt2spk[i])) has_utt2spk =true;
      if(valid_string(opts_.vec_feature_transform[i])) has_nnet_transf = true;
      ParallelOneNnet *pOneNet = new ParallelOneNnet(has_utt2spk, has_nnet_transf, mNnet_, rnd_opts_);
      pOneNnet->InitNnet(opts_.vec_nnet[i]);
      pOneNnet->InitFeatReader(opts_vec_feature[i]);
      pOneNnet->InitFeatTransform(opts_.vec_feature_transform[i]);
      pOneNnet->InitUtt2SpkReader(opts_.vec_utt2spk[i]);
      nnet_vec_.push_back(pOneNnet);
    }
  }
  void HasUtt(const std::string &utt) {
    for(int32 i = 0; i < nnet_vec_.size(); ++ i) {
      if(! nnet_vec_[i].HasUtt(utt))
        return false;
    }
    return true;
  }
  bool AddData(const std::string &utt, const int32 &nRows) {
    for(int32 i = 0; i < nnet_vec_.size(); ++i) {
      if(! nnet_vec_[i].AddData(utt, nRows))
        return false;
    }
    return true;
  }
  void FeatRandomize(const std::vector<int32> &mask) {
    for(int32 i = 0; i < nnet_vec_.size(); ++i) {
      nnet_vec_[i].FeatRandomize(mask);
    }
  }
  void Propagate() {
    mNnet_->ClearParallelOutput();
    for(int32 i = 0; i < nnet_vec_.size(); ++i) {
      nnet_vec_[i].Propagate();
    }
  }
 private:
  ParallelNnetOptions opts_;
  NnetDataRandomizerOptions *rnd_opts_;
  Nnet *mNnet_;
  std::vector<*ParallelOneNnet> nnet_vec_; 

  void Destroy() {
    for(int32 i = 0; i < nnet_vec_.size(); ++i)
      delete nnet_vec_[i];
    nnet_vec_.clear();
  }
};

}  // namespace nnet1
}  // namespace kaldi

#endif  // KALDI_NNET_NNET_NNET_H_

