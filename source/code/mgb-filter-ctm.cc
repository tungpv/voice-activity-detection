#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "mgb-lib.h"
#include <string>
#ifndef _MSC_VER
#include <unistd.h>
#endif

namespace kaldi {
void SplitExtCTMLine(const std::string &line, std::string *key, CTMItem *value) {
  std::vector<std::string> vec_str;
  SimpleSplitString(line, ' ', &vec_str);
  if (vec_str.size() != 6)
    KALDI_ERR << "bad line: " << line;
  *key = vec_str[0];
  value->spk = vec_str[1];
  ConvertStringToInteger(vec_str[2], &value->chanId);
  ConvertStringToReal(vec_str[3], &value->start);
  ConvertStringToReal(vec_str[4], &value->dur);
  value->word = vec_str[5]; 
}
void ReadExtCTMFile(std::istream &infile, std::map<std::string, std::vector<CTMItem> > *map_ctm) {
  std::string line;
  while(std::getline(infile, line)) {
    std::string key;
    CTMItem value;
    SplitExtCTMLine(line, &key, &value);
    std::cout << key << " " << value.spk << " " << value.chanId << " "
              << value.start << " " << value.dur << " " << value.word << "\n";
    std::map<std::string, vecCTM>::iterator it_map = map_ctm->find(key),
                                                  it_map_end = map_ctm->end();
    if(it_map != it_map_end) {
      vecCTM &vec_ctm = it_map->second;
      vec_ctm.push_back(value);
    } else {
      vecCTM vec_ctm;
      vec_ctm.push_back(value);
      map_ctm->insert(std::pair<std::string, vecCTM>(key, vec_ctm));
    }
  }
  SortCTM(map_ctm);
  CheckOrder(*map_ctm);
}

} //end namespace kaldi
int main(int argc, char *argv[]) {
  using namespace kaldi;
  try {
    const char *usage =
    "Filter target ctm file using source ctm file.\n"
    "e.g.:\n"
    "mgb-filter-ctm ctm target_ctm new_ctm";
    ParseOptions po(usage);
    bool use_conf = false;
    po.Register("use-conf", &use_conf, "use word confidence threshold");
    float conf_thresh = 0;
    po.Register("conf-thresh", &conf_thresh, "word confidence threshold");
    bool use_voting = false;
    po.Register("use-voting", &use_voting, "use voting to decide which word will be extracted");
    po.Read(argc, argv);
    if(po.NumArgs() != 3) {
      po.PrintUsage();
      exit(-1);
    } 
    std::string src_ctm_filename = po.GetArg(1);
    std::string target_ctm_filename = po.GetArg(2);
    std::string new_ctm_filename = po.GetArg(3);

    bool binary_in;
    Input ki(src_ctm_filename, &binary_in);
    std::istream &infile = ki.Stream();
    std::map<std::string, std::vector<CTMItem> > map_ctm;
    ReadSpkCTMFile(infile, &map_ctm);
    Input ctm_ki(target_ctm_filename, &binary_in);
    Output ctm_ko(new_ctm_filename, false);
    std::ostream &ofile = ctm_ko.Stream();
    std::string line;
    float tot_len = 0;
    while(std::getline(ctm_ki.Stream(), line)) {
      std::string key; 
      CTMItem value, overlap_ctm;
      SplitCTMLine(line, &key, &value);
      if(FindTimeOverlap(map_ctm, key, value, &overlap_ctm) == true) {
        value.spk = overlap_ctm.spk;
        if(use_voting == true && value.word == overlap_ctm.word) {
          DumpCTMLine(ofile, key, value, &tot_len);  
        } else if(use_voting == false){
          DumpCTMLine(ofile, key, value, &tot_len);
        }
      } else { 
        if(use_conf == true && value.conf >= conf_thresh) {
          value.spk = overlap_ctm.spk;
          DumpCTMLine(ofile, key, value, &tot_len);
        }    
      }
    }
    std::string len_str;
    char buf[1024];
    sprintf(buf, "%.4f", tot_len/3600);
    len_str = buf;
    KALDI_LOG << "Total data selected: " << len_str << " hrs";
    ctm_ko.Close();
    return 0;
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}

