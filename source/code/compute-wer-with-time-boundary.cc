#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "mgb-lib.h"
#include <string>

namespace kaldi {

} // namespace kaldi

int main(int argc, char **argv) {
  using namespace kaldi;
  try {
    const char *usage =
    "Compute WER using time boundary info.\n"
    "e.g.:\n"
    "compute-wer-with-time-boundary ref-ctm hyp_ctm\n";
    ParseOptions po(usage);
    float overlap_thresh = 0.6;
    po.Register("overlap-thresh", &overlap_thresh, "time-overlap lower boundary for two words to be compared");
    po.Read(argc, argv);
    if(po.NumArgs() != 2) {
      po.PrintUsage();
      exit (-1);
    }
    std::string ref_ctm_filename = po.GetArg(1);
    std::string hyp_ctm_filename = po.GetArg(2);
    bool binary_in;
    Input ki_ref(ref_ctm_filename, &binary_in);
    std::map<std::string, std::vector<CTMItem> > map_ctm;
    ReadCTMFile(ki_ref.Stream(), &map_ctm);
    ki_ref.Close();
    Input ki_hyp(hyp_ctm_filename, &binary_in);
    std::string line;
    float overlapped_corr = 0, overlapped_error = 0, unoverlapped_error = 0;
    while(std::getline(ki_hyp.Stream(), line)) {
      std::string key;
      CTMItem value, overlap_ctm;
      SplitCTMLine(line, &key, &value);
      if(FindTimeOverlap(map_ctm, key, value, &overlap_ctm, overlap_thresh) == true) {
        if(overlap_ctm.word == value.word)
          overlapped_corr ++;
        else
          overlapped_error ++;
      } else {
        unoverlapped_error ++; 
      }
    }
    ki_hyp.Close();
    float total_word = overlapped_corr + overlapped_error + unoverlapped_error;
    char error_rate_formatted[1024];
    float error_rate = (overlapped_error + unoverlapped_error) / total_word;
    sprintf(error_rate_formatted, "%.2f%c", error_rate*100, '%');
    char acc_formatted[1024];
    sprintf(acc_formatted, "%.4f", 1 - error_rate);
    std::cout << total_word << " " << overlapped_corr << " "
	      << overlapped_error << " " << unoverlapped_error << " "
              <<  error_rate_formatted << " " << acc_formatted << "\n";
    return 0;
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  } 
}
