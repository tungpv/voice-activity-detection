// gmmbin/gmm-compute-likes.cc

// Copyright 2009-2011  Microsoft Corporation

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.


#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "gmm/am-diag-gmm.h"
#include "hmm/transition-model.h"
#include "fstext/fstext-lib.h"
#include "thread/kaldi-thread.h"
#include "thread/kaldi-mutex.h"
#include "base/timer.h"

namespace kaldi {

template<typename Real> static void AssertEqual(const Matrix<Real> &A,
                                             const Matrix<Real> &B,
                                             float tol = 0.001) {
  KALDI_ASSERT(A.NumRows() == B.NumRows()&&A.NumCols() == B.NumCols());
  for (MatrixIndexT i = 0;i < A.NumRows();i++)
    for (MatrixIndexT j = 0;j < A.NumCols();j++) {
      KALDI_ASSERT(std::abs(A(i, j)-B(i, j)) < tol*std::max(1.0, (double) (std::abs(A(i, j))+std::abs(B(i, j)))));
    }
}

class GmmLikesThread {
 public:
  GmmLikesThread(const Matrix<BaseFloat> &features, AmDiagGmm &am_gmm, Matrix<BaseFloat> *loglikes):
                 features_(&features), am_gmm_(&am_gmm), loglikes_(loglikes) {}
  void operator() () {
    int32 num_features = features_->NumRows();
    int32 block_size = (num_features + (num_threads_-1)) / num_threads_;
    int32 start = block_size * thread_id_, end = std::min(num_features, start + block_size);
    for(int32 i = start; i < end; i ++) {
      for(int32 j = 0; j < am_gmm_->NumPdfs(); j ++) {
        SubVector<BaseFloat> feat_row(*features_, i);
        (*loglikes_)(i, j) = am_gmm_->LogLikelihood(j, feat_row);
      }
    }
  }
  static void *run(void *c_in) {
    GmmLikesThread *c = static_cast<GmmLikesThread*>(c_in);
    (*c)();
    return NULL;
  }
 public:
  int32 thread_id_;
  int32 num_threads_;
 private:
  GmmLikesThread() {}
  const Matrix<BaseFloat>  *features_;
  AmDiagGmm *am_gmm_;
  Matrix<BaseFloat> *loglikes_;
};  
} // namespace kaldi

int main(int argc, char *argv[]) {
  try {
    using namespace kaldi;
    typedef kaldi::int32 int32;
    using fst::SymbolTable;
    using fst::VectorFst;
    using fst::StdArc;

    const char *usage =
        "Compute log-likelihoods from GMM-based model\n"
        "(outputs matrices of log-likelihoods indexed by (frame, pdf)\n"
        "Usage: gmm-compute-likes [options] model-in features-rspecifier likes-wspecifier\n";
    ParseOptions po(usage);
    int32 num_threads = 1;
    po.Register("num-threads", &num_threads, "number of threads to run in a parallel way");
    po.Read(argc, argv);

    if (po.NumArgs() != 3) {
      po.PrintUsage();
      exit(1);
    }

    std::string model_in_filename = po.GetArg(1),
        feature_rspecifier = po.GetArg(2),
        loglikes_wspecifier = po.GetArg(3);
    g_num_threads = num_threads;
    AmDiagGmm am_gmm;
    {
      bool binary;
      TransitionModel trans_model;  // not needed.
      Input ki(model_in_filename, &binary);
      trans_model.Read(ki.Stream(), binary);
      am_gmm.Read(ki.Stream(), binary);
    }

    BaseFloatMatrixWriter loglikes_writer(loglikes_wspecifier);
    SequentialBaseFloatMatrixReader feature_reader(feature_rspecifier);

    int32 num_done = 0;
    for (; !feature_reader.Done(); feature_reader.Next()) {
      std::string key = feature_reader.Key();
      const Matrix<BaseFloat> &features (feature_reader.Value());
      Matrix<BaseFloat> loglikes(features.NumRows(), am_gmm.NumPdfs());
      Matrix<BaseFloat> loglikes_single(features.NumRows(), am_gmm.NumPdfs());
      GmmLikesThread gc(features, am_gmm, &loglikes);
      RunMultiThreaded(gc);
      loglikes_writer.Write(key, loglikes);
      num_done++;
    }

    KALDI_LOG << "gmm-compute-likes: computed likelihoods for " << num_done
              << " utterances.";
    return 0;
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}


