#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "matrix/kaldi-matrix.h"

namespace kaldi {

bool MergeFeatures(std::vector< Matrix<BaseFloat> > &spk_features, std::vector< Vector<BaseFloat> > &spk_vads,
                   Matrix<BaseFloat> *merged_features, Vector<BaseFloat> *merged_vads) {
  if(spk_features.size() == 0)
    return false;
  int32 total_rows = spk_features[0].NumRows();
  int32 dim = spk_features[0].NumCols();
  for(int32 i = 1; i < spk_features.size(); ++i) {
    KALDI_ASSERT(spk_features[i].NumCols() == dim);
    total_rows += spk_features[i].NumRows();
  }
  merged_features->Resize(total_rows, dim);
  int32 cur_rows = 0;
  for(int32 i = 0; i < spk_features.size(); ++i) {
    SubMatrix<BaseFloat> cur_features(*merged_features, cur_rows, spk_features[i].NumRows(), 0, dim);
    cur_features.CopyFromMat(spk_features[i]);
    cur_rows += spk_features[i].NumRows();
  }
  if(spk_vads.size() == 0)
    return true;
  int32 num_vads = 0;
  for(int32 i = 0; i < spk_vads.size(); ++i)
    num_vads += spk_vads[i].Dim();
  if (total_rows != num_vads) {
    KALDI_WARN << "total features " << total_rows << ", while total vads " << num_vads;
    return false;
  }
  merged_vads->Resize(total_rows);
  int32 cur_dims = 0;
  for(int32 i = 0; i < spk_vads.size(); ++i) {
    SubVector<BaseFloat> cur_vads(*merged_vads, cur_dims, spk_vads[i].Dim());
    cur_vads.CopyFromVec(spk_vads[i]);
    cur_dims += spk_vads[i].Dim();
  }
  return true;
}
///
struct MergeFeatOptions {
  float times;
  int32 min_samples;
  int32 max_samples;
  bool overall_merge;
  bool partial_sequential_merge;
  MergeFeatOptions(): times(1.0),
                      min_samples(2000),
                      max_samples(4000),
                      overall_merge(false),
                      partial_sequential_merge(false)
                      {}
  void Register(OptionsItf *opts) {
    opts->Register("times", &times, "how much times the entire data covered");
    opts->Register("min-samples", &min_samples, "minimum samples of the merged data");
    opts->Register("max-samples", &max_samples, "maximum samples of the merged data");
    opts->Register("overall-merge", &overall_merge, "merge the overall data from the same speaker");
    opts->Register("partial-sequential-merge", &partial_sequential_merge, "merge the data sequentially and partially");
  }
  friend std::ostream & operator<<(std::ostream& os, const MergeFeatOptions &opts) {
    os << "MergeFeatOptions: "
       << "times " << opts.times << ", "
       << "min_samples " << opts.min_samples << ", "
       << "max_samples " << opts.max_samples << ", "
       << "overall_merge " << std::endl;
    return os;
  }
};
class MergeFeat {
 public:
  MergeFeat(const MergeFeatOptions &opts): spkName_(""), spkIndex_(0),
                                           opts_(opts), utt2new_spk_("")  {}
  ~MergeFeat() {
    if(! utt2new_spk_.empty()) {
      utt2new_spk_output_.Close();
    }
  }
  void SetSpkName(const std::string &spkName) {
    spkName_ = spkName; 
    spkIndex_ = 0; 
    sample_counters_.resize(0); 
    spk_features_.resize(0);
    spk_vads_.resize(0);
    utt2spk_.resize(0);
    uttlist_.resize(0);
  }
  void DumpFeats(BaseFloatMatrixWriter &feature_writer, BaseFloatVectorWriter &vad_writer, std::ostream &os);
  int32 GetEndIndex(const int32 start, const int32 expected_samples, int32 *samples);
  void DumpSeqFeats(const int32 start, const int32 end, const int32 samples, BaseFloatMatrixWriter &feature_writer, 
                     BaseFloatVectorWriter &vad_writer);
  void DumpRandFeats(const std::vector<int32> &index_vec, BaseFloatMatrixWriter &feature_writer, BaseFloatVectorWriter &vad_writer);
  void PushMat(const Matrix<BaseFloat> &features) { spk_features_.push_back(features); }
  void PushVec(const Vector<BaseFloat> &vec) { spk_vads_.push_back(vec);}
  void PushUtt(const std::string &utt) {uttlist_.push_back(utt);}
  void SetUtt2SpkOutput(std::string &utt2spk_fname) {
    utt2new_spk_ = utt2spk_fname;
    if(! utt2new_spk_.empty()) {
      utt2new_spk_output_.Open(utt2new_spk_, false, false);
    } 
  }
 private:
  std::string spkName_;
  int32 spkIndex_;
  MergeFeatOptions opts_;
  int32 tot_samples_;
  std::vector<int32> sample_counters_;
  std::vector<Matrix<BaseFloat> > spk_features_;
  std::vector<Vector<BaseFloat> > spk_vads_;
  std::vector<std::string> utt2spk_;
  std::string utt2new_spk_;
  Output utt2new_spk_output_;
  std::vector<std::string> uttlist_;
  std::map<std::string, int32> utt_map_;
  void DumpUtt2Spk(std::ostream &os);
  bool HasValue(std::string &utt) {
    std::map<std::string, int32>::iterator it = utt_map_.find(utt), end = utt_map_.end();
    if(it == end) {
      utt_map_.insert(std::pair<std::string, int32>(utt, 1));
      return false;
    } 
    return true;
  }
  bool CheckSeqSamples(const int32 start, const int32 end, const int32 samples);
  std::string GetSpkName(const bool use_original_spk = false) {
    std::string cur_spkName = spkName_;
    if (! use_original_spk) {
      spkIndex_ ++;
      char spk_char_str[2048];
      sprintf(spk_char_str, "%s_%04d", spkName_.c_str(), spkIndex_);
      cur_spkName = spk_char_str;
    }
    utt2spk_.push_back(cur_spkName);
    return cur_spkName;
  }
  void MergeMatrix(const int32 index, Matrix<BaseFloat> *merged_features,
                   int32 *cur_rows) {
    const Matrix<BaseFloat> &cur_features = spk_features_[index];
    int32 rows = cur_features.NumRows(), cols = cur_features.NumCols();
    KALDI_ASSERT(merged_features->NumCols() == cols && *cur_rows + rows <= merged_features->NumRows());
    SubMatrix<BaseFloat> block_features(*merged_features, *cur_rows, rows, 0, cols);
    block_features.CopyFromMat(cur_features);
    *cur_rows += rows;
  }
  void MergeVector(const int32 index, Vector<BaseFloat> *merged_vec,
                   int32 *cur_dims) {
    const Vector<BaseFloat> &cur_vec = spk_vads_[index];
    int32 dim = cur_vec.Dim();
    KALDI_ASSERT(*cur_dims + dim <= merged_vec->Dim());
    SubVector<BaseFloat> block_vec(*merged_vec, *cur_dims, dim);
    block_vec.CopyFromVec(cur_vec);
    *cur_dims += dim;
  }
};
///
int32 MergeFeat::GetEndIndex(const int32 start, const int32 expected_samples, int32 *samples) {
  KALDI_ASSERT(expected_samples < tot_samples_);
  KALDI_ASSERT(start < sample_counters_.size());
  int32 i = start;
  int32 extracted_samples = 0;
  while(extracted_samples < expected_samples) {
    extracted_samples += sample_counters_[i];
    i ++;
    i %= sample_counters_.size();
  }
  *samples = extracted_samples;
  return i;
}
///
bool MergeFeat::CheckSeqSamples(const int32 start, const int32 end, const int32 samples) {
  if(start == end)
    return false;
  int32 extracted_samples = 0;
  if(start < end) {
    for(int i = start; i < end; ++i) {
      extracted_samples += sample_counters_[i];
    }
  } else {
    for(int i = start; i < sample_counters_.size(); ++i) 
      extracted_samples += sample_counters_[i];
    for(int i = 0; i < end; ++i)
      extracted_samples += sample_counters_[i];
  }
  if(extracted_samples != samples)
    return false;
  return true;
}
//
void MergeFeat::DumpSeqFeats(const int32 start, const int32 end, const int32 samples, BaseFloatMatrixWriter &feature_writer,
                             BaseFloatVectorWriter &vad_writer) {
  if(! CheckSeqSamples(start, end, samples))
    KALDI_ERR << "samples declared " << samples << " are inconsistency";
  int32 dim = spk_features_[0].NumCols();
  bool use_original_spk = false;
  if(start==0 && end == sample_counters_.size())
    use_original_spk = true;
  Matrix<BaseFloat> merged_features(samples, dim);
  if(start < end) {
    int32 cur_rows = 0;
    for(int32 i = start; i < end; ++i)
      MergeMatrix(i, &merged_features, &cur_rows);
    KALDI_ASSERT(cur_rows == samples);
    std::string spk = GetSpkName(use_original_spk);
    feature_writer.Write(spk, merged_features);
    // write out new utt2spk
    if(! utt2new_spk_.empty()) {
      for(int32 i = start; i < end; ++i) { 
        if(! HasValue(uttlist_[i]))
          utt2new_spk_output_.Stream() << uttlist_[i] << " " << spk << std::endl;
      }
    }
    if (spk_vads_.size() > 0) {
      Vector<BaseFloat> merged_vads(samples);
      int32 cur_dims = 0;
      for(int32 i = start; i < end; ++i)
        MergeVector(i, &merged_vads, &cur_dims);
      KALDI_ASSERT(cur_dims == samples);
      vad_writer.Write(spk, merged_vads);
    } 
  } else {
    int32 cur_rows = 0;
    for(int32 i = start; i < spk_features_.size(); ++i)
      MergeMatrix(i, &merged_features, &cur_rows);
    for(int32 i = 0; i < end; ++i) 
      MergeMatrix(i, &merged_features, &cur_rows);
    KALDI_ASSERT(cur_rows == samples);
    std::string  spk = GetSpkName();
    feature_writer.Write(spk, merged_features);
    if(! utt2new_spk_.empty()) {
      for(int32 i = start; i < spk_features_.size(); ++i) {
        if(! HasValue(uttlist_[i]))
          utt2new_spk_output_.Stream() << uttlist_[i] << " " << spk << std::endl;
      }
      for(int32 i = 0; i < end; ++i) {
        if(! HasValue(uttlist_[i]))
          utt2new_spk_output_.Stream()  << uttlist_[i] << " " << spk << std::endl;
      }
    }
    if(spk_vads_.size() > 0) {
      Vector<BaseFloat> merged_vads(samples);
      int32 cur_dims = 0;
      for(int32 i = start; i < spk_vads_.size(); ++i) 
        MergeVector(i, &merged_vads, &cur_dims);
      for(int32 i = 0; i < end; ++i) 
        MergeVector(i, &merged_vads, &cur_dims);
      KALDI_ASSERT(cur_dims = samples);
      vad_writer.Write(spk, merged_vads);
    }
  }
}
///
void MergeFeat::DumpRandFeats(const std::vector<int32> &index_vec, BaseFloatMatrixWriter &feature_writer,
                              BaseFloatVectorWriter &vad_writer) {
  int32 samples = 0;
  for(int32 i = 0; i < index_vec.size(); ++i) 
    samples += spk_features_[index_vec[i]].NumRows();
  if (samples == 0)
    return;
  int32 dim = spk_features_[0].NumCols();
  Matrix<BaseFloat> merged_features(samples, dim);
  int32 cur_rows = 0;
  for(int32 i = 0; i < index_vec.size(); ++ i) {
    int32 j = index_vec[i];
    MergeMatrix(j, &merged_features, &cur_rows);
  }
  KALDI_ASSERT(cur_rows == samples);
  std::string  spk = GetSpkName();
  feature_writer.Write(spk, merged_features);
  if(! utt2new_spk_.empty()) {
    for(int32 i = 0; i < index_vec.size(); ++i) {
      int32 j = index_vec[j];
      if(! HasValue(uttlist_[j]))
        utt2new_spk_output_.Stream() << uttlist_[j] << " " << spk << std::endl;
    }
  }
  if(spk_vads_.size() > 0) {
    Vector<BaseFloat> merged_vads(samples);
    int32 cur_dims = 0;
    for(int32 i = 0; i < index_vec.size(); ++ i) {
      int32 j = index_vec[i];
      MergeVector(j, &merged_vads, &cur_dims);
    }
    KALDI_ASSERT(cur_dims == samples);
    vad_writer.Write(spk, merged_vads);
  }
}
  void MergeFeat::DumpUtt2Spk (std::ostream &os) {
  for(int32 i = 0; i < utt2spk_.size(); ++i) {
    os << utt2spk_[i] << " " << spkName_ << std::endl;
  } 
}
///
void MergeFeat::DumpFeats(BaseFloatMatrixWriter &feature_writer,
                   BaseFloatVectorWriter &vad_writer, std::ostream &os) {
  KALDI_ASSERT(uttlist_.size() == spk_features_.size());
  tot_samples_ = 0; 
  for(int32 i = 0; i < spk_features_.size(); ++i) {
    tot_samples_ += spk_features_[i].NumRows();
    sample_counters_.push_back(spk_features_[i].NumRows());
  }
  if(opts_.partial_sequential_merge == true) {
    if (tot_samples_ <= opts_.min_samples) { // merge all
      DumpSeqFeats(0, spk_features_.size(), tot_samples_, feature_writer, vad_writer);
      DumpUtt2Spk(os);
      return;
    }
    int32 acc_samples = 0, last_samples = 0;
    int i = spk_features_.size() - 1, k;
    while(acc_samples < opts_.min_samples && i >= 0) {
      acc_samples += spk_features_[i].NumRows();
      i --;
    }
    if(i == 0) { // merge all
      DumpSeqFeats(0, spk_features_.size(), tot_samples_, feature_writer, vad_writer);
      DumpUtt2Spk(os);
      return;
    }
    last_samples = acc_samples;
    k = i; i = 0; acc_samples = 0;
    int j = 0;
    while(i <= k) {
      acc_samples += spk_features_[i].NumRows();
      if (acc_samples >= opts_.min_samples) {
        DumpSeqFeats(j, i+1, acc_samples, feature_writer, vad_writer);
        acc_samples = 0;
        j = i+1;
      }
      i ++;
    }
    if(acc_samples > 0 && acc_samples < opts_.min_samples) {
      last_samples += acc_samples;
      DumpSeqFeats(j, spk_features_.size(), last_samples, feature_writer, vad_writer );
    }
    DumpUtt2Spk(os);
    return;
  }
  if (tot_samples_ <= opts_.min_samples || opts_.overall_merge == true) { // too few samples for a speaker
    DumpSeqFeats(0, spk_features_.size(), tot_samples_, feature_writer, vad_writer);
  } else {
    int32 cur_samples = 0;
    int32 startIndex = 0;
    while (cur_samples < tot_samples_) {
      int32 rand_samples = RandInt(opts_.min_samples, opts_.max_samples);
      KALDI_VLOG(3) << "Current random samples " << rand_samples;
      int32 endIndex, extracted_samples = 0;
      endIndex = GetEndIndex(startIndex, rand_samples, &extracted_samples);
      DumpSeqFeats(startIndex, endIndex, extracted_samples, feature_writer, vad_writer);
      startIndex = endIndex;
      cur_samples += extracted_samples;
    }
    while (cur_samples < tot_samples_*opts_.times) {  // now we extract samples randomly
      int32 rand_samples = RandInt(opts_.min_samples, opts_.max_samples);
      std::vector<int32> index_vec;
      int32 now_samples = 0;
      while(now_samples < rand_samples) {
        int32 rand_index = RandInt(0, spk_features_.size()-1);
        KALDI_ASSERT(rand_index >=0 && rand_index < spk_features_.size());
        index_vec.push_back(rand_index);
        now_samples += sample_counters_[rand_index];
      }
      DumpRandFeats(index_vec, feature_writer, vad_writer);
      cur_samples += now_samples;   
    }
  }
  DumpUtt2Spk(os);
}

} // namespace kaldi

int main(int argc, char *argv[]) {
  try {
    using namespace kaldi;
    const char *usage =
          "Splice features from the same speaker\n"
          "Usage: splice-feats-by-speaker [options] <spk2utt> <feature-rspecifier> [<vad-rspecifier>] <new_utt2spk> <feature-wspecifier> [<vad-wspecifier>]\n"
          "e.g.: splice-feats-by-speaker ark:spk2utt scp:feats.scp scp:vads.scp ark,scp:foo.ark,foo.scp utt2spk ark,scp:foo-vad.ark,foo-vad.scp\n"
          "See also: copy-feats, copy-matrix, select-feats\n";
    ParseOptions po(usage);
    std::string utt2new_spk = ""; 
    po.Register("utt2new-spk", &utt2new_spk, "map utt to a \'new\' spk, and the \'new\' spk is a subset of the original spk");
    MergeFeatOptions merge_opts;
    merge_opts.Register(&po);
    po.Read(argc, argv);
    if(po.NumArgs() != 4 && po.NumArgs() != 6) {
      po.PrintUsage();
      exit(-1); 
    }
    std::string spk2utt_rspecifier = po.GetArg(1);
    std::string feature_rspecifier = po.GetArg(2);
    std::string vad_rspecifier = "";
    if (po.NumArgs() == 6)
      vad_rspecifier = po.GetArg(3);
    std::string utt2spk_fname = "";
    std::string feature_wspecifier = "";
    if (po.NumArgs() == 6) {
      utt2spk_fname = po.GetArg(4);
      feature_wspecifier = po.GetArg(5);
    }
    else {
      utt2spk_fname = po.GetArg(3);
      feature_wspecifier = po.GetArg(4);
    }
    std::string vad_wspecifier = "";
    if (po.NumArgs() == 6)
      vad_wspecifier = po.GetArg(6);

    SequentialTokenVectorReader spk2utt_reader(spk2utt_rspecifier);
    RandomAccessBaseFloatMatrixReader feature_reader(feature_rspecifier);
    RandomAccessBaseFloatVectorReader vad_reader;
    if ( !vad_rspecifier.empty() )
      vad_reader.Open(vad_rspecifier);
    BaseFloatMatrixWriter feature_writer(feature_wspecifier);
    BaseFloatVectorWriter vad_writer;
    Output utt2spk_output(utt2spk_fname, false);
    if( !vad_wspecifier.empty() )
      vad_writer.Open(vad_wspecifier);
    MergeFeat merge_feature(merge_opts);
    merge_feature.SetUtt2SpkOutput(utt2new_spk);
    int32 error_num = 0;
    int32 total_num = 0;
    for(; !spk2utt_reader.Done(); spk2utt_reader.Next()) {
      std::string spk = spk2utt_reader.Key();
      merge_feature.SetSpkName(spk);
      const std::vector<std::string> &uttlist = spk2utt_reader.Value();
      for(int32 i  = 0; i < uttlist.size();  ++i, ++total_num) {
        std::string utt = uttlist[i];
        if( !feature_reader.HasKey(utt) ) {
          KALDI_WARN << "no feature for utterance " << "'"
                     << utt << "'";
          error_num ++;
          continue;
        }
        const Matrix<BaseFloat> &features = feature_reader.Value(utt);
        if( !vad_rspecifier.empty() ) {
          if( !vad_reader.HasKey(utt) ) {
            KALDI_WARN << "no vad for utterance " << "'"
                     << utt << "'";
            error_num ++;
            continue;
          }
          const Vector<BaseFloat> &vads = vad_reader.Value(utt);
          if (vads.Dim() != features.NumRows()) {
            KALDI_WARN << "features are " << features.NumRows() << ", vads are"
                       << vads.Dim() << " for file " << utt;
            error_num ++;
            continue;
          }
          merge_feature.PushVec(vads);
        }
        merge_feature.PushMat(features);
        merge_feature.PushUtt(utt);
      }
      merge_feature.DumpFeats(feature_writer, vad_writer, utt2spk_output.Stream());
    }
    utt2spk_output.Close();
    KALDI_LOG << "total utterances " << total_num << " processed, " 
              << "utterances " << error_num << " have no features or vads";
    return 0;
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}

