#include "base/kaldi-common.h"
#include "matrix/matrix-common.h"
#include "util/common-utils.h"
#include "matrix/kaldi-matrix.h"
#include "hmm/posterior.h"

namespace kaldi {

} // namespace kaldi

int main(int argc, char *argv[]) {
  try {
    using namespace kaldi;
    const char *usage =
      "Make feature labels from features to do feature enhancement\n"
      "Usage: copy-feats-to-label [options] <utt2utt> <feats-rxfilename1> <feats-rxfilename2> <post-wspecifier>\n"
      "e.g.: copy-feats-to-label utt2utt ark:noise.ark ark:clean.ark post.txt\n";
    
    ParseOptions po(usage);
    int32 feat_dim = 0;
    po.Register("feat-dim", &feat_dim, "static feature dimension as the DNN output");
    po.Read(argc, argv);
    if(po.NumArgs() != 4) {
      po.PrintUsage();
      exit(1);
    }
    std::string utt2utt_file = po.GetArg(1); 
    std::string rspecifier_noise = po.GetArg(2);
    std::string rspecifier_clean = po.GetArg(3);
    std::string wspecifier_post = po.GetArg(4);
    RandomAccessTokenReader reader_utt2utt;

    if(!reader_utt2utt.Open(utt2utt_file)) {
      KALDI_ERR << "Error: utt2utt file " << utt2utt_file << " cannot open";
    }
    SequentialBaseFloatMatrixReader reader_noise(rspecifier_noise);
    RandomAccessBaseFloatMatrixReader reader_clean(rspecifier_clean);
    PosteriorWriter writer_post(wspecifier_post);  
    for(; !reader_noise.Done(); reader_noise.Next()) {
      std::string utt = reader_noise.Key();
      Matrix<BaseFloat> feature_noise = reader_noise.Value();
      if(feat_dim == 0)
        feat_dim = feature_noise.NumCols();
      KALDI_ASSERT(feature_noise.NumCols() == feat_dim);
      if(!reader_utt2utt.HasKey(utt)) {
        KALDI_WARN << "no target utterance to be mapped for " << utt;
        continue;
      }
      std::string utt_mapped = reader_utt2utt.Value(utt);
      Matrix<BaseFloat> feature_clean = reader_clean.Value(utt_mapped);
      KALDI_ASSERT(feature_clean.NumCols() == feat_dim);
      if(feature_noise.NumRows() != feature_clean.NumRows()) {
        KALDI_WARN << "features are mismatched, input features are " << feature_noise.NumRows() 
                   << ", while output features are " << feature_clean.NumRows();
        continue;
      }
      Posterior post;
      for(int32 i = 0; i < feature_clean.NumRows(); i ++) {
        std::vector<std::pair<int32, BaseFloat> > post_t;
        for(int32 j = 0; j < feature_clean.NumCols(); j++) {
           post_t.push_back(std::pair<int32, BaseFloat>(j, feature_clean(i,j)));
        }
        post.push_back(post_t);
      }
      writer_post.Write(utt, post);
    }
  } catch(std::exception &e) {
    std::cerr << e.what();
    return(-1);
  }
}
