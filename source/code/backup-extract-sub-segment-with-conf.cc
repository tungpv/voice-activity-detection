#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"

#ifndef _MSC_VER
#include <unistd.h>
#endif

namespace kaldi {
struct SegItem{
  std::string fname;
  float start;
  float end;
};
struct CTMItem {
  int32 chanId;
  float start;
  float dur;
  std::string word;
  float conf;
};
void SplitSegLine(std::string &line, std::string *key, SegItem *value) {
  std::string rest;
  SplitStringOnFirstSpace(line, key, &rest);
  std::string rest2;
  SplitStringOnFirstSpace(rest, &value->fname, &rest2);
  std::string start_str, end_str;
  SplitStringOnFirstSpace(rest2, &start_str, &end_str);
  ConvertStringToReal(start_str, &value->start);
  ConvertStringToReal(end_str, &value->end);
}
void SplitCTMLine (std::string &line, std::string *key, CTMItem *value) {
  std::string rest;
  SplitStringOnFirstSpace(line, key, &rest);
  std::string rest2, str_tmp;
  SplitStringOnFirstSpace(rest, &str_tmp, &rest2);
  ConvertStringToInteger(str_tmp,&value->chanId);
  SplitStringOnFirstSpace(rest2, &str_tmp, &rest);
  ConvertStringToReal(str_tmp, &value->start);
  SplitStringOnFirstSpace(rest, &str_tmp, &rest2);
  ConvertStringToReal(str_tmp, &value->dur);
  SplitStringOnFirstSpace(rest2, &value->word, &rest);
  ConvertStringToReal(rest, &value->conf);
}
void PrintUttCTM (const std::string &str_info, 
                  const std::string &key,
                  const std::vector<CTMItem> &vec_ctm) {
  std::vector<CTMItem>::const_iterator it = vec_ctm.begin(), end = vec_ctm.end();
  std::cout << "\n\n" << str_info << "\n";
  for(; it < end; it++) {
    std::cout << key << " " << it->chanId << " "
              << it->start << " " << it->dur << " " << it->word
              << " " << it->conf << "\n";
  }
}
float ExtractSubSeg(const float conf_thresh, 
                   const float merge_tolerance,  // 0.05
                   const std::map<std::string, SegItem> &map_seg, 
                   const std::string &key, 
                   std::vector<CTMItem> &vec_ctm, 
                   std::ostream &os_seg, std::ostream &os_text) {
  std::map<std::string, SegItem>::const_iterator it_map = map_seg.find(key),
                                                 it_map_end = map_seg.end();
  float tot_sec = 0;
  if(it_map == it_map_end) {
    KALDI_ERR << "key " << key << " does not exist in the segments";
  }
  // PrintUttCTM ("Before thresholding", key, vec_ctm);
  const SegItem seg_item = it_map->second;
  std::vector<CTMItem>:: iterator it_ctm = vec_ctm.begin(), it_fwd, it_end = vec_ctm.end();
  
  it_fwd = it_ctm;
  while ( it_fwd < it_end) {
    *it_ctm = *it_fwd;
    it_fwd ++;
    float end_time = it_ctm->start + it_ctm->dur;
    while( it_fwd < it_end &&
           it_fwd->conf >= conf_thresh &&
           it_ctm->conf >= conf_thresh &&
           it_fwd->start - end_time < merge_tolerance) {
      it_ctm->dur += it_fwd->start - end_time + it_fwd->dur;
      it_ctm->word = it_ctm->word + " " + it_fwd->word;
      end_time = it_fwd->start + it_fwd->dur;
      it_fwd ++;
    }
    if(it_ctm ->conf >= conf_thresh) {
      it_ctm ++;
    }
  }
  vec_ctm.erase(it_ctm, it_end);
  // PrintUttCTM ("After thresholding", key, vec_ctm);
  it_ctm = vec_ctm.begin(); it_end = vec_ctm.end();
  int32 idx = 0;
  std::ostringstream ostr_seg, ostr_txt;
  for(; it_ctm < it_end; it_ctm ++) {
    idx ++;
    char cbuf[1024];
    sprintf(cbuf, "%04d", idx);
    std::string key_new = key + '_' + cbuf;
    float start_time = seg_item.start + it_ctm->start;
    float end_time = start_time + it_ctm->dur;
    KALDI_ASSERT (end_time <= seg_item.end);
    ostr_seg << key_new << " " << seg_item.fname << " " << start_time << " " << end_time << "\n";
    ostr_txt << key_new << " " << it_ctm->word << "\n";
    tot_sec += it_ctm->dur;
  }
  // std::cout << "segment:\n" << ostr_seg.str() << "\n";
  // std::cout << "text:\n" << ostr_txt.str() << "\n";
  if(vec_ctm.size() > 0) {
    os_seg << ostr_seg.str();
    os_text << ostr_txt.str();
  }
  return tot_sec;
}

}  // end namespace kaldi

int main(int argc, char *argv[]) {
  using namespace kaldi;
  try{
    const char *usage = 
    "Extract sub-segments from ctm file with confidence score as threshold.\n"
    "Usage: extract-sub-segment-with-conf [options] segments utt.ctm new_segments new_text\n"
    "e.g.:\n"
    "extract-sub-segment-with-conf --conf-thresh=0.5 segments utt.ctm new_segments new_text\n";
    
    ParseOptions po(usage);
    bool smoothing = false;
    po.Register("smoothing", &smoothing, "do smoothing, ignoring actual score");
    float conf_thresh = 0;
    po.Register("conf-thresh", &conf_thresh, "confidence threshold to extract sub-segment");
    float merge_tolerance = 0.05;
    po.Register("merge-tolerance", &merge_tolerance, "merge tolerance in sec");
    po.Read(argc, argv);
    if(po.NumArgs() != 4) {
      po.PrintUsage();
      exit(-1);
    }
    std::string segment_filename = po.GetArg(1);
    std::string utt_ctm_filename = po.GetArg(2);
    std::string new_seg_filename, new_text_filename;
    new_seg_filename = po.GetArg(3);
    new_text_filename = po.GetArg(4);

    bool sbinary_in;
    Input ki(segment_filename, &sbinary_in);
    std::istream &infile = ki.Stream();
    std::map<std::string, SegItem> map_segment;
    std::string line;
    int32 line_number = 0;
    while(std::getline(infile, line)) {
      line_number ++;
      std::string key; 
      SegItem value;
      SplitSegLine(line, &key, &value);
      map_segment.insert(std::pair<std::string, SegItem>(key, value));
    }
    float tot_sec = 0;
    Input ki_ctm(utt_ctm_filename, &sbinary_in);
    std::set<std::string> set_key;
    std::string prev_key = "";
    std::vector<CTMItem> vec_ctm;
    Output ko_seg(new_seg_filename, false);
    Output ko_text(new_text_filename, false);
    while(std::getline(ki_ctm.Stream(), line)) {
      std::string cur_key;
      CTMItem value;
      SplitCTMLine(line, &cur_key, &value);
      if(cur_key != prev_key) {
        if(set_key.find(cur_key) != set_key.end()) {
          KALDI_ERR << "key " << cur_key << " duplicated";
        }
        set_key.insert(cur_key);
        if(vec_ctm.size() > 0) {
         tot_sec += ExtractSubSeg(conf_thresh, merge_tolerance,
                        map_segment, 
                        prev_key, vec_ctm, ko_seg.Stream(), ko_text.Stream());
          vec_ctm.resize(0);
        }
        vec_ctm.push_back(value);
        prev_key = cur_key;
      } else {
        vec_ctm.push_back(value); 
      }
    }
    if(vec_ctm.size() > 0) {
      tot_sec += ExtractSubSeg(conf_thresh, merge_tolerance,
                    map_segment, 
                    prev_key, vec_ctm, ko_seg.Stream(), ko_text.Stream());
    }
    ko_seg.Close();
    ko_text.Close();
    KALDI_LOG << "Total " << tot_sec/3600 << " hours remained after selection";
    return 0;
  } catch(const std::exception &e) {
    std::cerr << e.what(); 
    return -1;
  }
}
