#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "mgb-lib.h"
#include <string>
#ifndef _MSC_VER
#include <unistd.h>
#endif

namespace kaldi {
void OutputData(const std::string &key, const std::string &spk, const float start, const float end, const std::string &str_utt,
                 std::ostream &os_utt2spk, std::ostream &os_seg, std::ostream &os_text) {
  char buf[2048];
  sprintf(buf,"%s_seg-%07d:%07d",spk.c_str(), static_cast<int32>(start*100), static_cast<int32>(end*100));
  std::string uttName = buf;
  os_utt2spk << uttName << " " << spk << "\n";
  os_seg << uttName << " " << key << " " << start << " " << end << "\n";
  os_text << uttName << " " << str_utt << "\n";
} 
void ConvertCTM(const std::string &key, 
                const vecCTM &vec_ctm, std::ostream &os_utt2spk, std::ostream &os_seg, std::ostream &os_text) {
  std::string str_prev_spk = "";
  std::string str_utt = "";
  float start = 0, end = 0;
  for(int32 i = 0; i < vec_ctm.size(); i++) {
    std::string str_cur_spk = vec_ctm[i].spk;
    if(str_cur_spk != str_prev_spk ) {
      if(! str_utt.empty()) 
        OutputData(key, str_prev_spk, start, end, str_utt, os_utt2spk, os_seg, os_text);
      start = vec_ctm[i].start; 
      end = start + vec_ctm[i].dur;
      str_utt =vec_ctm[i].word;
      str_prev_spk = str_cur_spk;
      continue;
    }
    if (vec_ctm[i].start - end <= 0.3) {
      str_utt = str_utt + " " + vec_ctm[i].word;
      end = vec_ctm[i].start + vec_ctm[i].dur;
    } else {
      OutputData(key, str_prev_spk, start, end, str_utt, os_utt2spk, os_seg, os_text);
      start = vec_ctm[i].start;
      end = start + vec_ctm[i].dur;
      str_utt = vec_ctm[i].word;
    }
  }
  if (! (str_utt.empty() || str_prev_spk.empty())) {
    OutputData(key, str_prev_spk, start, end, str_utt, os_utt2spk, os_seg, os_text);
  }
}

}  // end namespace kaldi

int main(int argc, char*argv[]) {
  using namespace kaldi;
  try {
    const char *usage =
    "Extract kaldi data from a ctm variant that has speaker info.\n"
    "e.g.\n"
    "mgb-ctm-to-kaldi-data ctm.has-spkr utt2spk segments text";
    ParseOptions po(usage);
    po.Read(argc, argv);
    if(po.NumArgs() != 4) {
      po.PrintUsage();
      exit(-1);
    }
    std::string ctm_filename = po.GetArg(1);
    std::string utt2spk_filename = po.GetArg(2);
    std::string seg_filename = po.GetArg(3);
    std::string trans_filename = po.GetArg(4);
    bool binary_in;
    Input ki(ctm_filename, &binary_in);
    std::istream &infile = ki.Stream();
    std::map<std::string, std::vector<CTMItem> > map_ctm;
    ReadSpkCTMFile(infile, &map_ctm);
    Output utt2spk_output(utt2spk_filename, false);
    Output seg_output(seg_filename, false);
    Output trans_output(trans_filename, false);
    std::map<std::string, std::vector<CTMItem> >::iterator it = map_ctm.begin(), it_end = map_ctm.end();
    for(; it != it_end; it++) {  // output is not sorted here
      vecCTM &vec_ctm = it->second;
      ConvertCTM(it->first, vec_ctm, utt2spk_output.Stream(), seg_output.Stream(), trans_output.Stream());
    }                    
    utt2spk_output.Close();
    seg_output.Close();
    trans_output.Close();
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
