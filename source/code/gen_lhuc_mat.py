#!/usr/bin/python


# from __future__ import print_function
import sys

from optparse import OptionParser

parser = OptionParser()
parser.add_option('--utt2x', dest='utt2x', help='utt2x map file, for instance utt2spk')
parser.add_option('--hid-dim', dest='hid_dim', help='hidden dimensions of target NN')
parser.add_option('--hid-layer', dest='hid_layer', help='hidden layers of target NN')
(options, args) = parser.parse_args()

if(options.utt2x == None):
	parser.print_help()
	sys.exit(1)

utt2x = options.utt2x
hid_dim = int(options.hid_dim)
hid_layer = int(options.hid_layer)

utt2x_file = open(utt2x, 'r')
for line in utt2x_file:
	tokens = line.split()
	if len(tokens) != 2:
		print >> sys.stderr, "ERROR, bad line: %s" %line
		sys.exit(1)
	print("%s [" %tokens[1])
	for row in range(hid_layer):
		for col in range(hid_dim):
			print ' 0',			
		print
	print(" ]")		
utt2x_file.close()
	
