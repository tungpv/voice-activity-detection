#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "mgb-lib.h"
#include <string>

namespace kaldi {

} // namespace kaldi

int main(int argc, char **argv) {
  using namespace kaldi;
  try {
    const char *usage =
    "Filter target ctm file using source.\n"
    "e.g.:\n"
    "filter-ctm ctm target_ctm new_ctm\n";
    ParseOptions po(usage);
    bool use_conf = false;
    po.Register("use-conf", &use_conf, "use word confidence threshold");
    float conf_thresh = 0;
    po.Register("conf-thresh", &conf_thresh, "word confidence threshold");
    po.Read(argc, argv);
    if(po.NumArgs() != 3) {
      po.PrintUsage();
      exit (-1);
    }
    std::string src_ctm_filename = po.GetArg(1);
    std::string target_ctm_filename = po.GetArg(2);
    std::string new_ctm_filename = po.GetArg(3);
    bool binary_in;
    Input ki(src_ctm_filename, &binary_in);
    std::map<std::string, std::vector<CTMItem> > map_ctm;
    ReadCTMFile(ki.Stream(), &map_ctm);
    ki.Close();
    Input ctm_ki(target_ctm_filename, &binary_in);
    Output ctm_ko(new_ctm_filename, false);
    std::string line;
    float tot_len = 0;
    while(std::getline(ctm_ki.Stream(), line)) {
      std::string key;
      CTMItem value, overlap_ctm;
      SplitCTMLine(line, &key, &value);
      if(FindTimeOverlap(map_ctm, key, value, &overlap_ctm) == true) {
        DumpCTMLine(ctm_ko.Stream(), key, value, &tot_len);
      }
    }
    ctm_ki.Close();
    std::string len_str;
    char buf[1024];
    sprintf(buf, "%.4f", tot_len/3600);
    len_str = buf;
    KALDI_LOG << "Total data selected: " << len_str << " hrs";
    ctm_ko.Close();
    return 0;
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  } 
}
