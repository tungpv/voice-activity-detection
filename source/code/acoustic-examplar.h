#ifndef ACOUSTIC_EXAMPLAR_H_
#define ACOUSTIC_EXAMPLAR_H_
#include <string>
#include <iostream>
#include <vector>
#include <map>

namespace kaldi {
struct UnitTimeBoundary {
  float start;
  float end;
  std::string featFile;
  UnitTimeBoundary(float stime, float etime, std::string &fName):start(stime),
                end(etime), featFile(fName) {}
};
class AcousticUnit {
 public:
  AcousticUnit();
  void Init(std::string &words, std::string &segments, std::string &uttCtmFile);              
 private:
  void ReadCTMFile(std::string &uttCtmFile);
  void ReadWords(std::string &words);
  void ReadSegments(std::string &segments); 
  void InsertAcousticUnit(int wordId, int segId, UnitTimeBoundary &unitBound);
  std::map<int, std::map<int, std::vector<UnitTimeBoundary> > > map_wrd2boundary_;
  std::map<std::string, int> map_word2Id_;
  std::map<std::string, int> map_featFile2SegmentId_; 
};

}  // namespace kaldi
#endif
