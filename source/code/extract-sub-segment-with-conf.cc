#include "base/io-funcs.h"
#include "util/kaldi-io.h"
#include "base/kaldi-math.h"
#include "base/kaldi-common.h"
#include "util/common-utils.h"

#ifndef _MSC_VER
#include <unistd.h>
#endif

namespace kaldi {
struct SegItem{
  std::string fname;
  float start;
  float end;
};
struct CTMItem {
  int32 chanId;
  float start;
  float dur;
  std::string word;
  float conf;
};
void SplitSegLine(std::string &line, std::string *key, SegItem *value) {
  std::string rest;
  SplitStringOnFirstSpace(line, key, &rest);
  std::string rest2;
  SplitStringOnFirstSpace(rest, &value->fname, &rest2);
  std::string start_str, end_str;
  SplitStringOnFirstSpace(rest2, &start_str, &end_str);
  ConvertStringToReal(start_str, &value->start);
  ConvertStringToReal(end_str, &value->end);
}
void SplitCTMLine (std::string &line, std::string *key, CTMItem *value) {
  std::string rest;
  SplitStringOnFirstSpace(line, key, &rest);
  std::string rest2, str_tmp;
  SplitStringOnFirstSpace(rest, &str_tmp, &rest2);
  ConvertStringToInteger(str_tmp,&value->chanId);
  SplitStringOnFirstSpace(rest2, &str_tmp, &rest);
  ConvertStringToReal(str_tmp, &value->start);
  SplitStringOnFirstSpace(rest, &str_tmp, &rest2);
  ConvertStringToReal(str_tmp, &value->dur);
  SplitStringOnFirstSpace(rest2, &value->word, &rest);
  ConvertStringToReal(rest, &value->conf);
}
void PrintUttCTM (const std::string &str_info, 
                  const std::string &key,
                  const std::vector<CTMItem> &vec_ctm) {
  std::vector<CTMItem>::const_iterator it = vec_ctm.begin(), end = vec_ctm.end();
  std::cout << "\n\n" << str_info << "\n";
  for(; it < end; it++) {
    std::cout << key << " " << it->chanId << " "
              << it->start << " " << it->dur << " " << it->word
              << " " << it->conf << "\n";
  }
}
float ExtractSubSeg(const float conf_thresh, 
                   const float merge_tolerance,  // 0.05
                   const std::map<std::string, SegItem> &map_seg, 
                   const std::string &key, 
                   std::vector<CTMItem> &vec_ctm, 
                   std::ostream &os_seg, std::ostream &os_text) {
  std::map<std::string, SegItem>::const_iterator it_map = map_seg.find(key),
                                                 it_map_end = map_seg.end();
  float tot_sec = 0;
  if(it_map == it_map_end) {
    KALDI_ERR << "key " << key << " does not exist in the segments";
  }
  // PrintUttCTM ("Before thresholding", key, vec_ctm);
  const SegItem seg_item = it_map->second;
  std::vector<CTMItem>:: iterator it_ctm = vec_ctm.begin(), it_fwd, it_end = vec_ctm.end();
  
  it_fwd = it_ctm;
  while ( it_fwd < it_end) {
    *it_ctm = *it_fwd;
    it_fwd ++;
    float end_time = it_ctm->start + it_ctm->dur;
    while( it_fwd < it_end &&
           it_fwd->conf >= conf_thresh &&
           it_ctm->conf >= conf_thresh &&
           it_fwd->start - end_time < merge_tolerance) {
      it_ctm->dur += it_fwd->start - end_time + it_fwd->dur;
      it_ctm->word = it_ctm->word + " " + it_fwd->word;
      end_time = it_fwd->start + it_fwd->dur;
      it_fwd ++;
    }
    if(it_ctm ->conf >= conf_thresh) {
      it_ctm ++;
    }
  }
  vec_ctm.erase(it_ctm, it_end);
  // PrintUttCTM ("After thresholding", key, vec_ctm);
  it_ctm = vec_ctm.begin(); it_end = vec_ctm.end();
  int32 idx = 0;
  std::ostringstream ostr_seg, ostr_txt;
  for(; it_ctm < it_end; it_ctm ++) {
    idx ++;
    char cbuf[1024];
    sprintf(cbuf, "%04d", idx);
    std::string key_new = key + '_' + cbuf;
    float start_time = seg_item.start + it_ctm->start;
    float end_time = start_time + it_ctm->dur;
    KALDI_ASSERT (end_time <= seg_item.end);
    ostr_seg << key_new << " " << seg_item.fname << " " << start_time << " " << end_time << "\n";
    ostr_txt << key_new << " " << it_ctm->word << "\n";
    tot_sec += it_ctm->dur;
  }
  // std::cout << "segment:\n" << ostr_seg.str() << "\n";
  // std::cout << "text:\n" << ostr_txt.str() << "\n";
  if(vec_ctm.size() > 0) {
    os_seg << ostr_seg.str();
    os_text << ostr_txt.str();
  }
  return tot_sec;
}
void LoadCTMFile(std::string &utt_ctm_filename, std::vector<CTMItem> &vec_ctm,
                 std::vector<std::pair<std::string, int> > *utt_bound_index) {
  bool sbinary_in;
  Input ki_ctm(utt_ctm_filename, &sbinary_in);
  std::set<std::string> set_key;
  std::string prev_key = "", key;
  std::string line;
  while(std::getline(ki_ctm.Stream(), line)) {
    CTMItem value;
    SplitCTMLine(line, &key, &value);
    vec_ctm.push_back(value);
    if(prev_key != key) {
      if(set_key.find(key) != set_key.end())
        KALDI_ERR << "ctm file is not well sorted for key " << key;
      int index = vec_ctm.size() - 1;
      if(utt_bound_index != NULL)
        utt_bound_index->push_back(std::pair<std::string, int>(key, index));
      prev_key = key;
    }
  }
  if(utt_bound_index != NULL)
    utt_bound_index->push_back(std::pair<std::string, int>(key, vec_ctm.size()));
  ki_ctm.Close();
}
void MakeSearchGridVector(std::vector<float> &vec_search_grid) {
  float grid_precision = 0;
  for(int i = 0; i <= 200; ++i, grid_precision += 0.005)
    vec_search_grid.push_back(grid_precision); 
}
void GetUttAvgConf(std::vector<CTMItem> &vec_ctm,
                   int ilow, int ihigh, float *utt_conf, 
                   float *effective_length_sec) {
  KALDI_ASSERT(ilow < ihigh);
  *utt_conf = 0;
  *effective_length_sec = 0;
  for(int i = ilow; i < ihigh; ++i) {
    CTMItem &value = vec_ctm[i];
    *utt_conf += value.conf;
    *effective_length_sec  += value.dur;
  }
  *utt_conf  /= (ihigh - ilow);
}
void UttRatio(std::vector<CTMItem> &vec_ctm, 
              std::vector<std::pair<std::string, int> > &utt_bound_index,
              float conf_thresh, float *ratio_selected) {
  float selected_sec = 0, unselected_sec = 0;
  for(int i = 0; i < utt_bound_index.size()-1; ++i) {
    int ilow = utt_bound_index[i].second;
    int ihigh = utt_bound_index[i+1].second;
    float utt_conf, effective_length_sec;
    GetUttAvgConf(vec_ctm, ilow, ihigh, &utt_conf, &effective_length_sec);
    if(utt_conf >= conf_thresh)
      selected_sec += effective_length_sec;
    else
      unselected_sec += effective_length_sec;
  }
  float total_sec = selected_sec + unselected_sec;
  KALDI_ASSERT(total_sec > 0);
  *ratio_selected = selected_sec / total_sec;
  KALDI_LOG << "Utterance: conf_thresh " << conf_thresh
              << ", selected ratio " << *ratio_selected;
}
void UtteranceThreshold(std::string &utt_ctm_filename,
                        float ratio_selected, float *conf_thresh) {
  std::vector<CTMItem> vec_ctm;
  std::vector<std::pair<std::string, int> > utt_bound_index;
  LoadCTMFile(utt_ctm_filename, vec_ctm, &utt_bound_index);
  std::vector<float> vec_search_grid;
  MakeSearchGridVector(vec_search_grid);
  float cur_ratio_selected;
  int imin = 0, imax = vec_search_grid.size() - 1, imiddle;
  while(imin <= imax) {
    imiddle = (imin + imax) / 2;
    float cur_conf_thresh = vec_search_grid[imiddle];
    UttRatio(vec_ctm, utt_bound_index, cur_conf_thresh, &cur_ratio_selected);
    if(cur_ratio_selected < ratio_selected)
      imax = imiddle - 1;
    else if (cur_ratio_selected > ratio_selected)
      imin = imiddle + 1;
    else 
      break;
  }
  *conf_thresh = vec_search_grid[imiddle];
  KALDI_LOG << "Utterance: final_utt_conf_thresh " << *conf_thresh
            << ", actually selected ratio " << cur_ratio_selected;
}
void WordSegRatio(std::vector<CTMItem> &vec_ctm, 
                  float conf_thresh, float *ratio_selected) {

  float selected_sec = 0, unselected_sec = 0;
  for(int i = 0; i < vec_ctm.size(); ++i) {
    CTMItem &value = vec_ctm[i];
    if(value.conf >= conf_thresh)
      selected_sec += value.dur;
    else
      unselected_sec += value.dur;
  }
  float total_sec = selected_sec + unselected_sec;
  KALDI_ASSERT(total_sec > 0);
  *ratio_selected = selected_sec / total_sec;
  KALDI_LOG << "Word: conf_thresh " << conf_thresh
              << ", selected ratio " << *ratio_selected;
}
void WordSegThreshold(std::string &utt_ctm_filename, float ratio_selected,
                      float *conf_thresh) {
  KALDI_ASSERT(ratio_selected > 0);
  std::vector<float> vec_search_grid;
  MakeSearchGridVector(vec_search_grid);
  std::vector<CTMItem> vec_ctm;
  LoadCTMFile(utt_ctm_filename, vec_ctm, NULL);
  float cur_ratio_selected;
  int imin = 0, imax = vec_search_grid.size() - 1, imiddle;
  while(imin <= imax) {
    imiddle = (imin + imax)/2;
    float cur_conf_thresh = vec_search_grid[imiddle];
    WordSegRatio(vec_ctm, cur_conf_thresh, &cur_ratio_selected);
    if(cur_ratio_selected < ratio_selected)
      imax = imiddle - 1;
    else if(cur_ratio_selected > ratio_selected)
      imin = imiddle + 1;
    else
      break;
  }
  *conf_thresh = vec_search_grid[imiddle];
  KALDI_LOG << "Word: final_word_conf_thresh " << *conf_thresh
            << ", actually selected ratio " << cur_ratio_selected;
}
void WriteWordSegment(const std::string &key,
                      std::vector<CTMItem> &vec_ctm, 
                      const int ilow, const int ihigh,
                      const float conf_thresh, const float merge_tolerance,
                      std::map<std::string, SegItem> &map_seg,
                      std::ostream &os_seg, std::ostream &os_text) {
  std::vector<CTMItem> vec_utt;
  vec_utt.resize(ihigh - ilow);
  std::copy(vec_ctm.begin()+ilow, vec_ctm.begin()+ihigh, vec_utt.begin());
  ExtractSubSeg(conf_thresh, merge_tolerance, map_seg, key, 
                vec_utt, os_seg, os_text); 
}
void WriteUtterance(const std::string &key,
                    std::vector<CTMItem> &vec_ctm,
                    const int ilow, const int ihigh,
                    const float conf_thresh, const float merge_tolerance,
                    std::map<std::string, SegItem> &map_seg,
                    std::ostream &os_seg, std::ostream &os_text) {
  float utt_conf, effective_length_sec;
  GetUttAvgConf(vec_ctm, ilow, ihigh, &utt_conf, &effective_length_sec);
  if(utt_conf >= conf_thresh) {
    std::vector<CTMItem> vec_utt;
    vec_utt.resize(ihigh - ilow);
    std::copy(vec_ctm.begin()+ilow, vec_ctm.begin()+ihigh, vec_utt.begin());
    ExtractSubSeg(0, merge_tolerance, map_seg, key, 
                  vec_utt, os_seg, os_text);
  }
}

}  // end namespace kaldi

int main(int argc, char *argv[]) {
  using namespace kaldi;
  try{
    const char *usage = 
    "Extract sub-segments from ctm file with confidence score as threshold.\n"
    "Usage: extract-sub-segment-with-conf [options] segments utt.ctm new_segments new_text\n"
    "e.g.:\n"
    "extract-sub-segment-with-conf --conf-thresh=0.5 segments utt.ctm new_segments new_text\n";
    
    ParseOptions po(usage);
    bool select_utterance = false;
    po.Register("select-utterance", &select_utterance, "utterance based data selection");
    float ratio_selected = -0.1;
    po.Register("ratio-selected", &ratio_selected, "ratio to decide how much corpus will be selected");
    float conf_thresh = 0;
    po.Register("conf-thresh", &conf_thresh, "confidence threshold to extract sub-segment");
    float merge_tolerance = 0.02;
    po.Register("merge-tolerance", &merge_tolerance, "merge tolerance in sec");
    po.Read(argc, argv);
    if(po.NumArgs() != 4) {
      po.PrintUsage();
      exit(-1);
    }
    std::string segment_filename = po.GetArg(1);
    std::string utt_ctm_filename = po.GetArg(2);
    std::string new_seg_filename, new_text_filename;
    new_seg_filename = po.GetArg(3);
    new_text_filename = po.GetArg(4);

    bool sbinary_in;
    Input ki_seg(segment_filename, &sbinary_in);
    std::map<std::string, SegItem> map_segment;
    std::string line;
    int32 line_number = 0;
    while(std::getline(ki_seg.Stream(), line)) {  // load segments
      line_number ++;
      std::string key; 
      SegItem value;
      SplitSegLine(line, &key, &value);
      map_segment.insert(std::pair<std::string, SegItem>(key, value));
    }
    ki_seg.Close();
    float tot_sec = 0;
    if(ratio_selected > 0) { // if we do ratio based data selection, then conf_thresh will be overwritten
      if(select_utterance) 
        UtteranceThreshold(utt_ctm_filename, ratio_selected, &conf_thresh);
      else
        WordSegThreshold(utt_ctm_filename, ratio_selected, &conf_thresh);
      std::vector<CTMItem> vec_ctm;
      std::vector<std::pair<std::string, int> > utt_bound_index;
      LoadCTMFile(utt_ctm_filename, vec_ctm, &utt_bound_index);
      Output ko_seg(new_seg_filename, false);
      Output ko_text(new_text_filename, false);
      for(int i = 0; i < utt_bound_index.size()-1; ++i) {
        int ilow = utt_bound_index[i].second;
        std::string key = utt_bound_index[i].first;
        int ihigh = utt_bound_index[i+1].second;
        if(select_utterance)
          WriteUtterance(key, vec_ctm, ilow, ihigh, conf_thresh, merge_tolerance,
                         map_segment, ko_seg.Stream(), ko_text.Stream());
        else
          WriteWordSegment(key, vec_ctm, ilow, ihigh, conf_thresh, merge_tolerance,
                           map_segment, ko_seg.Stream(), ko_text.Stream());   
      } 
      ko_seg.Close();
      ko_text.Close();
      return 0;
    }
    Input ki_ctm(utt_ctm_filename, &sbinary_in);
    std::set<std::string> set_key;
    std::string prev_key = "";
    std::vector<CTMItem> vec_ctm;
    Output ko_seg(new_seg_filename, false);
    Output ko_text(new_text_filename, false);
    while(std::getline(ki_ctm.Stream(), line)) {
      std::string cur_key;
      CTMItem value;
      SplitCTMLine(line, &cur_key, &value);
      if(cur_key != prev_key) {
        if(set_key.find(cur_key) != set_key.end()) {
          KALDI_ERR << "key " << cur_key << " duplicated";
        }
        set_key.insert(cur_key);
        if(vec_ctm.size() > 0) {
         tot_sec += ExtractSubSeg(conf_thresh, merge_tolerance,
                        map_segment, 
                        prev_key, vec_ctm, ko_seg.Stream(), ko_text.Stream());
          vec_ctm.resize(0);
        }
        vec_ctm.push_back(value);
        prev_key = cur_key;
      } else {
        vec_ctm.push_back(value); 
      }
    }
    if(vec_ctm.size() > 0) {
      tot_sec += ExtractSubSeg(conf_thresh, merge_tolerance,
                    map_segment, 
                    prev_key, vec_ctm, ko_seg.Stream(), ko_text.Stream());
    }
    ko_seg.Close();
    ko_text.Close();
    KALDI_LOG << "Total " << tot_sec/3600 << " hours remained after selection";
    return 0;
  } catch(const std::exception &e) {
    std::cerr << e.what(); 
    return -1;
  }
}
