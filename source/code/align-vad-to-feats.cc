#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "matrix/kaldi-matrix.h"

namespace kaldi {

} // namespace kaldi

int main(int argc, char *argv[]) {
  try {
    using namespace kaldi;
    const char *usage = 
       "Make the vad stream corresponding to the current feature stream\n"
       "Usage: align-vad-to-feats [options] <feats-rspecifier> <vad-rspecifier> <vad-wspecifier>\n"
       "e.g.: align-vad-to-feats scp:feats.scp scp:vad.scp ark,scp:foo-vad.ark,foo-vad.scp\n";
    ParseOptions po(usage);
    po.Read(argc, argv);
    if(po.NumArgs() != 3) {
      po.PrintUsage();
      exit(-1);
    }
    std::string feat_rspecifier = po.GetArg(1);
    std::string vad_rspecifier = po.GetArg(2);
    std::string vad_wspecifier = po.GetArg(3);
    SequentialBaseFloatMatrixReader feat_reader(feat_rspecifier);
    RandomAccessBaseFloatVectorReader vad_reader(vad_rspecifier);
    BaseFloatVectorWriter vad_writer(vad_wspecifier);
    int32 num_done = 0, num_err = 0;
    for(; ! feat_reader.Done(); feat_reader.Next(), num_done++) {
      std::string utt = feat_reader.Key();
      Matrix<BaseFloat> feat(feat_reader.Value());
      if(feat.NumRows() == 0) {
        KALDI_WARN << "Empty feature matrix for utterance " << '"' << utt
                   << '"';
        num_err ++;
        continue;
      }
      if(!vad_reader.HasKey(utt)) {
        KALDI_WARN << "No vad stream of utterance " << '"' << utt << '"';
        num_err ++;
        continue;
      }
      Vector<BaseFloat> vad_feat(vad_reader.Value(utt));
      int32 diff = abs(feat.NumRows() - vad_feat.Dim());
      if(diff != 0) {
        KALDI_WARN << "The difference between features (" << feat.NumRows()
                   << ") and vads (" << vad_feat.Dim() << ") is " << diff 
                   << " for utterance " << utt;
        if(vad_feat.Dim() < feat.NumRows()) {
          Vector<BaseFloat> vad_feat1;
          vad_feat1.Resize(feat.NumRows());
          vad_feat1.SetZero();
          SubVector<BaseFloat> subv(vad_feat1, 0, vad_feat.Dim());
          subv.CopyFromVec(vad_feat);
          vad_writer.Write(utt, vad_feat1);
        } else {
          SubVector<BaseFloat> subv(vad_feat, 0, feat.NumRows());
          Vector<BaseFloat> vad_feat1(subv);
          vad_writer.Write(utt, vad_feat1);
        }
      } else {
        vad_writer.Write(utt, vad_feat);
      }
    }
    KALDI_LOG << "Aligning vads to features done; "
              << "processed " << num_done << " utterances in total; "
              << num_err << " had errors.";
    return 0;
  } catch (const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
