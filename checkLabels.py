#################VAD version 5.1 with investigation ################
#The difference between this version and the previous 5.1 is that we 
#print the 
import numpy
import math
import soundfile as sf
import scipy.interpolate
import os
import random
import scipy.signal
import scipy.fftpack
import sys
import scipy.stats as ss
from copy import deepcopy
#import scipy.io.wavfile as wav

label = sys.argv[1]

prevStart = 0
prevEnd = 0
prevSeg = ""
with open(label) as infile: 
    for line in infile:
        toke_line = line.strip().split()
        if len(toke_line) != 2:
            print("Expect two column at line " + line.strip())
        if "," in line.strip():
            print("Check the , and . at line " + line.strip())
        start = int(round(float(toke_line[0]),2) * 100)
        end = int(round(float(toke_line[1]), 2) * 100)
        if end <= start:
            print("Error ! End is smaller or equals start for line " + line.strip())
        if start <= prevEnd:
            print("Error ! The current segment " + line.strip() + " is overlap with previous segment " + prevSeg)
        if end - start >= 1000:
            print("Warning ! The current segment " + line.strip() + " is too long")
        if start - prevEnd > 1000:
            print("Warning ! The gap between current segment " + line.strip() + " and previous segment " + prevSeg + " is too long")
        prevStart = start
        prevEnd = end    
        prevSeg = line.strip()     
