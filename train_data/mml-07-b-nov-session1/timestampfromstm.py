#!/usr/bin/python
# textgrid2csv.py
# D. Gibbon
# 2016-03-15
# 2016-03-15 (V02, includes filename in CSV records)

#-------------------------------------------------
# Import modules

import sys, re
import os

#-------------------------------------------------
# Text file input / output

def inputtextlines(filename):
    handle = open(filename,'r')
    linelist = handle.readlines()
    handle.close()
    return linelist

def outputtext(filename, text):
    handle = open(filename,'w')
    handle.write(text)
    handle.close()

#-------------------------------------------------
# Conversion routines

def converttextgrid2csv(textgridlines,textgridname,fileid):

    #csvtext = '# TextGrid to CSV (D. Gibbon, 2008-11-23)\n# Open the file with OpenOffice.org Calc or MS-Excel.\nFileName\tTierType\tTierName\tLabel\tStart\tEnd\tDuration\n'
    csvtext = ''
    newtier = False
    for line in textgridlines[9:]:
        line = re.sub('\n','',line)
        line = re.sub('^ *','',line)
        linepair = line.split(' = ')
        if len(linepair) == 2:
            if linepair[0] == 'class':
                classname = linepair[1]
            if linepair[0] == 'name':
                tiername = linepair[1]
            if linepair[0] == 'xmin':
                a = float(linepair[1])
		xmin=format(a,'.2f')
            if linepair[0] == 'xmax':
                b = float(linepair[1])
		xmax=format(b,'.2f')
            if linepair[0] == 'text':
                if linepair[1].strip().startswith('"') and linepair[1].strip().endswith('"'):
                    text = linepair[1].strip()[1:-1]
                else:
                    text = linepair[1][1:-2]
                diff = str(float(xmax)-float(xmin))
                print text.strip()
                if (text.strip() == 'Speech' or text.strip() == 'speech'):
                    csvtext += xmin.strip() + ' ' + xmax.strip() +'\n'
    return csvtext

#-------------------------------------------------
# Main caller

def main():    

    if len(sys.argv) < 2 or len(sys.argv) > 2:
        print("Usage: textgrid2csv.py <textgridfilename>")
        exit()
    textgridname = sys.argv[1]
    file_path=os.path.dirname(textgridname)
    fileid=os.path.basename(os.path.splitext(textgridname)[0])
    #print fileid
    #print file_path

    stmname = os.path.join( file_path, fileid + '.txt')
    #print stmname
    #sys.exit(0)
    textgrid = inputtextlines(textgridname)

    textcsv = converttextgrid2csv(textgrid,textgridname,fileid)

    outputtext(stmname,textcsv)

    print("Output file: " + stmname)

    return

#-------------------------------------------------

main()

#--- EOF -----------------------------------------
