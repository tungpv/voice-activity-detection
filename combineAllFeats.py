from __future__ import print_function
import kaldi_io_vesis84
import numpy
import os
import sys

datadir = sys.argv[1]
recId = sys.argv[2]
kaldiDataDir = sys.argv[3]
numChan = int(sys.argv[4])
numSeg = int(sys.argv[5])
outdir = sys.argv[6]
dict_dat = {}
for key,mat in  kaldi_io_vesis84.read_mat_scp(kaldiDataDir + "/feats.scp"):
    dict_dat[key] = mat


for i in range(1, numChan + 1):
    for j in range(1, numSeg + 1):
        dat2 = numpy.loadtxt(datadir + "/feats_DNN_chan" + str(i) + "_seg" + str(j) + ".txt")
        key = recId + "_chan" + str(i) + "_seg" + str(j)
        dat1 = dict_dat[key]
        if len(dat1) != len(dat2):
            print("Error! Mismatch len: " + str(len(dat1)) + " vs. " + str(len(dat2)))
            exit(1)
        dat = numpy.hstack((dat2, dat1))
        print("Size of all data " + str(i) + " = " + str(dat.shape))
        kaldi_io_vesis84.write_mat(outdir + "/allFeats_" + key + "_tmp.ark", dat, key)
        #numpy.savetxt(datadir + "/dat_chan" + str(i-1) + "_allFeats.txt", dat, fmt="%.6f")
    
