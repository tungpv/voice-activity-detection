import os
import sys
import time
import timeit

import numpy as np
from six.moves import xrange  # pylint: disable=redefined-builtin

def genSpkID (count):
    spkID = ""
    if count < 10:
        spkID = "00000" + str(count)
    elif count < 100:
        spkID = "0000" + str(count)
    elif count < 1000:
        spkID = "000" + str(count)
    elif count < 10000:
        spkID = "00" + str(count)
    elif count < 100000:
        spkID = "0" + str(count)
    else:
        spkID = str(count)
    return spkID

def readWavFols(wavDir, wav_scp_out, text_out):
      dirs = os.listdir( wavDir )
      fw = open(wav_scp_out,"w")
      fw2 = open(text_out,"w")
      count = 1
      for file in sorted(dirs):
         file_wtExt = os.path.splitext(file)[0]
         content = file_wtExt + " " + "sox \"" + wavDir + "/" + file + "\" -r 16000 -c 1 -b 16 -e signed-integer -t wavpcm - |"
         fw.write(content + "\n")
         count = count + 1
         fw2.write(file_wtExt + " text" + "\n");
      fw.close()
      fw2.close()

wavDir = sys.argv[1]
wav_scp_out = sys.argv[2] 
text_out = sys.argv[3]
readWavFols(wavDir, wav_scp_out, text_out)



