from __future__ import print_function
import kaldi_io_vesis84
import numpy
import os
import sys

file_ark = sys.argv[1]
outdir= sys.argv[2]
dict_dat = {}
for key,mat in  kaldi_io_vesis84.read_mat_ark(file_ark):
    numpy.savetxt(outdir + "/" + key + "_prob.txt", mat[:, 1], fmt="%.6f")
    currChanVAD = (mat[:, 1] >= 0.5).astype(int)
    numpy.savetxt(outdir + "/" + key + ".txt", currChanVAD, fmt="%d")

    
