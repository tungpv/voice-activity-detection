#################VAD version 5.1 with investigation ################
#The difference between this version and the previous 5.1 is that we 
#print the 
import numpy
import math
import soundfile as sf
import scipy.interpolate
import os
import random
import scipy.signal
import scipy.fftpack
import sys
import scipy.stats as ss
from copy import deepcopy
#import scipy.io.wavfile as wav

def getInsDelInfor(consideredFile, targetArr, insOrDel, startField):
    countOK = 0
    countTotalErr = 0
    countPartiallyErr = 0
    with open(consideredFile) as infile: 
        for line in infile:
            toke_line = line.strip().split()
            start = int(round(float(toke_line[startField]),2) * 100)
            end = int(round(float(toke_line[startField + 1]), 2) * 100)
            if start > len(targetArr):
                break
            l = end - start + 1
            matchedLabels = numpy.count_nonzero(targetArr[start - 1: end, 0])
            if matchedLabels == 0:
                print(insOrDel + " for segment [" + str(start) + "," + str(end) + "]")
                countTotalErr += 1
            elif l - matchedLabels >= 6:
                print("Partially " + insOrDel + " at segment [" + str(start) + "," + str(end) + "]" + " mismatch = " + str(l - matchedLabels)) 
                countPartiallyErr += 1
            else:
                print("Ok Segment [" + str(start) + "," + str(end) + "]")
                countOK += 1  
    print("OK segment = " + str(countOK))
    print("Partially error = " + str(countPartiallyErr))
    print("Totally " + insOrDel + " = " + str(countTotalErr))


def evaluateVADpred(refVAD, predictedVAD, finishAt):
    label = numpy.zeros((1000000, 1), numpy.int)
    label = label[:finishAt, :]
    extend = 30 ## Allow mismatch 0.3 second at begining and ending of speech segment
    with open(refVAD) as infile: 
        for line in infile:
            #print ("Line = " + line)
            toke_line = line.strip().split()
            start = int(round(float(toke_line[0]),2) * 100)
            end = int(round(float(toke_line[1]), 2) * 100)
            label[start - 1: end, 0] = 1
            for i in range(1,extend+1):
            	if start - i > 0:
            		if label[start-i, 0] == 0:
            			label[start -i, 0] = 2
            for i in range(1, extend + 1):
            	if end + i < finishAt:
            		if label[end + i, 0] == 0:
            			label[end + i, 0] = 2
    print("Shape of label = " + str(label.shape))
    
    pred = numpy.zeros((1000000, 1), numpy.int)
    pred = pred[:finishAt, :]
    with open(predictedVAD) as infile: 
        for line in infile:
            #print ("Line = " + line)
            toke_line = line.strip().split()
            start = int(round(float(toke_line[1]),2) * 100)
            end = int(round(float(toke_line[2]), 2) * 100)
            pred[start - 1: end, 0] = 1
    print("Shape of pred = " + str(pred.shape))
    count_true_pos = 0 # TP, or hit. Correctly predict speech frame as speech
    count_false_pos = 0 # FP, or false alarm. Wrongly predict silence to speech
    count_false_neg = 0 # miss. Wrongly predict speech as silence
    count_true_neg = 0 # correctly rejection, i.e. correctly predict silence frame as silence
    for i in range(finishAt):
        if (label[i, 0] == 0 or label[i, 0] == 2) and pred[i, 0] == 0:
            count_true_neg += 1
        elif (label[i, 0] == 1 or label[i, 0] == 2) and pred[i, 0] == 1:
            count_true_pos += 1
        elif label[i, 0] == 1 and pred[i, 0] == 0:
            count_false_neg += 1
        elif label[i, 0] == 0 and pred[i, 0] == 1:
            count_false_pos += 1
    speechFrame = numpy.count_nonzero(label==1)
    #### Now get the statistical information #######
    getInsDelInfor(predictedVAD, label, "insertion", 1)
    getInsDelInfor(refVAD, pred, "deletion", 0)
    totalSamp = (count_true_pos + count_false_pos + count_false_neg + count_true_neg) #Another way to estimate number of frame in the test audio ?
    speechFrame2 = count_true_pos + count_false_neg ### Another way to estimate the number of speech frame
    accuracy = (count_true_neg + count_true_pos) * 100.0/totalSamp
    fa_rate = count_false_pos * 100.0/(count_false_pos + count_true_pos)
    miss_rate = count_false_neg * 100.0/speechFrame
    precision = count_true_pos * 100.0/(count_false_pos + count_true_pos)
    recall = 1 - miss_rate
    print("-----------STATISTICAL INFORMATION ---------")
    print("No. frame considered = " + str(finishAt) + ", speechFrame = " + str(speechFrame) + ", or speechFrame2 = " + str(speechFrame2))
    print("True positive (corr predict speech) = " + str(count_true_pos) + ", true neg (corr predict silence) = " + str(count_true_neg) + ", false negative (miss) = " + str(count_false_neg) + ", false positive (false alarm) = " + str(count_false_pos))
    print("False alarm (insertion) = " + str(fa_rate)) 
    print("Miss rate (delete) = "+ str(miss_rate) ) 
    print("Accuracy = " + str(accuracy))
    print("Precison = " + str(precision))
    print("Recall = " + str(recall))         

refVAD = sys.argv[1]
predVAD = sys.argv[2]
finishAt = int(sys.argv[3])
evaluateVADpred(refVAD, predVAD, finishAt)
