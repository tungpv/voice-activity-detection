#################VAD version 5.1 with investigation ################
#The difference between this version and the previous 5.1 is that we 
#print the 
import numpy
import math
import soundfile as sf
import scipy.interpolate
import os
import random
import scipy.signal
import scipy.fftpack
import sys
import scipy.stats as ss
from copy import deepcopy
#import scipy.io.wavfile as wav

CUR_DIR = os.path.dirname(os.path.realpath(__file__))


closedTalkChans_str = sys.argv[1]
out_wav_dir= sys.argv[2]
if not os.path.exists(out_wav_dir):
    os.makedirs(out_wav_dir)

data_dir = sys.argv[3]
recId = sys.argv[4]
if not os.path.exists(data_dir):
    os.makedirs(data_dir)

############# Main #####################

def segm(audio, fs, channName, outdir):
    nframeEachSeg = 20000
    numSample = len(audio)
    flength = int(fs * 0.025);
    hsize = int(fs * 0.01);
    numSampleEachSeg = (nframeEachSeg - 1) * hsize + flength
  
    sampleStartFrame = 0
    sampleEndFrame = min(numSample, sampleStartFrame + numSampleEachSeg)
  
    segCount = 1
    while sampleStartFrame < numSample:    
        curr_seg_audio = audio[sampleStartFrame:sampleEndFrame]
        sf.write(channName + "_seg" + str(segCount) + ".wav", curr_seg_audio, fs)
        segCount = segCount + 1        
        sampleStartFrame = sampleStartFrame + numSampleEachSeg
        sampleEndFrame = min(numSample, sampleEndFrame + numSampleEachSeg)
    print("Finish segmentation!")
    with open(outdir + "/numSeg", 'w') as outfile:
        outfile.write(str(segCount - 1))
        

closeTalkChans = closedTalkChans_str.split(';')
with open(data_dir + "/numChan", 'w') as outfile2:
    outfile2.write(str(len(closeTalkChans)))
for i in range(len(closeTalkChans)):
	audio, fs = sf.read(closeTalkChans[i])
	segm(audio, fs, out_wav_dir + "/" + recId + "_chan" + str(i+1), data_dir)
