#################VAD version 5.1 with investigation ################
#The difference between this version and the previous 5.1 is that we 
#print the 
import numpy
import math
import soundfile as sf
import scipy.interpolate
import os
import random
import scipy.signal
import scipy.fftpack
import sys
import scipy.stats as ss
from copy import deepcopy
#import scipy.io.wavfile as wav


label_dir= sys.argv[1]
utt_to_length = sys.argv[2]

utt2Length = {}
with open(utt_to_length) as infile:
    for line in infile:
        toke_line = line.strip().split()
        num_frame = int(toke_line[1])
        expectedLabel = label_dir + "/" + toke_line[0] + ".txt"
        label = numpy.zeros((num_frame, 1), numpy.int)
        with open(expectedLabel) as infile: 
            for line in infile:
                #print ("Line = " + line)
                tl = line.strip().split()
                start = int(round(float(tl[0]),2) * 100)
                end = int(round(float(tl[1]), 2) * 100)
                label[start - 1: end, 0] = 1
        print(toke_line[0] + " " + ' '.join(str(x[0]) for x in label) + "\n")
