#################VAD version 5.1 with investigation ################
#The difference between this version and the previous 5.1 is that we 
#print the 
import numpy
import math
import soundfile as sf
import scipy.interpolate
import os
import random
import scipy.signal
import scipy.fftpack
import sys
import scipy.stats as ss
from copy import deepcopy
#import scipy.io.wavfile as wav

CUR_DIR = os.path.dirname(os.path.realpath(__file__))

recId = sys.argv[1]
labelsFiles = sys.argv[2]
lengthFiles = sys.argv[3]

vadLabel = labelsFiles.split(';')

lengthEachTrunk = 0
totalLength = 0

with open(lengthFiles) as infile: 
    for line in infile:
        toke_line = line.strip().split()
        if recId + "_chan1_" in toke_line[0]:
            if toke_line[0] == recId + "_chan1_seg1":
                lengthEachTrunk = int(toke_line[1])
            totalLength = totalLength + int(toke_line[1])

numSegs = int(totalLength/lengthEachTrunk) + 1
#print("Total length = " + str(totalLength) + ", length each trunk = " + str(lengthEachTrunk) + ", numSeg = " + str(numSegs))
for i in range(1, len(vadLabel) + 1):
    label = numpy.zeros(totalLength, numpy.int)
    with open(vadLabel[i-1]) as infile: 
        for line in infile:
            #print ("Line = " + line)
            toke_line = line.strip().split()
            start = int(round(float(toke_line[0]),2) * 100)
            end = int(round(float(toke_line[1]), 2) * 100)
            label[start - 1: end] = 1
    for j in range(1, numSegs + 1):
        start_index = lengthEachTrunk * (j - 1)
        end_index =  start_index + lengthEachTrunk
        #print("Start index = " + str(start_index) + ", end = " + str(end_index))
        if end_index > totalLength: #For the last segment
            end_index = totalLength
        currLabel = label[start_index:end_index]
        str_label = [str(la) for la in currLabel]
        print(recId + "_chan" + str(i) + "_seg" + str(j) + " " + ' '.join(str_label))
