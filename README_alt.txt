INSTRUCTION TO RUN OUR DNN-BASED VAD

REQUIREMENT
Hardware:
- CPU 4 cores
- 8GB Ram+. Suggest to have 16GB
- GPU ?

Software:
- Ubuntu 14.04 or newer version
- Python 2.7 with numpy, scipy, soundfile package
- Kaldi

RUNNING PROCEDURE
SETTING
Before running any script, please set the kaldi path in path.sh

This package is VAD using DNN. Therefore, theoritically it consists of training and testing as follows:
Training: Please run vad_DNN_v4_train.sh. It contains:
Step 1: Extract the features (fbank_pitch and Tung's features) for various recording. Also prepare VAD label (i.e. convert from segment-based to frame-based) for various recording
Step 2: Combine data (feature, label,...) from all recordings into one data
Step 3: Split data into train_dnn and validate_dnn to train the DNN
Step 4: Train the DNN


Testing: vad_DNN_v4_test2.sh. In this file you need to define input audio channels, output audio channels, output segment files
Step 5: Using the trained DNN in step 2 to generate the predicted VAD for the test data
Step 6: Modify the audio, i.e. delete cross talk using the predicted VAD. Also convert the frame-base VAD to segment-base VAD, so that it can replace LIUM segment.
Step 7: (Optional): If we have the ground truth label of the test data we might also want to evaluate the accuracy of our VAD

Some notes:
	- The training process normally is ran only one time. After that you will have the DNN model for testing with any incoming test set
	- If you don't need to measure how good (quantity) the DNN results, please skip step 7
	- You run two times with the same data might results in slightly different results

