#!/bin/bash
. path.sh
wavDir=$1
testDir=$2

python2.7 genWav_andText_test.py $wavDir $testDir/wav.scp $testDir/text
wav-to-duration scp:$testDir/wav.scp ark,t:$testDir/dur.txt > $testDir/log_wav2seg.txt
python2.7 local/gen_Segment_utt2spk.py $testDir/dur.txt $testDir/segments $testDir/utt2spk
utils/utt2spk_to_spk2utt.pl $testDir/utt2spk > $testDir/spk2utt

if [ 0 -eq 0 ]
then
	steps/make_fbank_pitch.sh --cmd "run.pl" --nj 1 --fbank-config conf/fbank16k40.conf --pitch-config conf/pitch16k.conf $testDir || exit 1;
   #steps/compute_cmvn_stats.sh $testDir || exit 1;
fi



