from __future__ import print_function
import kaldi_io
import numpy
import os
import sys

file_str = sys.argv[1]
column_str = sys.argv[2]
ark_out = sys.argv[3]
key="unknown"
if len(sys.argv) > 4:
	key = sys.argv[4]

columns = column_str.split(",") #e.g. 2-9,10,11-15
feats = []
for rd in columns:
    if rd.find("-") == -1:
        feats.append(int(rd)-1)
    else:
        rangeDat = rd.split("-")
        for j in range(int(rangeDat[0])-1, int(rangeDat[1])):
            feats.append(j)
print("Selected features = " + str(feats)) 
mat = numpy.loadtxt(file_str)
mat = mat[:, feats]
with open(ark_out,'wb') as f:
	kaldi_io.write_mat(f, mat, key=key)
	
