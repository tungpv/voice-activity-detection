from __future__ import print_function

# Import MNIST data

import tensorflow as tf
import sys
import numpy
from sklearn import preprocessing
from sklearn.externals import joblib

def getData(dat_file):
    dat = numpy.loadtxt(dat_file, dtype=numpy.float)
    #print("Number of columns = " + str(dat.shape[1]))
    numFeats = dat.shape[1] - 1
    dat_feats = dat[:, 1:]
    dat_label = dat[:, 0]
    print ("First 5 data items:")
    for i in range(5):
        print("Feats = " + str(dat_feats[i]) + ", label = " + str(dat_label[i]) + "\n")
    return dat_feats, dat_label, numFeats


hiddenSize = 200
testFile = sys.argv[1]
modelDir = sys.argv[2]
scalerModel = sys.argv[3]
outFile = sys.argv[4]
if len(sys.argv) > 5:
    hiddenSize = int(sys.argv[5])

test_feats, test_label, numFeats = getData(testFile)
    
scaler = joblib.load(scalerModel)   
test_feats_scaled = scaler.transform(test_feats)


feature_columns = [tf.contrib.layers.real_valued_column("", dimension=numFeats)]
classifier = tf.contrib.learn.DNNClassifier(feature_columns=feature_columns,
                                          hidden_units=[hiddenSize],
                                          optimizer=tf.train.ProximalAdagradOptimizer(
                                          	learning_rate=0.01,
                                          	l2_regularization_strength=0.00003
                                          ),
                                          n_classes=2,
                                          model_dir=modelDir)

# Train model.
y = list(classifier.predict(x=test_feats_scaled, as_iterable=True))
numpy.savetxt(outFile, y, fmt = "%d")

