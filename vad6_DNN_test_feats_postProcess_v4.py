#################V2.1 ################
#The difference between this version and the previous V2 is at line 696. PostProcessVAD(currChanVAD, 20) instead of MedianFilter(currChanVAD, 25). Also we use results from DNN with sliced features
#print the 
import numpy
import math
import soundfile as sf
import scipy.interpolate
import os
import sys
import random
import scipy.signal
import scipy.fftpack
import scipy.stats as ss
from copy import deepcopy

#import scipy.io.wavfile as wav

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
orgAudioFiles = sys.argv[1]
in_out_dir= sys.argv[2]
post_pro = int(sys.argv[3])
outAudioFiles = sys.argv[4]
featSet = sys.argv[5]
segmentsFilesBuff = sys.argv[6]
segmentsFilesOrg = sys.argv[7]
recId = sys.argv[8]
post_pro_str = "no_postproc"
if post_pro == 1:
    post_pro_str = "PostProVAD20"
elif post_pro == 2:
    post_pro_str = "MedianFilter25"






class MyClass:
    def __init__(self):
        self.vadFlag = 0
        self.countVADON = 0
        self.frameIdx = 0
        self.nf = 0
        self.slow_nf = 0
        self.speechVar = 0
        self.th = 0
        self.th2 = 0
        self.val = 0
        self.MinRatioSpeechPeakNoiseFloor = 0
        self.ThOffset = 0
        self.MinFeatureVal = 0

class Seg:
    def __init__(self):
        self.start = []
        self.stop = []
        self.label = []

def calPowerSum_segment(audio, fs):
    #logger(Const.CROSSTALK_LOGGER_NAME).info("calPowerSum_segment: ")
    print("calPowerSum_segment: ")
    flength = int(fs * 0.025);
    hsize = int(fs * 0.01);
    ##dither the speech signal
    #logger(Const.CROSSTALK_LOGGER_NAME).info("##dither the speech signal")
    for i in range(len(audio)):
        audio[i] = audio[i] + numpy.random.randn()/(pow(2,32))
        
    ## DC removal ##
    #logger(Const.CROSSTALK_LOGGER_NAME).info("## DC removal ##")
    #print("## DC removal ##")
    num = numpy.array([0.999,-0.999])
    denum = numpy.array([1,-0.999])
    audio = scipy.signal.lfilter(num, denum, audio)
    
    ## Pre-emphasis ###
    #logger(Const.CROSSTALK_LOGGER_NAME).info("## Pre-emphasis ###")
    #print("## Pre-emphasis ###")
    tmp_audio = numpy.zeros(len(audio))
    for i in range(1, len(audio)):
        tmp_audio[i] = audio[i] - 0.97 * audio[i-1]
    tmp_audio[0] = audio[0]
    audio = tmp_audio
    
    ## Perform FFT ####
    #logger(Const.CROSSTALK_LOGGER_NAME).info("## Perform FFT ####")
    #print("## Perform FFT ####")
    num_frame = int(math.floor((len(audio)-flength)/hsize+1))
    spec = numpy.zeros((257, num_frame), dtype=numpy.complex)
    for i in range(num_frame):
        frame_dat = audio[i * hsize: i * hsize + flength]
        frame_dat = frame_dat * numpy.hamming(flength)
        result = scipy.fftpack.fft(frame_dat, 512)
        result = result[0:result.size // 2 + 1]
        spec[:, i] = result
    #spec = spectrogram(audio, framelength=flength, hopsize=hsize, window=numpy.hamming, transform=scipy.fftpack.fft)
    #tmp = numpy.conjugate(spec) * spec
    tmp = numpy.multiply(spec, numpy.conjugate(spec))
    power = tmp.real
    #powerSum = [sum([row[i] for row in power]) for i in range(0,len(power[0]))]
    powerSum = power.sum(axis=0)
    #logger(Const.CROSSTALK_LOGGER_NAME).info("calPowerSum_segment: Final result=" + str(powerSum))
    #print("calPowerSum_segment: Final result=" + str(powerSum))
    return powerSum

def calPowerSum(audio):
    #logger(Const.CROSSTALK_LOGGER_NAME).info("calPowerSum: " + audio)
    print("calPowerSum: " + audio)
    audio, fs = sf.read(audio)
    flength = int(fs * 0.025);
    hsize = int(fs * 0.01);
    
    ##dither the speech signal
    #logger(Const.CROSSTALK_LOGGER_NAME).info("##dither the speech signal")
    print("##dither the speech signal")
    for i in range(len(audio)):
        audio[i] = audio[i] + numpy.random.randn()/(pow(2,32))
        
    ## DC removal ##
    #logger(Const.CROSSTALK_LOGGER_NAME).info("## DC removal ##")
    print("## DC removal ##")
    #coff = 0.999
    num = numpy.array([0.999,-0.999])
    denum = numpy.array([1,-0.999])
    audio = scipy.signal.lfilter(num, denum, audio)
    
    ## Pre-emphasis ###
    #logger(Const.CROSSTALK_LOGGER_NAME).info("## Pre-emphasis ###")
    print("## Pre-emphasis ###")
    tmp_audio = numpy.zeros(len(audio))
    for i in range(1, len(audio)):
        tmp_audio[i] = audio[i] - 0.97 * audio[i-1]
    tmp_audio[0] = audio[0]
    audio = tmp_audio
    
    ## Perform FFT ####
    #logger(Const.CROSSTALK_LOGGER_NAME).info("## Perform FFT ####")
    print("## Perform FFT ####")
    num_frame = int(math.floor((len(audio)-flength)/hsize+1))
    spec = numpy.zeros((257, num_frame), dtype=numpy.complex)
    for i in range(num_frame):
        frame_dat = audio[i * hsize: i * hsize + flength]
        frame_dat = frame_dat * numpy.hamming(flength)
        result = scipy.fftpack.fft(frame_dat, 512)
        result = result[0:result.size // 2 + 1]
        spec[:, i] = result
    #spec = spectrogram(audio, framelength=flength, hopsize=hsize, window=numpy.hamming, transform=scipy.fftpack.fft)
    #tmp = numpy.conjugate(spec) * spec
    tmp = numpy.multiply(spec, numpy.conjugate(spec))
    power = tmp.real
    #powerSum = [sum([row[i] for row in power]) for i in range(0,len(power[0]))]
    powerSum = power.sum(axis=0)
    #logger(Const.CROSSTALK_LOGGER_NAME).info("calPowerSum: Final result=" + powerSum)
    print("calPowerSum: Final result=" + powerSum)
    return powerSum

def norArr(arr1, sumArr):
    for i in range(len(arr1)):
        arr1[i] = arr1[i]/sumArr[i]
    return arr1

def batch_peridoc_pitch_count_fast(testWave, Fs, numFrame, nSamplePerFrame, nSampleForward):
    in_args = [testWave, Fs, numFrame, nSamplePerFrame, nSampleForward]
    #logger(Const.CROSSTALK_LOGGER_NAME).warn("batch_peridoc_pitch_count_fast: " + str(in_args))
    
    x_start   = 0;
    x_end     = nSamplePerFrame;
    act_frame_x = numpy.zeros((nSamplePerFrame*2,numFrame)) # A matrix 
    for j in range(numFrame):
        #act_frame_x(:,j) =  [testWave(x_start:x_end); zeros(x_end-x_start+1,1)];
        tmp_arr = numpy.concatenate([testWave[x_start:x_end],numpy.zeros(x_end-x_start)])
        act_frame_x[:,j] = tmp_arr
        
        x_start = x_start + nSampleForward;
        x_end   = x_end   + nSampleForward;
    
    frSize, nFr = act_frame_x.shape
    binWidth = Fs/frSize;
    hammWinFrame  = numpy.hamming(frSize);
    HighPassFreqUpper = 800;
    numHighPassFreqUpperBin = int(math.ceil(HighPassFreqUpper/binWidth))
    mapX= list(range(1,numHighPassFreqUpperBin + 1)) #[1:numHighPassFreqUpperBin];
    mapX = 1.0/numHighPassFreqUpperBin * numpy.array(mapX); #mapX = 1/numHighPassFreqUpperBin.*mapX;
    #mapX = mapX';
    #numpy.savetxt('mapX.txt', mapX, fmt = '%.5f') 
    frame_x = numpy.zeros((nSamplePerFrame*2,numFrame))
    for i in range(nFr):
        tmp_norm = numpy.linalg.norm(act_frame_x[:,i])
        if tmp_norm == 0:
            #logger(Const.CROSSTALK_LOGGER_NAME).warn("Warning (TUNG)! norm equals 0 at " + str(i))
            for j in range(frame_x.shape[0]):
                frame_x[j,i] = random.random() * 0.00001
            continue
        frame_x[:,i] = act_frame_x[:,i]/numpy.linalg.norm(act_frame_x[:,i]);
        # so that signal level is NOT at play here!!!
    
    ###frame_x_hamm  = bsxfun(@times, frame_x, hammWinFrame);
    frame_x_hamm = numpy.zeros((nSamplePerFrame*2,numFrame))
    for i in range(nFr):
        frame_x_hamm[:,i] = numpy.multiply(frame_x[:,i], hammWinFrame)
        
    ###abs_fft_x = abs(fft(frame_x_hamm)); % FFT on each column. Hence fft(frame_x_hamm) return a vector with size = num_of_column   
    abs_fft_x = numpy.absolute(numpy.fft.fft(frame_x_hamm, axis = 0)) ##PLEASE CHECK
    #numpy.savetxt('abs_fft_x_JustAfterAbsFFT.txt', abs_fft_x, fmt = '%.5f')
    ###abs_fft_x(1:numHighPassFreqUpperBin,:) = bsxfun(@times, abs_fft_x(1:numHighPassFreqUpperBin,:), mapX);
    for i in range(nFr):
        abs_fft_x[:numHighPassFreqUpperBin,i] = numpy.multiply(abs_fft_x[:numHighPassFreqUpperBin,i], mapX)
    #numpy.savetxt('abs_fft_x_JustAfterMulWithMapX.txt', abs_fft_x, fmt = '%.6f')
    
    ###abs_fft_x = bsxfun(@times, abs_fft_x, 1./max(abs_fft_x(1:2*HighPassFreqUpper/binWidth,:)));
    max_each_col = numpy.reciprocal(numpy.amax(abs_fft_x[:2*HighPassFreqUpper/binWidth,:], axis = 0)) #Equivalent to 1./max(abs_fft_x(1:2*HighPassFreqUpper/binWidth,:))
    for i in range(frSize):
        abs_fft_x[i, :] = numpy.multiply(abs_fft_x[i, :], max_each_col)
    #numpy.savetxt('abs_fft_x_ScaleWithReciporalMax.txt', abs_fft_x, fmt = '%.6f')    
    
    ## The above operation with mapX reduces low frequency power, basically
    ## frequency filtering!!!
    
    ## extract noise level from 2K-3K freq range
    abs_fft_x = numpy.power(abs_fft_x, 1.2); #abs_fft_x = abs_fft_x.^1.2;
    #numpy.savetxt('abs_fft_x_afterPower.txt', abs_fft_x, fmt = '%.6f')
    second_dim = abs_fft_x.shape[1]
    #print("Size of abs_fft_x = " + str(second_dim))
    L = math.floor(frSize/2);
    binWidth = (Fs/(L*2));
    PitchRangeLower = 80;
    PitchRangeUpper = 250;
    
    PitchIdxRange = int(math.ceil(PitchRangeUpper - PitchRangeLower)/binWidth)
    val_histPeak = numpy.zeros((second_dim, PitchIdxRange));
    val_histTrough = numpy.zeros((second_dim, PitchIdxRange));
    SumPeakVal = numpy.zeros((second_dim, PitchIdxRange, 6))
    SumTroughVal = numpy.zeros((second_dim, PitchIdxRange, 6))
    #print("Size of SumPeakVal = " + str(SumPeakVal.shape))
    beamWidth =2;
    
    for i in range(PitchIdxRange):
        pitchVal  = math.floor( ((PitchRangeLower/binWidth) +i)*binWidth);
    
        # number of harmonics, skipping the first harmonics bcos very noisy!!!
        for j in range(1,7):
            idx          = int(math.floor(((j +1 )*pitchVal)/binWidth)) ### PLEASE CHECK
            idx_trough   = int(math.floor(idx + (pitchVal/(2*binWidth))))
        
            #Pi  = sum(abs_fft_x(idx-beamWidth:idx+beamWidth,:));
            tmpMat_for_Pi = abs_fft_x[idx-beamWidth-1:idx+beamWidth,:]
            Pi  = tmpMat_for_Pi.sum(axis = 0);
            
            #Ti =  sum(abs_fft_x(idx_trough-beamWidth:idx_trough+beamWidth,:));
            tmpMat_for_Ti = abs_fft_x[idx_trough-beamWidth-1:idx_trough+beamWidth,:]
            Ti =  tmpMat_for_Ti.sum(axis = 0);
            #print("Size of PI" + str(Pi.shape) + " size Ti = " + str(Ti.shape))                                   
            SumPeakVal[:,i,j-1]   =  Pi;
            SumTroughVal[:,i,j-1] = Ti;
    
    
    SP = SumPeakVal.sum(axis = 2);
    ST = SumTroughVal.sum(axis = 2);
    val_histTrough  = numpy.power(ST, 2) #ST.^2;
    #numpy.savetxt('val_histTrough.txt', val_histTrough, fmt = '%.6f')
    
    for i in range(PitchIdxRange):
        #currSumPeakVal = squeeze(SumPeakVal(:,i,:))';
        SumPeakVal_ith = SumPeakVal[:,i,:]
        SumPeakVal_ith_squeezed = SumPeakVal_ith.squeeze();
        ##currSumPeakVal = SumPeakVal_ith_squeezed.conj().transpose()
        currSumPeakVal = SumPeakVal_ith_squeezed.transpose()
        #if i == 0:
        #    numpy.savetxt('currSumPeakVal_i0.txt', currSumPeakVal, fmt = '%.6f')
        
        #currSumTroughVal = squeeze(SumTroughVal(:,i,:))';
        SumTroughVal_ith = SumTroughVal[:,i,:]
        SumTroughVal_ith_squeezed = SumTroughVal_ith.squeeze();
        ##currSumTroughVal = SumTroughVal_ith_squeezed.conj().transpose()
        currSumTroughVal = SumTroughVal_ith_squeezed.conj().transpose()
        #if i == 0:
        #    numpy.savetxt('currSumTroughVal_i0.txt', currSumTroughVal, fmt = '%.6f')
        #val_histPeak(:,i)   = SP(:,i).^2 - var(currSumPeakVal)' - var(currSumTroughVal)';
        var_currSumPeakVal = numpy.var(currSumPeakVal, axis = 0, ddof = 1)
        var_currSumTroughVal = numpy.var(currSumTroughVal, axis = 0, ddof = 1)
        #if i == 0:
        #    numpy.savetxt('var_currSumPeakVal_i0.txt', var_currSumPeakVal, fmt = '%.6f')
        #    numpy.savetxt('var_currSumTroughVal_i0.txt', var_currSumTroughVal, fmt = '%.6f')
        #val_histPeak[:,i]   = numpy.power(SP[:,i], 2) - var_currSumPeakVal.transpose() - var_currSumTroughVal.transpose();
        tmp1 = numpy.power(SP[:,i], 2)
        tmp2 = var_currSumPeakVal.transpose()
        tmp3 = var_currSumTroughVal.transpose()
        tmp4 = tmp1 - tmp2 - tmp3;
        val_histPeak[:,i] = tmp4
        #if i == 0:
        #    numpy.savetxt('tmp1_i0.txt', tmp1, fmt = '%.6f')
        #    numpy.savetxt('tmp2_i0.txt', tmp2, fmt = '%.6f')
        #    numpy.savetxt('tmp3_i0.txt', tmp3, fmt = '%.6f')
        #    numpy.savetxt('tmp4_i0.txt', tmp4, fmt = '%.6f')
    
    #numpy.savetxt('val_histPeak.txt', val_histPeak, fmt = '%.6f')
    #[V1 iV1] = max(val_histPeak');
    val_histPeak_conjTransp = val_histPeak.conj().transpose()
    V1 = val_histPeak_conjTransp.max(0);
    iV1 = val_histPeak_conjTransp.argmax(0)
    V2 = numpy.zeros(nFr)
    for i in range(nFr):
        V2[i] = abs(val_histTrough[i,iV1[i]]);
    #numpy.savetxt('V1.txt', V1, fmt = '%.6f') 
    #numpy.savetxt('V2.txt', V2, fmt = '%.6f') 
    valFeature      = numpy.divide(V1, V2);
    return valFeature


def energyVADVal(frame_x, Fs):
    return numpy.linalg.norm(frame_x)
    

def adaptThreshold(s_old, val):
    if s_old.frameIdx <= 1 :
        s_old.val = val

    #diffVal      = abs(val-s_old.val)
    s_new = MyClass()
    s_new     = s_old

    s_new.frameIdx = s_new.frameIdx+1


    if s_new.frameIdx < 3:
        s_new.vadFlag  = 0;
        s_new.countVADON = 0;
        if s_new.frameIdx == 1:
            s_new.nf = s_new.val;
            s_new.slow_nf = s_new.val;
        s_new.nf          = max(s_new.nf, s_new.val);
        s_new.slow_nf     = max(s_new.slow_nf, s_new.val);
        s_new.speechVar  =  s_new.MinRatioSpeechPeakNoiseFloor*s_new.nf + s_new.ThOffset;
        s_new.th           =  s_new.slow_nf     + s_new.ThOffset;
        s_new.th2          =  s_new.slow_nf*1.3 + s_new.ThOffset;


    s_new.val = val;

    if (s_new.frameIdx >= 3):

        s_new.speechVar = 0.8*s_new.speechVar + 0.2*s_new.val;
        if (s_new.speechVar < s_new.th2):
            s_new.speechVar = s_new.th2;
      
        if (s_new.val > s_new.th2):
            s_new.vadFlag = 1;
            if (s_new.val > s_new.speechVar):
                s_new.speechVar = s_new.val;
        else:
            s_new.vadFlag = 0;

        s_new.nf = 0.99*s_new.nf + 0.01*s_new.val;
        s_new.slow_nf = 0.999*s_new.slow_nf + 0.001*s_new.val;
        if (s_new.val < s_new.slow_nf):
            s_new.slow_nf = s_new.val; 
            if (s_new.slow_nf < s_new.MinFeatureVal):
                s_new.slow_nf  = s_new.MinFeatureVal;
  
        if (s_new.val < s_new.nf):
            s_new.nf = s_new.val;
            if (s_new.nf < s_new.MinFeatureVal):
                s_new.nf = s_new.MinFeatureVal;

  
        s_new.th         =   s_new.nf     + s_new.ThOffset;
        s_new.th2        =   s_new.nf*1.3 + s_new.ThOffset;

    return s_new


def CombVAD(wav, fs, DEBUG):
    max_nSampleTotal = len(wav);
    FrameSzTime     = 0.1 # ?? msec frame Window
    # the current wisdom is to use bin Bandwidth of 10 Hz for FFT resolution
    FrameForward    = 0.02 #  % ?? No overlap in this example

    nSamplePerFrame = int(FrameSzTime * fs)
    nSampleForward  = int(FrameForward * fs)

    numFrame  = int(math.floor((max_nSampleTotal - nSamplePerFrame)*1.0/nSampleForward))
    s_new = MyClass()
    s_new.frameIdx = 0;
    s_new.MinFeatureVal = 15;
    s_new.MinRatioSpeechPeakNoiseFloor = 2.5;
    s_new.ThOffset  = 10;

    periodicVADVal = batch_peridoc_pitch_count_fast(wav, fs, numFrame, nSamplePerFrame, nSampleForward);
    
    x_start   = 0;
    x_end     = nSamplePerFrame;

    energyVAD_flag = numpy.zeros(numFrame)
    vad_flag = numpy.zeros(numFrame)
    for j in range(numFrame):
        frame_x  =  wav[x_start:x_end];
        energyVAD_flag[j]    = energyVADVal(frame_x, fs);
    
        s_new = adaptThreshold(s_new,periodicVADVal[j]);
    
        #Test.th(j)       = s_new.th;
        #Test.th2(j)      = s_new.th2;
        #Test.speecVar(j) = s_new.speechVar;
        #Test.nf(j)       = s_new.nf;
        #Test.slow_nf(j)  = s_new.slow_nf;
        vad_flag[j]  = s_new.vadFlag;
    
        x_start = x_start + nSampleForward;
        x_end   = x_end   + nSampleForward;

    return vad_flag, energyVAD_flag, periodicVADVal

def MedianFilter(x,filter_len):
    nf = len(x);
    hs = (filter_len-1)//2
    y=deepcopy(x);
    for i in range(hs, nf-hs):
        y[i] = numpy.median(x[i-hs:i+hs+1]);
    return y

def printChannelsInfor(x):
    start = 0
    isSpeech = 0
    for i in range(len(x)):
        if x[i] > 0:
            if isSpeech == 0:
                isSpeech = 1
                start = i
        else:
            if isSpeech == 1:
                #logger(Const.CROSSTALK_LOGGER_NAME).info("Start = " + str(start) + ", stop = " + str(i))
                print("Start = " + str(start) + ", stop = " + str(i))
                print("Start = " + str(start) + ", stop = " + str(i))
                isSpeech = 0


def PostProcessVAD(vad, buffer_len, filter_len = 40):
    vad = vad[:];
    
    # First filter the vad flat using a median filter
    vad_smoothed = MedianFilter(vad, 3);
    # Then grow any valid speech frame by 20 frames, as we think
    # anything immediately before and after the voiced frames are
    # speech.
    
    #if nargin<3
    #    filter_len = 40;
    #end
    bool_arr = numpy.greater(numpy.convolve(numpy.ones(filter_len), vad_smoothed), 0.5)
    vad_extended = bool_arr.astype(numpy.double)
    vad_extended = vad_extended[filter_len//2:]#vad_extended(1:filter_len/2) = [];
    vad_extended = vad_extended[:len(vad_extended)-filter_len//2+1]#vad_extended(end-filter_len/2+2:end) = [];
    
    # We don't want to have too many segments. Sometimes, we can merge
    # several segments into one.
    filter_len = buffer_len-40;
    if filter_len>0:
        bool_arr2 = numpy.greater(numpy.convolve(numpy.ones(filter_len), vad_extended), 0.5)
        vad_merged = bool_arr2.astype(numpy.double)
        vad_merged = vad_merged[filter_len/2:]#vad_merged(1:filter_len/2) = [];
        vad_merged = vad_merged[:len(vad_merged)-filter_len/2+1]#vad_merged(end-filter_len/2+2:end) = [];
    else:
        vad_merged = vad_extended;
    
    return vad_merged


def label2seg(label):
    # If the state is changed
    diff = label[1:] - label[0:len(label)-1];
    idx_tuple = numpy.nonzero(diff);
    idx = idx_tuple[0]
    N_seg = len(idx);
    #print ("Number of segment = " + str(N_seg))
    seg = Seg() 
    if N_seg ==0:
        seg.start.append(0);
        seg.stop.append(len(label)-1);
        seg.label.append(label[0])
        return seg  
    for i in range(N_seg):
        if i==0 :
            seg.start.append(0);
        else:
            seg.start.append(idx[i-1]+1)
        seg.stop.append(idx[i])
        seg.label.append(label[idx[i]])
    seg.start.append(idx[-1]+1)
    seg.stop.append(len(label)-1);
    seg.label.append(label[-1])
    
    return seg

def getStat(arr):
    if len(arr) == 0:
        return 0,0,0
    maxVal = numpy.amax(arr)
    avgVal = numpy.average(arr)
    median = numpy.median(arr)
    return maxVal, avgVal, median
    
def seg2label(seg, numFrames):
    label = numpy.zeros(numFrames, dtype=numpy.int)
    N_seg = len(seg.label);
    #print("Now we have " + str(N_seg))
    for i in range(N_seg):
        label[seg.start[i] : seg.stop[i] +1 ] = seg.label[i];
        #print("Set from " + str(seg.start[i]) + ", " + str(seg.stop[i]) + ", " + str(seg.label[i]))
    return label

def writeSeg(seg, posfix, maxLength, buffExt, discardShortSegLength, tollerance, segmentBuffFile, segmentOrgFile):
    #discardShortSegLength=0.07
    #tollerance= 0.5
    N_seg = len(seg.label)
    segInfor = []
    #for i in range(numChans):
    #  writer = open("segment_" + str(i+1) +  posfix + ".txt", "w")
    #  segWriter.append(writer)
    for i in range(N_seg):
        if seg.label[i] > 0:
            segInfor.append("speech " + str(seg.start[i] * 0.01) + " " + str(seg.stop[i] * 0.01))  
        
    writer = open(in_out_dir + "/segment_chan" + posfix + ".txt", "w")
    for j in range(len(segInfor)):
        writer.write(segInfor[j] + "\n")
    writer.close()
    currStart = 0
    currEnd = 0
    currSeg = 0
    wf = open(segmentOrgFile, "w")
    wf2 = open(segmentBuffFile, "w")
    for i in range(N_seg):
        if seg.label[i] > 0:
            start = seg.start[i] * 0.01
            end = seg.stop[i] * 0.01
            #print("Consider: " + posfix + " and (" + str(currStart) + "," + str(currEnd) + ")")
            if currSeg == 0: #Already finish the process of merging close segments, now starting a new segment
                currSeg = 1
                currEnd = end;
                currStart = start;
            else: # In the process of merging close segments
                if start - currEnd <= tollerance: #The segment aSeg can be merged into the current segment
                    currEnd = end;
                else:#The previous segment is too far from the current segment => Write down the previous segment and start new segment
                    if(currEnd - currStart) < discardShortSegLength: #The current segment is too short
                        print("Discard short segment " + str(currStart) + " " + str(currEnd))
                        #logger(Const.CROSSTALK_LOGGER_NAME).info("Discard short segment " + str(currStart) + " " + str(currEnd))
                    else:
                        #logger(Const.CROSSTALK_LOGGER_NAME).info("Speech " + str(currStart) + " " + str(currEnd))
                        actStart = currStart - buffExt
                        actEnd = currEnd + buffExt
                        if actStart < 0:
                            actStart = 0
                        if actEnd > maxLength:
                            actEnd = maxLength
                        print(posfix + " ->	Speech " + str(currStart) + " " + str(currEnd))
                        wf.write("speech " + str(currStart) + " " + str(currEnd) +  "\n" )
                        wf2.write("speech " + str(actStart) + " " + str(actEnd) +  "\n" )
                    currStart = start
                    currEnd = end
                    currSeg = 1
    
    actStart = currStart - buffExt
    actEnd = currEnd + buffExt
    if actStart < 0:
    	actStart = 0
    if actEnd > maxLength:
    	actEnd = maxLength
    print(posfix + " ->	Speech " + str(currStart) + " " + str(currEnd))
    wf.write("speech " + str(currStart) + " " + str(currEnd) +  "\n" )
    wf2.write("speech " + str(actStart) + " " + str(actEnd) +  "\n" )
    wf.close()
    wf2.close()

def calEnergy(audio, fs, postfix):
    #numSample = len(audio[0,:])
    powerSum = numpy.empty((0,0), dtype=numpy.double)
    numOfFrame = -1
    numChan = len(audio)
    for i in range(len(audio)):
        curr_powerSum = calPowerSum_segment(audio[i,:], fs)
        if i == 0:
            powerSum = numpy.empty((0,len(curr_powerSum)), dtype=numpy.double)
            numOfFrame = len(curr_powerSum)      
        powerSum = numpy.vstack([powerSum, curr_powerSum])

    #logger(Const.CROSSTALK_LOGGER_NAME).info("Number of frame this segment = " + str(numOfFrame))
    print("Number of frame this segment = " + str(numOfFrame))
    #hsize = int(fs * 0.01);
    #flength = int(fs * 0.025);
    
    sumAll = numpy.sum(powerSum, axis = 0)
    powerSum_norm = numpy.zeros(powerSum.shape)
    for i in range(numOfFrame):
        powerSum_norm[:i] = numpy.divide(powerSum[:i], sumAll);
    #logger(Const.CROSSTALK_LOGGER_NAME).info("Finish normalising energy")
    #for i in range(len(audio)):
    #    numpy.savetxt('data/power_sum_' + postfix + '_chan_' + str(i+1) + '.txt', powerSum_norm[i, :], fmt = '%.3f')
    #print("Finish normalising energy")
    
    channels = numpy.argmax(powerSum_norm, axis = 0)
    speech_energy = numpy.zeros(numChan, dtype=numpy.double)
    count_speech = numpy.zeros(numChan, dtype=numpy.int)
    for i in range(numOfFrame):
        active_chan = channels[i]
        speech_energy[active_chan] = speech_energy[active_chan] + powerSum[active_chan,i]
        count_speech[active_chan] = count_speech[active_chan] + 1
    for i in range(len(channels)):
        channels[i] = channels[i] + 1
    print("Count speech = " + str(count_speech) + ", energy = " + str(speech_energy))
    return count_speech, speech_energy


def genFinalVAD(audio, fs, postfix, speech_energy_all, nframeEachSeg):
    #numSample = len(audio[0,:])
    powerSum = numpy.empty((0,0), dtype=numpy.double)
    numOfFrame = -1
    numChan = len(audio)
    for i in range(len(audio)):
        curr_powerSum = calPowerSum_segment(audio[i,:], fs)
        if i == 0:
            powerSum = numpy.empty((0,len(curr_powerSum)), dtype=numpy.double)
            numOfFrame = len(curr_powerSum)      
        powerSum = numpy.vstack([powerSum, curr_powerSum])

    #logger(Const.CROSSTALK_LOGGER_NAME).info("Number of frame this segment = " + str(numOfFrame))
    print("Number of frame this segment = " + str(numOfFrame))
    #hsize = int(fs * 0.01);
    #flength = int(fs * 0.025);
    
    sumAll = numpy.sum(powerSum, axis = 0)
    powerSum_norm = numpy.zeros(powerSum.shape)
    for i in range(numOfFrame):
        powerSum_norm[:i] = numpy.divide(powerSum[:i], sumAll);
    #logger(Const.CROSSTALK_LOGGER_NAME).info("Finish normalising energy")
    for i in range(len(audio)):
        numpy.savetxt('data/power_sum_seg' + str(postfix) + '_chan_' + str(i+1) + '.txt', powerSum_norm[i, :], fmt = '%.3f')
    print("Finish normalising energy")
    
    channels = numpy.argmax(powerSum_norm, axis = 0)
    for i in range(len(channels)):
        channels[i] = channels[i] + 1
    
    numpy.savetxt('data/vad_energy_raw_seg' + str(postfix) + '.txt', channels, fmt = '%d')
    
    
    #channels_smooth = MedianFilter(channels, 51)
    #numpy.savetxt('data/vad_energy_' + postfix + '.txt', channels_smooth, fmt = '%d')
    spkID_seg = label2seg(channels) # An struct Segment{int start[]; int end[]; int channelId[]}
    
    allVAD = numpy.zeros((numChan,numOfFrame), dtype = numpy.int)
    for i in range(numOfFrame):
        activeChan = channels[i] - 1
        for j in range(numChan):
            if j == activeChan or powerSum[j, i] >= 0.2 * speech_energy_all[j]:
                allVAD[j, i] = 1
            else:
                allVAD[j, i] = 0
    for i in range(numChan):
        allVAD[i,:] = MedianFilter(allVAD[i,:], 25)
    pitchVAD = numpy.empty((0,0), dtype=numpy.int)
    for i in range(len(audio)):
        comb_vad_flag, energy_val, periodicVal = CombVAD(audio[i,:], fs, 0)
        currVAD = comb_vad_flag;
        #currVAD = scipy.interpolate.interp1d([1:length(currVAD)]*2, currVAD, 1:(2*length(currVAD)), 'nearest', 'extrap');
        idx_arr = numpy.zeros(len(currVAD), dtype = numpy.int)
        for j in range(len(currVAD)):
            idx_arr[j] = j * 2
        func = scipy.interpolate.interp1d(idx_arr, currVAD, kind='nearest', fill_value = 'extrapolate');
        new_arr_idx = list(range(2 * len(currVAD)));
        currVAD = func(new_arr_idx)
        currVAD_post = PostProcessVAD(currVAD, 20)
        #Now make pitchVAD equals length with energy VAD
        numFramePitchVAD = len(currVAD_post)
        diffFramePitchVAD_vs_eneryVAD = numOfFrame - numFramePitchVAD
        tempArr=numpy.empty(diffFramePitchVAD_vs_eneryVAD) 
        tempArr.fill(currVAD_post[numFramePitchVAD-1])
        currVAD_post = numpy.append(currVAD_post, tempArr)
        #for j in range(diffFramePitchVAD_vs_eneryVAD):
        #    currVAD_post = numpy.append(currVAD_post[numFramePitchVAD-1])
        #print("Length of VAD = " + str(len(currVAD_post)))
        if i == 0:
            #print("Initialize with " + str(len(currVAD_post)))
            pitchVAD = numpy.empty((0,len(currVAD_post)), dtype=numpy.int)
        pitchVAD = numpy.vstack([pitchVAD, currVAD_post])
        numpy.savetxt('data/pitchVAD_chan' + str(i+1) + "_seg" + str(postfix) + '.txt', currVAD_post, fmt = '%f')
    allVAD_refine = numpy.zeros((numChan,numOfFrame), dtype = numpy.int)
    rank_all = numpy.zeros((numChan,numOfFrame), dtype = numpy.float)
    
    for j in range(numOfFrame):
        rank = ss.rankdata(powerSum_norm[:, j])
        rank = (len(rank)+1) - rank
        rank_all[:, j] = rank
    for i in range(numChan):
        data = []
        for j in range(numOfFrame):
            feats = []
            feats.append(0)
            feats.append(pitchVAD[i, j])
            feats.append(powerSum[i, j])
            feats.append(powerSum_norm[i, j] * numChan)
            feats.append(rank_all[i, j])
            feats.append(powerSum[i, j] - speech_energy_all[i])
            feats.append((numpy.sum(pitchVAD[:, j]) - pitchVAD[i,j])/(numChan-1))
            feats.append((numpy.sum(powerSum[:, j]) - powerSum[i,j])/(numChan-1))
            data.append(feats)
        numpy.savetxt('data/feats_DNN_chan' + str(i+1) + "_seg" + str(postfix) + '.txt', numpy.array(data), fmt = '%f')
    return 0


############# Main #####################

def modifySignal(tmp_audio, spkID_smooth_refined, hsize, flength, numFrameToExtend):
    #logger(Const.CROSSTALK_LOGGER_NAME).info("---- Modify the signal -----")
    numFrame = spkID_smooth_refined.shape[1]
    #numFrameToExtend = 5
    print("---- Modify the signal -----")
    audio = numpy.zeros((tmp_audio.shape[0], tmp_audio.shape[1]), dtype= numpy.double)
    numSample = len(tmp_audio[0,:])
    #logger(Const.CROSSTALK_LOGGER_NAME).info("Number of sample = " + str(numSample))
    print("Number of sample = " + str(numSample))
    
    if audio.shape[0] == 0 or audio.shape[1] == 0:
        print("Wrong" + str(audio.shape[0]) + ", " + str(audio.shape[1]))
        #logger(Const.CROSSTALK_LOGGER_NAME).error("Wrong" + str(audio.shape[0]) + ", " + str(audio.shape[1]))
    for j in range(spkID_smooth_refined.shape[1]):
        startFrameExtend = int(max(0,j - numFrameToExtend)) * hsize
        endFrameExtend   = int(min(numSample, (j + numFrameToExtend-1) * hsize + flength))
        #activeChannel = spkID_smooth_refined[j] 
        start_frame = j * hsize
        end_frame = min(start_frame + flength, numSample)        
        for i in range(len(tmp_audio)):
            if spkID_smooth_refined[i,j] == 1:
                if j > 0 and spkID_smooth_refined[i,j-1]== 0:
                    start_frame_curChan = startFrameExtend
                else:
                    start_frame_curChan = start_frame 
                end_frame_curChan = end_frame
                for k in range (start_frame_curChan,end_frame_curChan):
                    audio[i, k] = tmp_audio[i, k]
            else:
                if j > 0 and spkID_smooth_refined[i,j-1]== 1:
                    start_frame_curChan = start_frame
                    end_frame_curChan = endFrameExtend
                    for k in range (start_frame_curChan,end_frame_curChan):
                        audio[i, k] = tmp_audio[i, k]
    return audio


def loadPredictedResult(DNN_out, segId, numChan, recId):
    tmp_dat = numpy.loadtxt(DNN_out + "/" + recId + "_chan1_seg" + str(segId) + ".txt");
    numOfFrame = tmp_dat.shape[0]
    allVAD_refine = numpy.zeros((numChan,numOfFrame), dtype = numpy.int)
    for i in range(numChan):
        currChanVAD = numpy.loadtxt(DNN_out + "/" + recId + "_chan" +  str(i + 1) + "_seg" + str(segId) + ".txt")
        if post_pro == 1:
            allVAD_refine[i, :] = PostProcessVAD(currChanVAD, 20)#MedianFilter(currChanVAD, 25); #An array with 80227 items (frames). A refined of spkID_smooth
        elif post_pro == 2:
            allVAD_refine[i, :] = PostProcessVAD(currChanVAD, 7)
        elif post_pro == 3:
            allVAD_refine[i, :] = MedianFilter(currChanVAD, 25)
        elif post_pro == 4:
            allVAD_refine[i, :] = MedianFilter(currChanVAD, 7)
        else:
            allVAD_refine[i, :] = currChanVAD
        numpy.savetxt(DNN_out + "/chan" + str(i + 1) + "_seg" + str(segId) + "_" + featSet + "_labels_postproc" + str(post_pro) + ".txt", allVAD_refine[i, :], fmt = '%f')   
    return allVAD_refine


def crossTalkRemover(closeTalkChans, closeTalkChannelsAfterVAD, DNN_out, segmentBuff, segmentOrg, recId):
    tollerance = 0.12 # Merge 2 speech segments that have their gap smaller than this tollerance
    discardShortSeg = 0.4 # Ignore speech segment that smaller than this value
    frameExt = 5
    buffExt = frameExt * 0.01
    tmp_dat, fs = sf.read(closeTalkChans[0])
    numSample = len(tmp_dat)
    #logger(Const.CROSSTALK_LOGGER_NAME).info("Number of sample = " + str(numSample))
    print("Number of sample = " + str(numSample))
  
    audio = numpy.empty((0,numSample), dtype=numpy.double)
    for i in range(len(closeTalkChans)):
        dat, fs1 = sf.read(closeTalkChans[i])
        audio = numpy.vstack([audio, dat])
    nframeEachSeg = 20000
    
    flength = int(fs * 0.025);
    hsize = int(fs * 0.01);
    numSampleEachSeg = (nframeEachSeg - 1) * hsize + flength
    #logger(Const.CROSSTALK_LOGGER_NAME).info("Number of sample each segment = " + str(numSampleEachSeg))
    print("Number of sample each segment = " + str(numSampleEachSeg))
  
    sampleStartFrame = 0
    sampleEndFrame = min(numSample, sampleStartFrame + numSampleEachSeg)
  
    segCount = 1
    numChan = len(audio)
    vad_all = numpy.empty((numChan,0), dtype=numpy.int)
    while sampleStartFrame < numSample:
        #logger(Const.CROSSTALK_LOGGER_NAME).info("******** Segment " + str(segCount) + ", Start sample = " + str(sampleStartFrame) + ", end sample = " + str(sampleEndFrame) + " *********")
        print("******** Segment " + str(segCount) + ", Start sample = " + str(sampleStartFrame) + ", end sample = " + str(sampleEndFrame) + " *********")
            
        curr_seg_audio = audio[:, sampleStartFrame:sampleEndFrame]
        spkID_smooth_refined = loadPredictedResult(DNN_out, segCount, audio.shape[0], recId)
        if segCount == 1:
            vad_all = deepcopy(spkID_smooth_refined)
        else:
            vad_all = numpy.hstack((vad_all, spkID_smooth_refined))
        audio[:, sampleStartFrame:sampleEndFrame] = modifySignal(audio[:, sampleStartFrame:sampleEndFrame], spkID_smooth_refined, hsize , flength, frameExt)
        segCount = segCount + 1
        sampleStartFrame = sampleStartFrame + numSampleEachSeg
        sampleEndFrame = min(numSample, sampleEndFrame + numSampleEachSeg)
    maxLength = len(vad_all[0]) * 0.01
    print ("Max length = " + str(maxLength))
    for i in range(numChan):
        vad_seg = label2seg(vad_all[i]) # An struct Segment{int start[]; int end[]; int channelId[]}
        writeSeg(vad_seg, str(i+1) + "_" + featSet + "_postproc" + str(post_pro), maxLength, buffExt, discardShortSeg, tollerance, segmentBuff[i], segmentOrg[i])
    for i in range(len(closeTalkChans)):
        sf.write(closeTalkChannelsAfterVAD[i], audio[i,:], fs)
        
    docid_fullname = closeTalkChans[0] # path_to_output_docid/channelx/clean/filename.wav
    dir_with_docid = (os.path.abspath(os.path.join(docid_fullname, '../../..')))
    paths = os.path.split(os.path.abspath(dir_with_docid))
    docid = paths[1]
    #for i in range(len(closeTalkChans)):
    #    #diarizationDir = os.path.join(dir_with_docid, "channel" + str(i+1), Const.FILE_DIARIZATION_DIR)
    #    diarizationDir = "."
    #    combineSegment(i + 1, "channel", numSample, segCount - 1, tollerance, discardShortSeg, nframeEachSeg * 0.01, docid, diarizationDir )

closedTalkChans = orgAudioFiles.split(";")
closedTalkChans_alfterVAD = outAudioFiles.split(";")
segmentBuff = segmentsFilesBuff.split(";")
segmentOrg = segmentsFilesOrg.split(";")
crossTalkRemover(closedTalkChans, closedTalkChans_alfterVAD, in_out_dir, segmentBuff, segmentOrg, recId)
