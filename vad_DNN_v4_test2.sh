#!/bin/bash
. path.sh
#Train the DNN

#### Setting
cmd='slurm.pl --quiet --gres=gpu:1'
postPro=1
featSet=allFeats



audioFiles="test_data/profChngAndrewArmy/MNT1.WAV;test_data/profChngAndrewArmy/MNT2.WAV;test_data/profChngAndrewArmy/MNT3.WAV;test_data/profChngAndrewArmy/MNT4.WAV"

outAudioFiles="test_data/profChngAndrewArmy/chan1_${featSet}_postpro$postPro.wav;test_data/profChngAndrewArmy/chan2_${featSet}_postpro$postPro.wav;test_data/profChngAndrewArmy/chan3_${featSet}_postpro$postPro.wav;test_data/profChngAndrewArmy/chan4_${featSet}_postpro$postPro.wav"
segmentFiles="test_data/profChngAndrewArmy/segment_chan1_${featSet}_postproc${postPro}_discard0.07_merged0.12_buffAtSegBound.txt;test_data/profChngAndrewArmy/segment_chan2_${featSet}_postproc${postPro}_discard0.07_merged0.12_buffAtSegBound.txt;test_data/profChngAndrewArmy/segment_chan3_${featSet}_postproc${postPro}_discard0.07_merged0.12_buffAtSegBound.txt;test_data/profChngAndrewArmy/segment_chan4_${featSet}_postproc${postPro}_discard0.07_merged0.12_buffAtSegBound.txt"
segmentFilesOrg="test_data/profChngAndrewArmy/segment_chan1_${featSet}_postproc${postPro}_discard0.07_merged0.12.txt;test_data/profChngAndrewArmy/segment_chan2_${featSet}_postproc${postPro}_discard0.07_merged0.12.txt;test_data/profChngAndrewArmy/segment_chan3_${featSet}_postproc${postPro}_discard0.07_merged0.12.txt;test_data/profChngAndrewArmy/segment_chan4_${featSet}_postproc${postPro}_discard0.07_merged0.12.txt"


recName=profChngAndrew
datadir=data_Tung/$recName
mkdir -p $datadir

echo "##################### Test recoding $recName #############################"
echo "****************************************************************************"
############################# Step 1: Extracting features for test recording #########################
if [ 0 -eq 1 ]; then
	./process_one_recording.sh $audioFiles $datadir $recName
fi

############################# Step 2: create VAD label using the trained DNN #########################
tgbnf=dnn_output_${recName}
mkdir -p $tgbnf/log
mkdir -p data/${recName}_bnf
if [ 0 -eq 1 ]; then
  steps/nnet/make_bn_feats.sh  --cmd "slurm.pl --quiet --gres=gpu:1" --remove-last-components 0 \
  --use-gpu yes data/${recName}_bnf data/${recName}_addFeats dnn $tgbnf/log $tgbnf || exit 1  
fi

## Convert kaldi to txt format to process later on
if [ 0 -eq 1 ]; then
	files=$tgbnf/*.ark
	for f in $files
	do
		python convertToTxt.py $f $tgbnf

	done	

fi


################## Step 3: Modify the audio, i.e. delete cross talk using the predicted VAD. Also convert the frame-base VAD to segment-base VAD, so that it can replace LIUM segment. ####
if [ 0 -eq 1 ]; then
	python vad6_DNN_test_feats_postProcess_v4.py $audioFiles $tgbnf $postPro $outAudioFiles $featSet $segmentFiles $segmentFilesOrg $recName
fi

################# Step 4: To evaluate the VAD performance only. In case you have label ##############
if [ 0 -eq 1 ]; then
	
	### Following command are for evaluation only, i.e. if we know the true VAD. In general case if you only want to generate the VAD, just comment those commands ######
	### For this test set, the accuracy is about 91% for channel 1 and 4, 93-94% for channel 2 and 3. Note that DNN trained in different machines might produce different results since it is initialized differently
	echo "****************Feature set $featSet, Channel 1 *****************"
	python performance_evaluation_v3_allowPaddingAndMerging.py /home2/tungpham/vad_crosstalkRemover/mydata/mml-12-sept-2017/profChngAndrewArmy/new_vad/MNT1.txt test_data/profChngAndrewArmy/segment_chan1_${featSet}_postproc${postPro}_discard0.07_merged0.12.txt 80226
	echo "****************Feature set $featSet, Channel 2 *****************"
	python performance_evaluation_v3_allowPaddingAndMerging.py /home2/tungpham/vad_crosstalkRemover/mydata/mml-12-sept-2017/profChngAndrewArmy/new_vad/MNT2_v2.txt test_data/profChngAndrewArmy/segment_chan2_${featSet}_postproc${postPro}_discard0.07_merged0.12.txt 80226
	echo "****************Feature set $featSet, Channel 3 *****************"
	python performance_evaluation_v3_allowPaddingAndMerging.py /home2/tungpham/vad_crosstalkRemover/mydata/mml-12-sept-2017/profChngAndrewArmy/new_vad/MNT3_v2.txt test_data/profChngAndrewArmy/segment_chan3_${featSet}_postproc${postPro}_discard0.07_merged0.12.txt 80226
	echo "****************Feature set $featSet, Channel 4 *****************"
	python performance_evaluation_v3_allowPaddingAndMerging.py /home2/tungpham/vad_crosstalkRemover/mydata/mml-12-sept-2017/profChngAndrewArmy/new_vad/MNT4_v2.txt test_data/profChngAndrewArmy/segment_chan4_${featSet}_postproc${postPro}_discard0.07_merged0.12.txt 80226
fi


