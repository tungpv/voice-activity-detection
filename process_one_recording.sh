#!/bin/bash
. path.sh


audioFiles=$1
datadir=$2
recName=$3
#### Setting
echo "======= Processing audio files with following setting======"
echo "file list = $audioFiles" 
echo "------------------------------------------------------------"

mkdir -p $datadir


rm -r data/$recName
rm -r tmp_wav/$recName
mkdir -p data/$recName
mkdir -p tmp_wav/$recName
python2.7 segmentation_aud.py $audioFiles tmp_wav/$recName $datadir $recName
./prepare_test_data.sh tmp_wav/$recName data/$recName

numChan=`cat $datadir/numChan`
numSeg=`cat $datadir/numSeg`

python2.7 vad6_DNN_train_extractDat.py $datadir $audioFiles


rm -r data/${recName}_addFeats
mkdir -p data/${recName}_addFeats/data
cp data/${recName}/{text,utt2spk,spk2utt,wav.scp,segments} data/${recName}_addFeats
python2.7 combineAllFeats.py $datadir $recName data/$recName $numChan $numSeg data/${recName}_addFeats/data

for (( i=1; i<=$numChan; i=i+1 ))
do
    for (( j=1; j<=$numSeg; j=j+1 ))
    do
        copy-feats ark:data/${recName}_addFeats/data/allFeats_${recName}_chan${i}_seg${j}_tmp.ark ark,scp:data/${recName}_addFeats/data/${recName}_chan${i}_seg${j}.ark,data/${recName}_addFeats/data/${recName}_chan${i}_seg${j}.scp
    done
done

cat data/${recName}_addFeats/data/${recName}_chan*_seg*.scp > data/${recName}_addFeats/feats.scp

mkdir data/${recName}_addFeats/data_cmvn
steps/compute_cmvn_stats.sh data/${recName}_addFeats data/${recName}_addFeats/data_cmvn

