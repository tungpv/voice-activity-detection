from __future__ import print_function
import kaldi_io_vesis84
import numpy
import os
import sys

datadir = sys.argv[1]
currChan = int(sys.argv[2])
currSeg = int(sys.argv[3])

dat1 = numpy.loadtxt(datadir + "/dat_chan" + str(currChan) + "_seg" + str(currSeg) + "_kaldi_mfcc_pitch.txt")
dat2 = numpy.loadtxt(datadir + "/feats_DNN_chan" + str(currChan) + "_seg" + str(currSeg) + ".txt")
if len(dat1) != len(dat2):
    print("Error! Mismatch len: " + str(len(dat1)) + " vs. " + str(len(dat2)))
    exit(1)
dat = numpy.hstack((dat2, dat1))
print("Size of all data = " + str(dat.shape))
numpy.savetxt(datadir + "/dat_chan" + str(currChan) + "_seg" + str(currSeg) + "_allFeats.txt", dat, fmt="%.6f")
    
